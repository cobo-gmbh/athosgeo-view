/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgExtractCategory.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <set>
#include <map>
#include <locale>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <utilNormNames.h>
#include <atgCategoriesManager.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkStringArray.h>
#include <vtkDataArray.h>
#include <vtkSmartPointer.h>
#include <vtkLongLongArray.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgExtractCategory.h>

vtkStandardNewMacro(vtkAtgExtractCategory)

vtkAtgExtractCategory::vtkAtgExtractCategory()
:   PortId(nullptr),
    Category(nullptr),
    Value(nullptr),
    EarlyApply(false),
    GridMTime(0)
{
}

vtkAtgExtractCategory::~vtkAtgExtractCategory()
{
    SetValue(nullptr);
    SetCategory(nullptr);
    SetPortId(nullptr);
}

int vtkAtgExtractCategory::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // unstructured grid
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgExtractCategory::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // grid with Pit variable
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgExtractCategory::RequestData(vtkInformation* request,
                                       vtkInformationVector** inputVector,
                                       vtkInformationVector* outputVector)
{
    // get the input grid
    vtkUnstructuredGrid* grid = vtkUnstructuredGrid::GetData(inputVector[0], 0);
    if(nullptr == grid)
        return 1; // should not happen - but also no crash

    // if we have no category/values info, or if the grid has changed, we refresh
    // that property
    if((GridMTime != grid->GetMTime()) && (nullptr != PortId))
    {
        GridMTime = grid->GetMTime();

        // make sure the change is noted by the property widgets
        std::string currentCat((nullptr != Category) ? Category : ""),
                    currentVal((nullptr != Value) ? Value : "");
        atgCategoriesManager::instance().findCategoriesValues(grid, PortId, currentCat, currentVal);

        // set the values
        SetCategory(currentCat.c_str());
        SetValue(currentVal.c_str());
    }

    // done if this is only an "early apply" for gathering category and value infos
    if(EarlyApply)
        return 1;

    // if we still do not have a category and a value
    if((nullptr == Category) || (nullptr == Value))
        return 1;

    // try to find the category attribute in the cell data, and do nothing
    // if not found
    vtkAbstractArray* catArr = grid->GetCellData()->GetAbstractArray(Category);
    if(nullptr == catArr)
        return 1;

    // try down casts
    vtkStringArray* strArr = vtkStringArray::SafeDownCast(catArr);
    vtkDataArray* numArr = vtkDataArray::SafeDownCast(catArr);
    if((nullptr == strArr) && (nullptr == numArr))
        return 1; // should really not happen!!

    // one more test: in case of a numeric array: is our value really numeric?
    double number = 0.;
    if(nullptr != numArr)
    {
        try
        {
            number = std::stod(Value);
        }
        catch(...)
        {
            return 1;
        }
    }
    number = ::floor(number + .5);

    // generate an INDEX selection for extracting the remaining blocks
    vtkSmartPointer<vtkLongLongArray> selIds = vtkSmartPointer<vtkLongLongArray>::New();
    selIds->SetName("__SELECTION__");
    vtkSmartPointer<vtkSelection> sel = vtkSmartPointer<vtkSelection>::New();
    vtkSmartPointer<vtkSelectionNode> sn = vtkSmartPointer<vtkSelectionNode>::New();
    sn->SetFieldType(vtkSelectionNode::CELL);
    sn->SetContentType(vtkSelectionNode::INDICES);
    sn->SetSelectionList(selIds);
    sel->AddNode(sn);

    // handle string category array
    if(nullptr != strArr)
    {
        for(vtkIdType c = 0; c < strArr->GetNumberOfTuples(); ++c)
        {
            std::string val = strArr->GetValue(c);
            boost::trim(val);
            if(boost::iequals(val, Value))
                selIds->InsertNextValue(c);
        }
    }

    // handle numeric category array
    // note that we know here already that numArr does exist: see above
    else
    {
        for(vtkIdType c = 0; c < numArr->GetNumberOfTuples(); ++c)
        {
            double num = ::floor(numArr->GetVariantValue(c).ToDouble() + .5);
            if(num == number)
                selIds->InsertNextValue(c);
        }
    }

    // do the extraction using the selection
    // note: the assumption is that this will generate a new model ("deep extract")
    vtkSmartPointer<vtkExtractSelection> extractSel = vtkSmartPointer<vtkExtractSelection>::New();
    extractSel->SetInputData(grid);
    extractSel->SetInputData(1, sel);
    extractSel->Update();

    // output the filtered data
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->ShallowCopy(extractSel->GetOutput());

    return 1;
}
