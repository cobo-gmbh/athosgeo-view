/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgExtractCategory.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgExtractCategoryFilter_h
#define vtkAtgExtractCategoryFilter_h

#include <vector>
#include <string>
#include <vtkSetGet.h>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgExtractCategoryModuleModule.h>

class vtkUnstructuredGrid;
class vtkInformation;

class ATGEXTRACTCATEGORYMODULE_EXPORT vtkAtgExtractCategory: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgExtractCategory* New();
    vtkTypeMacro(vtkAtgExtractCategory, vtkUnstructuredGridAlgorithm)

    vtkSetStringMacro(PortId)
    vtkGetStringMacro(PortId)

    vtkSetStringMacro(Category)
    vtkGetStringMacro(Category)

    vtkSetStringMacro(Value)
    vtkGetStringMacro(Value)

    vtkSetMacro(EarlyApply, bool)
    vtkGetMacro(EarlyApply, bool)

protected:

    vtkAtgExtractCategory();
    ~vtkAtgExtractCategory();

    virtual int RequestData(vtkInformation* request, vtkInformationVector** inVec,
                            vtkInformationVector* outVec) override;

    virtual int FillInputPortInformation(int port, vtkInformation* info);
    virtual int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // find all available categories and their values in the input grid and store them
    // in the categories manager
    void findCategoriesValues(vtkUnstructuredGrid* grid);

    char *PortId,
         *Category,
         *Value;

    // remember key and current modification time for the category/value data
    vtkMTimeType GridMTime;
    bool EarlyApply;

};

#endif
