/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgMergeByCategory.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/dynamic_bitset.hpp>
#include <utilNormNames.h>
#include <utilSummarizeAttributes.h>
#include <utilTrackDefs.h>
#include <vtkTable.h>
#include <vtkVariant.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCleanUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkStringArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTableToBlocks.h>
#include <vtkAtgMergeByCategory.h>

// absolute tolerance for merging common points of neighbor cells
static const double eps = .001;

// cell id which means "no cell neighbor in this direction"
static const vtkIdType noNeighborCellId = -10000;

vtkStandardNewMacro(vtkAtgMergeByCategory)

vtkAtgMergeByCategory::vtkAtgMergeByCategory()
:   Category(nullptr)
{
}

vtkAtgMergeByCategory::~vtkAtgMergeByCategory()
{
    SetCategory(nullptr);
}

void vtkAtgMergeByCategory::AddCategoryPercAtts(char const* cat)
{
    CategoryPercAtts.insert(std::string(cat));
    Modified();
}

void vtkAtgMergeByCategory::ClearCategoryPercAtts()
{
    CategoryPercAtts.clear();
    Modified();
}

int vtkAtgMergeByCategory::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // unstructured grid
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeByCategory::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // grid with Pit variable
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeByCategory::RequestData(vtkInformation* request,
                                       vtkInformationVector** inputVector,
                                       vtkInformationVector* outputVector)
{
    TRACK_FUNCTION_TIME

    // get the input grid
    vtkUnstructuredGrid* input = vtkUnstructuredGrid::GetData(inputVector[0], 0);
    if(nullptr == input)
        return 1; // should not happen - but also no crash

    // in order to easily find upper/lower neighbor blocks, we need to ensure that
    // duplicate points are allways joined
    auto cleanFilter = vtkSmartPointer<vtkCleanUnstructuredGrid>::New();
    cleanFilter->SetInputData(input);
    cleanFilter->SetToleranceIsAbsolute(true);
    cleanFilter->SetAbsoluteTolerance(eps);
    cleanFilter->Update();
    vtkUnstructuredGrid* grid = cleanFilter->GetOutput();

    TRACK_ELAPSED("clean filter done")

    // make sure we have a category - otherwise we cannot do anything
    if((nullptr == Category) || (0 == ::strlen(Category)))
    {
        vtkOutputWindowDisplayWarningText("No Category attribute specified\n");
        return 1;
    }

    // bookkeeping of unprocessed blocks - initially all
    boost::dynamic_bitset<> not_processed_blocks(grid->GetNumberOfCells());
    not_processed_blocks.set(0, not_processed_blocks.size(), true);

    // primary required columns
    std::string nameCent = utilNormNames::getName(utilNormNames::BLOCKCENTER),
                nameSize = utilNormNames::getName(utilNormNames::BLOCKSIZE),
                nameAng = utilNormNames::getName(utilNormNames::ANGLE),
                nameLS = utilNormNames::getName(utilNormNames::LS),
                nameKTons = utilNormNames::getName(utilNormNames::KTONS),
                nameTons = utilNormNames::getName(utilNormNames::TONS);
    vtkDataArray *centArr = grid->GetCellData()->GetArray(nameCent.c_str()),
                 *sizeArr = grid->GetCellData()->GetArray(nameSize.c_str()),
                 *angArr = grid->GetCellData()->GetArray(nameAng.c_str()),
                 *lsArr = grid->GetCellData()->GetArray(nameLS.c_str()),
                 *tonnArr = grid->GetCellData()->GetArray(nameKTons.c_str());
    if(nullptr == tonnArr)
        tonnArr = grid->GetCellData()->GetArray(nameTons.c_str());
    vtkAbstractArray* catArr = grid->GetCellData()->GetAbstractArray(Category);
    std::vector<vtkAbstractArray*> percCatAttArrs;
    for(auto it = CategoryPercAtts.begin(); it != CategoryPercAtts.end(); ++it)
        percCatAttArrs.push_back(grid->GetCellData()->GetAbstractArray(it->c_str()));
    std::vector<std::string> notFound;
    if(nullptr == centArr)
        notFound.push_back(nameCent);
    if(nullptr == sizeArr)
        notFound.push_back(nameSize);
    if(nullptr == catArr)
        notFound.push_back(Category);
    if(nullptr == tonnArr)
        notFound.push_back(nameKTons + " or " + nameTons);
    for(std::size_t i = 0; i < percCatAttArrs.size(); ++i)
    {
        if(nullptr == percCatAttArrs[i])
        {
            auto it = CategoryPercAtts.begin();
            std::advance(it, i);
            notFound.push_back(*it);
        }
    }
    if(!notFound.empty())
    {
        vtkOutputWindowDisplayWarningText(std::string("Required input column(s) not found:\n" +
                                                      boost::algorithm::join(notFound, ", ") +
                                                      "\n(or some accepted equivalent(s))\n").c_str());
        return 1;
    }

    // prepare a summarizer for the block attributes
    std::unique_ptr<utilSummarizeAttributes> summAtts(new utilSummarizeAttributes(grid->GetCellData()));
    summAtts->setValuesOnly(true);
    summAtts->setSelectedOnly(true);
    summAtts->setIncludeCementModuli(nullptr != lsArr);
    summAtts->setNormalization(std::vector<std::string>(1, std::string(tonnArr->GetName())));

    // we start by generating arrays for a table that will afterwards be converted to a block model
    vtkSmartPointer<vtkDoubleArray> newCentArr = vtkSmartPointer<vtkDoubleArray>::New(),
                                    newSizeArr = vtkSmartPointer<vtkDoubleArray>::New(),
                                    newAngArr;
    newCentArr->SetName(nameCent.c_str());
    newCentArr->SetNumberOfComponents(3);
    newSizeArr->SetName(nameSize.c_str());
    newSizeArr->SetNumberOfComponents(3);
    if(nullptr != angArr)
    {
        newAngArr = vtkSmartPointer<vtkDoubleArray>::New();
        newAngArr->SetName(nameAng.c_str());
    }
    vtkSmartPointer<vtkAbstractArray> newCatArr;
    newCatArr.TakeReference(catArr->NewInstance());
    newCatArr->SetName(Category);

    // generate category value attributes if required
    std::map<std::string, vtkDoubleArray*> catValArrs;
    for(auto cit = percCatAttArrs.begin(); cit != percCatAttArrs.end(); ++cit)
    {
        // generate a set of all values of the category
        std::set<vtkVariant> catVals;
        for(vtkIdType b = 0; b < (*cit)->GetNumberOfValues(); ++b)
            catVals.insert((*cit)->GetVariantValue(b));

        // generate attributes for each value of each category and append it to the blocks table
        for(auto vit = catVals.begin(); vit != catVals.end(); ++vit)
        {
            std::string name = generateCatAttName((*cit)->GetName(), *vit);
            vtkDoubleArray* arr = vtkDoubleArray::New();
            arr->SetName(name.c_str());
            catValArrs[name] = arr;
        }
    }

    // here we are going to collect the other attributes arrays
    std::map<std::string, vtkDoubleArray*> generalArrs;

    TRACK_ELAPSED("new model preparations done")
    TRACK_COLLECT_INIT(geom)
    TRACK_COLLECT_INIT(attr)

    // add new blocks as long as there are still unprocessed old blocks
    vtkIdType blk,
              newBlk = 0;
    long lastProgress = -1,
         totalBlocks = not_processed_blocks.size();
    while(not_processed_blocks.npos != static_cast<boost::dynamic_bitset<>::size_type>((blk = not_processed_blocks.find_first())))
    {
        TRACK_COLLECT_START(geom)

        long processed = totalBlocks - not_processed_blocks.count();
        int progress = ::floor(100. * processed / totalBlocks);

        if(progress != lastProgress)
        {
            // note: we have to ensure that even after 0 processed blocks we have to
            //   report 1% progress, because otherwise the progress bar ist not even shown
            std::string progressText = "processed "
                                     + std::to_string(processed)
                                     + " (of initially "
                                     + std::to_string(totalBlocks)
                                     + ") blocks";
            SetProgressText(progressText.c_str());
            UpdateProgress(std::max(.01 * progress, .01));
            lastProgress = progress;
        }

        double xCent = centArr->GetComponent(blk, 0),
               yCent = centArr->GetComponent(blk, 1),
               zCent,
               dxSize = sizeArr->GetComponent(blk, 0),
               dySize = sizeArr->GetComponent(blk, 1),
               dzSize,
               ang = ((nullptr != angArr) ? angArr->GetComponent(blk, 0) : 0.);
        vtkVariant cat = catArr->GetVariantValue(blk);
        std::vector<vtkIdType> ids;
        ids.push_back(blk);

        // in this loop, we collect the ids first upwards, then downwards
        for(int d = 0; d < 2; ++d)
        {
            vtkIdType currentBlk = blk;
            while(0 <= currentBlk)
            {
                // d == 0 -> upwards, d == 1 -> downwards
                currentBlk = getVerticalNeighbor(grid, currentBlk, (0 == d));
                if(0 > currentBlk)
                    continue;

                // we leave the loop if we are not any more in the same category value
                vtkVariant newCat = catArr->GetVariantValue(currentBlk);
                if(cat != newCat)
                    break;

                // with the current method (getVertical Neighbor), we can be sure that a found
                // upper/lower neighbor block also has the same centroid, size and angle
                ids.push_back(currentBlk);
            }
        }

        // we want the ids sorted from bottom to top
        std::sort(ids.begin(), ids.end(),
                  [centArr](auto const& a, auto const& b)
        {
            return centArr->GetComponent(a, 2) < centArr->GetComponent(b, 2);
        });

        // add a line to the new table arrays and fill in the new data
        newCatArr->Resize(newBlk + 1);
        newCatArr->SetNumberOfTuples(newBlk + 1);
        newCentArr->Resize(newBlk + 1);
        newCentArr->SetNumberOfTuples(newBlk + 1);
        newSizeArr->Resize(newBlk + 1);
        newSizeArr->SetNumberOfTuples(newBlk + 1);
        if(nullptr != angArr)
        {
            newAngArr->Resize(newBlk + 1);
            newAngArr->SetNumberOfTuples(newBlk + 1);
        }

        // same for the category value arrays, initializing them with 0. at the same time
        for(auto it = catValArrs.begin(); it != catValArrs.end(); ++it)
        {
            vtkDoubleArray* arr = it->second;
            arr->Resize(newBlk + 1);
            arr->SetNumberOfTuples(newBlk + 1);
            arr->SetValue(newBlk, 0.);
        }

        // calculate new z components for new block
        vtkIdType bottomBlk = ids.front(),
                  topBlk = ids.back();
        double zTop = centArr->GetComponent(topBlk, 2) + sizeArr->GetComponent(topBlk, 2) * .5,
               zBottom = centArr->GetComponent(bottomBlk, 2) - sizeArr->GetComponent(bottomBlk, 2) * .5;
        zCent = (zBottom + zTop) * .5;
        dzSize = zTop - zBottom;

        // enter values into new table row
        newCatArr->SetVariantValue(newBlk, cat);
        newCentArr->SetComponent(newBlk, 0, xCent);
        newCentArr->SetComponent(newBlk, 1, yCent);
        newCentArr->SetComponent(newBlk, 2, zCent);
        newSizeArr->SetComponent(newBlk, 0, dxSize);
        newSizeArr->SetComponent(newBlk, 1, dySize);
        newSizeArr->SetComponent(newBlk, 2, dzSize);
        if(nullptr != angArr)
            newAngArr->SetComponent(newBlk, 0, ang);

        // prepare an ids set for summarizing the attributes
        std::set<int> idSet;

        // remove all processed ids from the bitset and insert them into the id set
        for(auto it = ids.begin(); it != ids.end(); ++it)
        {
            not_processed_blocks.reset(*it);
            idSet.insert(*it);
        }

        TRACK_COLLECT_STOP(geom)
        TRACK_COLLECT_START(attr)

        // summarize the attributes
        vtkSmartPointer<vtkTable> attTable = vtkSmartPointer<vtkTable>::New();
        summAtts->setSelectedIds(idSet);
        summAtts->summarize(attTable);

        // go through the category attributes and calculate percentages
        // note: for each category we find the values for all blocks to be merged. The percentage we add
        //   to the respective attribute is 100 / ids.size()
        for(auto ait = percCatAttArrs.begin(); ait != percCatAttArrs.end(); ++ait)
        {
            for(auto iit = ids.begin(); iit != ids.end(); ++iit)
            {
                vtkVariant catVal = (*ait)->GetVariantValue(*iit);
                std::string attName = generateCatAttName((*ait)->GetName(), catVal);
                vtkDoubleArray* att = catValArrs[attName];
                att->SetValue(newBlk, att->GetValue(newBlk) + 100. / ids.size());
            }
        }

        // if this is the first new block, we want to generate the new columns here
        if(0 == newBlk)
        {
            for(vtkIdType c = 0; c < attTable->GetNumberOfColumns(); ++c)
            {
                std::string name = attTable->GetColumnName(c);
                if(name == utilNormNames::getName(utilNormNames::NUMITEMS))
                    continue;
                vtkDoubleArray* newArr = vtkDoubleArray::New();
                newArr->SetName(name.c_str());
                generalArrs[name] = newArr;
            }
        }

        // for all blocks, we can now copy the calculated values
        for(vtkIdType c = 0; c < attTable->GetNumberOfColumns(); ++c)
        {
            std::string name = attTable->GetColumnName(c);
            if(name == utilNormNames::getName(utilNormNames::NUMITEMS))
                continue;
            vtkDoubleArray* arr = generalArrs[name];
            arr->Resize(newBlk + 1);
            arr->SetNumberOfTuples(newBlk + 1);
            arr->SetValue(newBlk, attTable->GetColumnByName(name.c_str())->GetVariantValue(0).ToDouble());
        }

        // increment new block id
        ++newBlk;

        TRACK_COLLECT_STOP(attr)
    }

    TRACK_COLLECT_REPORT(geom, "geometry calculations")
    TRACK_COLLECT_REPORT(attr, "summarizing attributes")
    TRACK_ELAPSED("blocks all handled")

    // this will be the table with the new blocks
    vtkSmartPointer<vtkTable> newBlocksTab = vtkSmartPointer<vtkTable>::New();

    // add the geometric information arrays
    newBlocksTab->AddColumn(newCatArr);
    newBlocksTab->AddColumn(newCentArr);
    newBlocksTab->AddColumn(newSizeArr);
    if(nullptr != angArr)
       newBlocksTab->AddColumn(newAngArr);

    // add the general attributes arrays (and decrement the reference counter)
    for(auto it = generalArrs.begin(); it != generalArrs.end(); ++it)
    {
        vtkDoubleArray* arr = it->second;
        newBlocksTab->AddColumn(arr);
        arr->Delete();
    }

    // add the category array attributes arrays (and decrement the reference counter)
    for(auto it = catValArrs.begin(); it != catValArrs.end(); ++it)
    {
        vtkDoubleArray* arr = it->second;
        newBlocksTab->AddColumn(arr);
        arr->Delete();
    }

    // generate block model from table
    // convert into block model
    // note: we pass the block size and angle parameters to the conversion
    // filter, so they can be used if values do not exist in the input table
    vtkSmartPointer<vtkAtgTableToBlocks> generateBlockModel =
            vtkSmartPointer<vtkAtgTableToBlocks>::New();
    SetProgressText("cleaning the unstructured grid");
    generateBlockModel->SetInputData(newBlocksTab);
    if(nullptr == angArr)
        generateBlockModel->SetRotAngle(0.);
    generateBlockModel->Update();

    // extract the output block model
    // get the input grid
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->DeepCopy(generateBlockModel->GetOutput());

    TRACK_ELAPSED("new block model generated")

    return 1;
}

std::string vtkAtgMergeByCategory::generateCatAttName(std::string const& categoryAtt,
                                                      vtkVariant const& categoryVal)
{
    std::string value = categoryVal.ToString();
    std::replace(value.begin(), value.end(), ' ', '_');
    return "C_" + categoryAtt + "_" + value;
}

vtkIdType vtkAtgMergeByCategory::getVerticalNeighbor(vtkUnstructuredGrid* grid,
                                                     vtkIdType cellId, bool upper)
{
    // get all cell point ids
    vtkCell* cell = grid->GetCell(cellId);
    vtkIdList* cids = cell->GetPointIds();

    // make absolutely sure we are having a hexahedron cell
    if((VTK_HEXAHEDRON != cell->GetCellType()) ||
       (8 != cids->GetNumberOfIds()))
        return noNeighborCellId;

    // if we are looking for the lower neighbor, we need points 0..3, and if
    // we are looking for the upper neighbor, we need 4..7
    vtkIdType loLimit = upper ? 4 : 0,
              hiLimit = upper ? 8 : 4;

    auto ptidarr = vtkSmartPointer<vtkIdList>::New();
    for(vtkIdType pid = loLimit; pid < hiLimit; ++pid)
        ptidarr->InsertNextId(cids->GetId(pid));

    // now we get the neighbor cell
    // note that the result will always be either 0 or 1 cell for a proper
    //   unstructured grid
    auto cellIds = vtkSmartPointer<vtkIdList>::New();
    grid->GetCellNeighbors(cellId, ptidarr, cellIds);

    // provide result
    if(0 < cellIds->GetNumberOfIds())
        return cellIds->GetId(0);
    else
        return noNeighborCellId;
}
