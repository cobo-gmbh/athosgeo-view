/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgMergeByCategory.h

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgMergeByCategoryFilter_h
#define vtkAtgMergeByCategoryFilter_h

#include <set>
#include <string>
#include <vtkSetGet.h>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgMergeByCategoryModuleModule.h>

class vtkVariant;
class vtkInformation;

class ATGMERGEBYCATEGORYMODULE_EXPORT vtkAtgMergeByCategory: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgMergeByCategory* New();
    vtkTypeMacro(vtkAtgMergeByCategory, vtkUnstructuredGridAlgorithm)

    vtkSetStringMacro(Category)
    vtkGetStringMacro(Category)

    void AddCategoryPercAtts(char const* cat);
    void ClearCategoryPercAtts();

protected:

    vtkAtgMergeByCategory();
    ~vtkAtgMergeByCategory();

    virtual int RequestData(vtkInformation* request, vtkInformationVector** inVec,
                            vtkInformationVector* outVec) override;

    virtual int FillInputPortInformation(int port, vtkInformation* info);
    virtual int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // generate an attribute name from a category attribute name and a value
    std::string generateCatAttName(std::string const& categoryAtt, vtkVariant const& categoryVal);

    // find upper/lower neighbors (cell id) of a block, not using the Nb attribute, but
    // looking for common points along upper/lower cell face
    vtkIdType getVerticalNeighbor(vtkUnstructuredGrid* grid, vtkIdType cellId, bool upper);

    char* Category;

    typedef std::set<std::string> categorySet;
    categorySet CategoryPercAtts;

};

#endif
