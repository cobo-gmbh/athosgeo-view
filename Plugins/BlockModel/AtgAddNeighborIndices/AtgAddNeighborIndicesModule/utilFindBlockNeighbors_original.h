/*=========================================================================

   Program: AthosGEO
   Module:  utilFindBlockNeighbors.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef utilFindBlockNeighbors_h
#define utilFindBlockNeighbors_h

#include <memory>
#include <vtkSystemIncludes.h>
#include <vtkType.h>
#include <AtgAddNeighborIndicesModuleModule.h>

class vtkIdList;
class vtkPoints;
class vtkUnstructuredGrid;

class utilFindBlockNeighbors_Impl;

class ATGADDNEIGHBORINDICESMODULE_EXPORT utilFindBlockNeighbors
{
public:

    // generate a cell locator object (octree based)
    utilFindBlockNeighbors(vtkUnstructuredGrid* grid);
    ~utilFindBlockNeighbors();

    // for each face, find the neighbor block id and store them in the ids list
    // in the order: xneg, xpos, yneg, ypos, zneg, zpos
    // note: the ids list must exist before the call
    // note: if the block has more than one neighbor at face, a "main" neighbor is selected
    //   according to specific rules (direction dependent)
    // note - special case: if a block has no horizontal neighbor in one direction,
    //   but there is one vertically higher up, then this is considered a "neighbor"
    //   (because it allows to go further upwards to see a "highest" neighbor
    void findBBoxNeighbors(vtkIdType ownId, vtkIdList* ids, bool extendVertically = false);

    // test and return true if two cell bounding boxes are intersecting
    bool intersectingBBoxes(vtkIdType id1, vtkIdType id2, bool extendFirstVertically = false);

    // convert block points coordinates to the system of one cell
    // note: the input points are overwritten with the converted points
    void convertToCellCoords(vtkIdType cellId, vtkPoints* pts);

private:

    // do not copy objects of this class
    utilFindBlockNeighbors(utilFindBlockNeighbors const&);
    void operator=(utilFindBlockNeighbors const&);

    // implementation
    std::unique_ptr<utilFindBlockNeighbors_Impl> impl_;

};

#endif
