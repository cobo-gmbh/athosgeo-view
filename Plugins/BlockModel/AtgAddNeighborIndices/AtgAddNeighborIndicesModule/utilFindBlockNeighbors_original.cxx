/*=========================================================================

   Program: AthosGEO
   Module:  utilFindBlockNeighbors.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vector>
#include <valarray>
#include <vtkSmartPointer.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>
#include <vtkCell.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellIterator.h>
#include <vtkPoints.h>
#include <vtkCellLocator.h>
#include <vtkIdList.h>
#include <utilFindBlockNeighbors.h>

// this is the order of the points in a block cell:
//
// Z
// ^   7 -------- 6
// | / |        / |
// 4 -------- 5   |
// |   |   Y  |   |
// |   | /    |   |
// |   3 ---- | - 2
// | /        | /
// 0 -------- 1  -> X
//
// however, depending on the rotation angle around the Z axis, the min and max X and Y
// are at different indexes:
//
// angle      min x  max x  min y  max y
// 0   - 90     3      1      0      2
// 90  - 180    2      0      3      1
// 180 - 270    1      3      2      0
// 270 - 360    0      2      1      3
//
// however, with GetCellBounds we already have a function for finding the bounding box,
// with the min/max values in the following order (always):
//
// 0 min x
// 1 max x
// 2 min y
// 3 max y
// 4 min z
// 5 max z

// tolerance for all "double" type comparisons: 1 cm
static const double distEps = 0.01;

class utilFindBlockNeighbors_Impl
{
public:

    // constructor
    utilFindBlockNeighbors_Impl(vtkUnstructuredGrid* grid);


    // remember the grid pointer
    vtkUnstructuredGrid* grid_;

    // cell locator structure:
    // will be built in the constructor, for use directly out of the find... function
    vtkSmartPointer<vtkCellLocator> cellLocator_;

    // bounding boxes of all the cells
    std::vector<std::vector<double>> bBoxes_;

    // conversion matrices that allow the conversion of cell coordinates into the
    // coordinate system of a cell
    // note: coordinates of that specific cell itself are all in the range 0..1
    std::vector<vtkSmartPointer<vtkMatrix4x4>> convMat_;

};

utilFindBlockNeighbors::utilFindBlockNeighbors(vtkUnstructuredGrid* grid)
:   impl_(std::unique_ptr<utilFindBlockNeighbors_Impl>(new utilFindBlockNeighbors_Impl(grid)))
{
}

utilFindBlockNeighbors::~utilFindBlockNeighbors()
{
}

void utilFindBlockNeighbors::findBBoxNeighbors(vtkIdType ownId, vtkIdList* ids, bool extendVertically)
{
    // initialize the ids list
    ids->SetNumberOfIds(0);

    // bounding box
    double bbox[6];
    impl_->grid_->GetCellBounds(ownId, bbox);

    // slightly extend the box - in order to avoid near misses, and in zpos
    // direction make it "infinite" because we want so get also "upper
    // neighbors" if no direct neighbors exist
    bbox[0] -= distEps;
    bbox[1] += distEps;
    bbox[2] -= distEps;
    bbox[3] += distEps;
    bbox[4] -= distEps;
    if(extendVertically)
        bbox[5] = 10000.;
    else
        bbox[5] += distEps;

    // use the octree for searching
    impl_->cellLocator_->FindCellsWithinBounds(bbox, ids);
}

bool utilFindBlockNeighbors::intersectingBBoxes(vtkIdType id1, vtkIdType id2, bool extendFirstVertically)
{
    std::vector<double> &bb1 = impl_->bBoxes_[id1],
                        &bb2 = impl_->bBoxes_[id2];

    if(extendFirstVertically)
        bb1[5] = 10000.;

    // check with a little tolerance
    return (((bb1[0] - distEps) < bb2[1]) != ((bb1[1] + distEps) < bb2[0])) &&
           (((bb1[2] - distEps) < bb2[3]) != ((bb1[3] + distEps) < bb2[2])) &&
           (((bb1[4] - distEps) < bb2[5]) != ((bb1[5] + distEps) < bb2[4]));
}

void utilFindBlockNeighbors::convertToCellCoords(vtkIdType cellId, vtkPoints* pts)
{
    // get the precalculated transformation matrix for the base cell
    vtkMatrix4x4* conv = impl_->convMat_[cellId].Get();

    // converted points
    vtkSmartPointer<vtkPoints> ptsConv = vtkSmartPointer<vtkPoints>::New();
    ptsConv->SetDataType(VTK_DOUBLE);

    // convert input points one by one
    for(vtkIdType p = 0; p < pts->GetNumberOfPoints(); ++p)
    {
        double* pt = pts->GetPoint(p);
        std::valarray<double> pt4({pt[0], pt[1], pt[2], 1.});
        double* resPt = conv->MultiplyDoublePoint(&pt4[0]);
        ptsConv->InsertNextPoint(resPt[0], resPt[1], resPt[2]);
        // note: vtkMatrix4x4 contains a result buffer for one point, so we do
        // not need to care about the resPt pointer here!
    }

    // copy new points to in/output points
    pts->DeepCopy(ptsConv);
}

utilFindBlockNeighbors_Impl::utilFindBlockNeighbors_Impl(vtkUnstructuredGrid* grid)
:   grid_(grid),
    cellLocator_(vtkSmartPointer<vtkCellLocator>::New())
{
    // prepare the cell locator octree
    // note MaxLevel: 8 is already very large and will only be reached with block numbers of 1 mio or more...
    // note CellsPerBucket:
    //  - with 7, up to about 1 mio blocks timing was growing about linear, but above not any more
    //  - with 30 or 20, linear growth extended up to a model with 1.7 mio blocks, taking about 50 min,
    //    but longer for the preparation of the octree: 9 or 7 instead of 4 secs, thus peanuts compared to
    //    the search loop.
    //    However, at the end the program crashed: memory overflow?? The number of buckets was 1.9 mio,
    //    thus exceeding the number of blocks!
    //    -> back to 7, even though it is much slower for models with more than 1 mio blocks!
    cellLocator_->Initialize();
    cellLocator_->SetLazyEvaluation(false);
    cellLocator_->SetRetainCellLists(true);
    cellLocator_->SetAutomatic(false); // do not automatically calculate locator depth / number of cells per bucket
    cellLocator_->SetDataSet(grid);
    cellLocator_->SetMaxLevel(10); // 8 is already very large...
    //cellLocator_->SetNumberOfCellsPerBucket(20);
    cellLocator_->SetNumberOfCellsPerBucket(7);
    cellLocator_->SetTolerance(distEps);
    cellLocator_->BuildLocator();

    // generate the bounding boxes and conversion matrices for each cell
    for(vtkIdType c = 0; c < grid->GetNumberOfCells(); ++c)
    {
        // get and save bounding box
        std::vector<double> bVec(6, 0.);
        grid->GetCellBounds(c, bVec.data());
        bBoxes_.push_back(bVec);

        // generate translation matrix
        vtkPoints* pts = grid->GetCell(c)->GetPoints();
        vtkSmartPointer<vtkTransform> trans = vtkSmartPointer<vtkTransform>::New();
        std::valarray<double> transVec({-pts->GetPoint(0)[0], -pts->GetPoint(0)[1], -pts->GetPoint(0)[2]});
        trans->Translate(&transVec[0]);
        vtkSmartPointer<vtkMatrix4x4> transMat = vtkSmartPointer<vtkMatrix4x4>::New();
        transMat->DeepCopy(trans->GetMatrix());

        // rotation matrix with scaling
        // note: the target coordinate system maps the cell into a box between (0,0,0) and (1,1,1)
        vtkSmartPointer<vtkMatrix4x4> rotMat = vtkSmartPointer<vtkMatrix4x4>::New();
        rotMat->Identity();
        std::valarray<double> xVec = std::valarray<double>({pts->GetPoint(1)[0], pts->GetPoint(1)[1], pts->GetPoint(1)[2]}) + transVec,
                              yVec = std::valarray<double>({pts->GetPoint(3)[0], pts->GetPoint(3)[1], pts->GetPoint(3)[2]}) + transVec,
                              zVec = std::valarray<double>({pts->GetPoint(4)[0], pts->GetPoint(4)[1], pts->GetPoint(4)[2]}) + transVec;
        xVec /= (xVec * xVec).sum();
        yVec /= (yVec * yVec).sum();
        zVec /= (zVec * zVec).sum();

        //rotMat.setRow(0, xVec);
        rotMat->SetElement(0, 0, xVec[0]);
        rotMat->SetElement(0, 1, xVec[1]);
        rotMat->SetElement(0, 2, xVec[2]);
        //rotMat.setRow(1, yVec);
        rotMat->SetElement(1, 0, yVec[0]);
        rotMat->SetElement(1, 1, yVec[1]);
        rotMat->SetElement(1, 2, yVec[2]);
        //rotMat.setRow(2, zVec);
        rotMat->SetElement(2, 0, zVec[0]);
        rotMat->SetElement(2, 1, zVec[1]);
        rotMat->SetElement(2, 2, zVec[2]);

        // apply first translation and then rotation
        // and store the result in the vector
        vtkMatrix4x4::Multiply4x4(rotMat, transMat, transMat);
        convMat_.push_back(transMat);
    }
}
