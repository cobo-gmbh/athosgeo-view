/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgAddNeighborIndices.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLongLongArray.h>
#include <vtkStringArray.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIdList.h>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <vtkAtgAddNeighborIndices.h>

vtkStandardNewMacro(vtkAtgAddNeighborIndices)

// tolerance for "neighborhood" and angle values
// unit is meters for distances, degrees for angles
// note: for lengths, we will work with a relative eps where this one that
//   is derived from the block size (x and y dimensions) and multiplied with this factor
static const double eps = 0.01,
                    angEps = 0.01;

static const double deg_to_rad = 3.14159264 / 180.;

// key indicating an angle with rounding and a block rectangle
struct SubmodelKey
{
    SubmodelKey(double ang, double dx, double dy, double lenEps)
    :   angle(ang),
        sizeX(dx),
        sizeY(dy),
        roundAngle((long)::round(ang / angEps) % (long)::round(360. / angEps)),
        roundSizeX((long long)::round(dx / lenEps)),
        roundSizeY((long long)::round(dy / lenEps))
    {}

    SubmodelKey()
    :   angle(0.),
        sizeX(0.),
        sizeY(0.),
        roundAngle(0),
        roundSizeX(0),
        roundSizeY(0)
    {}

    bool operator<(SubmodelKey const& smKey2) const
    {
        if(smKey2.roundAngle != roundAngle)
            return smKey2.roundAngle < roundAngle;
        else if(smKey2.roundSizeX != roundSizeX)
            return smKey2.roundSizeX != roundSizeX;
        else
            return smKey2.roundSizeY != roundSizeY;
    }

    double angle,
           sizeX,
           sizeY;
    long roundAngle;
    long long roundSizeX,
              roundSizeY;
};

// key indicating x/y position with rounding
struct XYPos
{
    XYPos(double posX, double posY, double lenEps)
    :   position(std::initializer_list<double>({posX, posY})),
        roundPos(std::initializer_list<long>({
            (long)::round(posX / lenEps),
            (long)::round(posY / lenEps)}))
    {}

    XYPos()
    :   position(std::initializer_list<double>({0., 0.})),
        roundPos(std::initializer_list<long>({0, 0}))
    {}

    bool operator<(XYPos const& pos2) const
    {
        if(pos2.roundPos[1] != roundPos[1])
            return pos2.roundPos[1] < roundPos[1];
        else
            return pos2.roundPos[0] < roundPos[0];
    }

    std::vector<double> position;
    std::vector<long> roundPos;
};

// storage for one vertical block column
struct VertColumn
{
    VertColumn(long newColId)
    :   colId(newColId),
        sz(2, 0.),
        nb(4, utilNormNames::INDEX_NO_BLOCK)
    {}

    VertColumn()
    :   colId(0),
        sz(2, 0.),
        nb(4, utilNormNames::INDEX_NO_BLOCK)
    {}

    long colId;
    std::vector<double> sz;
    std::vector<long> nb;
    std::vector<vtkIdType> blocks;
};

// prepare for the r-tree
namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;
typedef bg::model::point<double, 2, bg::cs::cartesian> point;
typedef bg::model::box<point> rectangle;
typedef std::pair<rectangle, long> node;

vtkAtgAddNeighborIndices::vtkAtgAddNeighborIndices()
{
}

vtkAtgAddNeighborIndices::~vtkAtgAddNeighborIndices()
{
}

int vtkAtgAddNeighborIndices::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAddNeighborIndices::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model with added neighborhood columns
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAddNeighborIndices::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    TRACK_FUNCTION_TIME;

    // get the input table
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
    vtkUnstructuredGrid* input =
            vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("No input data\n");
        return 1;
    }

    // required attribute names, and check for mandatory columns
    std::string angName = utilNormNames::getName(utilNormNames::ANGLE),
                posName = utilNormNames::getName(utilNormNames::BLOCKCENTER),
                sizeName = utilNormNames::getName(utilNormNames::BLOCKSIZE);
    if(nullptr == input->GetCellData()->GetArray(posName.c_str()))
    {
        std::string err = "AtgAddNeighborIndices: Mandatory input attribute missing: <" + posName + ">\n";
        vtkOutputWindowDisplayWarningText(err.c_str());
        return 1;
    }
    if(nullptr == input->GetCellData()->GetArray(sizeName.c_str()))
    {
        std::string err = "AtgAddNeighborIndices: Mandatory input attribute missing: <" + sizeName + ">\n";
        vtkOutputWindowDisplayWarningText(err.c_str());
        return 1;
    }

    // get the output grid
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* output =
            vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // shallow copy the input
    output->ShallowCopy(input);
    vtkDataArray *angArr = output->GetCellData()->GetArray(angName.c_str()),
                 *posArr = output->GetCellData()->GetArray(posName.c_str()),
                 *sizeArr = output->GetCellData()->GetArray(sizeName.c_str());
    vtkIdType numCells = output->GetNumberOfCells();

    TRACK_ELAPSED("shallow copy");

    // find or otherwise re-generate a blockId column
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID);
    vtkDataArray* bidArr = output->GetCellData()->GetArray(bidName.c_str());
    if(nullptr == bidArr)
    {
        vtkSmartPointer<vtkLongLongArray> bidNewArr = vtkSmartPointer<vtkLongLongArray>::New();
        bidNewArr->SetName(bidName.c_str());
        bidNewArr->SetNumberOfTuples(output->GetNumberOfCells());
        output->GetCellData()->AddArray(bidNewArr);
    }
    bidArr = output->GetCellData()->GetArray(bidName.c_str());

    // at this point we ensure that the block position in the arrays is identical
    // with the BlockId attribute
    for(vtkIdType bid = 0; bid < bidArr->GetNumberOfTuples(); ++bid)
        bidArr->SetVariantValue(bid, bid);

    TRACK_ELAPSED("new block ids generated");

    // add new column with 6 components for the neighbors in 6 main directions
    vtkSmartPointer<vtkLongLongArray> nbArr = vtkSmartPointer<vtkLongLongArray>::New();
    std::string nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
    nbArr->SetName(nbName.c_str());
    nbArr->SetNumberOfComponents(6);
    std::vector<std::string> compNames = utilNormNames::getComponents(nbName);
    vtkIdType cid = 0;
    for(auto it = compNames.begin(); it != compNames.end(); ++it)
        nbArr->SetComponentName(cid++, it->c_str());
    nbArr->SetNumberOfTuples(output->GetNumberOfCells());
    nbArr->Fill(utilNormNames::INDEX_NO_BLOCK);
    output->GetCellData()->AddArray(nbArr);

    TRACK_ELAPSED("new nb attribute generated");

    // derive the length tolerance value from the average x and y block size
    // and z block size, respectively
    double horLenEps = 0.,
           vertLenEps = 0.;
    for(vtkIdType bid = 0; bid < numCells; ++bid)
    {
        horLenEps += sizeArr->GetComponent(bid, 0)
                     + sizeArr->GetComponent(bid, 1);
        vertLenEps += sizeArr->GetComponent(bid, 2);
    }
    horLenEps *= eps / (2 * numCells);
    vertLenEps = eps / numCells;

    // first of all, we want to check whether all blocks within a "block
    // column" (defined by common x and y centroid coordinates) are having
    // the same size in x and y and the same rotation angle
    std::map<XYPos, SubmodelKey> columns;
    for(vtkIdType bid = 0; bid < numCells; ++bid)
    {
        // find the block rotation angle and generate a "submodel key"
        // from angle and size in x and y direction
        double angle = 0.;
        if(nullptr != angArr)
            angle = angArr->GetVariantValue(bid).ToDouble();
        SubmodelKey smKey(angle,
                          sizeArr->GetComponent(bid, 0),
                          sizeArr->GetComponent(bid, 1),
                          horLenEps);

        // centroid position, not rotated
        double x = posArr->GetComponent(bid, 0),
               y = posArr->GetComponent(bid, 1);
        XYPos pos(x, y, horLenEps);

        // if we do not "know" the column (XYPos) yet, we add it
        auto it = columns.find(pos);
        if(columns.end() == it)
        {
            columns[pos] = smKey;
        }

        // if we know the column, we have to compare size and angle
        else
        {
            if((columns[pos] < smKey) || (smKey < columns[pos]))
            {
                std::string err = "Not all blocks are of same size and rotation angle"
                                  " at the following position:\n" +
                                  std::to_string(pos.position[0]) + " / " +
                                  std::to_string(pos.position[1]);
                vtkOutputWindowDisplayWarningText(err.c_str());
                return 1;
            }
        }
    }

    TRACK_ELAPSED("checked that all blocks at one x/y position have same x/y size and rotation angle");

    // build pointer structure for model handling, including back-rotated
    // positions if there is a rotation angle for all/some blocks
    std::map<SubmodelKey, std::map<XYPos, VertColumn>> submodels;
    long newColId = 0;
    for(vtkIdType bid = 0; bid < numCells; ++bid)
    {
        // find the block rotation angle and generate a "submodel key"
        // from angle and size in x and y direction
        double angle = 0.;
        if(nullptr != angArr)
            angle = angArr->GetVariantValue(bid).ToDouble();
        SubmodelKey smKey(angle,
                          sizeArr->GetComponent(bid, 0),
                          sizeArr->GetComponent(bid, 1),
                          horLenEps);

        // find or generate submodel for current angle
        auto smit = submodels.find(smKey);
        if(submodels.end() == smit)
            submodels[smKey] = std::map<XYPos, VertColumn>();
        std::map<XYPos, VertColumn>& sm = submodels[smKey];

        // current position, rotated
        double x = posArr->GetComponent(bid, 0),
               y = posArr->GetComponent(bid, 1),
               sang = ::sin(smKey.angle * deg_to_rad),
               cang = ::cos(smKey.angle * deg_to_rad),
               rotX = cang * x + sang * y,
               rotY = cang * y - sang * x;
        XYPos pos(rotX, rotY, horLenEps);

        // find or generate vertical block column for current position
        auto cit = sm.find(pos);
        if(sm.end() == cit)
            sm[pos] = VertColumn(newColId++);
        VertColumn& col = sm[pos];

        // assign the current block id
        // note: above we ensured that bid and BlockId are identical
        col.blocks.push_back(bid);
    }

    TRACK_ELAPSED("blocks inserted into structure");

    // for each submodel there will be 5 steps
    vtkIdType numberOfProgressSteps = 5 * submodels.size();
    double progressStep = 100. / numberOfProgressSteps,
           currentProgress = 0.;
    UpdateProgress(currentProgress);

    // handle one submodel after the other
    for(auto subit = submodels.begin(); subit != submodels.end(); ++subit)
    {
        std::map<XYPos, VertColumn>& mdl = subit->second;
        std::map<long, VertColumn*> vcolIndex;
        std::map<long, XYPos const*> xyPosIndex;
        for(auto it = mdl.begin(); it != mdl.end(); ++it)
        {
            vcolIndex[it->second.colId] = &it->second;
            xyPosIndex[it->second.colId] = &it->first;
        }

        // get column size from the first block in the related block column
        // note that with the above check, it can be assumed that all blocks in the
        // block column are of the same size, so we do not need to check this again
        for(auto colit = mdl.begin(); colit != mdl.end(); ++colit)
        {
            // get the size from the first block in the column
            std::vector<vtkIdType>& blocks = colit->second.blocks;
            vtkIdType firstBlock = *blocks.begin();
            colit->second.sz[0] = sizeArr->GetComponent(firstBlock, 0);
            colit->second.sz[1] = sizeArr->GetComponent(firstBlock, 1);
        }

        TRACK_ELAPSED("added size of first block to size attribute of the block column");

        // sort column blocks vertically
        for(auto colit = mdl.begin(); colit != mdl.end(); ++colit)
        {
            // get the size from the first block in the column
            std::vector<vtkIdType>& blocks = colit->second.blocks;
            std::sort(blocks.begin(), blocks.end(),
                      [posArr](vtkIdType id1, vtkIdType id2)
            {
                return posArr->GetComponent(id1, 2) < posArr->GetComponent(id2, 2);
            });
        }
        currentProgress += progressStep;
        UpdateProgress(currentProgress); // step 2

        TRACK_ELAPSED("sorted all blocks vertically")

        // check for vertical overlaps and generate Nb values in Z
        for(auto colit = mdl.begin(); colit != mdl.end(); ++colit)
        {
            // get the size from the first block in the column
            std::vector<vtkIdType>& blocks = colit->second.blocks;

            for(auto it1 = blocks.begin(); it1 != blocks.end(); ++it1)
            {
                // get next iterator, and terminate loop if that is end()
                auto it2 = it1;
                it2++;
                if(it2 == blocks.end())
                    break;

                // top of lower and bottom of upper block
                double top1 = posArr->GetComponent(*it1, 2) + .5 * sizeArr->GetComponent(*it1, 2),
                       bot2 = posArr->GetComponent(*it2, 2) - .5 * sizeArr->GetComponent(*it2, 2);

                // check for overlap
                if(vertLenEps < (top1 - bot2))
                {
                    std::string err = "Vertically overlapping blocks found\n"
                                      "Example: BlockIds " + std::to_string(*it1) +
                                      " and " + std::to_string(*it2);
                    vtkOutputWindowDisplayWarningText(err.c_str());
                    return 1;
                }

                // set vertical neighbor attributes if blocks are adjacent
                if(vertLenEps > ::fabs(top1 - bot2))
                {
                    nbArr->SetComponent(*it1, utilNormNames::DIR_ZPOS, *it2);
                    nbArr->SetComponent(*it2, utilNormNames::DIR_ZNEG, *it1);
                }
            }
        }
        currentProgress += progressStep;
        UpdateProgress(currentProgress); // step 3

        TRACK_ELAPSED("vertical overlap check done and vertical Nb values generated")

        // build r-tree of rectangles
        bgi::rtree<node, bgi::quadratic<16>> rtree;
        for(auto colit = mdl.begin(); colit != mdl.end(); ++colit)
        {
            // get parameters of rectangle
            XYPos const& pos = colit->first;
            std::vector<double> const& sz = colit->second.sz;
            long rectId = colit->second.colId;

            // create a rectangle from two opposite points
            rectangle r(point(pos.position[0] - .5 * sz[0], pos.position[1] - .5 * sz[1]),
                        point(pos.position[0] + .5 * sz[0], pos.position[1] + .5 * sz[1]));

            // insert new value
            rtree.insert(std::make_pair(r, rectId));
        }
        currentProgress += progressStep;
        UpdateProgress(currentProgress); // step 4

        TRACK_ELAPSED("r-tree for submodel finished");

        // check for overlapping rectangles and find neighbor rectangles
        for(auto it = rtree.begin(); it != rtree.end(); ++it)
        {
            std::vector<node> result;
            long colId = it->second;

            // if we reduce a rectangle by eps on all sides, it should only
            // intersect with itself finally
            point qpoint1(it->first.min_corner().get<0>() + horLenEps,
                          it->first.min_corner().get<1>() + horLenEps),
                  qpoint2(it->first.max_corner().get<0>() - horLenEps,
                          it->first.max_corner().get<1>() - horLenEps);
            rectangle qrect(qpoint1, qpoint2);
            rtree.query(bgi::intersects(qrect), std::back_inserter(result));
            if(result.empty() ||
               (result.begin()->second != colId) ||
               (1 < result.size()))
            {
                vtkIdType blkId = *vcolIndex[colId]->blocks.begin();
                std::string err = "Horizontally overlapping blocks found\n"
                                  "Example: BlockId " + std::to_string(blkId) +
                                  " at x/y " + std::to_string(posArr->GetComponent(blkId, 0)) +
                                  " " + std::to_string(posArr->GetComponent(blkId, 1));
                vtkOutputWindowDisplayWarningText(err.c_str());
                return 1;
            }

            // find neighbor columns by expanding the query rectangle by 2*eps
            // for all four horizontal directions

            // direction: X-
            qpoint1.set<0>(qpoint1.get<0>() - 2. * horLenEps);
            qrect = rectangle(qpoint1, qpoint2);
            result.clear();
            rtree.query(bgi::intersects(qrect), std::back_inserter(result));
            if(1 < result.size())
            {
                long nbId = result[0].second;
                if(colId == nbId)
                    nbId = result[1].second;
                vcolIndex[colId]->nb[0] = nbId;
            }

            // direction: X+
            qpoint1.set<0>(qpoint1.get<0>() + 2. * horLenEps);
            qpoint2.set<0>(qpoint2.get<0>() + 2. * horLenEps);
            qrect = rectangle(qpoint1, qpoint2);
            result.clear();
            rtree.query(bgi::intersects(qrect), std::back_inserter(result));
            if(1 < result.size())
            {
                long nbId = result[0].second;
                if(colId == nbId)
                    nbId = result[1].second;
                vcolIndex[colId]->nb[1] = nbId;
            }

            // direction: Y-
            qpoint2.set<0>(qpoint2.get<0>() - 2. * horLenEps);
            qpoint1.set<1>(qpoint1.get<1>() - 2. * horLenEps);
            qrect = rectangle(qpoint1, qpoint2);
            result.clear();
            rtree.query(bgi::intersects(qrect), std::back_inserter(result));
            if(1 < result.size())
            {
                long nbId = result[0].second;
                if(colId == nbId)
                    nbId = result[1].second;
                vcolIndex[colId]->nb[2] = nbId;
            }

            // direction: Y+
            qpoint1.set<1>(qpoint1.get<1>() + 2. * horLenEps);
            qpoint2.set<1>(qpoint2.get<1>() + 2. * horLenEps);
            qrect = rectangle(qpoint1, qpoint2);
            result.clear();
            rtree.query(bgi::intersects(qrect), std::back_inserter(result));
            if(1 < result.size())
            {
                long nbId = result[0].second;
                if(colId == nbId)
                    nbId = result[1].second;
                vcolIndex[colId]->nb[3] = nbId;
            }
        }
        currentProgress += progressStep;
        UpdateProgress(currentProgress); // step 5

        TRACK_ELAPSED("rectangle overlap check done and neighbor rectangles found")

        // generate Nb values in X and Y directions and check whether sizes do match

        // do this for all block columns
        for(auto cit = rtree.begin(); cit != rtree.end(); ++cit)
        {
            // get data of current block column
            long colId = cit->second;
            auto key = cit->first;
            VertColumn& col = *vcolIndex[colId];
            long firstBlock = *col.blocks.begin();
            double centCoordX = .5 * (key.min_corner().get<0>() + key.max_corner().get<0>()),
                   centCoordY = .5 * (key.min_corner().get<1>() + key.max_corner().get<1>()),
                   blockSizeX = sizeArr->GetComponent(firstBlock, 0),
                   blockSizeY = sizeArr->GetComponent(firstBlock, 1);

            // iterate all 4 possible horizontal neighbor directions
            for(int dir = 0; dir < 4; ++dir)
            {
                // if there is no neighbor column in a direction: nothing to do
                long nbColId = col.nb[dir];
                if(0 > nbColId)
                    continue;

                // get neighbor block column data and check position and size
                // note: we want that neighboring block columns are aligned and have same size,
                //   because the mining strategy in the Scheduler module could not handle
                //   different constellations
                VertColumn& nbCol = *vcolIndex[nbColId];
                auto nbXYPos = xyPosIndex[nbColId];
                long nbFirstBlock = *nbCol.blocks.begin();
                double diffPos = 0.,
                       diffSize = 0.;
                switch(dir)
                {
                    case utilNormNames::DIR_XNEG:
                    case utilNormNames::DIR_XPOS:
                    {
                        // centroid positins must match in y, sizes in x
                        double nbCentCoord = nbXYPos->position[1],
                               nbBlockSize = sizeArr->GetComponent(nbFirstBlock, 0);
                        diffPos = ::fabs(nbCentCoord - centCoordY);
                        diffSize = ::fabs(nbBlockSize - blockSizeX);
                        break;
                    }
                    case utilNormNames::DIR_YNEG:
                    case utilNormNames::DIR_YPOS:
                    default:
                    {
                        // centroid positins must match in x, sizes in y
                        double nbCentCoord = nbXYPos->position[0],
                               nbBlockSize = sizeArr->GetComponent(nbFirstBlock, 1);
                        diffPos = ::fabs(nbCentCoord - centCoordX);
                        diffSize = ::fabs(nbBlockSize - blockSizeY);
                    }
                }
                if((horLenEps < diffPos) || (horLenEps < diffSize))
                {
                    std::string err = "Horizontally neighboring block columns are not aligned"
                                      " or do not have the same x/y sizes\n"
                                      "Example: BlockIds " + std::to_string(firstBlock) +
                                      " and " + std::to_string(nbFirstBlock);
                    vtkOutputWindowDisplayWarningText(err.c_str());
                    return 1;
                }

                // block by block: for each block in the central block column, find the
                // lowest neighbor block that is not fully below, with the vertical
                // distance being defined as:
                //   <vertical distance> = <top neighbor block> - <bottom central block>
                for(auto bit = col.blocks.begin(); bit != col.blocks.end(); ++bit)
                {
                    double centBottom = posArr->GetComponent(*bit, 2)
                                        - .5 * sizeArr->GetComponent(*bit, 2);
                    for(auto nbit = nbCol.blocks.begin(); nbit != nbCol.blocks.end(); ++nbit)
                    {
                        double blockTop = posArr->GetComponent(*nbit, 2)
                                          + .5 * sizeArr->GetComponent(*nbit, 2);
                        if(vertLenEps < (blockTop - centBottom))
                        {
                            nbArr->SetComponent(*bit, dir, *nbit);
                            break;
                        }
                    }
                }
            }
        }
        currentProgress += progressStep;
        UpdateProgress(currentProgress); // step 6

        TRACK_ELAPSED("Nb values in X and Y generated")
    }

    return 1;
}

int vtkAtgAddNeighborIndices::RequestInformation(vtkInformation* request,
                                                    vtkInformationVector** inputVector,
                                                    vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgAddNeighborIndices::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
