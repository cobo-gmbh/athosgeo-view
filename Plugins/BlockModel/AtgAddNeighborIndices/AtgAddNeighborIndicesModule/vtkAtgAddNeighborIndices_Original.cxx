/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgAddNeighborIndices.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cmath>
#include <vector>
#include <valarray>
#include <map>
#include <string>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLongLongArray.h>
#include <vtkStringArray.h>
#include <vtkCell.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkOBBTree.h>
#include <vtkCellLocator.h>
#include <vtkIdList.h>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <utilFindBlockNeighbors.h>
#include <vtkAtgAddNeighborIndices.h>

vtkStandardNewMacro(vtkAtgAddNeighborIndices)

// tolerance for "neighborhood"
// note: unit "fraction of block"
static const double eps = 0.01;

vtkAtgAddNeighborIndices::vtkAtgAddNeighborIndices()
{
}

vtkAtgAddNeighborIndices::~vtkAtgAddNeighborIndices()
{
}

int vtkAtgAddNeighborIndices::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAddNeighborIndices::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model with added neighborhood columns
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAddNeighborIndices::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    //77//
    TRACK_FUNCTION_TIME;

    // get the input table
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
    vtkUnstructuredGrid* input =
            vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
    if(0 == input)
    {
        vtkOutputWindowDisplayWarningText("No input data\n");
        return 1;
    }

    // get the output grid
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* output =
            vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // shallow copy the input
    output->ShallowCopy(input);

    //77//
    TRACK_ELAPSED("shallow copy");

    // find or otherwise generate a blockId column
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID);
    vtkDataArray* bidArr = output->GetCellData()->GetArray(bidName.c_str());
    if(nullptr == bidArr)
    {
        vtkSmartPointer<vtkLongLongArray> bidNewArr = vtkSmartPointer<vtkLongLongArray>::New();
        bidNewArr->SetName(bidName.c_str());
        bidNewArr->SetNumberOfTuples(output->GetNumberOfCells());
        output->GetCellData()->AddArray(bidNewArr);
    }
    bidArr = output->GetCellData()->GetArray(bidName.c_str());
    for(vtkIdType bid = 0; bid < bidArr->GetNumberOfTuples(); ++bid)
        bidArr->SetVariantValue(bid, bid);

    //77//
    TRACK_ELAPSED("new block ids generated");

    // add new column with 6 components for the neighbors in 6 main directions
    vtkSmartPointer<vtkLongLongArray> nbArr = vtkSmartPointer<vtkLongLongArray>::New();
    std::string nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
    nbArr->SetName(nbName.c_str());
    nbArr->SetNumberOfComponents(6);
    std::vector<std::string> compNames = utilNormNames::getComponents(nbName);
    vtkIdType cid = 0;
    for(auto it = compNames.begin(); it != compNames.end(); ++it)
        nbArr->SetComponentName(cid++, it->c_str());
    nbArr->SetNumberOfTuples(output->GetNumberOfCells());
    output->GetCellData()->AddArray(nbArr);

    //77//
    TRACK_ELAPSED("new nb attribute generatedd");

    // generate octree and conversion matrix list
    utilFindBlockNeighbors* igrid = new utilFindBlockNeighbors(output);

    //77//
    TRACK_ELAPSED("octree find tool initialized");

    vtkIdType numCells = output->GetNumberOfCells();
    vtkIdType progressStep = numCells / 100;
    if(progressStep == 0)
        progressStep = 1;

    vtkSmartPointer<vtkIdList> nbIds = vtkSmartPointer<vtkIdList>::New();
    for(vtkIdType cid = 0; cid < numCells; ++cid)
    {
        if(cid % progressStep == 0)
          UpdateProgress((double)cid / numCells);

        // find the neighbor candidates through the bbox based octree search
        // note: use vertically extended bbox
        igrid->findBBoxNeighbors(cid, nbIds, true);

        // collect the neighbors by face (xneg, xpos, yneg, ypos, zneg, zpos)
        // and for each face a multimap that maps a pair of neg. vertical distance and common surface to id
        std::vector<std::multimap<std::pair<double, double>, vtkIdType>>
                neighborMaps(6, std::multimap<std::pair<double, double>, vtkIdType>());
        for(vtkIdType n = 0; n < nbIds->GetNumberOfIds(); ++n)
        {
            vtkIdType id = nbIds->GetId(n);

            // get rid of self-intersections and furthermore ensure that
            // at least the bounding boxes really intersect
            // note: we want to vertically extend the bbox of cid
            if((cid == id) || !igrid->intersectingBBoxes(cid, id, true))
                continue;

            // express all neighbor candidate coordinates in a system where
            // the base cell has all coordinates within [0..1]
            // note: with this we can easily check whether a candidate is
            // really a neighbor at any face of the cell
            vtkPoints* pts = output->GetCell(id)->GetPoints();
            igrid->convertToCellCoords(cid, pts);

            // try at which face the neighbor cell could be a neighbor
            // - if it is a neighbor at all
            //
            // note: testing is done with this indexing scheme for cells
            // (which is the standard in VTK)
            // Z
            // ^   7 -------- 6
            // | / |        / |
            // 4 -------- 5   |
            // |   |   Y  |   |
            // |   | /    |   |
            // |   3 ---- | - 2
            // | /        | /
            // 0 -------- 1  -> X

            // face coordinates for checking
            // note: for x/y neg/pos, the first two point indices are defining the
            //   bottom segment of the block: this allows to further down check x1
            //   to find out if a neighbor is only at the corner (x1 == 0)
            static const int fptid[6][4] =
            {
                {1, 2, 6, 5}, // Xneg cell == Xpos neighbor (etc.)
                {3, 0, 4, 7}, // Xpos
                {2, 3, 7, 6}, // Yneg
                {0, 1, 5, 4}, // Ypos
                {4, 5, 6, 7}, // Zneg
                {0, 3, 2, 1}  // Zpos
            };

            // rotate coordinates so that always "x" is the coordinate of interest
            static const int cmap[3][3] =
            {
                {0, 1, 2},
                {1, 2, 0},
                {2, 0, 1}
            };
            for(int face = 0; face < 6; ++face)
            {
                int dim = face >> 1,
                    dir = face & 1;
                std::vector<std::valarray<double>> square;
                for(int p = 0; p < 4; ++p)
                {
                    double cc[3];
                    pts->GetPoint(fptid[face][p], cc);
                    square.push_back(std::valarray<double>({cc[cmap[dim][0]] - dir,
                                                            cc[cmap[dim][1]],
                                                            cc[cmap[dim][2]]}));
                }

                // if now all x coordinates are zero, this is a neighbor along this face
                bool isNeighbor = true;
                for(auto pt = square.begin(); pt != square.end(); ++pt)
                    isNeighbor = isNeighbor && (::fabs((*pt)[0]) < eps);
                if(!isNeighbor)
                    continue;

                // make sure y and z coordinates are in the range [0..1]
                for(auto it = square.begin(); it != square.end(); ++it)
                {
                    std::valarray<double>& pt = *it;
                    pt[1] = std::max<double>(std::min<double>(pt[1], 1.), 0.);
                    pt[2] = std::max<double>(std::min<double>(pt[2], 1.), 0.);
                }

                // get the two coordinates of the common surface, and check if both
                // are zero (other than only one)
                // note: with x1 segment zero, the block is certainly not a
                //   neighbor candidate because it touches only at the corner
                //   (see comment for fptid above)
                double x1 = ::sqrt(std::pow(square[1] - square[0], 2.).sum()),
                       x2 = ::sqrt(std::pow(square[3] - square[0], 2.).sum());
                if(x1 < eps)
                    continue;

                // calculate common surface fraction
                double fraction = x1 * x2;

                // with a fraction of 0 we still want to check if we have an "above neighbor",
                // so we calculate the vertical distance (bottom main block to top neighbor,
                // in main block height units)
                // note: this is only of interest for the vertical faces (x/y neg/pos)
                double vertDist = 0.;
                if(face < utilNormNames::DIR_ZNEG)
                {
                    double cc[3];
                    pts->GetPoint(4, cc);
                    vertDist = cc[2];
                }

                // skip the face if either the fraction is (almost) zero or the
                // vertical distance is zero or negative
                if((fraction < eps) && (vertDist < eps))
                    continue;

                // this sorts the fraction/vertical distance data in a way that the "most important"
                // comes always last, which is:
                // - the neighbor with the smallest vertical distance, or else
                // - the neighbor with the largest surface fraction
                // note: for the neighbors in horizontal direction we want the "lowest" neighbor
                //   that is not entirely below the block because later on we want to safely find
                //   the "highest" neighbor, but this can disappear through mining progress
                std::multimap<std::pair<double, double>, vtkIdType>& fnb = neighborMaps[face];
                fnb.insert(std::pair<std::pair<double, double>, vtkIdType>(std::pair<double, double>(-vertDist, fraction), id));
            }
        }

        // iterate through neighbor maps (corresponding to faces)
        for(int face = 0; face < 6; ++face)
        {
            // no neighbor
            vtkIdType nbId = utilNormNames::INDEX_NO_BLOCK;

            // get the map
            std::multimap<std::pair<double, double>, vtkIdType>& fnb = neighborMaps[face];

            // the last entry corresponds to the "most important" neighbor: see above
            if(!fnb.empty())
                nbId = bidArr->GetVariantValue(fnb.rbegin()->second).ToUnsignedLongLong();

            // insert the neighbor id
            nbArr->SetComponent(cid, face, nbId);
        }
    }

    //77//
    TRACK_ELAPSED("nb attribute done");

    // clean up
    delete igrid;

    return 1;
}

int vtkAtgAddNeighborIndices::RequestInformation(vtkInformation* request,
                                                    vtkInformationVector** inputVector,
                                                    vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgAddNeighborIndices::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
