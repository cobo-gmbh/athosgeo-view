/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithSurface.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgClipWithSurface_h
#define vtkAtgClipWithSurface_h

#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgClipWithSurfaceModuleModule.h>

class vtkPoints;
class vtkPolyData;

class ATGCLIPWITHSURFACEMODULE_EXPORT vtkAtgClipWithSurface: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgClipWithSurface* New();
    vtkTypeMacro(vtkAtgClipWithSurface, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetMacro(KeepBlocksBelow, bool)
    vtkGetMacro(KeepBlocksBelow, bool)

    vtkSetMacro(MinVolFactorRetained, double)
    vtkGetMacro(MinVolFactorRetained, double)

    vtkSetMacro(RespectPrevVert, bool)
    vtkGetMacro(RespectPrevVert, bool)

protected:

    vtkAtgClipWithSurface();
    ~vtkAtgClipWithSurface();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgClipWithSurface(vtkAtgClipWithSurface const&);
    void operator=(vtkAtgClipWithSurface const&);

    // progress indicator, discrete percent
    void ShowProgress(int step, int totalSteps);

    // if this is true, the clipping will be from above, otherwise from below
    bool KeepBlocksBelow;

    // minimum volume factor to be retained
    double MinVolFactorRetained;

    // if the model was already cut vertically, this should be checked to avoid cutting
    // the same volume twice
    bool RespectPrevVert;

};

#endif
