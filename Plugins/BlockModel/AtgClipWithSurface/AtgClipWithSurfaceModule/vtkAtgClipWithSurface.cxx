/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithSurface.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iomanip>
#include <string>
#include <memory>
#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellLocator.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <utilRenumberBlocks.h>
#include <utilUpdateNeighborsForRemainingBlocks.h>
#include <vtkAtgClipWithSurface.h>

vtkStandardNewMacro(vtkAtgClipWithSurface)

vtkAtgClipWithSurface::vtkAtgClipWithSurface()
:   KeepBlocksBelow(true),
    MinVolFactorRetained(10.),
    RespectPrevVert(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgClipWithSurface::~vtkAtgClipWithSurface()
{
}

int vtkAtgClipWithSurface::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // first object: grid (block model)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        case 1:
            // second object: topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 0);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipWithSurface::RequestData(vtkInformation* request,
                                       vtkInformationVector** inputVector,
                                       vtkInformationVector* outputVector)
{
    // input port 0: the input unstructured grid
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputVector[0]);
    if(nullptr == inputGrid)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithSurface: No valid input block model found");
        return 1;
    }

    // input port 1: the input poly data object
    vtkPolyData* inputPoly = vtkPolyData::GetData(inputVector[1]);
    if(nullptr == inputPoly)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithSurface: No valid input geometry found");
        return 1;
    }

    // clean and normalize the "raw topo" poly data
    // - output port 0: cleaned and normalized topo surface:
    //   made up of cells of type triangle
    // - output port 1: closed topo boundary polygon
    //   exactly one cell of type polyline
    // note: the latter is currently not any more required for this filter, but the
    //   first helps to avoid tons of "useless" error messages and geometric problems
    // ---------------------------------------------
    // NOTE (2024-01-31)
    //   It turns out that within this "cleaning class", a call to the "feature edges"
    //   filter has the potential to demand so much memory that the entire function crashes.
    //   On top of that it turns out that even with some "unclean" surfaces, this filter
    //   will work just well because the method is currently much more robust than it
    //   was initially (just "shooting vertical lines" at certain points - no complex
    //   triangle geometry stuff...): The worst that can happen if the surface is really
    //   completely odd that results will be equally odd.
    //   For this reason, the vtkAtgCleanNormTopo class was removed from the active code
    //vtkSmartPointer<vtkAtgCleanNormTopo> topo = vtkSmartPointer<vtkAtgCleanNormTopo>::New();
    //topo->SetInputData(inputPoly);
    //topo->Update();
    //if(nullptr != topo->GetErrorMsg())
    //{
    //    std::string msg(topo->GetErrorMsg());
    //    vtkOutputWindowDisplayWarningText((msg + "\n").c_str());
    //    return 1;
    //}
    // ---------------------------------------------

    // build the cell locator for further processing
    // note: this filter is mainly an octree based, very fast item finder in space.
    //   It has a function that allows to "shoot" a line segment through the
    //   input polydata structure - the topo in this case. It will be used with
    //   a vertical line through some x/y points, for quick calculation of the
    //   topo z coordinate (elevation) at that point. This functionality is the
    //   "workhorse function" for the current clipping filter.
    vtkSmartPointer<vtkCellLocator> cloc = vtkSmartPointer<vtkCellLocator>::New();
    cloc->SetDataSet(inputPoly);
    cloc->BuildLocator();

    // some variables required for the intersection calculations
    double tt,
           intersect[3],
           pcoord[3],
           elevation[4];
    int subId;

    // using a temporary array for the block handling
    vtkSmartPointer<vtkDoubleArray> tempArr = vtkSmartPointer<vtkDoubleArray>::New();
    tempArr->SetName("__TEMP__");
    tempArr->SetNumberOfValues(inputGrid->GetNumberOfCells());
    tempArr->Fill(-100.); // means: not handled yet

    // in this loop, the blocks are checked in relation with the topo surface:
    // - fully inside the area of the topo?
    // - fully above or below the topo?
    // - if cut by the topo, calculate the fraction below (approximately)
    for(vtkIdType c = 0; c < inputGrid->GetNumberOfCells(); ++c)
    {
        // progress every 1000 blocks
        if(0 == (c % 1000))
            ShowProgress(c, inputGrid->GetNumberOfCells());

        // get the corner points of the block
        vtkPoints* pts = inputGrid->GetCell(c)->GetPoints();

        // go through the four bottom points of the block and get the topo elevation there
        bool outside = false;
        elevation[0] = elevation[1] = elevation[2] = elevation[4] = -10000.;
        for(vtkIdType p = 0; p < std::min<vtkIdType>(4, pts->GetNumberOfPoints() / 2); ++p)
        {
            double pt[3];
            pts->GetPoint(p, pt);
            double btmPt[3] = {pt[0], pt[1], -500.},
                   topPt[3] = {pt[0], pt[1], 10000.};

            // this function call will return true if an intersection with the topo was found,
            // or otherwise false. In the intersect array the coordinates of the intersection
            // point will be returned.
            // note: the function always returns either 0 or 1 intersection points, even if
            //   there would be more. In some tests, this 1 intersection point was always the
            //   first (i.e. lowest) along the line from btmPt to topPt, but not sure if this
            //   is always the case. However, we are assuming here that _normally_ a topo should
            //   only have one intersection point, and only if it is not really "clean" there
            //   would be more - in which case it might be acceptable to just take one of them
            //   and continue. This looks like a good strategy also in terms of rubustness of
            //   the filter regarding arbitrary "unclean" topo input.
            if(!cloc->IntersectWithLine(btmPt, topPt, 0.0001, tt, intersect, pcoord, subId))
            {
                // no intersection means: we are here not in the range of the topo, so we
                // do not further touch the block
                tempArr->SetValue(c, 100.);
                outside = true;
                continue;
            }

            elevation[p] = intersect[2];
        }

        // if one of the points was outside the topo range, continue
        if(outside)
            continue;

        // get the bottom and top elevations of the block
        double btmElev = pts->GetPoint(0)[2],
               topElev = pts->GetPoint(4)[2];

        // count the corners where the topo is fully above or below the topo
        int numAbove = 0, // counts where the block is fully above the topo
            numBelow = 0; // counts the inverse
        for(int c = 0; c < 4; ++c)
        {
            if(elevation[c] >= topElev)
                ++numBelow;
            if(elevation[c] <= btmElev)
                ++numAbove;
        }

        // handle the cases where the block is either fully above or below
        if(4 == numAbove)
        {
            // block is fully above
            tempArr->SetValue(c, (KeepBlocksBelow ? 0. : 100.));
            continue;
        }
        else if(4 == numBelow)
        {
            // block is fully below
            tempArr->SetValue(c, (KeepBlocksBelow ? 100. : 0.));
            continue;
        }

        // now we need to calculate the fraction of the block above or below
        // note: we simply approximate the calculation by rastering the block
        //   surface into 4x4 grid and take the mean value of the center elevations

        // define a start point and two vectors for the raster calculation
        double pt0[3],  // "lower left corner"
               vecu[3], // block edge from origin corner
               vecv[3]; // orthogonal edge
        pts->GetPoint(0, pt0);
        pts->GetPoint(1, vecu);
        pts->GetPoint(3, vecv);
        for(int i = 0; i < 3; ++i)
        {
            vecu[i] -= pt0[i];
            vecv[i] -= pt0[i];
        }

        // double loop for the 4x4 grid
        // note: we carry a counter for the number of grid points as a safety measure:
        //   in a case where the elevation function would not return a value, e.g. because
        //   of a little hole in the topo, this point would simply be ignored
        // note: the loop run variables are 0.5, 1.5 etc. in order to take the center
        //   points of the 4x4 block face segments
        static const double numSegs = 4.;
        double partBelow = 0.;
        int cnt = 0;
        for(double u = .5; u < numSegs; u += 1.)
        {
            for(double v = .5; v < numSegs; v += 1.)
            {
                double btmPt[3] = {pt0[0] + u * vecu[0] / numSegs + v * vecv[0] / numSegs,
                                   pt0[1] + u * vecu[1] / numSegs + v * vecv[1] / numSegs,
                                   -500.},
                       topPt[3] = {btmPt[0], btmPt[1], 10000.};

                if(cloc->IntersectWithLine(btmPt, topPt, 0.0001, tt, intersect, pcoord, subId))
                {
                    double el = intersect[2];
                    if(el >= topElev)
                        partBelow += 100.;
                    else if(el <= btmElev)
                        partBelow += 0.;
                    else
                        partBelow += 100. * (el - btmElev) / (topElev - btmElev);
                    ++cnt;
                }
            }
        }
        partBelow /= cnt;

        // assign part to temp array - taking the "keep blocks below" option into account
        tempArr->SetValue(c, (KeepBlocksBelow ? partBelow : (100. - partBelow)));
    }

    // progress: "almost done"
    ShowProgress(99, 100);

    // generate a new VolFactor attribute and either copy old one or fill with all 100
    std::string volFactorName = utilNormNames::getName(utilNormNames::VOLFACTOR);
    vtkSmartPointer<vtkDoubleArray> volFactorArr = vtkSmartPointer<vtkDoubleArray>::New();
    volFactorArr->SetName(volFactorName.c_str());
    vtkDataArray* oldVolFactorArr = inputGrid->GetCellData()->GetArray(volFactorName.c_str());
    if(nullptr == oldVolFactorArr)
    {
        volFactorArr->SetNumberOfTuples(inputGrid->GetNumberOfCells());
        volFactorArr->Fill(100.);
    }
    else
    {

        volFactorArr->DeepCopy(oldVolFactorArr);
    }

    // generate a temporary grid and add the volume factor array
    vtkSmartPointer<vtkUnstructuredGrid> grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid->ShallowCopy(inputGrid);
    grid->GetCellData()->AddArray(volFactorArr);

    // if we have tonnage attributes, we want to handle them as well
    std::vector<vtkDataArray*> tonnArrs;
    std::vector<vtkSmartPointer<vtkDoubleArray>> newTonnArrs;
    std::vector<int> tonnArrIds(std::initializer_list<int>(
    {
        utilNormNames::KTONS,
        utilNormNames::TONS
    }));
    for(auto it = tonnArrIds.begin(); it != tonnArrIds.end(); ++it)
    {
        vtkDataArray* tonnArr = grid->GetCellData()->GetArray(utilNormNames::getName(*it).c_str());
        if(nullptr != tonnArr)
        {
            tonnArrs.push_back(tonnArr);
            auto newTonnArr = vtkSmartPointer<vtkDoubleArray>::New();
            newTonnArr->SetName(tonnArr->GetName());
            newTonnArr->SetNumberOfTuples(tonnArr->GetNumberOfTuples());
            newTonnArrs.push_back(newTonnArr);
        }
    }

    // transfer the temporary array values to the new volume factor array, while respecting
    // any previous value if the option is set
    for(vtkIdType b = 0; b < volFactorArr->GetNumberOfTuples(); ++b)
    {
        double vf = tempArr->GetValue(b),
               prevVf = volFactorArr->GetValue(b);
        if(RespectPrevVert)
            // if the cut goes in the same direction, we keep the minimum of the two
            vf = std::min<double>(vf, prevVf);
        else
            // otherwise we multiply the two values
            vf = vf * prevVf / 100.;

        // assign the resulting volume factor
        volFactorArr->SetValue(b, vf);

        // handle the tonnage attributes
        auto nit = newTonnArrs.begin();
        for(auto it = tonnArrs.begin(); it != tonnArrs.end(); ++it, ++nit)
        {
            vtkDataArray* arr = *it;
            auto narr = *nit;
            narr->SetVariantValue(b, vf * arr->GetVariantValue(b).ToDouble() / prevVf);
        }
    }

    // replace old tonnage arrays with new ones
    for(auto nit = newTonnArrs.begin(); nit != newTonnArrs.end(); ++nit)
        grid->GetCellData()->AddArray(*nit);

    // generate an INDEX selection for extracting the remaining blocks
    // note: at the same time we fill a bitset in order to adapt the nb attribute if existing
    //   and before doing the renumbering further down
    boost::dynamic_bitset<> remaining;
    remaining.resize(grid->GetNumberOfCells(), false);
    vtkSmartPointer<vtkLongLongArray> selIds = vtkSmartPointer<vtkLongLongArray>::New();
    selIds->SetName("__SELECTION__");
    vtkSmartPointer<vtkSelection> sel = vtkSmartPointer<vtkSelection>::New();
    vtkSmartPointer<vtkSelectionNode> sn = vtkSmartPointer<vtkSelectionNode>::New();
    sn->SetFieldType(vtkSelectionNode::CELL);
    sn->SetContentType(vtkSelectionNode::INDICES);
    sn->SetSelectionList(selIds);
    sel->AddNode(sn);

    // select all blocks where the volume factor is higher than indicated limit
    for(vtkIdType b = 0; b < volFactorArr->GetNumberOfTuples(); ++b)
    {
        if(MinVolFactorRetained <= volFactorArr->GetValue(b))
        {
            selIds->InsertNextValue(b);
            remaining.set(b);
        }
    }

    // update neighbor indices in such a way that indices of blocks that are not remaining
    // will not appear any more
    std::unique_ptr<utilUpdateNeighborsForRemainingBlocks> updateNb(new utilUpdateNeighborsForRemainingBlocks(grid));
    updateNb->updateForRemaining(remaining);

    // do the extraction using the selection
    // note: the assumption is that this will generate a new model ("deep extract")
    vtkSmartPointer<vtkExtractSelection> extractSel = vtkSmartPointer<vtkExtractSelection>::New();
    extractSel->SetInputData(grid);
    extractSel->SetInputData(1, sel);
    extractSel->Update();

    // output the filtered data
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->ShallowCopy(extractSel->GetOutput());

    // update the block ids and neighbor indices, if present
    std::unique_ptr<utilRenumberBlocks> renumBlocks(new utilRenumberBlocks(outGrid));
    renumBlocks->renumberBlocksAndNeighbors();

    // progress: "fully done"
    ShowProgress(100, 100);

    return 1;
}

int vtkAtgClipWithSurface::RequestInformation(vtkInformation* request,
                                              vtkInformationVector** inputVector,
                                              vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgClipWithSurface::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}

void vtkAtgClipWithSurface::ShowProgress(int step, int totalSteps)
{
    if(AbortExecute)
        return;

    float progress = static_cast<float>(step) / totalSteps,
          rounded = static_cast<float>(static_cast<int>((progress * 100) + 0.5f)) / 100.f;
    if(GetProgress() != rounded)
        UpdateProgress(rounded);
}
