/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithGeotiff.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <vtkSmartPointer.h>
#include <vtkCallbackCommand.h>
#include <vtkGDALRasterReader.h>
#include <vtkAtgProgressObserver.h>
#include <vtkCell.h>
#include <vtkPoints.h>
#include <vtkImageData.h>
#include <vtkBoundingBox.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>
#include <vtkSetGet.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilUpdateNeighborsForRemainingBlocks.h>
#include <utilRenumberBlocks.h>
#include <utilNormNames.h>
#include <vtkAtgClipWithGeotiff.h>

// coordinates tolerance: 1mm
static const double eps = 0.001;

vtkStandardNewMacro(vtkAtgClipWithGeotiff)

static void OnError(vtkObject* vtkNotUsed(caller),
                    unsigned long int vtkNotUsed(eventId),
                    void* clientData,
                    void* callData)
{
    // with this we signal that there was an error in the raster reader
    // note: we do not want to see a message from the raster reader
    bool* errorFlag = static_cast<bool*>(clientData);
    *errorFlag = true;

    // this would be the message that the raster reader wants to issue
    char const* msg = static_cast<char const*>(callData);
}

static bool pointInsidePoly(double x, double y, std::vector<std::vector<double>> const& poly)
{
    int intersectCount = 0,
        numCorners = poly.size();
    for(int side = 0; side < numCorners; ++side)
    {
        // proportion of line in y direction: 0 to 1 is "in range of the line segment"
        double p1[2] = {poly[side][0], poly[side][1]},
               p2[2] = {poly[(side + 1) % numCorners][0], poly[(side + 1) % numCorners][1]};

        // check if the side is horizontal
        if(eps > ::fabs(p1[1] - p2[1]))
        {
            // if test point is also on that horizontal line, and inside the side,
            // it is "on the boundary" and we count it as "inside"
            if((eps > ::fabs(y - p1[1])) &&
               (x >= (std::min<double>(p1[0], p2[0]) - eps)) &&
               (x <= (std::max<double>(p1[0], p2[0]) + eps)))
            {
                return true;
            }

            continue;
        }

        // now we want to see at which fraction of the side the test point is
        double p = (y - p1[1]) / (p2[1] - p1[1]);

        // general case: if less than 0 or more than 1 - ignore because outside of
        // the side's line segment
        if((p < -eps) || (p > (1. + eps)))
        {
            continue;
        }

        // special case: close to 0 or 1 we count only if this is an "upper end"
        // note: with this we get 1 intersection if our horizontal test line cuts
        //   the polygon at the join between two line segments, while in the case
        //   that such a join just touches the test line will generate either 0 or 2
        //   intersections - which is "the same" in our case
        else if(((eps >= ::fabs(p))      && ((p1[1] - p2[1]) < 0.)) ||
                ((eps >= ::fabs(p - 1.)) && ((p2[1] - p1[1]) < 0.)))
        {
            // here we only still need to check if the test point actually coincides
            // with one of the line segment ends - which means it would then be
            // "on the boundary", counting as "inside" here
            if(((eps > ::fabs(x - p1[0])) && (eps > ::fabs(y - p1[1]))) ||
               ((eps > ::fabs(x - p2[0])) && (eps > ::fabs(y - p2[1]))))
            {
                return true;
            }

            continue;
        }

        // now we need the x - in order to see if we are "left" or "right"
        double x0 = p1[0] + p * (p2[0] - p1[0]);

        // if now x0 coincides with x, we are "on the boundary" - and this
        // we want to count as "inside", no matter what other intersections occur
        if(eps > ::fabs(x - x0))
        {
            return true;
        }

        // we count intersections that are left of the test point
        if(x0 < x)
            ++intersectCount;
    }

    // if we have an odd number of intersections, we are "in"
    return 1 == (intersectCount % 2);
}

vtkAtgClipWithGeotiff::vtkAtgClipWithGeotiff()
:   GeotiffFileName(nullptr),
    KeepBlocksBelow(true),
    MinVolFactorRetained(10.),
    RespectPrevVert(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgClipWithGeotiff::~vtkAtgClipWithGeotiff()
{
    SetGeotiffFileName(nullptr);
}

int vtkAtgClipWithGeotiff::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // first object: grid (block model)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipWithGeotiff::RequestData(vtkInformation* request,
                                       vtkInformationVector** inputVector,
                                       vtkInformationVector* outputVector)
{
    // input: the input unstructured grid
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputVector[0]);
    if(nullptr == inputGrid)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithGeotiff: No valid input block model found");
        return 1;
    }

    // just in case "nothing works" below: we want to see the unchanged input block model then!
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->ShallowCopy(inputGrid);

    // make sure we have a file to read
    if(!GeotiffFileName || (0 == ::strlen(GeotiffFileName)))
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithGeotiff: No Geotiff file name specified\n");
        return 1;
    }

    // added to fix crash that would occur if input file was on the recent file menu
    // but had be deleted from the HDD, therefore no longer existing...
    // MLivingstone
    std::ifstream file;
    file.open(GeotiffFileName, ios::in);
    if(!file)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithGeotiff: The Geotiff file does not exist\n");
        file.close();
        return 1;
    }
    file.close();

    // we want to catch any error condition from the raster reader and handle it here,
    // and for that we need the readerError flag
    vtkNew<vtkCallbackCommand> errObs;
    errObs->SetCallback(&OnError);
    bool readerError = false;
    errObs->SetClientData(&readerError);

    // progress display
    vtkSmartPointer<vtkAtgProgressObserver> progObs = vtkSmartPointer<vtkAtgProgressObserver>::New();
    progObs->SetAlgorithm(this);

    // use the GDAL raster reader to read the raster image
    vtkSmartPointer<vtkGDALRasterReader> rasterReader = vtkSmartPointer<vtkGDALRasterReader>::New();
    rasterReader->SetFileName(GeotiffFileName);
    SetProgressText("Reading Raster Image");
    rasterReader->AddObserver("ErrorEvent", errObs);
    rasterReader->SetProgressObserver(progObs);
    rasterReader->Update();
    rasterReader->RemoveAllObservers();

    // if we had a reader error, not much we can do
    if(readerError)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithGeotiff: No georeferencing found in the Geotiff file\n");
        return 1;
    }

    // extract some georeferencing information
    double org[3] = {0., 0., 0.};
    rasterReader->GetDataOrigin(org);
    double spc[3] = {0., 0., 0.};
    rasterReader->GetDataSpacing(spc);
    int* rasDims = rasterReader->GetRasterDimensions();
    double invalidElev = rasterReader->GetInvalidValue();

    // normally georeferencing is "top to bottom", meaning that the origin is the upper left
    // point and the vertical spacing is negative (downwards). However, it looks like there
    // are exceptions to this rule, but since we want to rely on it, we correct it
    bool yInvert = 0. < spc[1];

    // note that the corner points from GetGeoCornerPoints are not completely correct:
    // the x coordinate of the second point is always the same as x of the first point
    double corn2[4] =
    {
        org[0],
        org[1],
        org[0] + spc[0] * rasDims[0],
        org[1] + spc[1] * rasDims[1]
    };
    if(yInvert)
        std::swap(corn2[1], corn2[3]);

    // now we need the elevations per cell
    // note: it looks like they are always in a cell attribute named "Elevation"
    vtkImageData* img = rasterReader->GetOutput();
    vtkCellData* cellData = img->GetCellData();
    vtkDataArray* elevArr = (nullptr != cellData) ? cellData->GetArray("Elevation") : nullptr;
    if(nullptr == elevArr)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithGeotiff: No elevation info found in Geotiff file\n");
        return 1;
    }

    // using a temporary array for the block handling
    vtkSmartPointer<vtkDoubleArray> tempArr = vtkSmartPointer<vtkDoubleArray>::New();
    tempArr->SetName("__TEMP__");
    tempArr->SetNumberOfValues(outGrid->GetNumberOfCells());
    tempArr->Fill(-100.); // means: not handled yet

    // for blocks that are directly above each other we want to avoid having to recalculate
    // all the geometry over and over again, so we keep here some buffer map: block corner
    // points vs. list of elevations
    typedef std::vector<double> Coord, ElevList;
    typedef std::vector<Coord> Polygon, ElevListList;
    std::map<Polygon, ElevListList> blockElevs;

    // weight factors for the first (main block) and second (neighbor blocks) elevation lists
    // note reason for the (arbitrary!) factor 32:
    // - if on average 1 or more points in a block's area, the weight of all 8 neighbor blocks
    //   is still only 1/4 of the weights in the actual block, thus not too much smoothing
    // - if on average 0-1 points in a block area, but still always 1+ in the block area
    //   including neighbors, the weight is unimportant in the case of 0 points in the main block
    // - if the geotiff raster is much larger than the block raster, this entire filter will
    //   not work properly: better use a geotiff reader first and triangulate the surface, then
    //   apply the "clip with surface" filter
    static const double weight1 = 32.,
                        weight2 = 1.;

    // in this loop, the blocks are checked in relation with the topo surface:
    // - fully inside the area of the topo?
    // - fully above or below the topo?
    // - if cut by the topo, calculate the fraction below (approximately)
    SetProgressText("Filtering and cutting blocks");
    for(vtkIdType c = 0; c < outGrid->GetNumberOfCells(); ++c)
    {
        // progress every 1000 blocks
        if(0 == (c % 1000))
            ShowProgress(c, outGrid->GetNumberOfCells());

        // get the corner points of the block
        vtkPoints* pts = outGrid->GetCell(c)->GetPoints();

        // for the first block we will check the ratio between raster spacing and block size
        // and warn if this ratio is not good enough for a reasonable clipping result
        if(0 == c)
        {
            double pp0[3],
                   pp1[3],
                   pp2[3];
            pts->GetPoint(0, pp0);
            pts->GetPoint(1, pp1);
            pts->GetPoint(2, pp2);
            double dx = pp1[0] - pp0[0],
                   dy = pp1[1] - pp0[1],
                   szx = ::sqrt(dx * dx + dy * dy);

            dx = pp2[0] - pp1[0];
            dy = pp2[1] - pp1[1];
            double szy = ::sqrt(dx * dx + dy * dy),
                   blockdiag = ::sqrt(szx * szx + szy * szy),
                   rasterdiag = ::sqrt(spc[0] * spc[0] + spc[1] * spc[1]);

            if(rasterdiag > (1.5 * blockdiag))
            {
                std::string msg = "AtgClipWithGeotiff: The TIFF raster cell size (" +
                                  std::to_string(spc[0]) + "x" + std::to_string(spc[1]) +
                                  ") is very coarse grained compared to the block size (" +
                                  std::to_string(szx) + "x" + std::to_string(szy) +
                                  ")\n"
                                  "Recommendation: Read the Geotiff file as a triangulated surface, then use the Clip with Surface filter!\n";
                vtkOutputWindowDisplayWarningText(msg.c_str());
            }
        }

        // see if that block area is already in the buffer
        ElevListList elevations;
        ElevList elev1,
                 elev2;
        Polygon poly;
        for(int i = 0; i < 4; ++i)
            poly.push_back(Coord(std::initializer_list<double>({pts->GetPoint(i)[0],
                                                                pts->GetPoint(i)[1]})));
        auto polyIt = blockElevs.find(poly);

        // already in buffer: just get the elevations
        if(blockElevs.end() != polyIt)
        {
            elevations = polyIt->second;
            elev1 = elevations[0];
            elev2 = elevations[1];
        }

        // otherwise we need to do the entire check
        else
        {
            // remember already the empty elevations list: in the case that we find
            // none later on, this is what will be the sign of it
            elevations.push_back(elev1);
            elevations.push_back(elev2);
            blockElevs[poly] = elevations;

            // go through the four bottom points of the block and see if we are fully inside topo,
            // and at the same time generate a block bounding box
            bool outside = false;
            vtkBoundingBox bbox;
            for(int p = 0; p < 4; ++p)
            {
                // note that the Geotiff is always parallel to the main coordinate axes
                outside = (poly[p][0] < (corn2[0] - eps)) ||
                          (poly[p][0] > (corn2[2] + eps)) ||
                          (poly[p][1] > (corn2[1] + eps)) ||
                          (poly[p][1] < (corn2[3] - eps));
                if(outside)
                    break;

                // put into bounding box
                bbox.AddPoint(poly[p][0], poly[p][1], 0.);
            }

            // if one of the points was outside the topo range: do not cut the
            // block and continue
            if(outside)
            {
                tempArr->SetValue(c, 100.);
                continue;
            }

            // generate the larger block area: block plus one block size in all directions,
            // including the extended bounding box
            Polygon lpoly;
            vtkBoundingBox lbbox;
            for(int i = 0; i < 4; ++i)
            {
                Coord ppt;
                int i2 = (i + 2) % 4;
                for(int c = 0; c < 2; ++c)
                {
                    double co = 2. * poly[i][c] - poly[i2][c];
                    ppt.push_back(co);
                }
                lpoly.push_back(ppt);
                lbbox.AddPoint(ppt[0], ppt[1], 0.);
            }

            // generate a "raster index bounding box" (min/max index in row/column)
            // note that in y direction, rows are counting from top to bottom!
            vtkIdType rasbbox[4] =
            {
                static_cast<vtkIdType>(::floor((bbox.GetBound(0) - org[0] - eps) / spc[0])),
                static_cast<vtkIdType>(::floor((bbox.GetBound(1) - org[0] + eps) / spc[0])),
                static_cast<vtkIdType>(::floor((bbox.GetBound(3) - org[1] - eps) / spc[1])),
                static_cast<vtkIdType>(::floor((bbox.GetBound(2) - org[1] + eps) / spc[1]))
            };
            if(yInvert)
                std::swap(rasbbox[2], rasbbox[3]);

            // dto. for the extended range
            // note that in this case we need to check that the row/col values are not out of range
            vtkIdType lrasbbox[4] =
            {
                static_cast<vtkIdType>(::floor((lbbox.GetBound(0) - org[0] - eps) / spc[0])),
                static_cast<vtkIdType>(::floor((lbbox.GetBound(1) - org[0] + eps) / spc[0])),
                static_cast<vtkIdType>(::floor((lbbox.GetBound(3) - org[1] - eps) / spc[1])),
                static_cast<vtkIdType>(::floor((lbbox.GetBound(2) - org[1] + eps) / spc[1]))
            };
            if(yInvert)
                std::swap(lrasbbox[2], lrasbbox[3]);
            lrasbbox[0] = std::max<vtkIdType>(lrasbbox[0], 0);
            lrasbbox[1] = std::min<vtkIdType>(lrasbbox[1], rasDims[0] - 1);
            lrasbbox[2] = std::max<vtkIdType>(lrasbbox[2], 0);
            lrasbbox[3] = std::min<vtkIdType>(lrasbbox[3], rasDims[1] - 1);

            // collect elevations inside the block and the extended block area
            // note: we constrain the row/col indices to the rasterBBox range, but still need
            //   to check for every raster point if it is inside or outside the block square
            for(vtkIdType row = lrasbbox[2]; row <= lrasbbox[3]; ++row)
            {
                double y = org[1] + (row + 0.5) * spc[1];
                for(vtkIdType col = lrasbbox[0]; col <= lrasbbox[1]; ++col)
                {
                    vtkIdType elevInx = (rasDims[1] - row - 1) * rasDims[0] + col;
                    double x = org[0] + (col + 0.5) * spc[0],
                           z = elevArr->GetVariantValue(elevInx).ToDouble();

                    // if the elevation is invalid, we ignore it
                    if(eps > ::fabs(invalidElev - z))
                    {
                        continue;
                    }

                    // check if row/col are in the inner raster bbox
                    if((col >= rasbbox[0]) &&
                       (col <= rasbbox[1]) &&
                       (row >= rasbbox[2]) &&
                       (row <= rasbbox[3]) &&
                       pointInsidePoly(x, y, poly))
                    {
                        elev1.push_back(z);
                    }

                    // otherwise we try the extended area
                    else if(pointInsidePoly(x, y, lpoly))
                    {
                        elev2.push_back(z);
                    }
                }
            }

            // remember for possible next time same block area
            elevations.clear();
            elevations.push_back(elev1);
            elevations.push_back(elev2);
            blockElevs[poly] = elevations;
        }

        // if we have no elevations: keep the block and continue
        if(elev1.empty() && elev2.empty())
        {
            tempArr->SetValue(c, 100.);
            continue;
        }

        // get the bottom and top elevations of the block
        double btmElev = pts->GetPoint(0)[2],
               topElev = pts->GetPoint(4)[2];

        // now we need to calculate the fraction of the block above or below
        // note: we simply take the elevations from the raster and assume that
        //   they are cutting the block into "equal parts"
        // note: elevations from elev2 (neighbor blocks) are only used if we have
        //   less than 4 elevations inside elev1 (the "real" block elevations),
        //   because in this case we assume that we could miss some slope that
        //   is affecting the block
        double partBelow = 0.,
               normalizer = 0.;
        for(auto it = elev1.begin(); it != elev1.end(); ++it)
        {
            if(*it >= topElev)
                partBelow += weight1 * 100.;
            else if(*it <= btmElev)
                partBelow += 0.;
            else
                partBelow += weight1 * 100. * (*it - btmElev) / (topElev - btmElev);
            normalizer += weight1;
        }
        if(4 > elev1.size())
        {
            for(auto it = elev2.begin(); it != elev2.end(); ++it)
            {
                if(*it >= topElev)
                    partBelow += weight2 * 100.;
                else if(*it <= btmElev)
                    partBelow += 0.;
                else
                    partBelow += weight2 * 100. * (*it - btmElev) / (topElev - btmElev);
                normalizer += weight2;
            }
        }
        partBelow /= normalizer;

        // assign part to temp array - taking the "keep blocks below" option into account
        tempArr->SetValue(c, (KeepBlocksBelow ? partBelow : (100. - partBelow)));
    }

    // progress: "almost done"
    ShowProgress(99, 100);

    // generate a new VolFactor attribute and either copy old one or fill with all 100
    std::string volFactorName = utilNormNames::getName(utilNormNames::VOLFACTOR);
    vtkSmartPointer<vtkDoubleArray> volFactorArr = vtkSmartPointer<vtkDoubleArray>::New();
    volFactorArr->SetName(volFactorName.c_str());
    vtkDataArray* oldVolFactorArr = outGrid->GetCellData()->GetArray(volFactorName.c_str());
    if(nullptr == oldVolFactorArr)
    {
        volFactorArr->SetNumberOfTuples(outGrid->GetNumberOfCells());
        volFactorArr->Fill(100.);
    }
    else
    {
        volFactorArr->DeepCopy(oldVolFactorArr);
    }

    // generate a temporary grid and add the volume factor array
    vtkSmartPointer<vtkUnstructuredGrid> grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid->DeepCopy(outGrid);
    grid->GetCellData()->AddArray(volFactorArr);

    // if we have tonnage attributes, we want to handle them as well
    std::vector<vtkDataArray*> tonnArrs;
    std::vector<vtkSmartPointer<vtkDoubleArray>> newTonnArrs;
    std::vector<int> tonnArrIds(std::initializer_list<int>(
    {
        utilNormNames::KTONS,
        utilNormNames::TONS
    }));
    for(auto it = tonnArrIds.begin(); it != tonnArrIds.end(); ++it)
    {
        vtkDataArray* tonnArr = grid->GetCellData()->GetArray(utilNormNames::getName(*it).c_str());
        if(nullptr != tonnArr)
        {
            tonnArrs.push_back(tonnArr);
            auto newTonnArr = vtkSmartPointer<vtkDoubleArray>::New();
            newTonnArr->SetName(tonnArr->GetName());
            newTonnArr->SetNumberOfTuples(tonnArr->GetNumberOfTuples());
            newTonnArrs.push_back(newTonnArr);
        }
    }

    // transfer the temporary array values to the new volume factor array, while respecting
    // any previous value if the option is set
    for(vtkIdType b = 0; b < volFactorArr->GetNumberOfTuples(); ++b)
    {
        double vf = tempArr->GetValue(b),
               prevVf = volFactorArr->GetValue(b);
        if(RespectPrevVert)
            // if the cut goes in the same direction, we keep the minimum of the two
            vf = std::min<double>(vf, prevVf);
        else
            // otherwise we multiply the two values
            vf = vf * prevVf / 100.;

        // assign the resulting volume factor
        volFactorArr->SetValue(b, vf);

        // handle the tonnage attributes
        auto nit = newTonnArrs.begin();
        for(auto it = tonnArrs.begin(); it != tonnArrs.end(); ++it, ++nit)
        {
            vtkDataArray* arr = *it;
            auto narr = *nit;
            narr->SetVariantValue(b, vf * arr->GetVariantValue(b).ToDouble() / prevVf);
        }
    }

    // replace old tonnage arrays with new ones
    for(auto nit = newTonnArrs.begin(); nit != newTonnArrs.end(); ++nit)
        grid->GetCellData()->AddArray(*nit);

    // generate an INDEX selection for extracting the remaining blocks
    // note: at the same time we fill a bitset in order to adapt the nb attribute if existing
    //   and before doing the renumbering further down
    boost::dynamic_bitset<> remaining;
    remaining.resize(grid->GetNumberOfCells(), false);
    vtkSmartPointer<vtkLongLongArray> selIds = vtkSmartPointer<vtkLongLongArray>::New();
    selIds->SetName("__SELECTION__");
    vtkSmartPointer<vtkSelection> sel = vtkSmartPointer<vtkSelection>::New();
    vtkSmartPointer<vtkSelectionNode> sn = vtkSmartPointer<vtkSelectionNode>::New();
    sn->SetFieldType(vtkSelectionNode::CELL);
    sn->SetContentType(vtkSelectionNode::INDICES);
    sn->SetSelectionList(selIds);
    sel->AddNode(sn);

    // select all blocks where the volume factor is higher than indicated limit
    for(vtkIdType b = 0; b < volFactorArr->GetNumberOfTuples(); ++b)
    {
        if(MinVolFactorRetained <= volFactorArr->GetValue(b))
        {
            selIds->InsertNextValue(b);
            remaining.set(b);
        }
    }

    // update neighbor indices in such a way that indices of blocks that are not remaining
    // will not appear any more
    std::unique_ptr<utilUpdateNeighborsForRemainingBlocks> updateNb(new utilUpdateNeighborsForRemainingBlocks(grid));
    updateNb->updateForRemaining(remaining);

    // do the extraction using the selection
    // note: the assumption is that this will generate a new model ("deep extract")
    vtkSmartPointer<vtkExtractSelection> extractSel = vtkSmartPointer<vtkExtractSelection>::New();
    extractSel->SetInputData(grid);
    extractSel->SetInputData(1, sel);
    extractSel->Update();

    // output the filtered data
    outGrid->ShallowCopy(extractSel->GetOutput());

    // update the block ids and neighbor indices, if present
    std::unique_ptr<utilRenumberBlocks> renumBlocks(new utilRenumberBlocks(outGrid));
    renumBlocks->renumberBlocksAndNeighbors();

    // progress: "fully done"
    ShowProgress(100, 100);

    return 1;
}

int vtkAtgClipWithGeotiff::RequestInformation(vtkInformation* request,
                                              vtkInformationVector** inputVector,
                                              vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgClipWithGeotiff::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}

void vtkAtgClipWithGeotiff::ShowProgress(int step, int totalSteps)
{
    if(AbortExecute)
        return;

    float progress = static_cast<float>(step) / totalSteps,
          rounded = static_cast<float>(static_cast<int>((progress * 100) + 0.5f)) / 100.f;
    if(GetProgress() != rounded)
        UpdateProgress(rounded);
}
