/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithGeotiff.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgClipWithGeotiff_h
#define vtkAtgClipWithGeotiff_h

#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgClipWithGeotiffModuleModule.h>

class vtkPoints;
class vtkPolyData;

class ATGCLIPWITHGEOTIFFMODULE_EXPORT vtkAtgClipWithGeotiff: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgClipWithGeotiff* New();
    vtkTypeMacro(vtkAtgClipWithGeotiff, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetStringMacro(GeotiffFileName)
    vtkGetStringMacro(GeotiffFileName)

    vtkSetMacro(KeepBlocksBelow, bool)
    vtkGetMacro(KeepBlocksBelow, bool)

    vtkSetMacro(MinVolFactorRetained, double)
    vtkGetMacro(MinVolFactorRetained, double)

    vtkSetMacro(RespectPrevVert, bool)
    vtkGetMacro(RespectPrevVert, bool)

protected:

    vtkAtgClipWithGeotiff();
    ~vtkAtgClipWithGeotiff();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgClipWithGeotiff(vtkAtgClipWithGeotiff const&);
    void operator=(vtkAtgClipWithGeotiff const&);

    // progress indicator, discrete percent
    void ShowProgress(int step, int totalSteps);

    // name of the Geotiff file
    char* GeotiffFileName;

    // if this is true, the clipping will be from above, otherwise from below
    bool KeepBlocksBelow;

    // minimum volume factor to be retained
    double MinVolFactorRetained;

    // if the model was already cut vertically, this should be checked to avoid cutting
    // the same volume twice
    bool RespectPrevVert;

};

#endif
