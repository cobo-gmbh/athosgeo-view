/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgNewBlockModel.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __vtkAtgNewBlockModel_h
#define __vtkAtgNewBlockModel_h

#include <string>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgNewBlockModelModuleModule.h>

class vtkTable;
class vtkAbstractArray;

class ATGNEWBLOCKMODELMODULE_EXPORT vtkAtgNewBlockModel: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgNewBlockModel* New();
    vtkTypeMacro(vtkAtgNewBlockModel, vtkUnstructuredGridAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetVector3Macro(Origin, double)
    vtkGetVector3Macro(Origin, double)

    vtkSetVector3Macro(BlockSize, double)
    vtkGetVector3Macro(BlockSize, double)

    vtkSetVector3Macro(NumBlocks, int)
    vtkGetVector3Macro(NumBlocks, int)

    vtkSetMacro(RotAngle, double)
    vtkGetMacro(RotAngle, double)

    vtkSetMacro(TonnageAtt, int)
    vtkGetMacro(TonnageAtt, int)

    vtkSetMacro(TonnageVal, double)
    vtkGetMacro(TonnageVal, double)

    vtkSetMacro(LevelAtt, bool)
    vtkGetMacro(LevelAtt, bool)

    vtkSetMacro(ColumnRowAtts, bool)
    vtkGetMacro(ColumnRowAtts, bool)

protected:

    vtkAtgNewBlockModel();
    ~vtkAtgNewBlockModel();

    int RequestData(vtkInformation* request,
                    vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector);

private:

    // origin (lower left corner)
    double Origin[3];

    // block size
    double BlockSize[3];

    // number of blocks per dimension
    int NumBlocks[3];

    // rotation angle
    double RotAngle;

    // tonnage attribute:
    // - 0: none
    // - 1: KTons
    // - 2: Tons
    int TonnageAtt;

    // tonnage value to assign (if not none)
    double TonnageVal;

    // flags: generate Level and/or Column/Row attributes
    bool LevelAtt,
         ColumnRowAtts;

};

#endif
