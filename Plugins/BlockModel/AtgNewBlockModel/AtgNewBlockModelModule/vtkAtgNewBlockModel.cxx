/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgNewBlockModel.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTable.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <utilNormNames.h>
#include <vtkAtgTableToBlocks.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgNewBlockModel.h>

vtkStandardNewMacro(vtkAtgNewBlockModel)

vtkAtgNewBlockModel::vtkAtgNewBlockModel()
:   Origin{0., 0., 0.},
    BlockSize{10., 10., 5.},
    NumBlocks{10, 10, 10},
    RotAngle(0.),
    TonnageAtt(1),
    TonnageVal(1.),
    LevelAtt(true),
    ColumnRowAtts(true)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkAtgNewBlockModel::~vtkAtgNewBlockModel()
{
}

void vtkAtgNewBlockModel::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}

int vtkAtgNewBlockModel::RequestData(vtkInformation* request,
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
    // get the info about the output port
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* output =
            vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // check the input parameters
    double blockVol = BlockSize[0] * BlockSize[1] * BlockSize[2];
    vtkIdType numRows = NumBlocks[0] * NumBlocks[1] * NumBlocks[2];
    static const double volEps = 1.0e-5;
    if(blockVol < volEps)
    {
        vtkOutputWindowDisplayWarningText("Block size must not be 0 in any direction");
        return 1;
    }
    if(numRows <= 0)
    {
        vtkOutputWindowDisplayWarningText("Number of blocks will be 0");
        return 1;
    }

    // generate an empty table
    vtkSmartPointer<vtkTable> inTable = vtkSmartPointer<vtkTable>::New();

    // add attribute columns: Center, Size, Angle and Block ID
    vtkSmartPointer<vtkDoubleArray> centArr = vtkSmartPointer<vtkDoubleArray>::New();
    centArr->SetName(utilNormNames::getName(utilNormNames::BLOCKCENTER).c_str());
    centArr->SetNumberOfComponents(3);
    centArr->SetComponentName(0, "X");
    centArr->SetComponentName(1, "Y");
    centArr->SetComponentName(2, "Z");
    centArr->SetNumberOfTuples(numRows);
    inTable->AddColumn(centArr);
    vtkSmartPointer<vtkDoubleArray> sizeArr = vtkSmartPointer<vtkDoubleArray>::New();
    sizeArr->SetName(utilNormNames::getName(utilNormNames::BLOCKSIZE).c_str());
    sizeArr->SetNumberOfComponents(3);
    sizeArr->SetComponentName(0, "X");
    sizeArr->SetComponentName(1, "Y");
    sizeArr->SetComponentName(2, "Z");
    sizeArr->SetNumberOfTuples(numRows);
    for(int c = 0; c < 3; ++c)
        sizeArr->FillComponent(c, BlockSize[c]);
    inTable->AddColumn(sizeArr);
    vtkSmartPointer<vtkDoubleArray> angArr = vtkSmartPointer<vtkDoubleArray>::New();
    angArr->SetName(utilNormNames::getName(utilNormNames::ANGLE).c_str());
    angArr->SetNumberOfTuples(numRows);
    angArr->Fill(RotAngle);
    inTable->AddColumn(angArr);
    vtkSmartPointer<vtkLongLongArray> idArr = vtkSmartPointer<vtkLongLongArray>::New();
    idArr->SetName(utilNormNames::getName(utilNormNames::BLOCKID).c_str());
    idArr->SetNumberOfTuples(numRows);
    inTable->AddColumn(idArr);

    // add tonnage attribute if chosen
    if(TonnageAtt > 0)
    {
        vtkSmartPointer<vtkDoubleArray> tonnAtt = vtkSmartPointer<vtkDoubleArray>::New();
        std::string tonnAttName = utilNormNames::getName((2 == TonnageAtt) ?
                                                         utilNormNames::TONS :
                                                         utilNormNames::KTONS);
        tonnAtt->SetName(tonnAttName.c_str());
        tonnAtt->SetNumberOfTuples(numRows);
        tonnAtt->Fill(TonnageVal);
        inTable->AddColumn(tonnAtt);
    }

    // add level attribute if chosen
    vtkSmartPointer<vtkDoubleArray> levelAtt;
    if(LevelAtt)
    {
        levelAtt = vtkSmartPointer<vtkDoubleArray>::New();
        levelAtt->SetName(utilNormNames::getName(utilNormNames::LEVEL).c_str());
        levelAtt->SetNumberOfTuples(numRows);
        inTable->AddColumn(levelAtt);
    }

    // add row and column attributes if chosen
    vtkSmartPointer<vtkDoubleArray> columnAtt,
                                    rowAtt;
    if(ColumnRowAtts)
    {
        columnAtt = vtkSmartPointer<vtkDoubleArray>::New();
        columnAtt->SetName(utilNormNames::getName(utilNormNames::COLUMN).c_str());
        columnAtt->SetNumberOfTuples(numRows);
        inTable->AddColumn(columnAtt);
        rowAtt = vtkSmartPointer<vtkDoubleArray>::New();
        rowAtt->SetName(utilNormNames::getName(utilNormNames::ROW).c_str());
        rowAtt->SetNumberOfTuples(numRows);
        inTable->AddColumn(rowAtt);
    }

    // fill center coordinates with values in a triple loop
    // note: at the same time fill level, row and column attributes if existing
    static const double rad = 3.1415926535897932384626433832795 / 180.;
    vtkIdType inx = 0;
    for(vtkIdType lev = 1; lev <= NumBlocks[2]; ++lev)
    {
        for(vtkIdType row = 1; row <= NumBlocks[1]; ++row)
        {
            for(vtkIdType col = 1; col <= NumBlocks[0]; ++col)
            {
                if(levelAtt)
                    levelAtt->SetValue(inx, lev);
                if(rowAtt)
                    rowAtt->SetValue(inx, row);
                if(columnAtt)
                    columnAtt->SetValue(inx, col);

                double x0 = (double(col) - .5) * BlockSize[0],
                       y0 = (double(row) - .5) * BlockSize[1],
                       z  = (double(lev) - .5) * BlockSize[2],
                       ang = rad * RotAngle,
                       x  = ::cos(ang) * x0 - ::sin(ang) * y0,
                       y  = ::sin(ang) * x0 + ::cos(ang) * y0;

                centArr->SetComponent(inx, 0, Origin[0] + x);
                centArr->SetComponent(inx, 1, Origin[1] + y);
                centArr->SetComponent(inx, 2, Origin[2] + z);
                idArr->SetValue(inx, inx);
                inx++;
            }
        }
    }

    // convert into block model
    // note: we pass the block size and angle parameters to the conversion
    // filter, so they can be used if values do not exist in the input table
    vtkSmartPointer<vtkAtgTableToBlocks> generateBlockModel =
            vtkSmartPointer<vtkAtgTableToBlocks>::New();
    generateBlockModel->SetInputData(inTable);
    generateBlockModel->SetBlockSize(GetBlockSize());
    generateBlockModel->SetRotAngle(GetRotAngle());
    generateBlockModel->Update();

    // copy the generated model
    output->ShallowCopy(generateBlockModel->GetOutput());

    return 1;
}
