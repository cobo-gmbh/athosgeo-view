/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithBoundary.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <string>
#include <sstream>
#include <map>
#include <memory>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/register/ring.hpp>
#include <boost/geometry/geometries/adapted/boost_tuple.hpp>
#include <boost/dynamic_bitset.hpp>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkHexahedron.h>
#include <vtkCell.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilUpdateNeighborsForRemainingBlocks.h>
#include <utilRenumberBlocks.h>
#include <utilNormNames.h>
#include <vtkAtgCleanNormBoundary.h>
#include <vtkAtgClipWithBoundary.h>

vtkStandardNewMacro(vtkAtgClipWithBoundary)

namespace bg = boost::geometry;

BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)

typedef boost::tuple<double, double> zfbPair;
typedef bg::model::ring<zfbPair> zfbRing;
typedef bg::model::polygon<zfbPair> zfbPolygon;
typedef bg::model::multi_polygon<zfbPolygon> zfbMultiPolygon;

struct zfbRingLess
{
    bool pairLess(zfbPair const& p1, zfbPair const& p2) const
    {
        if(bg::get<0>(p1) < bg::get<0>(p2))
            return true;
        if(bg::get<0>(p1) > bg::get<0>(p2))
            return false;
        return bg::get<1>(p1) < bg::get<1>(p2);
    }

    bool operator()(zfbRing const& r1, zfbRing const& r2) const
    {
        std::size_t npts = std::min(r1.size(), r2.size());
        for(unsigned long n = 0; n < npts; ++n)
        {
            zfbPair const& pt1 = r1.at(n);
            zfbPair const& pt2 = r2.at(n);
            if(pairLess(pt1, pt2))
                return true;
            if(pairLess(pt2, pt1))
                return false;
        }
        return false;
    }
};

vtkAtgClipWithBoundary::vtkAtgClipWithBoundary()
:   MinVolFactorRetained(10.),
    RespectPrevHor(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgClipWithBoundary::~vtkAtgClipWithBoundary()
{
}

vtkAtgClipWithBoundary::boundIntersectVec const& vtkAtgClipWithBoundary::getSelfIntersections() const
{
    return bi;
}

int vtkAtgClipWithBoundary::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // first object: grid (block model)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        case 1:
            // second object: topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 0);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipWithBoundary::FillOutputPortInformation(int port, vtkInformation* info)
{
    (void)port;

    // block model
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
    return 1;
}

int vtkAtgClipWithBoundary::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    (void)request;

    // primarily we do not have a problem
    bi.clear();

    // input port 0: the input unstructured grid
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputVector[0]);
    if(nullptr == inputGrid)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithBoundary: No valid input block model found\n");
        return 1;
    }

    // input port 1: the input poly data object
    vtkPolyData* inputPoly = vtkPolyData::GetData(inputVector[1]);
    if(nullptr == inputPoly)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithBoundary: No valid input geometry found\n");
        return 1;
    }

    // clean and normalize the "raw boundary" poly data
    // - merge line segments with common end points
    // - close open polylines
    // - remove polygons with less than 3 points
    vtkSmartPointer<vtkAtgCleanNormBoundary> cleanBound = vtkSmartPointer<vtkAtgCleanNormBoundary>::New();
    cleanBound->SetClosePolygons(true);
    cleanBound->SetInputData(inputPoly);
    cleanBound->Update();
    if(nullptr != cleanBound->GetErrorMsg())
    {
        std::string msg(cleanBound->GetErrorMsg());
        vtkOutputWindowDisplayWarningText(("AtgClipWithBoundary: " + msg + "\n").c_str());
        return 1;
    }

    // boundary polyline
    vtkSmartPointer<vtkPolyData> boundPoly = vtkSmartPointer<vtkPolyData>::New();
    boundPoly->ShallowCopy(cleanBound->GetOutput());

    // generate a new VolFactor attribute and either copy old one or fill with all 100
    std::string volFactorName = utilNormNames::getName(utilNormNames::VOLFACTOR);
    vtkSmartPointer<vtkDoubleArray> volFactorArr = vtkSmartPointer<vtkDoubleArray>::New();
    volFactorArr->SetName(volFactorName.c_str());
    vtkDataArray* oldVolFactorArr = inputGrid->GetCellData()->GetArray(volFactorName.c_str());
    if(nullptr == oldVolFactorArr)
    {
        volFactorArr->SetNumberOfTuples(inputGrid->GetNumberOfCells());
        volFactorArr->Fill(100.);
    }
    else
    {
        volFactorArr->DeepCopy(oldVolFactorArr);
    }

    // generate a temporary grid and add the volume factor array
    vtkSmartPointer<vtkUnstructuredGrid> grid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    grid->ShallowCopy(inputGrid);
    grid->GetCellData()->AddArray(volFactorArr);

    // generate multi-polygon structure from boundary polygons
    zfbMultiPolygon bBoundPoly;
    vtkPoints* pts = boundPoly->GetPoints();
    unsigned long validBounds = 0;
    for(vtkIdType p = 0; (p < boundPoly->GetNumberOfCells()) && bi.empty(); ++p)
    {
        vtkCell* cell = boundPoly->GetCell(p);
        if((nullptr == cell) && (2 < cell->GetPointIds()->GetNumberOfIds()))
            continue;

        // collect the points into a vector
        std::vector<zfbPair> plist;
        for(vtkIdType n = 0; n < cell->GetPointIds()->GetNumberOfIds(); ++n)
        {
            double pt[3];
            pts->GetPoint(cell->GetPointId(static_cast<int>(n)), pt);
            plist.push_back(zfbPair({pt[0], pt[1]}));
        }

        // close if not the case yet
        vtkIdType n0 = cell->GetPointId(0);
        if(n0 != cell->GetPointId(static_cast<int>(cell->GetPointIds()->GetNumberOfIds() - 1)))
            plist.push_back(plist[0]);

        // we do not want to deal with degenerate rings that are not at least a triangle
        // note: such "spikes" would generate a crash, caused by a "self-intersection", but
        //   the below self-intersectin test does not even handle this case correctly!
        //   Anyway, we avoid it from the beginning because we ignore this degenerate case.
        if(3 > cell->GetPointIds()->GetNumberOfIds())
            continue;
        ++validBounds;

        // generate a "ring" from the point vector
        zfbRing ring(plist.begin(), plist.end());

        // check for self-intersections
        typedef bg::point_type<zfbPolygon>::type point_type;
        typedef boost::geometry::detail::overlay::turn_info<point_type, boost::geometry::segment_ratio<double> > TurnInfoType;
        bg::detail::no_rescale_policy robust_policy;
        bg::detail::self_get_turn_points::no_interrupt_policy interrupt_policy;
        std::vector<TurnInfoType> turns;

        // a new parameter was introduced "somewhere" - not exactly sure which version!
#if BOOST_VERSION > 106600
        typename bg::strategy::intersection::services::default_strategy<typename bg::cs_tag<zfbRing>::type>::type ring_strategy;
        boost::geometry::self_turns<boost::geometry::detail::overlay::assign_null_policy>(ring, ring_strategy, robust_policy, turns, interrupt_policy);
#else
        boost::geometry::self_turns<boost::geometry::detail::overlay::assign_null_policy>(ring, robust_policy, turns, interrupt_policy);
#endif

        if(0 < turns.size())
        {
            for(unsigned long t = 0; t < turns.size(); ++t)
            {
                unsigned long seg = turns[t].operations[0].seg_id.segment_index;
                boundCoord2 coord(ring.at(seg).get<0>(), ring.at(seg).get<1>());
                bi.push_back(coord);
            }
        }

        // only continue if we found no self-intersection
        else
        {
            // check orientation and reverse if necessary
            double ar = bg::area(ring);
            if(ar < 0.)
                bg::reverse(ring);

            // use the "ring" as the "exterior ring" of a "polygon"
            // note: other rings would be holes and have to turn the other way round
            zfbPolygon poly;
            bg::exterior_ring(poly) = ring;

            // append to the multi-polygon
            bBoundPoly.push_back(poly);
        }
    }

    // report if we had a self-intersection
    if(!bi.empty())
    {
        std::ostringstream ostr;
        ostr << "AtgClipWithBoundary: Self intersections found in the boundary near coordinates" << std::endl;
        for(auto c = bi.begin(); c != bi.end(); c++)
            ostr << "- " << std::setprecision(2) << std::fixed << c->first << " " << c->second << std::endl;
        vtkOutputWindowDisplayWarningText(ostr.str().c_str());
        return 1;
    }

    // report if we did not find any valid boundary, i.e. all were degenerate
    if(0 == validBounds)
    {
        vtkOutputWindowDisplayWarningText("AtgClipWithBoundary: No valid boundary found\n");
        return 1;
    }

    // if we have tonnage attributes, we want to handle them as well
    std::vector<vtkDataArray*> tonnArrs;
    std::vector<vtkSmartPointer<vtkDoubleArray>> newTonnArrs;
    std::vector<int> tonnArrIds(std::initializer_list<int>(
    {
        utilNormNames::KTONS,
        utilNormNames::TONS
    }));
    for(auto it = tonnArrIds.begin(); it != tonnArrIds.end(); ++it)
    {
        vtkDataArray* tonnArr = grid->GetCellData()->GetArray(utilNormNames::getName(*it).c_str());
        if(nullptr != tonnArr)
        {
            tonnArrs.push_back(tonnArr);
            auto newTonnArr = vtkSmartPointer<vtkDoubleArray>::New();
            newTonnArr->SetName(tonnArr->GetName());
            newTonnArr->SetNumberOfTuples(tonnArr->GetNumberOfTuples());
            newTonnArrs.push_back(newTonnArr);
        }
    }

    // optimization of the following: take the "ring" of every block as a key into
    // a map of already calculated zone values - to avoid recalculating the intersection
    // at every block level again and again
    std::map<zfbRing, double, zfbRingLess> zoneValMap;

    // handle all cells of the block model
    pts = inputGrid->GetPoints();
    for(vtkIdType b = 0; b < inputGrid->GetNumberOfCells(); ++b)
    {
        vtkHexahedron* blk = vtkHexahedron::SafeDownCast(inputGrid->GetCell(b));
        if(nullptr == blk)
            continue;

        // collect the first 5 points of the hexahedron into a vector
        std::vector<zfbPair> plist;
        for(vtkIdType n = 0; n < 5; ++n)
        {
            double pt[3];
            pts->GetPoint(blk->GetPointId(static_cast<int>(n)), pt);
            plist.push_back(zfbPair({pt[0], pt[1]}));
        }

        // generate a ring from the cell points
        zfbRing ring(plist.begin(), plist.end());

        // initial volume factor
        double vf = 100.;

        // see if we already have the volume factor...
        auto zit = zoneValMap.find(ring);
        if(zoneValMap.end() != zit)
        {
            vf = zit->second;
        }

        // ...or else calculate and remember it
        else
        {
            // the block (cell) points are always anti-clockwise so we need
            // to reverse in any case
            // note that also this square we must wrap into a multi-polygon because
            //   otherwise the intersection function does not work properly
            bg::reverse(ring);
            zfbPolygon poly;
            bg::exterior_ring(poly) = ring;
            zfbMultiPolygon bCellPoly;
            bCellPoly.push_back(poly);

            // calculate the intersection multi-polygon of boundary and block
            zfbMultiPolygon bResPoly;
            bg::intersection(bBoundPoly, bCellPoly, bResPoly);

            // calculate the covered proportion (percent) for the zone value
            vf = 100. * bg::area(bResPoly) / bg::area(ring);
            zoneValMap[ring] = vf;
        }

        // deal with previous value
        double prevVf = volFactorArr->GetValue(b);
        if(RespectPrevHor)
            // if the cut goes in the same direction, we keep the minimum of the two
            vf = std::min<double>(vf, prevVf);
        else
            // otherwise we multiply the two values
            vf = vf * prevVf / 100.;

        // assign the resulting volume factor
        volFactorArr->SetValue(b, vf);

        // handle the tonnage attributes
        auto nit = newTonnArrs.begin();
        for(auto it = tonnArrs.begin(); it != tonnArrs.end(); ++it)
        {
            vtkDataArray* arr = *it;
            auto narr = *nit;
            narr->SetVariantValue(b, vf * arr->GetVariantValue(b).ToDouble() / prevVf);
        }
    }

    // replace old tonnage arrays with new ones
    for(auto nit = newTonnArrs.begin(); nit != newTonnArrs.end(); ++nit)
        grid->GetCellData()->AddArray(*nit);

    // generate an INDEX selection for extracting the remaining blocks
    // note: at the same time we fill a bitset in order to adapt the nb attribute if existing
    //   and before doing the renumbering further down
    boost::dynamic_bitset<> remaining;
    remaining.resize(static_cast<boost::dynamic_bitset<>::size_type>(grid->GetNumberOfCells()), false);
    vtkSmartPointer<vtkLongLongArray> selIds = vtkSmartPointer<vtkLongLongArray>::New();
    selIds->SetName("__SELECTION__");
    vtkSmartPointer<vtkSelection> sel = vtkSmartPointer<vtkSelection>::New();
    vtkSmartPointer<vtkSelectionNode> sn = vtkSmartPointer<vtkSelectionNode>::New();
    sn->SetFieldType(vtkSelectionNode::CELL);
    sn->SetContentType(vtkSelectionNode::INDICES);
    sn->SetSelectionList(selIds);
    sel->AddNode(sn);

    // select all blocks where the volume factor is higher than indicated limit
    for(vtkIdType b = 0; b < volFactorArr->GetNumberOfTuples(); ++b)
    {
        if(MinVolFactorRetained <= volFactorArr->GetValue(b))
        {
            selIds->InsertNextValue(b);
            remaining.set(static_cast<boost::dynamic_bitset<>::size_type>(b));
        }
    }

    // update neighbor indices in such a way that indices of blocks that are not remaining
    // will not appear any more
    std::unique_ptr<utilUpdateNeighborsForRemainingBlocks> updateNb(new utilUpdateNeighborsForRemainingBlocks(grid));
    updateNb->updateForRemaining(remaining);

    // do the extraction using the selection
    // note: the assumption is that this will generate a new model ("deep extract")
    vtkSmartPointer<vtkExtractSelection> extractSel = vtkSmartPointer<vtkExtractSelection>::New();
    extractSel->SetInputData(grid);
    extractSel->SetInputData(1, sel);
    extractSel->Update();

    // output the filtered data
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->ShallowCopy(extractSel->GetOutput());

    // update the block ids and neighbor indices, if present
    std::unique_ptr<utilRenumberBlocks> renumBlocks(new utilRenumberBlocks(outGrid));
    renumBlocks->renumberBlocksAndNeighbors();

    return 1;
}

void vtkAtgClipWithBoundary::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
