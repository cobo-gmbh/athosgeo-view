/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipWithBoundary.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgClipWithBoundary_h
#define vtkAtgClipWithBoundary_h

#include <vector>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgClipWithBoundaryModuleModule.h>

class vtkPoints;
class vtkPolyData;

class ATGCLIPWITHBOUNDARYMODULE_EXPORT vtkAtgClipWithBoundary: public vtkUnstructuredGridAlgorithm
{
public:

    typedef std::pair<double, double> boundCoord2;
    typedef std::vector<boundCoord2> boundIntersectVec;

    static vtkAtgClipWithBoundary* New();
    vtkTypeMacro(vtkAtgClipWithBoundary, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetMacro(MinVolFactorRetained, double)
    vtkGetMacro(MinVolFactorRetained, double)

    vtkSetMacro(RespectPrevHor, bool)
    vtkGetMacro(RespectPrevHor, bool)

    boundIntersectVec const& getSelfIntersections() const;

protected:

    vtkAtgClipWithBoundary();
    ~vtkAtgClipWithBoundary() override;

    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*) override;
    int FillInputPortInformation(int port, vtkInformation* info) override;
    int FillOutputPortInformation(int port, vtkInformation* info) override;

private:

    // copy constructor and assignment not implemented
    vtkAtgClipWithBoundary(vtkAtgClipWithBoundary const&);
    void operator=(vtkAtgClipWithBoundary const&);

    // minimum volume factor to be retained
    double MinVolFactorRetained;

    // if the model was already cut horizontally, this should be checked to avoid cutting
    // the same volume twice
    bool RespectPrevHor;

    // coordinates close to self-intersections in the boundary
    boundIntersectVec bi;

};

#endif
