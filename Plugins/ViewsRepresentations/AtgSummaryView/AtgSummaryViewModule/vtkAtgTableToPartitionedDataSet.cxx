/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToPartitionedDataSet.cxx

   Copyright (c) 2023 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkPartitionedDataSet.h>
#include <vtkPartitionedDataSetCollection.h>
#include <vtkConvertToPartitionedDataSetCollection.h>
#include <vtkDataObject.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTableToPartitionedDataSet.h>

vtkStandardNewMacro(vtkAtgTableToPartitionedDataSet)

vtkAtgTableToPartitionedDataSet::vtkAtgTableToPartitionedDataSet()
{
}

vtkAtgTableToPartitionedDataSet::~vtkAtgTableToPartitionedDataSet()
{
}

int vtkAtgTableToPartitionedDataSet::RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*)
{
    return 1;
}

int vtkAtgTableToPartitionedDataSet::RequestData(vtkInformation* request,
                                                 vtkInformationVector** inputVector,
                                                 vtkInformationVector* outputVector)
{
    (void)request;

    // get the input table
    vtkTable* input = vtkTable::GetData(inputVector[0], 0);
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("AtgTableToPaAtgWriteValue: No value or string specified\nrtitionedDataSet: no input table found");
        return 1;
    }

    // get the output data object
    vtkPartitionedDataSet* output = vtkPartitionedDataSet::GetData(outputVector, 0);

    // as a first step, we bring the table into a vtkPartitionedDataSetCollection
    auto convert = vtkSmartPointer<vtkConvertToPartitionedDataSetCollection>::New();
    convert->SetInputDataObject(input);
    convert->Update();

    // from the collection we extract the first data object, assuming that from one
    // single input table we would never get more than that
    vtkPartitionedDataSetCollection* pdsc = convert->GetOutput();
    output->ShallowCopy(pdsc->GetPartitionedDataSet(0));

    return 1;
}

int vtkAtgTableToPartitionedDataSet::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            break;
    }
    return 1;
}

int vtkAtgTableToPartitionedDataSet::FillOutputPortInformation(int port, vtkInformation* info)
{
    (void)port;

    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPartitionedDataSet");
    return 1;
}

void vtkAtgTableToPartitionedDataSet::PrintSelf(std::ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
