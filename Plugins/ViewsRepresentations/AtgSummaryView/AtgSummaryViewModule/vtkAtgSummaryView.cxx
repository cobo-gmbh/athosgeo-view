/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummaryView.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkSortedTableStreamer.h>
#include <vtkMarkSelectedRows.h>
#include <vtkReductionFilter.h>
#include <vtkTable.h>
#include <vtkClientServerMoveData.h>
#include <vtkAlgorithmOutput.h>
#include <vtkCommand.h>
#include <vtkCommunicator.h>
#include <vtkPartitionedDataSet.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTableViewInternal.h>
#include <vtkAtgTableToPartitionedDataSet.h>
#include <vtkAtgSummaryRepresentation.h>
#include <vtkAtgSummarizeAttributes.h>
#include <vtkAtgSummaryView.h>

vtkStandardNewMacro(vtkAtgSummaryView)

void vtkAtgSummaryView::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}

vtkAtgSummaryView::vtkAtgSummaryView()
:   Superclass(),
    SummarizeFilter(vtkAtgSummarizeAttributes::New()),
    PDSConvertFilter(vtkAtgTableToPartitionedDataSet::New()),
    UsePit(false),
    UseCategory(false),
    IncludeCementModuli(true),
    ApplyToSelection(false),
    Tonnage(nullptr),
    Pit(nullptr),
    Category(nullptr)
{
    // This is for the purpose to ensure that the table is not reacting to selections:
    // this would not make sense because the rows to not correspond to items any more.
    // Still this view knows the option to work on all or selected items only: this is
    // handled with the ApplyToSelection property in the AtgSummaryRepresentation
    SetShowExtractedSelection(true);

    SummarizeFilter->SetKeepQuiet();
}

vtkAtgSummaryView::~vtkAtgSummaryView()
{
    SetCategory(nullptr);
    SetPit(nullptr);
    SetTonnage(nullptr);
    SummarizeFilter->Delete();
    PDSConvertFilter->Delete();
}

int vtkAtgSummaryView::StreamToClient()
{
    vtkAtgSummaryRepresentation* cur =
            vtkAtgSummaryRepresentation::SafeDownCast(Internals->ActiveRepresentation);
    if(cur == nullptr)
    {
        if(NumberOfRows > 0)
        {
            // BUG #13231: It implies that we had some data in the previous render and
            // we don't have it anymore. We need to trigger "UpdateDataEvent" to
            // ensure that the UI can update the rows and columns correctly.
            NumberOfRows = 0;
            InvokeEvent(vtkCommand::UpdateDataEvent);
        }

        return 0;
    }

    // see if any property has changed
    // note: comparing with ::strcmp is a problem if GetTonnage returns a nullptr
    std::string tonn((nullptr != GetTonnage()) ? GetTonnage() : ""),
                ctonn((nullptr != cur->GetTonnage()) ? cur->GetTonnage() : ""),
                pt((nullptr != GetPit()) ? GetPit() : ""),
                cpt((nullptr != cur->GetPit()) ? cur->GetPit() : ""),
                cat((nullptr != GetCategory()) ? GetCategory() : ""),
                ccat((nullptr != cur->GetCategory()) ? cur->GetCategory() : "");
    SomethingUpdated = SomethingUpdated ||
            (tonn != ctonn) ||
            (GetUsePit() != cur->GetUsePit()) ||
            (pt != cpt) ||
            (GetUseCategory() != cur->GetUseCategory()) ||
            (cat != ccat) ||
            (GetIncludeCementModuli() != cur->GetIncludeCementModuli()) ||
            (GetApplyToSelection() != cur->GetApplyToSelection());

    // we need to refresh the attributes from the representation - because only
    // there we receive them from the user interface (property panel)
    SetTonnage(cur->GetTonnage());
    SetUsePit(cur->GetUsePit());
    SetPit(cur->GetPit());
    SetUseCategory(cur->GetUseCategory());
    SetCategory(cur->GetCategory());
    SetIncludeCementModuli(cur->GetIncludeCementModuli());
    SetApplyToSelection(cur->GetApplyToSelection());

    // From the active representation obtain the data producer that
    // need to be streamed to the client.
    // If we want to work with selected blocks only, we take the extracted data
    // producer instead: that way we do not have to deal with selections any more.
    vtkAlgorithmOutput* dataPort = cur->GetApplyToSelection() ?
                cur->GetExtractedDataProducer() :
                cur->GetDataProducer();

    // here we insert the summarize filter - instead of the selection marker

    // pass the options further to the summarize filter
    SummarizeFilter->SetTonnage(cur->GetTonnage());
    SummarizeFilter->SetUsePit(cur->GetUsePit());
    SummarizeFilter->SetPit(cur->GetPit());
    SummarizeFilter->SetUseCategory(cur->GetUseCategory());
    SummarizeFilter->SetCategory(cur->GetCategory());
    SummarizeFilter->SetIncludeCementModuli(cur->GetIncludeCementModuli());

    // insert either the full or extracted data as input to the summarizer
    // BCO 2023-01-19: we need to pass the output of the summarizing filter still
    //   through a converter filter to make it a vtkPartitionedDataSet
    SummarizeFilter->SetInputConnection(0, dataPort);
    PDSConvertFilter->SetInputConnection(SummarizeFilter->GetOutputPort());
    TableStreamer->SetInputConnection(PDSConvertFilter->GetOutputPort());

    // check if the number of rows has changed
    vtkTypeUInt64 num_rows = 0;
    if(dataPort)
    {
        SummarizeFilter->Update();
        DeliveryFilter->SetInputConnection(ReductionFilter->GetOutputPort());
        num_rows = SummarizeFilter->GetOutput()->GetNumberOfRows();
    }
    else
    {
        DeliveryFilter->RemoveAllInputs();
    }

    AllReduce(num_rows, num_rows, vtkCommunicator::SUM_OP);

    if(NumberOfRows != static_cast<vtkIdType>(num_rows))
        SomethingUpdated = true;
    NumberOfRows = num_rows;

    // trigger a repaint
    if(SomethingUpdated)
    {
        ClearCache();
        InvokeEvent(vtkCommand::UpdateDataEvent);
    }

    return 1;
}

void vtkAtgSummaryView::SetShowExtractedSelection(bool selOnly)
{
    // make sure the selectionOnly flag remains on - no matter what this function tells us!
    selOnly; // no "unused" complaint
    Superclass::SetShowExtractedSelection(true);
}
