/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummaryView.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgSummaryView_H
#define vtkAtgSummaryView_H

#include <vtkSetGet.h>
#include <vtkAtgTableView.h>
#include <AtgSummaryViewModuleModule.h>

class vtkAtgSummarizeAttributes;
class vtkAtgTableToPartitionedDataSet;

class ATGSUMMARYVIEWMODULE_EXPORT vtkAtgSummaryView: public vtkAtgTableView
{
public:

    static vtkAtgSummaryView* New();
    vtkTypeMacro(vtkAtgSummaryView, vtkAtgTableView)
    void PrintSelf(ostream& os, vtkIndent indent) override;

    // override the function of the base class and make sure that the value
    // always remains "on" (true)
    void SetShowExtractedSelection(bool selOnly);

    vtkSetStringMacro(Tonnage)
    vtkGetStringMacro(Tonnage)

    vtkSetMacro(UsePit, bool)
    vtkGetMacro(UsePit, bool)

    vtkSetStringMacro(Pit)
    vtkGetStringMacro(Pit)

    vtkSetMacro(UseCategory, bool)
    vtkGetMacro(UseCategory, bool)

    vtkSetStringMacro(Category)
    vtkGetStringMacro(Category)

    vtkSetMacro(IncludeCementModuli, bool)
    vtkGetMacro(IncludeCementModuli, bool)

    vtkSetMacro(ApplyToSelection, bool)
    vtkGetMacro(ApplyToSelection, bool)

protected:

    vtkAtgSummaryView();
    ~vtkAtgSummaryView() override;

    /**
    * On render streams all the data from the processes to the client.
    * Returns 0 on failure.
    * Note: Was removed from update because you can't call update()
    * while in an update
    */
    virtual int StreamToClient() override;

    vtkAtgSummarizeAttributes* SummarizeFilter;
    vtkAtgTableToPartitionedDataSet* PDSConvertFilter;

    bool UsePit,
         UseCategory,
         IncludeCementModuli,
         ApplyToSelection;
    char *Tonnage,
         *Pit,
         *Category;

private:

    vtkAtgSummaryView(const vtkAtgSummaryView&) = delete;
    void operator=(const vtkAtgSummaryView&) = delete;

};

#endif
