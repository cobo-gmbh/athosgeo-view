/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCategoryRepresentation.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkObjectFactory.h>
#include <vtkAtgCategoryRepresentationGeometryFilter.h>
#include <vtkAtgCategoryRepresentation.h>

vtkStandardNewMacro(vtkAtgCategoryRepresentation)

vtkAtgCategoryRepresentation::vtkAtgCategoryRepresentation()
{
    // replace the geometry filter by our own that applies
    // a texture coordinate system to the geometry before sending it to
    // the further processing
    GeometryFilter->Delete();
    GeometryFilter = vtkAtgCategoryRepresentationGeometryFilter::New();

    SetupDefaults();
}

vtkAtgCategoryRepresentation::~vtkAtgCategoryRepresentation()
{
}

void vtkAtgCategoryRepresentation::SetPortId(char const* PortId)
{
    // get the geometry filter that is set in the constructor, so can be
    // assumed to exist
    vtkAtgCategoryRepresentationGeometryFilter* gf =
            vtkAtgCategoryRepresentationGeometryFilter::SafeDownCast(GeometryFilter);
    gf->SetPortId(PortId);

    Modified();

    // with this RequestData is called
    MarkModified();
}

void vtkAtgCategoryRepresentation::SetCategory(char const* Category)
{
    // get the geometry filter that is set in the constructor, so can be
    // assumed to exist
    vtkAtgCategoryRepresentationGeometryFilter* gf =
            vtkAtgCategoryRepresentationGeometryFilter::SafeDownCast(GeometryFilter);
    gf->SetCategory(Category);

    Modified();

    // with this RequestData is called
    MarkModified();
}

void vtkAtgCategoryRepresentation::SetValue(char const* Value)
{
    // get the geometry filter that is set in the constructor, so can be
    // assumed to exist
    vtkAtgCategoryRepresentationGeometryFilter* gf =
            vtkAtgCategoryRepresentationGeometryFilter::SafeDownCast(GeometryFilter);
    gf->SetValue(Value);

    Modified();

    // with this RequestData is called
    MarkModified();
}

void vtkAtgCategoryRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
