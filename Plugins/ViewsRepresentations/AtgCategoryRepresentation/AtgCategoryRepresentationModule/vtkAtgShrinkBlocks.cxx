/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgShrinkBlocks.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <cmath>
#include <string>
#include <algorithm>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTable.h>
#include <vtkDoubleArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <vtkAtgTableToBlocks.h>
#include <vtkAtgShrinkBlocks.h>

vtkStandardNewMacro(vtkAtgShrinkBlocks)

vtkAtgShrinkBlocks::vtkAtgShrinkBlocks()
:   GhostsOnly(true)
{
}

vtkAtgShrinkBlocks::~vtkAtgShrinkBlocks()
{
}

int vtkAtgShrinkBlocks::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgShrinkBlocks::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model with shrinked blocks
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgShrinkBlocks::RequestData(vtkInformation* request,
                                    vtkInformationVector** inputVector,
                                    vtkInformationVector* outputVector)
{
    TRACK_FUNCTION_TIME

    // get the input grid
    vtkUnstructuredGrid* inGrid = vtkUnstructuredGrid::GetData(inputVector[0], 0);
    if(nullptr == inGrid)
        return 1; // should not happen - but also no crash

    // if the input grid is empty, this is valid, but generates no output
    if(0 == inGrid->GetNumberOfCells())
        return 1;

    // get the ghost array
    vtkUnsignedCharArray* ghostArr = GhostsOnly ? inGrid->GetCellGhostArray() : nullptr;

    // get the output grid
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);

    // generate a temporary table from the input cell data
    vtkSmartPointer<vtkTable> cellTab = vtkSmartPointer<vtkTable>::New();
    cellTab->SetRowData(inGrid->GetCellData());

    TRACK_ELAPSED("Temp table generated")

    // get the input size array
    std::string sizeName = utilNormNames::getName(utilNormNames::BLOCKSIZE);
    vtkDataArray* inSizeArr = vtkDataArray::SafeDownCast(cellTab->GetColumnByName(sizeName.c_str()));
    if(nullptr == inSizeArr)
    {
        vtkOutputWindowDisplayWarningText("AtgShrinkBlocks: No Block Size attribute found");
        return 1;
    }

    // now shrink all the cells by slightly reducing the cell sizes - respecting
    // also the GhostsOnly flag
    // note that we are iterating over the values, including all components
    vtkSmartPointer<vtkDoubleArray> outSizeArr = vtkSmartPointer<vtkDoubleArray>::New();
    outSizeArr->DeepCopy(inSizeArr);
    for(vtkIdType r = 0; r < outSizeArr->GetNumberOfTuples(); ++r)
    {
        if(!GhostsOnly || (ghostArr->GetValue(r) == vtkCellData::DUPLICATECELL))
        {
            for(vtkIdType c = 0; c < outSizeArr->GetNumberOfComponents(); ++c)
                outSizeArr->SetComponent(r, c, 0.9 * outSizeArr->GetComponent(r, c));
        }
    }
    cellTab->AddColumn(outSizeArr);

    TRACK_ELAPSED("Cell size values reduced")

    // now we generate a new grid with smaller blocks
    vtkSmartPointer<vtkAtgTableToBlocks> tabToBlocks = vtkSmartPointer<vtkAtgTableToBlocks>::New();
    tabToBlocks->SetInputData(cellTab);
    tabToBlocks->Update();

    TRACK_ELAPSED("Table to blocks")

    // that should be it!
    outGrid->ShallowCopy(tabToBlocks->GetOutput());

    return 1;
}

int vtkAtgShrinkBlocks::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgShrinkBlocks::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
