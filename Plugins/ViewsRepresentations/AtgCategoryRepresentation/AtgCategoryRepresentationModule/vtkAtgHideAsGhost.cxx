/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgHideAsGhost.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <utilTrackDefs.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkStringArray.h>
#include <vtkDataArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkSmartPointer.h>
#include <vtkLongLongArray.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgHideAsGhost.h>

vtkStandardNewMacro(vtkAtgHideAsGhost)

vtkAtgHideAsGhost::vtkAtgHideAsGhost()
:   Category(nullptr),
    Value(nullptr)
{
}

vtkAtgHideAsGhost::~vtkAtgHideAsGhost()
{
    SetCategory(nullptr);
    SetValue(nullptr);
}

int vtkAtgHideAsGhost::RequestData(vtkInformation* request,
                                   vtkInformationVector** inputVector,
                                   vtkInformationVector* outputVector)
{
    TRACK_FUNCTION_TIME

    // first of all we need a category and a value
    // note: if not available, we output an empty grid
    if((nullptr == Category) || (nullptr == Value))
        return 1;

    // get the input grid
    vtkUnstructuredGrid* inGrid = vtkUnstructuredGrid::GetData(inputVector[0], 0);
    if(nullptr == inGrid)
        return 1; // should not happen - but also no crash

    // shallow copy to output grid
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    outGrid->ShallowCopy(inGrid);

    // get the cell ghost array
    vtkUnsignedCharArray* ghostArr = outGrid->AllocateCellGhostArray();

    // try to find the category attribute in the cell data, and do nothing
    // if not found
    vtkAbstractArray* catArr = inGrid->GetCellData()->GetAbstractArray(Category);
    if(nullptr == catArr)
        return 1;

    // try down casts
    vtkStringArray* strArr = vtkStringArray::SafeDownCast(catArr);
    vtkDataArray* numArr = vtkDataArray::SafeDownCast(catArr);
    if((nullptr == strArr) && (nullptr == numArr))
        return 1; // should really not happen!!

    // one more test: in case of a numeric array: is our value really numeric?
    double number = 0.;
    if(nullptr != numArr)
    {
        try
        {
            number = std::stod(Value);
        }
        catch(...)
        {
            return 1;
        }
    }
    number = ::floor(number + .5);

    // handle cell by cell
    for(vtkIdType c = 0; c < ghostArr->GetNumberOfTuples(); ++c)
    {
        bool visible = true;

        if(nullptr != strArr)
        {
            std::string val = strArr->GetValue(c);
            boost::trim(val);
            visible = boost::iequals(val, Value);
        }
        else
        {
            double num = ::floor(numArr->GetVariantValue(c).ToDouble() + .5);
            visible = num == number;
        }

        ghostArr->SetValue(c, visible ? 0 : vtkCellData::DUPLICATECELL);
    }

    outGrid->UpdateCellGhostArrayCache();

    return 1;
}
