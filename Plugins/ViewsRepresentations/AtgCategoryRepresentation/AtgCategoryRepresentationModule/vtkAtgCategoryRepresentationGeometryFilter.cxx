/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCategoryRepresentationGeometryFilter.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <atgCategoriesManager.h>
#include <vtkUnstructuredGrid.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgShrinkBlocks.h>
#include <vtkAtgHideAsGhost.h>
#include <vtkAtgCategoryRepresentationGeometryFilter.h>

vtkStandardNewMacro(vtkAtgCategoryRepresentationGeometryFilter)

vtkAtgCategoryRepresentationGeometryFilter::vtkAtgCategoryRepresentationGeometryFilter()
:   Superclass(),
    SelectiveHide(vtkAtgHideAsGhost::New()),
    ShrinkBlocks(vtkAtgShrinkBlocks::New()),
    PortId(nullptr),
    Category(nullptr),
    Value(nullptr),
    GridMTime(0)
{
    ShrinkBlocks->SetGhostsOnly(true);
}

vtkAtgCategoryRepresentationGeometryFilter::~vtkAtgCategoryRepresentationGeometryFilter()
{
    SetValue(nullptr);
    SetCategory(nullptr);
    SetPortId(nullptr);
    ShrinkBlocks->Delete();
    SelectiveHide->Delete();
}

int vtkAtgCategoryRepresentationGeometryFilter::RequestData(vtkInformation* request,
                                                            vtkInformationVector** inputVector,
                                                            vtkInformationVector* outputVector)
{
    // get the input information: we need it for the initial data, and for later
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);

    // get the input data
    // note: we remember this also for setting it back at the end
    // note: if the input is not an unstructured grid we simply pass the job to the superclass
    vtkSmartPointer<vtkUnstructuredGrid> grid =
        vtkUnstructuredGrid::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
    if(!grid)
        return Superclass::RequestData(request, inputVector, outputVector);

    // if we have no category/values info, or if the grid has changed, we refresh
    // that property
    if((GridMTime != grid->GetMTime()) && (nullptr != PortId))
    {
        GridMTime = grid->GetMTime();

        // make sure the change is noted by the property widgets
        std::string currentCat((nullptr != Category) ? Category : ""),
                    currentVal((nullptr != Value) ? Value : "");
        atgCategoriesManager::instance().findCategoriesValues(grid, PortId, currentCat, currentVal);

        // set the values
        if(currentCat.empty() || currentVal.empty())
        {
            SetCategory(nullptr);
            SetValue(nullptr);
        }
        else
        {
            SetCategory(currentCat.c_str());
            SetValue(currentVal.c_str());
        }
    }

    // if there is no category and/or no value available, we tell the reason
    std::string err;
    if(nullptr == Category)
        err = "no category";
    if(nullptr == Value)
    {
        if(!err.empty())
            err += ", ";
        err += "no value";
    }
    if(!err.empty())
    {
        vtkOutputWindowDisplayWarningText((std::string("Categories cannot be displayed: ") + err).c_str());
        return Superclass::RequestData(request, inputVector, outputVector);
    }

    // hide blocks that do not fit the category/value by making them "ghost cells"
    SelectiveHide->SetInputData(grid);
    SelectiveHide->SetCategory(Category);
    SelectiveHide->SetValue(Value);
    SelectiveHide->Update();

    // slightly shrink all ghost cells, so the faces become visible
    // first of all, we shrink the cells of our input grid
    ShrinkBlocks->SetInputData(SelectiveHide->GetOutput());
    ShrinkBlocks->Update();

    // the current output will be the input for the original function call
    inInfo->Set(vtkDataObject::DATA_OBJECT(), ShrinkBlocks->GetOutput());

    // do the job of the parent class as if the filtered grid was the input
    Superclass::RequestData(request, inputVector, outputVector);

    // clean up: set the input back to original
    inInfo->Set(vtkDataObject::DATA_OBJECT(), grid);

    return 1;
}

void vtkAtgCategoryRepresentationGeometryFilter::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
