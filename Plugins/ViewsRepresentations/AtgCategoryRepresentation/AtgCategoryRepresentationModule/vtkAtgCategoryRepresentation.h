/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCategoryRepresentation.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgCategoryRepresentation_H
#define vtkAtgCategoryRepresentation_H

#include <string>
#include <vtkGeometryRepresentationWithFaces.h>
#include <AtgCategoryRepresentationModuleModule.h>

class vtkInformation;

class ATGCATEGORYREPRESENTATIONMODULE_EXPORT vtkAtgCategoryRepresentation:
        public vtkGeometryRepresentationWithFaces
{
public:

    static vtkAtgCategoryRepresentation* New();
    vtkTypeMacro(vtkAtgCategoryRepresentation, vtkGeometryRepresentationWithFaces)
    virtual void PrintSelf(ostream& os, vtkIndent indent) override;

    // these calls are directly passed on to the geometry filter

    // a key is derived from the port id to address the proper categories/values
    // from the categories manager
    void SetPortId(char const* PortId);

    // the current category to be used for the representation
    void SetCategory(char const* Category);

    // the value within the category to be used
    void SetValue(char const* Value);

protected:

    vtkAtgCategoryRepresentation();
    virtual ~vtkAtgCategoryRepresentation() override;

private:

    vtkAtgCategoryRepresentation(const vtkAtgCategoryRepresentation&) = delete;
    void operator=(const vtkAtgCategoryRepresentation&) = delete;

};

#endif
