/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummaryRepresentation.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkPVView.h>
#include <vtkInformationRequestKey.h>
#include <vtkDataObject.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTableView.h>
#include <vtkAtgSummaryRepresentation.h>

vtkStandardNewMacro(vtkAtgSummaryRepresentation)

void vtkAtgSummaryRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}

int vtkAtgSummaryRepresentation::ProcessViewRequest(vtkInformationRequestKey* request,
                                                    vtkInformation* inInfo,
                                                    vtkInformation* outInfo)
{
    if(!GetVisibility())
        return 0;

    if(request == vtkPVView::REQUEST_UPDATE())
    {
        if(vtkAtgTableView* view = vtkAtgTableView::SafeDownCast(inInfo->Get(vtkPVView::VIEW())))
        {
            SetGenerateCellConnectivity(view->GetGenerateCellConnectivity());
            SetFieldAssociation(view->GetFieldAssociation());
        }
    }

    // skip here the direct parent because we are replacing it
    return Superclass::Superclass::ProcessViewRequest(request, inInfo, outInfo);
}

vtkAtgSummaryRepresentation::vtkAtgSummaryRepresentation()
:   Superclass(),
    UsePit(false),
    UseCategory(false),
    IncludeCementModuli(true),
    ApplyToSelection(false),
    Tonnage(nullptr),
    Pit(nullptr),
    Category(nullptr)
{
}

vtkAtgSummaryRepresentation::~vtkAtgSummaryRepresentation()
{
    SetCategory(nullptr);
    SetPit(nullptr);
    SetTonnage(nullptr);
}
