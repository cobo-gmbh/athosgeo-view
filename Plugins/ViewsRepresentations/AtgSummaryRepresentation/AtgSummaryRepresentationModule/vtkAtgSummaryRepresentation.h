/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummaryRepresentation.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgSummaryRepresentation_H
#define vtkAtgSummaryRepresentation_H

#include <vtkSetGet.h>
#include <vtkSpreadSheetRepresentation.h>
#include <AtgSummaryRepresentationModuleModule.h>

class vtkAtgSummarizeAttributes;
class vtkInformationRequestKey;
class vtkInformation;
class vtkInformationVector;

// The purpose of this representation is only two things:
// - get the properties from the user interface and pass them to the
//   vtk AtgSummaryView class
// - make sure that in ProcessViewRequest that view class and not the
//   original spreadsheet view is used

class ATGSUMMARYREPRESENTATIONMODULE_EXPORT vtkAtgSummaryRepresentation: public vtkSpreadSheetRepresentation
{
public:

    static vtkAtgSummaryRepresentation* New();
    vtkTypeMacro(vtkAtgSummaryRepresentation, vtkSpreadSheetRepresentation);
    virtual void PrintSelf(ostream& os, vtkIndent indent) override;

    vtkSetStringMacro(Tonnage)
    vtkGetStringMacro(Tonnage)

    vtkSetMacro(UsePit, bool)
    vtkGetMacro(UsePit, bool)

    vtkSetStringMacro(Pit)
    vtkGetStringMacro(Pit)

    vtkSetMacro(UseCategory, bool)
    vtkGetMacro(UseCategory, bool)

    vtkSetStringMacro(Category)
    vtkGetStringMacro(Category)

    vtkSetMacro(IncludeCementModuli, bool)
    vtkGetMacro(IncludeCementModuli, bool)

    vtkSetMacro(ApplyToSelection, bool)
    vtkGetMacro(ApplyToSelection, bool)

    /**
     * Overridden to update state of `GenerateCellConnectivity` and `FieldAssociation`
     * which is specified on the view.
     */
    virtual int ProcessViewRequest(vtkInformationRequestKey* request,
                                   vtkInformation* inInfo, vtkInformation* outInfo) override;

protected:

    vtkAtgSummaryRepresentation();
    virtual ~vtkAtgSummaryRepresentation() override;

    // options for the summarization
    bool UsePit,
         UseCategory,
         IncludeCementModuli,
         ApplyToSelection;
    char *Tonnage,
         *Pit,
         *Category;

private:

    vtkAtgSummaryRepresentation(const vtkAtgSummaryRepresentation&) = delete;
    void operator=(const vtkAtgSummaryRepresentation&) = delete;

};

#endif
