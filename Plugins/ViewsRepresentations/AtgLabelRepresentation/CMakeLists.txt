set(plugin_name AtgLabelRepresentation)

paraview_add_plugin(${plugin_name}
  VERSION "1.2.0"
  SERVER_MANAGER_XML plugin.xml
  XML_DOCUMENTATION OFF)

# the only purpose of this is to bring the paraview.plugin file into QtCreator
add_custom_target(${plugin_name}_plugin_spec
  SOURCES
    "${CMAKE_CURRENT_SOURCE_DIR}/paraview.plugin"
    "${CMAKE_CURRENT_SOURCE_DIR}/plugin.xml")
