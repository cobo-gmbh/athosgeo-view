<!--
 Program: AthosGEO
 Copyright (c) 2020 cobo GmbH
 All rights reserved.
 AthosGEO View is a free software based on ParaView; you can redistribute
 and/or modify it under the same terms as ParaView
 Code adapted from an example provided by Sébastien Jourdain
-->

<ServerManagerConfiguration>
  <ProxyGroup name="representations">
    <RepresentationProxy name="AtgLabelRepresentation"
                         class="vtkDataLabelRepresentation"
                         processes="client|renderserver|dataserver">

      <InputProperty command="SetInputConnection"
                     name="Input">
        <InputArrayDomain name="input_array_any"></InputArrayDomain>
      </InputProperty>

      <IntVectorProperty command="SetVisibility"
                         default_values="1"
                         name="Visibility"
                         number_of_elements="1"
                         panel_visibility="never">
        <BooleanDomain name="bool" />
        <Documentation>Set the visibility for this representation.</Documentation>
      </IntVectorProperty>

      <StringVectorProperty command="SetPointFieldDataArrayName"
                            name="PointFieldDataArrayName"
                            default_values="Labels"
                            number_of_elements="1"
                            panel_visibility="default">
        <!-- the purpose of defining a default as "Labels" is the sampling reader
             that automatically generates a second output with points and an
             attribute with that name -->
        <ArrayListDomain input_domain_name="input_array"
                         name="array_list">
          <RequiredProperties>
            <Property function="Input"
                      name="Input" />
          </RequiredProperties>
        </ArrayListDomain>
      </StringVectorProperty>

      <IntVectorProperty command="SetPointLabelVisibility"
                         default_values="1"
                         name="PointLabelVisibility"
                         number_of_elements="1"
                         panel_visibility="default">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetMaximumNumberOfLabels"
                         default_values="1000"
                         name="MaximumNumberOfLabels"
                         number_of_elements="1"
                         panel_visibility="default">
        <IntRangeDomain min="1"
                        name="range" />
        <Documentation>The maximum number of point labels shown. If the dataset
          contains more points that this number, a random subset of this size is
          chosen and labeled.</Documentation>
        <Hints>
          <PropertyLink group="settings"
                        proxy="GeneralSettings"
                        property="MaximumNumberOfDataRepresentationLabels"/>
        </Hints>
      </IntVectorProperty>

     <IntVectorProperty command="SetPointLabelFontFamily"
                        default_values="0"
                        name="PointLabelFontFamily"
                        number_of_elements="1"
                        panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="Arial"
                 value="0" />
          <Entry text="Courier"
                 value="1" />
          <Entry text="Times"
                 value="2" />
          <Entry text="File"
                 value="4" />
        </EnumerationDomain>
      </IntVectorProperty>

      <StringVectorProperty command="SetPointLabelFontFile"
                            name="PointLabelFontFile"
                            number_of_elements="1"
                            default_values=""
                            panel_visibility="advanced">
      </StringVectorProperty>

      <IntVectorProperty command="SetPointLabelFontSize"
                         default_values="18"
                         name="PointLabelFontSize"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <IntRangeDomain min="0"
                        name="range" />
      </IntVectorProperty>

      <StringVectorProperty command="SetPointLabelFormat"
                            default_values=""
                            name="PointLabelFormat"
                            number_of_elements="1"
                            panel_visibility="advanced">
        <!-- that default format would be ok for percentages -->
        <Documentation>
          This should be a printf-style format string. By default,
          the mapper will try to print each component of the tuple
          using a sane format: d for integers, f for floats, g for
          doubles, ld for longs, et cetera. If you need a different
          format, set it here. You can do things like limit the number
          of significant digits, add prefixes/suffixes, basically
          anything that printf can do. Leave empty to auto select an
          appropriate format.
        </Documentation>
      </StringVectorProperty>
 
      <DoubleVectorProperty command="SetPointLabelColor"
                            default_values="1 1 0"
                            name="PointLabelColor"
                            number_of_elements="3"
                            panel_visibility="advanced">
        <DoubleRangeDomain max="1 1 1"
                           min="0 0 0"
                           name="range" />
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetPointLabelOpacity"
                            default_values="1.0"
                            name="PointLabelOpacity"
                            number_of_elements="1"
                            panel_visibility="advanced">
        <DoubleRangeDomain max="1.0"
                           min="0.0"
                           name="range" />
      </DoubleVectorProperty>

      <IntVectorProperty command="SetPointLabelBold"
                         default_values="0"
                         name="PointLabelBold"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetPointLabelItalic"
                         default_values="0"
                         name="PointLabelItalic"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetPointLabelShadow"
                         default_values="0"
                         name="PointLabelShadow"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetPointLabelJustification"
                         default_values="0"
                         name="PointLabelJustification"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="Left"
                 value="0" />
          <Entry text="Center"
                 value="1" />
          <Entry text="Right"
                 value="2" />
        </EnumerationDomain>
      </IntVectorProperty>

      <IntVectorProperty command="SetPointLabelMode"
                         default_values="6"
                         name="PointLabelMode"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="IDs"
                 value="0" />
          <Entry text="Scalars"
                 value="1" />
          <Entry text="Vectors"
                 value="2" />
          <Entry text="Normals"
                 value="3" />
          <Entry text="TCoords"
                 value="4" />
          <Entry text="Tensors"
                 value="5" />
          <Entry text="FieldData"
                 value="6" />
        </EnumerationDomain>
      </IntVectorProperty>

      <StringVectorProperty command="SetCellFieldDataArrayName"
                            name="CellFieldDataArrayName"
                            number_of_elements="1"
                            panel_visibility="default">
        <!-- no default_values: we are not using this property anyway... -->
        <ArrayListDomain input_domain_name="input_array"
                         name="array_list">
          <RequiredProperties>
            <Property function="Input"
                      name="Input" />
          </RequiredProperties>
        </ArrayListDomain>
      </StringVectorProperty>

      <IntVectorProperty command="SetCellLabelVisibility"
                         default_values="0"
                         name="CellLabelVisibility"
                         number_of_elements="1"
                         panel_visibility="default">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetCellLabelFontFamily"
                         default_values="0"
                         name="CellLabelFontFamily"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="Arial"
                 value="0" />
          <Entry text="Courier"
                 value="1" />
          <Entry text="Times"
                 value="2" />
          <Entry text="File"
                 value="4" />
        </EnumerationDomain>
      </IntVectorProperty>

      <StringVectorProperty command="SetCellLabelFontFile"
                            name="CellLabelFontFile"
                            number_of_elements="1"
                            default_values=""
                            panel_visibility="advanced">
      </StringVectorProperty>

      <IntVectorProperty command="SetCellLabelFontSize"
                         default_values="18"
                         name="CellLabelFontSize"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <IntRangeDomain min="0"
                        name="range" />
      </IntVectorProperty>

      <StringVectorProperty command="SetCellLabelFormat"
                            default_values=""
                            name="CellLabelFormat"
                            number_of_elements="1"
                            panel_visibility="advanced">
        <Documentation>
          This should be a printf-style format string. By default,
          the mapper will try to print each component of the tuple
          using a sane format: d for integers, f for floats, g for
          doubles, ld for longs, et cetera. If you need a different
          format, set it here. You can do things like limit the number
          of significant digits, add prefixes/suffixes, basically
          anything that printf can do. Leave empty to auto select an
          appropriate format.
        </Documentation>
      </StringVectorProperty>

      <DoubleVectorProperty command="SetCellLabelColor"
                            default_values="0.0 1.0 0.0"
                            name="CellLabelColor"
                            number_of_elements="3"
                            panel_visibility="advanced">
        <DoubleRangeDomain max="1 1 1"
                           min="0 0 0"
                           name="range" />
      </DoubleVectorProperty>

      <DoubleVectorProperty command="SetCellLabelOpacity"
                            default_values="1.0"
                            name="CellLabelOpacity"
                            number_of_elements="1"
                            panel_visibility="advanced">
        <DoubleRangeDomain max="1.0"
                           min="0.0"
                           name="range" />
      </DoubleVectorProperty>

      <IntVectorProperty command="SetCellLabelBold"
                         default_values="0"
                         name="CellLabelBold"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetCellLabelItalic"
                         default_values="0"
                         name="CellLabelItalic"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetCellLabelShadow"
                         default_values="0"
                         name="CellLabelShadow"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <BooleanDomain name="bool" />
      </IntVectorProperty>

      <IntVectorProperty command="SetCellLabelJustification"
                         default_values="0"
                         name="CellLabelJustification"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="Left"
                 value="0" />
          <Entry text="Center"
                 value="1" />
          <Entry text="Right"
                 value="2" />
        </EnumerationDomain>
      </IntVectorProperty>

      <IntVectorProperty command="SetCellLabelMode"
                         default_values="6"
                         name="CellLabelMode"
                         number_of_elements="1"
                         panel_visibility="advanced">
        <EnumerationDomain name="enum">
          <Entry text="IDs"
                 value="0" />
          <Entry text="Scalars"
                 value="1" />
          <Entry text="Vectors"
                 value="2" />
          <Entry text="Normals"
                 value="3" />
          <Entry text="TCoords"
                 value="4" />
          <Entry text="Tensors"
                 value="5" />
          <Entry text="FieldData"
                 value="6" />
        </EnumerationDomain>
      </IntVectorProperty>

      <Documentation>
        Representation for adding labels to samplings and drillholes
      </Documentation>
    </RepresentationProxy>

    <!-- For common geometric display items (not grids), we want to label points -->
    <Extension name="GeometryRepresentation">
       <Documentation>
         Extends the standard geometric object representation by adding
         labels to the points. The primary idea is to label samplings and drillholes.
      </Documentation>

     <RepresentationType subproxy="AtgLabelRepresentation"
                         text="Labels" />

      <SubProxy>
        <Proxy name="AtgLabelRepresentation"
               proxygroup="representations"
               proxyname="AtgLabelRepresentation" />

        <ShareProperties subproxy="SurfaceRepresentation">
          <Exception name="Input" />
          <Exception name="Visibility" />
        </ShareProperties>

        <ExposedProperties>
          <Property name="PointFieldDataArrayName" />
          <Property name="PointLabelVisibility" />
          <Property name="MaximumNumberOfLabels" />
          <Property name="PointLabelFontFamily" />
          <Property name="PointLabelFontSize" />
          <Property name="PointLabelFormat" />
          <Property name="PointLabelColor" />
          <Property name="PointLabelOpacity" />
          <Property name="PointLabelBold" />
          <Property name="PointLabelItalic" />
          <Property name="PointLabelJustification" />
          <Property name="PointLabelShadow" />
        </ExposedProperties>
      </SubProxy>
    </Extension>
  </ProxyGroup>
</ServerManagerConfiguration>
