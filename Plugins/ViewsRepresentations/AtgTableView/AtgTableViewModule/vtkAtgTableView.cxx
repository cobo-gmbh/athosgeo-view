/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableView.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <utilOrderByNames.h>
#include <vtkAlgorithmOutput.h>
#include <vtkCSVExporter.h>
#include <vtkCharArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkIdTypeArray.h>
#include <vtkStringArray.h>
#include <vtkDataArrayRange.h>
#include <vtkClientServerMoveData.h>
#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkDataSetAttributes.h>
#include <vtkMarkSelectedRows.h>
#include <vtkMemberFunctionCommand.h>
#include <vtkMultiProcessController.h>
#include <vtkMultiProcessStream.h>
#include <vtkObjectFactory.h>
#include <vtkPVMergeTables.h>
#include <vtkPVSession.h>
#include <vtkProcessModule.h>
#include <vtkReductionFilter.h>
#include <vtkSmartPointer.h>
#include <vtkSortedTableStreamer.h>
#include <vtkSpreadSheetRepresentation.h>
#include <vtkTable.h>
#include <vtkVariant.h>
#include <vtkAtgTableViewInternal.h>

#define DEF_TABLEVIEW_EXPORT
#include <vtkAtgTableView.h>

vtkStandardNewMacro(vtkAtgTableView);

namespace
{
void FetchRMI(void* localArg, void* remoteArg, int remoteArgLength, int)
{
    vtkMultiProcessStream stream;
    stream.SetRawData(reinterpret_cast<unsigned char*>(remoteArg), remoteArgLength);

    unsigned int id = 0;
    int blockid = -1;

    stream >> id >> blockid;

    vtkAtgTableView* self = reinterpret_cast<vtkAtgTableView*>(localArg);

    if(self->GetIdentifier() == id)
        self->FetchBlockCallback(blockid);
}

void FetchRMIBogus(void*, void*, int, int)
{}

// A subclass of vtkPVMergeTables to handle reduction for "vtkBlockNameIndices"
// and "vtkBlockNames" arrays correctly.
class SpreadSheetViewMergeTables: public vtkPVMergeTables
{
public:

    static SpreadSheetViewMergeTables* New();
    vtkTypeMacro(SpreadSheetViewMergeTables, vtkPVMergeTables);

protected:

    SpreadSheetViewMergeTables() = default;
    ~SpreadSheetViewMergeTables() override = default;

    int RequestData(vtkInformation* req, vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector) override
    {
        auto output = vtkTable::GetData(outputVector, 0);
        auto inputs = vtkPVMergeTables::GetTables(inputVector[0]);

        const bool has_block_names =
          (!inputs.empty() && inputs[0]->GetFieldData()->GetAbstractArray("vtkBlockNames"));
        if(!has_block_names)
            return Superclass::RequestData(req, inputVector, outputVector);

        // Reduce vtkBlockNameIndices array correctly.
        // vtkSortedTableStreamer adds a Row array named "vtkBlockNameIndices" which
        // is the index for "vtkBlockNames" field array which is the name of the
        // block. This indirection is used to avoid duplicating strings for all
        // elements since they don't change for the entire block. However, with MBs
        // block names are rarely consistent across ranks. So we do this reduction
        // to build a reduced `vtkBlockNames` array and update the
        // `vtkBlockNameIndices` accordingly.
        std::map<std::string, vtkIdType> nameMap;
        std::vector<vtkTable*> new_inputs(inputs.size(), nullptr);
        std::transform(inputs.begin(), inputs.end(), new_inputs.begin(),
                       [&nameMap](vtkTable* input)
        {
            vtkTable* xformed = vtkTable::New();
            xformed->ShallowCopy(input);

            auto inIndices = vtkIdTypeArray::SafeDownCast(input->GetColumnByName("vtkBlockNameIndices"));
            auto inNames =
                vtkStringArray::SafeDownCast(input->GetFieldData()->GetAbstractArray("vtkBlockNames"));
            if(!inIndices || !inNames)
                return xformed;

            // insert names in map, if not already present.
            for(vtkIdType cc = 0, max = inNames->GetNumberOfTuples(); cc < max; ++cc)
                nameMap.insert(std::make_pair(inNames->GetValue(cc), static_cast<vtkIdType>(nameMap.size())));

            vtkNew<vtkIdTypeArray> outIndices;
            outIndices->SetName("vtkBlockNameIndices");
            outIndices->SetNumberOfTuples(inIndices->GetNumberOfTuples());
            auto irange = vtk::DataArrayValueRange<1>(inIndices);
            auto orange = vtk::DataArrayValueRange<1>(outIndices);
            std::transform(irange.begin(), irange.end(), orange.begin(),
                           [&nameMap, inNames](const vtkIdType& index)
            {
                return nameMap.at(inNames->GetValue(index));
            });

            xformed->RemoveColumnByName("vtkBlockNameIndices");
            xformed->AddColumn(outIndices);
            return xformed;
        });

        vtkPVMergeTables::MergeTables(output, new_inputs);
        for(auto input : new_inputs)
            input->Delete();
        new_inputs.clear();

        vtkNew<vtkStringArray> outNames;
        outNames->SetName("vtkBlockNames");
        outNames->SetNumberOfTuples(static_cast<vtkIdType>(nameMap.size()));
        for(const auto& pair : nameMap)
            outNames->SetValue(pair.second, pair.first);
        output->GetFieldData()->RemoveArray("vtkBlockNames");
        output->GetFieldData()->AddArray(outNames);
        return 1;
    }

private:

    SpreadSheetViewMergeTables(const SpreadSheetViewMergeTables&) = delete;
    void operator=(const SpreadSheetViewMergeTables&) = delete;

};

vtkStandardNewMacro(SpreadSheetViewMergeTables);

}

vtkAtgTableView::vtkAtgTableView()
:   Superclass(),
    TableStreamer(vtkSortedTableStreamer::New()),
    TableSelectionMarker(vtkMarkSelectedRows::New()),
    ReductionFilter(vtkReductionFilter::New()),
    DeliveryFilter(vtkClientServerMoveData::New()),
    NumberOfRows(0),
    Internals(new vtkAtgTableViewInternal()),
    SomethingUpdated(false),
    RMICallbackTag(0)
{
    ReductionFilter->SetController(vtkMultiProcessController::GetGlobalController());

    // BCO 2023-01-19 - from the new (PV v5.10) vtkSpreadSheetView
    ReductionFilter->SetPostGatherHelper(vtkNew<SpreadSheetViewMergeTables>().GetPointer());
    //vtkPVMergeTables* post_gather_algo = vtkPVMergeTables::New();
    //ReductionFilter->SetPostGatherHelper(post_gather_algo);
    //post_gather_algo->FastDelete();

    DeliveryFilter->SetOutputDataType(VTK_TABLE);

    ReductionFilter->SetInputConnection(TableStreamer->GetOutputPort());

    Internals->MostRecentlyAccessedBlock = -1;

    Internals->Observer =
            vtkMakeMemberFunctionCommand(*this, &vtkAtgTableView::OnRepresentationUpdated);

    auto session = GetSession();
    assert(session);
    if(auto cController = session->GetController(vtkPVSession::CLIENT))
        CRMICallbackTag = cController->AddRMICallback(::FetchRMI, this, FETCH_BLOCK_TAG);
    if(auto pController = vtkMultiProcessController::GetGlobalController())
        PRMICallbackTag = pController->AddRMICallback(::FetchRMI, this, FETCH_BLOCK_TAG);
}

vtkAtgTableView::~vtkAtgTableView()
{
    auto session = GetSession();
    if(auto cController = (session ? session->GetController(vtkPVSession::CLIENT) : nullptr))
    {
        cController->RemoveRMICallback(CRMICallbackTag);
        CRMICallbackTag = 0;
    }
    if(auto pController = (session ? vtkMultiProcessController::GetGlobalController() : nullptr))
    {
        pController->RemoveRMICallback(PRMICallbackTag);
        PRMICallbackTag = 0;
    }

    TableStreamer->Delete();
    TableSelectionMarker->Delete();
    ReductionFilter->Delete();
    DeliveryFilter->Delete();

    Internals->Observer->Delete();
    delete Internals;
    Internals = nullptr;
}

void vtkAtgTableView::HideColumnByName(const char* columnName)
{
    if(columnName)
    {
        auto& internals = *Internals;
        internals.HiddenColumnsByName.insert(columnName);
    }
}

void vtkAtgTableView::ClearHiddenColumnsByName()
{
    auto& internals = *Internals;
    internals.HiddenColumnsByName.clear();
}

bool vtkAtgTableView::IsColumnHiddenByName(const char* columnName)
{
    const auto& internals = *Internals;
    return columnName
            ? internals.HiddenColumnsByName.find(columnName) != internals.HiddenColumnsByName.end()
            : true;
}

void vtkAtgTableView::HideColumnByLabel(const char* columnLabel)
{
    if(columnLabel)
    {
        auto& internals = *Internals;
        internals.HiddenColumnsByLabel.insert(columnLabel);
    }
}

void vtkAtgTableView::ClearHiddenColumnsByLabel()
{
    auto& internals = *Internals;
    internals.HiddenColumnsByLabel.clear();
}

bool vtkAtgTableView::IsColumnHiddenByLabel(std::string const& columnLabel)
{
    const auto& internals = *Internals;
    return !columnLabel.empty()
            ? internals.HiddenColumnsByLabel.find(columnLabel) != internals.HiddenColumnsByLabel.end()
            : true;
}

void vtkAtgTableView::ClearCache()
{
    Internals->CachedBlocks.clear();
}

bool vtkAtgTableView::GetColumnVisibility(vtkIdType index)
{
    auto name = GetColumnName(index);
    auto label = GetColumnLabel(name);

    return !(IsColumnInternal(name) ||
             IsColumnHiddenByName(name) ||
             IsColumnHiddenByLabel(label));
}

bool vtkAtgTableView::IsColumnInternal(vtkIdType index)
{
    return IsColumnInternal(GetColumnName(index));
}

bool vtkAtgTableView::IsColumnInternal(const char* columnName)
{
    if(Superclass::IsColumnInternal(columnName))
        return true;

    // note: trying to hide here the "Cell ID" column does not work: it seems to be
    //   mandatory for a cells display table!
    static const std::vector<std::string> internalCols(std::initializer_list<std::string>(
    {
        "",
        "__vtkIsSelected__",
        "Cell Type"
    }));
    return internalCols.end() != std::find(internalCols.begin(), internalCols.end(), columnName);
}

void vtkAtgTableView::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}

void vtkAtgTableView::Update()
{
    vtkSpreadSheetRepresentation *prev = Internals->ActiveRepresentation,
                                 *cur = nullptr;

    for(int cc = 0; cc < GetNumberOfRepresentations(); cc++)
    {
        vtkSpreadSheetRepresentation* repr =
                vtkSpreadSheetRepresentation::SafeDownCast(GetRepresentation(cc));
        if(repr && repr->GetVisibility())
        {
            cur = repr;
            break;
        }
    }

    if(prev != cur)
    {
        if(prev)
            prev->RemoveObserver(Internals->Observer);

        if(cur)
            cur->AddObserver(vtkCommand::UpdateDataEvent, Internals->Observer);

        Internals->ActiveRepresentation = cur;
        ClearCache();
    }

    SomethingUpdated = false;

    // above we are copying/correcting the code of the superclass
    // (using our own Internals), so here we should skip the replaced function
    Superclass::Superclass::Update();
}

int vtkAtgTableView::StreamToClient()
{
    vtkSpreadSheetRepresentation* cur = Internals->ActiveRepresentation;
    if(cur == nullptr)
    {
        if(NumberOfRows > 0)
        {
            // BUG #13231: It implies that we had some data in the previous render and
            // we don't have it anymore. We need to trigger "UpdateDataEvent" to
            // ensure that the UI can update the rows and columns correctly.
            NumberOfRows = 0;
            InvokeEvent(vtkCommand::UpdateDataEvent);
        }

        return 0;
    }

    // From the active representation obtain the data producer that
    // need to be streamed to the client.
    vtkAlgorithmOutput* dataPort = vtkAtgTableViewInternal::GetDataProducer(this, cur);
    TableSelectionMarker->SetInputConnection(0, dataPort);
    TableSelectionMarker->SetInputConnection(1, cur->GetExtractedDataProducer());
    TableStreamer->SetInputConnection(TableSelectionMarker->GetOutputPort());

    vtkTypeUInt64 num_rows = 0;
    if(dataPort)
    {
        dataPort->GetProducer()->Update();
        DeliveryFilter->SetInputConnection(ReductionFilter->GetOutputPort());
        num_rows = vtkAtgTableViewInternal::CountNumberOfRows(dataPort->GetProducer()->GetOutputDataObject(dataPort->GetIndex()));
    }
    else
    {
        DeliveryFilter->RemoveAllInputs();
    }

    AllReduce(num_rows, num_rows, vtkCommunicator::SUM_OP);

    if(NumberOfRows != static_cast<vtkIdType>(num_rows))
        SomethingUpdated = true;

    NumberOfRows = num_rows;

    if(SomethingUpdated)
    {
        ClearCache();
        InvokeEvent(vtkCommand::UpdateDataEvent);
    }

    return 1;
}

void vtkAtgTableView::OnRepresentationUpdated()
{
    ClearCache();
    SomethingUpdated = true;
}

vtkTable* vtkAtgTableView::FetchBlock(vtkIdType blockindex, bool skipCache)
{
    vtkTable* block;

    // Do not use cache when the blocks are fetched for exporting.
    if(skipCache)
    {
        block = FetchBlockCallback(blockindex);
    }

    else
    {
        block = Internals->GetDataObject(blockindex);

        if(!block)
        {
            block = FetchBlockCallback(blockindex);
            Internals->AddToCache(blockindex, block, 10);
            InvokeEvent(vtkCommand::UpdateEvent, &blockindex);
        }
    }

    return block;
}

vtkTable* vtkAtgTableView::FetchBlockCallback(vtkIdType blockindex)
{
    // Sanity Check
    if(!Internals->ActiveRepresentation)
        return nullptr;

    vtkTypeUInt64 data[2] =
    {
        Identifier, static_cast<vtkTypeUInt64>(blockindex)
    };
    if(auto dController = GetSession()->GetController(vtkPVSession::DATA_SERVER_ROOT))
        dController->TriggerRMIOnAllChildren(data, sizeof(vtkTypeUInt64) * 2, FETCH_BLOCK_TAG);
    auto pController = vtkMultiProcessController::GetGlobalController();
    if((pController && pController->GetLocalProcessId() == 0) && (pController->GetNumberOfProcesses() > 1))
        pController->TriggerRMIOnAllChildren(data, sizeof(vtkTypeUInt64) * 2, FETCH_BLOCK_TAG);

    TableStreamer->SetBlock(blockindex);
    TableStreamer->Modified();
    TableSelectionMarker->SetFieldAssociation(GetFieldAssociation());
    ReductionFilter->Modified();
    DeliveryFilter->Modified();
    DeliveryFilter->Update();

    return vtkTable::SafeDownCast(DeliveryFilter->GetOutput());
}

vtkIdType vtkAtgTableView::GetNumberOfColumns()
{
    if(Internals->ActiveRepresentation)
    {
        vtkTable* block0 = FetchBlock(Internals->GetMostRecentlyAccessedBlock(this));
        if(block0)
            return block0->GetNumberOfColumns();
    }

    return 0;
}

vtkIdType vtkAtgTableView::GetNumberOfRows()
{
    return NumberOfRows;
}

const char* vtkAtgTableView::GetColumnName(vtkIdType index)
{
    if(Internals->ActiveRepresentation)
    {
        vtkTable* block0 = FetchBlock(Internals->GetMostRecentlyAccessedBlock(this));
        if(block0)
            return block0->GetColumnName(index);
    }

    return nullptr;
}

std::string vtkAtgTableView::GetColumnLabel(vtkIdType index)
{
    return GetColumnLabel(GetColumnName(index));
}

std::string vtkAtgTableView::GetColumnLabel(const char* name)
{
    return Superclass::GetColumnLabel(name);
}

vtkVariant vtkAtgTableView::GetValue(vtkIdType row, vtkIdType col)
{
    vtkIdType blockSize = TableStreamer->GetBlockSize();
    vtkIdType blockIndex = row / blockSize;
    vtkTable* block = FetchBlock(blockIndex);
    vtkIdType blockOffset = row - (blockIndex * blockSize);

    return block->GetValue(blockOffset, col);
}

vtkVariant vtkAtgTableView::GetValueByName(vtkIdType row, const char* columnName)
{
    vtkIdType blockSize = TableStreamer->GetBlockSize();
    vtkIdType blockIndex = row / blockSize;
    vtkTable* block = FetchBlock(blockIndex);
    vtkIdType blockOffset = row - (blockIndex * blockSize);

    return block->GetValueByName(blockOffset, columnName);
}

bool vtkAtgTableView::IsRowSelected(vtkIdType row)
{
    vtkIdType blockSize = TableStreamer->GetBlockSize();
    vtkIdType blockIndex = row / blockSize;
    vtkTable* block = FetchBlock(blockIndex);
    vtkIdType blockOffset = row - (blockIndex * blockSize);
    vtkCharArray* vtkIsSelected =
    vtkCharArray::SafeDownCast(block->GetColumnByName("__vtkIsSelected__"));

    if(vtkIsSelected)
        return vtkIsSelected->GetValue(blockOffset) == 1;

    return false;
}

bool vtkAtgTableView::IsAvailable(vtkIdType row)
{
    vtkIdType blockSize = TableStreamer->GetBlockSize();
    vtkIdType blockIndex = row / blockSize;

    return Internals->GetDataObject(blockIndex) != NULL;
}

bool vtkAtgTableView::IsDataValid(vtkIdType row, vtkIdType col)
{
    vtkIdType blockSize = this->TableStreamer->GetBlockSize();
    vtkIdType blockIndex = row / blockSize;
    vtkTable* block = this->FetchBlock(blockIndex);
    vtkIdType blockOffset = row - (blockIndex * blockSize);

    if(auto columnName = this->GetColumnName(col))
    {
        const std::string maskColumnName(std::string(columnName) + "__vtkValidMask__");
        auto maskArray =
                vtkUnsignedCharArray::SafeDownCast(block->GetColumnByName(maskColumnName.c_str()));
        if(maskArray && maskArray->GetNumberOfTuples() > blockOffset &&
           maskArray->GetTypedComponent(blockOffset, 0) == static_cast<unsigned char>(0))
        {
            return false;
        }
    }

    return true;
}

bool vtkAtgTableView::Export(vtkCSVExporter* exporter)
{
    if(!exporter->Open())
        return false;

    ClearCache();

    if(auto activeRepr = Internals->ActiveRepresentation)
    {
        vtkIdType blockSize = TableStreamer->GetBlockSize();
        vtkIdType numBlocks = (GetNumberOfRows() / blockSize) + 1;

        for(vtkIdType cc = 0; cc < numBlocks; cc++)
        {
            if(vtkTable* block = FetchBlock(cc))
            {
                vtkDataSetAttributes* rowData = block->GetRowData();

                // sort columns for better usability - here for ensuring same order in exported tables
                // than we are having in the displayed tables
                auto cloneRowData = vtkSmartPointer<vtkDataSetAttributes>::New();
                std::vector<vtkAbstractArray*> arrays;
                for(vtkIdType cc = 0; cc < rowData->GetNumberOfArrays(); cc++)
                {
                    if(rowData->GetAbstractArray(cc))
                        arrays.push_back(rowData->GetAbstractArray(cc));
                }
                std::sort(arrays.begin(), arrays.end(), OrderByNames());
                for(std::vector<vtkAbstractArray*>::iterator viter = arrays.begin(); viter != arrays.end(); ++viter)
                    cloneRowData->AddArray(*viter);

                if(cc == 0)
                {
                    // update column labels; this ensures that all the columns have same
                    // names as the spreadsheet view.
                    for(vtkIdType idx = 0; idx < rowData->GetNumberOfArrays(); ++idx)
                    {
                        if(auto array = rowData->GetAbstractArray(idx))
                        {
                            // note: internal columns get nullptr label which the exporter
                            // skips.
                            auto name = array->GetName();
                            auto label = GetColumnLabel(name);

                            if(IsColumnInternal(name) ||
                               IsColumnHiddenByName(name) ||
                               IsColumnHiddenByLabel(label))
                                exporter->SetColumnLabel(array->GetName(), nullptr);
                            else
                                exporter->SetColumnLabel(array->GetName(), label.c_str());
                        }
                    }

                    exporter->WriteHeader(cloneRowData);
                }

                exporter->WriteData(cloneRowData);
            }
        }
    }

    exporter->Close();

    return true;
}

void vtkAtgTableView::SetColumnNameToSort(const char* name)
{
    TableStreamer->SetColumnNameToSort(name);
    ClearCache();
}

void vtkAtgTableView::SetComponentToSort(int val)
{
    TableStreamer->SetSelectedComponent(val);
    ClearCache();
}

void vtkAtgTableView::SetInvertSortOrder(bool val)
{
    TableStreamer->SetInvertOrder(val ? 1 : 0);
    ClearCache();
}

void vtkAtgTableView::SetBlockSize(vtkIdType val)
{
    TableStreamer->SetBlockSize(val);
    ClearCache();
}
