#include <iostream>
#include <string>
#include <vector>
#include <utilOrderByNames.h>
#include <vtkSortedTableStreamer.h>
#include <vtkSpreadSheetRepresentation.h>
#include <vtkCompositeDataSet.h>
#include <vtkCompositeDataIterator.h>
#include <vtkTable.h>
#include <vtkAbstractArray.h>
#include <vtkObjectFactory.h>
#include <vtkAtgTableView.h>
#include <vtkAtgTableViewInternal.h>

vtkTable* vtkAtgTableViewInternal::GetDataObject(vtkIdType blockId)
{
    CacheType::iterator iter = CachedBlocks.find(blockId);

    if(iter != CachedBlocks.end())
    {
        iter->second.RecentUseTime.Modified();
        MostRecentlyAccessedBlock = blockId;
        return iter->second.Dataobject.GetPointer();
    }

    return nullptr;
}

void vtkAtgTableViewInternal::AddToCache(vtkIdType blockId, vtkTable* data, vtkIdType max)
{
    CacheType::iterator iter = CachedBlocks.find(blockId);

    if(iter != CachedBlocks.end())
        CachedBlocks.erase(iter);

    if(static_cast<vtkIdType>(CachedBlocks.size()) == max)
    {
        // remove least-recent-used block.
        iter = CachedBlocks.begin();
        CacheType::iterator iterToRemove = CachedBlocks.begin();

        for(; iter != CachedBlocks.end(); ++iter)
        {
            if(iterToRemove->second.RecentUseTime > iter->second.RecentUseTime)
                iterToRemove = iter;
        }

        CachedBlocks.erase(iterToRemove);
    }

    CacheInfo info;
    vtkTable* clone = vtkTable::New();

    // sort columns for better usability.
    std::vector<vtkAbstractArray*> arrays;

    for(vtkIdType cc = 0; cc < data->GetNumberOfColumns(); cc++)
    {
        if(data->GetColumn(cc))
            arrays.push_back(data->GetColumn(cc));
    }

    std::sort(arrays.begin(), arrays.end(), OrderByNames());

    for(std::vector<vtkAbstractArray*>::iterator viter = arrays.begin(); viter != arrays.end(); ++viter)
        clone->AddColumn(*viter);

    info.Dataobject = clone;
    clone->FastDelete();
    info.RecentUseTime.Modified();
    CachedBlocks[blockId] = info;
    MostRecentlyAccessedBlock = blockId;
}

vtkIdType vtkAtgTableViewInternal::GetMostRecentlyAccessedBlock(vtkAtgTableView* view)
{
    vtkIdType maxBlockId = view->GetNumberOfRows() / view->TableStreamer->GetBlockSize();

    if(MostRecentlyAccessedBlock >= 0 && MostRecentlyAccessedBlock <= maxBlockId)
        return MostRecentlyAccessedBlock;

    MostRecentlyAccessedBlock = 0;

    return 0;
}

unsigned long vtkAtgTableViewInternal::CountNumberOfRows(vtkDataObject* dobj)
{
    vtkTable* table = vtkTable::SafeDownCast(dobj);
    if(table)
        return table->GetNumberOfRows();

    vtkCompositeDataSet* cd = vtkCompositeDataSet::SafeDownCast(dobj);
    if(cd)
    {
        unsigned long count = 0;
        vtkCompositeDataIterator* iter = cd->NewIterator();

        for(iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
            count += CountNumberOfRows(iter->GetCurrentDataObject());

        iter->Delete();
        return count;
    }

    return 0;
}

vtkAlgorithmOutput* vtkAtgTableViewInternal::GetDataProducer(vtkAtgTableView* view,
                                                             vtkSpreadSheetRepresentation* repr)
{
    if(repr)
    {
        if(view->GetShowExtractedSelection())
            return repr->GetExtractedDataProducer();
        else
            return repr->GetDataProducer();
    }

    return nullptr;
}
