/*=========================================================================

   Program: AthosGEO
   Module:  vtkSMTableViewProxy.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkDataObject.h>
#include <vtkPVDataInformation.h>
#include <vtkSMPropertyHelper.h>
#include <vtkObjectFactory.h>
#include <vtkSMSourceProxy.h>
#include <vtkSMViewProxy.h>
#include <vtkSMTableViewProxy.h>

vtkStandardNewMacro(vtkSMTableViewProxy);

void vtkSMTableViewProxy::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}

void vtkSMTableViewProxy::RepresentationVisibilityChanged(vtkSMProxy *repr,
                                                          bool new_visibility)
{
    // here we are skipping the direct parent
    Superclass::Superclass::RepresentationVisibilityChanged(repr, new_visibility);

    // from here this is a copy of what is happening in the direct parent,
    // with only the attributes vector changed in such a way that CELL comes before POINT
    if(repr && new_visibility)
    {
        // when a new representation is being made visible, we try to pick
        // FieldAssociation that produces non-empty result.
        vtkSMPropertyHelper inputHelper(repr, "Input", true);
        if(auto input = vtkSMSourceProxy::SafeDownCast(inputHelper.GetAsProxy()))
        {
            auto dataInfo = input->GetDataInformation(inputHelper.GetOutputPort());
            vtkSMPropertyHelper fieldAssociationHelper(this, "FieldAssociation");

            // if we have a field association and it is POINT, we want to change it to CELL
            if(dataInfo->GetNumberOfElements(fieldAssociationHelper.GetAsInt()) > 0)
            {
                int att;
                fieldAssociationHelper.Get(&att);
                if(vtkDataObject::POINT == att)
                {
                    fieldAssociationHelper.Set(vtkDataObject::CELL);
                    UpdateVTKObjects();
                }
            }

            // otherwise we try which field association is not empty - and we try CELL first
            else
            {
                // we define a list of attribute type in a different order than
                // the one described in vtkDataObject as we want the field data
                // to be considered if no other attribute is available.
                const std::vector<int> attributes =
                {
                    vtkDataObject::CELL,
                    vtkDataObject::POINT,
                    vtkDataObject::VERTEX,
                    vtkDataObject::EDGE,
                    vtkDataObject::ROW,
                    vtkDataObject::FIELD
                };

                // let's try to pick as association with non-empty tuples.
                for(int att: attributes)
                {
                    if(dataInfo->GetNumberOfElements(att) > 0)
                    {
                        fieldAssociationHelper.Set(att);
                        UpdateVTKObjects();
                        break;
                    }
                }
            }
        }
    }
}

vtkSMTableViewProxy::vtkSMTableViewProxy()
:   Superclass()
{
}

vtkSMTableViewProxy::~vtkSMTableViewProxy()
{
}
