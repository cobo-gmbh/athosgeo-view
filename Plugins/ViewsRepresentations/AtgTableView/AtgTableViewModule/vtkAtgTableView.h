/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableView.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTableView_H
#define vtkAtgTableView_H

#include <string>
#include <vtkSpreadSheetView.h>
#include <AtgTableViewModuleModule.h>

class vtkCSVExporter;
class vtkClientServerMoveData;
class vtkMarkSelectedRows;
class vtkReductionFilter;
class vtkSortedTableStreamer;
class vtkTable;
class vtkVariant;
class vtkAtgTableViewInternal;

class ATGTABLEVIEWMODULE_EXPORT vtkAtgTableView: public vtkSpreadSheetView
{
public:

    static vtkAtgTableView* New();
    vtkTypeMacro(vtkAtgTableView, vtkSpreadSheetView);
    void PrintSelf(ostream& os, vtkIndent indent) override;

    /**
    * Triggers a high-resolution render.
    * \note CallOnAllProcesses
    */
    void StillRender() override
    {
        StreamToClient();
    }

    /**
    * Triggers a interactive render. Based on the settings on the view, this may
    * result in a low-resolution rendering or a simplified geometry rendering.
    * \note CallOnAllProcesses
    */
    void InteractiveRender() override
    {
        StreamToClient();
    }

    /**
    * Overridden to identify and locate the active-representation.
    */
    void Update() override;

    //@{
    /**
    * This API enables the users to hide columns that should be shown.
    * Columns can be hidden using their names or labels.
    */
    void HideColumnByName(const char* columnName);
    bool IsColumnHiddenByName(const char* columnName);
    void ClearHiddenColumnsByName();

    void HideColumnByLabel(const char* columnLabel);
    bool IsColumnHiddenByLabel(std::string const& columnLabel);
    void ClearHiddenColumnsByLabel();
    //@}

    /**
    * Get the number of columns.
    * \note CallOnClient
    */
    virtual vtkIdType GetNumberOfColumns();

    /**
    * Get the number of rows.
    * \note CallOnClient
    */
    virtual vtkIdType GetNumberOfRows();

    /**
    * Returns the name for the column.
    * \note CallOnClient
    */
    virtual const char* GetColumnName(vtkIdType index);

    //@{
    /**
    * Returns true if the column is internal.
    */
    virtual bool IsColumnInternal(vtkIdType index);
    virtual bool IsColumnInternal(const char* columnName);
    //@}

    //@{
    /**
    * Returns the user-friendly label to use for the column
    * in the spreadsheet view.
    *
    * If `this->IsColumnInternal(..)` is true for the chosen column. Then this
    * method will return `nullptr`.
    *
    * \note CallOnClient
    */
    virtual std::string GetColumnLabel(vtkIdType index);
    virtual std::string GetColumnLabel(const char* columnName);
    //@}

    /**
    * Returns the visibility for the column at the given index.
    */
    virtual bool GetColumnVisibility(vtkIdType index);

    //@{
    /**
    * Returns the value at given location. This may result in collective
    * operations is data is not available locally. This method can only be called
    * on the CLIENT process for now.
    * \note CallOnClient
    */
    virtual vtkVariant GetValue(vtkIdType row, vtkIdType col);
    virtual vtkVariant GetValueByName(vtkIdType row, const char* columnName);
    //@}

    /**
    * Returns true if the row is selected.
    */
    virtual bool IsRowSelected(vtkIdType row);

    /**
    * Returns true is the data for the particular row is locally available.
    */
    virtual bool IsAvailable(vtkIdType row);

    /**
     * Returns true of the data at the given row and column is valid.
     */
    virtual bool IsDataValid(vtkIdType row, vtkIdType col);

    /**
    * Get/Set the column name to sort by.
    * \note CallOnAllProcesses
    */
    void SetColumnNameToSort(const char*);
    void SetColumnNameToSort() { this->SetColumnNameToSort(NULL); }

    /**
    * Get/Set the component to sort with. Use -1 (default) for magnitude.
    * \note CallOnAllProcesses
    */
    void SetComponentToSort(int val);

    /**
    * Get/Set whether the sort order must be Max to Min rather than Min to Max.
    * \note CallOnAllProcesses
    */
    void SetInvertSortOrder(bool);

    /**
    * Set the block size
    * \note CallOnAllProcesses
    */
    void SetBlockSize(vtkIdType val);

    /**
    * Export the contents of this view using the exporter.
    */
    virtual bool Export(vtkCSVExporter* exporter);

    /**
    * Allow user to clear the cache if he needs to.
    */
    void ClearCache();

    // don't call: it's INTERNAL
    vtkTable* FetchBlockCallback(vtkIdType blockindex);

protected:

    vtkAtgTableView();
    ~vtkAtgTableView() override;

    /**
    * On render streams all the data from the processes to the client.
    * Returns 0 on failure.
    * Note: Was removed from update because you can't call update()
    * while in an update
    */
    virtual int StreamToClient();

    void OnRepresentationUpdated();

    virtual vtkTable* FetchBlock(vtkIdType blockindex, bool skipCache = false);

    // leave attributes with parent class

    // we want to access the TableStreamer
    friend class vtkAtgTableViewInternal;

    vtkSortedTableStreamer* TableStreamer;
    vtkMarkSelectedRows* TableSelectionMarker;
    vtkReductionFilter* ReductionFilter;
    vtkClientServerMoveData* DeliveryFilter;

    vtkIdType NumberOfRows;

    enum
    {
        FETCH_BLOCK_TAG = 394732
    };

protected:

    vtkAtgTableViewInternal* Internals;
    bool SomethingUpdated;

private:

    vtkAtgTableView(const vtkAtgTableView&) = delete;
    void operator=(const vtkAtgTableView&) = delete;

    unsigned long RMICallbackTag;

};

#endif
