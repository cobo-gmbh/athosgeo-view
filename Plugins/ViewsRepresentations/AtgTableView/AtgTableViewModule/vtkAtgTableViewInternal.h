#ifndef VTKATGTABLEVIEWINTERNAL_H
#define VTKATGTABLEVIEWINTERNAL_H

#include <map>
#include <set>
#include <vtkSmartPointer.h>
#include <vtkWeakPointer.h>
#include <vtkType.h>
#include <AtgTableViewModuleModule.h>

class vtkTable;
class vtkSpreadSheetRepresentation;
class vtkCommand;
class vtkTimeStamp;
class vtkDataObject;
class vtkAlgorithmOutput;
class vtkAtgTableView;

class ATGTABLEVIEWMODULE_EXPORT vtkAtgTableViewInternal
{
public:

    class CacheInfo
    {
    public:

        vtkSmartPointer<vtkTable> Dataobject;
        vtkTimeStamp RecentUseTime;

    };

    typedef std::map<vtkIdType, CacheInfo> CacheType;
    CacheType CachedBlocks;

    vtkTable* GetDataObject(vtkIdType blockId);

    void AddToCache(vtkIdType blockId, vtkTable* data, vtkIdType max);

    vtkIdType GetMostRecentlyAccessedBlock(vtkAtgTableView* view);

    static unsigned long CountNumberOfRows(vtkDataObject* dobj);

    static vtkAlgorithmOutput* GetDataProducer(vtkAtgTableView* self, vtkSpreadSheetRepresentation* repr);

    vtkIdType MostRecentlyAccessedBlock;
    vtkWeakPointer<vtkSpreadSheetRepresentation> ActiveRepresentation;
    vtkCommand* Observer;

    std::set<std::string> HiddenColumnsByName;
    std::set<std::string> HiddenColumnsByLabel;

};

#endif
