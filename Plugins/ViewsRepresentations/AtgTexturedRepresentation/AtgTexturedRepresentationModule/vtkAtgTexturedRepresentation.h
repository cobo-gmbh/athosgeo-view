/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTexturedRepresentation.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTexturedRepresentation_H
#define vtkAtgTexturedRepresentation_H

#include <vtkGeometryRepresentationWithFaces.h>
#include <AtgTexturedRepresentationModuleModule.h>

class vtkInformation;
class vtkInformationVector;
class vtkTexture;

class ATGTEXTUREDREPRESENTATIONMODULE_EXPORT vtkAtgTexturedRepresentation:
        public vtkGeometryRepresentationWithFaces
{
public:

    static vtkAtgTexturedRepresentation* New();
    vtkTypeMacro(vtkAtgTexturedRepresentation, vtkGeometryRepresentationWithFaces)
    virtual void PrintSelf(ostream& os, vtkIndent indent) override;

    virtual int RequestData(vtkInformation* request,
                            vtkInformationVector** inputVector,
                            vtkInformationVector* outputVector) override;

    virtual int ProcessViewRequest(vtkInformationRequestKey* request_type,
                                   vtkInformation* inInfo,
                                   vtkInformation* outInfo) override;

protected:

    vtkAtgTexturedRepresentation();
    virtual ~vtkAtgTexturedRepresentation() override;

private:

    vtkAtgTexturedRepresentation(const vtkAtgTexturedRepresentation&) = delete;
    void operator=(const vtkAtgTexturedRepresentation&) = delete;

    vtkTexture* CreateTexture(char const* fname, bool ignoreTransparency);

    std::string mapFileName,
                existingMapFileName;
    bool ignoreAlpha,
         existingIgnoreTransparency;

};

#endif
