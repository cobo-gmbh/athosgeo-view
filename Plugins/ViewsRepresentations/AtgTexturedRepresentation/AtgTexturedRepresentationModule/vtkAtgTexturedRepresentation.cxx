/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTexturedRepresentation.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkFieldData.h>
#include <vtkStringArray.h>
#include <vtkImageData.h>
#include <vtkImageReader2.h>
#include <vtkImageReader2Factory.h>
#include <vtkImageConstantPad.h>
#include <vtkCallbackCommand.h>
#include <vtkTexture.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTexturedRepresentation.h>

vtkStandardNewMacro(vtkAtgTexturedRepresentation)

static void OnError(vtkObject* vtkNotUsed(caller),
                    unsigned long int vtkNotUsed(eventId),
                    void* clientData,
                    void* callData)
{
    // with this we signal that there was an error in the raster reader
    // note: we do not want to see a message from the raster reader
    bool* errorFlag = static_cast<bool*>(clientData);
    *errorFlag = true;

    // this would be the message that the raster reader wants to issue
    char const* msg = static_cast<char const*>(callData);
}

vtkAtgTexturedRepresentation::vtkAtgTexturedRepresentation()
:   ignoreAlpha(true),
    existingIgnoreTransparency(true)
{
}

vtkAtgTexturedRepresentation::~vtkAtgTexturedRepresentation()
{
}

int vtkAtgTexturedRepresentation::RequestData(vtkInformation* request,
                                              vtkInformationVector** inputVector,
                                              vtkInformationVector* outputVector)
{
    // get the input polydata object
    vtkPolyData* pdata = vtkPolyData::GetData(inputVector[0]);

    if(nullptr != pdata)
    {
        // see if the input has a field data object
        vtkFieldData* fdata = pdata->GetFieldData();
        if(nullptr != fdata)
        {
            // now see if we have a texture map file name here
            std::string tmfName("__TextureMapFile__");
            vtkStringArray* tmfArr = vtkStringArray::SafeDownCast(fdata->GetAbstractArray(tmfName.c_str()));
            if((nullptr != tmfArr) && (0 < tmfArr->GetNumberOfValues()))
            {
                mapFileName = tmfArr->GetValue(0);
            }

            // see if we also have an ignoreAlpha flag
            std::string iaName("__IgnoreAlpha__");
            vtkDataArray* iaArr = vtkDataArray::SafeDownCast(fdata->GetAbstractArray(iaName.c_str()));
            if((nullptr != iaArr) && (0 < iaArr->GetNumberOfValues()))
            {
                ignoreAlpha = iaArr->GetVariantValue(0).ToInt();
            }
        }
    }

    return Superclass::RequestData(request, inputVector, outputVector);
}

int vtkAtgTexturedRepresentation::ProcessViewRequest(vtkInformationRequestKey* request_type,
                                                     vtkInformation* inInfo,
                                                     vtkInformation* outInfo)
{
    // we do not want tiling for georeferencet textures
    SetRepeatTextures(false);

    if(!mapFileName.empty())
    {
        vtkTexture* texture = CreateTexture(mapFileName.c_str(), ignoreAlpha);
        if(nullptr != texture)
            SetTexture(texture);
    }

    return Superclass::ProcessViewRequest(request_type, inInfo, outInfo);
}

vtkTexture* vtkAtgTexturedRepresentation::CreateTexture(char const* fname, bool ignoreTransparency)
{
    // make sure that we have a valid file name
    if(nullptr == fname)
        return nullptr;

    // check if we already loaded that image
    if((existingMapFileName == fname) && (existingIgnoreTransparency == ignoreTransparency))
    {
        return nullptr;
    }
    else
    {
        existingMapFileName = fname;
        existingIgnoreTransparency = ignoreTransparency;
    }

    // we want to catch any error condition from the raster reader and handle it here,
    // and for that we need the readerError flag
    vtkNew<vtkCallbackCommand> errObs;
    errObs->SetCallback(&OnError);
    bool readerError = false;
    errObs->SetClientData(&readerError);

    // read the image file
    vtkSmartPointer<vtkImageReader2Factory> readerFactory = vtkSmartPointer<vtkImageReader2Factory>::New();
    vtkSmartPointer<vtkImageReader2> imgReader;
    imgReader.TakeReference(readerFactory->CreateImageReader2(fname));
    imgReader->AddObserver("ErrorEvent", errObs);
    imgReader->SetFileName(fname);
    imgReader->Update();
    vtkImageData* imgData = imgReader->GetOutput();

    // if we had a reader error, not much we can do: empirically it was found that the effect
    // is often only that some "strange" display happens, not a crash, and with this
    // I "dare" now to not abandon the creation, but continue with a message...
    if(readerError)
    {
        vtkOutputWindowDisplayWarningText("Error reading the texture map file (image file) from disk\n");
        //return nullptr;
    }

    // if we want to ignore transparency, and if we have four components, make sure
    // that the fourth (which should be the alpha channel) is always at maximum value,
    // typically 255 thus
    // purpose: some images have been found where this alpha was 0 - and the
    //   image was simply not appearing (except for the white boundary - see below)
    if(ignoreTransparency && (3 < imgData->GetNumberOfScalarComponents()))
    {
        double alpha = imgData->GetScalarTypeMax();
        int* dims = imgData->GetDimensions();
        for(int x = 0; x < dims[0]; ++x)
            for(int y = 0; y < dims[1]; ++y)
                imgData->SetScalarComponentFromDouble(x, y, 0, 3, alpha);
    }

    // get the image data out of the reader and extend the extent by 1 pixel in all directions
    // note: if the extent is used in the way as below
    int extent[6];
    imgData->GetExtent(extent);
    --extent[0];
    ++extent[1];
    --extent[2];
    ++extent[3];

    // add a white 1 pixel "boundary" to the image - for filling the rest of the surface
    // note: the value 255 will be written "everywhere", i.e. if the image has 3 "scalar
    //   components" (like an RGB image), all of the values will be set to 255 - resulting
    //   in a white plane
    vtkSmartPointer<vtkImageConstantPad> imgPad = vtkSmartPointer<vtkImageConstantPad>::New();
    imgPad->SetInputData(imgData);
    imgPad->SetOutputWholeExtent(extent);
    imgPad->SetConstant(255.);
    imgPad->Update();
    vtkImageData* imgDataExtended = imgPad->GetOutput();

    // generate a texture
    vtkTexture* texture = vtkTexture::New();

    // use this image data as the new texture
    texture->SetInputData(imgDataExtended);
    texture->RepeatOff();
    texture->EdgeClampOff();
    texture->SetColorModeToDirectScalars();

    return texture;
}

void vtkAtgTexturedRepresentation::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
