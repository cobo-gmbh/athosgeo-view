/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgZoneFromBoundary.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <string>
#include <vector>
#include <map>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/register/ring.hpp>
#include <boost/geometry/geometries/adapted/boost_tuple.hpp>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkHexahedron.h>
#include <vtkCell.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgCleanNormBoundary.h>
#include <vtkAtgZoneFromBoundary.h>

vtkStandardNewMacro(vtkAtgZoneFromBoundary)

namespace bg = boost::geometry;

BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)

typedef boost::tuple<double, double> zfbPair;
typedef bg::model::ring<zfbPair> zfbRing;
typedef bg::model::polygon<zfbPair> zfbPolygon;
typedef bg::model::multi_polygon<zfbPolygon> zfbMultiPolygon;

static const double coordEps = 0.01;

struct zfbRingLess
{
    bool pairLess(zfbPair const& p1, zfbPair const& p2) const
    {
        bool ret = false;
        if(bg::get<0>(p1) < (bg::get<0>(p2) - coordEps))
            ret = true;
        else if(bg::get<0>(p1) > (bg::get<0>(p2) + coordEps))
            ret = false;
        else
            ret = bg::get<1>(p1) < (bg::get<1>(p2) - coordEps);
        return ret;
    }

    bool operator()(zfbRing const& r1, zfbRing const& r2) const
    {
        unsigned long npts = std::min(r1.size(), r2.size());
        for(unsigned long n = 0; n < npts; ++n)
        {
            zfbPair const& pt1 = r1.at(n);
            zfbPair const& pt2 = r2.at(n);
            if(pairLess(pt1, pt2))
                return true;
            if(pairLess(pt2, pt1))
                return false;
        }
        return false;
    }
};

vtkAtgZoneFromBoundary::vtkAtgZoneFromBoundary()
:   ZoneExtension(nullptr),
    Invert(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(2);
}

vtkAtgZoneFromBoundary::~vtkAtgZoneFromBoundary()
{
    SetZoneExtension(nullptr);
}

int vtkAtgZoneFromBoundary::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // first object: grid (block model)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        case 1:
            // second object: topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 0);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgZoneFromBoundary::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        case 1:
            // "cleaned" boundary lines like they are used for the zone calculation
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgZoneFromBoundary::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    (void)request;

    // we will need some norm names throughout the function, and some temporary names
    static const std::string nameZoneNorm(utilNormNames::getName(utilNormNames::ZONE));
    std::string nameZone = nameZoneNorm;
    if(nullptr != ZoneExtension)
        nameZone = nameZone + "_" + ZoneExtension;

    // input port 0: the input unstructured grid
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputVector[0]);
    if(nullptr == inputGrid)
    {
        vtkOutputWindowDisplayWarningText("AtgZoneFromBoundary: No valid input block model found");
        return 1;
    }

    // input port 1: the input poly data object
    vtkPolyData* inputPoly = vtkPolyData::GetData(inputVector[1]);
    if(nullptr == inputPoly)
    {
        vtkOutputWindowDisplayWarningText("AtgZoneFromBoundary: No valid input geometry found");
        return 1;
    }

    // get the output grid and boundary
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector, 0);
    vtkPolyData* outPoly = vtkPolyData::GetData(outputVector, 1);

    // copy of input block model
    outGrid->ShallowCopy(inputGrid);

    // clean and normalize the "raw boundary" poly data
    // - merge line segments with common end points
    // - close open polylines
    // - remove polygons with less than 3 points
    vtkSmartPointer<vtkAtgCleanNormBoundary> cleanBound = vtkSmartPointer<vtkAtgCleanNormBoundary>::New();
    cleanBound->SetClosePolygons(true);
    cleanBound->SetInputData(inputPoly);
    cleanBound->Update();
    if(0 != cleanBound->GetErrorMsg())
    {
        std::string msg(cleanBound->GetErrorMsg());
        vtkOutputWindowDisplayWarningText(("AtgZoneFromBoundary: " + msg + "\n").c_str());
        return 1;
    }
    outPoly->ShallowCopy(cleanBound->GetOutput());

    // add a new attribute for the zone
    vtkSmartPointer<vtkDoubleArray> zoneArr = vtkSmartPointer<vtkDoubleArray>::New();
    zoneArr->SetName(nameZone.c_str());
    zoneArr->SetNumberOfTuples(outGrid->GetNumberOfCells());
    zoneArr->Fill(Invert ? 100. : 0.);
    outGrid->GetCellData()->AddArray(zoneArr);

    // generate multi-polygon structure from boundary polygons
    zfbMultiPolygon bBoundPoly;
    vtkPoints* pts = outPoly->GetPoints();
    for(vtkIdType p = 0; p < outPoly->GetNumberOfCells(); ++p)
    {
        vtkCell* cell = outPoly->GetCell(p);
        if((nullptr == cell) && (2 < cell->GetPointIds()->GetNumberOfIds()))
            continue;

        // collect the points into a vector
        std::vector<zfbPair> plist;
        for(vtkIdType n = 0; n < cell->GetPointIds()->GetNumberOfIds(); ++n)
        {
            double pt[3];
            pts->GetPoint(cell->GetPointId(n), pt);
            plist.push_back(zfbPair({pt[0], pt[1]}));
        }

        // close if not the case yet
        vtkIdType n0 = cell->GetPointId(0);
        if(n0 != cell->GetPointId(cell->GetPointIds()->GetNumberOfIds() - 1))
            plist.push_back(plist[0]);

        // generate a "ring" from the point vector
        zfbRing ring(plist.begin(), plist.end());

        // check orientation and reverse if necessary
        double ar = bg::area(ring);
        if(ar < 0.)
            bg::reverse(ring);

        // use the "ring" as the "exterior ring" of a "polygon"
        // note: other rings would be holes and have to turn the other way round
        zfbPolygon poly;
        bg::exterior_ring(poly) = ring;

        // append to the multi-polygon
        bBoundPoly.push_back(poly);
    }

    // optimization of the following: take the "ring" of every block as a key into
    // a map of already calculated zone values - to avoid recalculating the intersection
    // at every block level again and again
    std::map<zfbRing, double, zfbRingLess> zoneValMap;

    // handle all cells of the block model
    pts = inputGrid->GetPoints();
    for(vtkIdType b = 0; b < inputGrid->GetNumberOfCells(); ++b)
    {
        vtkHexahedron* blk = vtkHexahedron::SafeDownCast(inputGrid->GetCell(b));
        if(nullptr == blk)
            continue;

        // collect the first 5 points of the hexahedron into a vector
        std::vector<zfbPair> plist;
        for(vtkIdType n = 0; n < 5; ++n)
        {
            double pt[3];
            pts->GetPoint(blk->GetPointId(n), pt);
            plist.push_back(zfbPair({pt[0], pt[1]}));
        }

        // generate a ring from the cell points
        // note: the block (cell) points are always anti-clockwise so we need
        // to reverse in any case
        // make also sure the rectangle is really really closed
        zfbRing ring(plist.begin(), plist.end());
        bg::reverse(ring);
        ring[ring.size() - 1] = ring[0];

        // see if we already have the zone value
        auto zit = zoneValMap.find(ring);
        if(zoneValMap.end() != zit)
        {
            zoneArr->SetValue(b, Invert ? 100. - zit->second : zit->second);
            continue;
        }

        // note that we must wrap this square into a multi-polygon because
        //   otherwise the intersection function does not work properly
        zfbPolygon poly;
        bg::exterior_ring(poly) = ring;
        zfbMultiPolygon bCellPoly;
        bCellPoly.push_back(poly);

        // calculate the intersection multi-polygon of boundary and block
        zfbMultiPolygon bResPoly;
        bg::intersection(bBoundPoly, bCellPoly, bResPoly);

        // calculate the covered proportion (percent) for the zone value
        double zv = 100. * bg::area(bResPoly) / bg::area(ring);

        zoneArr->SetValue(b, Invert ? 100. - zv : zv);
        zoneValMap[ring] = zv;
    }

    // make the new zone the active attribute
    outGrid->GetCellData()->SetActiveScalars(nameZone.c_str());

    return 1;
}

int vtkAtgZoneFromBoundary::RequestInformation(vtkInformation* request,
                                                   vtkInformationVector** inputVector,
                                                   vtkInformationVector* outputVector)
{
    (void)request;
    (void)inputVector;
    (void)outputVector;

    return 1;
}

void vtkAtgZoneFromBoundary::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
