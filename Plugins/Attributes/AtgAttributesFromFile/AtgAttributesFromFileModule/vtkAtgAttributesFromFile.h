/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgAttributesFromFile.h

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgAttributesFromFile_h
#define vtkAtgAttributesFromFile_h

#include <string>
#include <map>
#include <list>
#include <vtkPointSetAlgorithm.h>
#include <AtgAttributesFromFileModuleModule.h>

class vtkCellData;
class vtkAbstractArray;
class vtkTable;
class vtkDataSetAttributes;

class ATGATTRIBUTESFROMFILEMODULE_EXPORT vtkAtgAttributesFromFile: public vtkPointSetAlgorithm
{
public:

    static vtkAtgAttributesFromFile* New();
    vtkTypeMacro(vtkAtgAttributesFromFile, vtkPointSetAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetStringMacro(AttributesFile)
    vtkGetStringMacro(AttributesFile)

    vtkSetStringMacro(FieldDelimiterCharacters)
    vtkGetStringMacro(FieldDelimiterCharacters)

    vtkSetMacro(AddTabFieldDelimiter, bool)
    vtkGetMacro(AddTabFieldDelimiter, bool)

    vtkSetStringMacro(CellId)
    vtkGetStringMacro(CellId)

    vtkSetStringMacro(EmptyCellString)
    vtkGetStringMacro(EmptyCellString)

    vtkSetMacro(RemoveExistingAttributes, bool)
    vtkGetMacro(RemoveExistingAttributes, bool)

    vtkSetMacro(EmptyCellValue, double)
    vtkGetMacro(EmptyCellValue, double)

protected:

    vtkAtgAttributesFromFile();
    ~vtkAtgAttributesFromFile();

    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);
    int RequestDataObject(vtkInformation*, vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:

    // copy constructor and assignment not implemented
    vtkAtgAttributesFromFile(vtkAtgAttributesFromFile const&);
    void operator=(vtkAtgAttributesFromFile const&);

    // prepare attribute names in the same way as the block model reader does it,
    // with N_ prepended to string attributes that are not categories, dates or
    // already names - in order to make them all names
    void prepareNameAttributes(vtkTable* tab);

    // remove existing attributes from cell data, except functional types for block models etc.
    void removeAttributes(vtkCellData* data);

    // assign attributes from table to cells, except functional types for block models etc.
    void assignAttributes(vtkCellData* data, vtkAbstractArray* keyArr, vtkTable* atts,
                          std::map<std::string, vtkIdType>& keyMap);

    // get list of non functional attributes from cell data or table
    void getNonFunctionalAttributes(vtkDataSetAttributes* data, std::list<std::string>& att);

    char *AttributesFile,
         *FieldDelimiterCharacters,
         *CellId,
         *EmptyCellString;
    bool AddTabFieldDelimiter,
         RemoveExistingAttributes;
    double EmptyCellValue;

};

#endif
