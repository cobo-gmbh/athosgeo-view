/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgAttributesFromFile.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <set>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/iterator/transform_iterator.hpp>
#include <utilNormNames.h>
#include <vtkSmartPointer.h>
#include <vtkPointSet.h>
#include <vtkTable.h>
#include <vtkCellData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkAtgCsvReader.h>
#include <vtkAtgNormalizeNames.h>
#include <vtkAtgAttributesFromFile.h>

vtkStandardNewMacro(vtkAtgAttributesFromFile)

vtkAtgAttributesFromFile::vtkAtgAttributesFromFile()
:   AttributesFile(nullptr),
    FieldDelimiterCharacters(nullptr),
    CellId(nullptr),
    EmptyCellString(nullptr),
    AddTabFieldDelimiter(false),
    RemoveExistingAttributes(true),
    EmptyCellValue(-1.)
{
    SetEmptyCellString("--");
}

vtkAtgAttributesFromFile::~vtkAtgAttributesFromFile()
{
    SetAttributesFile(nullptr);
    SetFieldDelimiterCharacters(nullptr);
    SetCellId(nullptr);
    SetEmptyCellString(nullptr);
}

int vtkAtgAttributesFromFile::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // any data object
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAttributesFromFile::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAttributesFromFile::RequestDataObject(vtkInformation*,
                                                vtkInformationVector** inputVector,
                                                vtkInformationVector* outputVector)
{
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkPointSet* output = vtkPointSet::SafeDownCast(info->Get(vtkPointSet::DATA_OBJECT()));

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkPointSet* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgAttributesFromFile::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    // check if we have a file name
    if(nullptr == AttributesFile)
    {
        vtkOutputWindowDisplayWarningText("Please specify an attribute file\n");
        return 1;
    }

    // check if we have a key attribute
    if(nullptr == CellId)
    {
        vtkOutputWindowDisplayWarningText("Please specify a key attribute name\n");
        return 1;
    }

    // the input unstructured grid
    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("No valid input block model found\n");
        return 1;
    }

    // get the output grid and shallow copy the input block model
    vtkPointSet* output = vtkPointSet::GetData(outputVector);
    output->ShallowCopy(input);

    // check if the key attribute is unique in the input grid and the array is not multi-component
    vtkAbstractArray* inKeyArr = input->GetCellData()->GetAbstractArray(CellId);
    if(nullptr == inKeyArr)
    {
        std::string msg = std::string("Attribute <") +
                          CellId + "> not found in the input model\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }
    if(1 < inKeyArr->GetNumberOfComponents())
    {
        std::string msg = std::string("The key attribute <") +
                          CellId + "> must not have more than one component\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }
    bool hasDuplicates = false;
    std::set<std::string> inKeySet;
    for(vtkIdType row = 0; row < inKeyArr->GetNumberOfValues(); ++row)
    {
        // note that we will add all the keys to the set even if we found duplicates:
        // if we do, we will warn the user, but theoretically he can assign values also
        // for entire groups of cells
        std::string key = inKeyArr->GetVariantValue(row).ToString();
        if(inKeySet.end() != inKeySet.find(key))
            hasDuplicates = true;
        inKeySet.insert(key);
    }
    if(hasDuplicates)
    {
        std::string msg = std::string("Warning: The key attribute <") +
                          CellId + "> in the input grid contains duplicate values:\n"
                          "With this, values will be assigned to multiple cells/blocks having the same key.\n"
                          "Note: This may indeed be what you want, so please ignore this warning in such a case!\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
    }

    // read the file into a vtkTable, using the user specified properties
    auto reader = vtkSmartPointer<vtkAtgCsvReader>::New();
    reader->SetFileName(AttributesFile);
    reader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
    reader->SetEmptyStringReplacement(EmptyCellString);
    reader->SetAddTabFieldDelimiter(AddTabFieldDelimiter);
    reader->SetEmptyNumberReplacement(EmptyCellValue);
    reader->SetTreatExcelErrorsAsEmpty(true);
    reader->SetRemoveLastLineIfLessCellsThanHeader(true);
    reader->Update();

    // fatal reading errors
    if(nullptr != reader->GetErrorMessage())
    {
        std::string msg = std::string("Fatal error during file reading:\n")
                        + reader->GetErrorMessage();
        return 1;
    }

    // if we have no rows or columns, this is fatal
    if(0 == reader->GetOutput()->GetNumberOfColumns())
    {
        std::string msg = "Input file has no columns\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }
    if(0 == reader->GetOutput()->GetNumberOfRows())
    {
        std::string msg = "Input file has no rows\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // we treat reading errors of single values like "empty cells" and report them as warning
    if(nullptr != reader->GetWarningMessages())
    {
        std::string msg = std::string("Warning:\n") + reader->GetWarningMessages();
        vtkOutputWindowDisplayWarningText(msg.c_str());
    }

    /*
    vtkSmartPointer<vtkDelimitedTextReader> reader = vtkSmartPointer<vtkDelimitedTextReader>::New();
    reader->SetFileName(AttributesFile);
    reader->SetHaveHeaders(true);
    reader->DetectNumericColumnsOn();
    reader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
    reader->SetAddTabFieldDelimiter(AddTabFieldDelimiter);
    reader->SetMergeConsecutiveDelimiters(false);
    reader->SetUseStringDelimiter(true);
    reader->SetStringDelimiter('\"');
    reader->TrimWhitespacePriorToNumericConversionOn();
    reader->ForceDoubleOn();
    reader->Update();
    if(!reader->GetLastError().empty())
    {
        vtkOutputWindowDisplayWarningText(("The CSV table reader found an error:\n" +
                                           reader->GetLastError()).c_str());
        return 1;
    }
    */

    // normalize the column names, including upper/lower case
    vtkSmartPointer<vtkAtgNormalizeNames> colNorm = vtkSmartPointer<vtkAtgNormalizeNames>::New();
    colNorm->SetIncludeComponent(true);
    colNorm->SetInputData(reader->GetOutput());
    colNorm->Update();
    vtkTable* newAttributes = colNorm->GetOutput();

    // make sure we have N_ prepended if necessary
    prepareNameAttributes(newAttributes);

    // report error during column name normalization
    if(nullptr != colNorm->GetError())
    {
        std::string err = std::string("Error during column name normalization:\n") + colNorm->GetError();
        vtkOutputWindowDisplayWarningText(err.c_str());
        return 1;
    }

    // see if we have our key also in the new attributes
    vtkAbstractArray* newKeyArr = newAttributes->GetColumnByName(CellId);
    if(nullptr == newKeyArr)
    {
        std::string msg = std::string("Attribute <") +
                          CellId + "> not found in the new attributes table (CSV)\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // collect the keys in a map structure that points to the row number
    // and at the same time also check for duplicates
    hasDuplicates = false;
    std::map<std::string, vtkIdType> newKeyMap;
    for(vtkIdType row = 0; !hasDuplicates && (row < newKeyArr->GetNumberOfValues()); ++row)
    {
        // note that here we will NOT add all the keys to the map if we found duplicates
        // because that is a reason to abandon the process
        std::string key = newKeyArr->GetVariantValue(row).ToString();
        if(newKeyMap.end() != newKeyMap.find(key))
            hasDuplicates = true;
        else
            newKeyMap[key] = row;
    }
    if(hasDuplicates)
    {
        std::string msg = std::string("Warning: The key attribute <") +
                          CellId + "> in the new attributes file (CSV) contains duplicate values:\n"
                          "Please make sure that this is not the case and try again.\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // find out if all the key values in the grid have their counterpart in the
    // new attributes table
    // note: the below loop could be replaced by a std::set_difference, but I did not
    //   manage to get this working with a set and a map...
    std::set<std::string> notFoundKeys;
    for(auto git = inKeySet.begin(); git != inKeySet.end(); ++git)
    {
        if(newKeyMap.end() == newKeyMap.find(*git))
            notFoundKeys.insert(*git);
    }
    if(!notFoundKeys.empty())
    {
        std::string msg = std::to_string(notFoundKeys.size()) + " attribute <" +
                          CellId + "> values in the grid could not be found in the new attributes table (CSV)\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // remove all existing cell attributes that are not functional for the model
    // - if this is what the user has chosen
    if(RemoveExistingAttributes)
        removeAttributes(output->GetCellData());

    // and now for all - apply the new attributes to the output
    assignAttributes(output->GetCellData(), inKeyArr, newAttributes, newKeyMap);

    return 1;
}

void vtkAtgAttributesFromFile::prepareNameAttributes(vtkTable* tab)
{
    for(vtkIdType col = 0; col < tab->GetNumberOfColumns(); ++col)
    {
        vtkAbstractArray* arr = tab->GetColumn(col);
        if(VTK_STRING != arr->GetDataType())
           continue;

        // if a string array is not in the type "category" we prepend a N_ (to bring it there)
        switch(utilNormNames::getType(arr->GetName()))
        {
            case utilNormNames::TY_NAME:
            case utilNormNames::TY_DATE:
            case utilNormNames::TY_CATEGORY:
            {
                // these are already ok
                break;
            }
            default:
            {
                arr->SetName((std::string("N_") + arr->GetName()).c_str());
            }
        }
    }
}

void vtkAtgAttributesFromFile::removeAttributes(vtkCellData* data)
{
    // get a list of those attributes that are not functional for the geometry etc.
    std::list<std::string> attList;
    getNonFunctionalAttributes(vtkDataSetAttributes::SafeDownCast(data), attList);

    // remove all the attributes that are not geometric
    for(auto it = attList.begin(); it != attList.end(); ++it)
        data->RemoveArray(it->c_str());
}

void vtkAtgAttributesFromFile::assignAttributes(vtkCellData* data,
                                                vtkAbstractArray* keyArr, vtkTable* atts,
                                                std::map<std::string, vtkIdType>& keyMap)
{
    // get a list of those attributes that are not functional for the geometry etc.
    std::list<std::string> attList;
    getNonFunctionalAttributes(atts->GetRowData(), attList);

    // go through all the arrays
    for(auto it = attList.begin(); it != attList.end(); ++it)
    {
        // get the input data array and generate a new one of same type
        vtkAbstractArray* tabArr = atts->GetColumnByName(it->c_str());
        vtkAbstractArray* arr = tabArr->NewInstance();
        arr->SetName(it->c_str());
        arr->SetNumberOfValues(data->GetNumberOfTuples());

        // fill in the data
        for(vtkIdType row = 0; row < keyArr->GetNumberOfValues(); ++row)
        {
            std::string key = keyArr->GetVariantValue(row).ToString();
            vtkIdType dataRow = keyMap[key];
            vtkVariant val = tabArr->GetVariantValue(dataRow);
            arr->SetVariantValue(row, val);
        }

        // add the array to the data, and "delete" the current reference
        data->AddArray(arr);
        arr->Delete();
    }
}

void vtkAtgAttributesFromFile::getNonFunctionalAttributes(vtkDataSetAttributes* data,
                                                          std::list<std::string>& att)
{
    // go through the columns and check their types/categories
    for(int col = 0; col < data->GetNumberOfArrays(); ++col)
    {
        std::string colName = data->GetArrayName(col);

        // these are all the attributes that are not geometry related
        switch(utilNormNames::getType(colName))
        {
            case utilNormNames::TY_NAME:
            case utilNormNames::TY_CATEGORY:
            case utilNormNames::TY_TONNAGE:
            case utilNormNames::TY_RATIO:
            case utilNormNames::TY_PERIOD:
            case utilNormNames::TY_WEIGHT:
            case utilNormNames::TY_SPECTONNAGE:
            case utilNormNames::TY_DERIVED:
            case utilNormNames::TY_DIRECT:
            {
                // we do not expect arrays with multiple components, but here
                // we exclude them explicitly - just to make sure
                if(1 == data->GetAbstractArray(colName.c_str())->GetNumberOfComponents())
                    att.push_back(colName);
            }
        }
    }
}

void vtkAtgAttributesFromFile::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
