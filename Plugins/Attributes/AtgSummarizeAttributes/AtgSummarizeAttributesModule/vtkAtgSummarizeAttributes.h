/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummarizeAttributes.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgSummarizeAttributes_h
#define vtkAtgSummarizeAttributes_h

#include <vector>
#include <set>
#include <string>
#include <vtkTableAlgorithm.h>
#include <AtgSummarizeAttributesModuleModule.h>

class ATGSUMMARIZEATTRIBUTESMODULE_EXPORT vtkAtgSummarizeAttributes: public vtkTableAlgorithm
{
public:

    static vtkAtgSummarizeAttributes* New();
    vtkTypeMacro(vtkAtgSummarizeAttributes, vtkTableAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetStringMacro(Tonnage)
    vtkGetStringMacro(Tonnage)

    vtkSetMacro(UsePit, bool)
    vtkGetMacro(UsePit, bool)

    vtkSetStringMacro(Pit)
    vtkGetStringMacro(Pit)

    vtkSetMacro(UseCategory, bool)
    vtkGetMacro(UseCategory, bool)

    vtkSetStringMacro(Category)
    vtkGetStringMacro(Category)

    vtkSetMacro(IncludeCementModuli, bool)
    vtkGetMacro(IncludeCementModuli, bool)

    // convenience method to specify the selection connection (2nd input port)
    void SetSelectionConnection(vtkAlgorithmOutput* algOutput)
    {
        SetInputConnection(1, algOutput);
    }

    // required helper for dealing with the selection input
    void RemoveAllSelectionsInputs()
    {
        SetInputConnection(1, nullptr);
    }

    vtkSetMacro(ApplyToSelection, bool)
    vtkGetMacro(ApplyToSelection, bool)

    void SetKeepQuiet(bool kq = true);
    bool GetKeepQuiet() const;

protected:

    vtkAtgSummarizeAttributes();
    ~vtkAtgSummarizeAttributes();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgSummarizeAttributes(vtkAtgSummarizeAttributes const&);
    void operator=(vtkAtgSummarizeAttributes const&);

    // attributes as received from the user interface
    bool UsePit,
         UseCategory,
         IncludeCementModuli;
    char *Tonnage,
         *Pit,
         *Category;

    // option for the application in the summary view: no messages
    bool KeepQuiet;

    // block ids to be marked
    bool ApplyToSelection;
    typedef std::set<int> idSet;
    idSet Ids;

    // attributes used for normalization
    // note: this will normally be tonnage, but can be also taken,
    // or tonnage and pit etc.
    std::vector<std::string> normAtts;

};

#endif
