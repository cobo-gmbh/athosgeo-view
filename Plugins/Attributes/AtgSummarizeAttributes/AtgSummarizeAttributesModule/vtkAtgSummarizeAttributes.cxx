/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSummarizeAttributes.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vector>
#include <string>
#include <vtkSmartPointer.h>
#include <vtkDataSet.h>
#include <vtkDataObject.h>
#include <vtkDataSetAttributes.h>
#include <vtkTable.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkLongLongArray.h>
#include <vtkStringArray.h>
#include <vtkVariant.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkPartitionedDataSet.h>
#include <vtkPConvertSelection.h>
#include <vtkIdTypeArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <utilNormNames.h>
#include <utilSummarizeAttributes.h>
#include <vtkAtgSummarizeAttributes.h>

vtkStandardNewMacro(vtkAtgSummarizeAttributes)

vtkAtgSummarizeAttributes::vtkAtgSummarizeAttributes()
:	UsePit(false),
    UseCategory(false),
    IncludeCementModuli(true),
    Tonnage(nullptr),
    Pit(nullptr),
    Category(nullptr),
    KeepQuiet(false),
    ApplyToSelection(false)
{
    // input data and selection (if any)
    SetNumberOfInputPorts(2);
}

vtkAtgSummarizeAttributes::~vtkAtgSummarizeAttributes()
{
    SetCategory(nullptr);
    SetPit(nullptr);
    SetTonnage(nullptr);
}

int vtkAtgSummarizeAttributes::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataObject");
            break;
        case 1:
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkSelection");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
            break;
    }
    return 1;
}

int vtkAtgSummarizeAttributes::FillOutputPortInformation(int port, vtkInformation* info)
{
    (void)port;

    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
    return 1;
}

int vtkAtgSummarizeAttributes::RequestData(vtkInformation* request,
                                           vtkInformationVector** inputVector,
                                           vtkInformationVector* outputVector)
{
    (void)request;

    // get the input data object
    vtkDataObject* inDataObject = vtkDataObject::GetData(inputVector[0]);
    if(nullptr == inDataObject)
    {
        if(!KeepQuiet)
            vtkOutputWindowDisplayWarningText("AtgSummarizeAttributes: No input data\n");
        return 1;
    }

    // in the summary view, we will receive the data as a "partitioned data set", with
    // the table of interest being the first data object
    if(VTK_PARTITIONED_DATA_SET == inDataObject->GetDataObjectType())
    {
        vtkPartitionedDataSet* pds = vtkPartitionedDataSet::SafeDownCast(inDataObject);
        if(0 != pds->GetNumberOfPartitions())
            inDataObject = pds->GetPartitionAsDataObject(0);
    }

    // the real data is in a data set attributes object
    // and this can either come from a data table...
    // ...or from the cell data of an unstructured grid
    vtkDataSetAttributes* input = nullptr;
    switch(inDataObject->GetDataObjectType())
    {
        case VTK_UNSTRUCTURED_GRID:
            input = inDataObject->GetAttributes(vtkDataObject::CELL);
            break;
        case VTK_TABLE:
            input = inDataObject->GetAttributes(vtkDataObject::ROW);
            break;
        default:
            if(!KeepQuiet)
                vtkOutputWindowDisplayWarningText("AtgSummarizeAttributes: "
                        "Input data must be either a block grid (unstructured grid) "
                        "or a table\n");
            return 1;
    }

    // get the input selection
    vtkSelection* selIn = vtkSelection::GetData(inputVector[1]);
    Ids.clear();
    if(ApplyToSelection)
    {
        if(nullptr == selIn)
        {
            if(!KeepQuiet)
                vtkOutputWindowDisplayWarningText("AtgSummarizeAttributes: Apply to selection, but nothing selected\n");
            return 1;
        }

        // we want indices
        vtkSmartPointer<vtkPConvertSelection> selConv = vtkSmartPointer<vtkPConvertSelection>::New();
        selConv->SetInputData(selIn);
        selConv->SetInputData(1, inDataObject);
        selConv->SetOutputType(vtkSelectionNode::INDICES);
        selConv->Update();
        vtkSelection* selInx = selConv->GetOutput();

        // no, this does not work: it does not consider QUERY selections - which seem to be a
        // PV feature only (based on Python), not a VTK feature! (even if the node type is defined
        // in a VTK class - vtkSelectionNode)
        //vtkSelection* selInx = vtkPConvertSelection::ToIndexSelection(selIn, inDataObject);

        if(0 < selInx->GetNumberOfNodes())
        {
            vtkIdTypeArray* arr = vtkIdTypeArray::SafeDownCast(selInx->GetNode(0)->GetSelectionList());
            if(nullptr != arr)
            {
                for(vtkIdType i = 0; i < arr->GetNumberOfValues(); ++i)
                    Ids.insert(arr->GetValue(i));
            }
        }
    }

    // get the output table
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkTable* output = vtkTable::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // define the normalization attributes: either one "tonnage" type attribute,
    // or else a tonnage plus a "pit" type attribute
    normAtts.clear();
    std::string normTonnage;

    if(nullptr != Tonnage)
        normTonnage = Tonnage;
    if(normTonnage.empty())
    {
        static const std::vector<std::string> defs(std::initializer_list<std::string>(
        {
            utilNormNames::getName(utilNormNames::KTONS),
            utilNormNames::getName(utilNormNames::TONS),
            utilNormNames::getName(utilNormNames::TAKENTOTAL)
        }));
        for(auto it = defs.begin(); normTonnage.empty() && (it != defs.end()); ++it)
        {
            for(vtkIdType c = 0; c < input->GetNumberOfArrays(); ++c)
            {
                if(*it == input->GetArrayName(c))
                {
                    normTonnage = input->GetArrayName(c);
                    break;
                }
            }
        }
    }
    if(normTonnage.empty())
    {
        if(!KeepQuiet)
        {
            std::string err = "No \'Tonnage\' (weight) attribute chosen for summary";
            vtkOutputWindowDisplayWarningText(("AtgSummarizeAttributes: " + err + "\n").c_str());
        }
        return 1;
    }
    normAtts.push_back(normTonnage);
    if(UsePit)
    {
        std::string normPit = Pit;
        if(normPit.empty())
        {
            if(!KeepQuiet)
            {
                std::string err = "\'UsePit\' option chosen, but no \'Pit\' attribute chosen";
                vtkOutputWindowDisplayWarningText(("AtgSummarizeAttributes: " + err + "\n").c_str());
            }
            return 1;
        }
        normAtts.push_back(normPit);
    }

    // if we are working on selected cells only, we need to retrieve
    // a list of the selected ids
    if(ApplyToSelection && Ids.empty())
    {
        if(!KeepQuiet)
        {
            std::string err = "The \'Apply to selection only\' option is chosen, but no "
                              "blocks are selected: Nothing to summarize!";
            vtkOutputWindowDisplayWarningText(("AtgSummarizeAttributes: " + err + "\n").c_str());
        }
        return 1;
    }

    // do the summarizing
    utilSummarizeAttributes summ(input);
    summ.setNormalization(normAtts);
    if(UseCategory)
        summ.setCategory(Category);
    summ.setSelectedOnly(ApplyToSelection);
    summ.setSelectedIds(Ids);
    summ.setIncludeCementModuli(IncludeCementModuli);
    summ.summarize(output);

    // display error if there is one
    if(!summ.error().empty())
    {
        if(!KeepQuiet)
            vtkOutputWindowDisplayWarningText(("AtgSummarizeAttributes: " + summ.error() + "\n").c_str());
        return 1;
    }

    return 1;
}

void vtkAtgSummarizeAttributes::SetKeepQuiet(bool kq)
{
    KeepQuiet = kq;
}

bool vtkAtgSummarizeAttributes::GetKeepQuiet() const
{
    return KeepQuiet;
}

int vtkAtgSummarizeAttributes::RequestInformation(vtkInformation* request,
                                                  vtkInformationVector** inputVector,
                                                  vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgSummarizeAttributes::PrintSelf(std::ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
