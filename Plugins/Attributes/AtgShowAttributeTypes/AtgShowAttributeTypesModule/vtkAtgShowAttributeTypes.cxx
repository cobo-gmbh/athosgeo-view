/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgShowAttributeTypes.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <utilNormNames.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkTable.h>
#include <vtkStringArray.h>
#include <vtkDoubleArray.h>
#include <vtkDataSet.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgShowAttributeTypes.h>

vtkStandardNewMacro(vtkAtgShowAttributeTypes)

vtkAtgShowAttributeTypes::vtkAtgShowAttributeTypes()
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgShowAttributeTypes::~vtkAtgShowAttributeTypes()
{
}

int vtkAtgShowAttributeTypes::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // we accept any data object
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataObject");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgShowAttributeTypes::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // will be same data type as input
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgShowAttributeTypes::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    // get the data object
    vtkDataObject* input = vtkDataObject::GetData(inputVector[0]);
    vtkTable* output = vtkTable::GetData(outputVector);

    // just to make sure - should not happen normally
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("AtgShowAttributeTypes: No valid input data object");
        return 1;
    }
    if(nullptr == output)
    {
        vtkOutputWindowDisplayWarningText("AtgShowAttributeTypes: No valid output data object");
        return 1;
    }

    // see whether we have a table or another data set: only these two can be accepted
    vtkTable* tabIn = vtkTable::SafeDownCast(input);
    vtkDataSet* dsIn = vtkDataSet::SafeDownCast(input);
    if((nullptr == tabIn) && (nullptr == dsIn))
    {
        vtkOutputWindowDisplayWarningText
                ("AtgShowAttributeTypes: Input datatype must be \'vtkTable\' or derived from \'vtkDataSet\'");
        return 1;
    }

    // common class for data inside tables and other data sets: we first try either table
    // or cell data - which would be the right thing for a block model
    vtkDataSetAttributes* dsaIn = nullptr;
    if(nullptr != tabIn)
        dsaIn = tabIn->GetRowData();
    else
        dsaIn = vtkDataSetAttributes::SafeDownCast(dsIn->GetCellData());

    // add columns to the table
    vtkSmartPointer<vtkStringArray> arrNameCol = vtkSmartPointer<vtkStringArray>::New(),
                                    arrTypeCol = vtkSmartPointer<vtkStringArray>::New(),
                                    arrDataTypeCol = vtkSmartPointer<vtkStringArray>::New();
    vtkSmartPointer<vtkDoubleArray> arrValueMin = vtkSmartPointer<vtkDoubleArray>::New(),
                                    arrValueMax = vtkSmartPointer<vtkDoubleArray>::New();
    arrNameCol->SetName("Attribute Name");
    arrTypeCol->SetName("Attribute Type");
    arrDataTypeCol->SetName("Data Type");
    arrValueMin->SetName("Min Value");
    arrValueMax->SetName("Max Value");
    int numAtts = dsaIn->GetNumberOfArrays();
    arrNameCol->SetNumberOfTuples(numAtts);
    arrTypeCol->SetNumberOfTuples(numAtts);
    arrDataTypeCol->SetNumberOfTuples(numAtts);
    arrValueMin->SetNumberOfTuples(numAtts);
    arrValueMax->SetNumberOfTuples(numAtts);
    arrValueMin->Fill(0.);
    arrValueMax->Fill(0.);

    // go through the arrays
    for(int a = 0; a < dsaIn->GetNumberOfArrays(); ++a)
    {
        // all arrays have a name or a type
        vtkAbstractArray* arr = dsaIn->GetAbstractArray(a);
        arrNameCol->SetValue(a, arr->GetName());
        arrTypeCol->SetValue(a, utilNormNames::getTypeDesc(arr->GetName()).c_str());
        arrDataTypeCol->SetValue(a, arr->GetDataTypeAsString());

        // numeric arrays have a min and max value
        vtkDataArray* darr = vtkDataArray::SafeDownCast(arr);
        if(nullptr != darr)
        {
            double range[2];
            darr->GetRange(range);
            arrValueMin->SetValue(a, range[0]);
            arrValueMax->SetValue(a, range[1]);
        }
    }

    // insert the new columns into the output table
    output->AddColumn(arrNameCol);
    output->AddColumn(arrTypeCol);
    output->AddColumn(arrDataTypeCol);
    output->AddColumn(arrValueMin);
    output->AddColumn(arrValueMax);

    return 1;
}

int vtkAtgShowAttributeTypes::RequestInformation(vtkInformation* request,
                                                 vtkInformationVector** inputVector,
                                                 vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgShowAttributeTypes::PrintSelf(std::ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
