/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCementModuli.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vector>
#include <string>
#include <utilDerivedValues.h>
#include <vtkSmartPointer.h>
#include <vtkDataSet.h>
#include <vtkTable.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgCementModuli.h>

vtkStandardNewMacro(vtkAtgCementModuli)

vtkAtgCementModuli::vtkAtgCementModuli()
:   NoErrorMessages(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgCementModuli::~vtkAtgCementModuli()
{
}

int vtkAtgCementModuli::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // any data object
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataObject");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCementModuli::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkDataObject");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCementModuli::RequestDataObject(vtkInformation*,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkDataObject* input = vtkDataObject::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkDataObject* output = info->Get(vtkDataObject::DATA_OBJECT());

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkDataObject* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgCementModuli::RequestData(vtkInformation* request,
                                    vtkInformationVector** inputVector,
                                    vtkInformationVector* outputVector)
{
    // get the data object
    vtkDataObject* input = vtkDataObject::GetData(inputVector[0]);
    vtkDataObject* output = vtkDataObject::GetData(outputVector);

    // just to make sure - should not happen normally
    if(nullptr == input)
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText("AtgCementModuli: No valid input data object");
        return 1;
    }
    if(nullptr == output)
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText("AtgCementModuli: No valid output data object");
        return 1;
    }
    if(std::string(input->GetClassName()) != output->GetClassName())
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText("AtgCementModuli: Output must match the input data type");
        return 1;
    }

    // as a first step we make a shallow copy of the input - no matter what specific data type
    output->ShallowCopy(input);

    // see whether we have a table or another data set: only these two can be accepted
    vtkTable* tabOut = vtkTable::SafeDownCast(output);
    vtkDataSet* dsOut = vtkDataSet::SafeDownCast(output);
    if((nullptr == tabOut) && (nullptr == dsOut))
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText("AtgCementModuli: Datatype must be \'vtkTable\' or derived from \'vtkDataSet\'");
        return 1;
    }

    // common class for data inside tables and other data sets: we first try either table
    // or cell data - which would be the right thing for a block model
    vtkDataSetAttributes* dsaOut = nullptr;
    if(nullptr != tabOut)
        dsaOut = tabOut->GetRowData();
    else
        dsaOut = vtkDataSetAttributes::SafeDownCast(dsOut->GetCellData());

    // if we have no data now and the in/output data is vtkDataSet, we can still see if we have point data
    if((0 == dsaOut->GetNumberOfTuples()) && (nullptr != dsOut))
        dsaOut = vtkDataSetAttributes::SafeDownCast(dsOut->GetPointData());

    // if now we have no data, we really have nothing to process!
    if(0 == dsaOut->GetNumberOfTuples())
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText("AtgCementModuli: No data found for processing");
        return 1;
    }

    // see if there are potential derived values to be calculated
    utilDerivedValues cdv(dsaOut);
    std::vector<std::string> derivedVec = cdv.derivedValues(false);
    if(derivedVec.empty())
    {
        if(!NoErrorMessages)
            vtkOutputWindowDisplayWarningText(
                "AtgCementModuli: No suitable input attributes (oxides) for calculating derived values");
        return 1;
    }

    // now we add the derived value columns
    // note: we do not care about any pre-existing derived attributes: we always
    //   simply calculate all that are possible from the direct attributes
    for(auto it = derivedVec.begin(); it != derivedVec.end(); ++it)
    {
        vtkSmartPointer<vtkDoubleArray> derArr = vtkSmartPointer<vtkDoubleArray>::New();
        derArr->SetNumberOfTuples(dsaOut->GetNumberOfTuples());
        derArr->SetName(it->c_str());
        dsaOut->AddArray(derArr);

        for(vtkIdType b = 0; b < derArr->GetNumberOfTuples(); ++b)
        {
            double val = cdv.calculate(*it, b);
            derArr->SetValue(b, val);
        }
    }

    // make the first available cement module the active scalar
    dsaOut->SetActiveScalars(derivedVec.front().c_str());

    return 1;
}

void vtkAtgCementModuli::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
