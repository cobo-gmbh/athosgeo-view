/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgWriteValue.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <vtkSmartPointer.h>
#include <vtkPointSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkStringArray.h>
#include <vtkVariant.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgWriteValue.h>

vtkStandardNewMacro(vtkAtgWriteValue)

vtkAtgWriteValue::vtkAtgWriteValue()
:   TargetArrayName(nullptr),
    NewValue(nullptr),
    FillerValue(nullptr),
    ApplyToAll(false),
    NoErrors(false)
{
}

vtkAtgWriteValue::~vtkAtgWriteValue()
{
    SetFillerValue(nullptr);
    SetNewValue(nullptr);
    SetTargetArrayName(nullptr);
}

void vtkAtgWriteValue::AddId(int id)
{
    Ids.insert(id);
    Modified();
}

void vtkAtgWriteValue::ClearIds()
{
    Ids.clear();
    Modified();
}

int vtkAtgWriteValue::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgWriteValue::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgWriteValue::RequestDataObject(vtkInformation*,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    // generate data objects of the same data type as the input data object
    // note: with this it is possible that a filter can return different
    //   data types depending on what the input data is
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkPointSet* output = vtkPointSet::SafeDownCast(info->Get(vtkDataObject::DATA_OBJECT()));

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkPointSet* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgWriteValue::RequestData(vtkInformation* request,
                                  vtkInformationVector** inputVector,
                                  vtkInformationVector* outputVector)
{
    // get the input and output point sets
    // note that this would normally be either unstructured grids or polydata
    vtkPointSet* input = vtkPointSet::GetData(inputVector[0], 0);
    vtkPointSet* output = vtkPointSet::GetData(outputVector, 0);
    output->ShallowCopy(input);

    // if this is the initialization call, we are done already here
    if(NoErrors)
        return 1;

    // make sure we have a value to apply
    if(nullptr == NewValue)
    {
        vtkOutputWindowDisplayWarningText("AtgWriteValue: No value or string specified\n");
        return 1;
    }

    // split into array and component name - if applicable
    std::vector<std::string> arrParts;
    std::string tan(TargetArrayName);
    boost::trim(tan);
    boost::split(arrParts, tan, boost::is_any_of(":"));

    // we must have one or two parts now
    if((1 > arrParts.size()) || (2 < arrParts.size()) ||
       (arrParts[0].empty()) || ((2 == arrParts.size()) && (arrParts[1].empty())))
    {
        if(!NoErrors)
            vtkOutputWindowDisplayWarningText((std::string("AtgWriteValue: No valid array name found: ") +
                                               TargetArrayName + "\n").c_str());
        return 1;
    }
    std::string arrName = arrParts[0],
                compName = (1 < arrParts.size()) ? arrParts[1] : "";

    // try to find an existing array with the chosen attribute name
    vtkAbstractArray* aarr = nullptr;
    for(vtkIdType a = 0; a < output->GetCellData()->GetNumberOfArrays(); ++a)
    {
        aarr = output->GetCellData()->GetAbstractArray(a);
        if(nullptr == aarr)
            continue;

        if(boost::iequals(arrName, std::string(aarr->GetName())))
        {
            arrName = aarr->GetName();
            break;
        }
        else
        {
            aarr = nullptr;
        }
    }
    vtkDataArray* darr = vtkDataArray::SafeDownCast(aarr);
    vtkStringArray* sarr = vtkStringArray::SafeDownCast(aarr);

    // now we need to specify if we are dealing with numbers or strings
    std::string nvstr = NewValue;
    bool newNum = is_number<double>(nvstr);

    // only possible incompatibility: cannot write strings to existing numeric attribute
    if((nullptr != darr) && !newNum)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgWriteValue: An existing numeric attribute cannot be overwritten by strings\n");
        return 1;
    }

    // if we have a number, we would generate a numeric array in the first place...
    bool generateNumArray = newNum;

    // ...but if we have an existing string array, we keep it strings
    if(nullptr != sarr)
        generateNumArray = false;

    // specify the size of the new array
    vtkIdType numBlocks = output->GetNumberOfCells(),
              numComps = (nullptr == aarr) ? 1 : aarr->GetNumberOfComponents();

    // if we have an array, and if it has components, try to find the required one
    // note: in the case of a one-component array we ignore any entered component
    //   name, while in the case of a multi-component array, we either overwrite an
    //   existing or add a new component
    vtkIdType comp = 0;
    bool newComp = false;
    if((nullptr != aarr) && (1 < numComps))
    {
        comp = -1;

        // look for an existing component
        for(vtkIdType c = 0; c < numComps; ++c)
        {
            if(boost::iequals(compName, std::string(aarr->GetComponentName(c))))
            {
                comp = c;
                compName = aarr->GetComponentName(c);
                break;
            }
        }

        // we need to add another component
        if(0 > comp)
        {
            comp = numComps++;
            newComp = true;
        }
    }

    // create a new attribute with the name - no matter if we already have one
    vtkSmartPointer<vtkAbstractArray> newArr = generateNumArray ?
        vtkSmartPointer<vtkAbstractArray>::Take(vtkDoubleArray::New()) :
        vtkSmartPointer<vtkAbstractArray>::Take(vtkStringArray::New());
    newArr->SetName(arrName.c_str());
    newArr->SetNumberOfComponents(numComps);
    newArr->SetNumberOfTuples(numBlocks);

    // name the components
    if(1 < numComps)
    {
        for(vtkIdType c = 0; c < numComps; ++c)
        {
            std::string cn = (c == comp) ? compName : std::string(aarr->GetComponentName(c));
            newArr->SetComponentName(c, cn.c_str());
        }
    }

    // if the filler is empty or wrong type (like string in numeric array)
    // we are going to adapt it somehow - basically replacing an invalid string with 0
    std::string fvstr = (nullptr != FillerValue) ? FillerValue : "";
    bool fillNum = is_number<double>(fvstr);
    if(generateNumArray && !fillNum)
        fvstr = "0.0";

    // write values to the new array
    for(vtkIdType c = 0; c < numComps; ++c)
    {
        for(vtkIdType b = 0; b < numBlocks; ++b)
        {
            if((c == comp) && (ApplyToAll || (Ids.end() != Ids.find(b))))
                // both component and b matching -> put in the new value
                newArr->SetVariantValue(b * numComps + c, nvstr.c_str());
            else if((nullptr == aarr) || ((c == comp) && newComp))
                // otherwise: either new array or new component -> filler value
                newArr->SetVariantValue(b * numComps + c, fvstr.c_str());
            else
                // otherwise: copy from old array
                newArr->SetVariantValue(b * numComps + c, aarr->GetVariantValue(b * numComps + c));
        }
    }

    // add the new array, either replacing the old one or generating a new,
    // and make it active
    vtkCellData* ocd = output->GetCellData();
    ocd->AddArray(newArr);
    ocd->SetActiveScalars(TargetArrayName);

    return 1;
}

void vtkAtgWriteValue::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
