set(plugin_name AtgRenameAttribute)

paraview_add_plugin(${plugin_name}
  VERSION "1.2.0"
  MODULES AthosGeoView::${plugin_name}Module
  MODULE_FILES ${plugin_name}Module/vtk.module
  MODULE_ARGS INSTALL_EXPORT AthosGeoView
  XML_DOCUMENTATION OFF)

# the only purpose of this is to bring the paraview.plugin file into QtCreator
add_custom_target(${plugin_name}_plugin_spec
  SOURCES
    "${CMAKE_CURRENT_SOURCE_DIR}/paraview.plugin"
    "${CMAKE_CURRENT_SOURCE_DIR}/${plugin_name}Module/plugin.xml")

