<!--
 Program: AthosGEO
 Copyright (c) 2018 cobo GmbH
 All rights reserved.
 AthosGEO View is a free software based on ParaView; you can redistribute
 and/or modify it under the same terms as ParaView
-->

<ServerManagerConfiguration>
  <ProxyGroup name="filters">
    <SourceProxy name="AtgRenameAttribute"
                 class="vtkAtgRenameAttribute"
                 label="Rename Attribute of table or data set">
      <Documentation
        long_help="Rename an attribute of a table or a data set"
        short_help="Rename an attribute">
          Rename an attribute of a table or data set, like a block model or
          a sampling data set
      </Documentation>

      <InputProperty name="Input"
                     command="SetInputConnection">
        <ProxyGroupDomain name="groups">
          <Group name="sources"/>
          <Group name="filters"/>
        </ProxyGroupDomain>
        <DataTypeDomain name="input_type">
          <DataType value="vtkDataObject"/>
        </DataTypeDomain>
        <Documentation>
          The input data object
        </Documentation>
      </InputProperty>

      <StringVectorProperty command="SetAttribute"
                            label="Select Attribute for Renaming"
                            name="Attribute"
                            number_of_elements="1"
                            panel_visibility="default">
        <RenameableArraysDomain name="array_list">
          <RequiredProperties>
            <Property function="Input"
                      name="Input" />
          </RequiredProperties>
        </RenameableArraysDomain>
        <Documentation>
          Attribute to rename. Be careful because AthosGEO recognizes attributes
          by their name, so functionality will change if you change or generate
          such a well known attribute name
        </Documentation>
      </StringVectorProperty>

      <StringVectorProperty command="SetNewName"
                            label="New Name"
                            name="NewName"
                            number_of_elements="1"
                            panel_visibility="default">
        <Documentation>
          New name for the attribute: see note above about the Attribute property
        </Documentation>
      </StringVectorProperty>

      <IntVectorProperty
          command="SetForceTypeChange"
          default_values="0"
          name="ForceTypeChange"
          number_of_elements="1"
          panel_visibility="default">
        <BooleanDomain name="bool" />
        <Documentation>
          Normally, renaming will not accept a new name that would change the
          data type of an attribute, because this will change the functionality.
          However, if you really know what you are doing, check this option and
          the renaming will do whatever it is told.
        </Documentation>
      </IntVectorProperty>

      <Hints>
        <ShowInMenu category="Attributes" />
        <View type="AtgRenderView" />
        <PipelineIcon name=":/AthosGeoView/icons/pqAtgRenderView.svg" />
        <RepresentationType
            view="AtgRenderView"
            type="Surface With Edges"/>
      </Hints>
    </SourceProxy>
  </ProxyGroup>
</ServerManagerConfiguration>
