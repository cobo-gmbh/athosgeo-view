/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgRenameAttribute.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <utilNormNames.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkTable.h>
#include <vtkDataSet.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgRenameAttribute.h>

vtkStandardNewMacro(vtkAtgRenameAttribute)

vtkAtgRenameAttribute::vtkAtgRenameAttribute()
:   Attribute(nullptr),
    NewName(nullptr),
    ForceTypeChange(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgRenameAttribute::~vtkAtgRenameAttribute()
{
    SetNewName(nullptr);
    SetAttribute(nullptr);
}

int vtkAtgRenameAttribute::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // we accept any data object
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataObject");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgRenameAttribute::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // will be same data type as input
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkDataObject");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgRenameAttribute::RequestDataObject(vtkInformation*,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkDataObject* input = vtkDataObject::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkDataObject* output = info->Get(vtkDataObject::DATA_OBJECT());

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkDataObject* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgRenameAttribute::RequestData(vtkInformation* request,
                                       vtkInformationVector** inputVector,
                                       vtkInformationVector* outputVector)
{
    // get the data object
    vtkDataObject* input = vtkDataObject::GetData(inputVector[0]);
    vtkDataObject* output = vtkDataObject::GetData(outputVector);

    // just to make sure - should not happen normally
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("AtgRenameAttribute: No valid input data object");
        return 1;
    }
    if(nullptr == output)
    {
        vtkOutputWindowDisplayWarningText("AtgRenameAttribute: No valid output data object");
        return 1;
    }
    if(std::string(input->GetClassName()) != output->GetClassName())
    {
        vtkOutputWindowDisplayWarningText("AtgRenameAttribute: Output must match the input data type");
        return 1;
    }

    // as a first step we make a shallow copy of the input - no matter what specific data type
    output->ShallowCopy(input);

    // this is a safety measure which can be overridden with an option
    if(!ForceTypeChange)
    {
        int inAttType = utilNormNames::getType(Attribute),
            outAttType = utilNormNames::getType(NewName);
        if(inAttType != outAttType)
        {
            std::string msg = std::string("AtgRenameAttribute: The attribute <") + Attribute +
                              "> is of type " + utilNormNames::getTypeDesc(Attribute) + ", renaming\n" +
                              "it to <" + NewName +  "> will convert it to type " +
                              utilNormNames::getTypeDesc(NewName) + "\n" +
                              "If you really want to do this, use the Force Type Change option\n";
            vtkOutputWindowDisplayWarningText(msg.c_str());
            return 1;
        }
    }

    // see whether we have a table or another data set: only these two can be accepted
    vtkTable* tabOut = vtkTable::SafeDownCast(output);
    vtkDataSet* dsOut = vtkDataSet::SafeDownCast(output);
    if((nullptr == tabOut) && (nullptr == dsOut))
    {
        vtkOutputWindowDisplayWarningText("AtgRenameAttribute: Datatype must be \'vtkTable\' or derived from \'vtkDataSet\'");
        return 1;
    }

    // common class for data inside tables and other data sets: we first try either table
    // or cell data - which would be the right thing for a block model
    vtkDataSetAttributes* dsaOut = nullptr;
    if(nullptr != tabOut)
        dsaOut = tabOut->GetRowData();
    else
        dsaOut = vtkDataSetAttributes::SafeDownCast(dsOut->GetCellData());

    // if we have no data now and the in/output data is vtkDataSet, we can still see if we have point data
    if((0 == dsaOut->GetNumberOfTuples()) && (nullptr != dsOut))
        dsaOut = vtkDataSetAttributes::SafeDownCast(dsOut->GetPointData());

    // if now we have no data, we really have nothing to process!
    if(0 == dsaOut->GetNumberOfTuples())
    {
        vtkOutputWindowDisplayWarningText("AtgRenameAttribute: No data found for processing");
        return 1;
    }

    // check if the new name already exists: this array will be deleted
    if(nullptr != dsaOut->GetAbstractArray(NewName))
    {
        vtkOutputWindowDisplayWarningText(
                (std::string("AtgRenameAttribute: The new name already exists: the old \'") +
                NewName + "\' attribute will be lost\n").c_str());

        dsaOut->RemoveArray(NewName);
    }

    // generate a clone of the old array
    vtkAbstractArray *oldArr = dsaOut->GetAbstractArray(Attribute),
                     *arr = oldArr->NewInstance();
    arr->DeepCopy(oldArr);

    // rename the clone, remove the old array and add the new one instead
    arr->SetName(NewName);
    dsaOut->RemoveArray(Attribute);
    dsaOut->AddArray(arr);

    // get rid of the excess reference count
    arr->Delete();

    // make the new array the active attribute
    // note: if the array is not a vtkDataArray (but e.g. vtkStringArray), this call will
    //   not work and only generate a warning, which we avoid
    if(nullptr != vtkDataArray::SafeDownCast(arr))
        dsaOut->SetActiveScalars(NewName);

    return 1;
}

int vtkAtgRenameAttribute::RequestInformation(vtkInformation* request,
                                              vtkInformationVector** inputVector,
                                              vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgRenameAttribute::PrintSelf(std::ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
}
