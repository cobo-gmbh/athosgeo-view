/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgWriteMultiValue.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgWriteMultiValue_h
#define vtkAtgWriteMultiValue_h

#include <set>
#include <vector>
#include <sstream>
#include <vtkPointSetAlgorithm.h>
#include <AtgWriteMultiValueModuleModule.h>

template<typename Numeric>
bool is_number(const std::string& s)
{
    Numeric n;
    return((std::istringstream(s) >> n >> std::ws).eof());
}

class ATGWRITEMULTIVALUEMODULE_EXPORT vtkAtgWriteMultiValue: public vtkPointSetAlgorithm
{
public:

    static vtkAtgWriteMultiValue* New();
    vtkTypeMacro(vtkAtgWriteMultiValue, vtkPointSetAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetStringMacro(TargetArrayName)
    vtkGetStringMacro(TargetArrayName)

    vtkSetStringMacro(FillerValue)
    vtkGetStringMacro(FillerValue)

    vtkSetMacro(NoErrors, bool)
    vtkGetMacro(NoErrors, bool)

    void AddNewValue(char const* value);
    void ClearNewValues();

    // an id set is in the property a string with comma separated id numbers
    void AddIdSet(char const* ids);
    void ClearIdSets();

    void AddLockItem(int item);
    void ClearLockItems();

    vtkSetMacro(CurrentItem, int)
    vtkGetMacro(CurrentItem, int)

protected:

    vtkAtgWriteMultiValue();
    ~vtkAtgWriteMultiValue();

    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);
    int RequestDataObject(vtkInformation*, vtkInformationVector** inputVector,
                              vtkInformationVector* outputVector);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:

    // copy constructor and assignment not implemented
    vtkAtgWriteMultiValue(vtkAtgWriteMultiValue const&);
    void operator=(vtkAtgWriteMultiValue const&);

    // name of the target array for assigning values
    char* TargetArrayName;

    // new value and filler value (for the case that the target array is new)
    char* FillerValue;

    // internal: for an initialization run before any selections were done
    bool NoErrors;

    // series of new values
    std::vector<std::string> NewValues;

    // series of id sets - one per new value
    typedef std::set<int> idSet;
    std::vector<idSet> IdSets;

    // items that are currently locked (id set not changeable), and the
    // currently active item
    // note: these are kept also here as properties even if they are only of
    //   interest for the property widget - because here they are not volatile,
    //   while the property widgets are destroyed if they are not any more
    //   currently active, so with these properties the constructor can bring
    //   the user back to where he left off
    std::vector<bool> LockItems;
    int CurrentItem;

};

#endif
