/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCalculatorForSelection.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <set>
#include <string>
#include <boost/algorithm/string/predicate.hpp>
#include <vtkSmartPointer.h>
#include <vtkPointSet.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkAtgCalculatorForSelection.h>

vtkStandardNewMacro(vtkAtgCalculatorForSelection)

vtkAtgCalculatorForSelection::vtkAtgCalculatorForSelection()
:	ApplyToAll(false),
    NoErrors(true)
{
    SetNumberOfInputPorts(1);
}

vtkAtgCalculatorForSelection::~vtkAtgCalculatorForSelection()
{
}

void vtkAtgCalculatorForSelection::AddId(int id)
{
    Ids.insert(id);
    Modified();
}

void vtkAtgCalculatorForSelection::ClearIds()
{
    Ids.clear();
    Modified();
}

int vtkAtgCalculatorForSelection::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPointSet");
            break;
    }

    return 1;
}

int vtkAtgCalculatorForSelection::FillOutputPortInformation(int port, vtkInformation* info)
{
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
    return 1;
}

int vtkAtgCalculatorForSelection::RequestData(vtkInformation* request,
                                              vtkInformationVector** inputVector,
                                              vtkInformationVector* outputVector)
{
    // get the input data object
    vtkPointSet* input = vtkPointSet::GetData(inputVector[0], 0);
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText("No input data set\n");
        return 1;
    }

    // output grid - shallow copy of input data object
    vtkPointSet* output = vtkPointSet::GetData(outputVector, 0);
    output->ShallowCopy(input);

    // if this is the initialization call, we are done already here
    if(NoErrors)
        return 1;

    // make sure we have a result array
    if(nullptr == ResultArrayName)
    {
        vtkOutputWindowDisplayWarningText("No name given for the result array\n");
        return 1;
    }
    std::string resArrName(ResultArrayName);

    // here we will copy the initial result array if it exists and
    // the "selected only" option was chosen
    // note: we need this in order to restore the original data for the
    //   non-selected data rows, and we use the replacement value for this if the
    //   array is a new one
    vtkSmartPointer<vtkDoubleArray> initialArr = vtkSmartPointer<vtkDoubleArray>::New();
    initialArr->SetNumberOfComponents(input->GetCellData()->GetNumberOfComponents());
    initialArr->SetNumberOfTuples(input->GetNumberOfCells());
    initialArr->Fill(ReplacementValue);

    // all this we need only if the user wants to work with selected cells only
    if(!ApplyToAll)
    {
        // in this case we need selected ids
        if(Ids.empty())
        {
            vtkOutputWindowDisplayWarningText("Neither \'apply to all\' nor any selected items found\n");
            return 1;
        }

        // see if we already have the result array (data array - not something else like strings...)
        vtkDataArray* arr = nullptr;
        for(vtkIdType a = 0; a < input->GetCellData()->GetNumberOfArrays(); ++a)
        {
            arr = vtkDataArray::SafeDownCast(input->GetCellData()->GetArray(a));
            if(nullptr == arr)
                continue;

            if(boost::iequals(resArrName, arr->GetName()))
            {
                resArrName = arr->GetName();
                break;
            }
            else
            {
                arr = nullptr;
            }
        }

        if(nullptr != arr)
        {
            for(vtkIdType v = 0; v < arr->GetNumberOfValues(); ++v)
                initialArr->SetValue(v, arr->GetVariantValue(v).ToDouble());
        }
    }

    // pass the result array name
    Superclass::SetResultArrayName(resArrName.c_str());

    // run the vtkPVArrayCalculator filter function that does
    // - parse the function row
    // - execute the function
    // - add a new data array for the result, or replace the existing one
    int res = Superclass::RequestData(request, inputVector, outputVector);
    if(0 == res)
        return res;

    // with the "apply to all" option we are finished now...
    if(ApplyToAll)
    {
        return 1;
    }

    // ...otherwise we still have to rework the output grid
    // note: normally the below "emergency brake" should not be required!
    vtkDataArray* resArr =
            vtkDataArray::SafeDownCast(output->GetCellData()->GetAbstractArray(resArrName.c_str()));
    if(nullptr == resArr)
        return 1;

    // for all non-selected blocks, copy back the initial values
    vtkIdType numComps = resArr->GetNumberOfComponents();
    for(vtkIdType b = 0; b < resArr->GetNumberOfTuples(); ++b)
    {
        if(Ids.end() == Ids.find(b))
        {
            for(vtkIdType c = 0; c < numComps; ++c)
            {
                resArr->SetVariantValue(b * numComps + c, initialArr->GetComponent(c, b));
            }
        }
    }

    return 1;
}

void vtkAtgCalculatorForSelection::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
