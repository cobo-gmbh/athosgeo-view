/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgFixedColorsAttribute.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <boost/algorithm/string/predicate.hpp>
#include <utilNormNames.h>
#include <vtkSmartPointer.h>
#include <vtkPointSet.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkTable.h>
#include <vtkAtgCsvReader.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgFixedColorsAttribute.h>

vtkStandardNewMacro(vtkAtgFixedColorsAttribute)

struct ColorRGBA
{
    ColorRGBA()
    :   red(.5),
        green(.5),
        blue(.5),
        alpha(1.)
    {}

    ColorRGBA(double r, double g, double b, double a = 1.)
    :   red(r),
        green(g),
        blue(b),
        alpha(a)
    {}

    double red,
           green,
           blue,
           alpha;
};

vtkAtgFixedColorsAttribute::vtkAtgFixedColorsAttribute()
:   CategoriesColorsFile(nullptr),
    FieldDelimiterCharacters(nullptr),
    Category(nullptr),
    AddTabFieldDelimiter(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgFixedColorsAttribute::~vtkAtgFixedColorsAttribute()
{
    SetCategoriesColorsFile(nullptr);
    SetFieldDelimiterCharacters(nullptr);
    SetCategory(nullptr);
}

int vtkAtgFixedColorsAttribute::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // any data object
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgFixedColorsAttribute::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgFixedColorsAttribute::RequestDataObject(vtkInformation*,
                                                  vtkInformationVector** inputVector,
                                                  vtkInformationVector* outputVector)
{
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkDataObject* output = info->Get(vtkDataObject::DATA_OBJECT());

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkDataObject* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgFixedColorsAttribute::RequestData(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    // get the data object
    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    vtkPointSet* output = vtkPointSet::GetData(outputVector);

    // just to make sure - should not happen normally
    if(nullptr == input)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No valid input data object");
        return 1;
    }
    if(nullptr == output)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No valid output data object");
        return 1;
    }
    if(std::string(input->GetClassName()) != output->GetClassName())
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: Output must match the input data type");
        return 1;
    }

    // as a first step we make a shallow copy of the input - no matter what specific data type
    output->ShallowCopy(input);

    // make sure we have a valid Category attribute in the input
    if(nullptr == Category)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No \'Category\' property found");
        return 1;
    }
    vtkAbstractArray* inCatArr = input->GetCellData()->GetAbstractArray(Category);
    if(nullptr == inCatArr)
    {
        std::string err = std::string("AtgFixedColorsAttribute: \'Category\' attribute \'") +
                          Category + "\' not found in the input";
        vtkOutputWindowDisplayWarningText(err.c_str());
        return 1;
    }

    // ensure a valid CSV categories/colors file is available
    // note: the Value and Colors columns are mandatory, while the Category is optional
    if(nullptr == CategoriesColorsFile)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No \'CategoriesColorsFile\' property found");
        return 1;
    }
    auto csvReader = vtkSmartPointer<vtkAtgCsvReader>::New();
    csvReader->SetFileName(CategoriesColorsFile);
    csvReader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
    csvReader->SetAddTabFieldDelimiter(AddTabFieldDelimiter);
    csvReader->Update();
    vtkTable* colorsTable = csvReader->GetOutput();

    // reading errors are fatal
    if(nullptr != csvReader->GetErrorMessage())
    {
        std::string msg = std::string("Error reading file:\n") + csvReader->GetErrorMessage();
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // possible data type related error messages
    // note: these errors are not treated as fatal
    if(nullptr != csvReader->GetWarningMessages())
    {
        std::string err = std::string("AtgFixedColorsAttribute:\n")
                        + csvReader->GetWarningMessages();
        vtkOutputWindowDisplayWarningText(err.c_str());
    }

    // join multi-component data columns
    // note: they will be type vtkDoubleArray at the end, no matter what they were before
    std::map<std::string, std::set<std::string>> multiCols;
    for(vtkIdType col = 0; col < colorsTable->GetNumberOfColumns(); ++col)
    {
        vtkDataArray* arr = vtkDataArray::SafeDownCast(colorsTable->GetColumn(col));
        if(nullptr == arr)
            continue;

        std::string colName = arr->GetName();
        int colon = colName.find(':');
        if(0 < colon)
        {
            std::string cname = colName.substr(0, colon),
                        comp = colName.substr(colon + 1, colName.length());
            if(boost::iequals(comp, "red"))
            {
                comp = "0";
                colorsTable->GetColumnByName(colName.c_str())->SetName((cname + ":0").c_str());
            }
            else if(boost::iequals(comp, "green"))
            {
                comp = "1";
                colorsTable->GetColumnByName(colName.c_str())->SetName((cname + ":1").c_str());
            }
            else if(boost::iequals(comp, "blue"))
            {
                comp = "2";
                colorsTable->GetColumnByName(colName.c_str())->SetName((cname + ":2").c_str());
            }
            else if(boost::iequals(comp, "alpha"))
            {
                comp = "3";
                colorsTable->GetColumnByName(colName.c_str())->SetName((cname + ":3").c_str());
            }
            std::set<std::string> comps;
            if(multiCols.end() != multiCols.find(cname))
                comps = multiCols[cname];
            comps.insert(comp);
            multiCols[cname] = comps;
        }
    }

    for(auto mcit = multiCols.begin(); mcit != multiCols.end(); ++mcit)
    {
        std::vector<vtkDataArray*> compArrs;
        std::string cname = mcit->first;
        std::set<std::string>& comps = mcit->second;
        for(auto it = comps.begin(); it != comps.end(); ++it)
        {
            std::string fullName = cname + ":" + *it;
            vtkDataArray* arr = vtkDataArray::SafeDownCast(colorsTable->GetColumnByName(fullName.c_str()));
            compArrs.push_back(arr);
        }

        auto newCol = vtkSmartPointer<vtkDoubleArray>::New();
        newCol->SetName(cname.c_str());
        newCol->SetNumberOfComponents(compArrs.size());
        newCol->SetNumberOfTuples(colorsTable->GetNumberOfRows());
        colorsTable->AddColumn(newCol);

        for(vtkIdType row = 0; row < colorsTable->GetNumberOfRows(); ++row)
        {
            for(std::vector<vtkDataArray*>::size_type c = 0; c < compArrs.size(); ++c)
                newCol->SetComponent(row, c, compArrs[c]->GetVariantValue(row).ToDouble());
        }

        for(auto it = comps.begin(); it != comps.end(); ++it)
        {
            std::string fullName = cname + ":" + *it;
            colorsTable->RemoveColumnByName(fullName.c_str());
        }
    }

    // check for all required columns
    vtkAbstractArray *tabCatArr = colorsTable->GetColumnByName("Category"),
                     *tabValArr = colorsTable->GetColumnByName("Value");
    std::string colorsName = utilNormNames::getName(utilNormNames::CELLCOLOR),
                alphaName = utilNormNames::getName(utilNormNames::CELLALPHA);
    vtkDataArray *colArr = vtkDataArray::SafeDownCast(colorsTable->GetColumnByName(colorsName.c_str())),
                 *alphaArr = vtkDataArray::SafeDownCast(colorsTable->GetColumnByName(alphaName.c_str()));
    bool alphaArrNeedsDelete = false;
    if(nullptr == tabValArr)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No \'Value\' column "
                    "in the categories/colors table found");
        return 1;
    }
    if(nullptr == colArr)
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: No \'Colors\' column "
                    "in the categories/colors table found");
        return 1;
    }
    if(3 > colArr->GetNumberOfComponents())
    {
        vtkOutputWindowDisplayWarningText(
                    "AtgFixedColorsAttribute: \'Colors\' column in the categories/colors "
                    "table must have 3 components: RGB");
        return 1;
    }

    // if we have no alpha array, but a 4th component in the colors array instead,
    // we generate an alpha array from that component
    if((nullptr == alphaArr) && (4 <= colArr->GetNumberOfComponents()))
    {
        alphaArr = vtkDoubleArray::New();
        alphaArr->SetName(alphaName.c_str());
        alphaArr->SetNumberOfTuples(colArr->GetNumberOfTuples());
        for(vtkIdType row = 0; row < colArr->GetNumberOfTuples(); ++row)
            alphaArr->SetComponent(row, 0, colArr->GetComponent(row, 3));
        alphaArrNeedsDelete = true;
    }

    // after all these checks we can proceed to add a Colors attribute to the output
    auto outColArr = vtkSmartPointer<vtkDoubleArray>::New();
    outColArr->SetName(colorsName.c_str());
    outColArr->SetNumberOfComponents(3);
    outColArr->SetNumberOfTuples(output->GetCellData()->GetNumberOfTuples());
    outColArr->Fill(0.);
    output->GetCellData()->AddArray(outColArr);

    // if we have a ColorAlpha array, we also add a ColorAlpha attribute
    vtkSmartPointer<vtkDoubleArray> outAlphaArr;
    if(nullptr != alphaArr)
    {
        outAlphaArr = vtkSmartPointer<vtkDoubleArray>::New();
        outAlphaArr->SetName(alphaName.c_str());
        outAlphaArr->SetNumberOfTuples(output->GetCellData()->GetNumberOfTuples());
        outAlphaArr->Fill(1.);
        output->GetCellData()->AddArray(outAlphaArr);
    }

    // generate a map for fast access to the colors
    std::map<std::string, ColorRGBA> colMap;
    for(vtkIdType row = 0; row < tabValArr->GetNumberOfTuples(); ++row)
    {
        // category name is optional: if not available, all values are used
        std::string catName;
        if(nullptr != tabCatArr)
            catName = tabCatArr->GetVariantValue(row).ToString();
        if(!catName.empty() && (catName != Category))
            continue;

        // add data from row to the map
        // note that the alpha value is optional and will be set to a default if not present
        std::string value = tabValArr->GetVariantValue(row).ToString();
        double r = colArr->GetComponent(row, 0),
               g = colArr->GetComponent(row, 1),
               b = colArr->GetComponent(row, 2),
               a = 1.;
        if(nullptr != alphaArr)
            a = alphaArr->GetComponent(row, 0);

        // add data to map
        colMap[value] = ColorRGBA(r, g, b, a);
    }

    // now we fill in the colors
    for(vtkIdType row = 0; row < outColArr->GetNumberOfTuples(); ++row)
    {
        std::string catVal = inCatArr->GetVariantValue(row).ToString();
        ColorRGBA col;
        if(colMap.end() != colMap.find(catVal))
            col = colMap[catVal];

        outColArr->SetTuple3(row, col.red, col.green, col.blue);

        if(outAlphaArr)
            outAlphaArr->SetComponent(row, 0, col.alpha);
    }

    // not the most elegant solution, but should work of course...
    if((nullptr != alphaArr) && alphaArrNeedsDelete)
        alphaArr->Delete();

    // Make Colors the active array
    output->GetCellData()->SetActiveScalars(colorsName.c_str());

    return 1;
}

void vtkAtgFixedColorsAttribute::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
