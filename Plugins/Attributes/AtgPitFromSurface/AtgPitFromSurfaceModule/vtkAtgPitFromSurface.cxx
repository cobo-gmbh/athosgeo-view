/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgPitFromSurface.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <string>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellLocator.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgPitFromSurface.h>

vtkStandardNewMacro(vtkAtgPitFromSurface)

vtkAtgPitFromSurface::vtkAtgPitFromSurface()
:   PitExtension(nullptr),
    RespectVolFactor(true),
    Invert(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgPitFromSurface::~vtkAtgPitFromSurface()
{
    SetPitExtension(nullptr);
}

int vtkAtgPitFromSurface::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // first object: grid (block model)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        case 1:
            // second object: topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 0);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgPitFromSurface::RequestData(vtkInformation* request,
                                      vtkInformationVector** inputVector,
                                      vtkInformationVector* outputVector)
{
    (void)request;

    // we will need some norm names throughout the function, and some temporary names
    static const std::string namePitNorm(utilNormNames::getName(utilNormNames::PIT));
    std::string namePit = namePitNorm;
    if(nullptr != PitExtension)
        namePit = namePit + "_" + PitExtension;

    // input port 0: the input unstructured grid
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputVector[0]);
    if(nullptr == inputGrid)
    {
        vtkOutputWindowDisplayWarningText("AtgPitFromSurface: No valid input block model found");
        return 1;
    }

    // input port 1: the input poly data object
    vtkPolyData* inputPoly = vtkPolyData::GetData(inputVector[1]);
    if(nullptr == inputPoly)
    {
        vtkOutputWindowDisplayWarningText("AtgPitFromSurface: No valid input geometry found");
        return 1;
    }

    // get volume factor attribute if asked for and existing
    std::string volFactorName = utilNormNames::getName(utilNormNames::VOLFACTOR);
    vtkDataArray* volFactorArr = nullptr;
    if(RespectVolFactor)
        volFactorArr = inputGrid->GetCellData()->GetArray(volFactorName.c_str());

    // get the output grid and shallow copy the input block model
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::GetData(outputVector);
    outGrid->ShallowCopy(inputGrid);

    // clean and normalize the "raw topo" poly data
    // - output port 0: cleaned and normalized topo surface:
    //   made up of cells of type triangle
    // - output port 1: closed topo boundary polygon
    //   exactly one cell of type polyline
    // note: the latter is currently not any more required for this filter, but the
    //   first helps to avoid tons of "useless" error messages and geometric problems
    // ---------------------------------------------
    // NOTE (2024-01-31)
    //   It turns out that within this "cleaning class", a call to the "feature edges"
    //   filter has the potential to demand so much memory that the entire function crashes.
    //   On top of that it turns out that even with some "unclean" surfaces, this filter
    //   will work just well because the method is currently much more robust than it
    //   was initially (just "shooting vertical lines" at certain points - no complex
    //   triangle geometry stuff...): The worst that can happen if the surface is really
    //   completely odd that results will be equally odd.
    //   For this reason, the vtkAtgCleanNormTopo class was removed from the active code
    //vtkSmartPointer<vtkAtgCleanNormTopo> topo = vtkSmartPointer<vtkAtgCleanNormTopo>::New();
    //topo->SetInputData(inputPoly);
    //topo->Update();
    //if(0 != topo->GetErrorMsg())
    //{
    //    std::string msg(topo->GetErrorMsg());
    //    vtkOutputWindowDisplayWarningText((msg + "\n").c_str());
    //    return 1;
    //}
    // ---------------------------------------------

    // build the cell locator for further processing
    // note: this filter is mainly an octree based, very fast item finder in space.
    //   It has a function that allows to "shoot" a line segment through the
    //   input polydata structure - the topo in this case. It will be used with
    //   a vertical line through some x/y points, for quick calculation of the
    //   topo z coordinate (elevation) at that point. This functionality is the
    //   "workhorse function" for the current clipping filter.
    vtkSmartPointer<vtkCellLocator> cloc = vtkSmartPointer<vtkCellLocator>::New();
    cloc->SetDataSet(inputPoly);
    cloc->BuildLocator();

    // some variables required for the intersection calculations
    double tt,
           intersect[3],
           pcoord[3],
           elevation[4];
    int subId;

    // add the new pit array that will be filled with values between 0. and 100.
    vtkSmartPointer<vtkDoubleArray> newPitArray = vtkSmartPointer<vtkDoubleArray>::New();
    newPitArray->SetName(namePit.c_str());
    newPitArray->SetNumberOfValues(outGrid->GetNumberOfCells());
    newPitArray->Fill(-100.); // means: not handled yet
    outGrid->GetCellData()->AddArray(newPitArray);

    // in this loop, the blocks are checked in relation with the topo surface:
    // - fully inside the area of the topo?
    // - fully above or below the topo?
    // - if cut by the topo, calculate the fraction below (approximately)
    for(vtkIdType c = 0; c < outGrid->GetNumberOfCells(); ++c)
    {
        // progress every 1000 blocks
        if(0 == (c % 1000))
            ShowProgress(c, inputGrid->GetNumberOfCells());

        // get the corner points of the block
        vtkPoints* pts = outGrid->GetCell(c)->GetPoints();

        // BCO 2023-01-18 changed strategy: until now, blocks only partially in the surface
        //   range were treated like being completely outside, but this turned out to be
        //   not desirable. With the new strategy, only blocks completely outside the
        //   range are ignored (all 4 corners have no related elevation: pit value is set
        //   to 0.), but for partially covered blocks the elevation is set to 10000 -
        //   with the idea to make it "infinite" - for points where an elevation is not
        //   found by intersection. This again has the result as if these points are
        //   outside the pit, but it's then only these points, not the entire block.

        // default elevation is "infinite"
        elevation[0] = elevation[1] = elevation[2] = elevation[3] = 10000.;

        // go through the four bottom points of the block and get the topo elevation there
        long long outside = 0;
        vtkIdType maxCorners = std::min<vtkIdType>(4, pts->GetNumberOfPoints() / 2);
        for(vtkIdType p = 0; p < maxCorners; ++p)
        {
            double pt[3];
            pts->GetPoint(p, pt);
            double btmPt[3] = {pt[0], pt[1], -500.},
                   topPt[3] = {pt[0], pt[1], 10000.};

            // this function call will return true if an intersection with the topo was found,
            // or otherwise false. In the intersect array the coordinates of the intersection
            // point will be returned.
            // note: the function always returns either 0 or 1 intersection points, even if
            //   there would be more. In some tests, this 1 intersection point was always the
            //   first (i.e. lowest) along the line from btmPt to topPt, but not sure if this
            //   is always the case. However, we are assuming here that _normally_ a topo should
            //   only have one intersection point, and only if it is not really "clean" there
            //   would be more - in which case it might be acceptable to just take one of them
            //   and continue. This looks like a good strategy also in terms of rubustness of
            //   the filter regarding arbitrary "unclean" topo input.
            if(!cloc->IntersectWithLine(btmPt, topPt, 0.0001, tt, intersect, pcoord, subId))
            {
                // no intersection means: we are here not in the range of the surface, so we
                // do not further touch the block (not in the new pit)
                newPitArray->SetValue(c, Invert ? 100. : 0.);
                ++outside;
                continue;
            }

            elevation[p] = intersect[2];
        }

        // if all corner points are outside the topo range, continue
        if(outside >= maxCorners)
            continue;

        // get the bottom and top elevations of the block
        double btmElev = pts->GetPoint(0)[2],
               topElev = pts->GetPoint(4)[2];

        // if we respect the volume factor, we assume that the block is "flattened", i.e.
        // the missing volume is at the top. Why? The assumption is that other cases
        // would mostly happen at the boundary or bottom of a model, and defining a pit
        // that is not fully inside a model is maybe a less likely scenario.
        if(nullptr != volFactorArr)
        {
            double volf = volFactorArr->GetVariantValue(c).ToDouble();
            topElev = btmElev + volf * (topElev - btmElev) / 100.;
        }

        // count the corners where the topo is fully above or below the topo
        int numAbove = 0, // counts where the block is fully above the topo
            numBelow = 0; // counts the inverse
        for(int c = 0; c < 4; ++c)
        {
            if(elevation[c] >= topElev)
                ++numBelow;
            if(elevation[c] <= btmElev)
                ++numAbove;
        }

        // handle the cases where the block is either fully above or below
        if(4 == numAbove)
        {
            // block is fully above - goes into the new pit
            newPitArray->SetValue(c, Invert ? 0. : 100.);
            continue;
        }
        else if(4 == numBelow)
        {
            // block is fully below - not in the pit
            newPitArray->SetValue(c, Invert ? 100. : 0.);
            continue;
        }

        // now we need to calculate the fraction of the block above or below
        // note: we simply approximate the calculation by rastering the block
        //   surface into 4x4 grid and take the mean value of the center elevations

        // define a start point and two vectors for the raster calculation
        double pt0[3],  // "lower left corner"
               vecu[3], // block edge from origin corner
               vecv[3]; // orthogonal edge
        pts->GetPoint(0, pt0);
        pts->GetPoint(1, vecu);
        pts->GetPoint(3, vecv);
        for(int i = 0; i < 3; ++i)
        {
            vecu[i] -= pt0[i];
            vecv[i] -= pt0[i];
        }

        // double loop for the 4x4 grid
        // note: we carry a counter for the number of grid points as a safety measure:
        //   in a case where the elevation function would not return a value, e.g. because
        //   of a little hole in the topo, this point would simply be ignored
        // note: the loop run variables are 0.5, 1.5 etc. in order to take the center
        //   points of the 4x4 block face segments
        // BCO 2023-01-18 changed strategy (see remark above): with this, we cannot
        //   any more simply ignore blocks with no elevation from intersection because
        //   we allowed blocks to be partially outside of the range of the surface.
        //   Instead we assume that at such a point elevation is "infinite",
        //   and this will now also happen for points that would irregularly deliver no
        //   real elevation result (like a hole in the topo)
        static const double numSegs = 4.;
        double partBelow = 0.;
        for(double u = .5; u < numSegs; u += 1.)
        {
            for(double v = .5; v < numSegs; v += 1.)
            {
                double btmPt[3] = {pt0[0] + u * vecu[0] / numSegs + v * vecv[0] / numSegs,
                                   pt0[1] + u * vecu[1] / numSegs + v * vecv[1] / numSegs,
                                   -500.},
                       topPt[3] = {btmPt[0], btmPt[1], 10000.};

                double el = 10000.;
                if(cloc->IntersectWithLine(btmPt, topPt, 0.0001, tt, intersect, pcoord, subId))
                    el = intersect[2];
                if(el >= topElev)
                    partBelow += 100.;
                else if(el <= btmElev)
                    partBelow += 0.;
                else
                    partBelow += 100. * (el - btmElev) / (topElev - btmElev);
            }
        }
        partBelow /= (numSegs * numSegs);

        // assign part to temp array - taking the "keep blocks below" option into account
        newPitArray->SetValue(c, Invert ? partBelow : 100. - partBelow);
    }

    // progress: "fully done"
    ShowProgress(100, 100);

    // make the new zone the active attribute
    outGrid->GetCellData()->SetActiveScalars(namePit.c_str());

    return 1;
}

int vtkAtgPitFromSurface::RequestInformation(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
    (void)request;
    (void)inputVector;
    (void)outputVector;

    return 1;
}

void vtkAtgPitFromSurface::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}

void vtkAtgPitFromSurface::ShowProgress(int step, int totalSteps)
{
    if(AbortExecute)
        return;

    float progress = static_cast<float>(step) / totalSteps,
          rounded = static_cast<float>(static_cast<int>((progress * 100) + 0.5f)) / 100.f;
    if(GetProgress() != rounded)
        UpdateProgress(rounded);
}
