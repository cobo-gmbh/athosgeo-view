/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTopoCutter.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/algorithms/validity_failure_type.hpp>
#include <vtkAtgContourLoopExtraction.h>
#include <vtkDataObject.h>
#include <vtkPolyData.h>
#include <vtkIdList.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>
#include <vtkCellLocator.h>
#include <vtkTriangleFilter.h>
#include <vtkDelaunay2D.h>
#include <vtkCleanPolyData.h>
#include <vtkTriangle.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTopoCutter.h>

static const double distEps = .01;

vtkStandardNewMacro(vtkAtgTopoCutter)

namespace bg = boost::geometry;
typedef bg::model::point<double, 2, bg::cs::cartesian> zfbPoint;
typedef bg::model::polygon<zfbPoint, true> zfbPolygon;
typedef bg::strategy::within::franklin<zfbPoint> zfbStrategyFranklin;

struct TriangleInfo
{
    TriangleInfo();
    TriangleInfo(TriangleInfo const& info);
    TriangleInfo(vtkCell* triangle, vtkPoints* polyDataPoints);

    double pts[3][3];
    zfbPolygon triangle;
    vtkBoundingBox bound;
};

TriangleInfo::TriangleInfo()
{
    ::memset(pts, 0, 9 * sizeof(double));
    triangle = zfbPolygon();
    bound = vtkBoundingBox();
}

TriangleInfo::TriangleInfo(TriangleInfo const& info)
{
    ::memcpy(pts, info.pts, 9 * sizeof(double));
    triangle = zfbPolygon(info.triangle);
    bound = vtkBoundingBox(info.bound);
}

TriangleInfo::TriangleInfo(vtkCell* triCell, vtkPoints* polyDataPoints)
{
    for(vtkIdType p = 0; p < 3; ++p)
    {
        polyDataPoints->GetPoint(triCell->GetPointId(p), pts[p]);
        bg::append(triangle, zfbPoint(pts[p][0], pts[p][1]));
    }
    double bnd[6];
    triCell->GetBounds(bnd);
    bnd[4] = -1.;
    bnd[5] = 1.;
    bound = vtkBoundingBox(bnd);

    // make sure that also the sense of rotation is adjusted
    bg::correct(triangle);
    if((triangle.outer().at(1).get<0>() != triangle.outer().at(2).get<0>()) ||
       (triangle.outer().at(1).get<1>() != triangle.outer().at(2).get<1>()))
    {
        for(int i = 0; i < 3; ++i)
        {
            double tmp = pts[1][i];
            pts[1][i] = pts[2][i];
            pts[2][i] = tmp;
        }
    }
}

struct PointInfo2
{
    PointInfo2();
    PointInfo2(double p[2]);
    PointInfo2(PointInfo2 const& point);

    void setPoint(double p[2]);
    bool operator<(PointInfo2 const& point2) const;

    double pt[2];
    bool valid;
};

PointInfo2::PointInfo2()
:   valid(false)
{
}

PointInfo2::PointInfo2(double p[2])
:   valid(true)
{
    ::memcpy(pt, p, 2 * sizeof(double));
}

PointInfo2::PointInfo2(PointInfo2 const& point)
:   valid(point.valid)
{
    ::memcpy(pt, point.pt, 2 * sizeof(double));
}

void PointInfo2::setPoint(double p[2])
{
    valid = true;
    ::memcpy(pt, p, 2 * sizeof(double));
}

bool PointInfo2::operator<(PointInfo2 const& point2) const
{
    if(!valid && !point2.valid)
        return false;
    if(!valid && point2.valid)
        return true;
    if(valid && !point2.valid)
        return false;

    if(pt[0] < (point2.pt[0] - distEps))
        return true;
    else if(pt[0] > (point2.pt[0] + distEps))
        return false;
    else
        return pt[1] < point2.pt[1];
}

struct EdgeInfo2
{
    EdgeInfo2(double p1[2], double p2[2]);
    EdgeInfo2(EdgeInfo2 const& edge);

    bool operator<(EdgeInfo2 const& edge2) const;

    PointInfo2 pt1,
               pt2;
};

EdgeInfo2::EdgeInfo2(double p1[2], double p2[2])
:   pt1(p1),
    pt2(p2)
{
    // make sure we have a guaranteed order of the two points, so we can
    // compare without bothering about the direction
    if(pt2 < pt1)
    {
        PointInfo2 temp = pt1;
        pt1 = pt2;
        pt2 = temp;
    }
}

EdgeInfo2::EdgeInfo2(EdgeInfo2 const& edge)
:   pt1(edge.pt1),
    pt2(edge.pt2)
{
}

bool EdgeInfo2::operator<(EdgeInfo2 const& edge2) const
{
    if(pt1 < edge2.pt1)
        return true;
    else if(edge2.pt1 < pt1)
        return false;
    else
        return pt2 < edge2.pt2;
}

struct PolygonInfo
{
    PolygonInfo(long newId, vtkPoints* pts, vtkCell* pc);
    PolygonInfo(zfbPolygon const& poly, std::vector<double> const& elev);
    PolygonInfo(PolygonInfo const& pi);

    bool isInside(PolygonInfo const& pi);
    static void generatePolygonHierarchy(vtkPolyData* basePolyData,
                                         std::vector<std::unique_ptr<PolygonInfo>>& hierarchy);

    long id;
    std::vector<double> elevations;
    std::vector<std::vector<double>> holeElevations;
    zfbPolygon polygon;
    std::vector<PolygonInfo*> surroundingParents;
    unsigned long hierarchyLevel;
};

PolygonInfo::PolygonInfo(long newId, vtkPoints* pts, vtkCell* pc)
:   id(newId),
    hierarchyLevel(0)
{
    vtkIdList* ids = pc->GetPointIds();
    for(vtkIdType p = 0; p < ids->GetNumberOfIds(); ++p)
    {
        double pt[3];
        pts->GetPoint(ids->GetId(p), pt);
        bg::append(polygon, zfbPoint(pt[0], pt[1]));
        elevations.push_back(pt[2]);
    }

    if((polygon.outer().front().get<0>() != polygon.outer().back().get<0>()) ||
       (polygon.outer().front().get<1>() != polygon.outer().back().get<1>()))
    {
        bg::append(polygon, polygon.outer().front());
        elevations.push_back(elevations.front());
    }

    // make sure that the polygon is closed and has the correct sense of rotation
    double abef = bg::area(polygon);
    bg::correct(polygon);
    if(abef != bg::area(polygon))
        std::reverse(elevations.begin(), elevations.end());
}

PolygonInfo::PolygonInfo(zfbPolygon const& poly, std::vector<double> const& elev)
:   id(-1),
    hierarchyLevel(0)
{
    polygon = poly;
    elevations = elev;
}

PolygonInfo::PolygonInfo(PolygonInfo const& pi)
:   id(pi.id),
    elevations(pi.elevations),
    holeElevations(pi.holeElevations),
    polygon(pi.polygon),
    surroundingParents(pi.surroundingParents),
    hierarchyLevel(pi.hierarchyLevel)
{
}

bool PolygonInfo::isInside(PolygonInfo const& pi)
{
    return bg::covered_by(pi.polygon, polygon);
}

void PolygonInfo::generatePolygonHierarchy(vtkPolyData* basePolyData,
                                           std::vector<std::unique_ptr<PolygonInfo>>& polygonVec)
{
    // we first build a linear data structure (vector) with the actual PolygonInfo objects
    // note: this vector will remain the actual data storage, even for included cells
    for(vtkIdType c = 0; c < basePolyData->GetNumberOfCells(); ++c)
        polygonVec.push_back(std::unique_ptr<PolygonInfo>(new PolygonInfo(c, basePolyData->GetPoints(), basePolyData->GetCell(c))));

    // check all polygons against all others and see whether one is included inside
    // the other
    // note: this strategy has O2 (quadratic) complexity which could theoretically be
    //   reduced by generating some quad-tree structure using bounding boxes, in order to
    //   restrict exact comparisons to a minimum. However, in this context we never expect
    //   a really large number of polygons to be handled, so we do it the "quick and dirty"
    //   way from the beginning
    for(int i = 0; i < (polygonVec.size() - 1); ++i)
    {
        for(auto j = i + 1; j < polygonVec.size(); ++j)
        {
            PolygonInfo &pi1 = (*polygonVec.at(i)),
                        &pi2 = (*polygonVec.at(j));

            if(pi1.isInside(pi2))
                pi2.surroundingParents.push_back(&pi1);
            else if(pi2.isInside(pi1))
                pi1.surroundingParents.push_back(&pi2);
        }
    }

    // the hierarchical level actually corresponds to the surrounding polygons
    for(auto it = polygonVec.begin(); it != polygonVec.end(); ++it)
        (*it)->hierarchyLevel = (*it)->surroundingParents.size();

    // remove all surrounding polygons that are not at the next higher level: with
    // this, always only one should remain
    for(auto it = polygonVec.begin(); it != polygonVec.end(); ++it)
    {
        for(auto spit = (*it)->surroundingParents.begin(); spit != (*it)->surroundingParents.end();)
        {
            if(((*it)->hierarchyLevel - 1) != (*spit)->hierarchyLevel)
                spit = (*it)->surroundingParents.erase(spit);
            else
                ++spit;
        }
    }

    // all uneven levels are actually holes, so we add them to the next upper level as such
    for(auto it = polygonVec.begin(); it != polygonVec.end();)
    {
        PolygonInfo& pi = *(*it);

        if(1 == (pi.hierarchyLevel % 2))
        {
            zfbPolygon& hole = pi.polygon;
            bg::reverse(hole);

            std::vector<double>& holeElev = pi.elevations;
            std::reverse(holeElev.begin(), holeElev.end());

            PolygonInfo* spi = pi.surroundingParents.front();
            spi->polygon.inners().push_back(hole.outer());
            spi->holeElevations.push_back(holeElev);

            it = polygonVec.erase(it);
        }
        else
        {
            if(0 < pi.hierarchyLevel)
                pi.surroundingParents.clear();

            ++it;
        }
    }
}

vtkAtgTopoCutter::vtkAtgTopoCutter()
:   OutputOption(RemoveInnerTriangles),
    DelaunayOffset(100.),
    NumPointsInserted(0)
{
    SetNumberOfInputPorts(2);
}

vtkAtgTopoCutter::~vtkAtgTopoCutter()
{
}

int vtkAtgTopoCutter::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - triangulated topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        case 1:
            // second input - boundary polygon(s)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTopoCutter::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output - triangulated topo surface with cut out "holes"
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTopoCutter::RequestData(vtkInformation* request,
                                  vtkInformationVector** inputVector,
                                  vtkInformationVector* outputVector)
{
    (void)request;

    // get the input and output
    vtkPolyData *input = vtkPolyData::GetData(inputVector[0], 0),
                *cutter = vtkPolyData::GetData(inputVector[1], 0),
                *output = vtkPolyData::GetData(outputVector, 0);

    // make sure we have two input data objects
    if((nullptr == input) || (0 == input->GetNumberOfCells()))
    {
        vtkOutputWindowDisplayWarningText("No or empty input polydata object found\n");
        return 1;
    }

    // see if we have a cutter
    if((nullptr == cutter) || (0 == cutter->GetNumberOfCells()))
    {
        vtkOutputWindowDisplayWarningText("No or empty cutter polydata object found\n");
        return 1;
    }

    // we want to find out the dimensionality of the cutter,
    // and we accept only purely linear cutters
    bool allLinear = true;
    for(vtkIdType c = 0; allLinear && (c < cutter->GetNumberOfCells()); ++c)
        allLinear = 1 == cutter->GetCell(c)->GetCellDimension();
    if(!allLinear)
    {
        vtkOutputWindowDisplayWarningText("The cutter must be one or several polygon outlines\n");
        return 1;
    }

    // we accept only triangulated surfaces as input, so we make sure that it is the case
    auto triFiltInput = vtkSmartPointer<vtkTriangleFilter>::New();
    triFiltInput->SetInputData(input);
    triFiltInput->SetPassLines(false);
    triFiltInput->SetPassVerts(false);
    triFiltInput->Update();
    vtkPolyData* surfaceTriangles = triFiltInput->GetOutput();

    // this filter ensures that we have only polygons that are defined by closed polygon
    // lines (first and last point id are the same)
    // at the same time, points that are shared by more than two line segments, are split,
    // by duplicating these points and making sure that the limit is met, and this in a
    // way that no cross-overs are being generated
    auto contourCutter2 = vtkSmartPointer<vtkAtgContourLoopExtraction>::New();
    contourCutter2->SetInputData(cutter);
    contourCutter2->Update();

    // if inserting additional points is required, we do it here
    auto contourPolygons = vtkSmartPointer<vtkPolyData>::New();
    if(0 >= NumPointsInserted)
    {
        contourPolygons->ShallowCopy(contourCutter2->GetOutput());
    }
    else
    {
        contourPolygons->Allocate();
        vtkPolyData* inCutter = contourCutter2->GetOutput();
        vtkPoints* inCutterPts = inCutter->GetPoints();
        auto outCutterPts = vtkSmartPointer<vtkPoints>::New();
        contourPolygons->SetPoints(outCutterPts);

        for(vtkIdType c = 0; c < inCutter->GetNumberOfCells(); ++c)
        {
            vtkCell* inCell = inCutter->GetCell(c);
            vtkIdList* inIds = inCell->GetPointIds();
            auto outIds = vtkSmartPointer<vtkIdList>::New();

            vtkIdType prevPt = -1;
            double pt1[3];
            for(vtkIdType p = 0; p < inCell->GetNumberOfPoints(); ++p)
            {
                double pt2[3];
                inCutterPts->GetPoint(inIds->GetId(p), pt2);

                if(0 <= prevPt)
                {
                    double diff[3] =
                    {
                        (pt2[0] - pt1[0]) / (NumPointsInserted + 1.),
                        (pt2[1] - pt1[1]) / (NumPointsInserted + 1.),
                        (pt2[2] - pt1[2]) / (NumPointsInserted + 1.)
                    };

                    for(int i = 1; i <= NumPointsInserted; ++i)
                    {
                        double pt[3] =
                        {
                            pt1[0] + i * diff[0],
                            pt1[1] + i * diff[1],
                            pt1[2] + i * diff[2]
                        };

                        outIds->InsertNextId(outCutterPts->InsertNextPoint(pt));
                    }
                }

                outIds->InsertNextId(outCutterPts->InsertNextPoint(pt2));

                prevPt = p;
                ::memcpy(pt1, pt2, 3 * sizeof(double));
            }

            contourPolygons->InsertNextCell(VTK_POLY_LINE, outIds);
        }
    }

    // establish an includion hierarchy for the generated polygons
    std::vector<std::unique_ptr<PolygonInfo>> polygonVec;
    PolygonInfo::generatePolygonHierarchy(contourPolygons, polygonVec);

    // sort the vector in such a way that outer polygons come always before included polygons
    std::sort(polygonVec.begin(), polygonVec.end(),
              [](std::unique_ptr<PolygonInfo> const& pgi1,
                 std::unique_ptr<PolygonInfo> const& pgi2)->bool
    {
        return pgi1->hierarchyLevel < pgi2->hierarchyLevel;
    });

    // here we will collect the output geometries
    auto outSurface = vtkSmartPointer<vtkPolyData>::New();
    outSurface->Allocate();
    auto outPoints = vtkSmartPointer<vtkPoints>::New();
    outSurface->SetPoints(outPoints);

    // collect surface triangles for further processing, if they are interfering with
    // the cutter bounds
    double bnd[6];
    contourPolygons->GetBounds(bnd);
    bnd[4] = -1.;
    bnd[5] = 1.;
    vtkBoundingBox cutterBounds(bnd);
    std::vector<TriangleInfo> surfaceTriInfo;
    vtkPoints* surfacePoints = surfaceTriangles->GetPoints();
    for(vtkIdType c = 0; c < surfaceTriangles->GetNumberOfCells(); ++c)
    {
        vtkCell* cell = surfaceTriangles->GetCell(c);
        TriangleInfo info(cell, surfacePoints);

        // if a triangle bound is within the cutter bound, we have to handle it further100
        if(cutterBounds.Intersects(info.bound))
        {
            surfaceTriInfo.push_back(info);
        }

        // if it is outside, and if we are not going to remove tht outer triangles, then
        // we can simply copy the triangle from the input to the output
        else if(OutputOption != RemoveOuterTriangles)
        {
            auto outIds = vtkSmartPointer<vtkIdList>::New();
            for(int i = 0; i < 3; ++i)
                outIds->InsertNextId(outPoints->InsertNextPoint(info.pts[i]));
            outSurface->InsertNextCell(VTK_TRIANGLE, outIds);
        }
    }

    // go through the triangles and add the output
    // note: this function has to respect also the OutputOption for proper operation
    vtkIdType oldPoints = outSurface->GetPoints()->GetNumberOfPoints();
    bool success = true;
    for(auto sit = surfaceTriInfo.begin(); sit != surfaceTriInfo.end(); ++sit)
    {
        bool triangulationSucceeded = true;
        addCutTriangle(&(*sit), &polygonVec, outSurface, triangulationSucceeded);
        success = success && triangulationSucceeded;
    }

    // warning and instructions for the case that retriangulation failed to fully
    // respect all constraint edges
    if(!success)
    {
        vtkOutputWindowDisplayWarningText(
                    "Retriangulation of cut surfaces not fully succeeded:\n"
                    "- If you see missing triangles (unwanted holes), increasing the Delaunay Offset may help\n"
                    "- If the triangulation does not fully respect the cut polygons, it may help to insert points "
                    "between the existing input polygon points\n");
    }

    // project the new points on the old surface
    projectPointsOnSurface(surfaceTriangles, outSurface->GetPoints(), oldPoints);

    // merge back all the newly added triangles
    auto cleanFilt = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanFilt->SetInputData(outSurface);
    cleanFilt->SetToleranceIsAbsolute(true);
    cleanFilt->SetAbsoluteTolerance(distEps);
    cleanFilt->SetPointMerging(true);
    cleanFilt->Update();

    output->ShallowCopy(cleanFilt->GetOutput());
    return 1;
}

void vtkAtgTopoCutter::addCutTriangle(TriangleInfo const* triangle,
                                      std::vector<std::unique_ptr<PolygonInfo>> const* cutter,
                                      vtkPolyData* output, bool& success)
{
    // here we are going to add the output points to an existing output poly data
    vtkPoints* outPoints = output->GetPoints();

    // initialize two polygon lists, so we can switch between them
    std::list<zfbPolygon> tempSurf[2];
    tempSurf[0].push_back(triangle->triangle);

    // go through the cutter polygons, cutting the input triangles
    int inInx = 0,
        outInx = 1;
    for(auto cit = cutter->begin(); cit != cutter->end(); ++cit)
    {
        for(auto sit = tempSurf[inInx].begin(); sit != tempSurf[inInx].end(); ++sit)
        {
            std::list<zfbPolygon> tempSurfResult;
            if(OutputOption == RemoveInnerTriangles)
                bg::difference(*sit, (*cit)->polygon, tempSurfResult);
            else // RemoveOuterTriangles
                bg::intersection(*sit, (*cit)->polygon, tempSurfResult);
            for(auto it = tempSurfResult.begin(); it != tempSurfResult.end(); ++it)
                tempSurf[outInx].push_back(*it);
        }

        // if we are keeping outer polygons, the already handled output will be
        // the input for the next cutter polygon, while otherwise we always
        // keep the same triangle and cumulate the output
        if(OutputOption == RemoveInnerTriangles)
        {
            inInx = (inInx + 1) % 2;
            outInx = (outInx + 1) % 2;
            tempSurf[outInx].clear();
        }
    }

    // if we were keeping inner polygons, we want to further handle the output,
    // which is always tempSurf[1] in this case, while otherwise it is the set of
    // polygons that would otherwise have been the input for a next cutter polygon
    if(OutputOption == RemoveOuterTriangles)
        inInx = 1;

    // append new triangles to the output surface
    for(auto sit = tempSurf[inInx].begin(); sit != tempSurf[inInx].end(); ++sit)
    {
        // triangulate the output surface polygon element
        zfbPolygon& poly = *sit;
        std::vector<double> polyElev(0, 0.);
        PolygonInfo polyInfo(poly, polyElev);
        auto outTriangles = vtkSmartPointer<vtkPolyData>::New();
        outTriangles->Allocate();
        bool triangulationSucceeded = true;
        makeTriangulate(polyInfo, outTriangles, triangulationSucceeded);
        success = success && triangulationSucceeded;

        // add the triangles to the output polydata
        vtkPoints* triPoints = outTriangles->GetPoints();
        for(vtkIdType tc = 0; tc < outTriangles->GetNumberOfCells(); ++tc)
        {
            vtkCell* tri = outTriangles->GetCell(tc);
            vtkIdList* triPtIds = tri->GetPointIds();
            auto ids = vtkSmartPointer<vtkIdList>::New();
            for(vtkIdType p = 0; p < tri->GetNumberOfPoints(); ++p)
                ids->InsertNextId(outPoints->InsertNextPoint(triPoints->GetPoint(triPtIds->GetId(p))));
            output->InsertNextCell(VTK_TRIANGLE, ids);
        }
    }
}

void vtkAtgTopoCutter::makeTriangulate(PolygonInfo const& poly, vtkPolyData* outTriangles, bool& success)
{
    // working copyhttps://odysee.com/@yellowgenius:0/Dr.-Ryan-Cole--200--Increase-in-Heart-Damage-in-Young-Men-after-Rona-Jab:1
    zfbPolygon polyCopy = poly.polygon;

    // we repeat the triangulations until we get a correct constrained triangulation,
    // inserting points if necessary
    // for the bounded triangulation, we are going to use the VTK delaunay 2D filter
    auto triInput = vtkSmartPointer<vtkPolyData>::New();
    auto triInputPoints = vtkSmartPointer<vtkPoints>::New();
    triInput->Allocate();
    triInput->SetPoints(triInputPoints);

    // collect edges also for later checking
    std::set<EdgeInfo2> inputEdges;
    PointInfo2 prevPoint;

    // add the outer polygon
    // note: all the polygons can be assumed to be closed in the sense that the last point
    //   repeats the first point. However, that way the delaunay 2D filter does not correctly
    //   deal with the constraint lines. In order to fix this, we have to close the polygon
    //   without adding the first point a second time, but just adding the first point id
    //   again, so it is clear that this one is really the same
    auto triInputOuterIds = vtkSmartPointer<vtkIdList>::New();
    for(auto it = polyCopy.outer().begin(); it != polyCopy.outer().end(); ++it)
    {
        auto it2 = it; ++it2;
        double pt[3] = {it->get<0>(), it->get<1>(), 0.};
        if(it2 != polyCopy.outer().end())
            triInputOuterIds->InsertNextId(triInputPoints->InsertNextPoint(pt));
        else
            triInputOuterIds->InsertNextId(triInputOuterIds->GetId(0));
        if(prevPoint.valid)
            inputEdges.insert(EdgeInfo2(prevPoint.pt, pt));
        prevPoint.setPoint(pt);
    }
    triInput->InsertNextCell(VTK_POLY_LINE, triInputOuterIds);
    prevPoint.valid = false;

    // add any inner (hole) polygons
    for(auto iit = polyCopy.inners().begin(); iit != polyCopy.inners().end(); ++iit)
    {
        auto triInputInnerIds = vtkSmartPointer<vtkIdList>::New();
        for(auto it = iit->begin(); it != iit->end(); ++it)
        {
            auto it2 = it; ++it2;
            double pt[3] = {it->get<0>(), it->get<1>(), 0.};
            if(it2 != iit->end())
                triInputInnerIds->InsertNextId(triInputPoints->InsertNextPoint(pt));
            else
                triInputInnerIds->InsertNextId(triInputInnerIds->GetId(0));
            if(prevPoint.valid)
                inputEdges.insert(EdgeInfo2(prevPoint.pt, pt));
            prevPoint.setPoint(pt);
        }
        triInput->InsertNextCell(VTK_POLY_LINE, triInputInnerIds);
        prevPoint.valid = false;
    }

    // triangulate the polygon and holes with the Delaunay 2D filter
    // note: a second data input with lines, polylines and polygons, using no other points than
    //   already defined in the primary input data, will work constraining the triangulation
    // note: the default offset value is 1., but with this value, we see cases that an initial
    //   triangle will not be fully triangulated after cutting out part of it, i.e. we get a
    //   convex triangulated surface. The documentation of the Delaunay 2D filter says that
    //   increasing that value will reduce the risk of ending up with convex polygons, but raised
    //   the risk of numerical problems. In other words: any such value may be wrong, depending
    //   on the actual triangulation that we need to perform!
    // note: we want the tolerance to be absolute, but the filter expects it as a relation to
    //   the diagonal of the bounding box
    vtkBoundingBox inputBBox(triInput->GetBounds());
    auto delFilter = vtkSmartPointer<vtkDelaunay2D>::New();
    delFilter->SetInputData(triInput);
    delFilter->SetInputDataObject(1, triInput);
    delFilter->SetOffset(DelaunayOffset);
    delFilter->SetTolerance(distEps / inputBBox.GetDiagonalLength());
    delFilter->Update();

    // collect all edges in the new triangulation
    std::set<EdgeInfo2> triangEdges;
    vtkPoints* triPoints = delFilter->GetOutput()->GetPoints();
    for(vtkIdType c = 0; c < delFilter->GetOutput()->GetNumberOfCells(); ++c)
    {
        vtkIdList* ids = delFilter->GetOutput()->GetCell(c)->GetPointIds();
        prevPoint.valid = false;
        for(vtkIdType p = 0; p < 4; ++p)
        {
            double pt[3];
            triPoints->GetPoint(ids->GetId(p % 3), pt);
            if(prevPoint.valid)
                triangEdges.insert(EdgeInfo2(prevPoint.pt, pt));
            prevPoint.setPoint(pt);
        }
        prevPoint.valid = false;
    }

    // check whether all input edges are now really part of the triangulation,
    // i.e. the constraints were really followed
    success = true;
    for(auto iit = inputEdges.begin(); (iit != inputEdges.end()); ++iit)
        success = success && triangEdges.end() != triangEdges.find(*iit);

    // remove either the inner or outer triangles
    removeTrianglesBeyondBound(delFilter->GetOutput(), poly, outTriangles);
}

void vtkAtgTopoCutter::removeTrianglesBeyondBound(vtkPolyData* inSurf, PolygonInfo const& bound,
                                                  vtkPolyData* outSurf)
{
    // with BuildCells, we generate a structure that allows random access to cells,
    // which we will need for calls to DeleteCell
    if(inSurf->NeedToBuildCells())
        inSurf->BuildCells();

    // go through all triangles and mark as deleted whatever is inside, then
    // remove all deleted cells
    // note: removing the cells also takes care of the cell data array rows, but not
    //   of the points
    zfbStrategyFranklin strat;
    for(vtkIdType c = 0; c < inSurf->GetNumberOfCells(); ++c)
    {
        vtkTriangle* tri = vtkTriangle::SafeDownCast(inSurf->GetCell(c));
        if(nullptr == tri)
            continue;

        double cent[3];
        vtkIdType ids[3] = {0, 1, 2};
        tri->ComputeCentroid(tri->GetPoints(), ids, cent);
        zfbPoint centPoint(cent[0], cent[1]);

        bool inside = bg::within(centPoint, bound.polygon, strat);
        if(!inside)
            inSurf->DeleteCell(c);
    }
    inSurf->RemoveDeletedCells();

    // with this we get rid of any now unused points and also remove related
    // point data array rows
    vtkSmartPointer<vtkCleanPolyData> cleanPD = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanPD->SetInputData(inSurf);
    cleanPD->Update();

    outSurf->DeepCopy(cleanPD->GetOutput());
}

void vtkAtgTopoCutter::projectPointsOnSurface(vtkPolyData* surf, vtkPoints* points, vtkIdType firstId)
{
    vtkBoundingBox surfBbox(surf->GetBounds());

    vtkSmartPointer<vtkCellLocator> cellLocator = vtkSmartPointer<vtkCellLocator>::New();
    cellLocator->SetDataSet(surf);
    cellLocator->SetMaxLevel(100);
    cellLocator->SetNumberOfCellsPerBucket(7);
    cellLocator->SetTolerance(distEps);
    cellLocator->BuildLocator();

    double pt1[3],
           pt2[3],
           t,
           pt[3],
           pcoords[3];
    int subid;
    vtkIdType cellid;
    vtkSmartPointer<vtkGenericCell> cell = vtkSmartPointer<vtkGenericCell>::New();
    for(vtkIdType p = firstId; p < points->GetNumberOfPoints(); ++p)
    {

        points->GetPoint(p, pt1);
        ::memcpy(pt2, pt1, sizeof(pt2));
        pt1[2] = surfBbox.GetMinPoint()[2];
        pt2[2] = surfBbox.GetMaxPoint()[2];
        if(cellLocator->IntersectWithLine(pt1, pt2, distEps, t, pt, pcoords, subid, cellid, cell))
            points->SetPoint(p, pt);
    }
}

void vtkAtgTopoCutter::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
