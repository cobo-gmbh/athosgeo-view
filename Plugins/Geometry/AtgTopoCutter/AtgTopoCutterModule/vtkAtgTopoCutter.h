/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTopoCutter.h

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTopoCutter_h
#define vtkAtgTopoCutter_h

#include <vector>
#include <memory>
#include <vtkPolyDataAlgorithm.h>
#include <AtgTopoCutterModuleModule.h>

struct TriangleInfo;
struct PolygonInfo;

class ATGTOPOCUTTERMODULE_EXPORT vtkAtgTopoCutter: public vtkPolyDataAlgorithm
{
public:

    // note: initially there was a third option: "imprint only", meaning that only the
    //   cut was done, but neither inner nor outer triangles were removed. However, we
    //   are not any more using the "imprint" filter, but rather cut the surface
    //   triangles one by one, and sometimes they need to be subdivided in order to be
    //   successful. If afterwards the parts would be put together again, we would not
    //   always end up with correct triangulations any more, because an edge may have
    //   been subdividef for the "remove inner..." version, but not for the other, and
    //   so the do not fully fit any more afterwards
    typedef enum
    {
        RemoveInnerTriangles,
        RemoveOuterTriangles
    }
    OutputOptions;

    static vtkAtgTopoCutter* New();
    vtkTypeMacro(vtkAtgTopoCutter, vtkPolyDataAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    virtual void SetOutputOption(int oOpt)
    {
        if(OutputOption != oOpt)
        {
            OutputOption = OutputOptions(oOpt);
            Modified();
        }
    }

    vtkGetMacro(OutputOption, OutputOptions)

    vtkSetMacro(DelaunayOffset, double)
    vtkGetMacro(DelaunayOffset, double)

    vtkSetMacro(NumPointsInserted, int)
    vtkGetMacro(NumPointsInserted, int)

protected:

    vtkAtgTopoCutter();
    ~vtkAtgTopoCutter();

    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:

    // copy constructor and assignment not implemented
    vtkAtgTopoCutter(vtkAtgTopoCutter const&);
    void operator=(vtkAtgTopoCutter const&);

    void addCutTriangle(TriangleInfo const* triangle,
                        const std::vector<std::unique_ptr<PolygonInfo>>* cutter,
                        vtkPolyData* output, bool& success);

    void makeTriangulate(PolygonInfo const& poly, vtkPolyData* outTriangles, bool& success);

    void removeTrianglesBeyondBound(vtkPolyData* inSurf, PolygonInfo const& bound, vtkPolyData* outSurf);

    void projectPointsOnSurface(vtkPolyData* surf, vtkPoints* points, vtkIdType firstId = 0);

    OutputOptions OutputOption;
    double DelaunayOffset;
    int NumPointsInserted;

};

#endif
