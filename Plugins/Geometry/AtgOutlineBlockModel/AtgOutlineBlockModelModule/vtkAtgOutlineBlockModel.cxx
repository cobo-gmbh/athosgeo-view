/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgOutlineBlockModel.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkDataObject.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkSmartPointer.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkFeatureEdges.h>
#include <vtkCleanPolyData.h>
#include <vtkStripper.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgOutlineBlockModel.h>

static const double distEps = .01;

vtkStandardNewMacro(vtkAtgOutlineBlockModel)

vtkAtgOutlineBlockModel::vtkAtgOutlineBlockModel()
{
}

vtkAtgOutlineBlockModel::~vtkAtgOutlineBlockModel()
{
}

int vtkAtgOutlineBlockModel::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgOutlineBlockModel::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: outline polygon(s)
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgOutlineBlockModel::RequestData(vtkInformation* request,
                                         vtkInformationVector** inputVector,
                                         vtkInformationVector* outputVector)
{
    (void)request;

    // get the input and output
    vtkUnstructuredGrid* input = vtkUnstructuredGrid::GetData(inputVector[0], 0);
    vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

    // extract surface
    vtkSmartPointer<vtkDataSetSurfaceFilter> extractSurface = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    extractSurface->SetInputData(input);
    extractSurface->Update();

    // generate surface normals: splitting, compute cell normals
    vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
    normals->SetInputData(extractSurface->GetOutput());
    normals->SetSplitting(true);
    normals->SetConsistency(false);
    normals->SetComputeCellNormals(true);
    normals->SetComputePointNormals(false);
    normals->Update();

    // extract faces with normal.Z = 1
    // note that only with BuildCells we are able to call DeleteCell afterwards
    vtkPolyData* tempSurf1 = normals->GetOutput();
    if(tempSurf1->NeedToBuildCells())
        tempSurf1->BuildCells();
    vtkDataArray* normArr = tempSurf1->GetCellData()->GetArray("Normals");
    for(vtkIdType c = 0; c < tempSurf1->GetNumberOfCells(); ++c)
    {
        if(.5 < normArr->GetComponent(c, 2))
            tempSurf1->DeleteCell(c);
    }
    tempSurf1->RemoveDeletedCells();

    // bring all points Z coordinates to 0
    vtkPoints* pts = tempSurf1->GetPoints();
    double pt[3];
    for(vtkIdType p = 0; p < pts->GetNumberOfPoints(); ++p)
    {
        pts->GetPoint(p, pt);
        pt[2] = 0.;
        pts->SetPoint(p, pt);
    }
    tempSurf1->Modified();

    // remove orphaned points and merge adjoining edges
    vtkSmartPointer<vtkCleanPolyData> cleanPts = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanPts->SetInputData(tempSurf1);
    cleanPts->SetPointMerging(true);
    cleanPts->SetToleranceIsAbsolute(true);
    cleanPts->SetAbsoluteTolerance(distEps);
    cleanPts->SetConvertPolysToLines(true);
    cleanPts->SetConvertLinesToPoints(true);
    cleanPts->SetConvertPolysToLines(true);
    cleanPts->Update();

    // extract the boundaries
    vtkSmartPointer<vtkFeatureEdges> features = vtkSmartPointer<vtkFeatureEdges>::New();
    features->SetInputData(cleanPts->GetOutput());
    features->SetBoundaryEdges(true);
    features->SetFeatureEdges(false);
    features->SetOutputPointsPrecision(DOUBLE_PRECISION);
    features->Update();

    // so far we have now outlines consisting of only line segments, unconnected.
    // The "triangle strip" filter will append them to each other - even though this
    // does not sound very intuitive... The logic is obviously: What triangles and
    // triangle strips are in the 2D world, would correspond to line segments and
    // polylines in a 1D world.
    // note: with this, also all the attributes will be removed - which was intended with
    //   a vtkPassArrays filter before, without success...
    vtkSmartPointer<vtkStripper> triangStrip = vtkSmartPointer<vtkStripper>::New();
    triangStrip->SetInputData(features->GetOutput());
    triangStrip->SetMaximumLength(100000);
    triangStrip->Update();

    output->ShallowCopy(vtkPolyData::SafeDownCast(triangStrip->GetOutput()));

    return 1;
}

void vtkAtgOutlineBlockModel::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
