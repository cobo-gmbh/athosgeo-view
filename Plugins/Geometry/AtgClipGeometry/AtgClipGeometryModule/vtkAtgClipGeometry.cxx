/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipGeometry.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <vtkDataObject.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkTriangleFilter.h>
#include <vtkAtgImplicitPolyDataDistance.h>
#include <vtkAtgImplicitPolyLineDistance.h>
#include <vtkClipPolyData.h>
#include <vtkClipDataSet.h>
#include <vtkExtractGeometry.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgClipGeometry.h>

static const double epsDist = 0.001;

vtkStandardNewMacro(vtkAtgClipGeometry)

vtkAtgClipGeometry::vtkAtgClipGeometry()
:   ClipCells(false),
    KeepAlongBoundary(false),
    OnlyAlongBoundary(false),
    Invert(false)
{
    SetNumberOfInputPorts(2);
}

vtkAtgClipGeometry::~vtkAtgClipGeometry()
{
}

int vtkAtgClipGeometry::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPointSet");
            return 1;

        case 1:
            // main input - block model
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipGeometry::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipGeometry::RequestDataObject(vtkInformation*,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    // generate data objects of the same data type as the input data object
    // note: with this it is possible that a filter can return different
    //   data types depending on what the input data is
    if((GetNumberOfInputPorts() == 0) || (GetNumberOfOutputPorts() == 0))
        return 1;

    vtkPointSet* input = vtkPointSet::GetData(inputVector[0]);
    if(nullptr == input)
        return 0;

    // for each output
    for(int i = 0; i < GetNumberOfOutputPorts(); ++i)
    {
        vtkInformation* info = outputVector->GetInformationObject(i);
        vtkPointSet* output = vtkPointSet::SafeDownCast(info->Get(vtkDataObject::DATA_OBJECT()));

        if((nullptr == output) || !output->IsA(input->GetClassName()))
        {
            vtkPointSet* newOutput = input->NewInstance();
            info->Set(vtkDataObject::DATA_OBJECT(), newOutput);
            newOutput->Delete();
        }
    }

    return 1;
}

int vtkAtgClipGeometry::RequestData(vtkInformation* request,
                                    vtkInformationVector** inputVector,
                                    vtkInformationVector* outputVector)
{
    (void)request;

    // get the input and output point sets
    // note that the first would normally be either unstructured grids or polydata
    vtkPointSet* input = vtkPointSet::GetData(inputVector[0], 0);
    vtkPolyData* clipper = vtkPolyData::GetData(inputVector[1], 0);
    vtkPointSet* output = vtkPointSet::GetData(outputVector, 0);

    // handled input data items
    vtkUnstructuredGrid* inGrid = vtkUnstructuredGrid::SafeDownCast(input);
    vtkPolyData* inPolyData = vtkPolyData::SafeDownCast(input);
    if((nullptr == inGrid) && (nullptr == inPolyData))
    {
        vtkOutputWindowDisplayWarningText((std::string("Data input type ") +
                                           input->GetClassName() +
                                           " is not supported").c_str());
        output->ShallowCopy(input);
    }

    // use the triangle filter to find out if we are having a clipper that is a surface
    vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->PassVertsOff();
    triangleFilter->PassLinesOff();
    triangleFilter->SetInputData(clipper);
    triangleFilter->Update();
    bool isSurface = (nullptr != triangleFilter->GetOutput()) &&
                     (0 < triangleFilter->GetOutput()->GetNumberOfCells());

    // generate an implicit function from the clipper
    vtkImplicitFunction* impFunc = nullptr;
    if(isSurface)
    {
        auto impPdDist = vtkAtgImplicitPolyDataDistance::New();
        impPdDist->SetInput(clipper);
        impPdDist->SetTolerance(epsDist);
        impPdDist->SetInvertDistances(Invert);
        impFunc = impPdDist;
    }
    else
    {
        auto impProjDist = vtkAtgImplicitPolyLineDistance::New();
        impProjDist->SetInput(clipper);
        impProjDist->SetInvertDistances(Invert);
        impFunc = impProjDist;
    }

    // clip through the cells
    if(ClipCells)
    {
        // clip an unstructured grid
        if(nullptr != inGrid)
        {
            auto dsClip = vtkSmartPointer<vtkClipDataSet>::New();
            dsClip->SetInputData(inGrid);
            dsClip->SetClipFunction(impFunc);
            dsClip->Update();
            output->DeepCopy(dsClip->GetOutput());
        }

        // clip polydata
        else if(nullptr != inPolyData)
        {
            auto pdClip = vtkSmartPointer<vtkClipPolyData>::New();
            pdClip->SetInputData(inPolyData);
            pdClip->SetClipFunction(impFunc);
            pdClip->SetOutputPointsPrecision(VTK_DOUBLE);
            pdClip->Update();
            output->DeepCopy(pdClip->GetOutput());
        }
    }

    // extract entire cells
    else
    {
        // extract from unstructured grid
        if(nullptr != inGrid)
        {
            // note: inverting happens here because we are inverting the implicit
            //   function already, and the ExtractInside is only to make the behaviour
            //   consistent with the above clipping filters
            // note: the only along boundary filter works only if also keep along
            //   boundary is on at the same time
            auto dsExtract = vtkSmartPointer<vtkExtractGeometry>::New();
            dsExtract->SetInputData(inGrid);
            dsExtract->SetImplicitFunction(impFunc);
            dsExtract->SetExtractInside(false);
            dsExtract->SetExtractBoundaryCells(OnlyAlongBoundary || KeepAlongBoundary);
            dsExtract->SetExtractOnlyBoundaryCells(OnlyAlongBoundary);
            dsExtract->Update();
            output->DeepCopy(dsExtract->GetOutput());
        }

        // extract from polydata
        else if(nullptr != inPolyData)
        {
            auto pdExtract = vtkSmartPointer<vtkExtractPolyDataGeometry>::New();
            pdExtract->SetInputData(inPolyData);
            pdExtract->SetImplicitFunction(impFunc);
            pdExtract->SetExtractInside(false);
            pdExtract->SetExtractBoundaryCells(KeepAlongBoundary);
            pdExtract->Update();
            output->DeepCopy(pdExtract->GetOutput());
        }
    }

    // this one we need to clean up ourselves
    impFunc->Delete();

    return 1;
}

void vtkAtgClipGeometry::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
