set(classes
    vtkAtgTextureGeoReferenced)

vtk_module_add_module(AthosGeoView::AtgTextureGeoReferencedModule
  CLASSES
    ${classes}
  PRIVATE_CLASSES
    ${private_classes})

paraview_add_server_manager_xmls(
  XMLS plugin.xml)

# the only purpose of this is to bring the vtk.module file into QtCreator
add_custom_target(AtgTextureGeoReferenced_module_spec
  SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/vtk.module)

