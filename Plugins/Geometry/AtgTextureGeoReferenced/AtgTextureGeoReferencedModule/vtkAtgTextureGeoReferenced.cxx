/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTextureGeoReferenced.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <vtkDataObject.h>
#include <vtkPolyData.h>
#include <vtkFieldData.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkCallbackCommand.h>
#include <vtkGDALRasterReader.h>
#include <vtkImageData.h>
#include <vtkTextureMapToPlane.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTextureGeoReferenced.h>

vtkStandardNewMacro(vtkAtgTextureGeoReferenced)

static void OnError(vtkObject* vtkNotUsed(caller),
                    unsigned long int vtkNotUsed(eventId),
                    void* clientData,
                    void* callData)
{
    // with this we signal that there was an error in the raster reader
    // note: we do not want to see a message from the raster reader
    bool* errorFlag = static_cast<bool*>(clientData);
    *errorFlag = true;

    // this would be the message that the raster reader wants to issue
    char const* msg = static_cast<char const*>(callData);
}

vtkAtgTextureGeoReferenced::vtkAtgTextureGeoReferenced()
:   MapFileName(nullptr),
    FlipVertically(false),
    IgnoreAlpha(true)
{
}

vtkAtgTextureGeoReferenced::~vtkAtgTextureGeoReferenced()
{
    SetMapFileName(nullptr);
}

int vtkAtgTextureGeoReferenced::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // triangulated topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTextureGeoReferenced::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // textured topo surface
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTextureGeoReferenced::RequestData(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    (void)request;

    // get the input and output surfaces
    vtkPolyData* input = vtkPolyData::GetData(inputVector[0], 0);
    vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

    // properties
    std::string mfNameStr;
    if(nullptr != MapFileName)
        mfNameStr = MapFileName;

    // this texture mapper will do the actual texturing
    textureMapper->SetInputData(input);

    // if we have a map file, we want to extract and use the georeferencing info
    bool hasGeoRef = false;
    if(!mfNameStr.empty())
    {
        // we want to catch any error condition from the raster reader and handle it here,
        // and for that we need the readerError flag
        vtkNew<vtkCallbackCommand> errObs;
        errObs->SetCallback(&OnError);
        bool readerError = false;
        errObs->SetClientData(&readerError);

        // use the GDAL raster reader to read the raster image
        vtkSmartPointer<vtkGDALRasterReader> gdalReader = vtkSmartPointer<vtkGDALRasterReader>::New();
        gdalReader->SetFileName(mfNameStr.c_str());
        gdalReader->AddObserver("ErrorEvent", errObs);
        gdalReader->Update();
        vtkImageData* gdalImg = gdalReader->GetOutput();

        // if an image is from TIFF, then there is a chance that it needs vertical flipping, while
        // JPG or PNG do not need it. This flipping will be inverted if the explicit flip flag is on.
        bool needsFlipping = gdalReader->GetDriverShortName() == "GTiff";

        // if we had a reader error, not much we can do
        if(readerError || (nullptr == gdalImg))
        {
            vtkOutputWindowDisplayWarningText("No georeferencing found in the texture map file\n");
        }

        // otherwise we are going to apply the georeferencing to the texture mapper
        else
        {
            // get the georeferencing
            double org[3],
                   spc[3];
            gdalImg->GetOrigin(org);
            gdalImg->GetSpacing(spc);
            vtkIdType dims[3];
            gdalImg->GetDimensions(dims);
            double pt1[2] = {org[0] + spc[0] * dims[0], org[1]},
                   pt2[2] = {org[0], org[1] + spc[1] * dims[1]};

            // flip vertically if required
            // note: the ^ operator is xor
            if(FlipVertically ^ needsFlipping)
            {
                double orgy = pt2[1];
                pt2[1] = org[1];
                org[1] = orgy;
                pt1[1] = org[1];
            }

            // convert this into origin / point1 / point2 for the texture mapper
            textureMapper->SetOrigin(org[0], org[1], 0.);
            textureMapper->SetPoint1(pt1[0], pt1[1], 0.);
            textureMapper->SetPoint2(pt2[0], pt2[1], 0.);
            textureMapper->SetAutomaticPlaneGeneration(false);

            // from now we have true georeferencing coordinates
            hasGeoRef = true;
        }
    }

    else
    {
        vtkOutputWindowDisplayWarningText("No texture file found\n");
    }

    // we have no texture mapping coordinate, so just go for automatic
    if(!hasGeoRef)
        textureMapper->SetAutomaticPlaneGeneration(true);

    // now do it, and get the output
    textureMapper->Update();
    output->DeepCopy(textureMapper->GetOutput());

    // remember the map file
    if(hasGeoRef)
    {
        // get existing fdata or generate a new one
        vtkFieldData* fdata = output->GetFieldData();
        if(nullptr == fdata)
        {
            fdata = vtkFieldData::New();
            output->SetFieldData(fdata);
            fdata->Delete();
        }

        // in order to be really really sure: if there are already arrays with less than 1 element,
        // we need to expand them all
        if((fdata->GetNumberOfArrays() > 0) && (fdata->GetNumberOfTuples() < 1))
        {
            for(vtkIdType a = 0; a < fdata->GetNumberOfArrays(); ++a)
                fdata->GetAbstractArray(a)->SetNumberOfTuples(1);
        }

        // get or generate a map file array
        std::string tmfName("__TextureMapFile__");
        vtkStringArray* tmfArr = vtkStringArray::SafeDownCast(fdata->GetAbstractArray(tmfName.c_str()));
        if(nullptr == tmfArr)
        {
            tmfArr = vtkStringArray::New();
            tmfArr->SetName(tmfName.c_str());
            tmfArr->SetNumberOfValues(std::max<vtkIdType>(fdata->GetNumberOfTuples(), 1));
            fdata->AddArray(tmfArr);
            tmfArr->Delete();
        }

        // and now we can write that file name to the field data
        tmfArr->SetValue(0, mfNameStr);

        // get or generate an ignore alpha array
        std::string iaName("__IgnoreAlpha__");
        vtkIntArray* iaArr = vtkIntArray::SafeDownCast(fdata->GetAbstractArray(iaName.c_str()));
        if(nullptr == iaArr)
        {
            iaArr = vtkIntArray::New();
            iaArr->SetName(iaName.c_str());
            iaArr->SetNumberOfValues(std::max<vtkIdType>(fdata->GetNumberOfTuples(), 1));
            fdata->AddArray(iaArr);
            iaArr->Delete();
        }

        // and now we can write the ignore alpha flag to the field data
        iaArr->SetValue(0, IgnoreAlpha);
    }

    return 1;
}

void vtkAtgTextureGeoReferenced::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
