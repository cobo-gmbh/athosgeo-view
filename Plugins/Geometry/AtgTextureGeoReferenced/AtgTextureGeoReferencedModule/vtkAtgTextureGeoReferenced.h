/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTextureGeoReferenced.h

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTextureGeoReferenced_h
#define vtkAtgTextureGeoReferenced_h

#include <vtkPolyDataAlgorithm.h>
#include <AtgTextureGeoReferencedModuleModule.h>

class vtkTextureMapToPlane;

class ATGTEXTUREGEOREFERENCEDMODULE_EXPORT vtkAtgTextureGeoReferenced: public vtkPolyDataAlgorithm
{
public:

    static vtkAtgTextureGeoReferenced* New();
    vtkTypeMacro(vtkAtgTextureGeoReferenced, vtkPolyDataAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    // with this own implementation we ensure that RequestData is called on
    // changing the map file name, so we can read the texture and georeferencing there
    vtkSetStringMacro(MapFileName)
    vtkGetStringMacro(MapFileName)

    // in case the image is displayed upside down, this will flip it back to normal
    vtkSetMacro(FlipVertically, bool)
    vtkGetMacro(FlipVertically, bool)

    // ignoring alpha means: opaque texture, and applying it means making the surface
    // itself partially/fully transparent (no color mixing only!)
    vtkSetMacro(IgnoreAlpha, bool)
    vtkGetMacro(IgnoreAlpha, bool)

protected:

    vtkAtgTextureGeoReferenced();
    ~vtkAtgTextureGeoReferenced();

    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:

    // copy constructor and assignment not implemented
    vtkAtgTextureGeoReferenced(vtkAtgTextureGeoReferenced const&);
    void operator=(vtkAtgTextureGeoReferenced const&);

    // name of georeferenced texture map file
    char* MapFileName;

    // flip vertically option - and previous setting of the option
    bool FlipVertically;

    // ignore the alpha channel if it exists - meaning: everything should be opaque
    // note: during texturing, the alpha channel is not applied to the color only, but
    //   to the appearance of the surface itself - which becomes transparent wherever
    //   the texture image has a reduced value of alpha
    bool IgnoreAlpha;

    // texture to plane mapper object, doing the real work
    vtkNew<vtkTextureMapToPlane> textureMapper;

};

#endif
