/*=========================================================================

   Program: AthosGEO
   Module:  vtkSMPitArraysDomain.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <utilNormNames.h>
#include <vtkObjectFactory.h>
#include <vtkPVArrayInformation.h>
#include <vtkPVDataInformation.h>
#include <vtkPVDataSetAttributesInformation.h>
#include <vtkSMPitArraysDomain.h>

vtkStandardNewMacro(vtkSMPitArraysDomain)

vtkSMPitArraysDomain::vtkSMPitArraysDomain()
:   vtkSMArrayListDomain()
{
}

bool vtkSMPitArraysDomain::IsFilteredArray(vtkPVDataInformation* info,
                                           int association, char const* arrayName)
{
    // if we have no arrInfo we do not want to proceed - and ignore the array
    // note that the arrInfo can be either cell data or row data in our cases
    vtkPVArrayInformation* arrInfo = info->GetCellDataInformation()->GetArrayInformation(arrayName);
    if(nullptr == arrInfo)
        arrInfo = info->GetRowDataInformation()->GetArrayInformation(arrayName);
    if(nullptr == arrInfo)
        return true;

    // we do not want to deal here with multi-component arrays
    int numComps = arrInfo->GetNumberOfComponents();
    if(1 < numComps)
        return true;

    // types:
    // - WEIGHT - additional weight for summarizing, normally pits
    switch(utilNormNames::getType(arrayName))
    {
        case utilNormNames::TY_WEIGHT:
            return false;

        default:
            return true;
    }
}

void vtkSMPitArraysDomain::PrintSelf(ostream& os, vtkIndent indent)
{
    os << "vtkSMPitArraysDomain" << std::endl;
    vtkSMArrayListDomain::PrintSelf(os, indent);
}
