/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoShapeReader.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkShapeFileReader from VTK\IO\vtkSimplePointsReader & vtkLocalConeSource - Example ParaView Plugin Source
// .SECTION Description
// vtkShapeFileReader is a subclass of vtkPolyDataAlgorithm to read Data Files Extracted from DXF Files.
// Read Data Files (*.shp / *.shx) ESRI data files.
// POINT, LINE, POLYGON, MESH
// Without properties

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <list>
#include <string>

#include <vtkGeoShapeRead.h>
#include <vtkGeoShapeMGDecl.h>
#include <vtkGeoShapeHPolygon.h>

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkStringList.h>
#include <vtkLongArray.h>
#include <vtkOutputWindow.h>

#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkGeoShapeReader.h>

// define Gocad file type identifers.
#define POINTOBJ 1
#define LINEOBJ 2
#define FACEOBJ 3
#define MESHOBJ 4

vtkStandardNewMacro(vtkGeoShapeReader)

vtkGeoShapeReader::vtkGeoShapeReader()
:   FileName(nullptr)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkGeoShapeReader::~vtkGeoShapeReader()
{
    SetFileName(nullptr);
}

void vtkGeoShapeReader::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os,indent);
    os << indent <<  "FileName: "
       << (FileName ? FileName : "(none)") << "\n";
}

int vtkGeoShapeReader::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // output geometry
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkGeoShapeReader::RequestData(vtkInformation* request,
                                   vtkInformationVector** inputVector,
                                   vtkInformationVector* outputVector)
{
    // Make sure we have a shape file to read.
    if(!FileName || (::strlen(FileName) == 0))
    {
        vtkOutputWindowDisplayWarningText("GeoShapeReader: No filename found\n");
        return 1;
    }

    // check for existence of both .shp and .shx files
    std::string shpName = FileName,
                shxName = FileName;
    int dotpos = shxName.find_last_of(".");
    shxName.replace(dotpos, 4, ".shx");
    std::ifstream shp(shpName.c_str());
    std::ifstream shx(shxName.c_str());

    // check both .shp and .SHP
    bool read_failed = false;
    if(!shp.good())
    {
        shp.close();
        shpName.replace(dotpos, 4, ".SHP");
        shp.open(shpName.c_str());
        if(!shp.good())
        {
            read_failed = true;
            vtkOutputWindowDisplayWarningText("Could not find .SHP or .shp file");
        }
    }

    // check also both .shx and .SHX
    if(!shx.good())
    {
        shx.close();
        shxName.replace(dotpos, 4, ".SHX");
        shx.open(shxName.c_str());
        if(!shx.good())
        {
            read_failed = true;
            vtkOutputWindowDisplayWarningText("Could not find .SHX or .shx file");
        }
    }

    // failure: clean up and leave
    if(read_failed)
    {
        shp.close();
        shx.close();
        return 1;
    }

    // variables
    vtkIdType* nodes = nullptr;
    vtkIdType fnodes[4];
    vtkSmartPointer<vtkPoints> myPointsPtr = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> myCellsPtr = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkCellArray> faceCellsPtr = vtkSmartPointer<vtkCellArray>::New();
    //vtkFloatArray* fap = nullptr;
    int filetype = 0;
    HPolygon poly;
    int nPolygons = 0;
    double baseZ;
    vtkLongArray* fapLAP = vtkLongArray::New();

    // Get SHP Handle (SHPInfo struct pointer.
    SHPHandle shpih = SHPOpen(shpName.c_str(), "rb");
    if(shpih == NULL)
    {
        // in case the .shx is missing or the shape file reader failed
        fapLAP->Delete();
        return 0;
    }

    // find the geometry type
    SHPObject* shpop = nullptr;
    switch(shpih->nShapeType)
    {
        case SHPT_NULL:
        {
            std::string err = std::string("NULL object type in file\n") + FileName;
            vtkOutputWindowDisplayWarningText(err.c_str());
            SHPClose(shpih);
            fapLAP->Delete();
            return 1;
        }

        case SHPT_POINT:
        case SHPT_POINTZ:
        case SHPT_POINTM:
        case SHPT_MULTIPOINT:
        case SHPT_MULTIPOINTZ:
        case SHPT_MULTIPOINTM:
        {
            filetype = POINTOBJ;
            break;
        }

        case SHPT_ARC:  // Polyline
        case SHPT_ARCZ:
        case SHPT_ARCM:
        {
            filetype = LINEOBJ;
            break;
        }

        case SHPT_POLYGON:
        case SHPT_POLYGONZ:
        case SHPT_POLYGONM:
        {
            // default: form triangulated surfaces.
            filetype = FACEOBJ;

            // arbitrary limit set to avoid triangulation overload
            // (e.g. ArcGIS\ArcTutor\Map\airport.mdb.shp)
            // note BCO: this limit is pretty low in 2019: is it really required?
            for(int ri0 = 0; ri0 < shpih->nRecords; ri0++)
            {
                shpop = SHPReadObject(shpih, ri0);
                if(shpop->nVertices > 100)
                {
                    // make it a polygon outline only.
                    filetype = LINEOBJ;
                    break;
                }
            }
            break;
        }

        case SHPT_MULTIPATCH:
        {
            filetype = MESHOBJ;
            nPolygons = 0;
            for(int ri0 = 0; ri0 < shpih->nRecords; ri0++)
            {
                shpop = SHPReadObject(shpih, ri0);
                if((shpop->nSHPType != SHPP_TRISTRIP) && (shpop->nSHPType != SHPP_TRIFAN))
                    nPolygons += shpop->nParts;
            }
            if(nPolygons > 0)
                poly.mtabSize.resize(nPolygons);
            break;
        }

        default:
        {
            std::string err = std::string("Unknown object type found in file\n") + FileName;
            vtkOutputWindowDisplayWarningText(err.c_str());
            SHPClose(shpih);
            fapLAP->Delete();
            return 1;
        }
    }

    // some more variables
    char errbuff[80];
    int pid = 0,
        pidBase,
        ei,
        ei0,
        si,
        pi,
        vi,
        nodecnt,
        polypart = 0;

    for(int ri = 0; ri < shpih->nRecords; ri++)
    {
        shpop = SHPReadObject(shpih, ri);

        // skip some non-supported cases
        if((shpop->nSHPType != shpih->nShapeType) && (shpih->nShapeType != SHPT_MULTIPATCH))
        {
            std::string err = std::string("Object type (") +
                              std::to_string((int)shpop->nSHPType) +
                              ") does not match overall type (" +
                              std::to_string((int)shpih->nShapeType) +
                              "), skipped record " +
                              std::to_string(ri + 1);
            vtkOutputWindowDisplayWarningText(err.c_str());
            continue;
        }

        switch(filetype)
        {
            case POINTOBJ:
            {
                if(nodes != NULL)
                    delete nodes;
                nodes = new vtkIdType[shpop->nVertices];

                for(vi = 0; vi < shpop->nVertices; vi++)
                {
                    myPointsPtr->InsertPoint(pid, shpop->padfX[vi], shpop->padfY[vi], shpop->padfZ[vi]);
                    nodes[vi] = pid++;
                }

                myCellsPtr->InsertNextCell(shpop->nVertices, nodes);
                delete nodes;
                nodes = NULL;
                break;
            }

            case LINEOBJ:
            {
                for(pi = 0; pi < shpop->nParts; pi++)
                {
                    si = shpop->panPartStart[pi];
                    ei = (pi == (shpop->nParts - 1)) ? shpop->nVertices : shpop->panPartStart[pi + 1];

                    if(nodes != NULL)
                        delete nodes;
                    nodes = new vtkIdType[ei-si];

                    for(vi = si; vi < ei; vi++)
                    {
                        myPointsPtr->InsertPoint(pid, shpop->padfX[vi], shpop->padfY[vi], shpop->padfZ[vi]);
                        nodes[vi-si] = pid++;
                    }

                    myCellsPtr->InsertNextCell(ei-si, nodes);
                }
                break;
            }

            case FACEOBJ:
            {
                // HGRD
                poly.mtabSize.resize(shpop->nParts);

                // default Z for polygon(s) of this record.
                baseZ = shpop->padfZ[0];

                for(pi = 0; pi < shpop->nParts; pi++)
                {
                    si = shpop->panPartStart[pi];
                    ei = (pi == (shpop->nParts - 1)) ? shpop->nVertices : shpop->panPartStart[pi + 1];
                    ei0 = ei;

                    if(ei > (si + 200))
                    {
                        ei = si + 200;
                        nodecnt = 201;
                    }
                    else
                    {
                        nodecnt = ei - si;
                    }

                    poly.mtabSize[pi] = nodecnt;
                    polypart++;

                    for(vi = si; vi < ei; vi++)
                        poly.mtabPnt.insert(poly.mtabPnt.end(), Vect2D(shpop->padfX[vi], shpop->padfY[vi]));

                    if(ei != ei0)
                        poly.mtabPnt.insert(poly.mtabPnt.end(), Vect2D(shpop->padfX[si], shpop->padfY[si]));
                }

                pidBase = pid;

                // do TRIANGULATION on poly(s) for this record and build face cells
                poly.Triangulate();
                for(int pti = 0; pti < (int)poly.mtabPnt.size(); pti++)
                    myPointsPtr->InsertPoint(pid++, poly.mtabPnt[pti].X(), poly.mtabPnt[pti].Y(), baseZ);

                for(int tri = 0; tri < (int)poly.mtabCell.size(); tri++)
                {
                    for(int ni = 0; ni < 3; ni++)
                        fnodes[ni] = pidBase + poly.mtabCell[tri].Index(ni);
                    faceCellsPtr->InsertNextCell(3, fnodes);
                }
                break;
            }

            case MESHOBJ:
            {
                for(pi = 0; pi < shpop->nParts; pi++)
                {
                    if((shpop->nSHPType == SHPP_TRISTRIP) || (shpop->nSHPType == SHPP_TRIFAN))
                    {
                        pidBase = pid;
                        si = shpop->panPartStart[pi];
                        ei = (pi == (shpop->nParts - 1)) ? shpop->nVertices : shpop->panPartStart[pi + 1];

                        for(vi = si; vi < ei; vi++)
                            myPointsPtr->InsertPoint(pid++, shpop->padfX[vi], shpop->padfY[vi], shpop->padfZ[vi]);

                        switch(shpop->nSHPType)
                        {

                            case SHPP_TRISTRIP:
                            {
                                for(int sti = pidBase + 2; sti < pid; sti++)
                                {
                                    fnodes[0] = sti - 2;
                                    fnodes[1] = sti - 1;
                                    fnodes[2] = sti;
                                    faceCellsPtr->InsertNextCell(3, fnodes);
                                }
                                break;
                            }

                            case SHPP_TRIFAN:
                            {
                                for(int sti = pidBase + 2; sti < pid; sti++)
                                {
                                    fnodes[0] = pidBase;
                                    fnodes[1] = sti - 1;
                                    fnodes[2] = sti;
                                    faceCellsPtr->InsertNextCell(3, fnodes);
                                }
                                break;
                            }
                        }
                    }

                    // all polygon (RING) cases
                    else
                    {
                        // Combine all Rings before triangulation to produce correct holes.
                        // default Z for ALL polygon(s).
                        baseZ = shpop->padfZ[0];
                        for(pi = 0; pi < shpop->nParts; pi++)
                        {
                            si = shpop->panPartStart[pi];
                            ei = (pi == (shpop->nParts - 1)) ? shpop->nVertices : shpop->panPartStart[pi + 1];
                            poly.mtabSize[polypart++] = ei - si;
                            for(vi = si; vi < ei; vi++)
                                poly.mtabPnt.insert(poly.mtabPnt.end(), Vect2D(shpop->padfX[vi], shpop->padfY[vi]));
                        }
                    }
                }
                break;
            }
        }

        SHPDestroyObject(shpop);
    }

    if((filetype == MESHOBJ) && (nPolygons > 0))
    {
        // do TRIANGULATION on accumulated poly(s) and build face cells
        pidBase = pid;
        poly.Triangulate();
        for(int pti2 = 0; pti2 < (int)poly.mtabPnt.size(); pti2++)
            myPointsPtr->InsertPoint(pid++, poly.mtabPnt[pti2].X(), poly.mtabPnt[pti2].Y(), baseZ);

        for(int tri2 = 0; tri2 < (int)poly.mtabCell.size(); tri2++)
        {
            for(int ni = 0; ni < 3; ni++)
                fnodes[ni] = pidBase + poly.mtabCell[tri2].Index(ni);
            faceCellsPtr->InsertNextCell(3, fnodes);
        }
    }

    SHPClose(shpih);
    if(nodes != NULL)
    {
        delete nodes;
        nodes = NULL;
    }

    int propcnt = 0;

    // Store the points and cells in the output data object.
    vtkPolyData* output = vtkPolyData::GetData(outputVector);
    output->SetPoints(myPointsPtr);
    switch(filetype)
    {
        case POINTOBJ:
            output->SetVerts(myCellsPtr); break;
        case LINEOBJ:
            output->SetLines(myCellsPtr); break;
        case FACEOBJ:
            // HGRD triangulated processing
            output->SetPolys(faceCellsPtr); break;
        default: // MESHOBJ
            output->SetPolys(faceCellsPtr);
    }

    // cleanup
    for(int pi = 0; pi < propcnt; ++pi)
        ((vtkFloatArray*)fapLAP->GetValue(pi))->Delete();
    fapLAP->Delete();

    return 1;
}
