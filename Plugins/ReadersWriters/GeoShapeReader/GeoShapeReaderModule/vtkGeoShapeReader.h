/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoShapeReader.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkShapeFileReader from VTK\IO\vtkSimplePointsReader & vtkLocalConeSource - Example ParaView Plugin Source
// .SECTION Description
// vtkShapeFileReader is a subclass of vtkPolyDataAlgorithm to read Data Files Extracted from DXF Files.
// Read Data Files (*.shp / *.shx) ESRI data files.
// POINT, LINE, POLYGON, MESH
// Without properties

#ifndef __vtkGeoShapeReader_h
#define __vtkGeoShapeReader_h

#include <vtkMultiBlockDataSetAlgorithm.h>
#include <GeoShapeReaderModuleModule.h>

class GEOSHAPEREADERMODULE_EXPORT vtkGeoShapeReader: public vtkMultiBlockDataSetAlgorithm
{
public:

    static vtkGeoShapeReader* New();
    vtkTypeMacro(vtkGeoShapeReader, vtkMultiBlockDataSetAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetStringMacro(FileName)

protected:

    vtkGeoShapeReader();
    ~vtkGeoShapeReader();

    int RequestData(vtkInformation* request,
                    vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector);

    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    //name of file (full path), useful for obtaining object names
    char const* FileName;

};

#endif
