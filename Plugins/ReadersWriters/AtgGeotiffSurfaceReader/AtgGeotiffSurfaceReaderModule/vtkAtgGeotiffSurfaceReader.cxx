/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgGeotiffSurfaceReader.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <vtkAtgProgressObserver.h>
#include <vtkGDALRasterReader.h>
#include <vtkImageData.h>
#include <vtkCellData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkDecimatePro.h>
#include <vtkSetGet.h>
#include <vtkCallbackCommand.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgGeotiffSurfaceReader.h>

vtkStandardNewMacro(vtkAtgGeotiffSurfaceReader)

static void OnError(vtkObject* vtkNotUsed(caller),
                    unsigned long int vtkNotUsed(eventId),
                    void* clientData,
                    void* callData)
{
    // with this we signal that there was an error in the raster reader
    // note: we do not want to see a message from the raster reader
    bool* errorFlag = static_cast<bool*>(clientData);
    *errorFlag = true;

    // this would be the message that the raster reader wants to issue
    char const* msg = static_cast<char const*>(callData);
}

vtkAtgGeotiffSurfaceReader::vtkAtgGeotiffSurfaceReader()
:   FileName(nullptr),
    Decimate(true),
    TargetReduction(0.99),
    FeatureAngle(15),
    BoundaryVertexDeletion(true),
    FlipVertically(false)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkAtgGeotiffSurfaceReader::~vtkAtgGeotiffSurfaceReader()
{
    // this will free the memory for the strings
    SetFileName(nullptr);
}

void vtkAtgGeotiffSurfaceReader::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);

    os << indent <<  "FileName: "
       << (this->FileName ? this->FileName : "(none)") << "\n";
}

int vtkAtgGeotiffSurfaceReader::RequestData(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    // make sure we have a file to read
    if(!FileName || (0 == ::strlen(FileName)))
    {
        vtkOutputWindowDisplayWarningText("No file name specified\n");
        return 0;
    }

    // added to fix crash that would occur if input file was on the recent file menu
    // but had be deleted from the HDD, therefore no longer existing...
    // MLivingstone
    std::ifstream file;
    file.open(FileName, ios::in);
    if(!file)
    {
        vtkOutputWindowDisplayWarningText("The input file does not exist\n");
        file.close();
        return 1;
    }
    file.close();

    // we want to catch any error condition from the raster reader and handle it here,
    // and for that we need the readerError flag
    vtkNew<vtkCallbackCommand> errObs;
    errObs->SetCallback(&OnError);
    bool readerError = false;
    errObs->SetClientData(&readerError);

    // progress display
    vtkSmartPointer<vtkAtgProgressObserver> progObs = vtkSmartPointer<vtkAtgProgressObserver>::New();
    progObs->SetAlgorithm(this);

    // use the GDAL raster reader to read the raster image
    vtkSmartPointer<vtkGDALRasterReader> rasterReader = vtkSmartPointer<vtkGDALRasterReader>::New();
    rasterReader->SetFileName(FileName);
    SetProgressText("Reading Raster Image");
    rasterReader->AddObserver("ErrorEvent", errObs);
    rasterReader->SetProgressObserver(progObs);
    rasterReader->Update();
    rasterReader->RemoveAllObservers();

    // if we had a reader error, not much we can do
    if(readerError)
    {
        vtkOutputWindowDisplayWarningText("No georeferencing found in the GeoTIFF file\n");
        return 1;
    }

    // extract some georeferencing information
    double org[3] = {0., 0., 0.};
    rasterReader->GetDataOrigin(org);
    double spc[3] = {0., 0., 0.};
    rasterReader->GetDataSpacing(spc);
    int* rasDims = rasterReader->GetRasterDimensions();
    double invalidElev = rasterReader->GetInvalidValue();

    // note that the corner points from GetGeoCornerPoints are not completely correct:
    // the x coordinate of the second point is always the same as x of the first point
    // however the following would work (if required):
    //double corn2[4] =
    //{
    //    org[0],
    //    org[1],
    //    org[0] + spc[0] * rasDims[0],
    //    org[1] + spc[1] * rasDims[1]
    //};

    // now we need the elevations per cell
    // note: it looks like they are always in a cell attribute named "Elevation"
    vtkImageData* img = rasterReader->GetOutput();
    vtkCellData* cellData = img->GetCellData();
    vtkDataArray* elevArr = (nullptr != cellData) ? cellData->GetArray("Elevation") : nullptr;
    if(nullptr == elevArr)
    {
        vtkOutputWindowDisplayWarningText("No elevation info found in GeoTIFF file\n");
        return 1;
    }

    // generate the points
    std::vector<vtkIdType> ptMapper;
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    for(vtkIdType row = 0; row < rasDims[1]; ++row)
    {
        double y = FlipVertically ?
                       org[1] + (rasDims[1] - row - 0.5) * spc[1] :
                       org[1] + (row + 0.5) * spc[1];
        for(vtkIdType col = 0; col < rasDims[0]; ++col)
        {
            vtkIdType elevInx = (rasDims[1] - row - 1) * rasDims[0] + col;
            double x = org[0] + (col + 0.5) * spc[0],
                   z = elevArr->GetVariantValue(elevInx).ToDouble();

            if(z != invalidElev)
                ptMapper.push_back(points->InsertNextPoint(x, y, z));
            else
                ptMapper.push_back(-1); // placeholder for invalid elevations
        }
    }

    // generate a triangulation
    // note: the delaunay filter would take much longer, but this is actually
    //   a rather trivial case (which is possibly the reason why the delaunay
    //   filter takes so long...!??)
    vtkSmartPointer<vtkPolyData> triang = vtkSmartPointer<vtkPolyData>::New();
    triang->SetPoints(points);
    triang->Allocate();

    // we want to indicate the progress also
    SetProgressText("Triangulating the Raster");
    vtkIdType progressStep = rasDims[1] / 100;
    if(progressStep == 0)
        progressStep = 1;

    // proceed row pair by row pair, with a "pair" being {(row-1), row}
    for(vtkIdType row = 1; row < rasDims[1]; ++row)
    {
        if(row % progressStep == 0)
            UpdateProgress((double)row / rasDims[1]);

        // same "pair" loop for columns
        for(vtkIdType col = 1; col < rasDims[0]; ++col)
        {
            // get 4 point indices, in clockwise order
            vtkIdType quadInx[4];
            quadInx[0] = (row - 1) * rasDims[0] + col - 1;
            quadInx[1] = quadInx[0] + 1;
            quadInx[2] = quadInx[1] + rasDims[0];
            quadInx[3] = quadInx[2] - 1;

            // check for invalid points based on the mapper values
            // note: if we have 4 valid points we get two triangles, if one point
            //   is invalid we can still generate one triangle, and with more
            //   invalid points this will be a hole in the triangulation
            int invInx = -1;       // index of one invalid point - if there is one
            bool noTriang = false; // 2 or more invalid points
            for(int i = 0; (i < 4) && !noTriang; ++i)
            {
                if(ptMapper[quadInx[i]] < 0)
                {
                    if(0 < invInx)
                        // this is the first invalid index - remember and continue
                        invInx = i;
                    else
                        // this is the second invalid index - no triangles possible
                        noTriang = true;
                }
            }

            // no triangles possible - no triangles to generate
            if(noTriang)
                continue;

            // triangulate with 0 or 1 invalid points
            switch(invInx)
            {
                case -1:
                {
                    // no invalid point - two triangles
                    // note that there are two possible solutions that are equivalent in the case of
                    //   a square rectangle. Only in order to avoid a "preferred diagonal", we choose
                    //   a pattern of even/odd to always change between the two possible solutions
                    int odd = (row + col) % 2;
                    vtkIdType tri1[3] = {ptMapper[quadInx[odd]], ptMapper[quadInx[odd + 1]], ptMapper[quadInx[odd + 2]]};
                    vtkIdType tri2[3] = {ptMapper[quadInx[odd + 2]], ptMapper[quadInx[(odd + 3) % 4]], ptMapper[quadInx[odd]]};
                    triang->InsertNextCell(VTK_TRIANGLE, 3, tri1);
                    triang->InsertNextCell(VTK_TRIANGLE, 3, tri2);
                    break;
                }

                default:
                {
                    // one of the indices 0 to 3 is invalid
                    vtkIdType tri[3] = {ptMapper[quadInx[(invInx + 1) % 4]],
                                        ptMapper[quadInx[(invInx + 2) % 4]],
                                        ptMapper[quadInx[(invInx + 3) % 4]]};
                    triang->InsertNextCell(VTK_TRIANGLE, 3, tri);
                }
            }
        }
    }

    // if needed, apply the decimator, to simplify the triangle
    vtkSmartPointer<vtkDecimatePro> decim = vtkSmartPointer<vtkDecimatePro>::New();
    if(Decimate)
    {
        decim->SetInputData(triang);
        decim->SetTargetReduction(TargetReduction);
        decim->PreserveTopologyOn();
        decim->SetFeatureAngle(FeatureAngle);
        decim->SetBoundaryVertexDeletion(BoundaryVertexDeletion);
        SetProgressText("Decimate Triangulated Surface");
        decim->SetProgressObserver(progObs);
        decim->Update();
    }

    // get the output poly data
    vtkPolyData* output = vtkPolyData::GetData(outputVector);

    // return either the raw or the decimated triangulation
    if(Decimate)
        output->DeepCopy(decim->GetOutput());
    else
        output->DeepCopy(triang);

    // special case: give a warning if we have no triangles now
    if(0 == output->GetNumberOfCells())
    {
        std::string warn = "The output has no triangles and only " +
                           std::to_string(output->GetNumberOfPoints()) +
                           " valid point(s)\n";
        vtkOutputWindowDisplayWarningText(warn.c_str());
    }

    return 1;
}
