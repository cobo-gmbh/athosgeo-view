/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgDXFWriter.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <set>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkErrorCode.h>
#include <vtkAtgDXFWriter.h>

vtkStandardNewMacro(vtkAtgDXFWriter)

vtkAtgDXFWriter::vtkAtgDXFWriter()
:   FileName(nullptr)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(0);
}

vtkAtgDXFWriter::~vtkAtgDXFWriter()
{
    SetFileName(nullptr);
}

void vtkAtgDXFWriter::Write()
{
    // make sure the latest input is available.
    GetInputAlgorithm()->Update();
    SetErrorCode(vtkErrorCode::NoError);

    // get the input data
    vtkPolyData* input = vtkPolyData::SafeDownCast(GetInput());

    // error checking
    if(nullptr == input)
    {
        vtkErrorMacro(<< "Write: no polydata input found");
        return;
    }
    if(nullptr == FileName)
    {
        vtkErrorMacro(<< "Write: no DXF filename found");
        SetErrorCode(vtkErrorCode::NoFileNameError);
        return;
    }
    if((0 == input->GetNumberOfPoints()) ||
       (0 == input->GetNumberOfCells()))
    {
        vtkErrorMacro(<< "Write: no points and/or cells in input data");
        return;
    }

    // create the new DXF file
    std::ofstream ostr;
    ostr.open(FileName, std::fstream::out | std::fstream::trunc);
    if(!ostr.is_open())
    {
        vtkErrorMacro(<< "Write: could not open DXF file for writing");
        SetErrorCode(vtkErrorCode::CannotOpenFileError);
        return;
    }

    // write the DXF file header
    ostr << "0" << std::endl
         << "SECTION" << std::endl
         << "2" << std::endl
         << "ENTITIES" << std::endl;

    // get the points
    vtkPoints* ptCoord = input->GetPoints();

    // iterate through the cells
    std::set<int> unknownTypes;
    std::string layer("1");
    long color = 7;
    for(vtkIdType c = 0; c < input->GetNumberOfCells(); ++c)
    {
        vtkCell* cell = input->GetCell(c);
        vtkIdList* cellPts = cell->GetPointIds();

        switch(cell->GetCellType())
        {
            case VTK_VERTEX:
            {
                if(1 > cellPts->GetNumberOfIds())
                    continue;
                writeItem(ostr, "POINT", cellPts, ptCoord, layer, color);
                break;
            }

            case VTK_POLY_VERTEX:
            {
                if(1 > cellPts->GetNumberOfIds())
                    continue;
                auto oneId = vtkSmartPointer<vtkIdList>::New();
                oneId->SetNumberOfIds(1);
                for(vtkIdType p = 0; p < cellPts->GetNumberOfIds(); ++p)
                {
                    oneId->SetId(0, cellPts->GetId(p));
                    writeItem(ostr, "POINT", oneId, ptCoord, layer, color);
                }
                break;
            }

            case VTK_LINE:
            {
                if(2 > cellPts->GetNumberOfIds())
                    continue;
                writeItem(ostr, "LINE", cellPts, ptCoord, layer, color);
                break;
            }

            case VTK_POLY_LINE:
            {
                if(1 > cellPts->GetNumberOfIds())
                    continue;
                // head of the polyline point sequence
                // code 70: Polyline flag (bit-coded; default = 0)
                // - 1 = This is a closed polyline (or a polygon mesh closed in the M direction)
                // - 2 = Curve-fit vertices have been added
                // - 4 = Spline-fit vertices have been added
                // - 8 = This is a 3D polyline
                // - 16 = This is a 3D polygon mesh
                // - 32 = The polygon mesh is closed in the N direction
                // - 64 = The polyline is a polyface mesh
                // - 128 = The linetype pattern is generated continuously around the vertices of this polyline
                ostr << "0" << std::endl
                     << "POLYLINE" << std::endl
                     << "70" << std::endl
                     << "8" << std::endl;

                // points
                auto oneId = vtkSmartPointer<vtkIdList>::New();
                oneId->SetNumberOfIds(1);
                for(vtkIdType p = 0; p < cellPts->GetNumberOfIds(); ++p)
                {
                    oneId->SetId(0, cellPts->GetId(p));
                    writeItem(ostr, "VERTEX", oneId, ptCoord, layer);
                }

                // end of the polyline point sequence
                ostr << "0" << std::endl
                     << "SEQEND" << std::endl;
                break;
            }

            case VTK_TRIANGLE:
            {
                if(3 > cellPts->GetNumberOfIds())
                    continue;
                auto ids = vtkSmartPointer<vtkIdList>::New();
                ids->DeepCopy(cellPts);
                ids->InsertNextId(ids->GetId(0)); // close the triangle
                writeItem(ostr, "3DFACE", ids, ptCoord, layer, color);
                break;
            }

            case VTK_TRIANGLE_STRIP:
            {
                if(3 > cellPts->GetNumberOfIds())
                    continue;

                // triangles - one by one
                auto triId = vtkSmartPointer<vtkIdList>::New();
                triId->SetNumberOfIds(4);
                for(vtkIdType p = 0; p < (cellPts->GetNumberOfIds() - 2); ++p)
                {
                    for(vtkIdType tp = 0; tp < 3; ++tp)
                        triId->SetId(tp, cellPts->GetId(p + tp));
                    triId->SetId(3, cellPts->GetId(p)); // close the triangle
                    writeItem(ostr, "3DFACE", triId, ptCoord, layer, color);
                }

                break;
            }

            case VTK_QUAD:
            case VTK_PIXEL:
            {
                if(4 > cellPts->GetNumberOfIds())
                    continue;

                // same as triangle, but the fourth point is separate
                writeItem(ostr, "3DFACE", cellPts, ptCoord, layer, color);

                break;
            }

            default:
            {
                unknownTypes.insert(cell->GetCellType());
            }
        }
    }

    // footer of the file
    ostr << "0" << std::endl
         << "ENDSEC" << std::endl
         << "0" << std::endl
         << "EOF" << std::endl;

    // done
    ostr.close();

    // see if we have unknown types
    if(unknownTypes.end() != unknownTypes.find(VTK_POLYGON))
    {
        vtkOutputWindowDisplayWarningText(
                    "Polygons are not supported: use the Triangulate filter first");
        unknownTypes.erase(unknownTypes.find(VTK_POLYGON));
    }
    if(!unknownTypes.empty())
    {
        std::ostringstream osstr;
        osstr << "Unsupported cell types found:";
        for(auto it = unknownTypes.begin(); it != unknownTypes.end(); ++it)
            osstr << " " << *it;
        osstr << "\n";
        vtkOutputWindowDisplayText(osstr.str().c_str());
    }
}

void vtkAtgDXFWriter::writeItem(std::ostream& ostr, std::string const& type,
                                vtkIdList* ptIds, vtkPoints* pts,
                                std::string const& layer, long color)
{
    // write header of the item
    ostr << "0" << std::endl
         << type << std::endl
         << "8" << std::endl
         << layer << std::endl;

    if(0 <= color)
        ostr << "62" << std::endl
             << color << std::endl;

    // write points
    for(vtkIdType pt = 0; pt < ptIds->GetNumberOfIds(); ++pt)
    {
        // note that we need to close the triangle by appending the first point again
        double* pc = pts->GetPoint(ptIds->GetId(pt));
        for(int c = 0; c < 3; ++c)
        {
            ostr << (c + 1) << pt << std::endl
                 << std::fixed << std::setprecision(3) << pc[c] << std::endl;
        }
    }
}

void vtkAtgDXFWriter::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
