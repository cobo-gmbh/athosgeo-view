/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFReader.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkGeoDXFReader
// By: Robert Maynard && Matthew Livingstone && Eric Daoust
// .SECTION Description
// vtkGeoDXFReader is a subclass of vtkMultiBlockDataSetAlgorithm to read DXF Files
// used a wrapper to get rid of weird memory leaks

#ifndef __vtkGeoDXFReader_h
#define __vtkGeoDXFReader_h

#include <vtkPolyDataAlgorithm.h>
#include <GeoDXFReaderModuleModule.h>

class vtkPolyData;
class vtkMultiBlockDataSet;

class GEODXFREADERMODULE_EXPORT vtkGeoDXFReader: public vtkPolyDataAlgorithm
{
public:

    static vtkGeoDXFReader* New();
    vtkTypeMacro(vtkGeoDXFReader, vtkPolyDataAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetStringMacro(FileName)

    // option to draw points
    vtkSetMacro(DrawPoints, bool)
    vtkGetMacro(DrawPoints, bool)

    // option to drawn frozen/invisible layers
    vtkSetMacro(DrawHidden, bool)
    vtkGetMacro(DrawHidden, bool)

    // option to scale all entities
    vtkSetMacro(AutoScale, bool)
    vtkGetMacro(AutoScale, bool)

protected:

    vtkGeoDXFReader();
    ~vtkGeoDXFReader();

    virtual int RequestData(vtkInformation* request,
                            vtkInformationVector** inputVector,
                            vtkInformationVector* outputVector);

    int layerExists;
    int type;

private:

    //name of file (full path), useful for obtaining object names
    char const* FileName;

    // enable/disable point drawing
    bool DrawPoints;

    // enable/disable drawing of frozen/invisible layers
    bool DrawHidden;

    // enable/disable scaling of entities
    bool AutoScale;

    // count available polydata objects and return the first
    unsigned long countAndFindFirstPolyData(vtkMultiBlockDataSet* mbl,
                                            vtkPolyData*& pdata);

};

#endif
