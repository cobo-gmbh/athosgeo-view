/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFReader.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkGeoDXFReader.cxx
// By: Robert Maynard && Matthew Livingstone && Eric Daoust
// made to remove memory leaks in the original class

#include <iostream>
#include <iomanip>
#include <string>

#include <vtkGeoDXFReader.h>
#include <vtkGeoDXFObject.h>
#include <vtkObjectFactory.h>
#include <vtkDataObject.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkPolyData.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <vtkImplicitPolyDataDistance.h>
#include <vtkPolyDataNormals.h>
#include <vtkCellData.h>

#include <vtkDataObject.h>
#include <vtkType.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkCell.h>

vtkStandardNewMacro(vtkGeoDXFReader)

// for debugging only
void showBlockTree(vtkMultiBlockDataSet* mbds, int indent);

vtkGeoDXFReader::vtkGeoDXFReader()
:   FileName(nullptr),
    DrawPoints(false),
    DrawHidden(false),
    AutoScale(false)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkGeoDXFReader::~vtkGeoDXFReader()
{
    SetFileName(nullptr);
}

void vtkGeoDXFReader::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os,indent);
    os << indent <<  "FileName: "
       << (FileName ? FileName : "(none)") << "\n";
}

int vtkGeoDXFReader::RequestData(vtkInformation* request,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
    // Make sure we have a file to read.
    if(!FileName || (::strlen(FileName) == 0))
    {
        vtkOutputWindowDisplayWarningText("GeoDXFReader: No filename found\n");
        return 1;
    }

    // Added to fix crash that would occur if a DXF was on the recent file menu
    // but had be deleted from the HDD, therefore no longer existing...
    // MLivingstone
    std::ifstream file;
    file.open(FileName, std::ios::in);
    if(!file)
    {
        vtkOutputWindowDisplayWarningText("GeoDXFReader: DXF file does not exist\n");
        file.close();
        return 1;
    }
    file.close();

    // this fixes the problem with vtkGeoDXFObject holding references to way to many instances
    // of classes, causing it to uses roughly 60mb of memory it does not need
    // by doing a deep copy of the data, we can make sure we release
    // all the instances that dxf created but could not delete
    vtkSmartPointer<vtkGeoDXFObject> dxfObject = vtkSmartPointer<vtkGeoDXFObject>::New();
    dxfObject->SetFileName(FileName);
    dxfObject->SetDrawPoints(GetDrawPoints());
    dxfObject->SetDrawHidden(GetDrawHidden());
    dxfObject->SetAutoScale(GetAutoScale());
    dxfObject->Update();

    // Check if there is any data block at all
    vtkMultiBlockDataSet* dxfGeom = dxfObject->GetOutput();
    if(1 > dxfGeom->GetNumberOfBlocks())
    {
        if(dxfObject->GetIsBinary())
            vtkOutputWindowDisplayWarningText("GeoDXFReader: This is a binary DXF file - unable to read\n");
        else
            vtkOutputWindowDisplayWarningText("GeoDXFReader: Nothing found in DXF file\n");
        return 1;
    }


    // generate a warning if the file contains unsupported SOLID entities
    if(0 < dxfObject->GetNumUnsupportedSolid())
    {
        std::string warn = std::string("GeoDXFReader warning: found ") +
                           std::to_string(dxfObject->GetNumUnsupportedSolid()) +
                           " unsupported SOLID entities in the file\n";
        vtkOutputWindowDisplayWarningText(warn.c_str());
    }

    // next we want to see if we can find a polydata object - and warn if not
    // note: it is important to initialize pdata to nullptr here!
    vtkPolyData* pdata = nullptr;
    unsigned long numpd = countAndFindFirstPolyData(dxfGeom, pdata);
    if(nullptr == pdata)
    {
        vtkOutputWindowDisplayWarningText("GeoDXFReader: No polydata (geometry) object found in DXF file\n");
        return 1;
    }

    // give a message if more polydata would be in the DXF
    if(1 < numpd)
    {
        std::string msg = "GeoDXFReader: Showing first polydata (geometry) object, but "
                        + std::to_string(numpd)
                        + " objects would be available: use the GeoDXFReaderMB "
                          "to get them all";
        vtkOutputWindowDisplayWarningText(msg.c_str());
    }

    // otherwise we get the output and copy our polydata in there
    vtkPolyData* output = vtkPolyData::GetData(outputVector);
    output->DeepCopy(pdata);

    return 1;
}

unsigned long vtkGeoDXFReader::countAndFindFirstPolyData(vtkMultiBlockDataSet* mbl,
                                                         vtkPolyData*& pdata)
{
    // note: it is important that pdata is nullptr on the initial call,
    // but here we cannot assure it because of the recursion!

    // emergency
    if(nullptr == mbl)
        return 0;

    // count polydata objects recursively, and remember the first
    unsigned long count = 0;
    for(unsigned int bl = 0; bl < mbl->GetNumberOfBlocks(); ++bl)
    {
        // get the block and check it more closely
        vtkDataObject* block = mbl->GetBlock(bl);
        switch(block->GetDataObjectType())
        {
            case VTK_POLY_DATA:
            {
                // this looks like something we want to count
                count++;
                if(nullptr == pdata)
                    pdata = vtkPolyData::SafeDownCast(block);
            }

            case VTK_MULTIBLOCK_DATA_SET:
            {
                // if we find another multi block dataset, we process
                // it recursively
                count += countAndFindFirstPolyData(vtkMultiBlockDataSet::SafeDownCast(block),
                                                   pdata);
            }
        }
    }

    return count;
}

// for debug only
void showBlockTree(vtkMultiBlockDataSet* mbds, int indent)
{
    for(vtkIdType bl = 0; bl < mbds->GetNumberOfBlocks(); ++bl)
    {
        vtkDataObject* block = mbds->GetBlock(bl);
        int btype = block->GetDataObjectType();
        static const char* BlockType[] =
        {
            "VTK_POLY_DATA",
            "VTK_STRUCTURED_POINTS",
            "VTK_STRUCTURED_GRID",
            "VTK_RECTILINEAR_GRID",
            "VTK_UNSTRUCTURED_GRID",
            "VTK_PIECEWISE_FUNCTION",
            "VTK_IMAGE_DATA",
            "VTK_DATA_OBJECT",
            "VTK_DATA_SET",
            "VTK_POINT_SET",
            "VTK_UNIFORM_GRID",
            "VTK_COMPOSITE_DATA_SET",
            "VTK_MULTIGROUP_DATA_SET",
            "VTK_MULTIBLOCK_DATA_SET",
            "VTK_HIERARCHICAL_DATA_SET",
            "VTK_HIERARCHICAL_BOX_DATA_SET",
            "VTK_GENERIC_DATA_SET",
            "VTK_HYPER_OCTREE",
            "VTK_TEMPORAL_DATA_SET",
            "VTK_TABLE",
            "VTK_GRAPH",
            "VTK_TREE",
            "VTK_SELECTION",
            "VTK_DIRECTED_GRAPH",
            "VTK_UNDIRECTED_GRAPH",
            "VTK_MULTIPIECE_DATA_SET",
            "VTK_DIRECTED_ACYCLIC_GRAPH",
            "VTK_ARRAY_DATA",
            "VTK_REEB_GRAPH",
            "VTK_UNIFORM_GRID_AMR",
            "VTK_NON_OVERLAPPING_AMR",
            "VTK_OVERLAPPING_AMR",
            "VTK_HYPER_TREE_GRID",
            "VTK_MOLECULE",
            "VTK_PISTON_DATA_OBJECT",
            "VTK_PATH",
            "VTK_UNSTRUCTURED_GRID_BASE"
        };

        static const char* elemTypeName[] =
        {
            "POINT",
            "CELL",
            "FIELD",
            "POINT_THEN_CELL",
            "VERTEX",
            "EDGE",
            "ROW"
        };

        std::string indStr(indent, ' ');
        std::cout << indStr
                  << "block "
                  << bl
                  << " type "
                  << btype
                  << " "
                  << BlockType[btype]
                  << std::endl;

        if(VTK_MULTIBLOCK_DATA_SET == btype)
        {
            vtkMultiBlockDataSet* mBlock = vtkMultiBlockDataSet::SafeDownCast(block);
            showBlockTree(mBlock, indent + 2);
        }

        else if(VTK_POLY_DATA == btype)
        {
            indStr = std::string(indent + 2, ' ');
            vtkPolyData* pBlock = vtkPolyData::SafeDownCast(block);
            vtkPoints* points = pBlock->GetPoints();
            std::cout << indStr
                      << "points "
                      << points->GetNumberOfPoints()
                      << std::endl;
            vtkCellArray* vertCells = pBlock->GetVerts();
            vtkIdType vertNum = vertCells->GetNumberOfCells();
            std::cout << indStr
                      << "cells "
                      << vertNum
                      << std::endl;
            vtkCellArray* lineCells = pBlock->GetLines();
            vtkIdType lineNum = lineCells->GetNumberOfCells();
            std::cout << indStr
                      << "lines "
                      << lineNum
                      << std::endl;
            vtkCellArray* polyCells = pBlock->GetPolys();
            vtkIdType polyNum = polyCells->GetNumberOfCells();
            std::cout << indStr
                      << "polys "
                      << polyNum
                      << std::endl;
            vtkCellArray* strpCells = pBlock->GetStrips();
            vtkIdType strpNum = strpCells->GetNumberOfCells();
            std::cout << indStr
                      << "strps "
                      << strpNum
                      << std::endl;
            std::cout << indStr
                      << "piecs "
                      << pBlock->GetNumberOfPieces()
                      << std::endl;
            std::cout << indStr
                      << "verts "
                      << pBlock->GetNumberOfVerts()
                      << std::endl;
            for(int et = 0; et < vtkDataObject::NUMBER_OF_ATTRIBUTE_TYPES; ++et)
                std::cout << indStr
                          << "elems "
                          << elemTypeName[et]
                          << " "
                          << pBlock->GetNumberOfElements(et)
                          << std::endl;

            // if this is a polygon grid, do further investigations
            // DEBUG -----------
            /*/ add or remove * between the // in order to comment / un-comment debug output
            if(0 < polyCells->GetNumberOfCells())
            {
                // check what are the triangle normals for the surface
                vtkPolyDataNormals* polyNorm = vtkPolyDataNormals::New();
                polyNorm->SetInputData(pBlock);
                polyNorm->SplittingOff();
                polyNorm->ConsistencyOn();
                polyNorm->ComputePointNormalsOff();
                polyNorm->ComputeCellNormalsOn();
                polyNorm->FlipNormalsOn();
                polyNorm->Update();
                vtkPolyData* pBlockN = vtkPolyData::New();
                pBlockN->ShallowCopy(polyNorm->GetOutput());
                vtkDataArray* narr = pBlockN->GetCellData()->GetArray("Normals");
                if(0 == narr)
                {
                    std::cout << "normal array could not be generated" << std::endl;
                    return;
                }
                std::cout << "tuples " << narr->GetNumberOfTuples() << std::endl;
                std::cout << "components " << narr->GetNumberOfComponents() << std::endl;
                std::cout << "values " << narr->GetNumberOfValues() << std::endl;
                for(vtkIdType k = 0; k < narr->GetNumberOfTuples(); ++k)
                {
                    std::cout << k << " normal z: " << narr->GetComponent(k, 2) << std::endl;
                }

                // check the sign of the "implicit poly data distance" for points above
                // and below the triangulated surface

                // wrap the input poly data into an "implicit function" object
                vtkImplicitPolyDataDistance* polyDist = vtkImplicitPolyDataDistance::New();
                polyDist->SetInput(pBlockN);

                // check the "distance" for points above and below the polyData item
                vtkPoints* pdpts = pBlockN->GetPoints();
                int posAbove = 0,
                    negAbove = 0,
                    posBelow = 0,
                    negBelow = 0;
                static const double testDist = 1000.;
                for(vtkIdType n = 0; n < pBlockN->GetNumberOfPoints(); ++n)
                {
                    double pt[3];
                    pdpts->GetPoint(n, pt);
                    pt[2] = pt[2] + testDist;
                    double distAbove = polyDist->EvaluateFunction(pt);
                    if(0. < distAbove)
                    {
                        ++posAbove;
                    }
                    else
                    {
                        ++negAbove;
                        std::cout << std::setprecision(3)
                                  << "neg above: "
                                  << pt[0]
                                  << " "
                                  << pt[1]
                                  << " "
                                  << pt[2] - testDist
                                  << std::endl;
                    }
                    pt[2] = pt[2] - 2. * testDist;
                    double distBelow = polyDist->EvaluateFunction(pt);
                    if(0. > distBelow)
                    {
                        ++negBelow;
                    }
                    else
                    {
                        ++posBelow;
                        std::cout << std::setprecision(3)
                                  << "pos below: "
                                  << pt[0]
                                  << " "
                                  << pt[1]
                                  << " "
                                  << pt[2] + testDist
                                  << std::endl;
                    }
                }
                std::cout << "above: pos "
                          << posAbove
                          << " / neg "
                          << negAbove
                          << ", below: pos "
                          << posBelow
                          << " / neg "
                          << negBelow
                          << std::endl;

                polyDist->Delete();
                pBlockN->Delete();
                polyNorm->Delete();
            }
            //*/
            // DEBUG ------------
        }
    }
}
