/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgBlockModelReader.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <vector>
#include <boost/algorithm/string/join.hpp>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDelimitedTextReader.h>
#include <vtkTable.h>
#include <vtkAbstractArray.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkCellData.h>
#include <utilNormNames.h>
#include <utilTrackDefs.h>
#include <vtkAtgNormalizeNames.h>
#include <vtkAtgProgressObserver.h>
#include <vtkAtgTableToBlocks.h>
#include <vtkAtgAddNeighborIndices.h>
#include <vtkAtgCsvReader.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgBlockModelReader.h>

vtkStandardNewMacro(vtkAtgBlockModelReader)

vtkAtgBlockModelReader::vtkAtgBlockModelReader()
:   FileName(nullptr),
    FieldDelimiterCharacters(nullptr),
    AddTabFieldDelimiter(false),
    RotAngle(0.),
    EmptyCellString(nullptr),
    EmptyCellValue(-1.),
    NeighborAttribute(false)
{
    for(int i = 0; i < 3; ++i)
        BlockSize[i] = 0.;

    SetEmptyCellString("--");

    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkAtgBlockModelReader::~vtkAtgBlockModelReader()
{
    // this will free the memory for the strings
    SetEmptyCellString(nullptr);
    SetFieldDelimiterCharacters(nullptr);
    SetFileName(nullptr);
}

void vtkAtgBlockModelReader::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);

    os << indent <<  "FileName: "
       << (this->FileName ? this->FileName : "(none)") << "\n";
}

int vtkAtgBlockModelReader::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    // make sure we have a file to read
    if(!FileName || (0 == ::strlen(FileName)))
    {
        vtkOutputWindowDisplayWarningText("No file name specified\n");
        return 0;
    }

    // added to fix crash that would occur if input file was on the recent file menu
    // but had be deleted from the HDD, therefore no longer existing...
    // MLivingstone
    std::ifstream file;
    file.open(FileName, ios::in);
    if(!file)
    {
        vtkOutputWindowDisplayWarningText("The input file does not exist\n");
        file.close();
        return 1;
    }
    file.close();

    // get the info about the output port
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* output =
            vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // just show the user that something is going on...
    SetProgressText("reading the input table");
    UpdateProgress(0.05);

    // read the file into a vtkTable, using the user specified properties
    auto reader = vtkSmartPointer<vtkAtgCsvReader>::New();
    reader->SetFileName(FileName);
    reader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
    reader->SetEmptyStringReplacement(EmptyCellString);
    reader->SetAddTabFieldDelimiter(AddTabFieldDelimiter);
    reader->SetEmptyNumberReplacement(EmptyCellValue);
    reader->SetTreatExcelErrorsAsEmpty(true);
    reader->SetRemoveLastLineIfLessCellsThanHeader(true);
    reader->Update();

    // fatal reading errors
    if(nullptr != reader->GetErrorMessage())
    {
        std::string msg = std::string("Fatal error during file reading:\n")
                        + reader->GetErrorMessage();
        return 1;
    }

    // if we have no rows or columns, this is fatal
    if(0 == reader->GetOutput()->GetNumberOfColumns())
    {
        std::string msg = "Input file has no columns\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }
    if(0 == reader->GetOutput()->GetNumberOfRows())
    {
        std::string msg = "Input file has no rows\n";
        vtkOutputWindowDisplayWarningText(msg.c_str());
        return 1;
    }

    // we treat reading errors of single values like "empty cells" and report them as warning
    if(nullptr != reader->GetWarningMessages())
    {
        std::string msg = std::string("Warning:\n") + reader->GetWarningMessages();
        vtkOutputWindowDisplayWarningText(msg.c_str());
    }

    /*
    vtkSmartPointer<vtkDelimitedTextReader> reader = vtkSmartPointer<vtkDelimitedTextReader>::New();
    reader->SetFileName(FileName);
    reader->SetHaveHeaders(true);
    reader->DetectNumericColumnsOn();
    reader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
    reader->SetAddTabFieldDelimiter(AddTabFieldDelimiter);
    reader->SetMergeConsecutiveDelimiters(false);
    reader->SetUseStringDelimiter(true);
    reader->SetStringDelimiter('\"');
    reader->TrimWhitespacePriorToNumericConversionOn();
    reader->ForceDoubleOn();
    reader->Update();

    // if there was an error message, tell the user
    if(!reader->GetLastError().empty())
    {
        vtkOutputWindowDisplayWarningText(("The CSV table reader found an error:\n" +
                                           reader->GetLastError()).c_str());
        return 1;
    }
    */

    // done reading the input table
    SetProgressText("generating raw blocks");

    // normalize the column names, including upper/lower case
    vtkSmartPointer<vtkAtgNormalizeNames> colNorm = vtkSmartPointer<vtkAtgNormalizeNames>::New();
    colNorm->SetIncludeComponent(true);
    colNorm->SetInputData(reader->GetOutput());
    colNorm->Update();

    // report error during column name normalization
    if(nullptr != colNorm->GetError())
    {
        std::string err = std::string("Error during column name normalization:\n") + colNorm->GetError();
        vtkOutputWindowDisplayWarningText(err.c_str());
        return 1;
    }

    // check for mandatory columns - which are Center:X/Y/Z
    std::vector<std::string> mand;
    mand.push_back(utilNormNames::getName(utilNormNames::BLOCKCENTER));
    std::vector<std::string> notFound;
    for(auto col = mand.begin(); col != mand.end(); ++col)
    {
        if(0 == colNorm->GetOutput()->GetColumnByName(col->c_str()))
            notFound.push_back(*col);
    }
    if(!notFound.empty())
    {
        vtkOutputWindowDisplayWarningText(std::string("Mandatory input column(s) not found:\n" +
                                                      boost::algorithm::join(notFound, ", ") +
                                                      "\n(or some accepted equivalent(s))\n").c_str());
        return 1;
    }

    // generate size and rotation angle colums if not present in the input
    addValueIfNotPresent(colNorm->GetOutput(), utilNormNames::BLOCKSIZE, 3, BlockSize);
    addValueIfNotPresent(colNorm->GetOutput(), utilNormNames::ANGLE, 1, &RotAngle);

    // add block ids if not present in the input
    addBlockIdIfNotPresent(colNorm->GetOutput());

    // convert into block model
    // note: we pass the block size and angle parameters to the conversion
    // filter, so they can be used if values do not exist in the input table
    vtkSmartPointer<vtkAtgTableToBlocks> generateBlockModel =
            vtkSmartPointer<vtkAtgTableToBlocks>::New();
    vtkSmartPointer<vtkAtgProgressObserver> progressObs =
            vtkSmartPointer<vtkAtgProgressObserver>::New();
    progressObs->SetAlgorithm(this);
    progressObs->SetRange(0.05, (NeighborAttribute ? 0.5 : 1.));
    SetProgressText("cleaning the unstructured grid");
    generateBlockModel->SetInputData(colNorm->GetOutput());
    generateBlockModel->SetBlockSize(GetBlockSize());
    generateBlockModel->SetRotAngle(GetRotAngle());
    generateBlockModel->SetProgressObserver(progressObs);
    generateBlockModel->Update();

    // add neighborhood columns to the block model if the option is set
    if(NeighborAttribute)
    {
        // note that this re-generates the neighbor attribute even if it already exists
        vtkSmartPointer<vtkAtgAddNeighborIndices> addNeighborPointers =
                vtkSmartPointer<vtkAtgAddNeighborIndices>::New();
        progressObs->SetRange(0.5, 1.0);
        SetProgressText("finding neighbor relations");
        addNeighborPointers->SetInputData(generateBlockModel->GetOutput());
        addNeighborPointers->SetProgressObserver(progressObs);
        addNeighborPointers->Update();
        output->ShallowCopy(addNeighborPointers->GetOutput());
    }

    // otherwise just copy the model (and convert nb attribute to Ids if it's not)
    else
    {
        // copy directly the generated model
        output->ShallowCopy(generateBlockModel->GetOutput());

        // replace the nb array with one of type vtkIdType
        std::string nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
        convertDoubleArrayToIdType(output, nbName.c_str());
    }

    // convert also blockId and pref arrays (if available)
    convertDoubleArrayToIdType(output, utilNormNames::getName(utilNormNames::BLOCKID));
    convertDoubleArrayToIdType(output, utilNormNames::getName(utilNormNames::PREFERENCE));

    // prepare name (string) attributes
    prepareNameAttributes(output);

    return 1;
}

void vtkAtgBlockModelReader::convertDoubleArrayToIdType(vtkUnstructuredGrid* grid,
                                                        std::string const& arrName)
{
    vtkDataArray* arr0 = grid->GetCellData()->GetArray(arrName.c_str());
    if(0 == arr0)
        return;

    vtkSmartPointer<vtkLongLongArray> arr = vtkSmartPointer<vtkLongLongArray>::New();
    arr->SetName(arrName.c_str());
    arr->SetNumberOfComponents(arr0->GetNumberOfComponents());
    arr->SetNumberOfTuples(arr0->GetNumberOfTuples());
    arr->DeepCopy(arr0);

    // only now I learned that DeepCopy() takes care of the data type conversion
    // already, so we do not need this kind of explicit copying...
    //for(vtkIdType c = 0; c < arr->GetNumberOfComponents(); ++c)
    //{
    //    arr->SetComponentName(c, arr0->GetComponentName(c));
    //    for(vtkIdType r = 0; r < arr->GetNumberOfTuples(); ++r)
    //        arr->SetComponent(r, c, vtkIdType(arr0->GetComponent(r, c)));
    //}

    grid->GetCellData()->AddArray(arr);
}

void vtkAtgBlockModelReader::prepareNameAttributes(vtkUnstructuredGrid* grid)
{
    for(vtkIdType col = 0; col < grid->GetCellData()->GetNumberOfArrays(); ++col)
    {
        vtkAbstractArray* arr = grid->GetCellData()->GetAbstractArray(col);
        if(VTK_STRING != arr->GetDataType())
           continue;

        // if a string array is not in the type "category" we prepend a N_ (to bring it there)
        switch(utilNormNames::getType(arr->GetName()))
        {
            case utilNormNames::TY_NAME:
            case utilNormNames::TY_DATE:
            case utilNormNames::TY_CATEGORY:
            {
                // these are already ok
                break;
            }
            default:
            {
                arr->SetName((std::string("N_") + arr->GetName()).c_str());
            }
        }
    }
}

void vtkAtgBlockModelReader::addValueIfNotPresent(vtkTable* table,
                                                  int nameId, int numComps, double* values)
{
    // note: if the column exists we ignore it (and do not overwrite with value)
    if((0 == table) || (0 > nameId))
        return;

    // get the column (base) name
    std::string name = utilNormNames::getName(nameId);

    // try to find a column of numeric type - and keep if present
    if(0 != vtkDataArray::SafeDownCast(table->GetColumnByName(name.c_str())))
        return;

    // generate column
    vtkSmartPointer<vtkDoubleArray> arr = vtkSmartPointer<vtkDoubleArray>::New();
    arr->SetName(name.c_str());
    arr->SetNumberOfComponents(numComps);
    arr->SetNumberOfTuples(table->GetNumberOfRows());

    // name the components - if they exist
    if(1 < numComps)
    {
        std::vector<std::string> compNames = utilNormNames::getComponents(name);
        vtkIdType cid = 0;
        for(auto it = compNames.begin(); it != compNames.end(); ++it)
            arr->SetComponentName(cid++, it->c_str());
    }

    // fill with value(s)
    for(vtkIdType cid = 0; cid < numComps; ++cid)
    {
        for(vtkIdType n = 0; n < arr->GetNumberOfTuples(); ++n)
            arr->SetComponent(n, cid, values[cid]);
    }

    table->AddColumn(arr);
}

void vtkAtgBlockModelReader::addBlockIdIfNotPresent(vtkTable* table)
{
    // note: if the column exists we ignore it (and do not overwrite with value)
    if(0 == table)
        return;

    // get the column (base) name
    std::string name = utilNormNames::getName(utilNormNames::BLOCKID);

    // try to find a column of numeric type - and keep if present
    if(0 != vtkDataArray::SafeDownCast(table->GetColumnByName(name.c_str())))
        return;

    // generate column
    vtkSmartPointer<vtkDoubleArray> arr = vtkSmartPointer<vtkDoubleArray>::New();
    arr->SetName(name.c_str());
    arr->SetNumberOfTuples(table->GetNumberOfRows());

    // fill with value(s)
    for(vtkIdType blockId = 0; blockId < arr->GetNumberOfTuples(); ++blockId)
        arr->SetValue(blockId, blockId);

    table->AddColumn(arr);
}
