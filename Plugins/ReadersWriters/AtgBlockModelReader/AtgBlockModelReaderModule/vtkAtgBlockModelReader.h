/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgBlockModelReader.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __vtkAtgBlockModelReader_h
#define __vtkAtgBlockModelReader_h

#include <string>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgBlockModelReaderModuleModule.h>

class vtkTable;
class vtkAbstractArray;

class ATGBLOCKMODELREADERMODULE_EXPORT vtkAtgBlockModelReader: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgBlockModelReader* New();
    vtkTypeMacro(vtkAtgBlockModelReader, vtkUnstructuredGridAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetStringMacro(FileName)
    vtkGetStringMacro(FileName)

    vtkSetStringMacro(FieldDelimiterCharacters)
    vtkGetStringMacro(FieldDelimiterCharacters)

    vtkSetMacro(AddTabFieldDelimiter, bool)
    vtkGetMacro(AddTabFieldDelimiter, bool)

    vtkSetVector3Macro(BlockSize, double)
    vtkGetVector3Macro(BlockSize, double)

    vtkSetMacro(RotAngle, double)
    vtkGetMacro(RotAngle, double)

    vtkSetStringMacro(EmptyCellString)
    vtkGetStringMacro(EmptyCellString)

    vtkSetMacro(EmptyCellValue, double)
    vtkGetMacro(EmptyCellValue, double)

    vtkSetMacro(NeighborAttribute, bool)
    vtkGetMacro(NeighborAttribute, bool)

protected:

    vtkAtgBlockModelReader();
    ~vtkAtgBlockModelReader();

    int RequestData(vtkInformation* request,
                    vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector);

private:

    // name of CSV file (full path)
    char* FileName;

    // field delimiter characters and related options
    char* FieldDelimiterCharacters;
    bool AddTabFieldDelimiter;

    // block size - if not already in file
    double BlockSize[3];

    // rotation angle - if not already in file
    double RotAngle;

    // replacement string if a string cell is empty (default: --)
    char* EmptyCellString;

    // replacement value if a numeric cell is empty (default: -1)
    double EmptyCellValue;

    // if true we automatically calculate the neighbor index attribute
    bool NeighborAttribute;

    // after reading all numeric data as double, some arrays need to be converted
    // (or rather rewritten) in vtkIdType format
    void convertDoubleArrayToIdType(vtkUnstructuredGrid* grid, std::string const& arrName);

    // convert any string attributes that are not "categories" into N_<name>
    void prepareNameAttributes(vtkUnstructuredGrid* grid);

    // add a value column if not already present
    void addValueIfNotPresent(vtkTable* table,
                              int nameId, int numComps, double* values);

    // add block IDs if not already present
    void addBlockIdIfNotPresent(vtkTable* table);

};

#endif
