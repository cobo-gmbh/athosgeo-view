/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoExtractMBPolyData.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.

========================================================================*/

// .NAME vtkGeoExtractMBPolyData - extracts PolyData blocks from a multiblock dataset.
// .SECTION Description
// vtkGeoExtractMBPolyData is a filter that extracts blocks from a multiblock dataset.
// Each node in the multi-block tree is identified by an \c index. The index can
// be obtained by performing a preorder traversal of the tree (including empty
// nodes). eg. A(B (D, E), C(F, G)).
// Inorder traversal yields: A, B, D, E, C, F, G
// Index of A is 0, while index of C is 4.

#ifndef __vtkGeoExtractMBPolyData_h
#define __vtkGeoExtractMBPolyData_h

#include <vtkPolyDataAlgorithm.h>
#include <GeoExtractMBPolyDataModuleModule.h>

class vtkCompositeDataIterator;
class vtkMultiPieceDataSet;
class vtkMultiBlockDataSet;
class vtkAppendPolyData;

class GEOEXTRACTMBPOLYDATAMODULE_EXPORT vtkGeoExtractMBPolyData: public vtkPolyDataAlgorithm
{
public:

    static vtkGeoExtractMBPolyData* New();
    vtkTypeMacro(vtkGeoExtractMBPolyData, vtkPolyDataAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    // Description:
    // Select the block indices to extract.
    // Each node in the multi-block tree is identified by an \c index. The index can
    // be obtained by performing a preorder traversal of the tree (including empty
    // nodes). eg. A(B (D, E), C(F, G)).
    // Inorder traversal yields: A, B, D, E, C, F, G
    // Index of A is 0, while index of C is 4.
    void AddIndex(unsigned int index);
    void RemoveIndex(unsigned int index);
    void RemoveAllIndices();

    // Description:
    // When set, the output mutliblock dataset will be pruned to remove empty
    // nodes. On by default.
    vtkSetMacro(PruneOutput, int)
    vtkGetMacro(PruneOutput, int)
    vtkBooleanMacro(PruneOutput, int)

protected:

    vtkGeoExtractMBPolyData();
    ~vtkGeoExtractMBPolyData();

    // Description:
    // Internal key, used to avoid pruning of a branch.
    static vtkInformationIntegerKey* DONT_PRUNE();

    /// Implementation of the algorithm.
    virtual int RequestData(vtkInformation*,
                          vtkInformationVector**,
                          vtkInformationVector*);

    /// ExtractMB subtree
    void CopySubTree(vtkCompositeDataIterator* loc,
    vtkMultiBlockDataSet* output, vtkMultiBlockDataSet* input);
    bool Prune(vtkMultiBlockDataSet* mblock);
    bool Prune(vtkMultiPieceDataSet* mblock);
    bool Prune(vtkDataObject* mblock);

    int PruneOutput;

    virtual int FillInputPortInformation(int port, vtkInformation* info);

private:

    vtkGeoExtractMBPolyData(const vtkGeoExtractMBPolyData&); // Not implemented.
    void operator=(const vtkGeoExtractMBPolyData&); // Not implemented.

    class vtkSet;
    vtkSet* Indices;

};

#endif
