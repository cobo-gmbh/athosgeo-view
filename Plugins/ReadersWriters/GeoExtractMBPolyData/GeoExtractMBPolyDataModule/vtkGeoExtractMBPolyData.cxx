/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoExtractMBPolyData.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// By: Matthew Livingstone

#include <set>
#include <vtkSmartPointer.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkObjectFactory.h>
#include <vtkDataSet.h>
#include <vtkDataObjectTreeIterator.h>
#include <vtkInformationIntegerKey.h>
#include <vtkMultiPieceDataSet.h>
#include <vtkPolyData.h>
#include <vtkAppendPolyData.h>
#include <vtkExecutive.h>
#include <vtkCompositeDataSet.h>
#include <vtkGeoExtractMBPolyData.h>

class vtkGeoExtractMBPolyData::vtkSet: public std::set<unsigned int>
{
};

vtkStandardNewMacro(vtkGeoExtractMBPolyData)
vtkInformationKeyMacro(vtkGeoExtractMBPolyData, DONT_PRUNE, Integer)

//----------------------------------------------------------------------------
vtkGeoExtractMBPolyData::vtkGeoExtractMBPolyData()
{
    Indices = new vtkGeoExtractMBPolyData::vtkSet();
    PruneOutput = 0;
}

//----------------------------------------------------------------------------
vtkGeoExtractMBPolyData::~vtkGeoExtractMBPolyData()
{
    delete Indices;
}

//----------------------------------------------------------------------------
void vtkGeoExtractMBPolyData::AddIndex(unsigned int index)
{
    Indices->insert(index);
    Modified();
}


//----------------------------------------------------------------------------
void vtkGeoExtractMBPolyData::RemoveIndex(unsigned int index)
{
    Indices->erase(index);
    Modified();
}

//----------------------------------------------------------------------------
void vtkGeoExtractMBPolyData::RemoveAllIndices()
{
    Indices->clear();
    Modified();
}

//----------------------------------------------------------------------------
void vtkGeoExtractMBPolyData::CopySubTree(vtkCompositeDataIterator* loc,
                                     vtkMultiBlockDataSet* output, vtkMultiBlockDataSet* input)
{
    vtkDataObject* inputNode = input->GetDataSet(loc);

    if(!inputNode->IsA("vtkCompositeDataSet"))
    {
        vtkDataObject* clone = inputNode->NewInstance();
        clone->ShallowCopy(inputNode);
        output->SetDataSet(loc, clone);
    }

    else
    {
        vtkCompositeDataSet* cinput = vtkCompositeDataSet::SafeDownCast(inputNode);
        vtkCompositeDataSet* coutput = vtkCompositeDataSet::SafeDownCast(
        output->GetDataSet(loc));
        vtkCompositeDataIterator* iter = cinput->NewIterator();

        for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
        {
            vtkDataObject* curNode = iter->GetCurrentDataObject();
            vtkDataObject* clone = curNode->NewInstance();
            clone->ShallowCopy(curNode);
            coutput->SetDataSet(iter, clone);
            clone->Delete();
        }

        iter->Delete();
    }
}

//----------------------------------------------------------------------------
int vtkGeoExtractMBPolyData::RequestData(vtkInformation* vtkNotUsed(request),
                                    vtkInformationVector** inputVector,
                                    vtkInformationVector* outputVector)
{
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkMultiBlockDataSet* input = vtkMultiBlockDataSet::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
    vtkPolyData* polyOutput = vtkPolyData::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    vtkSmartPointer<vtkMultiBlockDataSet> selectedItems = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    selectedItems->DeepCopy(input);

    // Copy selected blocks over to the output.
    vtkDataObjectTreeIterator* iter = vtkDataObjectTreeIterator::SafeDownCast(input->NewIterator());
    if(iter)
    {
        iter->VisitOnlyLeavesOff();
        for(iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
        {
            if(Indices->find(iter->GetCurrentFlatIndex()) != Indices->end())
            {
                CopySubTree(iter, selectedItems, input);
                // TODO: avoid copying if subtree has already been copied over.
            }
        }
        iter->Delete();
    }

    if(PruneOutput)
    {
        // Now prune the output tree.

        // Since in case multiple processes are involved, this process may have some
        // data-set pointers NULL. Hence, pruning cannot simply trim NULL ptrs, since
        // in that case we may end up with different structures on different
        // processess, which is a big NO-NO. Hence, we first flag nodes based on
        // whether they are being pruned or not.

        iter = vtkDataObjectTreeIterator::SafeDownCast(selectedItems->NewIterator());
        if(iter)
        {
            iter->VisitOnlyLeavesOff();
            iter->SkipEmptyNodesOff();
            for(iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
            {
                if(Indices->find(iter->GetCurrentFlatIndex()) != Indices->end())
                {
                    iter->GetCurrentMetaData()->Set(DONT_PRUNE(), 1);
                }
                else if(iter->HasCurrentMetaData() && iter->GetCurrentMetaData()->Has(DONT_PRUNE()))
                {
                    iter->GetCurrentMetaData()->Remove(DONT_PRUNE());
                }
            }
            iter->Delete();
        }

        // Do the actual pruning. Only those branches are pruned which don't have
        // DONT_PRUNE flag set.
        Prune(selectedItems);
    }

    if(selectedItems->GetNumberOfBlocks() <= 0)
    {
        // Ensure empty output
        vtkSmartPointer<vtkPolyData> temp = vtkSmartPointer<vtkPolyData>::New();
        polyOutput->ShallowCopy(temp);
        return 1;
    }

    // Final PolyData output object
    vtkSmartPointer<vtkAppendPolyData> polyDataGroup = vtkSmartPointer<vtkAppendPolyData>::New();

    // Append all blocks to the PolyData output
    vtkDataObjectTreeIterator* iter2 = vtkDataObjectTreeIterator::SafeDownCast(selectedItems->NewIterator());
    if(iter2)
    {
        iter2->VisitOnlyLeavesOn();
        iter2->TraverseSubTreeOn();
        while(!iter2->IsDoneWithTraversal())
        {
            polyDataGroup->AddInputData(vtkPolyData::SafeDownCast(iter2->GetCurrentDataObject()));
            iter2->GoToNextItem();
        }
        iter2->Delete();
    }

    polyDataGroup->Update();

    // Copy points & cells & properties
    polyOutput->ShallowCopy(polyDataGroup->GetOutput());

    return 1;
}

//----------------------------------------------------------------------------
bool vtkGeoExtractMBPolyData::Prune(vtkDataObject* branch)
{
    if (branch->IsA("vtkMultiBlockDataSet"))
    {
        return Prune(vtkMultiBlockDataSet::SafeDownCast(branch));
    }
    else if (branch->IsA("vtkMultiPieceDataSet"))
    {
        return Prune(vtkMultiPieceDataSet::SafeDownCast(branch));
    }

    return true;
}

//----------------------------------------------------------------------------
bool vtkGeoExtractMBPolyData::Prune(vtkMultiPieceDataSet* mpiece)
{
    // Remove any children on mpiece that don't have DONT_PRUNE set.
    vtkSmartPointer<vtkMultiPieceDataSet> clone = vtkSmartPointer<vtkMultiPieceDataSet>::New();
    unsigned int index = 0;
    unsigned int numChildren = mpiece->GetNumberOfPieces();

    for(unsigned int cc = 0; cc < numChildren; cc++)
    {
        if(mpiece->HasMetaData(cc) && mpiece->GetMetaData(cc)->Has(DONT_PRUNE()))
        {
            clone->SetPiece(index, mpiece->GetPiece(cc));
            clone->GetMetaData(index)->Copy(mpiece->GetMetaData(cc));
            index++;
        }
    }

    mpiece->ShallowCopy(clone);

    // tell caller to prune mpiece away if num of pieces is 0.
    return (mpiece->GetNumberOfPieces() == 0);
}

//----------------------------------------------------------------------------
bool vtkGeoExtractMBPolyData::Prune(vtkMultiBlockDataSet* mblock)
{
    vtkSmartPointer<vtkMultiBlockDataSet> clone = vtkSmartPointer<vtkMultiBlockDataSet>::New();
    unsigned int index = 0;
    unsigned int numChildren = mblock->GetNumberOfBlocks();

    for(unsigned int cc = 0; cc < numChildren; cc++)
    {
        vtkDataObject* block = mblock->GetBlock(cc);
        if(mblock->HasMetaData(cc) && mblock->GetMetaData(cc)->Has(DONT_PRUNE()))
        {
            clone->SetBlock(index, block);
            clone->GetMetaData(index)->Copy(mblock->GetMetaData(cc));
            index++;
        }

        else if(block)
        {
            bool prune = Prune(block);
            if(!prune)
            {
                vtkMultiBlockDataSet* prunedBlock = vtkMultiBlockDataSet::SafeDownCast(block);
                if(prunedBlock && prunedBlock->GetNumberOfBlocks() == 1)
                {
                    // shrink redundant branches.
                    clone->SetBlock(index, prunedBlock->GetBlock(0));
                    if (prunedBlock->HasMetaData(static_cast<unsigned int>(0)))
                    {
                        clone->GetMetaData(index)->Copy(prunedBlock->GetMetaData(
                        static_cast<unsigned int>(0)));
                    }
                }
                else
                {
                    clone->SetBlock(index, block);
                    if(mblock->HasMetaData(cc))
                    {
                        clone->GetMetaData(index)->Copy(mblock->GetMetaData(cc));
                    }
                }

                index++;
            }
        }
    }

    mblock->ShallowCopy(clone);

    return (mblock->GetNumberOfBlocks() == 0);
}

//----------------------------------------------------------------------------
void vtkGeoExtractMBPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);
    os << indent << "PruneOutput: " << PruneOutput << endl;
}

int vtkGeoExtractMBPolyData::FillInputPortInformation(int, vtkInformation *info)
{
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMultiBlockDataSet");
    return 1;
}
