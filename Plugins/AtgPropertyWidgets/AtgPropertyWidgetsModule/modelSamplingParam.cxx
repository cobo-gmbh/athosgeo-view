/*=========================================================================

   Program: AthosGEO
   Module:  modelSamplingParam.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <algorithm>
#include <QModelIndex>
#include <delegateSamplingParam.h>
#include <modelSamplingParam.h>

modelSamplingParam::modelSamplingParam(std::set<std::string> const& types,
                                       DecoratorDefMap& def, bool isHoles,
                                       delegateSamplingParam* delegate,
                                       QObject* parent)
:   QAbstractTableModel(parent),
    def_(def),
    isHoles_(isHoles),
    delegate_(delegate)
{
    for(auto it = types.begin(); it != types.end(); ++it)
    {
        int ty = 0;
        double sz = 1.;

        // check if we have a default
        DecoratorDefMap::iterator defIt = def_.find(*it);
        if(def_.end() != defIt)
        {
            ty = defIt->second.first;
            sz = defIt->second.second;
        }

        // remember
        types_[*it] = DecoratorDef(ty, sz);
    }
}

modelSamplingParam::~modelSamplingParam()
{
}

int modelSamplingParam::rowCount(QModelIndex const& parent) const
{
    (void)&parent;
    return static_cast<int>(types_.size());
}

int modelSamplingParam::columnCount(QModelIndex const &parent) const
{
    (void)&parent;
    return 3;
}

QVariant modelSamplingParam::headerData(int section, Qt::Orientation orientation, int role) const
{
    if((Qt::Vertical == orientation) ||
       (Qt::DisplayRole != role))
        return QVariant();

    switch(section)
    {
        case 0: return "Type";
        case 1: return "Decorator";
        case 2: return "Size";
        default: return QVariant();
    }
}

QVariant modelSamplingParam::data(QModelIndex const& index, int role) const
{
    if((index.row() >= static_cast<int>(types_.size())) || (index.column() >= 3))
        return QVariant();

    switch(role)
    {
        case Qt::DisplayRole:
        case Qt::EditRole:
        {
            DecoratorDefMap::const_iterator defIt = types_.begin();
            std::advance(defIt, index.row());

            switch(index.column())
            {
                case 0:
                {
                    return  defIt->first.c_str();
                }
                case 1:
                {
                    int inx = defIt->second.first;
                    return delegate_->getDecorator(inx).c_str();
                }
                case 2:
                {
                    return defIt->second.second;
                }
            }
            break;
        }

        case Qt::TextAlignmentRole:
        {
            switch(index.column())
            {
                case 0: return Qt::AlignLeft;
                case 1:
                case 2: return Qt::AlignHCenter;
            }
        }
    }

    return QVariant();
}

bool modelSamplingParam::setData(QModelIndex const& index, QVariant const& val, int role)
{
    (void)&role;

    if((index.row() >= static_cast<int>(types_.size())) || (index.column() == 0) || (index.column() >= 3))
        return false;

    DecoratorDefMap::iterator defIt = types_.begin();
    std::advance(defIt, index.row());

    switch(index.column())
    {
        case 1:
        {
            std::string dec = val.toString().toStdString();
            defIt->second.first = delegate_->getIndex(dec);
            break;
        }
        case 2:
        {
            defIt->second.second = val.toDouble();
            break;
        }
    }

    emit dataChanged(index, index);

    return true;
}

Qt::ItemFlags modelSamplingParam::flags(QModelIndex const& index) const
{
    switch(index.column())
    {
        case 1:
        case 2: return Qt::ItemIsEditable |
                       Qt::ItemIsEnabled;
    }

    return Qt::ItemIsEnabled;
}

void modelSamplingParam::getDecoratorDef(int row, std::string& type, DecoratorDef& def)
{
    if(row >= rowCount())
        return;

    type = data(index(row, 0)).toString().toStdString();
    def.first = delegate_->getIndex(data(index(row, 1), Qt::EditRole).toString().toStdString());
    def.second = data(index(row, 2), Qt::EditRole).toDouble();
}

void modelSamplingParam::sizeDecoratorsBy(double f)
{
    for(int r = 0; r < rowCount(); ++r)
    {
        double sz = data(index(r, 2), Qt::EditRole).toDouble();
        sz *= f;
        setData(index(r, 2), sz);
    }
}
