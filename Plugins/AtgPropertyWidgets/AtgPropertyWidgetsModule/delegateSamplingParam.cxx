/*=========================================================================

   Program: AthosGEO
   Module:  delegateSamplingParam.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <QStyleOptionViewItem>
#include <QWidget>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <delegateSamplingParam.h>

delegateSamplingParam::delegateSamplingParam(std::vector<std::string> const& items,
                                             QObject* parent)
:   QStyledItemDelegate(parent),
    items_(items)
{
}

delegateSamplingParam::~delegateSamplingParam()
{
}

QWidget* delegateSamplingParam::createEditor(QWidget* parent,
                                             QStyleOptionViewItem const& option,
                                             QModelIndex const& index) const
{
    switch(index.column())
    {
        // combo box with decorators
        case 1:
        {
            // create and populate the combo box
            QComboBox* cb = new QComboBox(parent);
            for(auto it = items_.begin(); it != items_.end(); ++it)
                cb->addItem(it->c_str());
            return cb;
        }
        case 2:
        {
            QDoubleSpinBox* sb = new QDoubleSpinBox(parent);
            sb->setSingleStep(0.1);
            return sb;
        }
        default:
        {
            return QStyledItemDelegate::createEditor(parent, option, index);
        }
    }
}

void delegateSamplingParam::setEditorData(QWidget* editor, QModelIndex const& index) const
{
    if(QComboBox* cb = qobject_cast<QComboBox*>(editor))
    {
        // retrieve the text string from the model and select it in the combo box
        QString ct = index.data(Qt::EditRole).toString();
        cb->setCurrentText(ct);
    }

    else if(QDoubleSpinBox* sb = qobject_cast<QDoubleSpinBox*>(editor))
    {
        double val = index.data(Qt::EditRole).toDouble();
        sb->setValue(val);
    }

    else
    {
        QStyledItemDelegate::setEditorData(editor, index);
    }
}

void delegateSamplingParam::setModelData(QWidget* editor, QAbstractItemModel* model,
                                              QModelIndex const& index) const
{
    if(QComboBox* cb = qobject_cast<QComboBox*>(editor))
    {
        // get text from combo box and write it to the model
        model->setData(index, cb->currentText(), Qt::EditRole);
    }

    else if(QDoubleSpinBox* sb = qobject_cast<QDoubleSpinBox*>(editor))
    {
        model->setData(index, sb->value(), Qt::EditRole);
    }

    else
    {
        QStyledItemDelegate::setModelData(editor, model, index);
    }
}

std::string delegateSamplingParam::getDecorator(int inx) const
{
    if((inx >= 0) && (inx < items_.size()))
        return items_[inx];
    else
        return "";
}

int delegateSamplingParam::getIndex(std::string const& decorator) const
{
    auto it = std::find(items_.begin(), items_.end(), decorator);
    if(items_.end() == it)
        return -1;
    else
        return std::distance(items_.begin(), it);
}
