﻿/*=========================================================================

   Program: AthosGEO
   Module:  widMultiSelsAndValues.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widMultiSelsAndValues_h
#define widMultiSelsAndValues_h

#include <vector>
#include <set>
#include <QList>
#include <QVariant>
#include <pqPropertyGroupWidget.h>
#include <modelValsAndSels.h>
#include <AtgPropertyWidgetsModuleModule.h>

class vtkSMProxy;
class vtkSMPropertyGroup;
class vtkSMIntVectorProperty;
class vtkSMStringVectorProperty;
class pqOutputPort;
class pqPropertiesPanel;
class pqSelectionManager;
class pqProxy;
class widCellArrays;

namespace Ui
{
    class MultiSelsAndValues;
}

class ATGPROPERTYWIDGETSMODULE_EXPORT widMultiSelsAndValues: public pqPropertyGroupWidget
{
    Q_OBJECT
    typedef pqPropertyGroupWidget Superclass;

    // mapping these two VTK properties to Qt Properties
    Q_PROPERTY(int NoErrors READ getNoErrors WRITE setNoErrors NOTIFY noErrorsChanged)
    Q_PROPERTY(QList<QVariant> NewValues READ getNewValues WRITE setNewValues NOTIFY newValuesChanged)
    Q_PROPERTY(QList<QVariant> IdSets READ getIdSets WRITE setIdSets NOTIFY idSetsChanged)
    Q_PROPERTY(QList<QVariant> LockItems READ getLockItems WRITE setLockItems NOTIFY lockItemsChanged)
    Q_PROPERTY(int CurrentItem READ getCurrentItem WRITE setCurrentItem NOTIFY currentItemChanged)

public:

    widMultiSelsAndValues(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup, QWidget* parent = 0);
    virtual ~widMultiSelsAndValues();

    // handle the "NoErrors" Qt property - for internal purposes only
    int getNoErrors() const;
    void setNoErrors(int nerr);

    // sequence of new values to be set
    QList<QVariant> getNewValues() const;
    void setNewValues(QList<QVariant> const& newVals);

    // sequence of strings, each one containing a comma separated list of selected block ids
    QList<QVariant> getIdSets() const;
    void setIdSets(QList<QVariant> const& idSets);

    // index of the currently locked item (new value), or negative if none
    QList<QVariant> getLockItems() const;
    void setLockItems(QList<QVariant> const& lockItems);

    // index of the currently active value
    int getCurrentItem() const;
    void setCurrentItem(int currentItem);

    // manage the selected row in the table view
    void selectRow(int row);
    int selectedRow() const;

protected:

    // catch this for updating the selection in the view(s)
    virtual void showEvent(QShowEvent* event);

signals:

    // handle the Qt properties
    void noErrorsChanged();
    void newValuesChanged();
    void idSetsChanged();
    void lockItemsChanged();
    void currentItemChanged();

private slots:

    // if the proxy is created, do the "early apply" - to ensure that we have
    // some real "output port" here already, even before the "user apply": we need
    // this to be able to do the selection on it - instead on the parent object
    void earlyApply(pqProxy* proxy);

    // make sure the active variable is properly updated after an apply
    void updateActiveVariable();

    // react on changes in the selection by the user
    void handleChangedSelection(pqOutputPort* port);

    // react on changes in the selected row in the table view
    void handleChangedRow();

    // add another value
    void addNewValue();

    // remove currently selected value (if this is not the last one)
    void removeNewValue();

private:

    Q_DISABLE_COPY(widMultiSelsAndValues)

    // copy the selected IDs from the current port to the Ids property
    void activeSelectionToProperty();

    // apply the ids Ids property to generate a selection in the current port
    void propertyToActiveSelection();

    // access to some PV items
    pqSelectionManager* getSelectionManager();
    widCellArrays* getVariablesWidget();

    // convert selection IDs from and to QList<QVariant>
    static std::vector<modelValsAndSels::IdSet> selIdsToIdSet(QList<QVariant> const& sel);
    static QList<QVariant> selIdsToVarList(std::vector<modelValsAndSels::IdSet> const& sel);

    // actual Qt widget
    Ui::MultiSelsAndValues* ui;

    // the output port to which the widget refers
    pqOutputPort* outPort;

    // the panel in which this widget appears
    pqPropertiesPanel* propertiesPanel;

    // this property contains the name of the active scalar attribute, to be set
    // after an "apply" call by the widget. The name of this property is specified
    // by the "ActiveScalar" property (which is hidden in the user interface)
    // that has to be specified in the plugin XML file.
    // note: this is a workaround for the fact that a call to "SetActiveScalar"
    // within the filter code has only once, during the first call, the effect of
    // actually showing that attribute in the renderer, but for filters that need
    // the special handling of selection ids, the first call is the "initialization
    // apply" call where the final attribute name is not yet known!
    vtkSMStringVectorProperty* activeScalarProperty;

    // this property is a "flag" that we transmit to the "apply" (=RequestData) function
    // of the related filter, in order to not complain if not all properties are specified yet
    // note: the noErrors variable is only a constraint for the PV system, in order to handle
    //   the property in the normal way (making use of the property system of Qt). The
    //   noErrorsProp is required for really setting the value "on" or "off" for the filter
    vtkSMProxy* smprx;
    int noErrors_;
    QMetaObject::Connection* connection;

    // flag indicating "init apply" or "regular apply": make sure that the
    // switching to an active attribute happens not during init apply
    static unsigned long long initializingInstance;

    // table view model
    // note: this is also the location where the class properties
    //   NewValues, IdSets, LockItems and CurrentItem are stored
    modelValsAndSels* valsSelsModel_;

    // internal flag
    std::vector<bool> first;

};

#endif
