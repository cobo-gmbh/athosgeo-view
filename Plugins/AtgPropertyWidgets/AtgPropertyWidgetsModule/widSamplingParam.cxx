/*=========================================================================

   Program: AthosGEO
   Module:  widSamplingParam.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <fstream>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <QWidget>
#include <QStringList>
#include <QTableWidgetItem>
#include <QFont>
#include <utilNormNames.h>
#include <utilFormattedToolTip.h>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkAtgCsvReader.h>
#include <vtkInitializationHelper.h>
#include <vtk_jsoncpp.h>
#include <vtkSMProxy.h>
#include <vtkSMProperty.h>
#include <vtkSMPropertyGroup.h>
#include <vtkSMDocumentation.h>
#include <vtkSMSettings.h>
#include <pqSMAdaptor.h>
#include <pqFileDialog.h>
#include <ui_widSamplingParam.h>
#include <delegateSamplingParam.h>
#include <modelSamplingParam.h>
#include <widSamplingParam.h>

widSamplingParam::widSamplingParam(vtkSMProxy* smproxy,
                                   vtkSMPropertyGroup* smgroup, QWidget* parent)
:   Superclass(smproxy, smgroup, parent),
    ui(new Ui::SamplingParam()),
    dedefModel(nullptr)
{
    // currently not using resources
    //Q_INIT_RESOURCE(widSamplingParam);

    ui->setupUi(this);

    // get the assay file name
    vtkSMProperty* assayProp = smproxy->GetProperty("AssayFile");
    if(nullptr != assayProp)
        assayFileName = pqSMAdaptor::getFileListProperty(assayProp).first().toStdString();

    // update the table column information table
    updateTableColTable();
    connect(ui->editCollar, SIGNAL(textChanged(QString)), SLOT(updateTableColTable()));
    connect(ui->editSurvey, SIGNAL(textChanged(QString)), SLOT(updateTableColTable()));
    connect(ui->pbMult2, SIGNAL(pressed()), SLOT(sizeMult2()));
    connect(ui->pbDiv2, SIGNAL(pressed()), SLOT(sizeDiv2()));
    connect(ui->pbCollar, SIGNAL(pressed()), SLOT(selectCollarFile()));
    connect(ui->pbSurvey, SIGNAL(pressed()), SLOT(selectSurveyFile()));

    // connections between Qt and VTK properties
    addPropertyLink(ui->editSamplingIdPrefix, "text", SIGNAL(textChanged(QString const&)), smgroup->GetProperty("SamplingIdPrefix"));
    addPropertyLink(ui->editEmptyCellString, "text", SIGNAL(textChanged(QString const&)), smgroup->GetProperty("EmptyCellString"));
    addPropertyLink(ui->sbEmptyCellValue, "value", SIGNAL(valueChanged(double)), smgroup->GetProperty("EmptyCellValue"));
    addPropertyLink(ui->cbNegativeDipDown, "checked", SIGNAL(stateChanged(int)), smgroup->GetProperty("NegativeDipDown"));
    addPropertyLink(this, "Decorations", SIGNAL(decorationsChanged()), smgroup->GetProperty("Decorations"));
    addPropertyLink(this, "CollarFile", SIGNAL(collarFileChanged()), smgroup->GetProperty("CollarFile"));
    addPropertyLink(this, "SurveyFile", SIGNAL(surveyFileChanged()), smgroup->GetProperty("SurveyFile"));
}

widSamplingParam::~widSamplingParam()
{
    ui->tableViewDecorators->setModel(nullptr);
    delete dedefModel;
    dedefModel = nullptr;
    delete ui;
    ui = nullptr;
}

QString widSamplingParam::getCollarFile() const
{
    return ui->editCollar->text();
}

void widSamplingParam::setCollarFile(QString const& cf)
{
    ui->editCollar->setText(cf);
    emit collarFileChanged();
}

QString widSamplingParam::getSurveyFile() const
{
    return ui->editSurvey->text();
}

void widSamplingParam::setSurveyFile(QString const& sf)
{
    ui->editSurvey->setText(sf);
    emit surveyFileChanged();
}

QString widSamplingParam::getDecorations() const
{
    QStringList bsl;

    QAbstractItemModel* md = ui->tableViewDecorators->model();
    delegateSamplingParam* del =
            qobject_cast<delegateSamplingParam*>(ui->tableViewDecorators->itemDelegate());
    if((nullptr == md) || (nullptr == del))
        return ""; // emergency exit

    for(int r = 0; r < md->rowCount(); ++r)
    {
        QString dec = QString("\"%1\",%2,%3")
                      .arg(md->data(md->index(r, 0)).toString())
                      .arg(del->getIndex(md->data(md->index(r, 1)).toString().toStdString()))
                      .arg(md->data(md->index(r, 2)).toDouble());
        bsl.push_back(dec);
    }

    return bsl.join(';');
}

void widSamplingParam::setDecorations(QString const& bs)
{
    // this should not really happen: we do this in the constructor
    emit decorationsChanged();
}

void widSamplingParam::sizeMult2()
{
    sizeDecoratorsBy(2.);
}

void widSamplingParam::sizeDiv2()
{
    sizeDecoratorsBy(.5);
}

void widSamplingParam::selectCollarFile()
{
    QString filters = "CSV Files (*.csv);;All files (*)",
            title = tr("Open Collar File:");
    pqFileDialog dialog(nullptr, this, title, QString(), filters);
    dialog.setFileMode(pqFileDialog::ExistingFile);

    if(QDialog::Accepted != dialog.exec())
        return;

    QStringList selectedFiles = dialog.getSelectedFiles();
    if(selectedFiles.empty())
        return;

    ui->editCollar->setText(selectedFiles.front());
}

void widSamplingParam::selectSurveyFile()
{
    QString filters = "CSV Files (*.csv);;All files (*)",
            title = tr("Open Survey File:");
    pqFileDialog dialog(nullptr, this, title, QString(), filters);
    dialog.setFileMode(pqFileDialog::ExistingFile);

    if(QDialog::Accepted != dialog.exec())
        return;

    QStringList selectedFiles = dialog.getSelectedFiles();
    if(selectedFiles.empty())
        return;

    ui->editSurvey->setText(selectedFiles.front());
}

void widSamplingParam::updateTableColTable()
{
    // there are up to 3 tables to handle
    std::vector<std::string> tabNames;
    tabNames.push_back(assayFileName);
    tabNames.push_back(ui->editCollar->text().toStdString());
    tabNames.push_back(ui->editSurvey->text().toStdString());
    std::string tabLetters("ACS");

    // we want to determine the decorator type
    bool hasDepth = true, // to let the loop below start
         hasHoleId = false,
         hasSampleId = false;
    std::set<std::string> holeTypes;

    // special case: if we find a "Center", we interpret it as "Collar"
    // note: the "Center" is for block models, but here we are dealing with samples
    std::string centerName = utilNormNames::getName(utilNormNames::BLOCKCENTER),
                collarName = utilNormNames::getName(utilNormNames::COLLAR),
                depthName = utilNormNames::getName(utilNormNames::DEPTH),
                holeIdName = utilNormNames::getName(utilNormNames::HOLEID),
                sampleIdName = utilNormNames::getName(utilNormNames::SAMPID),
                holeTypeName = utilNormNames::getName(utilNormNames::HOLETYPE);

    // reset the columns table
    ui->tableWidgetColumns->clearContents();
    ui->tableWidgetColumns->setRowCount(0);

    // read input files as far as present
    for(int ii = 0; hasDepth && (ii < 3); ++ii)
    {
        // collar and survey may be missing
        if((0 < ii) && tabNames[ii].empty())
            continue;

        // start by checking if the file exists at all
        std::ifstream file;
        file.open(tabNames[ii], ios::in);
        bool ok = file.is_open();
        file.close();
        if(!ok)
            continue;

        // we are reading the ENTIRE table here: not really the recommended
        // way to proceed, but not avoidable because of the sampling types
        auto reader = vtkSmartPointer<vtkAtgCsvReader>::New();
        reader->SetFileName(tabNames[ii].c_str());
        reader->SetFieldDelimiterCharacters(",");
        reader->SetEmptyStringReplacement("---");
        reader->SetAddTabFieldDelimiter(false);
        reader->SetEmptyNumberReplacement(-1);
        reader->SetTreatExcelErrorsAsEmpty(true);
        reader->SetRemoveLastLineIfLessCellsThanHeader(true);
        reader->Update();

        // remember table for further processing
        vtkSmartPointer<vtkTable> tab = vtkSmartPointer<vtkTable>::New();
        tab->DeepCopy(reader->GetOutput());

        // now we want to find out, but first we don't have
        if(ii == 0)
            hasDepth = false;

        // find standard forms and types of the columns and fill into the table widget
        for(int c = 0; c < tab->GetNumberOfColumns(); ++c)
        {
            // get the name and normalize it
            std::string name = tab->GetColumnName(c),
                        normName = utilNormNames::normalize(name, true);

            // split into base name and extension
            std::string::size_type colPos = normName.find(":");
            if(std::string::npos == colPos)
                colPos = normName.size();
            std::string baseName = normName.substr(0, colPos);

            // see if we have a "Depth", "HoleID" and "SampleID"
            // because then it will be a drill hole
            if(baseName == depthName)
            {
                hasDepth = true;
            }
            else if(baseName == holeIdName)
            {
                hasHoleId = true;
            }
            else if(baseName == sampleIdName)
            {
                hasSampleId = true;
            }

            // if this is the hole types column - remember it
            else if(baseName == holeTypeName)
            {
                vtkAbstractArray* htArr = tab->GetColumn(c);
                if(nullptr != htArr)
                {
                    for(vtkIdType r = 0; r < tab->GetNumberOfRows(); ++r)
                        holeTypes.insert(htArr->GetVariantValue(r).ToString().c_str());
                }
            }

            // if now the base name is "Center", we make it "Collar"
            else if(baseName == centerName)
            {
                baseName = collarName;
                normName = baseName +
                           ((colPos < normName.size()) ?
                                (":" + normName.substr(colPos + 1)) : "");
            }

            // check if this is a string column (or rather: not a numeric column)
            if(nullptr == vtkDataArray::SafeDownCast(tab->GetColumn(c)))
            {
                int dataType = utilNormNames::getType(baseName);
                if((utilNormNames::TY_CATEGORY != dataType) &&
                   (utilNormNames::TY_NAME != dataType) &&
                   (utilNormNames::TY_DATE != dataType))
                {
                    baseName = "N_" + baseName;
                    normName = "N_" + normName;
                }
            }

            // now we find the description of the type
            std::string typeDesc = utilNormNames::getTypeDesc(baseName);

            int nextRow = ui->tableWidgetColumns->rowCount();
            ui->tableWidgetColumns->insertRow(nextRow);

            QTableWidgetItem* item = new QTableWidgetItem((std::string(1, tabLetters[ii]) + ":" + name).c_str());
            ui->tableWidgetColumns->setItem(nextRow, 0, item);

            item = new QTableWidgetItem(normName.c_str());
            QFont f = item->font();
            f.setBold(true);
            item->setFont(f);
            ui->tableWidgetColumns->setItem(nextRow, 1, item);

            item = new QTableWidgetItem(typeDesc.c_str());
            f = item->font();
            f.setItalic(true);
            item->setFont(f);
            ui->tableWidgetColumns->setItem(nextRow, 2, item);
        }
    }

    // without a depth attribute, we do not expect holes, and without holes, we do
    // not need additional tables
    ui->editCollar->setEnabled(hasDepth);
    ui->pbCollar->setEnabled(hasDepth);
    ui->editSurvey->setEnabled(hasDepth);
    ui->pbSurvey->setEnabled(hasDepth);

    // adapt the sampling label label if depending on "holes" or "samples"
    if(hasDepth)
    {
        ui->labelSamplingId->setText("Hole ID Prefix");
        ui->editSamplingIdPrefix->setText("HOLE_");
        if(hasHoleId)
            ui->editSamplingIdPrefix->setEnabled(false);
    }
    else
    {
        ui->labelSamplingId->setText("Sample ID Prefix");
        ui->editSamplingIdPrefix->setText("SAMPLE_");
        if(hasSampleId)
            ui->editSamplingIdPrefix->setEnabled(false);
    }

    // decorations table - if we have a "HoleType" attribute
    if(holeTypes.empty())
        holeTypes.insert("(all)");

    // some standard decorators for hole types
    modelSamplingParam::DecoratorDefMap defMap;
    getDefMapFromSettings(defMap);

    // if the decorator table view has a model, remove and delete it
    modelSamplingParam* ddm = qobject_cast<modelSamplingParam*>(ui->tableViewDecorators->model());
    if(nullptr != ddm)
        updateDefMapFromModel(ddm, defMap);
    ui->tableViewDecorators->setModel(nullptr);
    if(nullptr != ddm)
        delete ddm;

    // generate a model for the decorator definition table view
    delegateSamplingParam* dedefDelegate = getDefDelegate(hasDepth);
    ui->tableViewDecorators->setItemDelegate(dedefDelegate);
    dedefModel = new modelSamplingParam(holeTypes, defMap, hasDepth, dedefDelegate, this);
    ui->tableViewDecorators->setModel(dedefModel);
    connect(dedefModel, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)),
            SIGNAL(decorationsChanged()));
}

void widSamplingParam::sizeDecoratorsBy(double f)
{
    dedefModel->sizeDecoratorsBy(f);
}

std::vector<std::vector<std::string>> widSamplingParam::parseCsv(std::string const& csvStr, bool tabs)
{
    // used to split the file in lines
    static const boost::regex linesregx("\\r\\n|\\n\\r|\\n|\\r");

    // used to split each line to tokens, assuming ',' as column separator
    static const boost::regex fieldsregx = tabs ?
                 boost::regex("\\t") :
                 boost::regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

    std::vector<std::vector<std::string>> result;

    // iterator splits data to lines
    boost::sregex_token_iterator li(csvStr.begin(), csvStr.end(), linesregx, -1);
    boost::sregex_token_iterator end;

    while(li != end)
    {
        std::string line = li->str();
        ++li;

        // split line to tokens
        boost::sregex_token_iterator ti(line.begin(), line.end(), fieldsregx, -1);
        boost::sregex_token_iterator end2;

        std::vector<std::string> row;

        while(ti != end2)
        {
            std::string token = ti->str();
            boost::trim_if(token, boost::is_any_of("\t \""));
            ++ti;
            row.push_back(token);
        }

        if(line.back() == ',')
        {
            // last character was a separator
            row.push_back("");
        }

        result.push_back(row);
    }

    return result;
}

void widSamplingParam::getDefMapFromSettings(modelSamplingParam::DecoratorDefMap& decoratorDefs)
{
    // JSON file with sampling reader default decoration settings
    std::string json = vtkInitializationHelper::GetUserSettingsDirectory() +
                       vtkInitializationHelper::GetApplicationName() +
                       "-SamplingReader.json";

    // if such a file does not exist we generate a default
    std::ifstream jsonFile(json);
    bool jsonExists = jsonFile.is_open();
    jsonFile.close();
    if(!jsonExists)
    {
        Json::CharReaderBuilder builder;
        builder["collectComments"] = true;
        std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
        std::string defdefs(
            "{ \"sources\" : { \"AtgSamplingReader\" : { \"DefaultDecorators\" : [ "
            "{\"Type\" : \"Baggersondierung\",   \"Decorator\" : 1, \"Size\" : 0.4},"
            "{\"Type\" : \"Handbohrung\",        \"Decorator\" : 2, \"Size\" : 0.3},"
            "{\"Type\" : \"Kernbohrung\",        \"Decorator\" : 0, \"Size\" : 0.3},"
            "{\"Type\" : \"Rammkernsondierung\", \"Decorator\" : 2, \"Size\" : 0.3} ] } } }");
        char const* input = defdefs.c_str();
        Json::Value jsonValue;
        if(reader->parse(input, input + ::strlen(input), &jsonValue, nullptr))
        {
            std::ofstream jsonFile(json);
            if(jsonFile.is_open())
            {
                jsonFile << jsonValue.toStyledString();
                jsonFile.close();
            }
        }
    }

    // get the settings from the file - either defaults or changed by the user
    vtkSMSettings* settings = vtkSMSettings::GetInstance();
    if(!settings->AddCollectionFromFile(json, 10.0))
        return; // should not happen

    // get the settings for further processing
    static const std::string sett("sources.AtgSamplingReader.DefaultDecorators");
    int num = settings->GetSettingNumberOfElements(sett.c_str());

    for(int i = 0; i < num; ++i)
    {
        std::string sett_i = sett + "[" + std::to_string(i) + "].";

        std::string ty = settings->GetSettingAsString((sett_i + "Type").c_str(), "");
        int dec = settings->GetSettingAsInt((sett_i + "Decorator").c_str(), 1);
        double sz = settings->GetSettingAsDouble((sett_i + "Size").c_str(), 0.3);

        decoratorDefs[ty] = modelSamplingParam::DecoratorDef(dec, sz);
    }
}

void widSamplingParam::updateDefMapFromModel(modelSamplingParam* model,
                                             modelSamplingParam::DecoratorDefMap& decoratorDefs)
{
    for(int row = 0; row < model->rowCount(); ++row)
    {
        std::string ty;
        modelSamplingParam::DecoratorDef def;
        model->getDecoratorDef(row, ty, def);

        decoratorDefs[ty] = def;
    }
}

delegateSamplingParam* widSamplingParam::getDefDelegate(bool isHoles)
{
    if(isHoles)
        return new delegateSamplingParam(
                    std::vector<std::string>(std::initializer_list<std::string>(
        {
            "Round cylinder",
            "Quadratic cylinder",
            "Triangular cylinder",
            "Hexagonal cylinder"
        })));
    else
        return new delegateSamplingParam(
                    std::vector<std::string>(std::initializer_list<std::string>(
        {
            "Sphere",
            "Cube",
            "Tetrahedron",
            "Octahedron"
         })));
}
