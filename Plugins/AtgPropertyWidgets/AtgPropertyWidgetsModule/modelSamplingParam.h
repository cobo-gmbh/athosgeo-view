/*=========================================================================

   Program: AthosGEO
   Module:  modelSamplingParam.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __modelSamplingParam_h
#define __modelSamplingParam_h

#include <map>
#include <set>
#include <string>
#include <QVariant>
#include <QAbstractTableModel>
#include <AtgPropertyWidgetsModuleModule.h>

class QModelIndex;
class delegateSamplingParam;

class ATGPROPERTYWIDGETSMODULE_EXPORT modelSamplingParam: public QAbstractTableModel
{
    Q_OBJECT

public:

    typedef std::pair<int, double> DecoratorDef;
    typedef std::map<std::string, DecoratorDef> DecoratorDefMap;

    modelSamplingParam(std::set<std::string> const& types, DecoratorDefMap& def, bool isHoles,
                       delegateSamplingParam* delegate, QObject* parent = nullptr);
    ~modelSamplingParam();

    virtual int rowCount(QModelIndex const& parent = QModelIndex()) const;
    virtual int columnCount(QModelIndex const& parent = QModelIndex()) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    virtual QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const;
    virtual bool setData(QModelIndex const& index, QVariant const& val, int role = Qt::EditRole);

    virtual Qt::ItemFlags flags(QModelIndex const& index) const;

    void getDecoratorDef(int row, std::string& type, DecoratorDef& def);

    // size all decorators by f
    void sizeDecoratorsBy(double f);

private:

    DecoratorDefMap types_;
    DecoratorDefMap def_;
    bool isHoles_;

    // only the delegate knows the list of decorators
    delegateSamplingParam* delegate_;

};

#endif
