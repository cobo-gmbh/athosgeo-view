/*=========================================================================

   Program: AthosGEO
   Module:  widCategoryValue.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <algorithm>
#include <QString>
#include <QComboBox>
#include <QMainWindow>
#include <QList>
#include <QVariant>
#include <utilFormattedToolTip.h>
#include <pqOutputPort.h>
#include <pqApplicationCore.h>
#include <pqObjectBuilder.h>
#include <pqPropertiesPanel.h>
#include <pqCoreUtilities.h>
#include <pqActiveObjects.h>
#include <vtkCommand.h>
#include <vtkSMProxy.h>
#include <vtkSMProperty.h>
#include <vtkSMIntVectorProperty.h>
#include <vtkSMStringVectorProperty.h>
#include <vtkSMPropertyGroup.h>
#include <vtkSMDocumentation.h>
#include <vtkPVXMLElement.h>
#include <atgPropertiesPanel.h>
#include <atgCategoriesManager.h>
#include <ui_widCategoryValue.h>
#include <widCategoryValue.h>

widCategoryValue::widCategoryValue(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup,
                                   QWidget* parent)
:   Superclass(smproxy, smgroup, parent),
    ui(new Ui::CategoryValue()),
    portIdProp(vtkSMStringVectorProperty::SafeDownCast(smgroup->GetProperty("PortId"))),
    earlyApplyProp(vtkSMIntVectorProperty::SafeDownCast(smgroup->GetProperty("EarlyApply")))
{
    // currently not using resources
    Q_INIT_RESOURCE(widCategoryValue);

    // initialize the widget
    ui->setupUi(this);

    // see if we have a documentation, to be applied in the tool tip
    vtkSMDocumentation* doc = smgroup->GetDocumentation();
    if(0 != doc)
    {
        utilFormattedToolTip ttip(doc->GetDescription());
        setToolTip(ttip.formattedText().c_str());
    }

    // make sure that changes of the array name are reflected in the Apply button
    connect(ui->cbCategory, SIGNAL(currentTextChanged(QString)), SLOT(updateCategory(QString)));
    connect(ui->cbCategory, SIGNAL(currentTextChanged(QString)), SLOT(adaptValues(QString)));
    connect(ui->cbValue, SIGNAL(currentTextChanged(QString)), SLOT(updateValue(QString)));

    // we also need a property link between Qt and VTK stuff
    addPropertyLink(this, "PortId", SIGNAL(portIdChanged()), smgroup->GetProperty("PortId"));
    addPropertyLink(this, "Category", SIGNAL(categoryChanged()), smgroup->GetProperty("Category"));
    addPropertyLink(this, "Value", SIGNAL(valueChanged()), smgroup->GetProperty("Value"));

    connect(ui->pbFirst, SIGNAL(pressed()), SLOT(setValueFirst()));
    connect(ui->pb5Back, SIGNAL(pressed()), SLOT(setValue5Back()));
    connect(ui->pbBack, SIGNAL(pressed()), SLOT(setValueBack()));
    connect(ui->pbForward, SIGNAL(pressed()), SLOT(setValueForward()));
    connect(ui->pb5Forward, SIGNAL(pressed()), SLOT(setValue5Forward()));
    connect(ui->pbLast, SIGNAL(pressed()), SLOT(setValueLast()));
    connect(ui->cbValue, SIGNAL(currentIndexChanged(int)), SLOT(setButtonEnableState()));

    // tell about the output port "id" number - if we do not have one yet!
    // note: this is finally for the provider of category/value info who will still prepend
    //   a prefix to this id string, depending on whether this is about a representation
    //   or a filter: in principle both can be applied to the same output port!
    if(portId.empty())
    {
        // convert the output port address to a number
        pqOutputPort* outPort = pqActiveObjects::instance().activePort();
        std::string idStr = atgCategoriesManager::portIdNum(outPort);

        // we have an "early apply property" if this widget is for a filter,
        // and otherwise it is for a representation
        if(nullptr == earlyApplyProp)
            idStr = "REP:" + idStr;
        else
            idStr = "FILT:" + idStr;

        // make this our key
        setPortId(QString(idStr.c_str()));
    }

    // if there is already some CatValInfo available: get and use it
    // note: this gets category/value info from the categories manager and fills them into
    //   the combo boxes, preserving selected items if they exist
    refreshCategoriesValues(portId);

    // make sure we get a message about the change of the categories and values list
    // note: It looks like there is no other way to notify from the server side to this widget
    //   that "data are ready" than with such a "manager"!
    //   Attempts with pqCoreUtilities::connect(vtkSMProperty* ...) look promising, but obviously
    //   the value changes in the vtkAlgorithm classes do not actually trigger that event
    //   to be fired - and so far I did not find another way to achieve that: impossible??
    connect(&atgCategoriesManager::instance(), SIGNAL(categoriesChanged(std::string)),
            SLOT(refreshCategoriesValues(std::string)));
    //vtkSMProperty* cvProp = smgroup->GetProperty("CatValInfo");
    //if(nullptr != cvProp)
    //    pqCoreUtilities::connect(cvProp, vtkCommand::ModifiedEvent, this, SLOT(refreshCategoriesValues()));

    // in the case that this property panel is for the Extract Category filter, we need
    // to do an "early apply" in order to initialize the categories and values, but we cannot
    // do it here in the constructor yet, but only after the filter and proxy are properly
    // generated by the "object builder", so we need to catch the signal that tells us
    // if this has happened
    // note: the proxyCreated signal is emitted in pqObjectBuilder::createFilter, immediately
    //   after also a filterCreated signal is emitted: we could probably also catch that one...
    // in the case that the property panel is for the Categories Representation, this "early apply"
    // is not required (and would achieve something completely different...), so we check for
    // the existence of an "earlyApplyProp" property first
    if(nullptr != earlyApplyProp)
    {
        pqObjectBuilder* objBuilder = pqApplicationCore::instance()->getObjectBuilder();
        connect(objBuilder, SIGNAL(proxyCreated(pqProxy*)), SLOT(earlyApply(pqProxy*)));
    }
}

widCategoryValue::~widCategoryValue()
{
    delete ui;
    ui = 0;
}

void widCategoryValue::widCategoryValue::setPortId(QString const& id)
{
    portId = id.toStdString();
    emit portIdChanged();
}

QString widCategoryValue::getPortId() const
{
    return QString(portId.c_str());
}

void widCategoryValue::setCategory(QString const& cat)
{
    category = cat.toStdString();
    ui->cbCategory->setCurrentText(cat);

    // tell to whoever needs to know
    emit categoryChanged();
}

QString widCategoryValue::getCategory() const
{
    return QString(category.c_str());
}

void widCategoryValue::setValue(QString const& val)
{
    value = val.toStdString();
    ui->cbValue->setCurrentText(val);

    // tell to whoever needs to know
    emit valueChanged();
}

QString widCategoryValue::getValue() const
{
    return QString(value.c_str());
}

void widCategoryValue::refreshCategoriesValues(const std::string& id)
{
    // if the ids do not match it is not "our" item that has sent the message
    if(id != getPortId().toStdString())
        return;

    // get the data from the categories manager
    // note: if this is a first call before the id has a prefix, there will also not be
    //   any data yet - so no problem
    CatValues = atgCategoriesManager::instance().getCatValMap(id);

    // refresh the category combo box - without signalling
    if(!CatValues.empty())
    {
        ui->cbCategory->blockSignals(true);
        ui->cbCategory->clear();
        for(auto it = CatValues.begin(); it != CatValues.end(); ++it)
            ui->cbCategory->addItem(it->first.c_str());

        // set current category back - if it exists
        if(!category.empty() && (0 <= ui->cbCategory->findText(category.c_str())))
        {
            ui->cbCategory->setCurrentText(category.c_str());
            ui->cbCategory->blockSignals(false);
        }

        // otherwise switch to the first - with signalling
        else
        {
            ui->cbCategory->blockSignals(false);
            ui->cbCategory->setCurrentIndex(0);
        }

        // refresh the values combo box - without signalling
        ui->cbValue->blockSignals(true);
        ui->cbValue->clear();
        category = ui->cbCategory->currentText().toStdString();

        for(auto it = CatValues[category].begin(); it != CatValues[category].end(); ++it)
            ui->cbValue->addItem(it->c_str());

        // set current value back - if category remained and it exists
        if(!value.empty() && (0 <= ui->cbValue->findText(value.c_str())))
        {
            ui->cbValue->setCurrentText(value.c_str());
            ui->cbValue->blockSignals(false);
        }

        // otherwise switch to first, with signalling
        else
        {
            ui->cbValue->blockSignals(false);
            ui->cbValue->setCurrentIndex(0);
        }
    }

    // otherwise just clear the combo boxes
    else
    {
        ui->cbCategory->clear();
        ui->cbValue->clear();
    }

    setButtonEnableState();
}

void widCategoryValue::earlyApply(pqProxy* proxy)
{
    // this way we could get a pointer to the filter - for whatever...
    //pqPipelineSource* filter = qobject_cast<pqPipelineSource*>(proxy);

    // tell the filter's RequestData function that we want no error messages even without
    // providing required properties - yet
    earlyApplyProp->SetElements1(1);

    // we need to "hack" our way to the properties panel
    pqPropertiesPanel* propertiesPanel = getPropertiesPanel();

    // do an "early apply": this makes sure that we have some real object at the output
    // port as soon as the property widget is also ready
    // note: with this we can always refer to the "current port" regarding the selection
    propertiesPanel->apply();

    // reset the no errors property
    earlyApplyProp->SetElements1(0);

    // from now on we do not need these notifications any more
    pqObjectBuilder* objBuilder = pqApplicationCore::instance()->getObjectBuilder();
    this->disconnect(objBuilder, SIGNAL(proxyCreated(pqProxy*)), this, SLOT(earlyApply(pqProxy*)));

    // just tell that values have changed - to re-enable the Apply button
    emit valueChanged();
}

void widCategoryValue::updateCategory(const QString& cat)
{
    setCategory(cat);
}

void widCategoryValue::updateValue(QString const& val)
{
    setValue(val);
}

void widCategoryValue::adaptValues(QString const& cat)
{
    ui->cbValue->blockSignals(true);
    ui->cbValue->clear();

    auto cit = CatValues.find(cat.toStdString());
    if(cit != CatValues.end())
    {
        for(auto it = cit->second.begin(); it != cit->second.end(); ++it)
            ui->cbValue->addItem(it->c_str());
    }

    ui->cbValue->blockSignals(false);

    ui->cbValue->setCurrentIndex(0);

    // looks like the above does not trigger an updateValue slot, so we call it
    QString val = ui->cbValue->currentText();
    updateValue(val);
}

void widCategoryValue::setValueFirst()
{
    ui->cbValue->setCurrentIndex(0);
}

void widCategoryValue::setValue5Back()
{
    ui->cbValue->setCurrentIndex(std::max(0, ui->cbValue->currentIndex() - 5));
}

void widCategoryValue::setValueBack()
{
    ui->cbValue->setCurrentIndex(std::max(0, ui->cbValue->currentIndex() - 1));
}

void widCategoryValue::setValueForward()
{
    int maxi = ui->cbValue->count() - 1;
    ui->cbValue->setCurrentIndex(std::min(maxi, ui->cbValue->currentIndex() + 1));
}

void widCategoryValue::setValue5Forward()
{
    int maxi = ui->cbValue->count() - 1;
    ui->cbValue->setCurrentIndex(std::min(maxi, ui->cbValue->currentIndex() + 5));
}

void widCategoryValue::setValueLast()
{
    ui->cbValue->setCurrentIndex(ui->cbValue->count() - 1);
}

void widCategoryValue::setButtonEnableState()
{
    int cinx = ui->cbValue->currentIndex(),
        maxi = ui->cbValue->count() - 1;

    ui->pbFirst->setEnabled(cinx > 0);
    ui->pb5Back->setEnabled(cinx > 4);
    ui->pbBack->setEnabled(cinx > 0);
    ui->pbForward->setEnabled(cinx < maxi);
    ui->pb5Forward->setEnabled(cinx < (maxi - 4));
    ui->pbLast->setEnabled(cinx < maxi);
}
