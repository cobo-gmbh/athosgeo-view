/*=========================================================================

   Program: AthosGEO
   Module:  widMultiSelsAndValues.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <boost/algorithm/string.hpp>
#include <QString>
#include <QMainWindow>
#include <QModelIndex>
#include <QModelIndexList>
#include <QItemSelectionModel>
#include <QComboBox>
#include <utilFormattedToolTip.h>
#include <atgPropertiesPanel.h>
#include <widCellArrays.h>
#include <pqApplicationCore.h>
#include <pqOutputPort.h>
#include <pqPropertiesPanel.h>
#include <pqSelectionManager.h>
#include <pqSMAdaptor.h>
#include <pqActiveObjects.h>
#include <pqPipelineSource.h>
#include <pqCoreUtilities.h>
#include <pqPropertyLinks.h>
#include <pqObjectBuilder.h>
#include <pqView.h>
#include <vtkSelectionNode.h>
#include <vtkPVDataInformation.h>
#include <vtkPVDataSetAttributesInformation.h>
#include <vtkPVArrayInformation.h>
#include <vtkSMSelectionHelper.h>
#include <vtkSMVectorProperty.h>
#include <vtkSMProperty.h>
#include <vtkSMIntVectorProperty.h>
#include <vtkSMStringVectorProperty.h>
#include <vtkSMPropertyGroup.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSourceProxy.h>
#include <vtkSMDocumentation.h>
#include <modelValsAndSels.h>
#include <ui_widMultiSelsAndValues.h>
#include <widMultiSelsAndValues.h>

widMultiSelsAndValues::widMultiSelsAndValues(vtkSMProxy* smproxy,
                                             vtkSMPropertyGroup* smgroup,
                                             QWidget* parent)
:   Superclass(smproxy, smgroup, parent),
    ui(new Ui::MultiSelsAndValues()),
    outPort(pqActiveObjects::instance().activePort()),
    propertiesPanel(getPropertiesPanel()),
    activeScalarProperty(nullptr),
    smprx(smproxy),
    noErrors_(0),
    connection(new QMetaObject::Connection),
    valsSelsModel_(nullptr),
    first(std::initializer_list<bool>({true, true, true}))
{
    // button icons for the table view
    Q_INIT_RESOURCE(widMultiSelsAndValues);

    // initialize the widget
    ui->setupUi(this);

    // see if we have a documentation, to be applied in the tool tip
    vtkSMDocumentation* doc = smgroup->GetDocumentation();
    if(nullptr != doc)
    {
        utilFormattedToolTip ttip(doc->GetDescription());
        setToolTip(ttip.formattedText().c_str());
    }

    // generate the table view
    valsSelsModel_ = new modelValsAndSels();
    ui->tableViewSelsAndVals->setModel(valsSelsModel_);

    // establish the property links between Qt and VTK stuff:
    // - NoErrors is a flag for the filter (RequestData function): if it is on, the
    //   function will not emit any error messages if properties are still missing -
    //   which will always be the case during an "early apply" that just generates
    //   an output model with nothing changed yet
    // note: for the latter to happen it is important that within the "RequestData"
    //   function there is an early outGrid->ShallowCopy(inputGrid) - certainly before
    //   any check for missing parameters may end the function early!
    addPropertyLink(this, "NoErrors", SIGNAL(noErrorsChanged()), smproxy, smgroup->GetProperty("NoErrors"));

    // - NewValues is a list of values to apply to the new or existing attribute
    addPropertyLink(this, "NewValues", SIGNAL(newValuesChanged()), smproxy, smgroup->GetProperty("NewValues"));

    // - IdSets is a list of ID sets, indicating to which blocks the values should be applied
    addPropertyLink(this, "IdSets", SIGNAL(idSetsChanged()), smproxy, smgroup->GetProperty("IdSets"));

    // - LockItem is the index of the NewValues that is currently locked, i.e.
    //   it's ID's are not changeable
    addPropertyLink(this, "LockItems", SIGNAL(lockItemsChanged()), smproxy, smgroup->GetProperty("LockItems"));

    // - CurrentItem is the current item that the user is working with
    // note: LockItem and CurrentItem are just for the property panel, to keep these
    //   values from being deleted and have to be re-invented if the user comes back and
    //   the panel will be re-created with the constructor
    addPropertyLink(this, "CurrentItem", SIGNAL(currentItemChanged()), smproxy, smgroup->GetProperty("CurrentItem"));

    // see if we have an active scalar property
    // note: this can either be directly the name of an attribute, or it is the
    // name of a string attribute that contains the name of the attribute
    vtkSMStringVectorProperty* activeScalarNameProperty =
            vtkSMStringVectorProperty::SafeDownCast(smproxy->GetProperty("ActiveScalar"));
    std::string activeScalarName;
    if((nullptr != activeScalarNameProperty) &&
       (0 < activeScalarNameProperty->GetNumberOfElements()))
        activeScalarName = activeScalarNameProperty->GetElement(0);
    if(!activeScalarName.empty())
        activeScalarProperty =
                vtkSMStringVectorProperty::SafeDownCast(smproxy->GetProperty(activeScalarName.c_str()));
    if(nullptr == activeScalarProperty)
        activeScalarProperty = activeScalarNameProperty;

    // add at least one row if there is none initially
    if(1 > valsSelsModel_->rowCount())
    {
        addNewValue();
        setCurrentItem(0);
    }

    // attach selection change to proper handling
    connect(ui->tableViewSelsAndVals->selectionModel(),
            SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
            SLOT(handleChangedRow()));

    // attach the two buttons to the table view
    connect(ui->pbPlus, SIGNAL(pressed()), SLOT(addNewValue()));
    connect(ui->pbMinus, SIGNAL(pressed()), SLOT(removeNewValue()));

    // we want to listen to selection change events and immediately react accordingly
    // and update the "Ids" property
    pqSelectionManager* selMan = getSelectionManager();
    connect(selMan, SIGNAL(selectionChanged(pqOutputPort*)),
            SLOT(handleChangedSelection(pqOutputPort*)));

    // we cannot do an "apply" already here in the constructor, but we can do it after
    // the filter and proxy are properly generated by the "object builder", so we need to
    // catch the signal that tells us if this has happened
    // note: the proxyCreated signal is emitted in pqObjectBuilder::createFilter, immediately
    //   after also a filterCreated signal is emitted: we could probably also catch that one...
    pqObjectBuilder* objBuilder = pqApplicationCore::instance()->getObjectBuilder();
    *connection = connect(objBuilder, SIGNAL(proxyCreated(pqProxy*)), SLOT(earlyApply(pqProxy*)));
}

widMultiSelsAndValues::~widMultiSelsAndValues()
{
    ui->tableViewSelsAndVals->setModel(nullptr);
    delete valsSelsModel_;
    valsSelsModel_ = nullptr;
    delete ui;
    ui = nullptr;
}

void widMultiSelsAndValues::setNoErrors(int nerr)
{
    // this function has no purpose here, but it is required because NoErrors is a "property"
    noErrors_ = nerr;
    emit noErrorsChanged();
}

int widMultiSelsAndValues::getNoErrors() const
{
    // this function has no purpose here, but it is required because NoErrors is a "property"
    return noErrors_;
}

QList<QVariant> widMultiSelsAndValues::getNewValues() const
{
    QList<QVariant> vals;
    for(int row = 0; row < valsSelsModel_->rowCount(); ++row)
        vals.push_back(valsSelsModel_->value(row).c_str());
    return vals;
}

void widMultiSelsAndValues::setNewValues(QList<QVariant> const& newVals)
{
    QList<QVariant> oldVals = getNewValues();
    if(newVals == oldVals)
        return;

    // make sure the number of rows is matching
    while(newVals.size() < valsSelsModel_->rowCount())
        valsSelsModel_->removeValue(valsSelsModel_->rowCount() - 1);
    while(newVals.size() > valsSelsModel_->rowCount())
        valsSelsModel_->addValue();

    for(int row = 0; row < newVals.size(); ++row)
        valsSelsModel_->setValue(row, newVals[row].toString().toStdString());

    if(first[0])
        first[0] = false;
    else
        emit newValuesChanged();
}

QList<QVariant> widMultiSelsAndValues::getIdSets() const
{
    std::vector<modelValsAndSels::IdSet> idSets;
    for(int row = 0; row < valsSelsModel_->rowCount(); ++row)
        idSets.push_back(valsSelsModel_->sels(row));
    QList<QVariant> ids = selIdsToVarList(idSets);

    return ids;
}

void widMultiSelsAndValues::setIdSets(QList<QVariant> const& idSets)
{
    QList<QVariant> oldIdSets = getIdSets();
    if(idSets == oldIdSets)
        return;

    std::vector<modelValsAndSels::IdSet> ids = selIdsToIdSet(idSets);

    // make sure the number of rows is matching
    while(ids.size() < valsSelsModel_->rowCount())
        valsSelsModel_->removeValue(valsSelsModel_->rowCount() - 1);
    while(ids.size() > valsSelsModel_->rowCount())
        valsSelsModel_->addValue();

    for(int row = 0; row < ids.size(); ++row)
        valsSelsModel_->setSels(row, ids[row]);

    if(first[1])
        first[1] = false;
    else
        emit idSetsChanged();
}

QList<QVariant> widMultiSelsAndValues::getLockItems() const
{
    QList<QVariant> locks;
    for(int row = 0; row < valsSelsModel_->rowCount(); ++row)
        locks.push_back(valsSelsModel_->lock(row));
    return locks;
}

void widMultiSelsAndValues::setLockItems(QList<QVariant> const& lockItems)
{
    QList<QVariant> oldLockItems = getLockItems();
    if(lockItems == oldLockItems)
        return;

    // make sure the number of rows is matching
    while(lockItems.size() < valsSelsModel_->rowCount())
        valsSelsModel_->removeValue(valsSelsModel_->rowCount() - 1);
    while(lockItems.size() > valsSelsModel_->rowCount())
        valsSelsModel_->addValue();

    for(int row = 0; row < lockItems.size(); ++row)
        valsSelsModel_->setLock(row, lockItems[row].toBool());

    if(first[2])
        first[2] = false;
    else
        emit lockItemsChanged();
}

int widMultiSelsAndValues::getCurrentItem() const
{
    return selectedRow();
}

void widMultiSelsAndValues::setCurrentItem(int currentItem)
{
    int oldCurrentItem = getCurrentItem();
    if(oldCurrentItem == currentItem)
        return;

    selectRow(currentItem);
    emit currentItemChanged();
}

void widMultiSelsAndValues::selectRow(int row)
{
    QItemSelectionModel* selm = ui->tableViewSelsAndVals->selectionModel();
    selm->select(ui->tableViewSelsAndVals->model()->index(row, 0),
                 QItemSelectionModel::Rows | QItemSelectionModel::ClearAndSelect);
}

int widMultiSelsAndValues::selectedRow() const
{
    QModelIndexList sel = ui->tableViewSelsAndVals->selectionModel()->selectedRows();
    if(sel.empty())
        return -1;
    else
        return sel.front().row();
}

void widMultiSelsAndValues::showEvent(QShowEvent* event)
{
    // avoid warning
    event;

    // re-activate the selection that was active when the user was here
    propertyToActiveSelection();
}

void widMultiSelsAndValues::earlyApply(pqProxy* proxy)
{
    // this way we could get a pointer to the filter - for whatever...
    //pqPipelineSource* filter = qobject_cast<pqPipelineSource*>(proxy);

    // get the property
    vtkSMIntVectorProperty* noErrorsProp =
            vtkSMIntVectorProperty::SafeDownCast(smprx->GetProperty("NoErrors"));

    // tell the filter's RequestData function that we want no error messages even without
    // providing required properties - yet
    noErrorsProp->SetElements1(1);
    smprx->UpdateProperty("NoErrors");

    // do an "early apply": this makes sure that we have some real object at the output
    // port as soon as the property widget is also ready
    // note: with this we can always refer to the "current port" regarding the selection
    propertiesPanel->apply();

    // reset the no errors property
    noErrorsProp->SetElements1(0);
    smprx->UpdateProperty("NoErrors");

    // we want to do this only once
    QObject::disconnect(*connection);
    delete connection;
    connection = nullptr;
}

void widMultiSelsAndValues::updateActiveVariable()
{
    // try to find a cell arrays widget
    widCellArrays* cellArrays = getVariablesWidget();

    // trivial preconditions
    if((nullptr == cellArrays) ||
       (nullptr == activeScalarProperty) ||
       (0 == activeScalarProperty->GetNumberOfElements()))
        return;

    // get the name of the attribute that should be activated
    std::string activeScalar = activeScalarProperty->GetElement(0);

    // set active variable
    cellArrays->setArray(activeScalar.c_str());
}

void widMultiSelsAndValues::handleChangedSelection(pqOutputPort* port)
{
    // avoid warning
    port;

    // always apply selection changes immediately to the property
    activeSelectionToProperty();
}

void widMultiSelsAndValues::handleChangedRow()
{
    setCurrentItem(selectedRow());
    propertyToActiveSelection();
}

void widMultiSelsAndValues::addNewValue()
{
    valsSelsModel_->addValue();
    setCurrentItem(valsSelsModel_->rowCount() - 1);
}

void widMultiSelsAndValues::removeNewValue()
{
    int sel = selectedRow();
    valsSelsModel_->removeValue(selectedRow());
    setCurrentItem(std::min(sel, valsSelsModel_->rowCount() - 1));
}

void widMultiSelsAndValues::activeSelectionToProperty()
{
    // just make sure
    if(nullptr == outPort)
        return;

    // flags
    bool clearingSelection = false,  // we have no selection (selected port)
         convertedSelection = false, // the selection type was not indices
         modifiedSelection = false;  // we had to adapt because of locking

    // get the selection manager and port
    // note: getSelectedPorts() would return ALL selected ports, not only the first
    //   (would that be an advantage??)
    pqSelectionManager* selMan = getSelectionManager();
    pqOutputPort* selPort = selMan->getSelectedPort();
    vtkSMProxy* activeSelection = nullptr;

    if(nullptr == selPort)
    {
        // no output port means: clear all selections - including ours
        clearingSelection = true;
    }

    // we only want a selection that refers to our output port
    else if(selPort != outPort)
    {
        return;
    }

    // if we have an output port, we want to get the active selection
    else
    {
        // returns any active selection on the output port of this widget
        // note: it is possible that we get no active selection if the current representation
        //   cannot display cell selections, or we get a wrong type, so we have to check both!
        activeSelection = outPort->getSelectionInput();

        // if there is no active selection, this means that
        // - EITHER we are clearing the selection (1)
        // - OR we are in a view that cannot display the selection (2)
        // -> in order to never lose a selection we are now assuming that
        //    the case is always (2), so we do nothing!
        // note: the risk with this is that we cannot manage the case that the
        //   user explicitly removes the selection entirely!
        //   (but see above: we assume that in this case no selected port exists)
        if(nullptr == activeSelection)
            return;

        // we are only interested in CELL selections
        vtkSMIntVectorProperty* ftp =
                vtkSMIntVectorProperty::SafeDownCast(activeSelection->GetProperty("FieldType"));
        int ft = ftp->GetElement(0);

        if(vtkSelectionNode::CELL != ft)
            return;
    }

    // prepare collecting the new IDs:
    // - get the old ones
    // - remove the current ID set
    // - have an empty string ready for filling in the new IDs

    std::vector<modelValsAndSels::IdSet> ids = selIdsToIdSet(getIdSets());
    auto currentIdsIt = ids.begin();
    std::advance(currentIdsIt, getCurrentItem());
    currentIdsIt->clear();
    modelValsAndSels::IdSet newIds;

    // if we have a selection, we need to retrieve the IDs
    if(!clearingSelection)
    {
        // check the selection type - and make sure we have an ID selection
        vtkSMSourceProxy* asel = 0;
        std::string xmlname = activeSelection->GetXMLName();
        if(xmlname == "IDSelectionSource")
        {
            // it is already an ID selection, so we just cast it
            asel = vtkSMSourceProxy::SafeDownCast(activeSelection);
        }
        else
        {
            // any other selection needs to be converted into an ID selection
            asel = vtkSMSourceProxy::SafeDownCast(
                       vtkSMSelectionHelper::ConvertSelection(vtkSelectionNode::INDICES,
                                                              activeSelection,
                                                              selPort->getSourceProxy(),
                                                              selPort->getPortNumber()));
            convertedSelection = true;
        }

        // extract the IDs list from the selection proxy, and remove the 0-s between
        // the IDs
        // note: has to do with some multi processing facility - in which case there could
        //   also be other numbers than only 0
        QList<QVariant> idsPadded =
                pqSMAdaptor::getMultipleElementProperty(asel->GetProperty("IDs"));
        bool even = true;
        for(auto varId = idsPadded.begin(); varId != idsPadded.end(); ++varId)
        {
            if(!even)
                newIds.insert(varId->toInt());
            even = !even;
        }

        // now we must handle the case of overlaps with existing sel id sets:
        // - normally the "other" needs to be adapted
        // - if the "other" is locked, then "our" set must be adapted
        // note: for simplicity we ignore now the case that "our" set is locked
        QList<QVariant> locks = getLockItems();
        for(auto sit = ids.begin(); sit != ids.end(); ++sit)
        {
            // we do not want to handle the "current" set
            if(sit == currentIdsIt)
                continue;

            // we use set intersection to find any common ids
            std::set<int> overlap;
            std::set_intersection(newIds.begin(), newIds.end(), sit->begin(), sit->end(),
                                  std::inserter(overlap, overlap.begin()));

            // if we have an overlap...
            if(!overlap.empty())
            {
                int row = std::distance(ids.begin(), sit);
                std::set<int> diff;

                // either the "other" needs to be adapted...
                if(!locks.at(row).toBool())
                {
                    std::set_difference(sit->begin(), sit->end(), overlap.begin(), overlap.end(),
                                        std::inserter(diff, diff.begin()));
                    *sit = diff;
                }

                // ...or the "current" one - because the "other" is locked
                else
                {
                    std::set_difference(newIds.begin(), newIds.end(), overlap.begin(), overlap.end(),
                                        std::inserter(diff, diff.begin()));
                    newIds = diff;
                    modifiedSelection = true;
                }
            }
        }

        // insert the new selection IDs in the id sets
        *currentIdsIt = newIds;
    }

    // pass the id's to the proper list widget - even if the current one is empty
    setIdSets(selIdsToVarList(ids));

    // in case we had to do some changes (type, indices) we need to
    // write this back to the view(s)
    if(convertedSelection || modifiedSelection)
        propertyToActiveSelection();
}

void widMultiSelsAndValues::propertyToActiveSelection()
{
    // just make sure
    if(!outPort)
        return;

    // get the block ids from the property and extract our "current" set
    std::vector<modelValsAndSels::IdSet> idsSet = selIdsToIdSet(getIdSets());
    auto currentIdsIt = idsSet.begin();
    std::advance(currentIdsIt, getCurrentItem());
    modelValsAndSels::IdSet const& ids = *currentIdsIt;

    // if the ids list is empty, we simply clear the selection
    // note: it seems that this is important: just creating a "cell" selection with
    //   zero cell ids seems to generate unpredictable results - like having afterwards
    //   a selection with "cell 0" selected...
    if(ids.empty())
    {
        // clear any selection on "our" port
        pqSelectionManager* selMan = getSelectionManager();
        selMan->clearSelection(outPort);
    }

    // otherwise we have to generate a selection and put in the ids
    else
    {
        // insert always a 0 before each id
        // note: this is the form in which the list can be passed to the selection manager
        QList<QVariant> idsPadded;
        for(auto id = ids.begin(); id != ids.end(); ++id)
        {
            idsPadded.push_back(QVariant(0));
            idsPadded.push_back(*id);
        }

        // generate a new IDs selection source proxy object
        vtkSMSessionProxyManager* pxm = pqActiveObjects::instance().activeServer()->proxyManager();
        vtkSMSourceProxy* selSource = vtkSMSourceProxy::SafeDownCast(pxm->NewProxy("sources", "IDSelectionSource"));
        pqSMAdaptor::setElementProperty(selSource->GetProperty("FieldType"), vtkSelectionNode::CELL);
        selSource->UpdateVTKObjects();

        // add an IDs property
        vtkSMVectorProperty* vp = vtkSMVectorProperty::SafeDownCast(selSource->GetProperty("IDs"));
        pqSMAdaptor::setMultipleElementProperty(vp, idsPadded);
        selSource->UpdateVTKObjects();

        // we want to avoid a handler call that tries to do the opposite of what we are
        // doing in this function
        pqSelectionManager* selMan = getSelectionManager();
        disconnect(selMan, SIGNAL(selectionChanged(pqOutputPort*)), this,
                   SLOT(handleChangedSelection(pqOutputPort*)));

        // set the selection on our port
        outPort->setSelectionInput(selSource, 0);

        // turn it on again
        connect(selMan, SIGNAL(selectionChanged(pqOutputPort*)),
                SLOT(handleChangedSelection(pqOutputPort*)));
    }

    // tell about the change
    pqView* av = pqActiveObjects::instance().activeView();
    if(nullptr != av)
        emit av->selected(outPort);
}

pqSelectionManager* widMultiSelsAndValues::getSelectionManager()
{
    // note: "SELECTION_MANAGER" gives the same result: the thing is registered twice,
    //   with these two names
    pqSelectionManager* selMan =
            qobject_cast<pqSelectionManager*>(pqApplicationCore::instance()->manager("SelectionManager"));

    if(!selMan)
    {
        // this should not happen if the compiled PV has a selection manager at all
        std::cout << "PROGRAM ERROR: no selection manager found" << std::endl;
        return 0;
    }

    return selMan;
}

widCellArrays* widMultiSelsAndValues::getVariablesWidget()
{
    // we need access to the "Variables" combo box because we want to adapt the
    // active variable (attribute) each time after Apply was pressed
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
    QList<widCellArrays*> caList = mainWindow->findChildren<widCellArrays*>();

    // if this list is empty it looks like we do not have a Variables combo box...
    if(caList.isEmpty())
        return nullptr;

    // normally we should have only one cell arrays widget anyway, otherwise - no idea...
    return caList.front();
}

std::vector<modelValsAndSels::IdSet> widMultiSelsAndValues::selIdsToIdSet(QList<QVariant> const& sel)
{
    std::vector<modelValsAndSels::IdSet> idSets;

    for(auto it1 = sel.begin(); it1 != sel.end(); ++it1)
    {
        modelValsAndSels::IdSet ids;

        QStringList idl = it1->toString().split(",");
        for(auto it2 = idl.begin(); it2 != idl.end(); ++it2)
        {
            bool ok;
            int id = it2->toInt(&ok);
            if(ok)
                ids.insert(id);
        }

        idSets.push_back(ids);
    }

    return idSets;
}

QList<QVariant> widMultiSelsAndValues::selIdsToVarList(std::vector<modelValsAndSels::IdSet> const& sel)
{
    QList<QVariant> varl;

    for(auto it1 = sel.begin(); it1 != sel.end(); ++it1)
    {
        QString str;
        for(auto it2 = it1->begin(); it2 != it1->end(); ++it2)
        {
            if(it2 != it1->begin())
                str.append(",");
            str.append(QString("%1").arg(*it2));
        }
        varl.push_back(str);
    }

    return varl;
}
