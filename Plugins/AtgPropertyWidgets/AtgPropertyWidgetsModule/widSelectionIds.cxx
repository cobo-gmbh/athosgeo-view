/*=========================================================================

   Program: AthosGEO
   Module:  widSelectionIds.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <algorithm>
#include <QString>
#include <QMainWindow>
#include <QMessageBox>
#include <QTextStream>
#include <QFile>
#include <QAction>
#include <utilFormattedToolTip.h>
#include <utilTrackDefs.h>
#include <atgPropertiesPanel.h>
#include <pqApplicationCore.h>
#include <pqOutputPort.h>
#include <pqPropertiesPanel.h>
#include <pqSelectionManager.h>
#include <pqSMAdaptor.h>
#include <pqActiveObjects.h>
#include <pqPipelineFilter.h>
#include <pqPipelineSource.h>
#include <pqCoreUtilities.h>
#include <pqPropertyLinks.h>
#include <pqObjectBuilder.h>
#include <pqFileDialog.h>
#include <pqView.h>
#include <pqRenderView.h>
#include <pqViewFrame.h>
#include <vtkPVDataInformation.h>
#include <vtkPVDataSetAttributesInformation.h>
#include <vtkPVArrayInformation.h>
#include <vtkSelectionNode.h>
#include <vtkSMSelectionHelper.h>
#include <vtkSMVectorProperty.h>
#include <vtkSMProperty.h>
#include <vtkSMStringVectorProperty.h>
#include <vtkSMIntVectorProperty.h>
#include <vtkSMPropertyGroup.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSourceProxy.h>
#include <vtkSMDocumentation.h>
#include <vtkPVXMLElement.h>
#include <ui_widSelectionIds.h>
#include <widSelectionIds.h>

// during "early apply", this is set to the address of "this", to indicate that
// in the next call the active scalar should NOT be adapted because it is for
// initialization only
// then, during the initialization call, the value is set to "this + 1", and if
// such a value is found, the active scalar is adapted indeed
// then, after an adapation has happened, the value is set back to 0 - and nothing
// else is going to happen any more
unsigned long long widSelectionIds::initializingInstance = 0;

widSelectionIds::widSelectionIds(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup,
                                 QWidget* parent)
:   Superclass(smproxy, smgroup, parent),
    ui(new Ui::SelectionIds()),
    outPort(pqActiveObjects::instance().activePort()),
    selPort(nullptr),
    lastSelPort(nullptr),
    propertiesPanel(getPropertiesPanel()),
    activeScalarProperty(nullptr),
    noErrorsProp(vtkSMIntVectorProperty::SafeDownCast(smgroup->GetProperty("NoErrors"))),
    noErrors(0),
    connection(new QMetaObject::Connection)
{
    // currently not using resources
    //Q_INIT_RESOURCE(widSelectionIds);

    // initialize the widget
    ui->setupUi(this);

    // see if we have a documentation, to be applied in the tool tip
    vtkSMDocumentation* doc = smgroup->GetDocumentation();
    if(nullptr != doc)
    {
        utilFormattedToolTip ttip(doc->GetDescription());
        setToolTip(ttip.formattedText().c_str());
    }

    // we always need to know the current and last selected port
    pqSelectionManager* selMan = getSelectionManager();
    selPort = selMan->getSelectedPort();
    connect(selMan, SIGNAL(selectionChanged(pqOutputPort*)), SLOT(changeSelPort(pqOutputPort*)));

    // establish the property links between Qt and VTK stuff:
    // - ApplyToAll is a flag that decides whether the filter is applied to all blocks
    //   or to the selected blocks only
    // - Ids is a list of block id numbers, representing the selection
    // - NoErrors is a flag for the filter (RequestData function): if it is on, the
    //   function will not emit any error messages if properties are still missing -
    //   which will always be the case during an "early apply" that just generates
    //   an output model with nothing changes yet
    // note: for the latter to happen it is important that within the "RequestData"
    //   function there is an early outGrid->ShallowCopy(inputGrid) - certainly before
    //   any check for missing parameters may end the function early!
    addPropertyLink(ui->rbAllBlocks, "checked", SIGNAL(toggled(bool)),
                    smgroup->GetProperty("ApplyToAll"));
    addPropertyLink(this, "Ids", SIGNAL(idsChanged()), smproxy,
                    smgroup->GetProperty("Ids"));
    addPropertyLink(this, "NoErrors", SIGNAL(noErrorsChanged()), smproxy,
                    smgroup->GetProperty("NoErrors"));

    // see if we have an active scalar property, possibly with a prefix
    // note: this can either be directly the name of an attribute, or it is the
    // name of a string attribute that contains the name of the attribute
    // note: prefix is only possible in the second case
    vtkSMStringVectorProperty* activeScalarNameProperty =
            vtkSMStringVectorProperty::SafeDownCast(smproxy->GetProperty("ActiveScalar"));
    std::string activeScalarName;
    if((nullptr != activeScalarNameProperty) &&
       (0 < activeScalarNameProperty->GetNumberOfElements()))
        activeScalarName = activeScalarNameProperty->GetElement(0);
    if(!activeScalarName.empty())
        activeScalarProperty =
                vtkSMStringVectorProperty::SafeDownCast(smproxy->GetProperty(activeScalarName.c_str()));
    if(nullptr == activeScalarProperty)
    {
        activeScalarProperty = activeScalarNameProperty;
    }
    else
    {
        vtkSMStringVectorProperty* activeScalarNamePreProperty =
                vtkSMStringVectorProperty::SafeDownCast(smproxy->GetProperty("ActiveScalarPre"));
        if((nullptr != activeScalarNamePreProperty) &&
           (0 < activeScalarNamePreProperty->GetNumberOfElements()))
            activeScalarNamePre = activeScalarNamePreProperty->GetElement(0);
    }

    // needed because there is no such thing as an 'editing done' signal for checkboxes.
    setChangeAvailableAsChangeFinished(true);

    // attach the load/save selection buttons
    connect(ui->pbLoadSelection, SIGNAL(pressed()), SLOT(loadSelection()));
    connect(ui->pbSaveSelection, SIGNAL(pressed()), SLOT(saveSelection()));

    // we want to listen to selection change events and immediately react accordingly
    // and update the "Ids" property
    selMan = getSelectionManager();
    connect(selMan, SIGNAL(selectionChanged(pqOutputPort*)),
            SLOT(handleChangedSelection(pqOutputPort*)));

    // we cannot do an "apply" already here in the constructor, but we can do it after
    // the filter and proxy are properly generated by the "object builder", so we need to
    // catch the signal that tells us if this has happened
    // note: the proxyCreated signal is emitted in pqObjectBuilder::createFilter, immediately
    //   after also a filterCreated signal is emitted: we could probably also catch that one...
    pqObjectBuilder* objBuilder = pqApplicationCore::instance()->getObjectBuilder();
    *connection = connect(objBuilder, SIGNAL(proxyCreated(pqProxy*)), SLOT(earlyApply(pqProxy*)));

    // always if the apply button was pressed and has finished, we want to make sure
    // that the active variable is properly updated
    connect(propertiesPanel, SIGNAL(applied()), SLOT(updateActiveVariable()));

    // initialize the radio buttons
    vtkSMIntVectorProperty* aap = vtkSMIntVectorProperty::SafeDownCast(smgroup->GetProperty("ApplyToAll"));
    switch(aap->GetElement(0))
    {
        case 0: ui->rbSelectedBlocks->setChecked(true); break;
        default: ui->rbAllBlocks->setChecked(true); break;
    }
}

widSelectionIds::~widSelectionIds()
{
    delete ui;
    ui = nullptr;
}

void widSelectionIds::setNoErrors(int nerr)
{
    // this function has no purpose here, but it is required because NoErrors is a "property"
    noErrors = nerr;
    emit noErrorsChanged();
}

int widSelectionIds::getNoErrors() const
{
    // this function has no purpose here, but it is required because NoErrors is a "property"
    return noErrors;
}

void widSelectionIds::setIds(QList<QVariant>& ids)
{
    // do nothing if nothing has changed
    QList<QVariant> oldList = getIds();

    if(ids == oldList)
        return;

    // sort the list numerically
    std::sort(ids.begin(), ids.end(),
              [](QVariant const& id1, QVariant const& id2)->bool
    {
        return id1.toInt() < id2.toInt();
    });

    // put the data into the list
    // note: we implement here a workaround for the case that an ID is
    //   coming twice: this is a "failed toggle" and should instead result in zero
    //   times that ID in the list
    // update note: now with PV 5.6.0 it looks like this is not any more the case:
    //   still sometimes IDs are coming multiple times, mostly in the case of an
    //   overlapping "add" selection, but in such case only the duplicated need
    //   to be removed in order to get the correct number of selected blocks
    selIds.clear();
    for(auto it = ids.begin(); it != ids.end(); ++it)
    {
        // get ID value and next - if available
        // note: duplicates will be neighbors because we sorted the list above!
        auto itNext = it + 1;
        int siCurr = it->toInt(),
            siNext = (ids.end() != itNext) ? itNext->toInt() : -1;

        if(siCurr != siNext)
            // if both ids are different keep them
            selIds.push_back(siCurr);
        // not any more: just skip this one if it comes once more, thus
        // eliminating the duplicate
        //else
            // otherwise skip also the next
        //    it++;
    }

    // update the radio button text
    QString selStr = QString("%1 selected blocks").arg(selIds.size());
    ui->rbSelectedBlocks->setText(selStr);

    // tell to whoever needs to know
    emit idsChanged();
}

QList<QVariant> widSelectionIds::getIds() const
{
    QList<QVariant> ids;

    for(auto it = selIds.begin(); it != selIds.end(); ++it)
        ids.push_back(QVariant(*it));

    return ids;
}

void widSelectionIds::showEvent(QShowEvent* event)
{
    // avoid warning
    (void)event;

    // re-activate the selection that was active when the user was here
    propertyToActiveSelection();
}

void widSelectionIds::earlyApply(pqProxy* proxy)
{
    (void)proxy;

    // this way we could get a pointer to the filter - for whatever...
    //pqPipelineSource* filter = qobject_cast<pqPipelineSource*>(proxy);

    // tell the filter's RequestData function that we want no error messages even without
    // providing required properties - yet
    noErrorsProp->SetElements1(1);

    // do an "early apply": this makes sure that we have some real object at the output
    // port as soon as the property widget is also ready
    // note: with this we can always refer to the "current port" regarding the selection
    initializingInstance = reinterpret_cast<unsigned long long>(this);
    propertiesPanel->apply();

    // reset the no errors property
    noErrorsProp->SetElements1(0);

    // we want to do this only once
    QObject::disconnect(*connection);
    delete connection;
    connection = nullptr;
}

void widSelectionIds::updateActiveVariable()
{
    // we need a cast value of "this"
    unsigned long long thisCast = reinterpret_cast<unsigned long long>(this);

    // we are "hacking" our way to the variables combo box
    // note: this looks like the best way to really set the "active variable"
    //   inside of ParaView
    QComboBox* variablesComboBox = getVariablesComboBox();

    // trivial preconditions, and the case that initializingInstance is 0
    if((nullptr == variablesComboBox) ||
       (nullptr == activeScalarProperty) ||
       (0 == activeScalarProperty->GetNumberOfElements()) ||
       (0 == initializingInstance))
        return;

    // this is the initializing call for this instance: prepare for the next call
    if(initializingInstance == thisCast)
    {
        // this is the initializing call: ignore and ask to adapt the _next_ time
        initializingInstance++;
    }

    // this is the "real" call to apply, so we want to set the active scalar - and
    // then never again for this instance
    else if(initializingInstance == (thisCast + 1))
    {
        // first call after initializtion: adapt active scalar
        // get the name of the attribute that should be activated
        std::string activeScalar = activeScalarProperty->GetElement(0);
        if(!activeScalarNamePre.empty())
            activeScalar = activeScalarNamePre + activeScalar;

        // set active variable
        // note: here we actually set the active variable
        variablesComboBox->setCurrentText(activeScalar.c_str());

        // after this, we do not want to fiddle round with the active scalar any more
        initializingInstance = 0;
    }
}

void widSelectionIds::handleChangedSelection(pqOutputPort* port)
{
    // avoid warning
    (void)port;

    // always apply selection changes immediately to the property
    activeSelectionToProperty();
}

void widSelectionIds::loadSelection()
{
    // get an existing file name
    pqFileDialog* fdlg = new pqFileDialog(nullptr, this, tr("Load Selection File"),
                                          QString(), tr("Selection File (*.sel *.SEL)"));
    fdlg->setFileMode(pqFileDialog::ExistingFile);

    // remember selected file
    QString fn;
    if((QDialog::Accepted == fdlg->exec()) && !fdlg->getSelectedFiles().isEmpty())
        fn = fdlg->getSelectedFiles().at(0);
    delete fdlg;
    if(fn.isEmpty())
        return;

    QFile f(fn);
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::warning(this, tr("Warning"), tr("Could not open the file"));
        return;
    }

    QTextStream tstr(&f);
    QString line;
    if(!tstr.readLineInto(&line) || (line != "ATGSEL v100"))
    {
        f.close();
        QMessageBox::warning(this, tr("Warning"), tr("Could not read selection file header"));
        return;
    }

    QList<QVariant> sels;
    while(!line.isEmpty())
    {
        line.clear();
        tstr.readLineInto(&line);
        bool isInt;
        int id = line.toInt(&isInt);
        if(isInt)
            sels.push_back(id);
    }
    f.close();

    if(sels.isEmpty())
    {
        QMessageBox::warning(this, tr("Warning"), tr("Could not read any selection IDs from the file"));
        return;
    }

    setIds(sels);
    propertyToActiveSelection();
}

void widSelectionIds::saveSelection()
{
    if(selIds.empty())
    {
        QMessageBox::warning(this, tr("Information"), tr("No active selection can be saved"));
        return;
    }

    QString fn = pqFileDialog::getSaveFileName(nullptr, this, tr("Save Selection File to.."),
                                               QString(), tr("Selection File (*.sel *.SEL)"));
    if(fn.isEmpty())
        return;

    QFile f(fn);
    if(f.exists())
    {
        if(QMessageBox::Yes != QMessageBox::question(this, tr("Question"),
                                                     tr("This file already exists: Overwrite?"),
                                                     QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No)))
            return;
    }

    if(!f.open(QIODevice::Truncate | QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::warning(this, tr("Warning"),
                             tr("Could not create the output file"));
        return;
    }

    // write the selected IDs, after a header
    QTextStream tstr(&f);
    tstr << "ATGSEL v100" << endl;
    for(auto it = selIds.begin(); it != selIds.end(); ++it)
        tstr << *it << endl;

    f.close();
}

void widSelectionIds::changeSelPort(pqOutputPort* newSelPort)
{
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
    std::cout << "widSelectionIds::changeSelPort: "
              << reinterpret_cast<unsigned long long>(selPort)
              << " -> "
              << reinterpret_cast<unsigned long long>(newSelPort)
              << std::endl;
#endif

    lastSelPort = selPort;
    selPort = newSelPort;
}

void widSelectionIds::activeSelectionToProperty()
{
    // just make sure
    if(nullptr == outPort)
        return;

    // flags
    bool clearingSelection = false,
         convertedSelection = false;

    // our "work selected port" is either the current selected port or the last one
    // note: the latter is for the case that the selection was deleted!
    pqOutputPort* workSelPort = (nullptr != selPort) ? selPort : lastSelPort;
    vtkSMProxy* activeSelection = nullptr;

#ifdef TRACK_ACTIVE_SELECTION_CHANGED
    std::cout << "widSelectionIds::activeSelectionToProperty" << std::endl;
    std::cout << "- outPort     " << reinterpret_cast<unsigned long long>(outPort) << std::endl;
    std::cout << "- selPort     " << reinterpret_cast<unsigned long long>(selPort) << std::endl;
    std::cout << "- lastSelPort " << reinterpret_cast<unsigned long long>(lastSelPort) << std::endl;
    std::cout << "- workSelPort " << reinterpret_cast<unsigned long long>(workSelPort) << std::endl;
#endif

    if(nullptr == workSelPort)
    {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- no work selPort, so nothing to do" << std::endl;
#endif

        // neither a selected port nor a last selected port means that we have
        // REALLY nothing to do here!
        return;
    }

    // we only want a selection that refers to our output port
    else if(workSelPort != outPort)
    {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- work selPort is not output port - do nothing" << std::endl;
#endif

        return;
    }

    // if we have an output port, we want to get the active selection
    else
    {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- work selPort and outPort are matching - get active selection" << std::endl;
#endif

        // returns any active selection on the output port of this widget
        // note: it is possible that we get no active selection if the current representation
        //   cannot display cell selections, or we get a wrong type, so we have to check both!
        activeSelection = outPort->getSelectionInput();

        // if there is no active selection, this means that
        // - EITHER we are clearing the selection (1)
        // - OR we are in a view that cannot display the selection (2)
        // -> in order to never lose a selection we are now assuming that
        //    the case is always (2), so we do nothing!
        // note: the risk with this is that we cannot manage the case that the
        //   user explicitly removes the selection entirely!
        if(nullptr == activeSelection)
        {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
            std::cout << "- no active selection on output port - clearing the selection" << std::endl;
#endif

            clearingSelection = true;
        }

        else
        {
            // we are only interested in CELL selections
            int ft = -1;
            vtkSMIntVectorProperty* ftp = vtkSMIntVectorProperty::SafeDownCast(activeSelection->GetProperty("FieldType"));
            if(ftp)
            {
                ft = ftp->GetElement(0);
            }
            else
            {
                // with PV 5.10, we cannot assume any more that we always have a <FieldType> property: It is
                // not any more the case if the selection is done with the FindData docking panel (which used
                // to be a common dialog before). Now we have to derive the field type by asking for the
                // <ElementType> property and converting it to a field type
                ftp = vtkSMIntVectorProperty::SafeDownCast(activeSelection->GetProperty("ElementType"));
                if(nullptr != ftp)
                    ft = vtkSelectionNode::ConvertAttributeTypeToSelectionField(ftp->GetElement(0));
            }

#ifdef TRACK_ACTIVE_SELECTION_CHANGED
            std::cout << "- active selection:" << std::endl;
            activeSelection->PrintSelf(std::cout, vtkIndent(2));
            std::cout << "- active selection is of type ";
            switch(ft)
            {
                case vtkSelectionNode::CELL:
                    std::cout << "CELL"; break;
                case vtkSelectionNode::POINT:
                    std::cout << "POINT"; break;
                case vtkSelectionNode::FIELD:
                    std::cout << "FIELD"; break;
                case vtkSelectionNode::VERTEX:
                    std::cout << "VERTEX"; break;
                case vtkSelectionNode::EDGE:
                    std::cout << "EDGE"; break;
                case vtkSelectionNode::ROW:
                    std::cout << "ROW"; break;
                default:
                    std::cout << "<???>";
            }
            std::cout << std::endl;
#endif

            if(vtkSelectionNode::CELL != ft)
            {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
            std::cout << "- active selection on output port not a cell selection" << std::endl;
#endif

                return;
            }
        }
    }

    // this are our new IDs - empty by default
    QList<QVariant> ids;

    // if we have a selection, we need to retrieve the IDs
    if(!clearingSelection)
    {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- not clearing selection - get the ids" << std::endl;
#endif

        // check the selection type - and make sure we have an ID selection
        vtkSMSourceProxy* asel = nullptr;
        std::string xmlname = activeSelection->GetXMLName();
        if(xmlname == "IDSelectionSource")
        {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
            std::cout << "- selection is already a cell selection" << std::endl;
#endif

            // it is already an ID selection, so we just cast it
            asel = vtkSMSourceProxy::SafeDownCast(activeSelection);
        }
        else
        {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
            std::cout << "- converting selection to cell selection" << std::endl;
#endif

            // any other selection needs to be converted into an ID selection
            asel =vtkSMSourceProxy::SafeDownCast(
                      vtkSMSelectionHelper::ConvertSelection(vtkSelectionNode::INDICES,
                                                             activeSelection,
                                                             workSelPort->getSourceProxy(),
                                                             workSelPort->getPortNumber()));
            convertedSelection = true;
        }

        // extract the IDs list from the selection proxy, and remove the 0-s between
        // the IDs
        // note: has to do with some multi processing facility - in which case there could
        //   also be other numbers than only 0
        QList<QVariant> idsPadded =
                pqSMAdaptor::getMultipleElementProperty(asel->GetProperty("IDs"));
        bool even = true;
        for(auto varId = idsPadded.begin(); varId != idsPadded.end(); ++varId)
        {
            if(!even)
                ids.push_back(*varId);
            even = !even;
        }

#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- number of selected cell ids: " << ids.size() << std::endl;
#endif
    }

#ifdef TRACK_ACTIVE_SELECTION_CHANGED
    else
    {
        std::cout << "- cleaning the selection - passing empty id list to property" << std::endl;
    }
#endif

    // pass the id's to the proper list widget - even if empty
    setIds(ids);

    // change the selection type if this is a converted selection: this is needed
    // to enable e.g. manual "adaptation" of a selection from a "Find Data" operation
    // we also re-save the selection if we are having duplicate ids
    // note: the latter may happen if the user first does a query selection and then
    //   tries to go for the "interactive" selection to turn off some selected blocks:
    //   instead of "toggling" the selection of these blocks, they are selected a
    //   second time
    if(convertedSelection || (ids.size() > static_cast<int>(selIds.size())))
    {
#ifdef TRACK_ACTIVE_SELECTION_CHANGED
        std::cout << "- reading property back to selection because: ";
        if(convertedSelection)
            std::cout << "selection type was converted";
        else
            std::cout << "size of new ids list and selected ids property do not match";
        std::cout << std::endl;
#endif

        propertyToActiveSelection();
    }
}

void widSelectionIds::propertyToActiveSelection()
{
    // just make sure
    if(!outPort)
        return;

    // get the block ids from the property
    QList<QVariant> ids = getIds();

    // if the ids list is empty, we simply clear the selection
    // note: it seems that this is important: just creating a "cell" selection with
    //   zero cell ids seems to generate unpredictable results - like having afterwards
    //   a selection with "cell 0" selected...
    if(ids.empty())
    {
        // clear any selection on "our" port
        pqSelectionManager* selMan = getSelectionManager();
        selMan->clearSelection(outPort);
    }

    // otherwise we have to generate a selection and put in the ids
    else
    {
        // insert always a 0 before each id
        // note: this is the form in which the list can be passed to the selection manager
        QList<QVariant> idsPadded;
        for(auto id = ids.begin(); id != ids.end(); ++id)
        {
            idsPadded.push_back(QVariant(0));
            idsPadded.push_back(*id);
        }

        // generate a new IDs selection source proxy object
        vtkSMSessionProxyManager* pxm = pqActiveObjects::instance().activeServer()->proxyManager();
        vtkSMSourceProxy* selSource = vtkSMSourceProxy::SafeDownCast(pxm->NewProxy("sources", "IDSelectionSource"));
        pqSMAdaptor::setElementProperty(selSource->GetProperty("FieldType"), vtkSelectionNode::CELL);
        selSource->UpdateVTKObjects();

        // add an IDs property
        vtkSMVectorProperty* vp = vtkSMVectorProperty::SafeDownCast(selSource->GetProperty("IDs"));
        pqSMAdaptor::setMultipleElementProperty(vp, idsPadded);
        selSource->UpdateVTKObjects();

        // set the selection on our port - and delete the reference within this function scope
        outPort->setSelectionInput(selSource, 0);
        selSource->Delete();
    }

    // tell about the change
    pqView* av = pqActiveObjects::instance().activeView();
    if(nullptr != av)
        emit av->selected(outPort);
}

pqSelectionManager* widSelectionIds::getSelectionManager()
{
    // note: "SELECTION_MANAGER" gives the same result: the thing is registered twice,
    //   with these two names
    pqSelectionManager* selMan =
            qobject_cast<pqSelectionManager*>(pqApplicationCore::instance()->manager("SelectionManager"));

    if(!selMan)
    {
        // this should not happen if the compiled PV has a selection manager at all
        std::cout << "PROGRAM ERROR: no selection manager found" << std::endl;
        return nullptr;
    }

    return selMan;
}

QComboBox* widSelectionIds::getVariablesComboBox()
{
    // we need access to the "Variables" combo box because we want to adapt the
    // active variable (attribute) each time after Apply was pressed
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
    QList<QComboBox*> cbList = mainWindow->findChildren<QComboBox*>("Variables");

    // if this list is empty it looks like we do not have a Variables combo box...
    if(cbList.isEmpty())
        return nullptr;

    // normally we should have only one Variables combo box anyway, otherwise - no idea...
    return cbList.front();
}
