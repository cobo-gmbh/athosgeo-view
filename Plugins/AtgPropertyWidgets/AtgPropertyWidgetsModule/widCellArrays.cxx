/*=========================================================================

   Program: AthosGEO
   Module:  widCellArrays.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <boost/algorithm/string/predicate.hpp>
#include <utilNormNames.h>
#include <pqOutputPort.h>
#include <pqActiveObjects.h>
#include <pqPipelineFilter.h>
#include <pqPipelineSource.h>
#include <vtkPVDataInformation.h>
#include <vtkPVDataSetAttributesInformation.h>
#include <vtkPVArrayInformation.h>
#include <vtkSMProperty.h>
#include <vtkPVXMLElement.h>
#include <ui_widCellArrays.h>
#include <widCellArrays.h>

widCellArrays::widCellArrays(vtkSMProxy* smproxy, vtkSMProperty* smproperty,
                             QWidget* parent)
:   Superclass(smproxy, parent),
    ui(new Ui::CellArrays())
{
    // currently not using resources
    //Q_INIT_RESOURCE(widCellArrays);

    ui->setupUi(this);

    // the active port is our output port for the filter that is about to operate,
    // meaning: it will be empty as long as nobody pressed Apply
    pqOutputPort* port = pqActiveObjects::instance().activePort();
    if(nullptr == port)
        return;

    // with this we are switching to the parent, which is our input - actually the
    // unstructured grid from which we want to see the cell arrays
    port = qobject_cast<pqPipelineFilter*>(port->getSource())->getAnyInput();

    // this object contains some "lightweight" (meta) information about the data object
    vtkPVDataInformation* info = port->getDataInformation();
    vtkPVDataSetAttributesInformation* dsInfo = info->GetCellDataInformation();

    // get some "hints" from the SM XML
    vtkPVXMLElement* hints = smproperty->GetHints();

    bool doubleArraysOnly = false;
    bool idAttributes = false;
    if(nullptr != hints)
    {
        for(unsigned int n = 0; n < hints->GetNumberOfNestedElements(); ++n)
        {
            vtkPVXMLElement* el = hints->GetNestedElement(n);
            if(boost::iequals(std::string(el->GetName()), std::string("DoubleArraysOnly")))
                doubleArraysOnly = true;
            if(boost::iequals(std::string(el->GetName()), std::string("IdAttributes")))
                idAttributes = true;
        }
    }

    // fill the combo box with the existing arrays and components
    ui->comboBoxArrays->clear();
    for(vtkIdType n = 0; n < dsInfo->GetNumberOfArrays(); ++n)
    {
        // get infos and name of the array
        vtkPVArrayInformation* arrInfo = dsInfo->GetArrayInformation(n);
        QString arrName = arrInfo->GetName();

        // check the condition that was given through the "hint" (see above)
        // note: if non-double arrays are allowed, they will become double arrays
        //   if they are replaced by a new array through the filter
        if(doubleArraysOnly && (VTK_DOUBLE != arrInfo->GetDataType()))
            continue;

        if(idAttributes)
        {
            // if a string array is not in the type "category" we prepend a N_ (to bring it there)
            switch(utilNormNames::getType(arrName.toStdString()))
            {
                case utilNormNames::TY_NAME:
                case utilNormNames::TY_DATE:
                case utilNormNames::TY_CATEGORY:
                {
                    // these are already ok
                    break;
                }
                default:
                {
                    arrName = QString("N_") + arrName;
                }
            }

            // we only collect potential ID attributes
            if((utilNormNames::TY_NAME != utilNormNames::getType(arrName.toStdString())) &&
               (VTK_STRING != arrInfo->GetDataType()) &&
               (utilNormNames::BLOCKID != utilNormNames::getIndex(arrName.toStdString()).first))
                continue;
        }

        // simple one-component array
        if(1 >= arrInfo->GetNumberOfComponents())
        {
            ui->comboBoxArrays->addItem(arrName);
        }

        // multi-component array
        else
        {
            for(vtkIdType c = 0; c < arrInfo->GetNumberOfComponents(); ++c)
            {
                QString compName = arrInfo->GetComponentName(c);
                ui->comboBoxArrays->addItem(arrName + ":" + compName);
            }
        }
    }

    // make sure that changes of the array name are reflected in the Apply button
    connect(ui->comboBoxArrays, SIGNAL(editTextChanged(QString)), SIGNAL(arrayChanged()));

    // we also need a property link between Qt and VTK stuff
    // note: it looks like we must not do this before the combo box is fully filled
    //   with the array names - because otherwise the "clear" above will "kill" the
    //   restored current "Array" name in case of reading from state file!
    addPropertyLink(this, "Array", SIGNAL(arrayChanged()), smproperty);
}

widCellArrays::~widCellArrays()
{
    delete ui;
    ui = 0;
}

void widCellArrays::setArray(QString const& array)
{
    ui->comboBoxArrays->setCurrentText(array);
}

QString widCellArrays::getArray() const
{
    QString array = ui->comboBoxArrays->currentText();
    return array;
}
