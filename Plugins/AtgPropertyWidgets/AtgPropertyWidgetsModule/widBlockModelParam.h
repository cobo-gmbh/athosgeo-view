﻿/*=========================================================================

   Program: AthosGEO
   Module:  widBlockModelParam.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widBlockModelParam_h
#define widBlockModelParam_h

#include <QList>
#include <QVariant>
#include <vector>
#include <string>
#include <sstream>
#include <pqPropertyGroupWidget.h>
#include <AtgPropertyWidgetsModuleModule.h>

// we are using it here for "double", but in this form it can be easily
// adapted for other types: keep it here just in order to remember!
// note: with the eof() we make sure that the entire string is checked,
//   and the string stream reading also takes care for leading/trailing
//   whitespace
template<typename Numeric>
bool is_number(const std::string& s)
{
    Numeric n;
    return((std::istringstream(s) >> n >> std::ws).eof());
}

namespace Ui
{
class BlockModelParam;
}

class ATGPROPERTYWIDGETSMODULE_EXPORT widBlockModelParam: public pqPropertyGroupWidget
{

    Q_OBJECT
    typedef pqPropertyGroupWidget Superclass;

    // block sizes dx, dy, dz
    Q_PROPERTY(QList<QVariant> BlockSize READ getBlockSize WRITE setBlockSize NOTIFY blockSizeChanged)

public:

    widBlockModelParam(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup, QWidget* parent = 0);
    virtual ~widBlockModelParam();

    QList<QVariant> getBlockSize() const;
    void setBlockSize(QList<QVariant> const& bs);

signals:

    void blockSizeChanged();

private slots:

private:

    Q_DISABLE_COPY(widBlockModelParam)

    Ui::BlockModelParam* ui;

    // this parses a CSV that is passed in one string and cuts it into a rows vector
    // of cell vectors for further processing
    // note: we are only going to parse the first row of the CSV file
    std::vector<std::vector<std::string>> parseCsv(std::string const& csvStr, bool tabs);

};

#endif
