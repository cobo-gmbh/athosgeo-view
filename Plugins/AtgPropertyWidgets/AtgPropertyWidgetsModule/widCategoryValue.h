﻿/*=========================================================================

   Program: AthosGEO
   Module:  widCategoryValue.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widCategoryValue_h
#define widCategoryValue_h

#include <string>
#include <vector>
#include <map>
#include <pqPropertyGroupWidget.h>
#include <AtgPropertyWidgetsModuleModule.h>

class vtkSMProxy;
class vtkSMPropertyGroup;
class vtkSMIntVectorProperty;
class vtkSMStringVectorProperty;
class pqPropertiesPanel;
class pqProxy;

namespace Ui
{
    class CategoryValue;
}

class ATGPROPERTYWIDGETSMODULE_EXPORT widCategoryValue: public pqPropertyGroupWidget
{
    Q_OBJECT
    typedef pqPropertyGroupWidget Superclass;

    // list of values on/off flags and values
    Q_PROPERTY(QString PortId READ getPortId WRITE setPortId NOTIFY portIdChanged)
    Q_PROPERTY(QString Category READ getCategory WRITE setCategory NOTIFY categoryChanged)
    Q_PROPERTY(QString Value READ getValue WRITE setValue NOTIFY valueChanged)

public:

    widCategoryValue(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup, QWidget* parent = 0);
    virtual ~widCategoryValue();

    // set/get the port id
    void setPortId(QString const& id);
    QString getPortId() const;

    // set/get category
    void setCategory(QString const& cat);
    QString getCategory() const;

    // set/get value
    void setValue(const QString& val);
    QString getValue() const;

signals:

    void portIdChanged();
    void categoryChanged();
    void valueChanged();

public slots:

    // get the categories and values from the categories manager
    void refreshCategoriesValues(std::string const& id);

private slots:

    // if the proxy is created, do the "early apply" - to ensure that we have
    // some real "output port" here already, even before the "user apply": we need
    // this to be able to do the selection on it - instead on the parent object
    void earlyApply(pqProxy* proxy);

    void updateCategory(QString const& cat);
    void updateValue(QString const& val);

    // make sure the values fit with the category
    void adaptValues(QString const& cat);

    // buttons for moving values up/down
    void setValueFirst();
    void setValue5Back();
    void setValueBack();
    void setValueForward();
    void setValue5Forward();
    void setValueLast();

    // dis/enable push buttons
    void setButtonEnableState();

private:

    Q_DISABLE_COPY(widCategoryValue)

    Ui::CategoryValue* ui;

    vtkSMStringVectorProperty* portIdProp;

    std::string portId,
                category,
                value;

    typedef std::vector<std::string> CatValueVec;
    typedef std::map<std::string, CatValueVec> CatValueMap;
    CatValueMap CatValues;

    vtkSMIntVectorProperty* earlyApplyProp;

};

#endif
