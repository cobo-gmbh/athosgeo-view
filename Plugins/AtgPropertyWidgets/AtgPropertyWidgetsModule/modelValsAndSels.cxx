/*=========================================================================

   Program: AthosGEO
   Module:  modelValsAndSels.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <QModelIndex>
#include <modelValsAndSels.h>

modelValsAndSels::modelValsAndSels(QObject* parent)
:   QAbstractTableModel(parent)
{
}

modelValsAndSels::~modelValsAndSels()
{
}

int modelValsAndSels::rowCount(QModelIndex const& parent) const
{
    return valSelLocks_.size();
}

int modelValsAndSels::columnCount(QModelIndex const& parent) const
{
    return 3;
}

QVariant modelValsAndSels::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(Qt::DisplayRole != role)
        return QVariant();

    switch(orientation)
    {
        case Qt::Horizontal:
            switch(section)
            {
                case 0: return "Locked";
                case 1: return "Value";
                case 2: return "Selected";
                default: return QVariant();
            }
    }

    return QVariant();
}

QVariant modelValsAndSels::data(QModelIndex const& inx, int role) const
{
    if((3 <= inx.column()) || (valSelLocks_.size() <= inx.row()))
        return QVariant();

    switch(role)
    {
        case Qt::EditRole:
        case Qt::DisplayRole:
        {
            switch(inx.column())
            {
                case 0:
                    return "";
                case 1:
                    return valSelLocks_.at(inx.row()).value.c_str();
                case 2:
                    return (int)valSelLocks_.at(inx.row()).ids.size();
            }
            break;
        }

        case Qt::CheckStateRole:
        {
            if(inx.column() != 0)
                break;
            Qt::CheckState chk = valSelLocks_.at(inx.row()).lock ?
                                     Qt::Checked : Qt::Unchecked;
            return chk;
        }

        case Qt::TextAlignmentRole:
        {
            return Qt::AlignHCenter;
        }
    }

    return QVariant();
}

bool modelValsAndSels::setData(QModelIndex const& inx, QVariant const& val, int role)
{
    if((3 <= inx.column()) || (valSelLocks_.size() <= inx.row()))
        return true;

    switch(role)
    {
        case Qt::EditRole:
        {
            switch(inx.column())
            {
                case 1:
                    valSelLocks_.at(inx.row()).value = val.toString().toStdString();
                    emit dataChanged(inx, inx);
                    break;
            }
            break;
        }

        case Qt::CheckStateRole:
        {
            switch(inx.column())
            {
                case 0:
                {
                    bool isChecked = static_cast<Qt::CheckState>(val.toInt()) == Qt::Checked;
                    valSelLocks_.at(inx.row()).lock = isChecked;
                    emit dataChanged(inx, inx);
                    return true;
                }
            }
            break;
        }
    }

    return true;
}

Qt::ItemFlags modelValsAndSels::flags(QModelIndex const& inx) const
{
    switch(inx.column())
    {
        case 0:
            return Qt::ItemIsEnabled |
                   Qt::ItemIsUserCheckable |
                   Qt::ItemIsSelectable;
        case 1:
            return Qt::ItemIsEnabled |
                   Qt::ItemIsEditable |
                   Qt::ItemIsSelectable;
        case 2:
            return Qt::ItemIsEnabled |
                   Qt::ItemIsSelectable;
        default:
            return Qt::NoItemFlags;
    }
}

void modelValsAndSels::addValue()
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    std::string val("0.0");
    modelValsAndSels::IdSet ids;
    valSelLocks_.push_back(modelValsAndSels::ValSelLock(val, ids, false));
    endInsertRows();
}

void modelValsAndSels::removeValue(int row)
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return;
    if(valSelLocks_.size() <= 1)
        return;

    beginRemoveRows(QModelIndex(), row, row);
    auto it = valSelLocks_.begin();
    std::advance(it, row);
    valSelLocks_.erase(it);
    endRemoveRows();
}

void modelValsAndSels::setValue(int row, std::string const& val)
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return;

    valSelLocks_[row].value = val;
    emit dataChanged(index(row, 1), index(row, 1));
}

std::string modelValsAndSels::value(int row) const
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return std::string();

    return valSelLocks_[row].value;
}

void modelValsAndSels::setSels(int row, modelValsAndSels::IdSet const& sel)
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return;

    valSelLocks_[row].ids = sel;
    emit dataChanged(index(row, 2), index(row, 2));
}

modelValsAndSels::IdSet modelValsAndSels::sels(int row) const
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return IdSet();

    return valSelLocks_[row].ids;
}

void modelValsAndSels::setLock(int row, bool lock)
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return;

    valSelLocks_[row].lock = lock;
    emit dataChanged(index(row, 0), index(row, 0));
}

bool modelValsAndSels::lock(int row) const
{
    if((0 > row) || (valSelLocks_.size() <= row))
        return false;

    return valSelLocks_[row].lock;
}
