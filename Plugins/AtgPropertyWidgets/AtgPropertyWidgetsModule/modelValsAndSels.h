﻿/*=========================================================================

   Program: AthosGEO
   Module:  modelValsAndSels.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef modelValsAndSels_h
#define modelValsAndSels_h

#include <vector>
#include <set>
#include <QVariant>
#include <QAbstractTableModel>
#include <AtgPropertyWidgetsModuleModule.h>

class QModelIndex;

class ATGPROPERTYWIDGETSMODULE_EXPORT modelValsAndSels: public QAbstractTableModel
{
    Q_OBJECT

public:

    typedef std::set<int> IdSet;
    struct ValSelLock
    {
        ValSelLock(std::string const& val_,
                   IdSet const& ids_,
                   bool lock_)
        :   value(val_),
            ids(ids_),
            lock(lock_)
        {}

        std::string value;
        IdSet ids;
        bool lock;
    };

    modelValsAndSels(QObject* parent = nullptr);
    ~modelValsAndSels();

    virtual int rowCount(QModelIndex const& parent = QModelIndex()) const;
    virtual int columnCount(QModelIndex const& parent = QModelIndex()) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    virtual QVariant data(QModelIndex const& inx, int role = Qt::DisplayRole) const;
    virtual bool setData(QModelIndex const& inx, QVariant const& val, int role = Qt::EditRole);

    virtual Qt::ItemFlags flags(QModelIndex const& inx) const;

    void addValue();
    void removeValue(int row);

    void setValue(int row, std::string const& val);
    std::string value(int row) const;

    void setSels(int row, IdSet const& sel);
    IdSet sels(int row) const;

    void setLock(int row, bool lock);
    bool lock(int row) const;

private:

    std::vector<ValSelLock> valSelLocks_;

};

#endif
