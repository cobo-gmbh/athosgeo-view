﻿/*=========================================================================

   Program: AthosGEO
   Module:  widSamplingParam.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widSamplingParam_h
#define widSamplingParam_h

#include <QString>
#include <vector>
#include <string>
#include <pqPropertyGroupWidget.h>
#include <modelSamplingParam.h>
#include <AtgPropertyWidgetsModuleModule.h>

namespace Ui
{
    class SamplingParam;
}

class QWidget;
class delegateSamplingParam;

class ATGPROPERTYWIDGETSMODULE_EXPORT widSamplingParam: public pqPropertyGroupWidget
{

    Q_OBJECT
    typedef pqPropertyGroupWidget Superclass;

    // block sizes dx, dy, dz
    Q_PROPERTY(QString CollarFile READ getCollarFile WRITE setCollarFile NOTIFY collarFileChanged)
    Q_PROPERTY(QString SurveyFile READ getSurveyFile WRITE setSurveyFile NOTIFY surveyFileChanged)
    Q_PROPERTY(QString Decorations READ getDecorations WRITE setDecorations NOTIFY decorationsChanged)

public:

    widSamplingParam(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup, QWidget* parent = 0);
    virtual ~widSamplingParam();

    void updateInputTables();

    QString getCollarFile() const;
    void setCollarFile(QString const& cf);

    QString getSurveyFile() const;
    void setSurveyFile(QString const& sf);

    QString getDecorations() const;
    void setDecorations(QString const& bs);

signals:

    void collarFileChanged();
    void surveyFileChanged();
    void decorationsChanged();

private slots:

    // multiply or divide all decorator sizes by 2
    void sizeMult2();
    void sizeDiv2();

    // choose collar and survey files
    void selectCollarFile();
    void selectSurveyFile();

    // update the table colums info
    void updateTableColTable();

private:

    Q_DISABLE_COPY(widSamplingParam)

    Ui::SamplingParam* ui;

    // size all decorators by f
    void sizeDecoratorsBy(double f);

    // this parses a CSV that is passed in one string and cuts it into a rows vector
    // of cell vectors for further processing
    // note: we are only going to parse the first row of the CSV file
    std::vector<std::vector<std::string>> parseCsv(std::string const& csvStr, bool tabs);

    // get predefined decorator assignments from a settings file
    static void getDefMapFromSettings(modelSamplingParam::DecoratorDefMap& decoratorDefs);

    // update a def map from what is already defined in a table
    static void updateDefMapFromModel(modelSamplingParam* model,
                                      modelSamplingParam::DecoratorDefMap& decoratorDefs);

    // get proper delegate for the combo boxes in the decorators table
    delegateSamplingParam* getDefDelegate(bool isHoles);

    std::string assayFileName;

    modelSamplingParam* dedefModel;

};

#endif
