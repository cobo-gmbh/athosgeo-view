/*=========================================================================

   Program: AthosGEO
   Module:  widBlockModelParam.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

// trying to overcome some MS macro overloading stuff that "kills" std::min()
#define NOMINMAX

#include <iostream>
#include <fstream>
#include <set>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <QWidget>
#include <QStringList>
#include <QTableWidgetItem>
#include <QFont>
#include <QMessageBox>
#include <utilNormNames.h>
#include <utilFormattedToolTip.h>
#include <vtkSMProxy.h>
#include <vtkSMProperty.h>
#include <vtkSMPropertyGroup.h>
#include <vtkSMDocumentation.h>
#include <pqSMAdaptor.h>
#include <ui_widBlockModelParam.h>
#include <widBlockModelParam.h>

widBlockModelParam::widBlockModelParam(vtkSMProxy* smproxy,
                                       vtkSMPropertyGroup* smgroup, QWidget* parent)
:   Superclass(smproxy, smgroup, parent),
    ui(new Ui::BlockModelParam())
{
    // currently not using resources
    //Q_INIT_RESOURCE(widBlockModelParam);

    ui->setupUi(this);

    // get the file name
    std::string fname;
    vtkSMProperty* fnProp = smproxy->GetProperty("FileName");
    if(0 != fnProp)
        fname = pqSMAdaptor::getFileListProperty(fnProp).first().toStdString();

    // check if file exists
    std::ifstream ist(fname, std::ios_base::in);
    if(!ist.is_open() || ist.eof())
    {
        QString warn = QString("CSV file does not exist or is empty:\n%1")
                           .arg(fname.c_str());
        QMessageBox::warning(this, "Warning", warn);
        return;
    }

    // get a list of column headers
    std::string hdrLine,
                firstLine;
    std::getline(ist, hdrLine);
    std::getline(ist, firstLine);
    ist.close();
    std::vector<std::string> hdrs = parseCsv(hdrLine, false).front(),
                             firsts = parseCsv(firstLine, false).front();

    if(hdrs.size() != firsts.size())
    {
        QString warn = QString("Inconsistency in CSV file:\n"
                               "The header row has %1 elements, while the first data row has %2")
                       .arg(hdrs.size())
                       .arg(firsts.size());
        QMessageBox::warning(this, "Warning", warn);
    }

    // we will check if block size and/or rotation angle are given in the CSV table,
    // and also check for duplicates in the output
    std::set<std::string> blockSizeCols,
                          angleCols,
                          outHdrs,
                          dupHdrs;

    // find standard forms and types of the columns and fill into the table widget
    for(int c = 0; c < std::min(hdrs.size(), firsts.size()); ++c)
    {
        // get the name and normalize it
        std::string name = hdrs[c],
                    normName = utilNormNames::normalize(name, true);

        // split into base name and extension
        std::string::size_type colPos = normName.find(":");
        if(std::string::npos == colPos)
            colPos = normName.size();
        std::string baseName = normName.substr(0, colPos);

        // if there is a string in the first row and the type is not category, we
        // assume this is "names", thus "category" - with a N_ prepended
        std::string firstVal = firsts[c];
        boost::trim(firstVal); // not sure if the istringstream would already take care of it!?
        if(!::is_number<double>(firstVal))
        {
            if(utilNormNames::TY_CATEGORY != utilNormNames::getType(baseName))
            {
                baseName = "N_" + baseName;
                normName = "N_" + normName;
            }
        }

        // now we find the description of the type
        std::string typeDesc = utilNormNames::getTypeDesc(baseName);

        if((outHdrs.end() != outHdrs.find(normName)) && !normName.empty())
            dupHdrs.insert(normName);
        outHdrs.insert(normName);

        if(baseName == utilNormNames::getName(utilNormNames::BLOCKSIZE))
            blockSizeCols.insert(normName);
        if(baseName == utilNormNames::getName(utilNormNames::ANGLE))
            angleCols.insert(normName);

        int nextRow = ui->tableWidgetColumns->rowCount();
        ui->tableWidgetColumns->insertRow(nextRow);

        QTableWidgetItem* item = new QTableWidgetItem(name.c_str());
        ui->tableWidgetColumns->setItem(nextRow, 0, item);

        item = new QTableWidgetItem(normName.c_str());
        QFont f = item->font();
        f.setBold(true);
        item->setFont(f);
        ui->tableWidgetColumns->setItem(nextRow, 1, item);

        item = new QTableWidgetItem(typeDesc.c_str());
        f = item->font();
        f.setItalic(true);
        item->setFont(f);
        ui->tableWidgetColumns->setItem(nextRow, 2, item);
    }

    // emit a warning if there are duplicate columns
    if(!dupHdrs.empty())
    {
        QStringList warn;
        warn.push_back("The automatic column name conversion will end up with duplicate columns:");
        for(auto it = dupHdrs.begin(); it != dupHdrs.end(); ++it)
            warn.push_back((std::string(" - ") + *it).c_str());
        warn.push_back("The output block model will ignore additional columns with the same name - "
                       "even if they still appear here in the parameters widget!");

        QMessageBox::warning(this, "Warning", warn.join("\n"));
    }

    // tool tips
    utilFormattedToolTip ttSize(
        smgroup->GetProperty("BlockSize")->GetDocumentation()->GetDescription());
    ui->spDX->setToolTip(ttSize.formattedText().c_str());
    ui->spDY->setToolTip(ttSize.formattedText().c_str());
    ui->spDZ->setToolTip(ttSize.formattedText().c_str());
    ui->spAngle->setToolTip(utilFormattedToolTip(
        smgroup->GetProperty("RotAngle")->GetDocumentation()->GetDescription()).formattedText().c_str());
    setToolTip(utilFormattedToolTip(
        smgroup->GetDocumentation()->GetDescription()).formattedText().c_str());

    // connections between Qt and VTK properties
    addPropertyLink(this, "BlockSize", SIGNAL(blockSizeChanged()), smgroup->GetProperty("BlockSize"));
    addPropertyLink(ui->spAngle, "value", SIGNAL(valueChanged(double)), smgroup->GetProperty("RotAngle"));

    // make sure that changes in the widgets are reflected in the properties
    connect(ui->spDX, SIGNAL(valueChanged(double)), SIGNAL(blockSizeChanged()));
    connect(ui->spDY, SIGNAL(valueChanged(double)), SIGNAL(blockSizeChanged()));
    connect(ui->spDZ, SIGNAL(valueChanged(double)), SIGNAL(blockSizeChanged()));

    // disable block size and/or angle if they are already given in the CSV file
    if(3 <= blockSizeCols.size())
    {
        ui->spDX->setValue(0.);
        ui->spDY->setValue(0.);
        ui->spDZ->setValue(0.);
        ui->spDX->setDisabled(true);
        ui->spDY->setDisabled(true);
        ui->spDZ->setDisabled(true);
    }
    if(!angleCols.empty())
    {
        ui->spAngle->setValue(0.);
        ui->spAngle->setDisabled(true);
    }
}

widBlockModelParam::~widBlockModelParam()
{
    delete ui;
    ui = 0;
}

QList<QVariant> widBlockModelParam::getBlockSize() const
{
    QList<QVariant> bs;

    bs.push_back(ui->spDX->value());
    bs.push_back(ui->spDY->value());
    bs.push_back(ui->spDZ->value());

    return bs;
}

void widBlockModelParam::setBlockSize(QList<QVariant> const& bs)
{
    // emergency...
    if(3 > bs.size())
        return;

    ui->spDX->setValue(bs[0].toDouble());
    ui->spDY->setValue(bs[1].toDouble());
    ui->spDZ->setValue(bs[2].toDouble());

    emit blockSizeChanged();
}

std::vector<std::vector<std::string>> widBlockModelParam::parseCsv(std::string const& csvStr, bool tabs)
{
    // used to split the file in lines
    static const boost::regex linesregx("\\r\\n|\\n\\r|\\n|\\r");

    // used to split each line to tokens, assuming ',' as column separator
    static const boost::regex fieldsregx = tabs ?
                 boost::regex("\\t") :
                 boost::regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

    std::vector<std::vector<std::string>> result;

    // iterator splits data to lines
    boost::sregex_token_iterator li(csvStr.begin(), csvStr.end(), linesregx, -1);
    boost::sregex_token_iterator end;

    while(li != end)
    {
        std::string line = li->str();
        ++li;

        // split line to tokens
        boost::sregex_token_iterator ti(line.begin(), line.end(), fieldsregx, -1);
        boost::sregex_token_iterator end2;

        std::vector<std::string> row;

        while(ti != end2)
        {
            std::string token = ti->str();
            boost::trim_if(token, boost::is_any_of("\t \""));
            ++ti;
            row.push_back(token);
        }

        if(line.back() == ',')
        {
            // last character was a separator
            row.push_back("");
        }

        result.push_back(row);
    }

    return result;
}
