﻿/*=========================================================================

   Program: AthosGEO
   Module:  widSelectionIds.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widSelectionIds_h
#define widSelectionIds_h

#include <QList>
#include <QVariant>
#include <QComboBox>
#include <vector>
#include <pqPropertyGroupWidget.h>
#include <AtgPropertyWidgetsModuleModule.h>

class vtkSMProxy;
class vtkSMPropertyGroup;
class vtkSMIntVectorProperty;
class vtkSMStringVectorProperty;
class pqOutputPort;
class pqPropertiesPanel;
class pqSelectionManager;
class pqProxy;

namespace Ui
{
    class SelectionIds;
}

class ATGPROPERTYWIDGETSMODULE_EXPORT widSelectionIds: public pqPropertyGroupWidget
{
    Q_OBJECT
    typedef pqPropertyGroupWidget Superclass;

    // this is set to 1 (true) for the first initialization "apply", so the filter knows
    // that it should not complain about errors like not yet defined properties
    Q_PROPERTY(int NoErrors READ getNoErrors WRITE setNoErrors NOTIFY noErrorsChanged)

    // the list of block ids that are selected and kept as a property, to make sure
    // they are persistent parts of the pipeline items
    Q_PROPERTY(QList<QVariant> Ids READ getIds WRITE setIds NOTIFY idsChanged)

public:

    widSelectionIds(vtkSMProxy* smproxy, vtkSMPropertyGroup* smgroup, QWidget* parent = nullptr);
    virtual ~widSelectionIds();

    // handle the "NoErrors" Qt property
    void setNoErrors(int nerr);
    int getNoErrors() const;

    // handle the "Ids" Qt property
    void setIds(QList<QVariant>& ids);
    QList<QVariant> getIds() const;

protected:

    // catch this for updating the selection in the view(s)
    virtual void showEvent(QShowEvent* event);

signals:

    // handle the Qt properties
    void noErrorsChanged();
    void idsChanged();

private slots:

    // if the proxy is created, do the "early apply" - to ensure that we have
    // some real "output port" here already, even before the "user apply": we need
    // this to be able to do the selection on it - instead on the parent object
    void earlyApply(pqProxy* proxy);

    // make sure the active variable is properly updated after an apply
    void updateActiveVariable();

    // react on changes in the selection by the user
    void handleChangedSelection(pqOutputPort* port);

    // load selection ids from a file
    void loadSelection();

    // save selection ids to a file
    void saveSelection();

    // if the selection changes, remenber the current and previous selPort
    void changeSelPort(pqOutputPort* newSelPort);

private:

    Q_DISABLE_COPY(widSelectionIds)

    // copy the selected IDs from the current port to the Ids property
    void activeSelectionToProperty();

    // apply the ids Ids property to generate a selection in the current port
    void propertyToActiveSelection();

    // access to some PV items
    pqSelectionManager* getSelectionManager();
    QComboBox* getVariablesComboBox();

    // actual Qt widget
    Ui::SelectionIds* ui;

    // the output port to which the widget refers
    pqOutputPort* outPort;

    // current and last selected port
    pqOutputPort *selPort,
                 *lastSelPort;

    // the panel in which this widget appears
    pqPropertiesPanel* propertiesPanel;

    // this property contains the name of the active scalar attribute, to be set
    // after an "apply" call by the widget. The name of this property is specified
    // by the "ActiveScalar" property (which is hidden in the user interface)
    // that has to be specified in the plugin XML file.
    // note: this is a workaround for the fact that a call to "SetActiveScalar"
    // within the filter code has only once, during the first call, the effect of
    // actually showing that attribute in the renderer, but for filters that need
    // the special handling of selection ids, the first call is the "initialization
    // apply" call where the final attribute name is not yet known!
    vtkSMStringVectorProperty* activeScalarProperty;

    // this can be a prefix to be applied to the scalar name
    std::string activeScalarNamePre;

    // this property is a "flag" that we transmit to the "apply" (=RequestData) function
    // of the related filter, in order to not complain if not all properties are specified yet
    // note: the noErrors variable is only a constraint for the PV system, in order to handle
    //   the property in the normal way (making use of the property system of Qt). The
    //   noErrorsProp is required for really setting the value "on" or "off" for the filter
    vtkSMIntVectorProperty* noErrorsProp;
    int noErrors;
    QMetaObject::Connection* connection;

    // flag indicating "init apply" or "regular apply": make sure that the
    // switching to an active attribute happens not during init apply
    static unsigned long long initializingInstance;

    // list of selected ids, as known in the widget
    // note: we need to know them here, but we do not need to display
    //   the numbers for the user
    std::vector<int> selIds;
};

#endif
