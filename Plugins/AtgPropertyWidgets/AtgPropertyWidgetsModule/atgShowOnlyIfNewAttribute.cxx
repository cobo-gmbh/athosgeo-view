/*=========================================================================

   Program: AthosGEO
   Module:  atgShowOnlyIfNewAttribute.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <QComboBox>
#include <QList>
#include <pqCoreUtilities.h>
#include <pqPropertyWidget.h>
#include <vtkCommand.h>
#include <vtkSMProperty.h>
#include <vtkSMProxy.h>
#include <vtkSMUncheckedPropertyHelper.h>
#include <atgShowOnlyIfNewAttribute.h>

atgShowOnlyIfNewAttribute::atgShowOnlyIfNewAttribute(vtkPVXMLElement* config,
                                                     pqPropertyWidget* parentObject)
:   Superclass(config, parentObject)
{
    vtkSMProxy* proxy = parentObject->proxy();
    vtkSMProperty* prop = proxy ? proxy->GetProperty("TargetArrayName") : nullptr;

    // this is just a safety measure
    if(nullptr == prop)
    {
        qDebug("Could not locate property named 'TargetArrayName'. "
               "atgShowOnlyIfNewAttribute will have no effect.");
        return;
    }

    ObservedObject = prop;
    ObserverId = pqCoreUtilities::connect(prop, vtkCommand::UncheckedPropertyModifiedEvent,
                                          this, SIGNAL(visibilityChanged()));
}

atgShowOnlyIfNewAttribute::~atgShowOnlyIfNewAttribute()
{
    if(ObservedObject && ObserverId)
        ObservedObject->RemoveObserver(ObserverId);
}

bool atgShowOnlyIfNewAttribute::canShowWidget(bool show_advanced) const
{
    // find the combo box with the arrays
    pqPropertyWidget* parentObject = parentWidget();
    QList<QComboBox*> cbl = parentObject->parentWidget()->findChildren<QComboBox*>("comboBoxArrays");

    // do this only if we found such a combo box
    if(!cbl.empty())
    {
        // check if the current text in the combo box appears also in the drop down list
        QComboBox* cbTargetArray = cbl.front();
        return 0 > cbTargetArray->findText(cbTargetArray->currentText());
    }

    // default - in case something goes wrong
    return Superclass::canShowWidget(show_advanced);
}
