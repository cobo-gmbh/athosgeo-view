/*=========================================================================

   Program: AthosGEO
   Module:  delegateSamplingParam.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __delegateSamplingParam_h
#define __delegateSamplingParam_h

#include <vector>
#include <string>
#include <QStyledItemDelegate>
#include <AtgPropertyWidgetsModuleModule.h>

class QStyleOptionViewItem;
class QWidget;
class QModelIndex;
class QAbstractItemModel;

class ATGPROPERTYWIDGETSMODULE_EXPORT delegateSamplingParam: public QStyledItemDelegate
{
    Q_OBJECT

public:

    delegateSamplingParam(const std::vector<std::string>& items, QObject* parent = nullptr);
    ~delegateSamplingParam();

    virtual QWidget* createEditor(QWidget* parent, QStyleOptionViewItem const& option, QModelIndex const& index) const;
    virtual void setEditorData(QWidget* editor, QModelIndex const& index) const;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, QModelIndex const& index) const;

    std::string getDecorator(int inx) const;
    int getIndex(std::string const& decorator) const;

private:

    std::vector<std::string> items_;

};

#endif
