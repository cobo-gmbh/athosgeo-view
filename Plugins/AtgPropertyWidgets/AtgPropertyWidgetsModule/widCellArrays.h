﻿/*=========================================================================

   Program: AthosGEO
   Module:  widCellArrays.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef widCellArrays_h
#define widCellArrays_h

#include <QString>
#include <pqPropertyWidget.h>
#include <AtgPropertyWidgetsModuleModule.h>

class pqOutputPort;
class pqPropertiesPanel;
class pqSelectionManager;

namespace Ui
{
    class CellArrays;
}

class ATGPROPERTYWIDGETSMODULE_EXPORT widCellArrays: public pqPropertyWidget
{
    Q_OBJECT
    typedef pqPropertyWidget Superclass;

    // list of IDs
    Q_PROPERTY(QString Array READ getArray WRITE setArray NOTIFY arrayChanged)

public:

    widCellArrays(vtkSMProxy* smproxy, vtkSMProperty* smproperty,
                  QWidget* parent = 0);
    virtual ~widCellArrays();

    // set/get the selected array
    void setArray(QString const& array);
    QString getArray() const;

signals:

    void arrayChanged();

private:

    Q_DISABLE_COPY(widCellArrays)

    Ui::CellArrays* ui;

};

#endif
