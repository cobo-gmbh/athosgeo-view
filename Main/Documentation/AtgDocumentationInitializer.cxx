#include <iostream>
#include <AtgDocumentationInitializer.h>

#include <QObject>
#include <QtPlugin>

void athosgeoview_documentation_initialize()
{
    Q_INIT_RESOURCE(AthosGeoView_documentation);
}
