/*=========================================================================

   Program: AthosGEO View
   Module:  atgDblPointFactory.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vtkPoints.h>
#include <atgDblPointsFactory.h>

// -----------------------------------------------------------------

class vtkDblPoints: public vtkPoints
{
public:

    vtkTypeMacro(vtkDblPoints, vtkPoints);

    vtkDblPoints();
    ~vtkDblPoints() override = default;

    static vtkDblPoints* New();

private:

    vtkDblPoints(vtkDblPoints const&) = delete;
    vtkDblPoints& operator=(vtkDblPoints const&) = delete;

};

// generating a vtkPoints object with "double" precision is the only
// purpose of this derived class
vtkDblPoints::vtkDblPoints()
:   vtkPoints(VTK_DOUBLE)
{
}

vtkDblPoints* vtkDblPoints::New()
{
    VTK_STANDARD_NEW_BODY(vtkDblPoints);
}

VTK_CREATE_CREATE_FUNCTION(vtkDblPoints)

// -----------------------------------------------------------------

atgDblPointsFactory::atgDblPointsFactory()
{
    RegisterOverride("vtkPoints",
                     "vtkDblPoints",
                     "double precision vertex factory override",
                     1,
                     vtkObjectFactoryCreatevtkDblPoints);
}

atgDblPointsFactory* atgDblPointsFactory::New()
{
    atgDblPointsFactory* f = new atgDblPointsFactory;
    f->InitializeObjectBase();
    return f;
}

const char* atgDblPointsFactory::GetVTKSourceVersion()
{
    return VTK_SOURCE_VERSION;
}

const char* atgDblPointsFactory::GetDescription()
{
    return "Factory for vtkPoints objects with double precision";
}
