/*=========================================================================

   Program: AthosGEO
   Module:  atgExampleDataCopyDialog.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <QFile>
#include <QFileDialog>
#include <QStandardPaths>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QStringList>
#include <QString>

#include <vtkPVConfig.h>
#include <pqActiveObjects.h>
#include <pqEventDispatcher.h>

#include <atgFileInformation.h>
#include <atgExampleDataCopyDialog.h>
#include <ui_atgExampleDataCopyDialog.h>

atgExampleDataCopyDialog::atgExampleDataCopyDialog(QWidget* Parent)
:   QDialog(Parent),
    ui(new Ui::atgExampleDataCopyDialog())
{
    ui->setupUi(this);

    // get rid of the question mark in the title bar
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    connect(ui->pushButtonBrowse, SIGNAL(pressed()), SLOT(browseTargetDir()));
}

atgExampleDataCopyDialog::~atgExampleDataCopyDialog()
{
    delete ui;
}

void atgExampleDataCopyDialog::accept()
{
    // get the example data path and see if something is in there
    QString dataPath(atgFileInformation::GetAthosGeoExampleFilesDirectory().c_str());
    QStringList files;
    if(!dataPath.isEmpty())
        files = QDir(dataPath).entryList(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    if(files.isEmpty())
    {
        QMessageBox::information(this, "Information",
                                 "Could not find example data: It looks like they are not installed");
        return;
    }

    // get the target directory and see if it exists
    QString targetDir = ui->lineEditTargetFolder->text();
    if(targetDir.isEmpty() || !QDir(targetDir).exists())
    {
        QMessageBox::warning(this, "Warning",
                             "The target directory does not exist");
        return;
    }

    // see if it is empty, and if not ask for permit to overwrite (or cancel)
    if(!QDir(targetDir).entryList(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot).isEmpty())
    {
        if(QMessageBox::Yes != QMessageBox::question(this, "Question",
                                                     "Files found in the target directory: ok to overwrite?"))
            return;
    }

    // copy files from the example data folder to the target directory
    foreach(QString fn, files)
    {
        // full paths
        QString src = dataPath + "/" + fn,
                trg = targetDir + "/" + fn;

        // remove if file exists
        if(QFile::exists(trg))
        {
            if(!QFile::remove(trg))
            {
                QMessageBox::warning(this, "Warning",
                                     "Could not overwrite file in target directory:\n" + fn);
                return;
            }
        }

        // copy the file
        if(!QFile::copy(src, trg))
        {
            QMessageBox::warning(this, "Warning",
                                 "Could not copy file to target directory:\n" + trg);
            return;
        }
    }

    // get the state files from the resources
    QStringList stateFiles(std::initializer_list<QString>(
    {
        ":/AthosGeoView/Resources/Examples/blockmodel.pvsm",
        ":/AthosGeoView/Resources/Examples/qualities.pvsm",
        ":/AthosGeoView/Resources/Examples/optimization.pvsm",
        ":/AthosGeoView/Resources/Examples/schedule.pvsm"
    }));

    // process the state files
    foreach(QString stateFile, stateFiles)
    {
        QFile sfile(stateFile);
        if(sfile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QString xmldata(sfile.readAll());
            xmldata.replace("$ATHOSGEO_EXAMPLES_DATA", targetDir);

            QString ofname = targetDir + "/" + stateFile.split('/').last();

            QFile ofile(ofname);
            if(!ofile.open(QIODevice::Text | QIODevice::WriteOnly | QIODevice::Truncate))
            {
                QMessageBox::warning(this, "Warning",
                                     "Could not open state file for writing:\n" + ofname);
                return;
            }
            if(0 > ofile.write(xmldata.toLocal8Bit()))
            {
                QMessageBox::warning(this, "Warning",
                                     "Could not write to state file:\n" + ofname);
                return;
            }
            ofile.close();
        }

        else
        {
            QMessageBox::warning(this, "Warning",
                                 "Failed to open resource with state file data");
            return;
        }
    }

    // the use wants to "see the success"!
    QMessageBox::information(this, "Information", "Example data successfully copied to work directory");

    // what does this do? some cleanup?
    QDialog::accept();

    // the accept function only hides the dialog: we want to close it
    QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
}

void atgExampleDataCopyDialog::browseTargetDir()
{
    // find the user's documents folder
    QString docDir = QStandardPaths::locate(QStandardPaths::HomeLocation,
                                            QStandardPaths::displayName(QStandardPaths::DocumentsLocation),
                                            QStandardPaths::LocateDirectory);

    QString targetDir =
            QFileDialog::getExistingDirectory(this, "Demo Work Directory", docDir,
                                              QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    ui->lineEditTargetFolder->setText(targetDir);
}
