/*=========================================================================

   Program: AthosGEO View
   Module:  atgMenuBuilders.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <QMenu>
#include <vtkSMProxyManager.h>
#include <vtkPVConfig.h>
#include <pqSetName.h>
#include <pqDesktopServicesReaction.h>
#include <AthosGeoViewMainWindow.h>
#include <atgFileInformation.h>
#include <atgExampleVisualizationsDialogReaction.h>
#include <atgExampleDataCopyReaction.h>
#include <atgAboutDialogReaction.h>
#include <atgHelpReaction.h>
#include <atgMenuBuilders.h>

void atgMenuBuilders::buildHelpMenu(QMenu& menu, AthosGeoViewMainWindow* mainWnd)
{
    QString documentationPath(atgFileInformation::GetAthosGeoDocDirectory().c_str()),
            athosGeoGettingStartedFile = documentationPath + mainWnd->getGettingStartedDoc(),
            dataPath(atgFileInformation::GetAthosGeoExampleFilesDirectory().c_str());

    // Getting Started with AthosGeo
    new pqDesktopServicesReaction(QUrl::fromLocalFile(athosGeoGettingStartedFile),
                                  (menu.addAction(QIcon(":/pqWidgets/Icons/pdf.png"),
                                                  "Getting Started with AthosGEO")
                                                      << pqSetName("actionGettingStarted")));

    // ParaView Guide
    // online guide CURRENTLY NOT REACHABLE
    /*
    QAction* guide = menu.addAction(QIcon(":/pqWidgets/Icons/pdf.png"), "ParaView Guide");
    guide->setObjectName("actionGuide");
    guide->setShortcut(QKeySequence::HelpContents);
    QString guideURL = QString("https://www.paraview.org/paraview-downloads/"
                             "download.php?submit=Download&version=v%1.%2&type=binary&os="
                             "Sources&downloadFile=ParaViewGuide-%1.%2.%3.pdf")
                       .arg(vtkSMProxyManager::GetVersionMajor())
                       .arg(vtkSMProxyManager::GetVersionMinor())
                       .arg(vtkSMProxyManager::GetVersionPatch());
    new pqDesktopServicesReaction(QUrl(guideURL), guide);
    */

    // Help
    QAction* help = menu.addAction("Reader, Filter, and Writer Reference")
                << pqSetName("actionHelp");
    new atgHelpReaction(help);

    // Reference Manual
    QString athosgeo_url(ATHOSGEO_URL);
    new pqDesktopServicesReaction(QUrl(athosgeo_url + "/atg_docs/index.html"),
                                  (menu.addAction("AthosGeoView Reference Manual")
                                       << pqSetName("actionReferenceManual")));
    new pqDesktopServicesReaction(QUrl(athosgeo_url + "/atg_docs/AthosGeoViewUsersGuide.pdf"),
                                  (menu.addAction("AthosGeoView Reference PDF")
                                       << pqSetName("actionReferencePdf")));

    //menu.addSeparator();

    // ParaView Tutorial
    /*
    QString tutorialURL = QString("https://www.paraview.org/paraview-downloads/"
                                "download.php?submit=Download&version=v%1.%2&type=binary&os="
                                "Sources&downloadFile=ParaViewTutorial-%1.%2.%3.pdf")
                          .arg(vtkSMProxyManager::GetVersionMajor())
                          .arg(vtkSMProxyManager::GetVersionMinor())
                          .arg(vtkSMProxyManager::GetVersionPatch());
    new pqDesktopServicesReaction(QUrl(tutorialURL),
                                  (menu.addAction(QIcon(":/pqWidgets/Icons/pdf.png"),
                                                  "ParaView Tutorial")
                                                      << pqSetName("actionTutorialNotes")));
    */

    // Sandia National Labs Tutorials
    /*
    new pqDesktopServicesReaction(QUrl("http://www.paraview.org/Wiki/SNL_ParaView_4_Tutorials"),
                                  (menu.addAction("Sandia National Labs Tutorials")
                                      << pqSetName("actionSNLTutorial")));
    */

    // Example Data Sets

    // Example Cases
    new atgExampleVisualizationsDialogReaction((menu.addAction("Example Cases") << pqSetName("ExampleCases")),
                                               mainWnd);

    // Open the example data folder
    new pqDesktopServicesReaction(QUrl::fromLocalFile(dataPath),
                                  (menu.addAction(QIcon(":/pqWidgets/Icons/pqFolder32.png"),
                                                  "Open Example Data")
                                                      << pqSetName("actionOpenExampleData")));

    // Copy the example data to a user folder
    new atgExampleDataCopyReaction(menu.addAction("Copy Example Data to User Folder")
                    << pqSetName("CopyExampleData"));

    menu.addSeparator();

    // put the ParaView related stuff together into this submenu
    QMenu* pvMenu = new QMenu("ParaView", &menu);
    menu.addMenu(pvMenu);

    // ParaView Web Site
    new pqDesktopServicesReaction(QUrl("http://www.paraview.org"),
                                  (pvMenu->addAction("ParaView Web Site")
                                       << pqSetName("actionWebSite")));

    // ParaView Wiki
    new pqDesktopServicesReaction(QUrl("http://www.paraview.org/Wiki/ParaView"),
                                  (pvMenu->addAction("ParaView Wiki")
                                       << pqSetName("actionWiki")));

    // ParaView Mailing Lists
    new pqDesktopServicesReaction(QUrl("http://www.paraview.org/mailing-lists/"),
                                  (pvMenu->addAction("ParaView Mailing Lists")
                                       << pqSetName("actionMailingLists")));

    // ParaView Release Notes
    QString versionString(PARAVIEW_VERSION_FULL);
    int indexOfHyphen = versionString.indexOf('-');
    if(indexOfHyphen > -1)
        versionString = versionString.left(indexOfHyphen);
    versionString.replace('.', '-');
    new pqDesktopServicesReaction(QUrl("https://blog.kitware.com/paraview-" + versionString + "-release-notes/"),
                                  (pvMenu->addAction("ParaView Release Notes")
                                       << pqSetName("actionReleaseNotes")));

    pvMenu->addSeparator();

    // Professional Support
    /*
    new pqDesktopServicesReaction(QUrl("http://www.kitware.com/products/paraviewpro.html"),
                                  (menu.addAction("Professional Support")
                                       << pqSetName("actionProfessionalSupport")));
    */

    /*
    // Professional Training
    new pqDesktopServicesReaction(QUrl("http://www.kitware.com/products/protraining.php"),
                                  (menu.addAction("Professional Training")
                                       << pqSetName("actionTraining")));
    */

    // ParaView Online Tutorials
    new pqDesktopServicesReaction(QUrl("http://www.paraview.org/tutorials/"),
                                  (pvMenu->addAction("ParaView Online Tutorials")
                                       << pqSetName("actionTutorials")));

    // Online Blogs
    /*
    new pqDesktopServicesReaction(QUrl("https://blog.kitware.com/tag/ParaView/"),
                                  (menu.addAction("Online Blogs")
                                       << pqSetName("actionBlogs")));
    */

#if !defined(__APPLE__)
        menu.addSeparator();
#endif

    // About
    new atgAboutDialogReaction((menu.addAction("About...") << pqSetName("actionAbout")),
                               mainWnd);
}
