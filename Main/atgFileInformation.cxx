/*=========================================================================

   Program: AthosGEO
   Module:  atgFileInformation.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <vtkNew.h>
#include <vtkCommonCoreModule.h>
#include <vtkVersion.h>
#include <vtkResourceFileLocator.h>
#include <vtksys/SystemTools.hxx>
#include <atgVersionInfo.h>
#include <atgFileInformation.h>

#undef LOG_RESOURCE_PATH_SEARCH

atgFileInformation::atgFileInformation()
{
    // does nothing - only static functions
}

std::string atgFileInformation::GetAthosGeoSharedResourcesDirectory()
{
    // Look from where the symbol is exported.
#ifdef _WIN32
    auto atg_libs = vtkGetLibraryPathForSymbol(GetAthosGeoVersion);
#else
    auto atg_libs = vtkGetLibraryPathForSymbol(GetAthosGeoVersion);
    for(int i = 0; i < 3; ++i)
    {
        // we want to move from the /bin subdirectory to the level of the
        // /Release and the /Debug where we generated a /share into which
        // we are keeping the /doc and /data directories
        std::string::size_type p = atg_libs.find_last_of("/");
        if((0 < p) && (std::string::npos != p))
            atg_libs = atg_libs.substr(0, p);
    }
#endif

#ifdef LOG_RESOURCE_PATH_SEARCH
    std::cout << "ATG Libs <" << atg_libs << ">" << std::endl;
#endif

    // NOTE - NOT WORKING ON LINUX!
    // The point is that dlsym() only works on functions in shared libraries, not if they are
    // in the main executable
    //std::cout << "atgFileInformation::GetAthosGeoSharedResourcesDirectory" << std::endl;
    //std::cout << "TRIED TO FIND SharedResourcesDirectory: <" << atg_libs << ">" << std::endl;
    //std::cout << "VTK version " << GetVTKVersion() << std::endl;
    //auto vtk_libs = vtkGetLibraryPathForSymbol(GetVTKVersion);
    //std::cout << "VTK version method <" << vtk_libs << ">" << std::endl;

    // Where docs might be in relation to the executable
    std::vector<std::string> prefixes =
    {
#if defined(_WIN32) || defined(__APPLE__)
        ".."
#else
        "share"
        //"share/atg-" ATHOSGEO_VERSION_FULL
#endif
    };

    // Search for the docs directory
    vtkNew<vtkResourceFileLocator> locator;
    auto resource_dir = locator->Locate(atg_libs, prefixes, "doc");

#ifdef LOG_RESOURCE_PATH_SEARCH
    std::cout << "Resource dir (1) <" << resource_dir << ">" << std::endl;
#endif

    if(!resource_dir.empty())
    {
        resource_dir = vtksys::SystemTools::CollapseFullPath(resource_dir);
    }

#ifdef LOG_RESOURCE_PATH_SEARCH
    std::cout << "Resource dir (2) <" << resource_dir << ">" << std::endl;
#endif

    return resource_dir;
}

std::string atgFileInformation::GetAthosGeoExampleFilesDirectory()
{
    return atgFileInformation::GetAthosGeoSharedResourcesDirectory() + "/data";
}

std::string atgFileInformation::GetAthosGeoDocDirectory()
{
    return atgFileInformation::GetAthosGeoSharedResourcesDirectory() + "/doc";
}
