/*=========================================================================

   Program: AthosGEO View
   Module:  atgVtkAdaptableCodec.cxx

   Copyright (c) 2024 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <stdexcept>
#include <QTextCodec>
#include <vtkObjectFactory.h>
#include <vtkTextCodecFactory.h>
#include <atgVtkAdaptableCodec.h>

vtkStandardNewMacro(atgVtkAdaptableCodec);

// this is the Qt codec that is specified within the class, in the
// NextUTF32CodePoint() function
QTextCodec* atgVtkAdaptableCodec::codec = nullptr;

char const* atgVtkAdaptableCodec::Name()
{
    return "adaptable";
}

bool atgVtkAdaptableCodec::CanHandle(char const* NameStr)
{
    return vtkTextCodec::CanHandle(NameStr) || (0 == strcmp(NameStr, "adaptable"));
}

void atgVtkAdaptableCodec::setCodec(char const* qtCodecName)
{
    codec = QTextCodec::codecForName(qtCodecName);
}

void atgVtkAdaptableCodec::setCodecLocale()
{
    codec = QTextCodec::codecForLocale();
}

vtkTypeUInt32 atgVtkAdaptableCodec::NextUTF32CodePoint(istream& InputStream)
{
    // fill the special character set if it is empty
    if(nullptr == codec)
    {
        // this would be the "normal" case: go for the locale of the user
        // note: This solution is not really satisfactory - which is probably the
        //   reason why it is not implemented like that in VTK/ParaView: The effect is
        //   that CSV files from other countries with different locale will be displayed
        //   with "strange" characters
        codec = QTextCodec::codecForLocale();

        // this is only implemented for Linux systems (for which the software is so far
        // not published) where the locale codec is always utf-8: It replaces the coded
        // in such a case by "western Europe" because this is what would often be
        // received by customers from this region, and utf-8 is supported anyway by the
        // standard already
        if(0 == QString(codec->name()).compare("utf-8", Qt::CaseInsensitive))
            setCodec("windows-1252");
    }

    // collecting bytes if we are dealing with a multi-byte character set
    QByteArray ba;

    while(true)
    {
        // get one byte, and if we cannot deliver another byte, transformation has failed
        vtkTypeInt32 cp = InputStream.get();
        if(InputStream.eof())
        {
            throw std::runtime_error("End of Input");
        }

        ba.append(cp);
        QString str = codec->toUnicode(ba);

        // if the character turns out to be "Symbol_Other", this is not a valid character,
        // but some kind of symbolic question mark that signifies "not possible - need more bytes..."
        // (more precisely: Qt will deliver the code 0xFFFD in that case that is displayed as a
        // question mark inside a vertical rhomboid)
        if(QChar::Symbol_Other != str.at(0).category())
        {
            return str.at(0).unicode();
        }
    }
}

atgVtkAdaptableCodec::atgVtkAdaptableCodec() = default;

atgVtkAdaptableCodec::~atgVtkAdaptableCodec() = default;

void atgVtkAdaptableCodec::PrintSelf(ostream& os, vtkIndent indent)
{
    os << indent << "atgVtkAdaptableCodec (" << this << ") \n";
    indent = indent.GetNextIndent();
    Superclass::PrintSelf(os, indent.GetNextIndent());
}
