/*=========================================================================

   Program: AthosGEO View
   Module:  atgExampleVisualizationsDialog.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iterator>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QString>
#include <QStringList>
#include <QPushButton>
#include <QIcon>
#include <QLabel>
#include <vtkPVConfig.h>
#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqEventDispatcher.h>
#include <pqLoadStateReaction.h>
#include <atgFileInformation.h>
#include <atgExampleVisualizationsDialog.h>
#include <ui_atgExampleVisualizationsDialog.h>

class atgExampleVisualizationsDialog::ExampleInfo
{
public:

    ExampleInfo(QPushButton* button,
                QString const& stateResource,
                QStringList const& neededFiles)
    :   button_(button),
        state_(stateResource),
        needed_(neededFiles)
    {}

    QPushButton* button_;
    QString state_;
    QStringList needed_;

};

atgExampleVisualizationsDialog::atgExampleVisualizationsDialog(QWidget* Parent)
:   QDialog(Parent),
    ui(new Ui::atgExampleVisualizationsDialog())
{
    ui->setupUi(this);

    // sometimes we need to test...
    //std::cout << "atgExampleVisualizationsDialog" << std::endl;
    //std::cout << "- LD_LIBRARY_PATH <" << ::getenv("LD_LIBRARY_PATH") << ">" << std::endl;

    // get rid of the question mark in the title bar
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

atgExampleVisualizationsDialog::~atgExampleVisualizationsDialog()
{
    for(auto it = info.begin(); it != info.end(); ++it)
        delete *it;
    info.clear();

    delete ui;
}

void atgExampleVisualizationsDialog::setTitle(QString const& title)
{
    ui->labelTitle->setText(title);
}

void atgExampleVisualizationsDialog::addExample(QString const& imageResource,
                                                QString const& caption,
                                                QString const& stateResource,
                                                QStringList const& neededFiles)
{
    int row = info.size(),
        col = row % 2;
    row = 2 * (row / 2);

    QPushButton* button = new QPushButton(QIcon(imageResource), "", this);
    button->setIconSize(QSize(300, 225));
    ui->gridLayoutExamples->addWidget(button, row, col);
    ui->gridLayoutExamples->addWidget(new QLabel(caption, this), row + 1, col);

    connect(button, SIGNAL(pressed()), SLOT(onButtonPressed()));

    info.push_back(new ExampleInfo(button, stateResource, neededFiles));

    // adapt to size of new items
    resize(0, 0);
}

void atgExampleVisualizationsDialog::onButtonPressed()
{
    // find out which button was pressed
    QPushButton* senderButton = qobject_cast<QPushButton*>(sender());
    int btnId = -1;
    for(auto it = info.begin(); it != info.end(); ++it)
    {
        if((*it)->button_ == senderButton)
        {
            btnId = std::distance(info.begin(), it);
            break;
        }
    }

    if(0 > btnId)
    {
        qCritical("No example file for button");
        return;
    }

    ExampleInfo const* exInfo = info.at(btnId);
    QString dataPath(atgFileInformation::GetAthosGeoExampleFilesDirectory().c_str());

    if(!exInfo->needed_.empty())
    {
        // check if the data path exists
        QFileInfo infoDataPath(dataPath);
        bool dataMissing = !infoDataPath.isDir();

        for(auto it = exInfo->needed_.begin(); !dataMissing && (it != exInfo->needed_.end()); ++it)
        {
            QFileInfo infoFile(dataPath + "/" + *it);
            dataMissing = !infoFile.exists();
        }

        if(dataMissing)
        {
            QString msg("Sorry, your installation doesn't have datasets to load the example cases");

            // dump to cout for easy copy/paste.
            std::cout << msg.toUtf8().data() << std::endl;

            QMessageBox::warning(this, "Missing data", msg, QMessageBox::Ok);
            return;
        }

        dataPath = infoDataPath.absoluteFilePath();
    }

    hide();

    // close the dialog immediately
//    QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);

    QFile qfile(exInfo->state_);
    if(qfile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox box(this);
        box.setText("Loading example visualization, please wait ...");
        box.setStandardButtons(QMessageBox::NoButton);
        box.show();

        // without this, the message box doesn't paint properly.
        pqEventDispatcher::processEventsAndWait(100);

        QString xmldata(qfile.readAll());
        xmldata.replace("$ATHOSGEO_EXAMPLES_DATA", dataPath);
        pqApplicationCore::instance()->loadStateFromString(
                    xmldata.toUtf8().data(), pqActiveObjects::instance().activeServer());

        // This is needed since XML state currently does not save active view.
        pqLoadStateReaction::activateView();
    }

    else
    {
        qCritical("Failed to open resource");
    }
}
