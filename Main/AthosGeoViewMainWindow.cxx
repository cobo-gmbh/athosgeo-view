/*=========================================================================

   Program: AthosGEO View
   Module:  AthosGeoViewMainWindow.cxx

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMessageBox>
#include <QTextCodec>
#include <QHelpEngine>
#include <QtDebug>
#include <vtksys/SystemTools.hxx>
#include <vtkCommand.h>
#include <vtkProcessModule.h>
#include <vtkObjectFactory.h>
#include <vtkTextCodecFactory.h>
#include <vtkSMSettings.h>
#include <vtkPVConfig.h>
#include <vtkPVGeneralSettings.h>
#include <vtkPVPlugin.h>
#include <vtkPVPluginTracker.h>
#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>
#include <pqDeleteReaction.h>
#include <pqOptions.h>
#include <pqSaveStateReaction.h>
#include <pqSettings.h>
#include <pqPluginManager.h>
#include <pqTimer.h>
#include <pqWelcomeDialog.h>
#include <pqHelpReaction.h>
#include <pqPythonShell.h>
#include <pqDesktopServicesReaction.h>
#include <pqMainWindowEventManager.h>
#include <pqMainControlsToolbar.h>
#include <pqParaViewBehaviors.h>
#include <pqParaViewMenuBuilders.h>
#include <vtkRemotingCoreConfiguration.h>
#include <utilDebugLog.h>
#include <utilTrackDefs.h>
#include <atgFileInformation.h>
#include <atgMenuBuilders.h>
#include <atgExampleVisualizationsDialog.h>
#include <atgWelcomeDialog.h>
#include <atgDblPointsFactory.h>
#include <atgVtkAdaptableCodec.h>
#include <atgAdaptRepresentationsBehaviour.h>
#include <AtgDocumentationInitializer.h>
#include <AthosGeoViewPlugins.h>
#include <AthosGeoViewMainWindow.h>
#include <ui_AthosGeoViewMainWindow.h>

#ifdef TRACK_AVAILABLE_RESOURCES
    #include <QDirIterator>
#endif

extern "C"
{
    // this seems to be still the "good old magic" from PV 5.6--
    void vtkPVInitializePythonModules();
}

// Convert a plugin name to its library name i.e. add platform specific
// library prefix and suffix.
// note: taken over directly from vtkPVPluginTracker.cxx
static std::string GetPluginFileNameFromName(std::string const& pluginname)
{
#if defined(_WIN32) && !defined(__CYGWIN__)
  return pluginname + ".dll";
#else
  // starting with ParaView 5.7, we are building .so's even on macOS
  // since they are built as "add_library(.. MODULE)" which by default generates
  // `.so`s which seems to be the convention.
  return pluginname + ".so";
#endif
}

static vtkTextCodec* atgVtkAdaptableCodecFromCallback()
{
    return atgVtkAdaptableCodec::New();
}

class AthosGeoViewMainWindow::atgvInternals
{
public:

    atgvInternals();
    ~atgvInternals();

    bool FirstShow;
    int CurrentGUIFontSize;
    QFont DefaultApplicationFont; // will be initialized to default app font in constructor.
    pqTimer UpdateFontSizeTimer;

#ifdef TRACK_INTO_DEBUG_FILE
    utilDebugLog* debugLogger;
#endif
};

AthosGeoViewMainWindow::atgvInternals::atgvInternals()
:   FirstShow(true),
    CurrentGUIFontSize(0)
#ifdef TRACK_INTO_DEBUG_FILE
    // this constructor redirects std::cout into a file
    , debugLogger(new utilDebugLog())
#endif
{
    UpdateFontSizeTimer.setInterval(0);
    UpdateFontSizeTimer.setSingleShot(true);
}

AthosGeoViewMainWindow::atgvInternals::~atgvInternals()
{
#ifdef TRACK_INTO_DEBUG_FILE
    delete debugLogger;
#endif
}

AthosGeoViewMainWindow::AthosGeoViewMainWindow(bool initMenusTools, const QStringList& excludePlugins)
:   ui(new Ui::pqClientMainWindow),
    internals(new atgvInternals())
{
    // Initialize Python use.
    vtkPVInitializePythonModules();

    // Initialize Paraview documentation.
    Q_INIT_RESOURCE(AthosGeoViewMainWindow);
    athosgeoview_documentation_initialize();

#ifdef TRACK_AVAILABLE_RESOURCES
    QDirIterator it(":", QDirIterator::Subdirectories);
    while(it.hasNext())
        std::cout << "-- RES <" << it.next().toStdString() << ">" << std::endl;
#endif

    // Setup the GUI.
    ui->setupUi(this);
    ui->outputWidgetDock->hide();

    ui->pythonShellDock->hide();
    pqPythonShell* shell = new pqPythonShell(this);
    shell->setObjectName("pythonShell");
    ui->pythonShellDock->setWidget(shell);

    // Show the output widget if an error message needs to be displayed.
    connect(ui->outputWidget, SIGNAL(messageDisplayed(QString, int)),
            SLOT(handleMessage(QString, int)));

    // Setup default GUI layout.
    setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    tabifyDockWidget(ui->colorMapEditorDock, ui->memoryInspectorDock);
    tabifyDockWidget(ui->colorMapEditorDock, ui->timeInspectorDock);
    tabifyDockWidget(ui->colorMapEditorDock, ui->comparativePanelDock);
    tabifyDockWidget(ui->colorMapEditorDock, ui->collaborationPanelDock);
    tabifyDockWidget(ui->colorMapEditorDock, ui->lightInspectorDock);
    tabifyDockWidget(ui->colorMapEditorDock, ui->findDataDock);

    ui->selectionDisplayDock->hide();
    ui->findDataDock->hide();
    ui->animationViewDock->hide();
    ui->statisticsDock->hide();
    ui->comparativePanelDock->hide();
    ui->collaborationPanelDock->hide();
    ui->memoryInspectorDock->hide();
    ui->multiBlockInspectorDock->hide();
    ui->colorMapEditorDock->hide();
    ui->timeInspectorDock->hide();
    ui->lightInspectorDock->hide();

    tabifyDockWidget(ui->animationViewDock, ui->statisticsDock);
    tabifyDockWidget(ui->animationViewDock, ui->outputWidgetDock);
    tabifyDockWidget(ui->animationViewDock, ui->pythonShellDock);

    // setup properties dock
    tabifyDockWidget(ui->propertiesDock, ui->viewPropertiesDock);
    tabifyDockWidget(ui->propertiesDock, ui->displayPropertiesDock);
    tabifyDockWidget(ui->propertiesDock, ui->informationDock);
    tabifyDockWidget(ui->propertiesDock, ui->multiBlockInspectorDock);

    // other than in standard ParaView, we keep the properties panels separate -
    // in order to avoid a real "monster panel". However, if the user really wants, he
    // can change this behaviour with an option - that has to be respected here
    vtkSMSettings* settings = vtkSMSettings::GetInstance();
    int propertiesPanelMode = settings->GetSettingAsInt(".settings.GeneralSettings.PropertiesPanelMode",
                                                        vtkPVGeneralSettings::ALL_SEPARATE);
    switch(propertiesPanelMode)
    {
        case vtkPVGeneralSettings::SEPARATE_DISPLAY_PROPERTIES:
        {
            delete ui->viewPropertiesPanel;
            delete ui->viewPropertiesDock;
            ui->viewPropertiesPanel = nullptr;
            ui->viewPropertiesDock = nullptr;
            ui->propertiesPanel->setPanelMode(
                        pqPropertiesPanel::SOURCE_PROPERTIES | pqPropertiesPanel::VIEW_PROPERTIES);
            break;
        }

        case vtkPVGeneralSettings::SEPARATE_VIEW_PROPERTIES:
        {
            delete ui->displayPropertiesPanel;
            delete ui->displayPropertiesDock;
            ui->displayPropertiesPanel = nullptr;
            ui->displayPropertiesDock = nullptr;
            ui->propertiesPanel->setPanelMode(
                        pqPropertiesPanel::SOURCE_PROPERTIES | pqPropertiesPanel::DISPLAY_PROPERTIES);
            break;
        }

        case vtkPVGeneralSettings::ALL_SEPARATE:
        {
            ui->propertiesPanel->setPanelMode(pqPropertiesPanel::SOURCE_PROPERTIES);
            break;
        }

        case vtkPVGeneralSettings::ALL_IN_ONE:
        default:
        {
            delete ui->viewPropertiesPanel;
            delete ui->viewPropertiesDock;
            ui->viewPropertiesPanel = nullptr;
            ui->viewPropertiesDock = nullptr;
            delete ui->displayPropertiesPanel;
            delete ui->displayPropertiesDock;
            ui->displayPropertiesPanel = nullptr;
            ui->displayPropertiesDock = nullptr;
            break;
        }
    }

    // update UI when font size changes.
    vtkPVGeneralSettings* gsSettings = vtkPVGeneralSettings::GetInstance();
    pqCoreUtilities::connect(
                gsSettings, vtkCommand::ModifiedEvent,
                &internals->UpdateFontSizeTimer, SLOT(start()));
    connect(&internals->UpdateFontSizeTimer, SIGNAL(timeout()), SLOT(updateFontSize()));

    ui->propertiesDock->show();
    ui->propertiesDock->raise();

    // Enable help from the properties panel.
    QObject::connect(ui->propertiesPanel,
                     SIGNAL(helpRequested(QString, QString)),
                     SLOT(showHelpForProxy(QString, QString)));

    // hook delete to pqDeleteReaction.
    QAction* tempDeleteAction = new QAction(this);
    pqDeleteReaction* handler = new pqDeleteReaction(tempDeleteAction);
    handler->connect(ui->propertiesPanel,
                     SIGNAL(deleteRequested(pqProxy*)),
                     SLOT(deleteSource(pqProxy*)));

    // setup color editor
    // note: Provide access to the color-editor panel for the application.
    pqApplicationCore::instance()->registerManager("COLOR_EDITOR_PANEL",
                                                   ui->colorMapEditorDock);

    // Provide access to the find data panel for the application.
    pqApplicationCore::instance()->registerManager("FIND_DATA_PANEL",
                                                   ui->findDataDock);

    // make sure we are notified if views and representations are changing
    new atgAdaptRepresentationsBehaviour(this);

    // menus and tools
    if(initMenusTools)
        buildAthosGeoViewMenusTools();

    // load the AthosGeo provided plugins
    // note: at least for the AtgRenderView this has to happen before the behaviours are
    //   generated because it is defined as the standard view below, so it has to exist
    loadPlugins(getAtgListOfPluginsForLoading(), excludePlugins);

    // object factory for generating vtkPoints with double precision
    // throughout the entire application (!)
    atgDblPointsFactory* ptsFactory = atgDblPointsFactory::New();
    vtkObjectFactory::RegisterFactory(ptsFactory);
    ptsFactory->Delete();

    // make sure that AtgRenderView is the default - if not explicitly set otherwise
    if(!settings->HasSetting(".settings.GeneralSettings.DefaultViewType"))
        settings->AddCollectionFromString("{ \"settings\" : { "
                                          "\"GeneralSettings\" : { "
                                          "\"DefaultViewType\" : \"AtgRenderView\" } } }",
                                          10.0);

    // change the number representation of the scalar bar widget
    // note: this refers to the numbers at top and bottom of the color legend bar in the render views
    settings->AddCollectionFromString("{ \"representations\" : { "
                                      "\"ScalarBarWidgetRepresentation\" : { "
                                      "\"RangeLabelFormat\" : \"%.2f\"} } }",
                                      10.0);

    // Final step, define application behaviors. Since we want all ParaView
    // behaviors, we use this convenience method.
    // note: again - if we called this constructor from a derived class constructor,
    //   that derived constructor must define the behaviours at the end!
    if(initMenusTools)
        new pqParaViewBehaviors(this, this);

    // we need some more codecs for reading CSV files
    initializeCodecs();
}

AthosGeoViewMainWindow::~AthosGeoViewMainWindow()
{
    delete internals;
    delete ui;
}

void AthosGeoViewMainWindow::insertAboutLicenseInformation(QTextEdit* licText)
{
    // note: at this point licText contains the Open Source license text for
    //   AthosGEO View, so nothing else to do here.
    //   A derived application can add own licensing text if required.
}

QString AthosGeoViewMainWindow::getSplashResource()
{
    return ":/AthosGeoView/Resources/Logo/AthosGeoViewSplash.png";
}

QString AthosGeoViewMainWindow::getGettingStartedDoc()
{
    return "/AthosGEO View First Steps.pdf";
}

void AthosGeoViewMainWindow::initializeExampleVisualizations(atgExampleVisualizationsDialog* dlg)
{
    // add example for block model with topo
    QStringList neededFilesBlockModel(std::initializer_list<QString>(
    {
        "blockmodel.csv", // only needed for Getting Started guide
        "blockmodel.vtu",
        "dtm.dxf",
        "aerial.png",
        "aerial.pgw"
    }));
    dlg->addExample(":/AthosGeoView/Resources/Examples/blockmodel.png",
                    "View the Demo Block Model",
                    ":/AthosGeoView/Resources/Examples/blockmodel.pvsm",
                    neededFilesBlockModel);

    // add example for qualities
    QStringList neededFilesQualities(std::initializer_list<QString>(
    {
        "blockmodel.csv", // only needed for Getting Started guide
        "blockmodel.vtu",
        "dtm.dxf",
        "aerial.png",
        "aerial.pgw"
    }));
    dlg->addExample(":/AthosGeoView/Resources/Examples/qualities.png",
                    "Qualities inside Demo Block Model",
                    ":/AthosGeoView/Resources/Examples/qualities.pvsm",
                    neededFilesQualities);
}

void AthosGeoViewMainWindow::loadPlugins(QStringList const& plugins,
                                         QStringList const& excludePlugins)
{
    QString plugin_xml = "<Plugins>";
    foreach(const QString plugin_name, plugins)
    {
        // plugins explicitly selected for not loading
        if(excludePlugins.end() != std::find(excludePlugins.begin(), excludePlugins.end(), plugin_name))
            continue;

        // note: we are not any more providing the plugin name explicitly,
        //   so the PV search mechanisms can do their job. This turned out to be
        //   a problem first with PV5.8, but now also the optional PV plugins are
        //   found if a path to them is provided in the PV_PLUGIN_PATH environment
        //   of the development system
        plugin_xml.append(QString("<Plugin name=\"%1\" auto_load=\"1\" />\n")
                          .arg(plugin_name));
    }
    plugin_xml.append("</Plugins>\n");
    vtkPVPluginTracker::GetInstance()->LoadPluginConfigurationXMLFromString(plugin_xml.toLatin1().data());

#ifdef TRACK_PLUGIN_LOADING
    std::cout << "plugin loading XML:" << std::endl;
    std::cout << plugin_xml.toStdString() << std::endl;
    vtkPVPluginTracker* tracker = vtkPVPluginTracker::GetInstance();
    for(unsigned int cc = 0; cc < tracker->GetNumberOfPlugins(); cc++)
    {
        if(tracker->GetPluginLoaded(cc))
        {
            vtkPVPlugin* plg = tracker->GetPlugin(cc);
            std::cout << "PLUGIN <" << plg->GetPluginName() << ">" << std::endl;
            std::cout << "description <" << plg->GetDescription() << ">" << std::endl;
            std::vector<std::string> plgres;
            plg->GetBinaryResources(plgres);
            for(size_t cc = 0; cc < plgres.size(); ++cc)
            {
                std::string const& rn = plgres[cc];
                std::cout << "resource " << cc << " size " << rn.size() << std::endl;
            }
        }
    }
#endif
}

void AthosGeoViewMainWindow::initializeCodecs()
{
    // this initializes 3 codecs:
    // - ASCII (all <= 127)
    // - UTF-8
    // - UTF-16
    vtkTextCodecFactory::Initialize();

    // with this we add the AthosGEO "adaptable" codec to the list: it will be chosen
    // if all others are failing, and it will be specified within the
    // atgVtkAdaptableCodec class
    vtkTextCodecFactory::RegisterCreateCallback(atgVtkAdaptableCodecFromCallback);
}

void AthosGeoViewMainWindow::buildAthosGeoViewMenusTools()
{
    // Populate application menus with actions.
    pqParaViewMenuBuilders::buildFileMenu(*ui->menu_File);
    QList<QAction*> fileActions = ui->menu_File->actions();
    static QList<QString> fileNotNeeded(std::initializer_list<QString>(
    {
        "Import Cinema Image Database...",
        "&Connect...",
        "&Disconnect",
        "Save State...",
        "Save Catalyst State..."
    }));
    static QList<QString> fileAddIcon(std::initializer_list<QString>(
    {
        "&Load State...",
        "&Save State..."
    }));
    static QList<QString> fileMenuIcon(std::initializer_list<QString>(
    {
        ":/AthosGeoView/Resources/Icons/pqOpenState.svg",
        ":/AthosGeoView/Resources/Icons/pqSaveState.svg"
    }));
    QList<QAction*> loadSaveStateAction(
                std::initializer_list<QAction*>({(QAction*)0, (QAction*)0}));
    foreach(QAction* fa, fileActions)
    {
        QString txt = fa->text();
        foreach(QString const& fnn, fileNotNeeded)
        {
            if(txt == fnn)
                ui->menu_File->removeAction(fa);
        }
        for(int n = 0; n < fileAddIcon.size(); ++n)
        {
            QString const& fai = fileAddIcon[n];
            if(txt == fai)
            {
                fa->setIcon(QIcon(fileMenuIcon[n]));
                loadSaveStateAction[n] = fa;
            }
        }
    }

    // Populate edit menu.
    pqParaViewMenuBuilders::buildEditMenu(*ui->menu_Edit, ui->propertiesPanel);

    // Populate sources and filters menus.
    pqParaViewMenuBuilders::buildSourcesMenu(*ui->menuSources, this);
    pqParaViewMenuBuilders::buildFiltersMenu(*ui->menuFilters, this);

    // Populate extractors menu.
    pqParaViewMenuBuilders::buildExtractorsMenu(*ui->menuExtractors, this);

    // Populate Tools menu.
    pqParaViewMenuBuilders::buildToolsMenu(*ui->menuTools);

    // Populate Catalyst menu.
    // We do not need this for AthosGEO
    //pqParaViewMenuBuilders::buildCatalystMenu(*ui->menu_Catalyst);

    // setup the context menu for the pipeline browser.
    pqParaViewMenuBuilders::buildPipelineBrowserContextMenu(*ui->pipelineBrowser->contextMenu());

    // Toolbar setup must come here in between the menu setup calls.
    pqParaViewMenuBuilders::buildToolbars(*this);

    // replace the connect/disconnect actions by load/save state
    pqMainControlsToolbar* mainTb = findChild<pqMainControlsToolbar*>();
    if(nullptr != mainTb)
    {
        QObjectList cl = mainTb->children();
        QAction* saveExtractAction = nullptr;
        foreach(QObject* obj, cl)
        {
            QAction* a = qobject_cast<QAction*>(obj);
            if(nullptr == a)
                continue;
            QString txt = a->text();
            if(txt == "Save Extracts...")
                saveExtractAction = a;
            foreach(QString const& fnn, fileNotNeeded)
            {
                if(txt == fnn)
                    mainTb->removeAction(a);
            }
        }
        if(nullptr != saveExtractAction)
        {
            foreach(QAction* a, loadSaveStateAction)
                mainTb->insertAction(saveExtractAction, a);
            mainTb->insertSeparator(saveExtractAction);
        }
    }

    // Setup the View menu.
    // note: This must be setup after all toolbars and dockwidgets have been created.
    pqParaViewMenuBuilders::buildViewMenu(*ui->menu_View, *this);

    // Setup the menu to show macros.
    pqParaViewMenuBuilders::buildMacrosMenu(*ui->menu_Macros);

    // Setup the help menu.
    atgMenuBuilders::buildHelpMenu(*ui->menu_Help, this);
}

void AthosGeoViewMainWindow::dragEnterEvent(QDragEnterEvent* evt)
{
    pqApplicationCore::instance()->getMainWindowEventManager()->dragEnterEvent(evt);
}

void AthosGeoViewMainWindow::dropEvent(QDropEvent* evt)
{
    pqApplicationCore::instance()->getMainWindowEventManager()->dropEvent(evt);
}

void AthosGeoViewMainWindow::showEvent(QShowEvent* evt)
{
    Superclass::showEvent(evt);

    if(internals->FirstShow)
    {
        internals->FirstShow = false;
        pqApplicationCore* core = pqApplicationCore::instance();
        if(!vtkRemotingCoreConfiguration::GetInstance()->GetDisableRegistry())
        {
            if(core->settings()->value("GeneralSettings.ShowWelcomeDialog", true).toBool())
            {
                pqTimer::singleShot(1000, this, SLOT(showWelcomeDialog()));
            }
            updateFontSize();
        }
    }

    pqApplicationCore::instance()->getMainWindowEventManager()->showEvent(evt);

}

void AthosGeoViewMainWindow::closeEvent(QCloseEvent* evt)
{
    pqApplicationCore::instance()->getMainWindowEventManager()->closeEvent(evt);
}

bool AthosGeoViewMainWindow::isFirstShow()
{
    return internals->FirstShow;
}

void AthosGeoViewMainWindow::showHelpForProxy(QString const& groupname, QString const& proxyname)
{
    pqHelpReaction::showProxyHelp(groupname, proxyname);
}

void AthosGeoViewMainWindow::showWelcomeDialog()
{
    atgWelcomeDialog* dlg = new atgWelcomeDialog(this);
    dlg->setSplashResource(getSplashResource());

    connect(dlg, SIGNAL(gettingStartedGuideClicked()), SLOT(displayGettingStartedGuide()));
    connect(dlg, SIGNAL(openDemoDataClicked()), SLOT(openDemoDataFolder()));
    connect(dlg, SIGNAL(exampleVisualizationsClicked()), SLOT(openExampleVisualizationsDialog()));

    dlg->exec();
    delete dlg;
}

void AthosGeoViewMainWindow::handleMessage(QString const& msg, int type)
{
    QDockWidget* dock = ui->outputWidgetDock;

    if(!dock->isVisible() &&
       (type == QtCriticalMsg || type == QtFatalMsg || type == QtWarningMsg))
    {
        // if dock is not visible, we always pop it up as a floating dialog. This
        // avoids causing re-renders which may cause more errors and more confusion.
        QRect rectApp = geometry();
        QRect rectDock(QPoint(0, 0),
                       QSize(static_cast<int>(rectApp.width() * 0.4), dock->sizeHint().height()));
        rectDock.moveCenter(QPoint(rectApp.center().x(), rectApp.bottom() - dock->sizeHint().height() / 2));
        dock->setFloating(true);
        dock->setGeometry(rectDock);
        dock->show();
    }

    if(dock->isVisible())
        dock->raise();
}

void AthosGeoViewMainWindow::updateFontSize()
{
    vtkPVGeneralSettings* gsSettings = vtkPVGeneralSettings::GetInstance();
    int fontSize = internals->DefaultApplicationFont.pointSize();

    if(gsSettings->GetGUIOverrideFont() && gsSettings->GetGUIFontSize() > 0)
    {
        fontSize = gsSettings->GetGUIFontSize();
    }

    if(fontSize <= 0)
    {
        qDebug() << "Ignoring invalid font size: " << fontSize;
        return;
    }

    if(internals->CurrentGUIFontSize != fontSize)
    {
        QFont newFont = font();
        newFont.setPointSize(fontSize);
        setFont(newFont);
        internals->CurrentGUIFontSize = fontSize;
    }

    // Console font size
    pqPythonShell* shell = qobject_cast<pqPythonShell*>(ui->pythonShellDock->widget());
    shell->setFontSize(fontSize);
    pqOutputWidget* outputWidget =
            qobject_cast<pqOutputWidget*>(ui->outputWidgetDock->widget());
    outputWidget->setFontSize(fontSize);
}

void AthosGeoViewMainWindow::displayGettingStartedGuide()
{
    QString documentationPath(atgFileInformation::GetAthosGeoDocDirectory().c_str());
    QString paraViewGettingStartedFile = documentationPath + getGettingStartedDoc();
    QUrl gettingStartedURL = QUrl::fromLocalFile(paraViewGettingStartedFile);

    if(pqDesktopServicesReaction::openUrl(gettingStartedURL))
    {
        // close the dialog immediately -> no why? The user might want to open also the demo data folder!
        //QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
    }
}

void AthosGeoViewMainWindow::openDemoDataFolder()
{
    QString dataPath(atgFileInformation::GetAthosGeoExampleFilesDirectory().c_str());
    QUrl dataPathURL = QUrl::fromLocalFile(dataPath);

    if(pqDesktopServicesReaction::openUrl(dataPathURL))
    {
        // close the dialog immediately -> no why? The user might want to open also the getting started guide!
        //QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
    }
}

void AthosGeoViewMainWindow::openExampleVisualizationsDialog()
{
    atgExampleVisualizationsDialog* exampleDlg = new atgExampleVisualizationsDialog(this);
    initializeExampleVisualizations(exampleDlg);
    exampleDlg->setModal(true);
    exampleDlg->setAttribute(Qt::WA_DeleteOnClose);

    // show the example visualizations
    exampleDlg->exec();
}
