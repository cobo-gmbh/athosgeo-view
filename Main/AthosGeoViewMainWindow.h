/*=========================================================================

   Program: AthosGEO View
   Module:  AthosGeoViewMainWindow.h

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#ifndef AthosGeoViewMainWindow_h
#define AthosGeoViewMainWindow_h

#include <QMainWindow>

class QStringList;
class QShowEvent;
class QCloseEvent;
class QTextEdit;
class atgExampleVisualizationsDialog;

namespace Ui
{
    class pqClientMainWindow;
}

class AthosGeoViewMainWindow: public QMainWindow
{
    Q_OBJECT
    typedef QMainWindow Superclass;

public:

    // the initMenusTools constructor is "true" by default - which is the case
    // that indeed a main window for AthosGEO View is being built. However, if
    // the constructor was subclassed for an extended variant, the constructor
    // call has to be set to false, and menus and tools have to be initialized only
    // later in the derived constructor. This is necessary because otherwise the
    // order of initializiations is not correct
    AthosGeoViewMainWindow(bool initMenusTools = true, QStringList const& excludePlugins = QStringList());
    ~AthosGeoViewMainWindow();

    virtual void insertAboutLicenseInformation(QTextEdit* licText);
    virtual QString getSplashResource();
    virtual QString getGettingStartedDoc();
    virtual void initializeExampleVisualizations(atgExampleVisualizationsDialog* dlg);

protected:

    Ui::pqClientMainWindow* ui;

    // load a list of plugins: happens in the base class and can be repeated in
    // derived classes
    void loadPlugins(QStringList const& plugins,
                     QStringList const& excludePlugins = QStringList());

    // initialize codecs and add some more
    void initializeCodecs();

    // to be called in the constructor of derived classes, after calling the parent
    // constructor with parameter 'false'
    void buildAthosGeoViewMenusTools();

    void dragEnterEvent(QDragEnterEvent* evt) override;
    void dropEvent(QDropEvent* evt) override;
    void showEvent(QShowEvent* evt) override;
    void closeEvent(QCloseEvent* evt) override;

    // true if first show event
    bool isFirstShow();

protected slots:

    void showHelpForProxy(QString const& groupname, QString const& proxyname);
    void showWelcomeDialog();
    void handleMessage(QString const&, int);
    void updateFontSize();

public slots:

    void displayGettingStartedGuide();
    void openDemoDataFolder();
    void openExampleVisualizationsDialog();

private:

    Q_DISABLE_COPY(AthosGeoViewMainWindow)

    class atgvInternals;
    atgvInternals* internals;

signals:

    void applicationClosing();

};

#endif
