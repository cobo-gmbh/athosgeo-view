/*=========================================================================

   Program: AthosGEO View
   Module:  atgWelcomeDialog.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <QString>
#include <vtkSMProxy.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMSessionProxyManager.h>
#include <pqDesktopServicesReaction.h>
#include <pqSettings.h>
#include <pqServer.h>
#include <pqApplicationCore.h>
#include <atgWelcomeDialog.h>
#include <ui_atgWelcomeDialog.h>

atgWelcomeDialog::atgWelcomeDialog(QWidget* parent)
:   QDialog(parent),
    ui(new Ui::atgWelcomeDialog())
{
    ui->setupUi(this);

    // get rid of the question mark in the title bar
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    connect(ui->DoNotShowAgainButton, SIGNAL(stateChanged(int)), SLOT(onDoNotShowAgainStateChanged(int)));
    connect(ui->GettingStartedGuideButton, SIGNAL(pressed()), SIGNAL(gettingStartedGuideClicked()));
    connect(ui->OpenDemoDataButton, SIGNAL(pressed()), SIGNAL(openDemoDataClicked()));
    connect(ui->ExampleVisualizationsButton, SIGNAL(pressed()), SIGNAL(exampleVisualizationsClicked()));
}

atgWelcomeDialog::~atgWelcomeDialog()
{
    delete ui;
}

void atgWelcomeDialog::setSplashResource(QString const& res)
{
    ui->labelSplash->setPixmap(res);

    // adapt to minimum size including the splash label
    resize(0, 0);
}

void atgWelcomeDialog::onDoNotShowAgainStateChanged(int state)
{
    bool showDialog = (state != Qt::Checked);

    pqSettings* settings = pqApplicationCore::instance()->settings();
    settings->setValue("GeneralSettings.ShowWelcomeDialog", showDialog ? 1 : 0);

    pqServer* server = pqApplicationCore::instance()->getActiveServer();
    if(!server)
    {
      qCritical("No active server available!");
      return;
    }

    vtkSMSessionProxyManager* pxm = server->proxyManager();
    if(!pxm)
    {
      qCritical("No proxy manager!");
      return;
    }

    vtkSMProxy* proxy = pxm->GetProxy("settings", "GeneralSettings");
    if (proxy)
    {
      vtkSMPropertyHelper(proxy, "ShowWelcomeDialog").Set(showDialog ? 1 : 0);
    }
}
