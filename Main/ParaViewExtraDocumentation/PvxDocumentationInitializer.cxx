#include <iostream>
#include <PvxDocumentationInitializer.h>

#include <QObject>
#include <QtPlugin>

void paraviewextra_documentation_initialize()
{
    Q_INIT_RESOURCE(ParaViewExtra_documentation);
}
