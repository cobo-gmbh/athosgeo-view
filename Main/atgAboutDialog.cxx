/*=========================================================================

   Program: AthosGEO View
   Module:  atgAboutDialog.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <QtTestingConfigure.h>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QApplication>
#include <QFile>
#include <QHeaderView>
#include <QTextEdit>
#include <sstream>
#include <pqApplicationCore.h>
//#include <pqCoreConfiguration.h>
#include <pqServer.h>
#include <pqServerManagerModel.h>
#include <pqServerResource.h>
#include <vtkNew.h>
#include <vtkPVConfig.h>
#include <vtkPVOpenGLInformation.h>
#include <vtkPVPythonInformation.h>
#include <vtkPVRenderingCapabilitiesInformation.h>
#include <vtkPVServerInformation.h>
#include <vtkProcessModule.h>
//#include <vtkRemotingCoreConfiguration.h>
//#include <vtkSMPTools.h>
#include <vtkSMProxyManager.h>
#include <vtkSMSession.h>
#include <vtkSMViewProxy.h>
#include <atgVersionInfo.h>
#include <atgFileInformation.h>
#include <atgAboutDialog.h>
#include "ui_atgAboutDialog.h"

atgAboutDialog::atgAboutDialog(QWidget* Parent)
:   QDialog(Parent),
    ui(new Ui::atgAboutDialog())
{
    ui->setupUi(this);
    setObjectName("atgAboutDialog");

    // get rid of the question mark in the title bar
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QString splashImage = QString(":/%1/%1_splash").arg(QApplication::applicationName());
    if(QFile::exists(splashImage))
        ui->Image->setPixmap(QPixmap(splashImage));

    // get extra information and put it in
    ui->VersionLabel->setText(
                QString("<html><b>Version: <i>%1</i></b></html>")
                .arg(QString(::GetAthosGeoVersion()) +
                     ", release " +
                     QString(__DATE__) +
                     " " +
                     QString(__TIME__)));
    AddClientInformation();
}

atgAboutDialog::~atgAboutDialog()
{
    delete ui;
}

QTextEdit* atgAboutDialog::licenseInfo()
{
    return ui->LicenseInformation;
}

QTreeWidget* atgAboutDialog::clientInfo()
{
    return ui->ClientInformation;
}

inline void addItem(QTreeWidget* tree, const QString& key, const QString& value)
{
    QTreeWidgetItem* item = new QTreeWidgetItem(tree);
    item->setText(0, key);
    item->setText(1, value);
}

inline void addItem(QTreeWidget* tree, const QString& key, int value)
{
    addItem(tree, key, QString("%1").arg(value));
}

void atgAboutDialog::AddClientInformation()
{
    // note that this "client information" from ParaView is now the "System Information"
    QTreeWidget* tree = ui->ClientInformation;

    addItem(tree, "ParaView Version", QString(PARAVIEW_VERSION_FULL));
    addItem(tree, "Qt Version", QT_VERSION_STR);
    addItem(tree, "vtkIdType size", QString("%1bits").arg(8 * sizeof(vtkIdType)));

    vtkNew<vtkPVPythonInformation> pythonInfo;
    pythonInfo->CopyFromObject(nullptr);

    addItem(tree, "Embedded Python", pythonInfo->GetPythonSupport() ? "On" : "Off");
    if(pythonInfo->GetPythonSupport())
    {
        addItem(tree, "Python Library Path", QString::fromStdString(pythonInfo->GetPythonPath()));
        addItem(tree, "Python Library Version", QString::fromStdString(pythonInfo->GetPythonVersion()));

        addItem(tree, "Python Numpy Support", pythonInfo->GetNumpySupport() ? "On" : "Off");
        if(pythonInfo->GetNumpySupport())
        {
            addItem(tree, "Python Numpy Path", QString::fromStdString(pythonInfo->GetNumpyPath()));
            addItem(tree, "Python Numpy Version", QString::fromStdString(pythonInfo->GetNumpyVersion()));
        }

        addItem(tree, "Python Matplotlib Support", pythonInfo->GetMatplotlibSupport() ? "On" : "Off");
        if(pythonInfo->GetMatplotlibSupport())
        {
            addItem(tree, "Python Matplotlib Path", QString::fromStdString(pythonInfo->GetMatplotlibPath()));
            addItem(tree, "Python Matplotlib Version",
            QString::fromStdString(pythonInfo->GetMatplotlibVersion()));
        }
    }

    // AthosGEO: ignore the following

//#if defined(QT_TESTING_WITH_PYTHON)
//    addItem(tree, "Python Testing", "On");
//#else
//    addItem(tree, "Python Testing", "Off");
//#endif

//#if defined(PARAVIEW_USE_MPI)
//    addItem(tree, "MPI Enabled", "On");
//#else
//    addItem(tree, "MPI Enabled", "Off");
//#endif

    //auto coreConfig = vtkRemotingCoreConfiguration::GetInstance();
    //auto pqConfig = pqCoreConfiguration::instance();
    //::addItem(tree, "Disable Registry", coreConfig->GetDisableRegistry() ? "On" : "Off");
    //::addItem(tree, "Test Directory", QString::fromStdString(pqConfig->testDirectory()));
    //::addItem(tree, "Data Directory", QString::fromStdString(pqConfig->dataDirectory()));

    //::addItem(tree, "SMP Backend", vtkSMPTools::GetBackend());
    //::addItem(tree, "SMP Max Number of Threads", vtkSMPTools::GetEstimatedNumberOfThreads());

    // For local OpenGL info, we ask Qt, as that's more truthful anyways.
    QOpenGLContext* ctx = QOpenGLContext::currentContext();
    if(QOpenGLFunctions* f = ctx ? ctx->functions() : nullptr)
    {
        const char* glVendor = reinterpret_cast<const char*>(f->glGetString(GL_VENDOR));
        const char* glRenderer = reinterpret_cast<const char*>(f->glGetString(GL_RENDERER));
        const char* glVersion = reinterpret_cast<const char*>(f->glGetString(GL_VERSION));
        addItem(tree, "OpenGL Vendor", glVendor);
        addItem(tree, "OpenGL Version", glVersion);
        addItem(tree, "OpenGL Renderer", glRenderer);
    }
    tree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
}
