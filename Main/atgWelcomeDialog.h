/*=========================================================================

   Program: AthosGEO
   Module:  atgWelcomeDialog.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#ifndef _atgWelcomeDialog_h
#define _atgWelcomeDialog_h

#include <QDialog>

namespace Ui
{
    class atgWelcomeDialog;
}

class QString;
class QTreeWidget;
class pqServer;
class AthosGeoViewMainWindow;

// note: PQCOMPONENTS_EXPORT is only for "real" ParaView modules!
class atgWelcomeDialog: public QDialog
{
    Q_OBJECT
    typedef QDialog Superclass;

public:

    atgWelcomeDialog(QWidget* parent = 0);
    ~atgWelcomeDialog() override;

    void setSplashResource(QString const& res);

signals:

    void gettingStartedGuideClicked();
    void openDemoDataClicked();
    void exampleVisualizationsClicked();

protected slots:

    void onDoNotShowAgainStateChanged(int state);

private:

    Q_DISABLE_COPY(atgWelcomeDialog)
    Ui::atgWelcomeDialog* ui;

};

#endif
