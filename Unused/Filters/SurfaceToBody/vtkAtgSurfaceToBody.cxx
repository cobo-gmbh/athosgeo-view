/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSurfaceToBody.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vtkSmartPointer.h>
#include <vtkPolyLine.h>
#include <vtkTriangleFilter.h>
#include <vtkCellArray.h>
#include <vtkPolyDataNormals.h>
#include <vtkAppendPolyData.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkTriangleStrip.h>
#include <vtkPoints.h>
#include <vtkType.h>
#include <vtkPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgSurfaceToBody.h>

vtkStandardNewMacro(vtkAtgSurfaceToBody)

vtkAtgSurfaceToBody::vtkAtgSurfaceToBody()
:   BottomLevel(0.),
    RandomVar(.1)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgSurfaceToBody::~vtkAtgSurfaceToBody()
{
}

int vtkAtgSurfaceToBody::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: a topo surface
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        case 1:
            // secondary input - mandatory: topo surface boundary (closed polygon)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgSurfaceToBody::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: topo with a closed body below
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgSurfaceToBody::RequestData(vtkInformation* request,
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
    // get the input topo surface and boundary
    vtkPolyData *inSurface = vtkPolyData::GetData(inputVector[0]),
                *inBound = vtkPolyData::GetData(inputVector[1]);

    // get the output table
    vtkPolyData* outBody = vtkPolyData::GetData(outputVector);

    // generate the curtain
    vtkPolyLine* curtainPLine = vtkPolyLine::SafeDownCast(inBound->GetCell(0));
    vtkIdType numCurtainPoints = 2 * curtainPLine->GetNumberOfPoints();

    vtkSmartPointer<vtkPoints> curtainPoints = vtkSmartPointer<vtkPoints>::New(),
                               capPoints = vtkSmartPointer<vtkPoints>::New();
    curtainPoints->SetDataType(VTK_DOUBLE);
    capPoints->SetDataType(VTK_DOUBLE);
    vtkSmartPointer<vtkTriangleStrip> curtainStrip = vtkSmartPointer<vtkTriangleStrip>::New();
    curtainStrip->GetPointIds()->SetNumberOfIds(numCurtainPoints);
    vtkSmartPointer<vtkIdList> capIds = vtkSmartPointer<vtkIdList>::New();

    double pt[3];
    for(vtkIdType sid = 0; sid < curtainPLine->GetNumberOfPoints(); ++sid)
    {
        vtkIdType pid = curtainPLine->GetPointId(sid);
        inBound->GetPoint(pid, pt);
        curtainPoints->InsertNextPoint(pt);
        pt[2] = BottomLevel;
        curtainPoints->InsertNextPoint(pt);
        curtainStrip->GetPointIds()->SetId(2 * sid, 2 * sid);
        curtainStrip->GetPointIds()->SetId(2 * sid + 1, 2 * sid + 1);
        capPoints->InsertNextPoint(pt);
        capIds->InsertNextId(sid);
    }

    vtkSmartPointer<vtkCellArray> curtainCell = vtkSmartPointer<vtkCellArray>::New();
    curtainCell->InsertNextCell(curtainStrip);

    vtkSmartPointer<vtkPolyData> curtainSurface = vtkSmartPointer<vtkPolyData>::New();
    curtainSurface->SetPoints(curtainPoints);
    curtainSurface->SetStrips(curtainCell);

    vtkSmartPointer<vtkPolyDataNormals> curtainNorms = vtkSmartPointer<vtkPolyDataNormals>::New();
    curtainNorms->SetInputData(curtainSurface);
    curtainNorms->AutoOrientNormalsOff();
    curtainNorms->ComputeCellNormalsOn();
    curtainNorms->ComputePointNormalsOff();
    curtainNorms->Update();

    vtkSmartPointer<vtkAppendPolyData> topoWithCurtain = vtkSmartPointer<vtkAppendPolyData>::New();
    topoWithCurtain->SetInputData(inSurface);
    topoWithCurtain->AddInputData(curtainNorms->GetOutput());
    topoWithCurtain->Update();

    // create cap polyline
    // note: this crashes if an empty cell array is not set as "lines" before inserting the cell
    vtkSmartPointer<vtkCellArray> capLines = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkPolyData> capPLines = vtkSmartPointer<vtkPolyData>::New();
    capPLines->SetPoints(capPoints);
    capPLines->SetLines(capLines);
    capPLines->InsertNextCell(VTK_POLY_LINE, capIds);

    // change the polyline into a polygon
    vtkSmartPointer<vtkPolyData> capPoly = vtkSmartPointer<vtkPolyData>::New();
    capPoly->SetPoints(capPLines->GetPoints());
    capPoly->SetPolys(capPLines->GetLines());

    vtkSmartPointer<vtkPolyDataNormals> capNorms = vtkSmartPointer<vtkPolyDataNormals>::New();
    capNorms->SetInputData(capPoly);
    capNorms->AutoOrientNormalsOff();
    capNorms->ComputeCellNormalsOn();
    capNorms->ComputePointNormalsOff();
    capNorms->FlipNormalsOn();
    capNorms->Update();

    vtkSmartPointer<vtkAppendPolyData> topoCurtainCap = vtkSmartPointer<vtkAppendPolyData>::New();
    topoCurtainCap->SetInputData(topoWithCurtain->GetOutput());
    topoCurtainCap->AddInputData(capNorms->GetOutput());
    topoCurtainCap->Update();

    vtkSmartPointer<vtkTriangleFilter> tri = vtkSmartPointer<vtkTriangleFilter>::New();
    tri->SetInputData(topoCurtainCap->GetOutput());
    tri->PassLinesOff();
    tri->PassVertsOff();
    tri->Update();

    outBody->ShallowCopy(tri->GetOutput());

    return 1;
}

int vtkAtgSurfaceToBody::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgSurfaceToBody::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
