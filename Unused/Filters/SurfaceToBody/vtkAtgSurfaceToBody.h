/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSurfaceToBody.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgSurfaceToBody_h
#define vtkAtgSurfaceToBody_h

#include <vtkPolyDataAlgorithm.h>
#include <atgviewutils_export.h>

class ATGVIEWUTILS_EXPORT vtkAtgSurfaceToBody: public vtkPolyDataAlgorithm
{
public:

    static vtkAtgSurfaceToBody* New();
    vtkTypeMacro(vtkAtgSurfaceToBody, vtkPolyDataAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetMacro(BottomLevel, double)
    vtkGetMacro(BottomLevel, double)

    vtkSetMacro(RandomVar, double)
    vtkGetMacro(RandomVar, double)

protected:

    vtkAtgSurfaceToBody();
    ~vtkAtgSurfaceToBody();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgSurfaceToBody(vtkAtgSurfaceToBody const&);
    void operator=(vtkAtgSurfaceToBody const&);

    // level of the bottom plane below the topo
    // note: this must be lower than the lowest part of the mines that is to be handled
    // note: default level is at 0.
    double BottomLevel;

    // random variation - up to the below value in both directions (+ or -) and 3 axes
    // this can be applied to all points, in order to avoid coplanar triangles if this
    // resulting "box" is cut with e.g. a block from a block model - which in turn
    // is important because the "boolean" filters on closed bodies are failing in this case
    // note: by setting RandomVar to 0. this step can be turned off
    // note: default variation is up to 0.1
    double RandomVar;

};

#endif
