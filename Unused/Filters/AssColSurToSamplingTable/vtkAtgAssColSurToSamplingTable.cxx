/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgAssColSurToSamplingTable.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgAssColSurToSamplingTable.h>

vtkStandardNewMacro(vtkAtgAssColSurToSamplingTable)

vtkAtgAssColSurToSamplingTable::vtkAtgAssColSurToSamplingTable()
{
    SetNumberOfInputPorts(3);
    SetNumberOfOutputPorts(1);
}

vtkAtgAssColSurToSamplingTable::~vtkAtgAssColSurToSamplingTable()
{
}

int vtkAtgAssColSurToSamplingTable::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // mandatory Assay table
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        case 1:
            // optional Collar table
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
            return 1;

        case 2:
            // optional Survey table
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAssColSurToSamplingTable::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: unified table
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgAssColSurToSamplingTable::RequestData(vtkInformation* request,
                                                vtkInformationVector** inputVector,
                                                vtkInformationVector* outputVector)
{
    // get the input tables
    vtkTable* assay = vtkTable::GetData(inputVector[0]->GetInformationObject(0));
    vtkTable* collar = vtkTable::GetData(inputVector[1]->GetInformationObject(0));
    vtkTable* survey = vtkTable::GetData(inputVector[2]->GetInformationObject(0));

    // output table
    vtkTable* output = vtkTable::GetData(outputVector->GetInformationObject(0));

    // copy the mandatory assay table to the output
    output->ShallowCopy(assay);

    return 1;
}

int vtkAtgAssColSurToSamplingTable::RequestInformation(vtkInformation* request,
                                                       vtkInformationVector** inputVector,
                                                       vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgAssColSurToSamplingTable::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
