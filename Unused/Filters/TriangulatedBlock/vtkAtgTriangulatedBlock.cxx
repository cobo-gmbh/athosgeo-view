/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTriangulatedBlock.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkSetGet.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTriangulatedBlock.h>

vtkStandardNewMacro(vtkAtgTriangulatedBlock)

vtkAtgTriangulatedBlock::vtkAtgTriangulatedBlock()
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgTriangulatedBlock::~vtkAtgTriangulatedBlock()
{
}

int vtkAtgTriangulatedBlock::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // points, wrapped into a poly data object (that has no function otherwise)
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTriangulatedBlock::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // triangulated block
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTriangulatedBlock::RequestData(vtkInformation* request,
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
    // input port 0: points (block vertices
    vtkInformation* infoPoints = inputVector[0]->GetInformationObject(0);
    vtkPolyData* inputPoints = vtkPolyData::SafeDownCast(infoPoints->Get(vtkDataObject::DATA_OBJECT()));

    // get the output poly data
    vtkInformation* infoBlock = outputVector->GetInformationObject(0);
    vtkPolyData* outBlock = vtkPolyData::SafeDownCast(infoBlock->Get(vtkDataObject::DATA_OBJECT()));

    // note: it is assumed that pts has exactly 8 points, and these points must be
    // in the following order:
    // ---
    // +--
    // ++-
    // -+-
    // --+
    // +-+
    // +++
    // -++
    //
    // order of points in a cell
    //
    // Z
    // ^   7 -------- 6
    // | / |        / |
    // 4 -------- 5   |
    // |   |   Y  |   |
    // |   | /    |   |
    // |   3 ---- | - 2
    // | /        | /
    // 0 -------- 1  -> X

    int triInx[12][3] =
    {
        {0, 3, 1},
        {2, 1, 3},
        {0, 4, 3},
        {7, 3, 4},
        {4, 5, 7},
        {6, 7, 5},
        {5, 1, 6},
        {2, 6, 1},
        {0, 1, 4},
        {5, 4, 1},
        {2, 3, 6},
        {7, 6, 3}
    };

    if(8 != inputPoints->GetNumberOfPoints())
    {
        vtkOutputWindowDisplayWarningText("Input for vtkAtgTriangulatedBlock is not 8 points\n");
        return 1;
    }

    vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
    for(int n = 0; n < 12; ++n)
    {
        vtkIdList* list = vtkIdList::New();
        for(int m = 0; m < 3; ++m)
            list->InsertNextId(triInx[n][m]);
        cells->InsertNextCell(list);
    }

    vtkSmartPointer<vtkPolyData> temp = vtkSmartPointer<vtkPolyData>::New();
    temp->SetPoints(inputPoints->GetPoints());
    temp->SetPolys(cells);

    vtkSmartPointer<vtkCleanPolyData> clean = vtkSmartPointer<vtkCleanPolyData>::New();
    clean->SetInputData(temp);
    clean->SetConvertLinesToPoints(true);
    clean->SetConvertPolysToLines(true);
    clean->SetConvertStripsToPolys(true);
    clean->Update();

    vtkSmartPointer<vtkPolyDataNormals> norm = vtkSmartPointer<vtkPolyDataNormals>::New();
    norm->SetInputData(clean->GetOutput());
    norm->SplittingOff();
    norm->ConsistencyOn();
    norm->ComputePointNormalsOff();
    norm->ComputeCellNormalsOn();
    norm->SetFlipNormals(false);
    norm->Update();

    outBlock->ShallowCopy(norm->GetOutput());

    outBlock->BuildCells();
    outBlock->BuildLinks();

    return 1;
}

int vtkAtgTriangulatedBlock::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgTriangulatedBlock::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
