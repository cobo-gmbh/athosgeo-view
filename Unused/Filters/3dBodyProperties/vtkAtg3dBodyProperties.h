/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtg3dBodyProperties.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtg3dBodyProperties_h
#define vtkAtg3dBodyProperties_h

#include <cstring>
#include <vtkUnstructuredGridAlgorithm.h>
#include <atgviewutils_export.h>

class ATGVIEWUTILS_EXPORT vtkAtg3dBodyProperties: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtg3dBodyProperties* New();
    vtkTypeMacro(vtkAtg3dBodyProperties, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    // get the barycenter of the body
    inline void GetBarycenter(double bc[3]) const;

    // get the volume of the body
    inline double GetVolume() const;

protected:

    vtkAtg3dBodyProperties();
    ~vtkAtg3dBodyProperties();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtg3dBodyProperties(vtkAtg3dBodyProperties const&);
    void operator=(vtkAtg3dBodyProperties const&);

    double bc_[3];
    double vol_;

};

inline void vtkAtg3dBodyProperties::GetBarycenter(double bc[3]) const
{
    ::memcpy(bc, bc_, sizeof(bc_));
}

inline double vtkAtg3dBodyProperties::GetVolume() const
{
    return vol_;
}

#endif
