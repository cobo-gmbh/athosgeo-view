/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtg3dBodyProperties.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <vector>
#include <valarray>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkCell.h>
#include <vtkIdList.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtg3dBodyProperties.h>

vtkStandardNewMacro(vtkAtg3dBodyProperties)

vtkAtg3dBodyProperties::vtkAtg3dBodyProperties()
:   vol_(0.)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);

    // initialize the barycenter
    ::memset(bc_, 0, sizeof(bc_));
}

vtkAtg3dBodyProperties::~vtkAtg3dBodyProperties()
{
}

int vtkAtg3dBodyProperties::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // poly data describing a closed surface for the body
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtg3dBodyProperties::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // the filter just passes the input to the output
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtg3dBodyProperties::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    // input: closed triangulated surface
    vtkInformation* infoIn = inputVector[0]->GetInformationObject(0);
    vtkPolyData* inputSurface =
            vtkPolyData::SafeDownCast(infoIn->Get(vtkDataObject::DATA_OBJECT()));

    // output: copy of input
    // note: the side effects are the interesting features of this "filter"
    vtkInformation* infoOut = outputVector->GetInformationObject(0);
    vtkPolyData* outputSurface =
            vtkPolyData::SafeDownCast(infoOut->Get(vtkDataObject::DATA_OBJECT()));
    outputSurface->ShallowCopy(inputSurface);

    // make sure we do not run into a trap if input is not correct
    if(0 == inputSurface->GetNumberOfPoints())
    {
        vtkOutputWindowDisplayWarningText("Atg3dBodyProperties: Input is empty\n");
        return 1;
    }

    // start values for barycenter and volume calculation of the cut body
    std::valarray<double> baryCenter({0., 0., 0.});
    vol_ = 0.;

    // convenient access to the points of the surface
    vtkPoints* points = inputSurface->GetPoints();

    // we arbitrarily take the first point of the surface as the "base point"
    double ptBuf[3];
    points->GetPoint(0, ptBuf);
    std::valarray<double> basePt(ptBuf, 3);

    // iterate through all the triangles of the surface
    for(vtkIdType i = 0; i < inputSurface->GetNumberOfCells(); ++i)
    {
        // get the ids that define the triangle
        vtkIdList* ids = inputSurface->GetCell(i)->GetPointIds();
        if(3 != ids->GetNumberOfIds())
        {
            vtkOutputWindowDisplayWarningText("Atg3dBodyProperties: Not all surface elements are triangles\n");
            return 1;
        }

        // get the 3 triangle points
        // note: if one of the point indices is 0, we skip this triangle:
        //   together with the base point it would define a degenerate tetrahedron
        std::vector<std::valarray<double>> triPts;
        bool includesBasePoint = false;
        for(vtkIdType p = 0; p < ids->GetNumberOfIds(); ++p)
        {
            vtkIdType id = ids->GetId(p);
            if(0 == id)
            {
                includesBasePoint = true;
                break;
            }

            points->GetPoint(id, ptBuf);
            std::valarray<double> pt(ptBuf, 3);
            triPts.push_back(pt);
        }

        // ignore triangles that include the base point
        if(includesBasePoint)
            continue;

        // barycenter of the tetrahedron - and normalization of tetrahedron points
        // by subtracting the base point
        std::valarray<double> tetraCenter = basePt;
        for(int n = 0; n < 3; ++n)
        {
            tetraCenter += triPts[n];
            triPts[n] -= basePt;
        }
        tetraCenter /= 4.;

        // calculate the volume of the tetrahedron and add it to the total volume
        // note: this volume can be negative, and indeed such volumes need to be subtracted
        //   because they correspond to "outer" tetrahedra that occur in concave bodies
        std::valarray<double> crossProd({triPts[0][1] * triPts[1][2] - triPts[0][2] * triPts[1][1],
                                         triPts[0][2] * triPts[1][0] - triPts[0][0] * triPts[1][2],
                                         triPts[0][0] * triPts[1][1] - triPts[0][1] * triPts[1][0]});
        double vol = (crossProd * triPts[2]).sum() / 6.;
        vol_ += vol;

        // add the volume weighted tetrahedron barycenter to the total barycenter
        baryCenter += vol * tetraCenter;
    }

    // finish the barycenter calculation, and copy to the double array
    baryCenter /= vol_;
    bc_[0] = baryCenter[0];
    bc_[1] = baryCenter[1];
    bc_[2] = baryCenter[2];

    return 1;
}

int vtkAtg3dBodyProperties::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtg3dBodyProperties::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
