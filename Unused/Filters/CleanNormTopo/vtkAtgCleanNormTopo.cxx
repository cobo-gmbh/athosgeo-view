/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCleanNormTopo.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>
#include <vtkDataArray.h>
#include <vtkCell.h>
#include <vtkPoints.h>
#include <vtkIdList.h>
#include <vtkCellType.h>
#include <vtkType.h>
#include <vtkCleanPolyData.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkFeatureEdges.h>
#include <vtkStripper.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgCleanNormTopo.h>

// ---------------------------------------------
// NOTE (2024-01-31)
//   It turns out that within this "cleaning class", a call to the "feature edges"
//   filter has the potential to demand so much memory that the entire function crashes.
//   On top of that it turns out that even with some "unclean" surfaces, this filter
//   will work just well because the method is currently much more robust than it
//   was initially (just "shooting vertical lines" at certain points - no complex
//   triangle geometry stuff...): The worst that can happen if the surface is really
//   completely odd that results will be equally odd.
//   For this reason, the vtkAtgCleanNormTopo class was removed from the active code
// ---------------------------------------------

vtkStandardNewMacro(vtkAtgCleanNormTopo)

vtkAtgCleanNormTopo::vtkAtgCleanNormTopo()
:   ErrorMsg(nullptr),
    EmitErrors(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(2);
    // port 0: cleaned normalized topo
    // port 1: topo boundary (closed polygon)
}

vtkAtgCleanNormTopo::~vtkAtgCleanNormTopo()
{
    SetErrorMsg(nullptr);
}

int vtkAtgCleanNormTopo::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // "raw" topo
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCleanNormTopo::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // cleaned normalized topo
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        case 1:
            // topo boundary
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCleanNormTopo::RequestData(vtkInformation* request,
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
    // input port 0: the input raw topo
    vtkPolyData* inputRaw = vtkPolyData::GetData(inputVector[0]);

    // get the output poly data
    vtkPolyData *outTopo = vtkPolyData::GetData(outputVector, 0),
                *outBound = vtkPolyData::GetData(outputVector, 1);

    // first "clean" the raw topo
    vtkSmartPointer<vtkCleanPolyData> clean = vtkSmartPointer<vtkCleanPolyData>::New();
    clean->SetInputData(inputRaw);
    clean->ConvertLinesToPointsOn();
    clean->ConvertPolysToLinesOn();
    clean->ConvertStripsToPolysOn();
    clean->Update();

    // make sure the surface consists of triangles only
    vtkSmartPointer<vtkTriangleFilter> triang = vtkSmartPointer<vtkTriangleFilter>::New();
    triang->SetInputData(clean->GetOutput());
    triang->PassVertsOff(); // we do not need single vertices (points)
    triang->PassLinesOff(); // just ignore lines (otherwise converts them into line segments)
    triang->Update();

    // check if really all the "cells" are triangles
    vtkCellArray* cells = triang->GetOutput()->GetPolys();
    vtkIdType triPts = 0;
    vtkIdType const* pts = nullptr;
    for(cells->InitTraversal(); cells->GetNextCell(triPts, pts);)
    {
        if(3 != triPts)
        {
            errorMessage("topo input is not and "
                         "cannot be converted into a clean triangulated surface");
            return 1;
        }
    }

    // now about the normals: they should all be pointing
    // - in the same vertical direction (up or down) -> otherwise this is not a "topo"
    // - upwards -> convention for further processing
    // in order to ensure the latter, we do the following step first once, then check and
    // then repeat it with "inv" on (meaning: invert all the normal directions)
    // purpose: when used with an "implicit function", the result should be
    // - "inside" --> below topo
    // - "outside" --> above topo
    vtkSmartPointer<vtkPolyDataNormals> norm = vtkSmartPointer<vtkPolyDataNormals>::New();
    for(int inv = 0; inv < 2; ++inv)
    {
        // get the normals
        norm->SetInputData(triang->GetOutput());
        norm->SplittingOff();
        norm->ConsistencyOn();
        norm->ComputePointNormalsOff();
        norm->ComputeCellNormalsOn();
        norm->SetFlipNormals(inv);
        norm->Update();

        // get the normal array and count how many point upwards (z positive) or downwards
        // in addition we want to know the covered horizontal surface that any normal
        // represents - plus the number of vertical triangle (with zero horizontal surface)
        vtkDataArray* narr = norm->GetOutput()->GetCellData()->GetArray("Normals");
        int numUp = 0,
            numDown = 0,
            numHor = 0;
        double surfUp = 0.,
               surfDown = 0.;
        static const double normEps = 0.001;
        for(vtkIdType k = 0; k < narr->GetNumberOfTuples(); ++k)
        {
            // normal array and triangle corners
            double* normArr = narr->GetTuple(k);
            vtkPoints* cellPts = norm->GetOutput()->GetCell(k)->GetPoints();
            double a[3], b[3], c[3];
            cellPts->GetPoint(0, a);
            cellPts->GetPoint(1, b);
            cellPts->GetPoint(2, c);

            // projected surface of the triangle
            double surf = 0.;
            if(3 == cellPts->GetNumberOfPoints())
            {
                // note: this condition (triangle) should always be met
                //   -> triangulate filter was applied above
                surf = (a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1])) / 2.;
            }

            // count up/down/horizontal normals, and summarize surface "up" and "down"
            if(normEps < normArr[2])
            {
                ++numUp;
                surfUp += surf;
            }
            else if(-normEps > normArr[2])
            {
                ++numDown;
                surfDown += surf;
            }
            else
            {
                ++numHor;
            }
        }

        // if now most normals point downwards:
        // - see whether it is all (-> "real" topo) or only the majority
        // - stop the loop here
        if(::fabs(surfUp) > ::fabs(surfDown))
        {
            // we want to avoid this level of "pickyness" !
            //if(0 < numDown)
            //{
            //    std::string countMsg = std::string("the input topo has overhangs:\n") +
            //            std::to_string(numDown) +
            //            " triangle normals (out of " +
            //            std::to_string(numDown + numUp) +
            //            ") are pointing in the opposite direction";
            //    errorMessage(countMsg.c_str());
            //    return 1;
            //}

            // leave the loop (otherwise do once more and invert the normals)
            break;
        }
    }

    // check whether the pit/topo has an outside boundary - or possibly several ??
    vtkSmartPointer<vtkFeatureEdges> fed = vtkSmartPointer<vtkFeatureEdges>::New();
    fed->SetInputData(norm->GetOutput());
    fed->BoundaryEdgesOn();
    fed->FeatureEdgesOff();
    fed->ManifoldEdgesOff();
    fed->NonManifoldEdgesOff();
    fed->Update();

    // collect single lines into polyline(s)
    // note: the stripper filter has a maximum length attribute that has to be set
    //   to a value that it is always larger than the number of line segments that
    //   we want to collect, because otherwise we may get more than one boundary
    //   even if this is not true! The default is 1000, so it would happen with
    //   large boundary lines with more than 1000 segments
    // note regarding the strange name "stripper" for this filter: The primary
    //   purpose seems to be to merge triangles into triangle strips, but for the
    //   1D == linear case, it was extended to also handle line segments and merge
    //   them into polylines
    vtkSmartPointer<vtkStripper> strip = vtkSmartPointer<vtkStripper>::New();
    strip->SetMaximumLength(2 * fed->GetOutput()->GetNumberOfLines());
    strip->SetInputData(fed->GetOutput());
    strip->SetJoinContiguousSegments(true);
    strip->Update();

    // clean the output strip

    // we need here one and only one boundary line for a valid topo
    if(strip->GetOutput()->GetNumberOfLines() != 1)
    {
        errorMessage("topo input does not have exactly one boundary: "
                     "holes? unclean triangulation?");
        return 1;
    }

    // copy the normalized topo to the output
    outTopo->ShallowCopy(norm->GetOutput());

    // copy the boundary to the output
    outBound->ShallowCopy(strip->GetOutput());

    return 1;
}

void vtkAtgCleanNormTopo::errorMessage(std::string const& msg)
{
    if(EmitErrors)
        vtkOutputWindowDisplayWarningText(("AtgCleanNormTopo: " + msg + "\n").c_str());

    SetErrorMsg(msg.c_str());
}

int vtkAtgCleanNormTopo::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgCleanNormTopo::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
