/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCleanNormTopo.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgCleanNormTopo_h
#define vtkAtgCleanNormTopo_h

#include <string>
#include <vtkPolyDataAlgorithm.h>
#include <AtgFiltersModule.h>

// ---------------------------------------------
// NOTE (2024-01-31)
//   It turns out that within this "cleaning class", a call to the "feature edges"
//   filter has the potential to demand so much memory that the entire function crashes.
//   On top of that it turns out that even with some "unclean" surfaces, this filter
//   will work just well because the method is currently much more robust than it
//   was initially (just "shooting vertical lines" at certain points - no complex
//   triangle geometry stuff...): The worst that can happen if the surface is really
//   completely odd that results will be equally odd.
//   For this reason, the vtkAtgCleanNormTopo class was removed from the active code
// ---------------------------------------------

class ATGFILTERS_EXPORT vtkAtgCleanNormTopo: public vtkPolyDataAlgorithm
{
public:

    static vtkAtgCleanNormTopo* New();
    vtkTypeMacro(vtkAtgCleanNormTopo, vtkPolyDataAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetStringMacro(ErrorMsg)
    vtkGetStringMacro(ErrorMsg)

    vtkSetMacro(EmitErrors, bool)
    vtkGetMacro(EmitErrors, bool)

protected:

    vtkAtgCleanNormTopo();
    ~vtkAtgCleanNormTopo();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgCleanNormTopo(vtkAtgCleanNormTopo const&);
    void operator=(vtkAtgCleanNormTopo const&);

    // handle error conditions
    void errorMessage(std::string const& msg);

    // error handling
    char* ErrorMsg;
    bool EmitErrors;

};

#endif
