/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgClipBlockGrid.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <cstring>
#include <cmath>
#include <set>
#include <string>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkDoubleArray.h>
#include <vtkDataArray.h>
#include <vtkLongLongArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkImageData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkCleanUnstructuredGrid.h>
#include <vtkCellCenters.h>
#include <vtkImageGaussianSmooth.h>
#include <vtkPassArrays.h>
#include <vtkResampleToImage.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgAddNeighborIndices.h>
#include <utilAddGridCells.h>
#include <vtkAtgClipBlockGrid.h>

vtkStandardNewMacro(vtkAtgClipBlockGrid)

#undef _GENERATE_GRADIENT_

vtkAtgClipBlockGrid::vtkAtgClipBlockGrid()
:   ClipTolerance(10.),
    PitRmtothing(3.),
    VerticalFacePreference(20.),
    NoBlockClipping(false)
{
    SetNumberOfInputPorts(1);
#ifdef _GENERATE_GRADIENT_
    SetNumberOfOutputPorts(2);
#else
    SetNumberOfOutputPorts(1);
#endif
}

vtkAtgClipBlockGrid::~vtkAtgClipBlockGrid()
{
}

int vtkAtgClipBlockGrid::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // unstructured grid with Clip attribute
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgClipBlockGrid::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // clipped grid
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

#ifdef _GENERATE_GRADIENT_
        case 1:
            // gradients of pit attribute, used for the clipping of blocks
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;
#endif

        default:
            return 0;
    }
}

int vtkAtgClipBlockGrid::RequestData(vtkInformation* request,
                                   vtkInformationVector** inputVector,
                                   vtkInformationVector* outputVector)
{
    // we will need some norm names throughout the function, and some temporary names
    static const std::string namePit(utilNormNames::getName(utilNormNames::PIT)),
                              nameCenter(utilNormNames::getName(utilNormNames::BLOCKCENTER)),
                              nameSize(utilNormNames::getName(utilNormNames::BLOCKSIZE)),
                              nameAngle(utilNormNames::getName(utilNormNames::ANGLE)),
                              nameNb(utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR));

    // get the input unstructured grid
    vtkInformation* infoGrid = inputVector[0]->GetInformationObject(0);
    vtkUnstructuredGrid* inputGrid =
            vtkUnstructuredGrid::SafeDownCast(infoGrid->Get(vtkDataObject::DATA_OBJECT()));

    // get the output unstructured grid (clipped)
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* outGrid = vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

#ifdef _GENERATE_GRADIENT_
    // get the output point set with the gradient vectors
    outInfo = outputVector->GetInformationObject(1);
    vtkUnstructuredGrid* outGradients = vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
#endif

    // shortcuts to data arrays
    vtkCellData* inCellData = inputGrid->GetCellData();
    vtkDataArray
        *pitArray = vtkDataArray::SafeDownCast(inCellData->GetArray(namePit.c_str())),
        *centerArray = vtkDataArray::SafeDownCast(inCellData->GetArray(nameCenter.c_str())),
        *sizeArray = vtkDataArray::SafeDownCast(inCellData->GetArray(nameSize.c_str())),
        *angleArray = vtkDataArray::SafeDownCast(inCellData->GetArray(nameAngle.c_str()));
    vtkLongLongArray
        *nbArray = vtkLongLongArray::SafeDownCast(inCellData->GetArray(nameNb.c_str()));

    // check the different mandatory arrays
    if((0 == centerArray) || (0 == sizeArray) || (0 == angleArray))
    {
        vtkOutputWindowDisplayWarningText("Block center/size/angle attributes not found\n");
        return 1;
    }
    if(0 == nbArray)
    {
        vtkOutputWindowDisplayWarningText("Neighbor block link attributes not found\n");
        return 1;
    }
    if(0 == pitArray)
    {
        vtkOutputWindowDisplayWarningText("No Pit attribute found\n");
        return 1;
    }

    // for proper block clipping we generate a gradient field from the pit values
    vtkDoubleArray* pitGradientArray = generateGradientArray(inputGrid, namePit.c_str());
    for(vtkIdType b = 0; b < pitGradientArray->GetNumberOfTuples(); ++b)
    {
        double zg = pitGradientArray->GetComponent(b, 2);
        zg /= VerticalFacePreference;
        pitGradientArray->SetComponent(b, 2, zg);
    }

#ifdef _GENERATE_GRADIENT_
    // generate a point set from the input block model centroids
    // and attach the gradient array to it
    vtkCellCenters* ccent = vtkCellCenters::New();
    ccent->SetInputData(inputGrid);
    ccent->Update();
    outGradients->CopyStructure(ccent->GetOutput());
    outGradients->GetPointData()->AddArray(pitArray);
    outGradients->GetPointData()->AddArray(pitGradientArray);
#endif

    // copy the neighborhood attributes: we need to change them
    vtkLongLongArray* nbTemp = vtkLongLongArray::New();
    nbTemp->DeepCopy(nbArray);

    // collect block ids of those blocks that will disappear completely
    std::set<vtkIdType> removeIds;
    for(vtkIdType c = 0; c < inputGrid->GetNumberOfCells(); ++c)
    {
        double clip = 100. - pitArray->GetVariantValue(c).ToDouble();
        if(ClipTolerance > clip)
            removeIds.insert(c);
    }

    // remove the ids to be removed from the copied neighborhood arrays
    for(vtkIdType b = 0; b < inputGrid->GetNumberOfCells(); ++b)
    {
        for(vtkIdType c = 0; c < nbTemp->GetNumberOfComponents(); ++c)
        {
            if(removeIds.end() != removeIds.find(nbTemp->GetComponent(b, c)))
                nbTemp->SetComponent(b, c, -1);
        }
    }

    // temporary new unstructured grid to generate, plus temp. points
    vtkUnstructuredGrid* newGrid = vtkUnstructuredGrid::New();
    newGrid->GetCellData()->CopyStructure(inputGrid->GetCellData());
    newGrid->GetCellData()
            ->CopyAllocate(inputGrid->GetCellData(),
                           inputGrid->GetCellData()->GetNumberOfTuples() - removeIds.size());
    vtkPoints* newPts = vtkPoints::New();
    newPts->SetDataType(VTK_DOUBLE);

    // this class adds cells to the temp. grid and the temp. points
    utilAddGridCells* addCells = new utilAddGridCells(newGrid, newPts);

    // iterate through input blocks and transfer / cut / ignore them
    for(vtkIdType b = 0; b < inputGrid->GetNumberOfCells(); ++b)
    {
        // get key for the clipping, and remove what is being dropped completely
        double clip = 100. - pitArray->GetVariantValue(b).ToDouble();
        if(ClipTolerance > clip)
            continue;

        // these we will need now anyway
        double fullSize[3],
               center[3],
               angle = angleArray->GetVariantValue(b).ToDouble();
        centerArray->GetTuple(b, center);
        sizeArray->GetTuple(b, fullSize);
        vtkIdType cellId;

        // blocks to transfer directly (nothing to clip away)
        if(((100. - ClipTolerance) <= clip) || NoBlockClipping)
        {
            // rebuild the cell
            cellId = addCells->generateCell(center, fullSize, angle);

            // copy the data from the input to the output grid and
            // if clip is < 100, adapt the attributes of the new cell
            newGrid->GetCellData()->CopyData(inputGrid->GetCellData(), b, cellId);
            adaptCellAttributes(newGrid, cellId, clip);

            // done for this case
            continue;
        }

        // from here on we need to clip the block
        // note: with the NoBlockClipping option we will never get here

        // vector defining the shift direction from old to reduced new block
        // notes:
        //   - the shift vector is the inverted pit gradient array because we want
        //     to "push the remaining block against the wall".
        //   - furthermore it is an "absolute" vector that we want to make
        //     "block relative" by dividing every element by it's corresponding
        //     block size
        //   - finally we need to rotate it into the block azimuth direction
        // note: thanks to the gradient filter we will always have some "face normal"
        //   array available - even if not in the input data
        double shift[3];
        pitGradientArray->GetTuple(b, shift);
        for(int i = 0; i < 3; ++i)
            shift[i] = shift[i] / fullSize[i];
        double sl = ::sqrt(shift[0] * shift[0] + shift[1] * shift[1] + shift[2] * shift[2]);
        for(int i = 0; i < 3; ++i)
            shift[i] /= sl;
        double a = 3.14159265 * angle / 180.,
               sa = ::sin(a),
               ca = ::cos(a),
               x = shift[0] * ca + shift[1] * sa,
               y = shift[1] * ca - shift[0] * sa;
        shift[0] = x;
        shift[1] = y;

        // first check: if the gradient has an upward component, set the vertical
        // component to 0 (because we never want to cut "from below")
        shift[2] = std::min(0., shift[2]);

        // double check: if the gradient points to nowhere let it point downwards
        if((0. == shift[0]) && (0. == shift[1]) && (0. == shift[2]))
            shift[2] = -1.;

        // first we check if there is at least one free face opposite to
        // it's corresponding shift direction
        // note: at the same time we are buffering the neighbor values
        vtkIdType nb[6];
        bool hasFullyFreeShiftFace = false;
        for(vtkIdType i = 0; i < 3; ++i)
        {
            vtkIdType k = 2 * i;
            for(k = 2 * i; k < (2 * i + 2); ++k)
                nb[k] = nbTemp->GetComponent(b, k);
            if(shift[i] == 0.)
                continue;
            k = (shift[i] > 0.) ? (2 * i) : (2 * i + 1);
            if(nb[k] < 0)
                hasFullyFreeShiftFace = true;
        }

        // if there is a fully free face we will generate one single reduced new
        // block in the direction of where the shift array(s) are pointing to
        if(hasFullyFreeShiftFace)
        {
            // make sure that the shift does not point away from a non-free face
            // note that here we are sure that this will not affect all 3 dimensions,
            //   and also we know that with this we will never end up with a zero
            //   shift array and still having to clip something
            for(vtkIdType i = 0; i < 3; ++i)
            {
                vtkIdType k = (shift[i] > 0.) ? (2 * i) : (2 * i + 1);

                if(nb[k] >= 0)
                    shift[i] = 0.;
            }

            // assertion: should never happen here!
            if((0. == shift[0]) && (0. == shift[1]) && (0. == shift[2]))
            {
                vtkOutputWindowDisplayWarningText("Assertion failure: unexpected zero shift array\n");
                return 1;
            }

            // normalize the shift vector
            double mag = ::sqrt(shift[0] * shift[0] + shift[1] * shift[1] + shift[2] * shift[2]);
            for(vtkIdType i = 0; i < 3; ++i)
                shift[i] /= mag;

            // generate size of the cut block from normalized shift vector with this method:
            //
            // cut vol = dx * dy * dz * prop
            // cut vol = (prop ^ dc(x)) * dx * (prop ^ dc(y)) * dy * (prop ^ dc(z))
            //   with dc() being the directional cosine
            //   and the directional cosine being the coordinate value of the shift vector
            double cutSize[3];
            for(int i = 0; i < 3; ++i)
                cutSize[i] = fullSize[i] * ::pow(clip / 100., shift[i] * shift[i]);

            // center is shifted by half of the difference of full and cut size,
            // towards the direction indicated by the sign
            double centerShift[3];
            for(int i = 0; i < 3; ++i)
                centerShift[i] = 0.5 * ((shift[i] > 0.) ? 1. : -1.) * (fullSize[i] - cutSize[i]);

            // generate and add the reduced cell
            // note: on return, the function will have adapted the center if there was
            //   a center shift applied
            cellId = addCells->generateCell(center, cutSize, angle, centerShift);

            // copy the data from the input to the output grid and
            // if clip is < 100, adapt the attributes of the new cell
            newGrid->GetCellData()->CopyData(inputGrid->GetCellData(), b, cellId);
            adaptCellAttributes(newGrid, cellId, clip);

            // done with one single cut block
            continue;
        }

        // from here we need to generate 2 cut blocks because we do not have
        // completely free faces away from the shift direction

        // we drop the dimension with the smallest shift vector because even
        // then we can ensure some contact to all 6 faces
        // note: to do this we generate an index vector that indirectly sorts
        //   the dimensions from largest to smallest shift component
        //   (do a little bubble sort), then we use only the first 2 dimensions
        vtkIdType ind[3] = {0, 1, 2};
        for(int i = 0; i < 2; ++i)
        {
            for(int j = 0; j < (2 - i); ++j)
            {
                if(::fabs(shift[ind[j]]) < ::fabs(shift[ind[j + 1]]))
                    std::swap(ind[j], ind[j + 1]);
            }
        }
        shift[ind[2]] = 0.;

        // normalize and invert the shift vector
        double negMmag = -::sqrt(shift[0] * shift[0] + shift[1] * shift[1] + shift[2] * shift[2]);
        for(vtkIdType i = 0; i < 3; ++i)
            shift[i] /= negMmag;

        // generate size of the "counter cut block" from inverted normalized shift vector
        // and the inverse of the clip value (actually the clipped away part) with this method:
        //
        // cut vol = dx * dy * dz * prop
        // cut vol = (prop ^ dc(x)) * dx * (prop ^ dc(y)) * dy * (prop ^ dc(z))
        //   with dc() being the directional cosine
        //   and the directional cosine being the coordinate value of the shift vector
        double invCutSize[3];
        for(vtkIdType i = 0; i < 3; ++i)
            invCutSize[i] = fullSize[i] * ::pow((100. - clip) / 100., shift[i] * shift[i]);

        // find the dimensions of the two new cut blocks
        double cutSize2[2][3];
        ::memcpy(cutSize2[0], fullSize, sizeof(fullSize));
        ::memcpy(cutSize2[1], fullSize, sizeof(fullSize));
        cutSize2[0][ind[0]] = fullSize[ind[0]] - invCutSize[ind[0]];
        cutSize2[1][ind[0]] = invCutSize[ind[0]];
        cutSize2[1][ind[1]] = fullSize[ind[1]] - invCutSize[ind[1]];

        // find the center shift for the two new blocks
        double centerShift2[2][3] = {{0., 0., 0.}, {0., 0., 0.}};
        double dir[2] = {((shift[ind[0]] < 0.) ? 0.5 : -0.5),
                         ((shift[ind[1]] < 0.) ? 0.5 : -0.5)};
        centerShift2[0][ind[0]] =  dir[0] * invCutSize[ind[0]];
        centerShift2[1][ind[0]] = -dir[0] * cutSize2[0][ind[0]];
        centerShift2[1][ind[1]] =  dir[1] * invCutSize[ind[1]];

        // find the clip distribution between the two new blocks
        double clip2[2],
               vol2[2];
        for(int i = 0; i < 2; ++i)
            vol2[i] = cutSize2[i][0] * cutSize2[i][1] * cutSize2[i][2];
        clip2[0] = vol2[0] * clip / (vol2[0] + vol2[1]);
        clip2[1] = clip - clip2[0];

        // generate the two new blocks
        for(int i = 0; i < 2; ++i)
        {
            // generate and add the reduced cell
            cellId = addCells->generateCell(center, cutSize2[i], angle, centerShift2[i]);

            // copy the data from the input to the output grid and
            // if clip is < 100, adapt the attributes of the new cell
            newGrid->GetCellData()->CopyData(inputGrid->GetCellData(), b, cellId);
            adaptCellAttributes(newGrid, cellId, clip2[i]);
        }
    }

    // clean up the cells generator
    delete addCells;

    // if we have no points by now, there is nothing left of the model
    // after the clipping!
    if(0 == newPts->GetNumberOfPoints())
    {
        vtkOutputWindowDisplayWarningText("Nothing left after the clipping\n");
        return 1;
    }

    // finish the temporary output grid
    newGrid->SetPoints(newPts);

    // remove the pit array
    newGrid->GetCellData()->RemoveArray(namePit.c_str());

    // clean the temporary output grid
    vtkSmartPointer<vtkCleanUnstructuredGrid> clean = vtkSmartPointer<vtkCleanUnstructuredGrid>::New();
    clean->SetInputData(newGrid);
    clean->Update();

    // add neighborhood indices
    vtkSmartPointer<vtkAtgAddNeighborIndices> addNb = vtkSmartPointer<vtkAtgAddNeighborIndices>::New();
    addNb->SetInputData(clean->GetOutput());
    addNb->Update();

    // the result is now the output
    outGrid->ShallowCopy(addNb->GetOutput());

    return 1;
}

int vtkAtgClipBlockGrid::RequestInformation(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgClipBlockGrid::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}

vtkDoubleArray* vtkAtgClipBlockGrid::generateGradientArray(vtkUnstructuredGrid* grid, char const* arr)
{
    // calculate the average cell size, and from this derive
    // some voxel size that - hopefully! - generates a sensible gradient
    // for the pit variable...
    double cellSizes[3] = {0., 0., 0.};
    for(vtkIdType b = 0; b < grid->GetNumberOfCells(); ++b)
    {
        double cb[6];
        grid->GetCellBounds(b, cb);
        for(int i = 0; i < 3; ++i)
            cellSizes[i] += cb[2 * i + 1] - cb[2 * i];
    }
    for(int i = 0; i < 3; ++i)
        cellSizes[i] /= grid->GetNumberOfCells();
    double voxelSize = (cellSizes[0] + cellSizes[1] + cellSizes[2]) / (3. * 4.);

    // bounding box and extent of the input grid
    // note: we extend the 3d grid by one voxel in all directions
    double gridBounds[6];
    grid->GetBounds(gridBounds);
    for(int i = 0; i < 3; ++i)
    {
        gridBounds[2 * i] -= voxelSize;
        gridBounds[2 * i + 1] += voxelSize;
    }
    int dims[3] =
    {
        (int)::floor((gridBounds[1] - gridBounds[0]) / voxelSize),
        (int)::floor((gridBounds[3] - gridBounds[2]) / voxelSize),
        (int)::floor((gridBounds[5] - gridBounds[4]) / voxelSize)
    };

    vtkSmartPointer<vtkPassArrays> parrs = vtkSmartPointer<vtkPassArrays>::New();
    parrs->SetInputData(grid);
    parrs->AddArray(vtkDataObject::CELL, arr);
    parrs->UseFieldTypesOff();
    parrs->AddFieldType(vtkDataObject::POINT);
    parrs->AddFieldType(vtkDataObject::CELL);
    parrs->Update();

    vtkSmartPointer<vtkResampleToImage> resi = vtkSmartPointer<vtkResampleToImage>::New();
    resi->SetInputDataObject(parrs->GetOutput());
    resi->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, arr);
    resi->SetUseInputBounds(false);
    resi->SetSamplingBounds(gridBounds);
    resi->SetSamplingDimensions(dims[0], dims[1], dims[2]);
    resi->Update();

    vtkImageData* img3 = resi->GetOutput();
    vtkDataArray* dataArr = vtkDataArray::SafeDownCast(img3->GetPointData()->GetArray(arr));

    // make sure that from bottom to top, pit values can only increase
    for(vtkIdType i = 0; i < dims[0]; ++i)
    {
        for(vtkIdType j = 0; j < dims[1]; ++j)
        {
            double pit = 0.;
            for(vtkIdType k = 0; k < dims[2]; ++k)
            {
                int ijk[3] = {int(i), int(j), int(k)};
                vtkIdType id = img3->ComputePointId(ijk);
                pit = std::max<double>(pit, dataArr->GetVariantValue(id).ToDouble());
                dataArr->SetComponent(id, 0, pit);
            }
        }
    }

    // add some smoothing
    // note: this works on the "scalars" of the 3d image - that need to be
    //   defined first as being the "Pit" attribute
    vtkSmartPointer<vtkImageGaussianSmooth> gauss = vtkSmartPointer<vtkImageGaussianSmooth>::New();
    img3->GetPointData()->SetActiveScalars(arr);
    gauss->SetInputData(img3);
    gauss->SetDimensionality(3);
    gauss->SetStandardDeviation(PitRmtothing);
    gauss->Update();
    dataArr = vtkDataArray::SafeDownCast(gauss->GetOutput()->GetPointData()->GetArray(arr));

    vtkSmartPointer<vtkCellCenters> ccent = vtkSmartPointer<vtkCellCenters>::New();
    ccent->SetInputData(grid);
    ccent->Update();

    vtkSmartPointer<vtkDoubleArray> grdArr = vtkSmartPointer<vtkDoubleArray>::New();
    grdArr->SetName("PitGradient");
    grdArr->SetNumberOfComponents(3);
    grdArr->Allocate(ccent->GetOutput()->GetNumberOfPoints());

    for(vtkIdType b = 0; b < ccent->GetOutput()->GetNumberOfPoints(); ++b)
    {
        double cpt[3];
        ccent->GetOutput()->GetPoint(b, cpt);
        int ijk[3];
        double pcoords[3];
        img3->ComputeStructuredCoordinates(cpt, ijk, pcoords);
        double gvec[3];
        img3->GetPointGradient(ijk[0], ijk[1], ijk[2], dataArr, gvec);

        grdArr->InsertTuple3(b, gvec[0], gvec[1], gvec[2]);
    }

    return grdArr;
}

void vtkAtgClipBlockGrid::adaptCellAttributes(vtkUnstructuredGrid* grid, vtkIdType cid, double clip)
{
    // nothing to clip
    if(99.9 < clip)
        return;

    // this is for the two possibilities that we either meet ktons first, or
    // the taken tonnages
    double ktons = 0.,
           ktonsRed = 0.;

    // we need it also here
    vtkDataArray
        *centerArr = vtkDataArray::SafeDownCast(grid->GetCellData()
            ->GetArray(utilNormNames::getName(utilNormNames::BLOCKCENTER).c_str())),
        *sizeArr = vtkDataArray::SafeDownCast(grid->GetCellData()
            ->GetArray(utilNormNames::getName(utilNormNames::BLOCKSIZE).c_str()));

    for(vtkIdType a = 0; a < grid->GetCellData()->GetNumberOfArrays(); ++a)
    {
        vtkDataArray* arr = grid->GetCellData()->GetArray(a);
        int arrType = utilNormNames::getType(arr->GetName()),
            arrId = utilNormNames::getIndex(arr->GetName()).first;

        switch(arrType)
        {
            case utilNormNames::TY_COORDINATE:
            {
                if((utilNormNames::BLOCKCENTER == arrId) && (3 == arr->GetNumberOfComponents()))
                {
                    double center[3];
                    centerArr->GetTuple(cid, center);
                    for(int i = 0; i < 3; ++i)
                        arr->SetComponent(cid, i, center[i]);
                }
                break;
            }

            case utilNormNames::TY_DIMENSION:
            {
                switch(arrId)
                {
                    case utilNormNames::BLOCKSIZE:
                    {
                        if(3 == arr->GetNumberOfComponents())
                        {
                            double size[3];
                            sizeArr->GetTuple(cid, size);
                            for(int i = 0; i < 3; ++i)
                                arr->SetComponent(cid, i, size[i]);
                        }
                        break;
                    }
                }
                break;
            }

            case utilNormNames::TY_DIMENSION3:
            {
                if(utilNormNames::VOLUME == arrId)
                {
                    double vol = arr->GetVariantValue(cid).ToDouble();
                    arr->SetVariantValue(cid, clip * vol / 100.);
                }
                break;
            }

            case utilNormNames::TY_TONNAGE:
            {
                switch(arrId)
                {
                    if(0. == ktons)
                    {
                        vtkDataArray* ktArr = grid->GetCellData()
                                ->GetArray(utilNormNames::getName(utilNormNames::KTONS).c_str());
                        ktons = ktArr->GetVariantValue(cid).ToDouble();
                        ktonsRed = (100. - clip) * ktons / 100.;
                    }

                    case utilNormNames::KTONS:
                    {
                        arr->SetVariantValue(cid, ktons - ktonsRed);
                        break;
                    }
                    case utilNormNames::TAKENTOTAL:
                    case utilNormNames::TAKENPROD:
                    case utilNormNames::OFFEREDPROD:
                    {
                        // TODO - not sure if this makes any sense yet...
                        double tk = arr->GetVariantValue(cid).ToDouble();
                        arr->SetVariantValue(cid, std::max<double>(0., tk - ktonsRed));
                        break;
                    }
                }
                break;
            }
        }
    }
}
