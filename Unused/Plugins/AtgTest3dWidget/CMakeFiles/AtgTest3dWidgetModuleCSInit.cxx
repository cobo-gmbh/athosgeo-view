#include "vtkABI.h"
#include "vtkClientServerInterpreter.h"

extern void vtkAtgTest3dWidget_Init(vtkClientServerInterpreter*);
extern void AtgTest3dWidgetModuleModule_Init(vtkClientServerInterpreter*);

extern "C" void VTK_ABI_EXPORT AtgTest3dWidgetModuleCS_Initialize(vtkClientServerInterpreter* csi)
{
  (void)csi;
  vtkAtgTest3dWidget_Init(csi);
  AtgTest3dWidgetModuleModule_Init(csi);
}
