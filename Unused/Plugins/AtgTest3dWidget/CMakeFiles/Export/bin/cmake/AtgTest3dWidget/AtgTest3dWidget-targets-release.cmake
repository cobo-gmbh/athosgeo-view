#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "AthosGeoView::AtgDebugLog" for configuration "Release"
set_property(TARGET AthosGeoView::AtgDebugLog APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgDebugLog PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgDebugLog.so"
  IMPORTED_SONAME_RELEASE "libAtgDebugLog.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgDebugLog )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgDebugLog "${_IMPORT_PREFIX}/bin/libAtgDebugLog.so" )

# Import target "AthosGeoView::AtgUtilities" for configuration "Release"
set_property(TARGET AthosGeoView::AtgUtilities APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgUtilities PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgUtilities.so"
  IMPORTED_SONAME_RELEASE "libAtgUtilities.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgUtilities )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgUtilities "${_IMPORT_PREFIX}/bin/libAtgUtilities.so" )

# Import target "AthosGeoView::AtgFilters" for configuration "Release"
set_property(TARGET AthosGeoView::AtgFilters APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgFilters PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "VTK::IOXML"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgFilters.so"
  IMPORTED_SONAME_RELEASE "libAtgFilters.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgFilters )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgFilters "${_IMPORT_PREFIX}/bin/libAtgFilters.so" )

# Import target "AthosGeoView::AtgManagers" for configuration "Release"
set_property(TARGET AthosGeoView::AtgManagers APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgManagers PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgManagers.so"
  IMPORTED_SONAME_RELEASE "libAtgManagers.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgManagers )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgManagers "${_IMPORT_PREFIX}/bin/libAtgManagers.so" )

# Import target "AthosGeoView::AtgSystem" for configuration "Release"
set_property(TARGET AthosGeoView::AtgSystem APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgSystem PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "Qt5::Core;Qt5::Widgets"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgSystem.so"
  IMPORTED_SONAME_RELEASE "libAtgSystem.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgSystem )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgSystem "${_IMPORT_PREFIX}/bin/libAtgSystem.so" )

# Import target "AthosGeoView::GeoDxf" for configuration "Release"
set_property(TARGET AthosGeoView::GeoDxf APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoDxf PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "VTK::FiltersGeneral;VTK::FiltersSources;VTK::RenderingFreeType"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoDxf.so"
  IMPORTED_SONAME_RELEASE "libGeoDxf.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoDxf )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoDxf "${_IMPORT_PREFIX}/bin/libGeoDxf.so" )

# Import target "AthosGeoView::AtgAddNeighborIndicesModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgAddNeighborIndicesModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgAddNeighborIndicesModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;VTK::FiltersGeneral"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgAddNeighborIndicesModule.so"
  IMPORTED_SONAME_RELEASE "libAtgAddNeighborIndicesModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgAddNeighborIndicesModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgAddNeighborIndicesModule "${_IMPORT_PREFIX}/bin/libAtgAddNeighborIndicesModule.so" )

# Import target "AthosGeoView::AtgTableViewModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgTableViewModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgTableViewModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgDebugLog;VTK::CommonExecutionModel"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgTableViewModule.so"
  IMPORTED_SONAME_RELEASE "libAtgTableViewModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgTableViewModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgTableViewModule "${_IMPORT_PREFIX}/bin/libAtgTableViewModule.so" )

# Import target "AthosGeoView::AtgSummarizeAttributesModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgSummarizeAttributesModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgSummarizeAttributesModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgSummarizeAttributesModule.so"
  IMPORTED_SONAME_RELEASE "libAtgSummarizeAttributesModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgSummarizeAttributesModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgSummarizeAttributesModule "${_IMPORT_PREFIX}/bin/libAtgSummarizeAttributesModule.so" )

# Import target "AthosGeoView::AtgSummaryRepresentationModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgSummaryRepresentationModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgSummaryRepresentationModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgTableViewModule"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgSummaryRepresentationModule.so"
  IMPORTED_SONAME_RELEASE "libAtgSummaryRepresentationModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgSummaryRepresentationModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgSummaryRepresentationModule "${_IMPORT_PREFIX}/bin/libAtgSummaryRepresentationModule.so" )

# Import target "AthosGeoView::AtgDomainsModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgDomainsModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgDomainsModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgDomainsModule.so"
  IMPORTED_SONAME_RELEASE "libAtgDomainsModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgDomainsModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgDomainsModule "${_IMPORT_PREFIX}/bin/libAtgDomainsModule.so" )

# Import target "AthosGeoView::AtgPropertyWidgetsModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgPropertyWidgetsModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgPropertyWidgetsModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters;AthosGeoView::AtgManagers;AthosGeoView::AtgSystem;AthosGeoView::AtgDebugLog;VTK::CommonCore;Qt5::Widgets"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgPropertyWidgetsModule.so"
  IMPORTED_SONAME_RELEASE "libAtgPropertyWidgetsModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgPropertyWidgetsModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgPropertyWidgetsModule "${_IMPORT_PREFIX}/bin/libAtgPropertyWidgetsModule.so" )

# Import target "AthosGeoView::AtgAttributesFromFileModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgAttributesFromFileModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgAttributesFromFileModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters;VTK::IOInfovis"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgAttributesFromFileModule.so"
  IMPORTED_SONAME_RELEASE "libAtgAttributesFromFileModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgAttributesFromFileModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgAttributesFromFileModule "${_IMPORT_PREFIX}/bin/libAtgAttributesFromFileModule.so" )

# Import target "AthosGeoView::AtgCalculatorForSelectionModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgCalculatorForSelectionModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgCalculatorForSelectionModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgCalculatorForSelectionModule.so"
  IMPORTED_SONAME_RELEASE "libAtgCalculatorForSelectionModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgCalculatorForSelectionModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgCalculatorForSelectionModule "${_IMPORT_PREFIX}/bin/libAtgCalculatorForSelectionModule.so" )

# Import target "AthosGeoView::AtgCementModuliModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgCementModuliModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgCementModuliModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgCementModuliModule.so"
  IMPORTED_SONAME_RELEASE "libAtgCementModuliModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgCementModuliModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgCementModuliModule "${_IMPORT_PREFIX}/bin/libAtgCementModuliModule.so" )

# Import target "AthosGeoView::AtgFixedColorsAttributeModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgFixedColorsAttributeModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgFixedColorsAttributeModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgFixedColorsAttributeModule.so"
  IMPORTED_SONAME_RELEASE "libAtgFixedColorsAttributeModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgFixedColorsAttributeModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgFixedColorsAttributeModule "${_IMPORT_PREFIX}/bin/libAtgFixedColorsAttributeModule.so" )

# Import target "AthosGeoView::AtgPitFromSurfaceModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgPitFromSurfaceModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgPitFromSurfaceModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgPitFromSurfaceModule.so"
  IMPORTED_SONAME_RELEASE "libAtgPitFromSurfaceModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgPitFromSurfaceModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgPitFromSurfaceModule "${_IMPORT_PREFIX}/bin/libAtgPitFromSurfaceModule.so" )

# Import target "AthosGeoView::AtgRenameAttributeModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgRenameAttributeModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgRenameAttributeModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgRenameAttributeModule.so"
  IMPORTED_SONAME_RELEASE "libAtgRenameAttributeModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgRenameAttributeModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgRenameAttributeModule "${_IMPORT_PREFIX}/bin/libAtgRenameAttributeModule.so" )

# Import target "AthosGeoView::AtgTest3dWidgetModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgTest3dWidgetModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgTest3dWidgetModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgTest3dWidgetModule.so"
  IMPORTED_SONAME_RELEASE "libAtgTest3dWidgetModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgTest3dWidgetModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgTest3dWidgetModule "${_IMPORT_PREFIX}/bin/libAtgTest3dWidgetModule.so" )

# Import target "AthosGeoView::AtgWriteMultiValueModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgWriteMultiValueModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgWriteMultiValueModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;ParaView::pqComponents"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgWriteMultiValueModule.so"
  IMPORTED_SONAME_RELEASE "libAtgWriteMultiValueModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgWriteMultiValueModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgWriteMultiValueModule "${_IMPORT_PREFIX}/bin/libAtgWriteMultiValueModule.so" )

# Import target "AthosGeoView::AtgWriteValueModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgWriteValueModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgWriteValueModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgWriteValueModule.so"
  IMPORTED_SONAME_RELEASE "libAtgWriteValueModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgWriteValueModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgWriteValueModule "${_IMPORT_PREFIX}/bin/libAtgWriteValueModule.so" )

# Import target "AthosGeoView::AtgZoneFromBoundaryModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgZoneFromBoundaryModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgZoneFromBoundaryModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgZoneFromBoundaryModule.so"
  IMPORTED_SONAME_RELEASE "libAtgZoneFromBoundaryModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgZoneFromBoundaryModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgZoneFromBoundaryModule "${_IMPORT_PREFIX}/bin/libAtgZoneFromBoundaryModule.so" )

# Import target "AthosGeoView::AtgClipWithBoundaryModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgClipWithBoundaryModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgClipWithBoundaryModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgClipWithBoundaryModule.so"
  IMPORTED_SONAME_RELEASE "libAtgClipWithBoundaryModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgClipWithBoundaryModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgClipWithBoundaryModule "${_IMPORT_PREFIX}/bin/libAtgClipWithBoundaryModule.so" )

# Import target "AthosGeoView::AtgClipWithGeotiffModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgClipWithGeotiffModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgClipWithGeotiffModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters;VTK::IOGDAL;VTK::FiltersExtraction"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgClipWithGeotiffModule.so"
  IMPORTED_SONAME_RELEASE "libAtgClipWithGeotiffModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgClipWithGeotiffModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgClipWithGeotiffModule "${_IMPORT_PREFIX}/bin/libAtgClipWithGeotiffModule.so" )

# Import target "AthosGeoView::AtgClipWithSurfaceModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgClipWithSurfaceModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgClipWithSurfaceModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgClipWithSurfaceModule.so"
  IMPORTED_SONAME_RELEASE "libAtgClipWithSurfaceModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgClipWithSurfaceModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgClipWithSurfaceModule "${_IMPORT_PREFIX}/bin/libAtgClipWithSurfaceModule.so" )

# Import target "AthosGeoView::AtgDuplicateModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgDuplicateModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgDuplicateModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgDuplicateModule.so"
  IMPORTED_SONAME_RELEASE "libAtgDuplicateModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgDuplicateModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgDuplicateModule "${_IMPORT_PREFIX}/bin/libAtgDuplicateModule.so" )

# Import target "AthosGeoView::AtgExtractCategoryModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgExtractCategoryModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgExtractCategoryModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgManagers"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgExtractCategoryModule.so"
  IMPORTED_SONAME_RELEASE "libAtgExtractCategoryModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgExtractCategoryModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgExtractCategoryModule "${_IMPORT_PREFIX}/bin/libAtgExtractCategoryModule.so" )

# Import target "AthosGeoView::AtgNewBlockModelModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgNewBlockModelModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgNewBlockModelModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgNewBlockModelModule.so"
  IMPORTED_SONAME_RELEASE "libAtgNewBlockModelModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgNewBlockModelModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgNewBlockModelModule "${_IMPORT_PREFIX}/bin/libAtgNewBlockModelModule.so" )

# Import target "AthosGeoView::AtgClipGeometryModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgClipGeometryModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgClipGeometryModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgClipGeometryModule.so"
  IMPORTED_SONAME_RELEASE "libAtgClipGeometryModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgClipGeometryModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgClipGeometryModule "${_IMPORT_PREFIX}/bin/libAtgClipGeometryModule.so" )

# Import target "AthosGeoView::AtgBlockModelReaderModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgBlockModelReaderModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgBlockModelReaderModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities;AthosGeoView::AtgFilters;AthosGeoView::AtgSystem;AthosGeoView::AtgAddNeighborIndicesModule;AthosGeoView::AtgDebugLog;ParaView::pqComponents;VTK::IOInfovis"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgBlockModelReaderModule.so"
  IMPORTED_SONAME_RELEASE "libAtgBlockModelReaderModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgBlockModelReaderModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgBlockModelReaderModule "${_IMPORT_PREFIX}/bin/libAtgBlockModelReaderModule.so" )

# Import target "AthosGeoView::AtgDXFWriterModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgDXFWriterModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgDXFWriterModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgDXFWriterModule.so"
  IMPORTED_SONAME_RELEASE "libAtgDXFWriterModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgDXFWriterModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgDXFWriterModule "${_IMPORT_PREFIX}/bin/libAtgDXFWriterModule.so" )

# Import target "AthosGeoView::AtgGeotiffSurfaceReaderModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgGeotiffSurfaceReaderModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgGeotiffSurfaceReaderModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgFilters;VTK::IOGDAL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgGeotiffSurfaceReaderModule.so"
  IMPORTED_SONAME_RELEASE "libAtgGeotiffSurfaceReaderModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgGeotiffSurfaceReaderModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgGeotiffSurfaceReaderModule "${_IMPORT_PREFIX}/bin/libAtgGeotiffSurfaceReaderModule.so" )

# Import target "AthosGeoView::GeoDXFReaderModule" for configuration "Release"
set_property(TARGET AthosGeoView::GeoDXFReaderModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoDXFReaderModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "VTK::FiltersGeneral;VTK::FiltersSources"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoDXFReaderModule.so"
  IMPORTED_SONAME_RELEASE "libGeoDXFReaderModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoDXFReaderModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoDXFReaderModule "${_IMPORT_PREFIX}/bin/libGeoDXFReaderModule.so" )

# Import target "AthosGeoView::GeoDXFReaderMBModule" for configuration "Release"
set_property(TARGET AthosGeoView::GeoDXFReaderMBModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoDXFReaderMBModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "VTK::FiltersGeneral;VTK::FiltersSources"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoDXFReaderMBModule.so"
  IMPORTED_SONAME_RELEASE "libGeoDXFReaderMBModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoDXFReaderMBModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoDXFReaderMBModule "${_IMPORT_PREFIX}/bin/libGeoDXFReaderMBModule.so" )

# Import target "AthosGeoView::GeoExtractMBPolyDataModule" for configuration "Release"
set_property(TARGET AthosGeoView::GeoExtractMBPolyDataModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoExtractMBPolyDataModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoExtractMBPolyDataModule.so"
  IMPORTED_SONAME_RELEASE "libGeoExtractMBPolyDataModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoExtractMBPolyDataModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoExtractMBPolyDataModule "${_IMPORT_PREFIX}/bin/libGeoExtractMBPolyDataModule.so" )

# Import target "AthosGeoView::GeoExtractMBUnstructuredGridModule" for configuration "Release"
set_property(TARGET AthosGeoView::GeoExtractMBUnstructuredGridModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoExtractMBUnstructuredGridModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoExtractMBUnstructuredGridModule.so"
  IMPORTED_SONAME_RELEASE "libGeoExtractMBUnstructuredGridModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoExtractMBUnstructuredGridModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoExtractMBUnstructuredGridModule "${_IMPORT_PREFIX}/bin/libGeoExtractMBUnstructuredGridModule.so" )

# Import target "AthosGeoView::GeoShapeReaderModule" for configuration "Release"
set_property(TARGET AthosGeoView::GeoShapeReaderModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::GeoShapeReaderModule PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libGeoShapeReaderModule.so"
  IMPORTED_SONAME_RELEASE "libGeoShapeReaderModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::GeoShapeReaderModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::GeoShapeReaderModule "${_IMPORT_PREFIX}/bin/libGeoShapeReaderModule.so" )

# Import target "AthosGeoView::AtgCategoryRepresentationModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgCategoryRepresentationModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgCategoryRepresentationModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgFilters;AthosGeoView::AtgManagers;AthosGeoView::AtgDebugLog"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgCategoryRepresentationModule.so"
  IMPORTED_SONAME_RELEASE "libAtgCategoryRepresentationModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgCategoryRepresentationModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgCategoryRepresentationModule "${_IMPORT_PREFIX}/bin/libAtgCategoryRepresentationModule.so" )

# Import target "AthosGeoView::AtgSummaryViewModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgSummaryViewModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgSummaryViewModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "AthosGeoView::AtgUtilities"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgSummaryViewModule.so"
  IMPORTED_SONAME_RELEASE "libAtgSummaryViewModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgSummaryViewModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgSummaryViewModule "${_IMPORT_PREFIX}/bin/libAtgSummaryViewModule.so" )

# Import target "AthosGeoView::AtgTexturedRepresentationModule" for configuration "Release"
set_property(TARGET AthosGeoView::AtgTexturedRepresentationModule APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AtgTexturedRepresentationModule PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "VTK::FiltersTexture;VTK::IOGDAL"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/libAtgTexturedRepresentationModule.so"
  IMPORTED_SONAME_RELEASE "libAtgTexturedRepresentationModule.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AtgTexturedRepresentationModule )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AtgTexturedRepresentationModule "${_IMPORT_PREFIX}/bin/libAtgTexturedRepresentationModule.so" )

# Import target "AthosGeoView::AthosGeoViewDocumentation" for configuration "Release"
set_property(TARGET AthosGeoView::AthosGeoViewDocumentation APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AthosGeoViewDocumentation PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libAthosGeoViewDocumentation.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AthosGeoViewDocumentation )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AthosGeoViewDocumentation "${_IMPORT_PREFIX}/lib/libAthosGeoViewDocumentation.a" )

# Import target "AthosGeoView::ParaViewExtraDocumentation" for configuration "Release"
set_property(TARGET AthosGeoView::ParaViewExtraDocumentation APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::ParaViewExtraDocumentation PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libParaViewExtraDocumentation.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::ParaViewExtraDocumentation )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::ParaViewExtraDocumentation "${_IMPORT_PREFIX}/lib/libParaViewExtraDocumentation.a" )

# Import target "AthosGeoView::AthosGeoView" for configuration "Release"
set_property(TARGET AthosGeoView::AthosGeoView APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AthosGeoView::AthosGeoView PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/AthosGeoView"
  )

list(APPEND _IMPORT_CHECK_TARGETS AthosGeoView::AthosGeoView )
list(APPEND _IMPORT_CHECK_FILES_FOR_AthosGeoView::AthosGeoView "${_IMPORT_PREFIX}/bin/AthosGeoView" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
