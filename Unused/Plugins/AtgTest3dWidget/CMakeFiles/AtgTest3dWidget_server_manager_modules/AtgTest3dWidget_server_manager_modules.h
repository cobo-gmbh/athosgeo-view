#ifndef AtgTest3dWidget_server_manager_modules_h
#define AtgTest3dWidget_server_manager_modules_h

#include "AtgTest3dWidget_server_manager_modules_data.h"
#include <string>
#include <vector>

void AtgTest3dWidget_server_manager_modules_initialize(std::vector<std::string>& xmls)
{
  (void)xmls;
  {
    char *init_string = AtgTest3dWidget_server_manager_modulespluginGetInterfaces();
    xmls.push_back(init_string);
    delete [] init_string;
  }
}

#endif
