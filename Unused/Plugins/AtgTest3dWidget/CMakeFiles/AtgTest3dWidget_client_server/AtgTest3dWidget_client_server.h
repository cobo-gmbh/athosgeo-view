#ifndef AtgTest3dWidget_client_server_h
#define AtgTest3dWidget_client_server_h

#include "vtkClientServerInterpreter.h"

extern "C" void AtgTest3dWidgetModuleCS_Initialize(vtkClientServerInterpreter*);

void AtgTest3dWidget_client_server_initialize(vtkClientServerInterpreter* csi)
{
  (void)csi;
  AtgTest3dWidgetModuleCS_Initialize(csi);
}

#endif
