# Install script for directory: /home/cornelis/Main/develop/AthosGeo/Projects/View/Sources/Plugins/Attributes/AtgTest3dWidget

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for the subdirectory.
  include("/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/Plugins/Attributes/AtgTest3dWidget/AtgTest3dWidgetModule/cmake_install.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets.cmake"
         "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/Plugins/Attributes/AtgTest3dWidget/CMakeFiles/Export/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget" TYPE FILE FILES "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/Plugins/Attributes/AtgTest3dWidget/CMakeFiles/Export/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget" TYPE FILE FILES "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/Plugins/Attributes/AtgTest3dWidget/CMakeFiles/Export/bin/cmake/AtgTest3dWidget/AtgTest3dWidget-targets-release.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/cmake/AtgTest3dWidget" TYPE FILE RENAME "AtgTest3dWidget-vtk-module-properties.cmake" FILES "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/Plugins/Attributes/AtgTest3dWidget/CMakeFiles/AtgTest3dWidget-vtk-module-properties.cmake.install")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/bin/libAtgTest3dWidgetModuleCS.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE MODULE FILES "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/bin/AtgTest3dWidget.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so"
         OLD_RPATH "/home/cornelis/Main/develop/AthosGeo/Projects/View/Binaries/Release/bin:/home/cornelis/Main/develop/ParaView/Binaries/ParaView-v5.8.0/Release/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/AtgTest3dWidget.so")
    endif()
  endif()
endif()

