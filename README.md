# AthosGEO View

AthosGEO View is a ParaView (https://paraview.org) based custom application that adds some functions and filters for specially dealing with block models and geo data: see https://cobo.bockemuehl.ch.

AthosGEO View is licensed as open source software, following the same permissive conditions as ParaView.

Binaries for Windows (64bit) can be downloaded from the website (with registration).

Instructions for building from sources are still missing at this moment. The first step would be in any case to build ParaView itself (version 5.7), applying a few minor patches to the sources. AthosGEO View will then be built, referring to that successful ParaView build.