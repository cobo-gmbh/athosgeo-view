/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFObjectMap.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

/*
.NAME vtkGeoDXFObjectMap.h
By MIRARCO::Tim Anema
.SECTION Description
This is a Wrapper Class to make DXFParser use
vtkstd::map instead of vtkCollection
to facilitate constant time searches for layers
*/

#ifndef __vtkGeoDXFObjectMap_h
#define __vtkGeoDXFObjectMap_h

#include <map>
#include <vtkObject.h>
#include <vtkGeoDXFLayer.h>
#include <vtkGeoDXFBlock.h>
#include <GeoDxfModule.h>

class GEODXF_EXPORT vtkGeoDXFObjectMap
{
public:

    int GetNumberOfItems() const;

    //original method used to find the object needed
    //still used for output itteration though
    vtkSmartPointer<vtkGeoDXFBlock> GetItemAsObject(int i);

    //new method to get the object in constant time
    //just fund the object with the key name
    vtkSmartPointer<vtkGeoDXFBlock> GetItemAsObject(std::string const& name);

    void AddItem(std::string const& name, vtkSmartPointer<vtkGeoDXFBlock> object);
    static vtkGeoDXFObjectMap* New();
    void Delete();

protected:

    vtkGeoDXFObjectMap();
    ~vtkGeoDXFObjectMap();

private:

    typedef std::map<std::string, vtkSmartPointer<vtkGeoDXFBlock>> ObjectMap;
    ObjectMap map;

};

#endif
