/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFLayer.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// By: Eric Daoust && Matthew Livingstone
#include <cmath>
#include <sstream>
#include <vtkGeoDXFLayer.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkTriangle.h>
#include <vtkCollection.h>
#include <vtkVectorText.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkDiskSource.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

#define PI 3.1415926

vtkStandardNewMacro(vtkGeoDXFLayer)

vtkGeoDXFLayer::vtkGeoDXFLayer()
:   vtkObject(),
    layerPropertyValue(0.0),
    freezeValue(0.0),
    drawHidden(false),
    layerExists(0),
    currElevation(0.0),
    scale(1.0),
    largeUnits(1),
    textRotation(0.0),
    arcPoints(vtkSmartPointer<vtkPoints>::New()),
    arcCells(vtkSmartPointer<vtkCellArray>::New()),
    pointPoints(vtkSmartPointer<vtkPoints>::New()),
    pointCells(vtkSmartPointer<vtkCellArray>::New()),
    polyLinePoints(vtkSmartPointer<vtkPoints>::New()),
    polyLineCells(vtkSmartPointer<vtkCellArray>::New()),
    lwPolyLinePoints(vtkSmartPointer<vtkPoints>::New()),
    lwPolyLineCells(vtkSmartPointer<vtkCellArray>::New()),
    linePoints(vtkSmartPointer<vtkPoints>::New()),
    lineCells(vtkSmartPointer<vtkCellArray>::New()),
    surfPoints(vtkSmartPointer<vtkPoints>::New()),
    surfCells(vtkSmartPointer<vtkCellArray>::New()),
    solidPoints(vtkSmartPointer<vtkPoints>::New()),
    solidCells(vtkSmartPointer<vtkCellArray>::New()),
    pointProps(vtkSmartPointer<vtkDoubleArray>::New()),
    polyLineProps(vtkSmartPointer<vtkDoubleArray>::New()),
    lwPolyLineProps(vtkSmartPointer<vtkDoubleArray>::New()),
    lineProps(vtkSmartPointer<vtkDoubleArray>::New()),
    surfProps(vtkSmartPointer<vtkDoubleArray>::New()),
    circleProps(vtkSmartPointer<vtkDoubleArray>::New()),
    textProps(vtkSmartPointer<vtkDoubleArray>::New()),
    arcProps(vtkSmartPointer<vtkDoubleArray>::New()),
    solidProps(vtkSmartPointer<vtkDoubleArray>::New()),
    textList(vtkSmartPointer<vtkCollection>::New()),
    circleList(vtkSmartPointer<vtkCollection>::New()),
    blockList(vtkSmartPointer<vtkCollection>::New())
{
    // Deep copy fails unless we manually state how many components
    pointProps->SetNumberOfComponents(1);
    polyLineProps->SetNumberOfComponents(1);
    lwPolyLineProps->SetNumberOfComponents(1);
    lineProps->SetNumberOfComponents(1);
    surfProps->SetNumberOfComponents(1);
    circleProps->SetNumberOfComponents(1);
    textProps->SetNumberOfComponents(1);
    arcProps->SetNumberOfComponents(1);
    solidProps->SetNumberOfComponents(1);

    // make sure coordinates are always double precision
    arcPoints->SetDataType(VTK_DOUBLE);
    pointPoints->SetDataType(VTK_DOUBLE);
    polyLinePoints->SetDataType(VTK_DOUBLE);
    lwPolyLinePoints->SetDataType(VTK_DOUBLE);
    linePoints->SetDataType(VTK_DOUBLE);
    surfPoints->SetDataType(VTK_DOUBLE);
    solidPoints->SetDataType(VTK_DOUBLE);
}

vtkGeoDXFLayer::~vtkGeoDXFLayer()
{
}

/*
#define UnRegisterAndCopy(x, y)\
    if(x)\
    {\
        x->UnRegister(this);\
    }\
    x = y;\
    if(x)\
    {\
        x->Register(this);\
    }
*/

void vtkGeoDXFLayer::ShallowCopy(vtkGeoDXFLayer *object)
{
    if(!object)
        return; // we failed!

    // BCO: replaced this reference counter fiddling with smart pointers
    pointPoints = object->pointPoints;
    pointCells = object->pointCells;
    arcPoints = object->arcPoints;
    arcCells = object->arcCells;
    linePoints = object->linePoints;
    lineCells = object->lineCells;
    polyLinePoints = object->polyLinePoints;
    polyLineCells = object->polyLineCells;
    lwPolyLinePoints = object->lwPolyLinePoints;
    lwPolyLineCells = object->lwPolyLineCells;
    surfPoints = object->surfPoints;
    surfCells = object->surfCells;
    solidPoints = object->solidPoints;
    solidCells = object->solidCells;
    pointProps = object->pointProps;
    polyLineProps = object->polyLineProps;
    lwPolyLineProps = object->lwPolyLineProps;
    lineProps = object->lineProps;
    surfProps = object->surfProps;
    circleProps = object->circleProps;
    textProps = object->textProps;
    arcProps = object->arcProps;
    solidProps = object->solidProps;
    textList = object->textList;
    circleList = object->circleList;
    blockList = object->blockList;

/*
    UnRegisterAndCopy(pointPoints, object->pointPoints);
    UnRegisterAndCopy(pointCells, object->pointCells);
    UnRegisterAndCopy(arcPoints, object->arcPoints);
    UnRegisterAndCopy(arcCells, object->arcCells);
    UnRegisterAndCopy(linePoints, object->linePoints);
    UnRegisterAndCopy(lineCells, object->lineCells);
    UnRegisterAndCopy(polyLinePoints, object->polyLinePoints);
    UnRegisterAndCopy(polyLineCells, object->polyLineCells);
    UnRegisterAndCopy(lwPolyLinePoints, object->lwPolyLinePoints);
    UnRegisterAndCopy(lwPolyLineCells, object->lwPolyLineCells);
    UnRegisterAndCopy(surfPoints, object->surfPoints);
    UnRegisterAndCopy(surfCells, object->surfCells);
    UnRegisterAndCopy(solidPoints, object->solidPoints);
    UnRegisterAndCopy(solidCells, object->solidCells);
    UnRegisterAndCopy(pointProps, object->pointProps);
    UnRegisterAndCopy(polyLineProps, object->polyLineProps);
    UnRegisterAndCopy(lwPolyLineProps, object->lwPolyLineProps);
    UnRegisterAndCopy(lineProps, object->lineProps);
    UnRegisterAndCopy(surfProps, object->surfProps);
    UnRegisterAndCopy(circleProps, object->circleProps);
    UnRegisterAndCopy(textProps, object->textProps);
    UnRegisterAndCopy(arcProps, object->arcProps);
    UnRegisterAndCopy(solidProps, object->solidProps);
    UnRegisterAndCopy(textList, object->textList);
    UnRegisterAndCopy(circleList, object->circleList);
    UnRegisterAndCopy(blockList, object->blockList);
*/

    // BCO: not all attributes were copied
    name = object->name;
    layerPropertyValue = object->layerPropertyValue;
    freezeValue = object->freezeValue;
    drawHidden = object->drawHidden;
    layerExists = object->layerExists;
    currElevation = object->currElevation;
    scale = object->scale;
    largeUnits = object->largeUnits;
    textRotation = object->textRotation;
}

void vtkGeoDXFLayer::PrintSelf(ostream& os, vtkIndent indent)
{
    //TODO// complete this method
    Superclass::PrintSelf(os, indent);
}
