/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFObject.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkGeoDXFObject
// By: Eric Daoust && Matthew Livingstone
// .SECTION Description
// vtkGeoDXFObject is a subclass of vtkMultiBlockDataSetAlgorithm to read DXF Files

#ifndef __vtkGeoDXFObject_h
#define __vtkGeoDXFObject_h

#include <string>
#include <vtkInformation.h>
#include <vtkMultiBlockDataSetAlgorithm.h>
#include <GeoDxfModule.h>

class GEODXF_EXPORT vtkGeoDXFObject: public vtkMultiBlockDataSetAlgorithm
{
public:

    static vtkGeoDXFObject* New();
    vtkTypeMacro(vtkGeoDXFObject,vtkMultiBlockDataSetAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetStringMacro(FileName)

    // flag: true if the file is a binary file
    vtkSetMacro(IsBinary, bool)
    vtkGetMacro(IsBinary, bool)

    // warning: number of non-supported SOLID entities
    vtkSetMacro(NumUnsupportedSolid, unsigned long)
    vtkGetMacro(NumUnsupportedSolid, unsigned long)

    // option to draw points
    vtkSetMacro(DrawPoints, bool)
    vtkGetMacro(DrawPoints, bool)

    // option to drawn frozen/invisible layers
    vtkSetMacro(DrawHidden, bool)
    vtkGetMacro(DrawHidden, bool)

    // MLivingstone:
    // Set/Get for option to scale all entities
    // This was added for use in special cases, when the EXACT units are needed from the dxf file.
    // If the dxf has both model space and paper space elements, disabling this will distort the drawing
    vtkSetMacro(AutoScale, bool)
    vtkGetMacro(AutoScale, bool)

protected:

    vtkGeoDXFObject();
    ~vtkGeoDXFObject();

    int RequestData(vtkInformation* request,
                  vtkInformationVector** inputVector,
                  vtkInformationVector* outputVector);

    void Read(std::ifstream* file);

    int layerExists;
    int type;

private:

    std::string GetObjectName();

    // name of file (full path), useful for obtaining object names
    char const* FileName;

    // text of current command line
    int CommandLine;

    // text of current value line
    std::string ValueLine;

    // flag to be returned
    // note: so far this should only trigger a message to the user because actually
    // reading the format is not supported
    bool IsBinary;

    // counter for unsupported solids
    unsigned long NumUnsupportedSolid;

    // enable/disable point drawing
    bool DrawPoints;

    // enable/disable drawing of frozen/invisible layers
    bool DrawHidden;

    // MLivingstone:
    // Set to enable/disable scaling of entities
    // This was added for use in special cases, when the EXACT units are needed from the dxf file.
    // If the dxf has both model space and paper space elements, disabling this will distort the drawing
    bool AutoScale;

};

#endif
