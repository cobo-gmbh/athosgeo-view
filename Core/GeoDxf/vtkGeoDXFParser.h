/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFParser.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// Layer object for DXF layers
// By: Eric Daoust && Matthew Livingstone

#ifndef __vtkGeoDXFParser_h
#define __vtkGeoDXFParser_h

#include <sstream>
#include <fstream>
#include <string>
#include <vtkCellArray.h>
#include <vtkGeoDXFLayer.h>
#include <vtkGeoDXFBlock.h>
#include <vtkGeoDXFObjectMap.h>
#include <GeoDxfModule.h>

class vtkCollection;

class GEODXF_EXPORT vtkGeoDXFParser: public vtkObject
{
public:

    static vtkGeoDXFParser* New();
    vtkTypeMacro(vtkGeoDXFParser,vtkObject)
    void PrintSelf(ostream& os, vtkIndent indent);

    void ApplyProperties();

    // read one pair of entries from file
    void Read(std::ifstream* file);

    vtkGeoDXFObjectMap* ParseData(std::string const& name, bool DrawHidden, bool AutoScale);

    inline bool isBinary() const
    {
        return IsBinary;
    }

    inline unsigned long numUnknownSolid() const
    {
        return NumUnknownSolid;
    }

    vtkSmartPointer<vtkGeoDXFLayer> GetLayer(std::string const& name, vtkGeoDXFObjectMap* layerList);
    vtkSmartPointer<vtkGeoDXFBlock> GetBlock(std::string const& name, vtkGeoDXFObjectMap* blockList);

    std::string TrimWhiteSpace(std::string const& input);
    int getIntFromLine(std::string const& line);
    void getLineSafe(std::ifstream* file, std::string& line);

    inline double getXScale() const
    {
        return XScale;
    }

    inline double getYScale() const
    {
        return YScale;
    }

    inline double getZScale() const
    {
        return ZScale;
    }

    // don't use directly
    ~vtkGeoDXFParser() override;

protected:

    vtkGeoDXFParser();

    std::string name;
    double layerPropertyValue;

    void ParseLine(std::ifstream* file, std::string* name,
                   vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseFace(std::ifstream* file, std::string* name,
                   vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParsePoints(std::ifstream* file, std::string* name,
                     vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParsePolyLine(std::ifstream* file, std::string* name,
                       vtkGeoDXFObjectMap* layerList, bool firstPoint, bool AutoScale);
    void ParseLWPolyLine(std::ifstream* file, std::string* name,
                         vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseText(std::ifstream* file, std::string* name,
                   vtkGeoDXFObjectMap* layerList, bool mText, bool AutoScale);
    void ParseCircle(std::ifstream* file, std::string* name,
                     vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseArc(std::ifstream* file, std::string* name,
                  vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseSolid(std::ifstream* file, std::string* name,
                    vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseSolid2(std::ifstream* file, std::string* name,
                     vtkGeoDXFObjectMap* layerList, bool AutoScale);

    void ParseLineBlock(std::ifstream* file, std::string* name,
                        vtkGeoDXFObjectMap* layerList,
                        vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseFaceBlock(std::ifstream* file, std::string* name,
                        vtkGeoDXFObjectMap* layerList,
                        vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParsePointsBlock(std::ifstream* file, std::string* name,
                          vtkGeoDXFObjectMap* layerList,
                          vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParsePolyLineBlock(std::ifstream* file, std::string* name,
                            vtkGeoDXFObjectMap* layerList, bool firstPoint,
                            vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseLWPolyLineBlock(std::ifstream* file, std::string* name,
                              vtkGeoDXFObjectMap* layerList,
                              vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseTextBlock(std::ifstream* file, std::string* name,
                        vtkGeoDXFObjectMap* layerList, bool mText,
                        vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseCircleBlock(std::ifstream* file, std::string* name,
                          vtkGeoDXFObjectMap* layerList,
                          vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseArcBlock(std::ifstream* file, std::string* name,
                       vtkGeoDXFObjectMap* layerList,
                       vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseSolidBlock(std::ifstream* file, std::string* name,
                         vtkGeoDXFObjectMap* layerList,
                         vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);
    void ParseSolid2Block(std::ifstream* file, std::string* name,
                          vtkGeoDXFObjectMap* layerList,
                          vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale);

    void ParseInsert(std::ifstream* file, std::string* name,
                     vtkGeoDXFObjectMap* layerList, bool AutoScale);
    void ParseDimension(std::ifstream* file, std::string* name,
                        vtkGeoDXFObjectMap* layerList, bool AutoScale);

    void ParseColorTable(std::ifstream* file,
                         vtkGeoDXFObjectMap* layerList, bool DrawHidden);

    void ParseBlockRecords(std::ifstream* file);
    void ParseHeader(std::ifstream* file);
    void ParseViewPort(std::ifstream* file);

    std::string CleanString(std::string& line, bool mText, int& numLine);
    double GetLine();

    // Used to determine if a layer has been found while parsing the file
    int layerExists;

    // Text scale
    double scale;

    // Elevation for polylines
    double currElevation;

    // Scale used for scaling ALL lines in *MODEL_SPACE
    double LineTypeScale;
    double XScale;
    double YScale;
    double ZScale;

    // Used to calculate the scaling values based on the extents of the
    // model space and paper space
    double ExtMinX;
    double ExtMaxX;
    double PExtMinX;
    double PExtMaxX;
    double ExtMinY;
    double ExtMaxY;
    double PExtMinY;
    double PExtMaxY;
    double ExtMinZ;
    double ExtMaxZ;
    double PExtMinZ;
    double PExtMaxZ;
    double XAdj;
    double YAdj;

    // Used for viewport information
    double ViewX;
    double ViewY;
    double ViewZoom;

    std::stringstream textString;

    // Used to ensure that BLOCKS should exist.
    bool blockRecordExists;

private:

    // flag: true if we are dealing with a binary DXF file
    bool IsBinary;

    // flag/counter: counts SOLID entities that are not supported
    unsigned long NumUnknownSolid;

    // text of current command line
    int CommandLine;

    // text of current value line
    std::string ValueLine;

    // Used for BLOCK data
    vtkGeoDXFObjectMap* blockList;

};

#endif
