/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFLayer.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// Layer object for DXF layers
// By: Eric Daoust && Matthew Livingstone

#ifndef __vtkGeoDXFLayer_h
#define __vtkGeoDXFLayer_h

#include <sstream>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkSmartPointer.h>
#include <GeoDxfModule.h>

class vtkCollection;

class GEODXF_EXPORT vtkGeoDXFLayer: public vtkObject
{
public:

    static vtkGeoDXFLayer* New();
    vtkTypeMacro(vtkGeoDXFLayer, vtkObject)
    void PrintSelf(ostream& os, vtkIndent indent) override;

    vtkCollection* ParseData(const char* name);

    inline vtkSmartPointer<vtkPoints> getArcPoints()
    {
        return arcPoints;
    }

    inline vtkSmartPointer<vtkCellArray> getArcCells()
    {
        return arcCells;
    }

    inline vtkSmartPointer<vtkPoints> getPointPoints()
    {
        return pointPoints;
    }

    inline vtkSmartPointer<vtkCellArray> getPointCells()
    {
        return pointCells;
    }

    inline vtkSmartPointer<vtkPoints> getLinePoints()
    {
        return linePoints;
    }

    inline vtkSmartPointer<vtkCellArray> getLineCells()
    {
        return lineCells;
    }

    inline vtkSmartPointer<vtkPoints> getPolyLinePoints()
    {
        return polyLinePoints;
    }

    inline vtkSmartPointer<vtkCellArray> getPolyLineCells()
    {
        return polyLineCells;
    }

    inline vtkSmartPointer<vtkPoints> getLWPolyLinePoints()
    {
        return lwPolyLinePoints;
    }

    inline vtkSmartPointer<vtkCellArray> getLWPolyLineCells()
    {
        return lwPolyLineCells;
    }

    inline vtkSmartPointer<vtkPoints> getSurfPoints()
    {
        return surfPoints;
    }

    inline vtkSmartPointer<vtkCellArray> getSurfCells()
    {
        return surfCells;
    }

    inline vtkSmartPointer<vtkPoints> getSolidPoints()
    {
        return solidPoints;
    }

    inline vtkSmartPointer<vtkCellArray> getSolidCells()
    {
        return solidCells;
    }

    inline vtkSmartPointer<vtkCollection> getText()
    {
        return textList;
    }

    inline vtkSmartPointer<vtkCollection> getCircles()
    {
        return circleList;
    }

    inline vtkSmartPointer<vtkDoubleArray> getPointProps()
    {
        return pointProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getLineProps()
    {
        return lineProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getPolyLineProps()
    {
        return polyLineProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getLWPolyLineProps()
    {
        return lwPolyLineProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getSurfProps()
    {
        return surfProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getCircleProps()
    {
        return circleProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getTextProps()
    {
        return textProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getArcProps()
    {
        return arcProps;
    }

    inline vtkSmartPointer<vtkDoubleArray> getSolidProps()
    {
        return solidProps;
    }

    inline vtkSmartPointer<vtkCollection> getBlockList()
    {
        return blockList;
    }

    inline std::string getName()
    {
        return name;
    }

    inline double getLayerPropertyValue()
    {
        return layerPropertyValue;
    }

    inline double getFreezeValue()
    {
        return freezeValue;
    }

    inline bool getDrawHidden()
    {
        return drawHidden;
    }

    void ShallowCopy(vtkGeoDXFLayer* object);

    inline void setName(std::string objName)
    {
        name = objName;
    }

    inline void setLayerPropertyValue(double value)
    {
        layerPropertyValue = value;
    }

    inline void setFreezeValue(double value)
    {
        freezeValue = value;
    }

    inline void setDrawHidden(bool value)
    {
        drawHidden = value;
    }

    vtkGeoDXFLayer* GetLayer(std::string name, vtkCollection* layerList);

protected:

    vtkGeoDXFLayer();
    ~vtkGeoDXFLayer();

    std::string name;
    double layerPropertyValue;

    // Used to store value if layer is frozen or not
    int freezeValue;

    // Used to determine if we are drawing frozen/invisible layers
    bool drawHidden;

    int layerExists;
    double currElevation;

    // Scale used for blocks
    double scale;
    int largeUnits;
    double textRotation;

    vtkSmartPointer<vtkPoints> arcPoints;
    vtkSmartPointer<vtkCellArray> arcCells;

    vtkSmartPointer<vtkPoints> pointPoints;
    vtkSmartPointer<vtkCellArray> pointCells;

    vtkSmartPointer<vtkPoints> polyLinePoints;
    vtkSmartPointer<vtkCellArray> polyLineCells;

    vtkSmartPointer<vtkPoints> lwPolyLinePoints;
    vtkSmartPointer<vtkCellArray> lwPolyLineCells;

    vtkSmartPointer<vtkPoints> linePoints;
    vtkSmartPointer<vtkCellArray> lineCells;

    vtkSmartPointer<vtkPoints> surfPoints;
    vtkSmartPointer<vtkCellArray> surfCells;

    vtkSmartPointer<vtkPoints> solidPoints;
    vtkSmartPointer<vtkCellArray> solidCells;

    vtkSmartPointer<vtkDoubleArray> pointProps;
    vtkSmartPointer<vtkDoubleArray> polyLineProps;
    vtkSmartPointer<vtkDoubleArray> lwPolyLineProps;
    vtkSmartPointer<vtkDoubleArray> lineProps;
    vtkSmartPointer<vtkDoubleArray> surfProps;
    vtkSmartPointer<vtkDoubleArray> circleProps;
    vtkSmartPointer<vtkDoubleArray> textProps;
    vtkSmartPointer<vtkDoubleArray> arcProps;
    vtkSmartPointer<vtkDoubleArray> solidProps;

    vtkSmartPointer<vtkCollection> textList;
    vtkSmartPointer<vtkCollection> circleList;

    // Used for BLOCK data
    vtkSmartPointer<vtkCollection> blockList;

};

#endif
