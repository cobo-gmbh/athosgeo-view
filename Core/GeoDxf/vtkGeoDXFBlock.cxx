/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFBlock.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// By: Eric Daoust && Matthew Livingstone
#include <sstream>
#include <vtkGeoDXFBlock.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkCollection.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

vtkStandardNewMacro(vtkGeoDXFBlock)

vtkGeoDXFBlock::vtkGeoDXFBlock()
:   vtkGeoDXFLayer(),
    blockPropertyValue(0),
    drawBlock(false)
{
    blockScale[0] = blockScale[1] = blockScale[2] = 1.0;
    blockTransform[0] = blockTransform[1] = blockTransform[2] = 0.0;
}

vtkGeoDXFBlock::~vtkGeoDXFBlock()
{
}

void vtkGeoDXFBlock::PrintSelf(ostream& os, vtkIndent indent)
{
    //TODO// complete this method
    Superclass::PrintSelf(os, indent);
}

void vtkGeoDXFBlock::CopyFrom(vtkGeoDXFBlock* block)
{
    // Point/Cell Data
    pointPoints->DeepCopy(block->getPointPoints());
    pointCells->DeepCopy(block->getPointCells());
    linePoints->DeepCopy(block->getLinePoints());
    lineCells->DeepCopy(block->getLineCells());
    polyLinePoints->DeepCopy(block->getPolyLinePoints());
    polyLineCells->DeepCopy(block->getPolyLineCells());
    lwPolyLinePoints->DeepCopy(block->getLWPolyLinePoints());
    lwPolyLineCells->DeepCopy(block->getLWPolyLineCells());
    surfPoints->DeepCopy(block->getSurfPoints());
    surfCells->DeepCopy(block->getSurfCells());
    solidPoints->DeepCopy(block->getSolidPoints());
    solidCells->DeepCopy(block->getSolidCells());
    arcPoints->DeepCopy(block->getArcPoints());
    arcCells->DeepCopy(block->getArcCells());

    for(int textItem = 0; textItem < block->getText()->GetNumberOfItems(); textItem++)
        textList->AddItem(vtkPolyData::SafeDownCast(block->getText()->GetItemAsObject(textItem)));

    for(int circleItem = 0; circleItem < block->getCircles()->GetNumberOfItems(); circleItem++)
        circleList->AddItem(vtkPolyData::SafeDownCast(block->getCircles()->GetItemAsObject(circleItem)));

    // Properties
    pointProps->DeepCopy(block->getPointProps());
    lineProps->DeepCopy(block->getLineProps());
    polyLineProps->DeepCopy(block->getPolyLineProps());
    lwPolyLineProps->DeepCopy(block->getLWPolyLineProps());
    surfProps->DeepCopy(block->getSurfProps());
    solidProps->DeepCopy(block->getSolidProps());
    arcProps->DeepCopy(block->getArcProps());
    circleProps->DeepCopy( block->getCircleProps() );
    textProps->DeepCopy(block->getTextProps());

    // from this class (BCO: these were missing)
    blockPropertyValue = block->blockPropertyValue;
    blockScale[0] = block->blockScale[0];
    blockScale[1] = block->blockScale[1];
    blockScale[2] = block->blockScale[2];
    blockTransform[0] = block->blockTransform[0];
    blockTransform[1] = block->blockTransform[1];
    blockTransform[2] = block->blockTransform[2];
    parentLayer = block->parentLayer;
    drawBlock = block->drawBlock;

    // from parent class (vtkGeoDXFLayer) (BCO: these were missing except the name)
    name = block->name;
    layerPropertyValue = block->layerPropertyValue;
    freezeValue = block->freezeValue;
    drawHidden = block->drawHidden;
    layerExists = block->layerExists;
    currElevation = block->currElevation;
    scale = block->scale;
    largeUnits = block->largeUnits;
    textRotation = block->textRotation;
}
