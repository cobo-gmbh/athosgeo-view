/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFParser.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// By: Eric Daoust && Matthew Livingstone

#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <boost/algorithm/string.hpp>
#include <vtkGeoDXFParser.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkTriangle.h>
#include <vtkCollection.h>
#include <vtkVectorText.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkDiskSource.h>
#include <vtkGeoDXFLayer.h>
#include <vtkGeoDXFBlock.h>
#include <vtkPointData.h>
#include <vtkGeoDXFObjectMap.h>

#define PI 3.141592653

vtkStandardNewMacro(vtkGeoDXFParser)

vtkGeoDXFParser::vtkGeoDXFParser()
:   layerExists(0),
    scale(1.0),
    currElevation(0.0),
    LineTypeScale(1.0), // If 0.0, objects that do not set their own scale will disappear
    XScale(1.0),
    YScale(1.0),
    ZScale(1.0),
    ExtMinX(0.0),
    ExtMaxX(0.0),
    PExtMinX(0.0),
    PExtMaxX(0.0),
    ExtMinY(0.0),
    ExtMaxY(0.0),
    PExtMinY(0.0),
    PExtMaxY(0.0),
    ExtMinZ(0.0),
    ExtMaxZ(0.0),
    PExtMinZ(0.0),
    PExtMaxZ(0.0),
    XAdj(0.0),
    YAdj(0.0),
    ViewX(0.0),
    ViewY(0.0),
    ViewZoom(0.0),
    blockRecordExists(false),
    IsBinary(false),
    NumUnknownSolid(0),
    CommandLine(0),
    blockList(vtkGeoDXFObjectMap::New())
{
}

vtkGeoDXFParser::~vtkGeoDXFParser()
{
    blockList->Delete();
}

std::string vtkGeoDXFParser::TrimWhiteSpace(std::string const& input)
{
    int size = 0;
    for(unsigned int i = 0; i < input.size(); ++i)
    {
        if(input.at(i) >= '0' && input.at(i) <= '9') //digit
            size++;
        else if(input.at(i) == '.')
            size++;
    }

    // make room for null termination by incrementing "size"
    size++;

    std::string output;
    std::ostringstream ostr(output);

    for(unsigned int x = 0; x < input.size(); x++)
    {
        if(input.at(x) >= '0' && input.at(x) <= '9') //digit
        {
            while((x < input.size()) &&
                  (((input.at(x) >= '0') && (input.at(x) <= '9')) ||
                   (input.at(x) == '.')))
            {
                ostr << input.at(x);
                x++;
            }
            ostr << '\0';
        }
    }

    return ostr.str();
}

int vtkGeoDXFParser::getIntFromLine(std::string const& line)
{
    std::string trimLine = TrimWhiteSpace(line);

    std::istringstream iss(trimLine);
    int outputInt;
    iss >> outputInt;

    return outputInt;
}

void vtkGeoDXFParser::getLineSafe(std::ifstream* file, std::string& line)
{
    // replace std::getline with a version that takes care of all possible combinations
    // of line endings: \r\n, \r or \n

    line.clear();

    // BCO 2019-01-29:
    // the original version assumed that a string must not contain
    // spaces, but this seems to be a wrong assumption - certainly now!
    // For this reason we are changing the strategy:
    // 1) read the entire line up to one of the above terminators, but including spaces
    // 2) trim the result, which will keep "inner" spaces intact

    if(IsBinary)
    {
        // read line until a NULL character
        while(!file->eof() && ('\000' != file->peek()))
            line.push_back(file->get());
    }

    else
    {
        // read any character up to a potential line terminator
        while(!file->eof() && (0 == ::strchr("\r\n", file->peek())))
            line.push_back(file->get());
    }

    // skip now exactly one line terminator - no matter which -, but also not more!
    if(!file->eof())
    {
        if('\r' == file->peek())
        {
            file->get();
            if(!file->eof() && ('\n' == file->peek()))
                file->get();
        }
        else if('\n' == file->peek())
            file->get();
    }

    // now remove preceding and terminating white space
    boost::trim(line);

    // original version that reads only up to some white space, and after that
    // simply runs into a mess if after an "inner space" some other chars are following...
    /*
    // skip leading white space
    while(!file->eof() && ::isspace(file->peek()))
        file->get();

    // read all characters that are not \r or \n
    while(!file->eof() && !::isspace(file->peek()) && (0 == ::strchr("\r\n", file->peek())))
        line.push_back(file->get());

    // skip trailing white space
    while(!file->eof() && isspace(file->peek()))
        file->get();

    // skip any \r and \n characters
    while(!file->eof() && (0 != ::strchr("\r\n", file->peek())))
        file->get();
    */
}

void vtkGeoDXFParser::Read(std::ifstream* file)
{
    if(IsBinary)
    {
        // Get the binary codes
        unsigned char code = file->get();
        if(255 == code)
            CommandLine = 255 * file->get() + file->get();
        else
            CommandLine = static_cast<int>(code);

        // That's not yet it!
        ValueLine.clear();

        // note:
        // now we should read the value with a data format according to group code
        // From https://www.fileformat.info/format/dxf/egff.htm :
        /*
         * Binary DXF
         *
         * The most commonly used form of DXF is stored in 7-bit ASCII characters,
         * but a binary format also uses the extension DXF. AutoCAD Release 10 was
         * the first to support binary DXF. Binary DXF files are usually 20 to 30
         * percent smaller than the ASCII version, and they load more quickly
         * into AutoCAD.
         *
         * Binary DXF files always begin with a specific 22-byte
         * identification string:
         *
         * AutoCAD Binary DXF<0Dh><0Ah><1Ah><00h>
         *
         * Binary DXF uses group-value pairs, too. Group codes are usually one byte,
         * followed by either a two-byte little-endian integer, an eight-byte IEEE
         * floating-point double value, or a zero-terminated string, depending on
         * the type of value associated with the group code's range. To represent
         * group codes greater than 254, the value 255 precedes a two-byte integer
         * group code.
         *
         * A third form of DXF known as DXB is an even simpler binary format. DXB
         * files are even smaller than the binary DXF format. DXB files are limited
         * to a small set of entities such as line, point, circle, arc, trace, solid,
         * polyline, and 3D face. Entities are indicated by their own byte code and
         * are immediately followed by the necessary data for that entity, in an
         * appropriate integer or floating-point format.
         *
         * A DXB file can be distinguished from a binary DXF file by the file
         * extension .DXB and by the fact that it always begins with a 19-byte
         * identification string:
         *
         * AutoCAD DXB 1.0<0Dh><0Ah><1Ah><00h>
         */
        // so far it looks like "hacking" all this would be a rather tedious job,
        // and thus time is not yet being assigned to it
    }

    else
    {
        // The file is read two lines at a time
        // command line is used to determine current mode (create new, add, etc.)
        // value line is corresponding data (Coordinate, etc.)
        std::string text1;
        getLineSafe(file, text1);
        CommandLine = getIntFromLine(text1.c_str());
        getLineSafe(file, ValueLine);
    }

    // BCO: we empirically add some provisions to avoid "subclasses" that are
    //      using codes 10/20/30 for some other purpose than adding coordinates
    //      by simply skipping all lines that follow such a subcode until the
    //      3 "misused" codes are "through"
    // note: while this avoids the worst cases of useless 0/0/0 coordinates in some files,
    //   it is far from providing proper support for subclass markers...
    std::set<std::string> avoidSubclasses(std::initializer_list<std::string>(
    {
        "AcDbPolyFaceMesh",
        "AcDbFaceRecord"
    }));

    // subclass markers are 100
    if((CommandLine == 100) && (avoidSubclasses.end() != avoidSubclasses.find(ValueLine)))
    {
        // skip everything until we have the code 30 (recursively)
        while(CommandLine != 30)
            Read(file);

        // get the next acceptable codes (recursively)
        Read(file);
    }
}

void vtkGeoDXFParser::ParseHeader(std::ifstream* file)
{
    Read(file);

    while(ValueLine != "ENDSEC")
    {
        if((CommandLine == 9) && (ValueLine == "$LTSCALE"))
        {
            Read(file);
            LineTypeScale = GetLine();
        }

        else if((CommandLine == 9) && (ValueLine == "$LIMMIN"))
        {
            Read(file);
            ExtMinX = GetLine();
            Read(file);
            ExtMinY = GetLine();

            if(ExtMinX < 0)
                XAdj = abs(ExtMinX);
            if(ExtMinY < 0)
                YAdj = abs(ExtMinY);
        }

        else if((CommandLine == 9) && (ValueLine == "$LIMMAX"))
        {
            Read(file);
            ExtMaxX = GetLine();
            Read(file);
            ExtMaxY = GetLine();
        }

        else if((CommandLine == 9) && (ValueLine == "$PLIMMIN"))
        {
            Read(file);
            PExtMinX = GetLine();
            Read(file);
            PExtMinY = GetLine();
        }

        else if((CommandLine == 9) && (ValueLine == "$PLIMMAX"))
        {
            Read(file);
            PExtMaxX = GetLine();
            Read(file);
            PExtMaxY = GetLine();
        }

        else if((CommandLine == 9) && (ValueLine == "$EXTMIN"))
        {
            Read(file);
            ViewX = GetLine();
            Read(file);
            ViewY = GetLine();

            if(ViewX < 0.0)
                ViewX = abs(ViewX);
            else
                ViewX = 0.0;
            if(ViewY < 0.0)
                ViewY = abs(ViewY);
            else
                ViewY = 0.0;
        }
        Read(file);
    }

    double PExtX = PExtMaxX - 0;
    double PExtY = PExtMaxY - 0;
    double PExtZ = PExtMaxZ - 0;

    double ExtX = ExtMaxX - 0;
    double ExtY = ExtMaxY - 0;
    double ExtZ = ExtMaxZ - 0;

    XScale = (PExtX/ExtX);
    YScale = (PExtY/ExtY);

    XAdj = ViewX;
    YAdj = ViewY;

    ZScale = 1.0;

    // Ensure that the scale is atleast 1
    if((XScale >= 10000) || (XScale <= -10000) || (ExtX == 0.0))
        XScale = 1.0;
    if((YScale >= 10000) || (YScale <= -10000) || (ExtY == 0.0))
        YScale = 1.0;
    if(ZScale == 0.0)
        ZScale = 1.0;
}

void vtkGeoDXFParser::ParseViewPort(std::ifstream* file)
{
    bool activeView = false;

    // May need to place a check for the active viewport (AutoCAD can have multiple
    // viewports)
    // Active viewport has CommandLine 2 and ValueLine "*ACTIVE"
    Read(file);
    while(CommandLine != 0)
    {
        if(CommandLine == 2 && ValueLine == "*ACTIVE")
        {
            activeView = true;
        }
        else if(CommandLine == 12 && activeView)
        {
            ViewX = GetLine();
        }
        else if(CommandLine == 22 && activeView)
        {
            ViewY = GetLine();
        }
        else if(CommandLine == 45 && activeView)
        {
            ViewZoom = GetLine();
        }
        Read(file);
    }
}

vtkGeoDXFObjectMap* vtkGeoDXFParser::ParseData(std::string const& name, bool DrawHidden, bool AutoScale)
{
    std::ifstream file;
    file.open(name, ios::in);
    vtkGeoDXFObjectMap* layerList = vtkGeoDXFObjectMap::New();

    // Used for dealing with BLOCK data in ParseData
    std::string blockName;
    std::string layerName;
    bool foundBlock = false;
    std::string currentLayerName;
    layerExists = 0;
    bool firstPoint = true;

    // Check whether the file is binary or text
    static const std::string binhdr("AutoCAD Binary DXF");
    char hdr[19];
    for(int i = 0; i < 18; ++i)
        hdr[i] = file.get();
    hdr[18] = '\000';
    IsBinary = 0 == binhdr.compare(hdr);
    file.seekg(0);

    // note: at this time, we are only recognizing the binary format, but NOT yet
    // supporting it!
    // for more details see comment inside the Read function
    if(IsBinary)
        return layerList;

    // Parse to get VIEWPORT data. Used for initial positions and
    // zoom information.
    // note BCO 2019-01-30: ValueLine can be empty and still valid !!
    Read(&file);
    while(ValueLine != "EOF")
    {
        if(CommandLine == 0 && ValueLine == "VIEWPORT")
        {
            ParseViewPort(&file);
        }
        else if(CommandLine == 0 && ValueLine == "VPORT")
        {
            ParseViewPort(&file);
        }
        else
        {
            Read(&file);
        }
    }

    // Second read for other data
    file.seekg(0, file.beg);
    Read(&file);

    // note BCO 2019-01-30: ValueLine can be empty and still valid !!
    while(ValueLine != "EOF")
    {
        if(CommandLine == 2 && ValueLine == "HEADER")
        {
            ParseHeader(&file);
        }

        else if(CommandLine == 2 && ValueLine == "LAYER")
        {
            // we are now in the section where we define layers (a.k.a color table)
            layerExists = 1;
            ParseColorTable(&file, layerList, DrawHidden);
        }

        else if(CommandLine == 2 && ValueLine == "BLOCK_RECORD")
        {
            // Block Records are used to create the initial block list, which all block methods
            // will search for to find specific blocks to add data to.
            ParseBlockRecords(&file);
        }

        else if(CommandLine == 2 && ValueLine == "ENTITIES")
        {
            // these are the entities that will be assigned to the created layers (if any exist)
            if(layerExists == 0)
            {
                // sometimes a DXF file does not have layers,
                // so in this case we create a default layer with value 0
                vtkSmartPointer<vtkGeoDXFBlock> layer = vtkSmartPointer<vtkGeoDXFBlock>::New();
                layer->setName("DefaultLayer");
                layer->setLayerPropertyValue(0);
                layer->setDrawHidden(DrawHidden);
                layerList->AddItem(layer->getName(), layer);
                layerExists = 1;
            }

            while(ValueLine != "ENDSEC")
            {
                if((ValueLine == "LINE") || (ValueLine == "3DLINE"))
                {
                    // this creates a line cell
                    ParseLine(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "POINT")
                {
                    // this creates many point cells
                    ParsePoints(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "POLYLINE")
                {
                    // this creates many poly lines (with an additional line if it is a closed poly line)
                    ParsePolyLine(&file, &currentLayerName, layerList, firstPoint, AutoScale);
                }
                else if(ValueLine == "LWPOLYLINE")
                {
                    // this creates many lines having common Z coordinates, and all lines are closed lines
                    ParseLWPolyLine(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "3DFACE")
                {
                    // this creates many triangle cells
                    ParseFace(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "TEXT" || ValueLine == "ATTRIB")
                {
                    // Creates a collection of vector text objects
                    ParseText(&file, &currentLayerName, layerList, false, AutoScale);
                }
                else if(ValueLine == "MTEXT")
                {
                    // Same as TEXT, with different scaling/transformations
                    ParseText(&file, &currentLayerName, layerList, true, AutoScale);
                }
                else if(ValueLine == "CIRCLE")
                {
                    // this creates a circle
                    ParseCircle(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "ARC")
                {
                    // this creates an arc
                    ParseArc(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(ValueLine == "SOLID")
                {
                    // this creates a polygon (3 or 4 verts)
                    ParseSolid(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(CommandLine == 0 && ValueLine == "INSERT")
                {
                    // INSERT commands insert blocks at given positions, with given scales
                    ParseInsert(&file, &currentLayerName, layerList, AutoScale);
                }
                else if(CommandLine == 0 && ValueLine == "DIMENSION")
                {
                    // I'm treating it like INSERT, but with NO transform
                    ParseDimension(&file, &currentLayerName, layerList, AutoScale);
                }
                else
                {
                    Read(&file);
                }
            }
        }

        else if(CommandLine == 0 && ValueLine == "BLOCK" && blockRecordExists)
        {
            vtkGeoDXFBlock *block;
            foundBlock = false;
            while(ValueLine != "ENDBLK")
            {
                if(CommandLine == 8)
                {
                    layerName = ValueLine;
                    vtkGeoDXFLayer* layer = GetLayer(layerName, layerList);

                    // if layer not found, then we have a file with no layers defined, so use the default one
                    if(layer == nullptr)
                    {
                        vtkErrorMacro("BLOCK found without LAYER reference. You cannot have a BLOCK outside a layer.");

                        // vtkGeoDXFObject will handle the nullptr returned
                        return nullptr;
                    }
                    if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
                       !DrawHidden)
                    {
                        // Negative colour means hidden block
                        CommandLine = 0;
                        ValueLine = "";
                    }
                    Read(&file);
                }
                else if(CommandLine == 2 && foundBlock == false && blockName != ValueLine)
                {
                    // Ensure that we do not try to look for another block,
                    // there are other CommandLine = 2 entries inside of the block
                    foundBlock = true;
                    blockName = ValueLine;
                    block = GetBlock(blockName, blockList);

                    if(block == nullptr)
                    {
                        vtkErrorMacro("BLOCK name does not match any entries in BLOCK listing.");

                        // vtkGeoDXFObject will handle the nullptr returned
                        return nullptr;
                    }
                }
                else if((ValueLine == "LINE") || (ValueLine == "3DLINE"))
                {
                    // this creates a line cell
                    ParseLineBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "POINT")
                {
                    // this creates many point cells
                    ParsePointsBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "POLYLINE")
                {
                    // this creates many poly lines (with an additional line if it is a closed poly line)
                    ParsePolyLineBlock(&file, &currentLayerName, layerList, firstPoint, block, AutoScale);
                }
                else if(ValueLine == "LWPOLYLINE")
                {
                    // this creates many lines having common Z coordinates, and all lines are closed lines
                    ParseLWPolyLineBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "3DFACE")
                {
                    // this creates many triangle cells
                    ParseFaceBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "TEXT" || ValueLine == "ATTRIB")
                {
                    // Creates a collection of vector text objects
                    ParseTextBlock(&file, &currentLayerName, layerList, false, block, AutoScale);
                }
                else if(ValueLine == "MTEXT")
                {
                    // Same as TEXT, with different scaling/transformations
                    ParseTextBlock(&file, &currentLayerName, layerList, true, block, AutoScale);
                }
                else if(ValueLine == "CIRCLE")
                {
                    // this creates a circle
                    ParseCircleBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "ARC")
                {
                    // this creates an arc
                    ParseArcBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else if(ValueLine == "SOLID")
                {
                    // this creates a polygon (3 or 4 verts)
                    ParseSolidBlock(&file, &currentLayerName, layerList, block, AutoScale);
                }
                else
                {
                    Read(&file);
                }
            }
        }

        else
        {
            // whatever else - just skip it
            Read(&file);
        }
    }
    file.close();

    return layerList;
}

// ParseColorTable occurs near the beginning of the DXF file.
// It's purpose is to find the names and properties from all the layer
// that are declared, and place them into a layer list (collection) for
// use/modification later
void vtkGeoDXFParser::ParseColorTable(std::ifstream* file, vtkGeoDXFObjectMap* layerList, bool DrawHidden)
{
    vtkSmartPointer<vtkGeoDXFBlock> currentLayer;
    std::string currentName = "";

    // Used to ensure that we only check for frozen layers once we have found one
    bool checkFrozen = false;

    Read(file);

    while(ValueLine != "ENDTAB")
    {
        if(CommandLine == 2)
        {
            // define new layer to be added to list
            currentName = ValueLine;
            currentLayer = vtkSmartPointer<vtkGeoDXFBlock>::New();
            currentLayer->setName(currentName);
            currentLayer->setDrawHidden(DrawHidden);
            layerList->AddItem(currentLayer->getName(), currentLayer);

            // Ensure that we only check for frozen layers once we have found one
            checkFrozen = true;
        }

        else if(CommandLine == 62)
        {
            // define associated color value to go with layer previously defined
            double value = GetLine();
            currentLayer->setLayerPropertyValue(value);
        }

        else if(CommandLine == 70 && checkFrozen == true)
        {
            double value = GetLine();
            if(value < 3 && value > -1)
            {
                currentLayer->setFreezeValue(value);
            }
        }

        Read(file);
    }
}

// ParseBlockRecords occurs near the beginning of the DXF file as well.
// It's purpose is to find the names and properties from all the blocks
// that are declared, and place them into a block list (collection) for
// use/modification later
void vtkGeoDXFParser::ParseBlockRecords(std::ifstream* file)
{
    blockRecordExists = true;
    vtkSmartPointer<vtkGeoDXFBlock> currentBlock;
    std::string currentName = "";
    Read(file);
    while(ValueLine != "ENDTAB")
    {
        if(CommandLine == 2)
        {
            // define new block to be added to list
            currentName = ValueLine;
            currentBlock = vtkSmartPointer<vtkGeoDXFBlock>::New();
            currentBlock->setName(currentName);
            blockList->AddItem(currentName, currentBlock);
        }
        Read(file);
    }
}

// Grab the layer with the given name from the layer list.
vtkSmartPointer<vtkGeoDXFLayer> vtkGeoDXFParser::GetLayer(std::string const& name,
                                                          vtkGeoDXFObjectMap* layerList)
{
    int size = layerList->GetNumberOfItems();

    // if there is only one then the first one is returned immeadiatly
    if(1 == size)
    {
        return (vtkGeoDXFLayer*)layerList->GetItemAsObject(0);
    }

    // find the layer with the name as the key
    if(layerList->GetItemAsObject(name))
    {
        return (vtkGeoDXFLayer*)layerList->GetItemAsObject(name);
    }

    // not found
    return nullptr;
}

// Grab the block from the given name from the given block list
// (each layer has it's own block list)
vtkSmartPointer<vtkGeoDXFBlock> vtkGeoDXFParser::GetBlock(std::string const& name,
                                                          vtkGeoDXFObjectMap* bl)
{
    int size = bl->GetNumberOfItems();

    // if there is only one then the first one is returned immeadiatly
    if(1 == size)
    {
        return bl->GetItemAsObject(0);
    }

    // find the block with the name as the key
    if(bl->GetItemAsObject(name))
    {
        return bl->GetItemAsObject(name);
    }

    // not found
    return nullptr;
}

// Used to push the string value from the file into a double
double vtkGeoDXFParser::GetLine()
{
    double value;
    textString << ValueLine;
    textString >> value;

    textString.clear();
    return value;
}

void vtkGeoDXFParser::ParsePolyLine(std::ifstream* file, std::string* name,
                                    vtkGeoDXFObjectMap* layerList,
                                    bool firstPoint, bool AutoScale)
{
    Read(file);
    bool closedPoly = false;
    currElevation = 0.0;
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    // Variables used to keep track of polyFace data
    bool skipVertex = false;
    bool polyFace = false;
    int vertCount = 6;
    int currVertNum = 0;
    bool vertSet = false;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(ValueLine != "SEQEND")
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with
            // no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }

            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getPolyLinePoints()->GetNumberOfPoints();
            pointId = layer->getPolyLinePoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        else if(CommandLine == 70)
        {
            std::string polyVal = TrimWhiteSpace(ValueLine);
            if(polyVal == "1")
            {
                // this defines a closed poly line, the first point will have
                // to be duplicated at the end of the list
                closedPoly = true;
            }
            else if(polyVal == "64")
            {
                polyFace = true;
                skipVertex = true;
                firstPoint = false;
            }
            else if(polyVal == "128")
            {
                skipVertex = true;
                firstPoint = false;
            }
        }

        else if(CommandLine == 71)
        {
            vertCount = (int)GetLine();

            // Added one extra end (6 points) as this count seems to leave them out.
            vertCount += 6;
        }

        // X Coordinate
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
            currVertNum++;
            vertSet = true;
        }

        // Y Coordinate
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30 || CommandLine == 31)
        {
            if(firstPoint == false)
            {
                xyz[2] = GetLine();

                // Check to see if the point needs the elevation applied to it
                if(xyz[2] != currElevation)
                {
                    xyz[2] += currElevation;
                }
            }
            else
            {
                // firstPoint = false;
                currElevation = GetLine();
            }
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        else if(CommandLine == 0)
        {
            if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
            {
                if(vertSet && !firstPoint)
                {
                    // Check to see if the object is in Model Space and needs to be scaled
                    if(inModelSpace && AutoScale)
                    {
                        xyz[0] += XAdj;
                        xyz[1] += YAdj;

                        xyz[0] *= XScale;
                        xyz[1] *= YScale;
                        xyz[2] *= ZScale;
                    }

                    layer->getPolyLinePoints()->InsertPoint(pointId, xyz);
                    layer->getPolyLineProps()->InsertNextTuple1(layerProp);
                    pointId++;
                    vertSet = false;
                }
                else
                {
                    if(firstPoint)
                    {
                        firstPoint = false;
                    }
                }
            }
            else
            {
                if(skipVertex)
                {
                    skipVertex = false;
                }
            }
        }

        Read(file);
    }

    if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
    {
        if(vertSet)
        {
            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getPolyLinePoints()->InsertPoint(pointId, xyz);
            layer->getPolyLineProps()->InsertNextTuple1(layerProp);
            pointId++;
        }
    }

    // Add the point IDs into the newly created cell
    int numPoints = layer->getPolyLinePoints()->GetNumberOfPoints() - startPointId;

    // Is the POLYLINE a PolyFace object?
    if(polyFace)
    {
        layer->getPolyLineCells()->InsertNextCell(7);

        // Create end "boxes" of PolyFace
        layer->getPolyLineCells()->InsertCellPoint(startPointId);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+1);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+2);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+3);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+4);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+5);
        layer->getPolyLineCells()->InsertCellPoint(startPointId);
        for(int i = 6; i < numPoints-5; i+=6)
        {
            layer->getPolyLineCells()->InsertNextCell(7);

            // Create end "boxes" of PolyFace
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+3);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);

            // Create connecting side lines
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-6);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-1);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-5);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
        }
    }

    else
    {
        // If it is a closed polyline, then the first point must be added at the
        // end in order to "close" the line
        if(closedPoly)
        {
            layer->getPolyLineCells()->InsertNextCell(numPoints+1);
        }
        else
        {
            layer->getPolyLineCells()->InsertNextCell(numPoints);
        }

        for(int i = 0; i < numPoints; i++)
        {
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
        }

        if(closedPoly)
        {
            layer->getPolyLineCells()->InsertCellPoint(startPointId);
        }
    }
}

void vtkGeoDXFParser::PrintSelf(ostream& os, vtkIndent indent)
{
    //TODO// complete this method
    Superclass::PrintSelf(os,indent);
}

void vtkGeoDXFParser::ParseFace(std::ifstream* file, std::string* name,
                                vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;
    vtkSmartPointer<vtkTriangle> cell;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            //if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }

            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getSurfPoints()->GetNumberOfPoints();
            pointId = layer->getSurfPoints()->GetNumberOfPoints();
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine >= 10 && CommandLine <= 14)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinats
        else if(CommandLine >= 20 && CommandLine <= 24)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine >= 30 && CommandLine <= 34)
        {
            xyz[2] = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getSurfPoints()->InsertPoint(pointId, xyz);
            layer->getSurfProps()->InsertNextTuple1(layerProp);
            pointId++;

            // Last point in the set of 3. Create the cell to hold our triangle data
            if(CommandLine == 32)
            {
                cell = vtkSmartPointer<vtkTriangle>::New();
                layer->getSurfCells()->InsertNextCell(cell);
            }
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    int numPoints = layer->getSurfPoints()->GetNumberOfPoints() - startPointId;
    layer->getSurfCells()->InsertNextCell(numPoints);
    for(int i = 0; i < numPoints; i++)
    {
        layer->getSurfCells()->InsertCellPoint(i+startPointId);
    }
}

void vtkGeoDXFParser::ParseInsert(std::ifstream* file, std::string* name,
                                  vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    std::string layerName, blockName;
    Read(file);
    double transforms[3];
    transforms[0] = transforms[1] = transforms[2] = 0.0;
    double scale[3];
    scale[0] = scale[1] = scale[2] = 1.0;
    bool inModelSpace = true;

    while(CommandLine != 0)
    {
        // Layer Name
        if(CommandLine == 8)
        {
            layerName = ValueLine;
        }

        // Block name
        else if(CommandLine == 2)
        {
            blockName = ValueLine;
        }

        // X Shift
        else if(CommandLine == 10)
        {
            transforms[0] = GetLine();
        }

        // Y Shift
        else if(CommandLine == 20)
        {
            transforms[1] = GetLine();
        }

        // Z Shift
        else if(CommandLine == 30)
        {
            transforms[2] = GetLine();
        }

        // X Scale
        else if(CommandLine == 41)
        {
            scale[0] = GetLine();
        }

        // Y Scale
        else if(CommandLine == 42)
        {
            scale[1] = GetLine();
        }

        // Z Scale
        else if(CommandLine == 43)
        {
            scale[2] = GetLine();
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        Read(file);
    }

    // If the block is set in the Paper Space, then undo the scaling
    // that was done before.
    if(!inModelSpace && AutoScale)
    {
        transforms[0] -= XAdj;
        transforms[1] -= YAdj;
        scale[0] /= XScale;
        scale[1] /= YScale;
        scale[2] /= ZScale;
    }

    // Otherwise, scale the coordinates like every other object
    else if(inModelSpace && AutoScale)
    {
        transforms[0] -= XAdj;
        transforms[1] -= YAdj;
        transforms[0] *= XScale;
        transforms[1] *= YScale;
        transforms[2] *= ZScale;
    }

    vtkGeoDXFLayer* layer;
    vtkGeoDXFBlock* block;

    layer = GetLayer(layerName, layerList);

    // If colour is negative, the layer is hidden
    if(layer->getLayerPropertyValue() >= 0 && layer->getFreezeValue() != 1)
    {
        block = GetBlock(blockName, blockList);
        if(block != nullptr)
        {
            vtkSmartPointer<vtkGeoDXFBlock> newBlock = vtkSmartPointer<vtkGeoDXFBlock>::New();

            // Copy the block into the layer's block list
            newBlock->CopyFrom(block);
            newBlock->setBlockTransform(transforms);
            newBlock->setBlockScale(scale);
            newBlock->setDrawBlock(true);
            newBlock->setBlockPropertyValue(layer->getLayerPropertyValue());

            layer->getBlockList()->AddItem(newBlock);
        }
    }
}

void vtkGeoDXFParser::ParseDimension(std::ifstream* file, std::string* name,
                                     vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    std::string layerName, blockName;
    Read(file);
    double transforms[3];
    transforms[0] = transforms[1] = transforms[2] = 0.0;
    double scale[3];
    scale[0] = scale[1] = scale[2] = 1.0;
    bool inModelSpace = true;

    while(CommandLine != 0)
    {
        // Layer Name
        if(CommandLine == 8)
        {
            layerName = ValueLine;
        }

        // Block name
        else if(CommandLine == 2)
        {
            blockName = ValueLine;
        }

        // X Shift
        else if(CommandLine == 10)
        {
            transforms[0] = GetLine();
        }

        // Y Shift
        else if(CommandLine == 20)
        {
            transforms[1] = GetLine();
        }

        // Z Shift
        else if(CommandLine == 30)
        {
            transforms[2] = GetLine();
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        Read(file);
    }

    // If the block is set in the Paper Space, then undo the scaling
    // that was done before.
    if(!inModelSpace && AutoScale)
    {
        transforms[0] -= XAdj;
        transforms[1] -= YAdj;
        scale[0] /= XScale;
        scale[1] /= YScale;
        scale[2] /= ZScale;
    }

    // Otherwise, scale the coordinates like every other object
    else if(inModelSpace && AutoScale)
    {
        transforms[0] -= XAdj;
        transforms[1] -= YAdj;
        transforms[0] *= XScale;
        transforms[1] *= YScale;
        transforms[2] *= ZScale;
    }

    vtkGeoDXFLayer *layer;
    vtkGeoDXFBlock *block;

    layer = GetLayer(layerName, layerList);
    block = GetBlock(blockName, blockList);
    if(block != nullptr)
    {
        vtkSmartPointer<vtkGeoDXFBlock> newBlock = vtkSmartPointer<vtkGeoDXFBlock>::New();

        // Copy the block into the layer's block list
        newBlock->CopyFrom(block);

        // DIMENSIONS don't seem to need any adjusting...
        transforms[0] = transforms[1] = transforms[2] = 0.0;
        newBlock->setBlockTransform(transforms);
        newBlock->setBlockScale(scale);
        newBlock->setDrawBlock(true);

        layer->getBlockList()->AddItem(newBlock);
    }
}

void vtkGeoDXFParser::ParseLine(std::ifstream* file, std::string* name,
                             vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    int startPointId = 0;
    int pointId = 0;
    vtkSmartPointer<vtkIdList> pointList = vtkSmartPointer<vtkIdList>::New();

    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            //if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }

            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getLinePoints()->GetNumberOfPoints();
            pointId = layer->getLinePoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinates
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine == 30 || CommandLine == 31)
        {
            xyz[2] = GetLine();
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getLinePoints()->InsertPoint(pointId, xyz);
            layer->getLineProps()->InsertNextTuple1(layerProp);
            pointList->InsertNextId( pointId ); //need to create the cell
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    layer->getLineCells()->InsertNextCell(pointList);
}

void vtkGeoDXFParser::ParsePoints(std::ifstream* file, std::string* name,
                                  vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getPointPoints()->GetNumberOfPoints();
            pointId = layer->getPointPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getPointPoints()->InsertPoint(pointId, xyz);
            layer->getPointProps()->InsertNextTuple1(layerProp);
            layer->getPointCells()->InsertNextCell(1);
            layer->getPointCells()->InsertCellPoint(pointId);
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }
}

void vtkGeoDXFParser::ParseLWPolyLine(std::ifstream* file, std::string* name,
                                      vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;

    // Used to determine if the lwpolyline is closed or not.
    double closedPoly = 0.0;
    double layerProp = 0;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    int startPointId = 0;
    int pointId = 0;
    vtkSmartPointer<vtkIdList> pointList = vtkSmartPointer<vtkIdList>::New();
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            // Grab the layer we are adding data to
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getLWPolyLinePoints()->GetNumberOfPoints();
            pointId = layer->getLWPolyLinePoints()->GetNumberOfPoints();

            //only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // Z Coordinate (Static)
        if(CommandLine == 38)
        {
            xyz[2] = GetLine();
        }

        else if(CommandLine == 70)
        {
            closedPoly = GetLine();
        }

        // X Coordinate
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getLWPolyLinePoints()->InsertPoint(pointId, xyz);
            layer->getLWPolyLineProps()->InsertNextTuple1(layerProp);
            pointList->InsertNextId( pointId );
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    if(closedPoly == 1.0)
    {
        pointList->InsertNextId( startPointId );
    }
    layer->getLWPolyLineCells()->InsertNextCell(pointList);
}

std::string vtkGeoDXFParser::CleanString(std::string &line, bool mText, int &numLine)
{
    // DXF files use %%, \, and | commands to manipulate the text
    // (%%uText%%u would underline the text)
    // This method will remove all instances of %% commands
    // as well as all slash (\) commands and | commands
    std::string::size_type idx = -1;

    // Replace MTEXT starting line, (A1;)
    idx = -1;
    std::string replaceMe = "\\A1;";
    std::string replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    // Replace new paragraph marks "\P" with "\n"
    idx = -1;
    replaceMe = "\\P";
    replaceWith = "\n";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
    {
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

        // Keep track of the number of new lines, will be used in the
        // jusification calculations
        numLine++;
    }

    // Replace new paragraph marks (P)
    idx = -1;
    replaceMe = "\\H";
    replaceWith = "\n";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str())+4, replaceWith);

    // Replace any %% command
    replaceMe = "%%";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str())+1, replaceWith);

    // Replace FROMI and Fromi
    idx = -1;
    replaceMe = "\\FROMI";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    idx = -1;
    replaceMe = "\\Fromi";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    // Replace the \F command for setting the font
    idx = -1;
    replaceMe = "\\F";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
    {
        int semiLocation = line.find(";", idx);
        line.replace(idx, ((semiLocation+1)-idx), replaceWith);
        idx = -1;
    }

    // Replace the \f command for setting the font
    idx = -1;
    replaceMe = "\\f";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
    {
        int semiLocation = line.find(";", idx);
        line.replace(idx, ((semiLocation+1)-idx), replaceWith);
        idx = -1;
    }

    // Replace the \C# commands
    idx = -1;
    replaceMe = "\\C";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str())+1, replaceWith);

    // Replace ;
    idx = -1;
    replaceMe = ";";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    // Replace any "\" command
    idx = -1;
    replaceMe = "\\";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str())+1, replaceWith);

    // Replace "+2205" command
    // Ignoring special characters in text.
    idx = -1;
    replaceMe = "+2205";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    // Replace Arial Black
    idx = -1;
    replaceMe = ";";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    // Replace { and }
    idx = -1;
    replaceMe = "{";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    idx = -1;
    replaceMe = "}";
    replaceWith = "";
    while((idx = line.find(replaceMe, idx+1)) != std::string::npos)
        line.replace(idx, strlen(replaceMe.c_str()), replaceWith);

    return line;
}

void vtkGeoDXFParser::ParseText(std::ifstream* file, std::string* name,
                                vtkGeoDXFObjectMap* layerList, bool mText, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;

    // Initialize the offsets to 0.0, this eliminates problems that
    // will happen later on if offset is nto initialized.
    double offset[3];
    offset[0] = offset[1] = offset[2] = 0.0;

    // Set default text scale to 1.0 (0 makes things disappear)
    double scale = 1.0;

    // Set default text rotation
    double textRotation = 0.0;

    // Justification:
    // 0 = left, 1 = center, 2 = right
    std::string justification = "0";

    // Length of the word/number to be justified
    int justLen = 0;

    // How many lines are in the text? (used for justification calculation)
    int numLine = 1;

    // Used to hold text data from multiple lines(CommandLine 3 and CommandLine 1)
    std::string line = "";
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    vtkSmartPointer<vtkVectorText> text = vtkSmartPointer<vtkVectorText>::New();

    while(CommandLine != 0)
    {
        // Grab the current layer that we are adding data to
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layerFound = true;
            layer = GetLayer(*name, layerList);
            layerProp = layer->getLayerPropertyValue();

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // X Alignment Offset
        else if(CommandLine == 11)
        {
            offset[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Y Alignment Offset
        else if(CommandLine == 21)
        {
            offset[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
        }

        // Z Alignment Offset
        else if(CommandLine == 31)
        {
            offset[2] = GetLine();
        }

        else if(CommandLine == 3)
        {
            line.append(ValueLine);
        }

        // Text data line
        else if(CommandLine == 1)
        {
            line.append(ValueLine);

            // Clean the AutoCAD junk from our text
            std::string cleanline = CleanString(line, mText, numLine);

            // Use our nice, clean text
            text->SetText(cleanline.c_str());

            // Save the length of the text added, will be used later for jusification
            justLen = line.length();
        }

        // The 40 command line is used to set the scale
        else if(CommandLine == 40)
        {
            scale = GetLine();

            // Adjust scale
            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                scale *= XScale;
            }

            // Autodesk and Paraview do not agree on scale, this fixes that
            scale *= 0.85;
        }

        // Text justification
        else if(CommandLine == 72)
        {
            justification = TrimWhiteSpace(ValueLine);
        }

        // Text rotation
        else if(CommandLine == 50)
        {
            textRotation = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Set text justification
    if(justification == "1")
    {
        // Center justification
        // MTEXT and TEXT elements need to aligned differently
        if(mText)
        {
            xyz[0] = xyz[0] - ((0.4*scale) * justLen/numLine);
        }
        else
        {
            xyz[0] = xyz[0] - ((0.1*scale) * (justLen/2));
        }
    }

    else if(justification == "2")
    {
        // Right justification
        if(mText)
        {
            xyz[0] = xyz[0] - ((0.8*scale) * justLen/numLine);
        }
        else
        {
            xyz[0] = xyz[0] - ((0.1*scale) * (justLen));
        }
    }

    // If there are offsets available, then use them
    // These offsets allow the text to be "aligned" properly
    // There is still an issue with the text not being in
    // the proper location. This is due to the text being
    // shifted for the alignment.
    // To have the text in the proper position, the text
    // must be grouped properly, and then the groups must
    // be shifted as a whole into the proper positions.
    // This is not possible with the way we are currently
    // handling the text data.
    // *NOT USING* Causes TEXT to be offset improperly
    // and MTEXT to go all over the place
    /*
    if(mText == true)
    {
        if(offset[0] != 0.0)
        {
            xyz[0] = xyz[0] - (offset[0] - xyz[0]);
        }
        if(offset[1] != 0.0)
        {
            xyz[1] = xyz[1] - (offset[1] - xyz[1]);
        }
        if(offset[2] != 0.0)
        {
            xyz[2] = xyz[2] - (offset[2] - xyz[2]);
        }
    }
    */

    // Check to see if the object is in Model Space and needs to be scaled
    if(inModelSpace && AutoScale)
    {
        xyz[0] += XAdj;
        xyz[1] += YAdj;

        xyz[0] *= XScale;
        xyz[1] *= YScale;
        xyz[2] *= ZScale;
    }

    // generate the 3D text
    text->Update();

    // The transform and transform filter are used to manipulate
    // the text object after it is created.
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    vtkSmartPointer<vtkTransformFilter> tf = vtkSmartPointer<vtkTransformFilter>::New();

    // Do not change the order of Translate and Scale, doing
    // so will cause the text to be placed in the wrong position
    transform->Translate(xyz);
    transform->Scale(scale, scale, scale);

    // Objects are drawn on the Z plane, so we can only scale on the Z axis.
    transform->RotateZ(textRotation);

    tf->SetInputData(text->GetOutput());
    tf->SetTransform(transform);
    tf->Update();

    vtkPolyData* pdo = tf->GetPolyDataOutput();
    if(pdo && pdo->GetPoints())
    {
        for(int i = 0; i < pdo->GetPoints()->GetNumberOfPoints(); i++)
        {
            layer->getTextProps()->InsertNextTuple1(layerProp);
        }
    }
    layer->getText()->AddItem(pdo);
}

void vtkGeoDXFParser::ParseCircle(std::ifstream* file, std::string* name,
                                  vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    double radius = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    vtkSmartPointer<vtkDiskSource> disk = vtkSmartPointer<vtkDiskSource>::New();

    // The transform and transform filter are used to manipulate
    // the text object after it is created.
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    vtkSmartPointer<vtkTransformFilter> tf = vtkSmartPointer<vtkTransformFilter>::New();

    while(CommandLine != 0)
    {
        // Grab the current layer that we are adding data to
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);
            layerFound = true;
            layerProp = layer->getLayerPropertyValue();

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        // The 40 command line is used to set the scale
        else if(CommandLine == 40)
        {
            radius = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                radius *= XScale;
            }

            disk->SetOuterRadius(radius);

            // make sure the inner radius is slightly less than the outer radius,
            // and ensure that this is the case for even small circles
            double innerRadius = (radius < 1.5) ?
                        radius - 0.035 :
                        radius - (0.030 * radius);
            disk->SetInnerRadius(innerRadius);
        }

        Read(file);
    }

    disk->SetCircumferentialResolution(10);

    // generate the circle
    disk->Update();

    // Check to see if the object is in Model Space and needs to be scaled
    if(inModelSpace && AutoScale)
    {
        xyz[0] += XAdj;
        xyz[1] += YAdj;

        xyz[0] *= XScale;
        xyz[1] *= YScale;
        xyz[2] *= ZScale;
    }

    // Do not change the order of Translate and Scale, doing
    // so will cause the text to be placed in the wrong position
    transform->Translate(xyz);

    tf->SetInputData(disk->GetOutput());
    tf->SetTransform(transform);
    tf->Update();

    vtkSmartPointer<vtkPolyData> pdo = tf->GetPolyDataOutput();
    if(pdo && pdo->GetPoints())
    {
        for(int i = 0; i < pdo->GetPoints()->GetNumberOfPoints(); i++)
        {
            layer->getTextProps()->InsertNextTuple1(layerProp);
        }
    }
    layer->getCircles()->AddItem(pdo);
}

void vtkGeoDXFParser::ParseArc(std::ifstream* file, std::string* name,
                               vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double centerPoint[3];
    centerPoint[0] = centerPoint[1] = centerPoint[2] = 0.0;
    double radius = 0;
    double beginAngle = 0;
    double endAngle = 0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getArcPoints()->GetNumberOfPoints();
            pointId = layer->getArcPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            centerPoint[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            centerPoint[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            centerPoint[2] = GetLine();
        }

        // Radius
        else if(CommandLine == 40)
        {
            radius = GetLine();
        }

        // Begin Angle
        else if(CommandLine == 50)
        {
            beginAngle = GetLine();
        }

        // End Angle
        else if(CommandLine == 51)
        {
            endAngle = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    double angleLength = endAngle - beginAngle;
    double x;
    double y;
    double z = centerPoint[2]; //always constant
    if(inModelSpace && AutoScale)
    {
        z *= ZScale;
    }

    // default angular distance between pts, may be reduced depending on angleLength
    double increment = 10.0;
    int numPts = angleLength / increment + 1;

    // for visibility purposes, we want at least 5 points.
    // this helps with arcs of short radian length.
    if(numPts < 5)
    {
        numPts = 5;
    }

    // new increment value so that points are uniformily distributed
    increment = angleLength / (numPts-1);
    int count = 0;
    double currentAngle;
    if(beginAngle < endAngle)
    {
        for(currentAngle = beginAngle;
            (currentAngle - endAngle) <= 0.000001;
            currentAngle += increment)
        {
            double cosResult = radius * cos(currentAngle * (PI / 180.0));
            double sinResult = radius * sin(currentAngle * (PI / 180.0));
            x = centerPoint[0] + cosResult;
            y = centerPoint[1] + sinResult;

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                x += XAdj;
                y += YAdj;

                x *= XScale;
                y *= YScale;
            }

            layer->getArcPoints()->InsertPoint(pointId, x, y, z);
            layer->getArcProps()->InsertNextTuple1(layerProp);
            pointId++;
            count++;
        }
        layer->getArcCells()->InsertNextCell(numPts);
    }

    // arc crosses 360 degrees and therefore endAngle < beginAngle
    else
    {
        double oldBegin = beginAngle;
        double oldEnd = endAngle;
        beginAngle = 180 - oldEnd;
        endAngle = 540 - oldBegin;
        angleLength = endAngle - beginAngle;
        increment = 10.0;
        numPts = angleLength / increment + 1;
        if(numPts < 5)
        {
            numPts = 5;
        }

        increment = angleLength / (numPts-1);
        for(currentAngle = beginAngle;
            (currentAngle - endAngle) <= 0.000001;
            currentAngle += increment)
        {
            x = centerPoint[0] - radius * cos(currentAngle * (PI / 180.0));
            y = centerPoint[1] + radius * sin(currentAngle * (PI / 180.0));

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                x += XAdj;
                y += YAdj;

                x *= XScale;
                y *= YScale;
            }

            layer->getArcPoints()->InsertPoint(pointId, x, y, z);
            layer->getArcProps()->InsertNextTuple1(layerProp);
            pointId++;
            count++;
        }

        layer->getArcCells()->InsertNextCell(numPts);
    }

    for(int i = 0; i < count; i++)
    {
        layer->getArcCells()->InsertCellPoint(i+startPointId);
    }
}

void vtkGeoDXFParser::ParseSolid(std::ifstream* file, std::string* name,
                                 vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;

    // Used to keep track of our vertices
    double xyzList[4][3];
    xyzList[0][0] = xyzList[0][1] = xyzList[0][2] =
    xyzList[1][0] = xyzList[1][1] = xyzList[1][2] =
    xyzList[2][0] = xyzList[2][1] = xyzList[2][2] =
    xyzList[3][0] = xyzList[3][1] = xyzList[3][2] = 0.0;
    int vertCount = 0;

    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // BCO 2020-11: empirically added a kind of "SOLID" that turns out to be a polyline,
        //   not found in the docs. In that example file the first code was
        //   number 70 - the polyline flag (in the case of polylines)
        if(CommandLine == 70)
        {
            ParseSolid2(file, name, layerList, AutoScale);
            return;
            // here we leave the function and the loop, in order to never return,
            // because this is an entirely different thing that we are parsing
        }

        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getSolidPoints()->GetNumberOfPoints();
            pointId = layer->getSolidPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine >= 10 && CommandLine <= 13)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinates
        else if(CommandLine >= 20 && CommandLine <= 23)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine >= 30 && CommandLine <= 33)
        {
            xyz[2] = GetLine();
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            xyzList[vertCount][0] = xyz[0];
            xyzList[vertCount][1] = xyz[1];
            xyzList[vertCount][2] = xyz[2];
            vertCount++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    if(xyzList[2][0] == xyzList[3][0] &&
       xyzList[2][1] == xyzList[3][1] &&
       xyzList[2][2] == xyzList[3][2])
    {
        vertCount--;
    }

    if(layerFound)
    {
        for(int pt = 0; pt < vertCount; pt++)
        {
            layer->getSolidPoints()->InsertPoint(pointId, xyzList[pt][0], xyzList[pt][1], xyzList[pt][2]);
            layer->getSolidProps()->InsertNextTuple1(layerProp);
            pointId++;
        }

        // Add the point IDs into the newly created cell
        int numPoints = layer->getSolidPoints()->GetNumberOfPoints() - startPointId;
        for(int i = 0; i < numPoints-2; i++)
        {
            layer->getSolidCells()->InsertNextCell(3);
            layer->getSolidCells()->InsertCellPoint(i+startPointId);
            layer->getSolidCells()->InsertCellPoint(i+1+startPointId);
            layer->getSolidCells()->InsertCellPoint(i+2+startPointId);
        }
    }
}

void vtkGeoDXFParser::ParseSolid2(std::ifstream* file, std::string* name,
                                  vtkGeoDXFObjectMap* layerList, bool AutoScale)
{
    // BCO 2020-11: This is a second version of the SOLID tag that was not found
    //   in any docs, but in some file. It is actually a polyline (maybe also mesh??)
    //   made up of line segments that do fit, with coordinates 10/20/30 to 11/21/31

    // note: this special format of SOLID is not what is documented in any DXF docs
    //   that I found so far, with 3 or 4 points. It looks like it describes a series
    //   of point pairs that are supposed to be put together to form a polyline or
    //   polygon, but maybe they could also be a grid in other files!? I do not know,
    //   and for the moment this format is not supported. The commented code below
    //   is copied from the ParsePolyLine function and would not work here as it is.
    // note: an example file with this strange SOLID construct is in the Examples
    //   directory together with this source code

    ++NumUnknownSolid;
    return;

    /*
    bool closedPoly = false;
    currElevation = 0.0;
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    // Variables used to keep track of polyFace data
    //bool skipVertex = false;
    //bool polyFace = false;
    int vertCount = 6;
    int currVertNum = 0;
    bool vertSet = false;
    int numVertices = 0;

    // the function is called from the ParseSolid function, and it is assumed that
    // code 70 was read (polyline flag) and the value is already in
    std::string polyVal = TrimWhiteSpace(ValueLine);
    if(polyVal == "1")
    {
        // this defines a closed poly line, the first point will have
        // to be duplicated at the end of the list
        closedPoly = true;
    }

    // note: these are taken over from the polyline, but we do not know if it
    // can happen also here
    //else if(polyVal == "64")
    //{
    //    polyFace = true;
    //    skipVertex = true;
    //    firstPoint = false;
    //}
    //else if(polyVal == "128")
    //{
    //    skipVertex = true;
    //    firstPoint = false;
    //}

    Read(file);

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with
            // no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }

            layerProp = layer->getLayerPropertyValue();
            startPointId = layer->getPolyLinePoints()->GetNumberOfPoints();
            pointId = layer->getPolyLinePoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        else if(CommandLine == 71)
        {
            vertCount = (int)GetLine();

            // Added one extra end (6 points) as this count seems to leave them out.
            vertCount += 6;
        }

        // X Coordinate
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
            currVertNum++;
            vertSet = true;
        }

        // Y Coordinate
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30 || CommandLine == 31)
        {
            if(firstPoint == false)
            {
                xyz[2] = GetLine();

                // Check to see if the point needs the elevation applied to it
                if(xyz[2] != currElevation)
                {
                    xyz[2] += currElevation;
                }
            }
            else
            {
                // firstPoint = false;
                currElevation = GetLine();
            }
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        else if(CommandLine == 0)
        {
            if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
            {
                if(vertSet && !firstPoint)
                {
                    // Check to see if the object is in Model Space and needs to be scaled
                    if(inModelSpace && AutoScale)
                    {
                        xyz[0] += XAdj;
                        xyz[1] += YAdj;

                        xyz[0] *= XScale;
                        xyz[1] *= YScale;
                        xyz[2] *= ZScale;
                    }

                    layer->getPolyLinePoints()->InsertPoint(pointId, xyz);
                    layer->getPolyLineProps()->InsertNextTuple1(layerProp);
                    pointId++;
                    vertSet = false;
                }
                else
                {
                    if(firstPoint)
                    {
                        firstPoint = false;
                    }
                }
            }
            else
            {
                if(skipVertex)
                {
                    skipVertex = false;
                }
            }
        }

        Read(file);
    }

    if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
    {
        if(vertSet)
        {
            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            layer->getPolyLinePoints()->InsertPoint(pointId, xyz);
            layer->getPolyLineProps()->InsertNextTuple1(layerProp);
            pointId++;
        }
    }

    // Add the point IDs into the newly created cell
    int numPoints = layer->getPolyLinePoints()->GetNumberOfPoints() - startPointId;

    // Is the POLYLINE a PolyFace object?
    if(polyFace)
    {
        layer->getPolyLineCells()->InsertNextCell(7);

        // Create end "boxes" of PolyFace
        layer->getPolyLineCells()->InsertCellPoint(startPointId);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+1);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+2);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+3);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+4);
        layer->getPolyLineCells()->InsertCellPoint(startPointId+5);
        layer->getPolyLineCells()->InsertCellPoint(startPointId);
        for(int i = 6; i < numPoints-5; i+=6)
        {
            layer->getPolyLineCells()->InsertNextCell(7);

            // Create end "boxes" of PolyFace
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+3);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);

            // Create connecting side lines
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-6);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-1);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-5);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            layer->getPolyLineCells()->InsertNextCell(2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId-2);
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
        }
    }

    else
    {
        // If it is a closed polyline, then the first point must be added at the
        // end in order to "close" the line
        if(closedPoly)
        {
            layer->getPolyLineCells()->InsertNextCell(numPoints+1);
        }
        else
        {
            layer->getPolyLineCells()->InsertNextCell(numPoints);
        }

        for(int i = 0; i < numPoints; i++)
        {
            layer->getPolyLineCells()->InsertCellPoint(i+startPointId);
        }

        if(closedPoly)
        {
            layer->getPolyLineCells()->InsertCellPoint(startPointId);
        }
    }
    */
}

// BLOCK PARSING METHODS
void vtkGeoDXFParser::ParseLineBlock(std::ifstream* file, std::string* name,
                                     vtkGeoDXFObjectMap* layerList,
                                     vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                vtkErrorMacro("BLOCK found without LAYER reference.");
            }

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getLinePoints()->GetNumberOfPoints();
            pointId = block->getLinePoints()->GetNumberOfPoints();

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinates
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine == 30 || CommandLine == 31)
        {
            xyz[2] = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            block->getLinePoints()->InsertPoint(pointId, xyz);
            block->getLineProps()->InsertNextTuple1(layerProp);
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    int numPoints = block->getLinePoints()->GetNumberOfPoints() - startPointId;
    block->getLineCells()->InsertNextCell(numPoints);
    for(int i = 0; i < numPoints; i++)
    {
        block->getLineCells()->InsertCellPoint(i+startPointId);
    }
}

void vtkGeoDXFParser::ParsePolyLineBlock(std::ifstream* file, std::string* name,
                                         vtkGeoDXFObjectMap* layerList, bool firstPoint,
                                         vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    bool closedPoly = false;
    currElevation = 0.0;
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    // Variables used to keep track of polyFace data
    bool skipVertex = false;
    bool polyFace = false;
    int vertCount = 6;
    int currVertNum = 0;
    bool vertSet = false;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(ValueLine != "SEQEND")
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                vtkErrorMacro("BLOCK found without LAYER reference. You cannot have a BLOCK outside a layer.");
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getPolyLinePoints()->GetNumberOfPoints();
            pointId = block->getPolyLinePoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        else if(CommandLine == 70)
        {
            std::string polyVal = TrimWhiteSpace(ValueLine);
            if(polyVal == "1")
            {
                // this defines a closed poly line, the first point will have
                // to be duplicated at the end of the list
                closedPoly = true;
            }
            else if(polyVal == "64")
            {
                polyFace = true;
                skipVertex = true;
                firstPoint = false;
            }
            else if(polyVal == "128")
            {
                skipVertex = true;
                firstPoint = false;
            }
        }

        else if(CommandLine == 71)
        {
            vertCount = (int)GetLine();
            // Added one extra end (6 points) as this count seems to leave them out.
            vertCount += 6;
        }

        // X Coordinate
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
            currVertNum++;
            vertSet = true;
        }

        // Y Coordinate
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30 || CommandLine == 31)
        {
            if(firstPoint == false)
            {
                xyz[2] = GetLine();

                // Check to see if the point needs the elevation applied to it
                if(xyz[2] != currElevation)
                {
                    xyz[2] += currElevation;
                }
            }
            else
            {
                currElevation = GetLine();
            }
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        else if(CommandLine == 0)
        {
            if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
            {
                if(vertSet && firstPoint == false)
                {
                    // Check to see if the object is in Model Space and needs to be scaled
                    if(inModelSpace && AutoScale)
                    {
                        xyz[0] += XAdj;
                        xyz[1] += YAdj;

                        xyz[0] *= XScale;
                        xyz[1] *= YScale;
                        xyz[2] *= ZScale;
                    }

                    block->getPolyLinePoints()->InsertPoint(pointId, xyz);
                    block->getPolyLineProps()->InsertNextTuple1(layerProp);
                    pointId++;
                    vertSet = false;
                }
                else
                {
                    if(firstPoint)
                    {
                        firstPoint = false;
                    }
                }
            }
            else
            {
                if(skipVertex)
                {
                    skipVertex = false;
                }
            }
        }

        Read(file);
    }

    if((currVertNum <= vertCount || vertCount == 6) && !skipVertex)
    {
        if(vertSet)
        {

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            block->getPolyLinePoints()->InsertPoint(pointId, xyz);
            block->getPolyLineProps()->InsertNextTuple1(layerProp);
            pointId++;
        }
    }

    // Add the point IDs into the newly created cell
    int numPoints = block->getPolyLinePoints()->GetNumberOfPoints() - startPointId;

    // Is the POLYLINE a PolyFace object?
    if(polyFace)
    {
        block->getPolyLineCells()->InsertNextCell(7);

        // Create end "boxes" of PolyFace
        block->getPolyLineCells()->InsertCellPoint(startPointId);
        block->getPolyLineCells()->InsertCellPoint(startPointId+1);
        block->getPolyLineCells()->InsertCellPoint(startPointId+2);
        block->getPolyLineCells()->InsertCellPoint(startPointId+3);
        block->getPolyLineCells()->InsertCellPoint(startPointId+4);
        block->getPolyLineCells()->InsertCellPoint(startPointId+5);
        block->getPolyLineCells()->InsertCellPoint(startPointId);
        for(int i = 6; i < numPoints-5; i+=6)
        {
            block->getPolyLineCells()->InsertNextCell(7);

            // Create end "boxes" of PolyFace
            block->getPolyLineCells()->InsertCellPoint(i+startPointId);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+3);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId);

            // Create connecting side lines
            block->getPolyLineCells()->InsertNextCell(2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId-6);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId);
            block->getPolyLineCells()->InsertNextCell(2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId-1);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+5);
            block->getPolyLineCells()->InsertNextCell(2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId-5);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+1);
            block->getPolyLineCells()->InsertNextCell(2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId-2);
            block->getPolyLineCells()->InsertCellPoint(i+startPointId+4);
        }
    }

    else
    {
        // If it is a closed polyline, then the first point must be added at the
        // end in order to "close" the line
        if(closedPoly)
        {
            block->getPolyLineCells()->InsertNextCell(numPoints+1);
        }
        else
        {
            block->getPolyLineCells()->InsertNextCell(numPoints);
        }
        for(int i = 0; i < numPoints; i++)
        {
            block->getPolyLineCells()->InsertCellPoint(i+startPointId);
        }
        if(closedPoly == true)
        {
            block->getPolyLineCells()->InsertCellPoint(startPointId);
        }
    }
}

void vtkGeoDXFParser::ParseLWPolyLineBlock(std::ifstream* file, std::string* name,
                                           vtkGeoDXFObjectMap* layerList,
                                           vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;

    // Used to ensure that the zCoordinate in only scaled once
    bool zChanged = false;

    // Used to determine if the lwpolyline is closed or not.
    double closedPoly = 0.0;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            // Grab the layer we are adding data to
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getLWPolyLinePoints()->GetNumberOfPoints();
            pointId = block->getLWPolyLinePoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        else if(CommandLine == 70)
        {
            closedPoly = GetLine();
        }

        // Z Coordinate (Static)
        if(CommandLine == 38)
        {
            xyz[2] = GetLine();
        }

        // X Coordinate
        else if(CommandLine == 10 || CommandLine == 11)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20 || CommandLine == 21)
        {
            xyz[1] = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;

                // Make sure we only scale the Z once, since it is static for the line
                if(zChanged == false)
                {
                    xyz[2] *= ZScale;
                    zChanged = true;
                }
            }

            block->getLWPolyLinePoints()->InsertPoint(pointId, xyz);
            block->getLWPolyLineProps()->InsertNextTuple1(layerProp);
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    int numPoints = block->getLWPolyLinePoints()->GetNumberOfPoints() - startPointId;
    if(closedPoly == 1.0)
    {
        block->getLWPolyLineCells()->InsertNextCell(numPoints+1);
    }
    else
    {
        block->getLWPolyLineCells()->InsertNextCell(numPoints);
    }

    for(int i = 0; i < numPoints; i++)
    {
        block->getLWPolyLineCells()->InsertCellPoint(i+startPointId);
    }

    if(closedPoly == 1.0)
    {
        // Insert first point again, to complete the line
        block->getLWPolyLineCells()->InsertCellPoint(startPointId);
    }
}

void vtkGeoDXFParser::ParseFaceBlock(std::ifstream* file, std::string* name,
                                     vtkGeoDXFObjectMap* layerList,
                                     vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getSurfPoints()->GetNumberOfPoints();
            pointId = block->getSurfPoints()->GetNumberOfPoints();
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine >= 10 && CommandLine <= 14)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinats
        else if(CommandLine >= 20 && CommandLine <= 24)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine >= 30 && CommandLine <= 34)
        {
            xyz[2] = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            block->getSurfPoints()->InsertPoint(pointId, xyz);
            block->getSurfProps()->InsertNextTuple1(layerProp);
            pointId++;

            // Last point in the set of 3. Create the cell to hold our triangle data
            if(CommandLine == 32)
            {
                // triangle has 3 points
                block->getSurfCells()->InsertNextCell(3);
            }
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    // Add the point IDs into the newly created cell
    int numPoints = block->getSurfPoints()->GetNumberOfPoints() - startPointId;
    block->getSurfCells()->InsertNextCell(numPoints);

    for(int i = 0; i < numPoints; i++)
    {
        block->getSurfCells()->InsertCellPoint(i+startPointId);
    }
}

void vtkGeoDXFParser::ParseTextBlock(std::ifstream* file, std::string* name,
                                     vtkGeoDXFObjectMap* layerList, bool mText,
                                     vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    vtkSmartPointer<vtkGeoDXFLayer> layer;
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    double offset[3];

    // Initialize the offsets to 0.0, this eliminates problems that
    // will happen later on if offset is nto initialized.
    offset[0] = offset[1] = offset[2] = 0.0;

    // Set default text scale to 1.0 (0 makes things disappear)
    double scale = 1.0;

    // Set default text rotation
    double textRotation = 0.0;

    // Justification:
    // 0 = left, 1 = center, 2 = right
    std::string justification = "0";

    // Length of the word/number to be justified
    int justLen = 0;

    // How many lines are in the text? (used for justification calculation)
    int numLine = 1;

    // Used to hold text data from multiple lines(CommandLine 3 and CommandLine 1)
    std::string line = "";
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkSmartPointer<vtkVectorText> text = vtkSmartPointer<vtkVectorText>::New();

    // The transform and transform filter are used to manipulate
    // the text object after it is created.
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    vtkSmartPointer<vtkTransformFilter> tf = vtkSmartPointer<vtkTransformFilter>::New();
    while(CommandLine != 0)
    {
        // Grab the current layer that we are adding data to
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);
            layerFound = true;
            layerProp = layer->getLayerPropertyValue();

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // X Alignment Offset
        else if(CommandLine == 11)
        {
            offset[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Y Alignment Offset
        else if(CommandLine == 21)
        {
            offset[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
        }

        // Z Alignment Offset
        else if(CommandLine == 31)
        {
            offset[2] = GetLine();
        }

        else if(CommandLine == 3)
        {
            line.append(ValueLine);
        }

        // Text data line
        else if(CommandLine == 1)
        {
            line.append(ValueLine);

            // Clean the AutoCAD junk from our text
            std::string cleanline = CleanString(line, mText, numLine);

            // Use our nice, clean text
            text->SetText(cleanline.c_str());

            // Save the length of the text added, will be used later for jusification
            justLen = line.length();
        }

        // The 40 command line is used to set the scale
        else if(CommandLine == 40)
        {
            scale = GetLine();

            // Adjust scale
            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                scale *= XScale;
            }

            // Autodesk and Paraview do not agree on scale, this fixes that
            scale *= 0.85;
        }

        // Text justification
        else if(CommandLine == 72)
        {
            justification = TrimWhiteSpace(ValueLine);
        }

        // Text rotation
        else if(CommandLine == 50)
        {
            textRotation = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }
        Read(file);
    }

    // Set text justification
    if(justification == "1")
    {
        // Center justification
        // MTEXT and TEXT elements need to aligned differently
        if(mText)
        {
            xyz[0] = xyz[0] - ((0.4*scale) * justLen/numLine);
        }
        else
        {
            xyz[0] = xyz[0] - ((0.1*scale) * (justLen/2));
        }
    }

    else if(justification == "2")
    {
        // Right justification
        if(mText)
        {
            xyz[0] = xyz[0] - ((0.8*scale) * justLen/numLine);
        }
        else
        {
            xyz[0] = xyz[0] - ((0.1*scale) * (justLen));
        }
    }

    // generate the text
    text->Update();

    // If there are offsets available, then use them
    // These offsets allow the text to be "aligned" properly
    // There is still an issue with the text not being in
    // the proper location. This is due to the text being
    // shifted for the alignment.
    // To have the text in the proper position, the text
    // must be grouped properly, and then the groups must
    // be shifted as a whole into the proper positions.
    // This is not possible with the way we are currently
    // handling the text data.
    // *NOT USING* Causes TEXT to be offset improperly
    // and MTEXT to go all over the place
    /*
    if(mText == true)
    {
        if(offset[0] != 0.0)
        {
            xyz[0] = xyz[0] - (offset[0] - xyz[0]);
        }
        if(offset[1] != 0.0)
        {
            xyz[1] = xyz[1] - (offset[1] - xyz[1]);
        }
        if(offset[2] != 0.0)
        {
            xyz[2] = xyz[2] - (offset[2] - xyz[2]);
        }
    }
    */

    // Check to see if the object is in Model Space and needs to be scaled
    if(inModelSpace && AutoScale)
    {
        xyz[0] += XAdj;
        xyz[1] += YAdj;

        xyz[0] *= XScale;
        xyz[1] *= YScale;
        xyz[2] *= ZScale;
    }

    // Do not change the order of Translate and Scale, doing
    // so will cause the text to be placed in the wrong position
    transform->Translate(xyz);
    transform->Scale(scale, scale, scale);

    // Objects are drawn on the Z plane, so we can only scale on the Z axis.
    transform->RotateZ(textRotation);

    tf->SetInputData(text->GetOutput());
    tf->SetTransform(transform);
    tf->Update();

    vtkPolyData* pdo = tf->GetPolyDataOutput();
    if(pdo && pdo->GetPoints())
    {
        for(int i = 0; i < pdo->GetPoints()->GetNumberOfPoints(); i++)
        {
            block->getTextProps()->InsertNextTuple1(layerProp);
        }
    }
    block->getText()->AddItem(pdo);
}

void vtkGeoDXFParser::ParsePointsBlock(std::ifstream* file, std::string* name,
                                       vtkGeoDXFObjectMap* layerList,
                                       vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getPointPoints()->GetNumberOfPoints();
            pointId = block->getPointPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }
        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }

            block->getPointPoints()->InsertPoint(pointId, xyz);
            block->getPointProps()->InsertNextTuple1(layerProp);
            block->getPointCells()->InsertNextCell(1);
            block->getPointCells()->InsertCellPoint(pointId);
            pointId++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }
}

void vtkGeoDXFParser::ParseCircleBlock(std::ifstream* file, std::string* name,
                                       vtkGeoDXFObjectMap* layerList,
                                       vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;
    double radius = 0.0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkSmartPointer<vtkGeoDXFLayer> layer;
    vtkSmartPointer<vtkDiskSource> disk = vtkSmartPointer<vtkDiskSource>::New();

    // The transform and transform filter are used to manipulate
    // the text object after it is created.
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    vtkSmartPointer<vtkTransformFilter> tf = vtkSmartPointer<vtkTransformFilter>::New();

    while(CommandLine != 0)
    {
        // Grab the current layer that we are adding data to
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);
            layerFound = true;
            layerProp = layer->getLayerPropertyValue();

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            xyz[2] = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        // The 40 command line is used to set the scale
        else if(CommandLine == 40)
        {
            radius = GetLine();

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                radius *= XScale;
            }

            double outerRadius = radius - (0.030 * radius);
            disk->SetOuterRadius(radius);
            if(radius < 1.5)
            {
                disk->SetInnerRadius(radius - 0.035);
            }
            else
            {
                disk->SetInnerRadius(outerRadius);
            }
        }

        Read(file);
    }

    disk->SetCircumferentialResolution(10);

    // generate the circle
    disk->Update();

    // Check to see if the object is in Model Space and needs to be scaled
    if(inModelSpace && AutoScale)
    {
        xyz[0] += XAdj;
        xyz[1] += YAdj;

        xyz[0] *= XScale;
        xyz[1] *= YScale;
        xyz[2] *= ZScale;
    }

    // Do not change the order of Translate and Scale, doing
    // so will cause the text to be placed in the wrong position
    transform->Translate(xyz);

    tf->SetInputData(disk->GetOutput());
    tf->SetTransform(transform);
    tf->Update();

    vtkPolyData* pdo = tf->GetPolyDataOutput();
    if(pdo && pdo->GetPoints())
    {
        for(int i = 0; i < pdo->GetPoints()->GetNumberOfPoints(); i++)
        {
            block->getTextProps()->InsertNextTuple1(layerProp);
        }
    }
    block->getCircles()->AddItem(pdo);
}

void vtkGeoDXFParser::ParseArcBlock(std::ifstream* file, std::string* name,
                                    vtkGeoDXFObjectMap* layerList,
                                    vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double centerPoint[3];
    centerPoint[0] = centerPoint[1] = centerPoint[2] = 0.0;
    double radius = 0;
    double beginAngle = 0;
    double endAngle = 0;
    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getArcPoints()->GetNumberOfPoints();
            pointId = block->getArcPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinate
        else if(CommandLine == 10)
        {
            centerPoint[0] = GetLine();
        }

        // Y Coordinate
        else if(CommandLine == 20)
        {
            centerPoint[1] = GetLine();
        }

        // Z Coordinate
        else if(CommandLine == 30)
        {
            centerPoint[2] = GetLine();
        }

        // Radius
        else if(CommandLine == 40)
        {
            radius = GetLine();
        }

        // Begin Angle
        else if(CommandLine == 50)
        {
            beginAngle = GetLine();
        }

        // End Angle
        else if(CommandLine == 51)
        {
            endAngle = GetLine();
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    double angleLength = endAngle - beginAngle;
    double x;
    double y;
    double z = centerPoint[2]; //always constant

    if(inModelSpace && AutoScale)
    {
        z *= ZScale;
    }

    // default angular distance between pts, may be reduced depending on angleLength
    double increment = 10.0;
    int numPts = angleLength / increment + 1;

    // for visibility purposes, we want at least 5 points.
    // this helps with arcs of short radian length.
    if(numPts < 5)
    {
        numPts = 5;
    }

    // new increment value so that points are uniformily distributed
    increment = angleLength / (numPts-1);
    int count = 0;
    double currentAngle;
    if(beginAngle < endAngle)
    {
        for(currentAngle = beginAngle; (currentAngle - endAngle) <= 0.000001; currentAngle += increment)
        {
            double cosResult = radius * cos(currentAngle * (PI / 180.0));
            double sinResult = radius * sin(currentAngle * (PI / 180.0));
            x = centerPoint[0] + cosResult;
            y = centerPoint[1] + sinResult;

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                x += XAdj;
                y += YAdj;

                x *= XScale;
                y *= YScale;
            }

            block->getArcPoints()->InsertPoint(pointId, x, y, z);
            block->getArcProps()->InsertNextTuple1(layerProp);
            pointId++;
            count++;
        }
        block->getArcCells()->InsertNextCell(numPts);
    }

    // arc crosses 360 degrees and therefore endAngle < beginAngle
    else
    {
        double oldBegin = beginAngle;
        double oldEnd = endAngle;
        beginAngle = 180 - oldEnd;
        endAngle = 540 - oldBegin;
        angleLength = endAngle - beginAngle;
        increment = 10.0;
        numPts = angleLength / increment + 1;
        if(numPts < 5)
        {
            numPts = 5;
        }

        increment = angleLength / (numPts-1);
        for(currentAngle = beginAngle; (currentAngle - endAngle) <= 0.000001; currentAngle += increment)
        {
            x = centerPoint[0] - radius * cos(currentAngle * (PI / 180.0));
            y = centerPoint[1] + radius * sin(currentAngle * (PI / 180.0));

            // Check to see if the object is in Model Space and needs to be scaled
            if(inModelSpace && AutoScale)
            {
                x += XAdj;
                y += YAdj;

                x *= XScale;
                y *= YScale;
            }

            block->getArcPoints()->InsertPoint(pointId, x, y, z);
            block->getArcProps()->InsertNextTuple1(layerProp);
            pointId++;
            count++;
        }

        block->getArcCells()->InsertNextCell(numPts);
    }

    for(int i = 0; i < count; i++)
    {
        block->getArcCells()->InsertCellPoint(i+startPointId);
    }
}

void vtkGeoDXFParser::ParseSolidBlock(std::ifstream* file, std::string* name,
                                      vtkGeoDXFObjectMap* layerList,
                                      vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    Read(file);
    double xyz[3];
    xyz[0] = xyz[1] = xyz[2] = 0.0;

    // Used to keep track of our vertices
    double xyzList[4][3];
    xyzList[0][0] = xyzList[0][1] = xyzList[0][2] =
        xyzList[1][0] = xyzList[1][1] = xyzList[1][2] =
        xyzList[2][0] = xyzList[2][1] = xyzList[2][2] =
        xyzList[3][0] = xyzList[3][1] = xyzList[3][2] = 0.0;
    int vertCount = 0;

    bool layerFound = false;
    bool inModelSpace = true;
    double layerProp = 0;

    vtkGeoDXFLayer* layer;
    int startPointId;
    int pointId;
    while(CommandLine != 0)
    {
        // BCO 2020-11: empirically added a kind of "SOLID" that turns out to be a polyline,
        //   not found in the docs. In that example file the first code was
        //   number 70 - the polyline flag (in the case of polylines)
        if(CommandLine == 70)
        {
            ParseSolid2Block(file, name, layerList, block, AutoScale);
            return;
            // here we leave the function and the loop, in order to never return,
            // because this is an entirely different thing that we are parsing
        }

        // Add colour properties to the property array
        if(CommandLine == 8 && !layerFound)
        {
            *name = ValueLine;
            layer = GetLayer(*name, layerList);

            // if layer not found, then we have a file with no layers defined, so use the default one
            if(layer == nullptr)
            {
                layer = GetLayer("DEFAULT", layerList);
            }
            layerProp = layer->getLayerPropertyValue();
            startPointId = block->getSolidPoints()->GetNumberOfPoints();
            pointId = block->getSolidPoints()->GetNumberOfPoints();

            // only define the layer once, as it is shared by all the objects in this section
            layerFound = true;

            // If the colour value is negative, the layer is invisible
            if((layer->getLayerPropertyValue() < 0 || layer->getFreezeValue() == 1) &&
               !layer->getDrawHidden())
            {
                return;
            }
        }

        else if(CommandLine == 67)
        {
            inModelSpace = false;
        }

        // X Coordinates
        else if(CommandLine >= 10 && CommandLine <= 13)
        {
            xyz[0] = GetLine();
        }

        // Y Coordinates
        else if(CommandLine >= 20 && CommandLine <= 23)
        {
            xyz[1] = GetLine();
        }

        // Z Coordinates
        else if(CommandLine >= 30 && CommandLine <= 33)
        {
            xyz[2] = GetLine();
            if(inModelSpace && AutoScale)
            {
                xyz[0] += XAdj;
                xyz[1] += YAdj;

                xyz[0] *= XScale;
                xyz[1] *= YScale;
                xyz[2] *= ZScale;
            }
            xyzList[vertCount][0] = xyz[0];
            xyzList[vertCount][1] = xyz[1];
            xyzList[vertCount][2] = xyz[2];
            vertCount++;
        }

        // Colour
        else if(CommandLine == 62)
        {
            layerProp = GetLine();
        }

        Read(file);
    }

    if(layerFound)
    {
        if(xyzList[2][0] == xyzList[3][0] &&
           xyzList[2][1] == xyzList[3][1] &&
           xyzList[2][2] == xyzList[3][2])
        {
            vertCount--;
        }

        for(int pt = 0; pt < vertCount; pt++)
        {
            block->getSolidPoints()->InsertPoint(pointId, xyzList[pt][0], xyzList[pt][1], xyzList[pt][2]);
            block->getSolidProps()->InsertNextTuple1(layerProp);
            pointId++;
        }

        // Add the point IDs into the newly created cell
        int numPoints = block->getSolidPoints()->GetNumberOfPoints() - startPointId;
        for(int i = 0; i < numPoints-2; i++)
        {
            block->getSolidCells()->InsertNextCell(3);
            block->getSolidCells()->InsertCellPoint(i+startPointId);
            block->getSolidCells()->InsertCellPoint(i+1+startPointId);
            block->getSolidCells()->InsertCellPoint(i+2+startPointId);
        }
    }
}

void vtkGeoDXFParser::ParseSolid2Block(std::ifstream* file, std::string* name,
                                       vtkGeoDXFObjectMap* layerList,
                                       vtkSmartPointer<vtkGeoDXFBlock> block, bool AutoScale)
{
    // see comments in ParseSolid2

    // note: the "Block" versions of the parse functions differ only in that they are
    //   adding their geometry data to a "block" structure and not a "layer"

    ++NumUnknownSolid;
    return;
}
