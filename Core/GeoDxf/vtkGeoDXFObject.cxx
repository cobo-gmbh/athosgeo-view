/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFObject.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// .NAME vtkGeoDXFObject.cxx
// By: Eric Daoust && Matthew Livingstone
// (Boolean statement above returns true)
// Read DXF file (.dxf) for single objects.

#include <sstream>
#include <string>
#include <vtkGeoDXFObject.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkTriangle.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkCollection.h>
#include <vtkGeoDXFLayer.h>
#include <vtkGeoDXFParser.h>
#include <vtkCleanPolyData.h>
#include <vtkVectorText.h>
#include <vtkAppendPolyData.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

#define POINT_NAME "Points"
#define LINE_NAME "Lines"
#define POLYLINE_NAME "PolyLines"
#define LWPOLY_NAME "LWPolyLines"
#define SURFACE_NAME "Surfaces"
#define TEXT_NAME "Text"
#define CIRCLE_NAME "Circles"
#define ARC_NAME "Arcs"
#define SOLID_NAME "Solids"
#define PROPERTY_NAME "Layer"

vtkStandardNewMacro(vtkGeoDXFObject)

vtkGeoDXFObject::vtkGeoDXFObject()
:   layerExists(0),
    type(0),
    FileName(nullptr),
    CommandLine(0),
    IsBinary(false),
    NumUnsupportedSolid(0),
    DrawPoints(false),
    DrawHidden(false),
    AutoScale(false)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkGeoDXFObject::~vtkGeoDXFObject()
{
    SetFileName(nullptr);
    CommandLine = 0;
    ValueLine = "";
}

void vtkGeoDXFObject::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os,indent);
    os << indent <<  "FileName: "
       << (FileName ? FileName : "(none)") << "\n";
}

int vtkGeoDXFObject::RequestData(vtkInformation* request,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
    vtkMultiBlockDataSet* output = vtkMultiBlockDataSet::SafeDownCast(GetExecutive()->GetOutputData(0));

    // note: delete mainLayer afterwards - and clean up toooons of memory
    vtkSmartPointer<vtkGeoDXFParser> mainLayer = vtkSmartPointer<vtkGeoDXFParser>::New();
    vtkGeoDXFObjectMap* layerList = mainLayer->ParseData(FileName, DrawHidden, AutoScale);
    SetIsBinary(mainLayer->isBinary());
    SetNumUnsupportedSolid(mainLayer->numUnknownSolid());

    if((nullptr == layerList) || GetIsBinary())
        return 1;

    // counting the blocks at the 1st level
    int mainBlockCounter = 0;

    for(int i = 0; i < layerList->GetNumberOfItems(); i++)
	{
        // grab layer that will be added from the list of layers
        vtkGeoDXFLayer* currLayer = layerList->GetItemAsObject(i);

        // generating a multi-block object from the layer
        vtkMultiBlockDataSet* currBlock = vtkMultiBlockDataSet::New();
        int elementCounter = 0;

        if(currLayer->getPointPoints()->GetNumberOfPoints() > 0 && DrawPoints)
		{
            // Create a new PolyData, to hold the point data
            vtkPolyData* pointObj = vtkPolyData::New();
			pointObj->SetPoints(currLayer->getPointPoints());
			pointObj->SetVerts(currLayer->getPointCells());

            // Add colour property
			if(currLayer->getPointProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getPointProps()->SetName(PROPERTY_NAME);
				pointObj->GetPointData()->AddArray(currLayer->getPointProps());
			}

			// Add the point data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, pointObj);
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), POINT_NAME);
			pointObj->Delete();

            elementCounter++;
		}

        if(currLayer->getLinePoints()->GetNumberOfPoints() > 0)
		{
            // Create a new PolyData, to hold the line data
            vtkPolyData* lineObj = vtkPolyData::New();
			lineObj->SetPoints(currLayer->getLinePoints());
			lineObj->SetLines(currLayer->getLineCells());

            // Add colour property
			if(currLayer->getLineProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getLineProps()->SetName(PROPERTY_NAME);
				lineObj->GetPointData()->AddArray(currLayer->getLineProps());
			}

			// Add the point data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, lineObj);
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), LINE_NAME);
			lineObj->Delete();

            elementCounter++;
		}

        if(currLayer->getPolyLinePoints()->GetNumberOfPoints() > 0)
		{
            // Create a new PolyData, to hold the line data
            vtkPolyData* polyLineObj = vtkPolyData::New();
			polyLineObj->SetPoints(currLayer->getPolyLinePoints());
			polyLineObj->SetLines(currLayer->getPolyLineCells());

            // Add colour property
			if(currLayer->getPolyLineProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getPolyLineProps()->SetName(PROPERTY_NAME);
				polyLineObj->GetPointData()->AddArray(currLayer->getPolyLineProps());
			}

			// Add the point data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, polyLineObj);
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), POLYLINE_NAME);
			polyLineObj->Delete();

            elementCounter++;
		}

        if(currLayer->getLWPolyLinePoints()->GetNumberOfPoints() > 0)
		{
            // Create a new PolyData, to hold the line data
            vtkPolyData* lwPolyLineObj = vtkPolyData::New();
			lwPolyLineObj->SetPoints(currLayer->getLWPolyLinePoints());
			lwPolyLineObj->SetLines(currLayer->getLWPolyLineCells());

            // Add colour property
			if(currLayer->getLWPolyLineProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getLWPolyLineProps()->SetName(PROPERTY_NAME);
				lwPolyLineObj->GetPointData()->AddArray(currLayer->getLWPolyLineProps());
			}

            // clean it
            vtkCleanPolyData* cleanData = vtkCleanPolyData::New();
            cleanData->SetInputData( lwPolyLineObj );
			cleanData->PointMergingOff();
			cleanData->Update();

			// Add the point data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, cleanData->GetOutput());
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), LWPOLY_NAME);
			
			cleanData->Delete();
			lwPolyLineObj->Delete();
			
            elementCounter++;
		}

        if(currLayer->getSurfPoints()->GetNumberOfPoints() > 0)
		{
            // 3dface
            // Create a new PolyData, to hold the surf data
            vtkPolyData* surfObj = vtkPolyData::New();
			surfObj->SetPoints(currLayer->getSurfPoints());
			surfObj->SetPolys(currLayer->getSurfCells());

            // Add colour property
			if(currLayer->getSurfProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getSurfProps()->SetName(PROPERTY_NAME);
				surfObj->GetPointData()->AddArray(currLayer->getSurfProps());
			}

			// Clean the object to remove duplicate points
            vtkCleanPolyData* cleanData = vtkCleanPolyData::New();
            cleanData->SetInputData(surfObj);
			cleanData->SetAbsoluteTolerance(0);

            // Merging data will cause problems with MultiBlockDataSets
			cleanData->SetConvertLinesToPoints(0);
			cleanData->SetConvertPolysToLines(0);
			cleanData->SetConvertStripsToPolys(0);
			cleanData->SetPieceInvariant(1);
			cleanData->SetPointMerging(1);
			cleanData->Update();

			// Add the point data to the MultiBlockDataSet
            double bounds[6];
            surfObj->GetBounds(bounds);
            currBlock->SetBlock(elementCounter, cleanData->GetOutput());
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), SURFACE_NAME);

			surfObj->Delete();
			cleanData->Delete();

            elementCounter++;
		}

        if(currLayer->getText()->GetNumberOfItems() > 0)
		{
            // Create a new AppendPolyData, to hold the text
            vtkAppendPolyData* textObj = vtkAppendPolyData::New();

            vtkCollection* textCollection = currLayer->getText();
			for(int i = 0; i < textCollection->GetNumberOfItems(); i++)
			{
                vtkPolyData* text = vtkPolyData::SafeDownCast(textCollection->GetItemAsObject(i));
                textObj->AddInputData(text);
				if(currLayer->getTextProps()->GetNumberOfTuples() > 0)
				{
					currLayer->getTextProps()->SetName(PROPERTY_NAME);
					text->GetPointData()->AddArray(currLayer->getTextProps());
				}
			}

			textObj->Update();

            currBlock->SetBlock(elementCounter, textObj->GetOutput());
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), TEXT_NAME);

			textObj->Delete();

            elementCounter++;
		}

        if(currLayer->getCircles()->GetNumberOfItems() > 0)
		{
            // Create a new PolyData, to hold the circle data
            vtkAppendPolyData* circleObj = vtkAppendPolyData::New();

            vtkCollection* circleCollection = currLayer->getCircles();

            // add inputs
			for(int i = 0; i < circleCollection->GetNumberOfItems(); i++)
			{
                vtkPolyData* circle = vtkPolyData::SafeDownCast(circleCollection->GetItemAsObject(i));
                circleObj->AddInputData(circle);

                // Add colour property
				if(currLayer->getCircleProps()->GetNumberOfTuples() > 0)
				{
					currLayer->getCircleProps()->SetName(PROPERTY_NAME);
					circle->GetPointData()->AddArray(currLayer->getCircleProps());
				}
			}

			circleObj->Update();

			// Add the circle data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, circleObj->GetOutput());
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), CIRCLE_NAME);

			circleObj->Delete();

            elementCounter++;
		}

        if(currLayer->getArcPoints()->GetNumberOfPoints() > 0)
		{
            // Create a new PolyData, to hold the arc data
            vtkPolyData* arcObj = vtkPolyData::New();
			arcObj->SetPoints(currLayer->getArcPoints());
			arcObj->SetLines(currLayer->getArcCells());

            // Add colour property
			if(currLayer->getArcProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getArcProps()->SetName(PROPERTY_NAME);
				arcObj->GetPointData()->AddArray(currLayer->getArcProps());
			}

			// Add the arc data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, arcObj);
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), ARC_NAME);

			arcObj->Delete();

            elementCounter++;
		}

        if(currLayer->getSolidPoints()->GetNumberOfPoints() > 0)
		{
            // Create a new PolyData, to hold the line data
            vtkPolyData* solidObj = vtkPolyData::New();
			solidObj->SetPoints(currLayer->getSolidPoints());
			solidObj->SetPolys(currLayer->getSolidCells());

            // Add colour property
			if(currLayer->getSolidProps()->GetNumberOfTuples() > 0)
			{
				currLayer->getSolidProps()->SetName(PROPERTY_NAME);
				solidObj->GetPointData()->AddArray(currLayer->getSolidProps());
			}

			// Add the point data to the MultiBlockDataSet
            currBlock->SetBlock(elementCounter, solidObj);
            currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), SOLID_NAME);

			solidObj->Delete();

            elementCounter++;
		}

        // ---- PARSE BLOCK DATA ----
		// If there are blocks in the layer, parse them now
		if(currLayer->getBlockList()->GetNumberOfItems() > 0)
		{
            int innerBlockCounter = 0;
            vtkCollection* blockCollection = currLayer->getBlockList();

            // Go through each block in the layer's block list
			for(int i = 0; i < blockCollection->GetNumberOfItems(); i++)
			{
                int innerElementCounter = 0;
                vtkMultiBlockDataSet* innerBlockSet = vtkMultiBlockDataSet::New();

                vtkGeoDXFBlock* innerBlock = vtkGeoDXFBlock::SafeDownCast(blockCollection->GetItemAsObject(i));
				if(innerBlock->getDrawBlock())
				{
                    if(innerBlock->getPointPoints()->GetNumberOfPoints() > 0  && DrawPoints)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* pointObj = vtkPolyData::New();
                        pointObj->SetPoints(innerBlock->getPointPoints());
                        pointObj->SetLines(innerBlock->getPointCells());

                        // Add colour property
                        if(innerBlock->getPointProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getPointProps()->SetName(PROPERTY_NAME);
                            pointObj->GetPointData()->AddArray(innerBlock->getPointProps());
                        }

                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(pointObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the 3DFace data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), POINT_NAME);

                        tf->Delete();
                        transform->Delete();
                        pointObj->Delete();

                        innerElementCounter++;
                    }

                    if(innerBlock->getLinePoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* lineObj = vtkPolyData::New();
                        lineObj->SetPoints(innerBlock->getLinePoints());
                        lineObj->SetLines(innerBlock->getLineCells());

                        // Add colour property
                        if(innerBlock->getLineProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getLineProps()->SetName(PROPERTY_NAME);
                            lineObj->GetPointData()->AddArray(innerBlock->getLineProps());
                        }

                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(lineObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the line data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), LINE_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        lineObj->Delete();
                    }

                    if(innerBlock->getPolyLinePoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* polyLineObj = vtkPolyData::New();
                        polyLineObj->SetPoints(innerBlock->getPolyLinePoints());
                        polyLineObj->SetLines(innerBlock->getPolyLineCells());

                        // Add colour property
                        if(innerBlock->getPolyLineProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getPolyLineProps()->SetName(PROPERTY_NAME);
                            polyLineObj->GetPointData()->AddArray(innerBlock->getPolyLineProps());
                        }
                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(polyLineObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the line data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), POLYLINE_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        polyLineObj->Delete();
                    }

                    if(innerBlock->getLWPolyLinePoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* lwPolyLineObj = vtkPolyData::New();
                        lwPolyLineObj->SetPoints(innerBlock->getLWPolyLinePoints());
                        lwPolyLineObj->SetLines(innerBlock->getLWPolyLineCells());

                        // Add colour property
                        if(innerBlock->getLWPolyLineProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getLWPolyLineProps()->SetName(PROPERTY_NAME);
                            lwPolyLineObj->GetPointData()->AddArray(innerBlock->getLWPolyLineProps());
                        }
                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(lwPolyLineObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the lwPolyLine data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), LWPOLY_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        lwPolyLineObj->Delete();
                    }

                    if(innerBlock->getSurfPoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* surfObj = vtkPolyData::New();
                        surfObj->SetPoints(innerBlock->getSurfPoints());
                        surfObj->SetLines(innerBlock->getSurfCells());

                        // Add colour property
                        if(innerBlock->getSurfProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getSurfProps()->SetName(PROPERTY_NAME);
                            surfObj->GetPointData()->AddArray(innerBlock->getSurfProps());
                        }
                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(surfObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the 3DFace data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), SURFACE_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        surfObj->Delete();
                    }

                    if(innerBlock->getText()->GetNumberOfItems() > 0)
                    {
                        // Create a new AppendPolyData, to hold the text
                        vtkAppendPolyData* textObj = vtkAppendPolyData::New();

                        vtkCollection* textCollection = innerBlock->getText();
                        for(int i = 0; i < textCollection->GetNumberOfItems(); i++)
                        {
                            vtkPolyData* text = vtkPolyData::SafeDownCast(textCollection->GetItemAsObject(i));
                            vtkTransform* transform = vtkTransform::New();
                            vtkTransformFilter* tf = vtkTransformFilter::New();
                            transform->Translate(innerBlock->getBlockTransform());
                            transform->Scale(innerBlock->getBlockScale());

                            tf->SetInputData(text);
                            tf->SetTransform(transform);

                            tf->Update();

                            textObj->AddInputData(tf->GetPolyDataOutput());
                            if(innerBlock->getTextProps()->GetNumberOfTuples() > 0)
                            {
                                innerBlock->getTextProps()->SetName(PROPERTY_NAME);
                                tf->GetPolyDataOutput()->GetPointData()->AddArray(innerBlock->getTextProps());
                            }

                            tf->Delete();
                            transform->Delete();
                        }

                        textObj->Update();

                        // Add the text data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, textObj->GetOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), TEXT_NAME);
                        innerElementCounter++;

                        textObj->Delete();
                    }
                    if(innerBlock->getCircles()->GetNumberOfItems() > 0)
                    {
                        // Create a new PolyData, to hold the circle data
                        vtkAppendPolyData* circleObj = vtkAppendPolyData::New();

                        vtkCollection* circleCollection = innerBlock->getCircles();

                        // add inputs
                        for(int i = 0; i < circleCollection->GetNumberOfItems(); i++)
                        {
                            vtkPolyData* circle = vtkPolyData::SafeDownCast(circleCollection->GetItemAsObject(i));
                            vtkTransform* transform = vtkTransform::New();
                            vtkTransformFilter* tf = vtkTransformFilter::New();
                            transform->Translate(innerBlock->getBlockTransform());
                            transform->Scale(innerBlock->getBlockScale());

                            tf->SetInputData(circle);
                            tf->SetTransform(transform);

                            tf->Update();

                            circleObj->AddInputData(tf->GetPolyDataOutput());

                            // Add colour property
                            if(innerBlock->getCircleProps()->GetNumberOfTuples() > 0)
                            {
                                innerBlock->getCircleProps()->SetName(PROPERTY_NAME);
                                tf->GetPolyDataOutput()->GetPointData()->AddArray(innerBlock->getCircleProps());
                            }

                            // rmaynard
                            tf->Delete();
                            transform->Delete();
                            circle->Delete();
                        }

                        circleObj->Update();

                        // Add the circle data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, circleObj->GetOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), CIRCLE_NAME);

                        innerElementCounter++;
                        circleObj->Delete();
                    }

                    if(innerBlock->getArcPoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the arc data
                        vtkPolyData* arcObj = vtkPolyData::New();
                        arcObj->SetPoints(innerBlock->getArcPoints());
                        arcObj->SetLines(innerBlock->getArcCells());

                        // Add colour property
                        if(innerBlock->getArcProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getArcProps()->SetName(PROPERTY_NAME);
                            arcObj->GetPointData()->AddArray(innerBlock->getArcProps());
                        }

                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(arcObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the arc data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), ARC_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        arcObj->Delete();
                    }

                    if(innerBlock->getSolidPoints()->GetNumberOfPoints() > 0)
                    {
                        // Create a new PolyData, to hold the line data
                        vtkPolyData* solidObj = vtkPolyData::New();
                        solidObj->SetPoints(innerBlock->getSolidPoints());
                        solidObj->SetPolys(innerBlock->getSolidCells());

                        // Add colour property
                        if(innerBlock->getSolidProps()->GetNumberOfTuples() > 0)
                        {
                            innerBlock->getSolidProps()->SetName(PROPERTY_NAME);
                            solidObj->GetPointData()->AddArray(innerBlock->getSolidProps());
                        }

                        vtkTransform* transform = vtkTransform::New();
                        vtkTransformFilter* tf = vtkTransformFilter::New();
                        transform->Translate(innerBlock->getBlockTransform());
                        transform->Scale(innerBlock->getBlockScale());

                        tf->SetInputData(solidObj);
                        tf->SetTransform(transform);

                        tf->Update();

                        // Add the solid data to the MultiBlockDataSet
                        innerBlockSet->SetBlock(innerElementCounter, tf->GetPolyDataOutput());
                        innerBlockSet->GetMetaData(innerElementCounter)->Set(vtkCompositeDataSet::NAME(), SOLID_NAME);
                        innerElementCounter++;

                        tf->Delete();
                        transform->Delete();
                        solidObj->Delete();
                    }

                    // Add the line data to the inner MultiBlockDataSet
                    currBlock->SetBlock(elementCounter, innerBlockSet);
                    std::string innerBlockName = innerBlock->getName();
                    currBlock->GetMetaData(elementCounter)->Set(vtkCompositeDataSet::NAME(), innerBlockName);
                    elementCounter++;
                    innerBlockSet->Delete();
                }
			}
		}

        // need to confirm that the block actually has something before we add it!
        if(elementCounter != 0)
        {
            output->SetBlock(output->GetNumberOfBlocks(), currBlock);
            vtkIdType last = output->GetNumberOfBlocks() - 1;
            output->GetMetaData(last)->Set(vtkCompositeDataSet::NAME(), currLayer->getName());
            ++mainBlockCounter;
        }

		currBlock->Delete();
	}

    layerList->Delete();

	return 1;
}

std::string vtkGeoDXFObject::GetObjectName()
{
    std::string fName = FileName;

    // Find position final '\' that occurs just before the file name
	int slashPosition = fName.find_last_of('\\');

    // sometimes path contains the other slash ('/')
	if(slashPosition == -1)
		slashPosition = fName.find_last_of('/');

    // Add one to slashPosition so that the slash is not included
    slashPosition = slashPosition + 1;

    // Find position of '.' that occurs before the file extension
	int dotPosition = fName.find_last_of('.');

    // Save the file extention into a stdString, so that the length that be found
    std::string extension = fName.substr(dotPosition);
	int extLen = extension.length();

    // Save the file name AND the file extention into a stdString, so that the length that be found
    std::string nameWithExt = fName.substr(slashPosition);
	int nameExtLen = nameWithExt.length();

    // Determine the length of the word, so that it can be taken from fName,
    // which has the file name and extension
    int finalNameLength = nameExtLen - extLen;

    // Pull the file name out of the full path (fName)
    std::string newName = fName.substr(slashPosition, finalNameLength);

	return newName;
}
