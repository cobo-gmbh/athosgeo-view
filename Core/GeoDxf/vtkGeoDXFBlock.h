/*=========================================================================

   Program: AthosGEO
   Module:  vtkGeoDXFBlock.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module is ported from the ParaViewGeo project
Copyright (c) 2012, Objectivity Inc.
All rights reserved.

========================================================================*/

// Block object for DXF layers
// By: Eric Daoust && Matthew Livingstone

#ifndef __vtkGeoDXFBlock_h
#define __vtkGeoDXFBlock_h

#include <sstream>
#include <vtkGeoDXFLayer.h>
#include <vtkCellArray.h>
#include <GeoDxfModule.h>

class vtkCollection;

class GEODXF_EXPORT vtkGeoDXFBlock: public vtkGeoDXFLayer
{
public:

    static vtkGeoDXFBlock* New();
    vtkTypeMacro(vtkGeoDXFBlock,vtkObject)
    void PrintSelf(ostream& os, vtkIndent indent);

    inline double getBlockPropertyValue()
    {
        return blockPropertyValue;
    }

    inline double* getBlockTransform()
    {
        return blockTransform;
    }

    inline double* getBlockScale()
    {
        return blockScale;
    }

    inline std::string getParentLayer()
    {
        return parentLayer;
    }

    inline bool getDrawBlock()
    {
        return drawBlock;
    }

    inline void setParentLayer(std::string name)
    {
        parentLayer = name;
    }

    inline void setBlockPropertyValue(double value)
    {
        blockPropertyValue = value;
    }

    inline void setBlockTransform(double* transform)
    {
        ::memcpy(blockTransform, transform, 3 * sizeof(double));
    }

    inline void setBlockScale(double* Scale)
    {
        ::memcpy(blockScale, Scale, 3 * sizeof(double));
    }

    inline void setDrawBlock(bool option)
    {
        drawBlock = option;
    }

    void CopyFrom(vtkGeoDXFBlock* block);

protected:

    vtkGeoDXFBlock();
    ~vtkGeoDXFBlock();

    double blockPropertyValue;
    double blockScale[3];
    double blockTransform[3];
    std::string parentLayer;
    bool drawBlock;

};

#endif
