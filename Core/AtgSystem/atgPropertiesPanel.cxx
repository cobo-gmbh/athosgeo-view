/*=========================================================================

   Program: AthosGEO
   Module:  atgPropertiesPanel.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <QMainWindow>
#include <QList>
#include <pqCoreUtilities.h>
#include <pqPropertiesPanel.h>
#include <atgPropertiesPanel.h>

pqPropertiesPanel* getPropertiesPanel()
{
    pqPropertiesPanel* ppw = nullptr;

    // find the properties panel even if the current widget has no parent yet
    // note: This is a rather "hackish" way to find the properties panel, by searching all the widget
    //   parent/child trees from the root (top level widgets). The problem is that
    //   the "this" widget does not have a valid parent at creation time yet
    // note: The children search needs to include the name "propertiesPanel" because in a ParaView
    //   setup where display and view panels are separate, these will also be "pqPropertiesPanel" item,
    //   but with different names!
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
    QList<pqPropertiesPanel*> ppwList = mainWindow->findChildren<pqPropertiesPanel*>("propertiesPanel");

    // differentiate cases: we need one and only one properties pane
    // - and if we have more we do not know how to differentiate them...
    switch(ppwList.size())
    {
        case 0:
        {
            // this should not happen in a "full" ParaView that always has a properties panel
            std::cout << "PROGRAM ERROR: No properties panel was detected" << std::endl;
            break;
        }

        case 1:
        {
            ppw = ppwList.first();
            break;
        }

        default:
        {
            // let's hope it does not happen (should not...)
            ppw = ppwList.first();
            std::cout << "WARNING (WidSelectionIds): There are more properties panels - no way to find the proper one..." << std::endl;
        }
    }

    return ppw;
}
