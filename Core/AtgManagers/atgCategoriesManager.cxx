/*=========================================================================

   Program: AthosGEO
   Module:  atgCategoriesManager.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <set>
#include <utilNormNames.h>
#include <utilDebugLog.h>
#include <pqApplicationCore.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkStringArray.h>

#define CATMGR_DLL
#include <atgCategoriesManager.h>

atgCategoriesManager::atgCategoriesManager(QObject* parent)
:   QObject(parent)
{
}

atgCategoriesManager::~atgCategoriesManager()
{
}

atgCategoriesManager& atgCategoriesManager::instance()
{
    // this will only run the first time the function is called
    static std::unique_ptr<atgCategoriesManager> cm(new atgCategoriesManager);

    // dereference here, so nobody will ever "delete" that pointer
    return *cm;
}

void atgCategoriesManager::findCategoriesValues(vtkUnstructuredGrid* grid, char const* PortId,
                                                std::string& currentCat, std::string& currentVal)
{
    // get the port Id that serves as a key for the data
    // in this case we cannot do anything
    if(nullptr == PortId)
        return;
    std::string pid(PortId);

    // this we are going to fill
    CatValMap cvm;

    // go through all the cell data arrays and see if they are of type "category"
    vtkCellData* cd = grid->GetCellData();
    for(vtkIdType c = 0; c < cd->GetNumberOfArrays(); ++c)
    {
        // ignore what is not "category" type
        std::string arrName = cd->GetArrayName(c);
        int ty = utilNormNames::getType(arrName);

        if(utilNormNames::TY_CATEGORY != ty)
            continue;

        // handle both numeric and string arrays
        vtkAbstractArray* arr = cd->GetAbstractArray(c);
        vtkStringArray* strArr = vtkStringArray::SafeDownCast(arr);
        vtkDataArray* numArr = vtkDataArray::SafeDownCast(arr);

        // here we collect the values of this category as strings
        ValVector strVals;

        // case: string array
        if(nullptr != strArr)
        {
            std::set<std::string> vals;
            for(vtkIdType r = 0; r < strArr->GetNumberOfTuples(); ++r)
                vals.insert(strArr->GetValue(r));

            strVals = std::vector<std::string>(vals.begin(), vals.end());
        }

        // case: numeric array
        else if(nullptr != numArr)
        {
            std::set<long long> vals;
            for(vtkIdType r = 0; r < numArr->GetNumberOfTuples(); ++r)
                vals.insert(static_cast<long long>(::fabs(numArr->GetVariantValue(r).ToDouble() + .5)));

            for(auto it = vals.begin(); it != vals.end(); ++it)
                strVals.push_back(std::to_string(*it));
        }

        // not really possible
        else
        {
            continue; // should not happen
        }

        // insert into category/value map
        cvm[arrName] = strVals;
    }

    // save and make sure the change is noted by the property widgets
    setCatValMap(pid, cvm);

    // see if current category and value exist and leave them like they are
    atgCategoriesManager::CatValMap::iterator catIt;
    if(!currentCat.empty() && !currentVal.empty())
    {
        catIt = cvm.find(currentCat);
        if(cvm.end() != catIt)
        {
            atgCategoriesManager::ValVector::iterator valIt =
                    std::find(catIt->second.begin(), catIt->second.end(), currentVal);
            if(catIt->second.end() != valIt)
                return;
        }
    }

    // otherwise set them to the first value of the first category -
    // provided that categories are available at all!
    if(cvm.empty())
    {
        currentCat = currentVal = "";
    }
    else
    {
        catIt = cvm.begin();
        currentCat = catIt->first;
        currentVal = catIt->second.front();
    }
}

void atgCategoriesManager::removeCategoriesIfFound(char const* PortId)
{
    // get the port Id that serves as a key for the data
    // in this case we cannot do anything
    if(nullptr == PortId)
        return;
    std::string pid(PortId);

    // do it
    removeCatValMap(pid);
}

void atgCategoriesManager::setCatValMap(std::string const& id, atgCategoriesManager::CatValMap const& cvm)
{
    catValues[id] = cvm;

    emit categoriesChanged(id);
}

void atgCategoriesManager::removeCatValMap(const std::string& id)
{
    auto it = catValues.find(id);
    if(catValues.end() != it)
        catValues.erase(it);
}

atgCategoriesManager::CatValMap atgCategoriesManager::getCatValMap(const std::string& id) const
{
    auto it = catValues.find(id);
    if(catValues.end() == it)
        return CatValMap();
    else
        return it->second;
}

std::string atgCategoriesManager::portIdNum(void* ptr)
{
    unsigned long long pid = reinterpret_cast<unsigned long long>(ptr);
    std::string str = std::to_string(pid);

    return str;
}
