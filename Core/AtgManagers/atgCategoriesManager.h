﻿/*=========================================================================

   Program: AthosGEO
   Module:  atgCategoriesManager.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef atgCategoriesManager_h
#define atgCategoriesManager_h

#include <string>
#include <vector>
#include <map>
#include <vtkSetGet.h>
#include <QObject>
#include <AtgManagersModule.h>

class vtkUnstructuredGrid;

class ATGMANAGERS_EXPORT atgCategoriesManager: public QObject
{
    Q_OBJECT

public:

    typedef std::vector<std::string> ValVector;
    typedef std::map<std::string, ValVector> CatValMap;

    // use this static function to get access to the one and only instance that resides
    // in the GetManager class
    // note: this is not true any more: the class is a "classical singleton", and the
    //   one and only instance is allocated in this function, with a managed pointer
    //   that takes care of destruction
    static atgCategoriesManager& instance();

    void findCategoriesValues(vtkUnstructuredGrid* grid, char const* PortId,
                              std::string& currentCat, std::string& currentVal);

    void removeCategoriesIfFound(char const* PortId);

    CatValMap getCatValMap(std::string const& id) const;

    static std::string portIdNum(void* ptr);

    // do not call: should be private which does not work
    ~atgCategoriesManager();

signals:

    void categoriesChanged(std::string const& id);

private:

    atgCategoriesManager(QObject* parent = 0);

    Q_DISABLE_COPY(atgCategoriesManager)

    void setCatValMap(std::string const& id, CatValMap const& cvm);
    void removeCatValMap(std::string const& id);

    std::map<std::string, CatValMap> catValues;

};

#endif
