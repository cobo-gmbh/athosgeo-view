/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgProgressObserver.cxx

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <algorithm>
#include <vtkAlgorithm.h>
#include <vtkObjectFactory.h>
#include <vtkAtgProgressObserver.h>

vtkStandardNewMacro(vtkAtgProgressObserver)

vtkAtgProgressObserver::vtkAtgProgressObserver()
:   Filter(nullptr),
    From(0.),
    To(1.),
    Amount(0.)
{
}

vtkAtgProgressObserver::~vtkAtgProgressObserver()
{
}

void vtkAtgProgressObserver::SetAlgorithm(vtkAlgorithm* algorithm)
{
    Filter = algorithm;
}

vtkAlgorithm* vtkAtgProgressObserver::GetAlgorithm() const
{
    return Filter;
}

void vtkAtgProgressObserver::SetRange(double from, double to)
{
    From = std::max<double>(0., from);
    To = std::min<double>(1., to);
}

void vtkAtgProgressObserver::GetRange(double& from, double& to) const
{
    from = From;
    to = To;
}

void vtkAtgProgressObserver::UpdateProgress(double amount)
{
    Amount = From + amount * (To - From);

    if(nullptr != Filter)
        Filter->UpdateProgress(amount);
}

double vtkAtgProgressObserver::GetProgress()
{
    return Amount;
}
