/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgNormalizeNames.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <map>
#include <set>
#include <vector>
#include <string>
#include <boost/algorithm/string/trim.hpp>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkDataSet.h>
#include <vtkCellData.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgNormalizeNames.h>

vtkStandardNewMacro(vtkAtgNormalizeNames)

vtkAtgNormalizeNames::vtkAtgNormalizeNames()
:   IncludeComponent(false),
    Error(nullptr)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgNormalizeNames::~vtkAtgNormalizeNames()
{
    SetError(nullptr);
}

int vtkAtgNormalizeNames::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: table with attributes
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        case 1:
            // optional reference data set
            // note: the purpose of this is to add columns from the reference data
            //   also to the main input, e.g. a correctives table, and fill them
            //   with all 0 values for DIRECT variables, or some "infinite" tonnage,
            //   in order to use this corrective table together with a model as
            //   input for optimization
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
            info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
            return 1;

        default:
            return 0;
    }
}

int vtkAtgNormalizeNames::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: table with added cement moduli
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgNormalizeNames::RequestData(vtkInformation* request,
                                      vtkInformationVector** inputVector,
                                      vtkInformationVector* outputVector)
{
    (void)request;

    // initialize
    SetError(nullptr);

    // get the input table
    vtkInformation* inTableInfo = inputVector[0]->GetInformationObject(0);
    vtkTable* inTable = vtkTable::SafeDownCast(inTableInfo->Get(vtkDataObject::DATA_OBJECT()));
    if(nullptr == inTable)
    {
        vtkOutputWindowDisplayWarningText("AtgNormalizeNames: No input data\n");
        return 1;
    }

    // get reference data set - if available
    vtkInformation* inRefInfo = inputVector[1]->GetInformationObject(0);
    vtkDataSet* inRef = nullptr;
    if(nullptr != inRefInfo)
        inRef = vtkDataSet::SafeDownCast(inRefInfo->Get(vtkDataObject::DATA_OBJECT()));

    // get the output table
    vtkInformation* outTableInfo = outputVector->GetInformationObject(0);
    vtkTable* outTable = vtkTable::SafeDownCast(outTableInfo->Get(vtkDataObject::DATA_OBJECT()));

    // first just copy the input
    outTable->ShallowCopy(inTable);

    // make sure all table column names are trimmed
    for(vtkIdType c = 0; c < outTable->GetNumberOfColumns(); ++c)
    {
        vtkAbstractArray* arr = outTable->GetColumn(c);
        std::string aname = arr->GetName();
        boost::trim(aname);
        arr->SetName(aname.c_str());
    }

    // go through all the columns and normalize the names
    // at the same time collect column name groups to be converted into components
    std::map<std::string, std::set<std::string>> compCols;
    for(vtkIdType col = 0; col < outTable->GetNumberOfColumns(); ++col)
    {
        std::string name = outTable->GetColumnName(col),
                    normName = utilNormNames::normalize(name, IncludeComponent);

        // if there is no normalized name: delete the column
        if(normName.empty())
        {
            AppendError("cannot handle input column <%s>", name);

            outTable->RemoveColumn(col);
            --col;
            continue;
        }

        // check if the new name already exists
        vtkAbstractArray* oldArr = outTable->GetColumnByName(normName.c_str());
        if(nullptr != oldArr)
        {
            for(auto ocol = 0; ocol < col; ++ocol)
            {
                if(oldArr == outTable->GetColumn(ocol))
                {
                    AppendError("normalized column name already exists <%s>", normName);
                    AppendError("(was initially <%s>)", name);
                }
            }
        }

        // otherwise rename it if required
        vtkAbstractArray* aarr = outTable->GetColumn(col);
        aarr->SetName(normName.c_str());

        // check if this is a the component of a compound column
        int colon = normName.find(':');
        if(0 < colon)
        {
            std::string cname = normName.substr(0, colon),
                         comp = normName.substr(colon + 1, normName.length());
            compCols[cname].insert(comp);
        }
    }

    // merge columns that have an index in their name
    for(auto it = compCols.begin(); it != compCols.end(); ++it)
    {
        // get standard component names for the column name
        std::vector<std::string> comps = utilNormNames::getComponents(it->first);
        if(comps.empty())
            continue;

        // check if the found components do match exactly the standard components
        std::set<std::string> compsSet;
        for(auto it2 = comps.begin(); it2 != comps.end(); ++it2)
            compsSet.insert(*it2);
        if(compsSet != it->second)
            continue;

        // check if the first of the component columns is a "data array" (and not
        // e.g. a string etc.)
        // note: it is assumed that all the other components are of the same type
        std::string name = it->first + ":" + comps.front();
        vtkDataArray* col = vtkDataArray::SafeDownCast(outTable->GetColumnByName(name.c_str()));
        if(nullptr == col)
        {
            vtkOutputWindowDisplayWarningText(("AtgNormalizeNames: " + name +
                                               " is not a purely numeric column\n").c_str());
            return 1;
        }

        // generate the new multi-component column, with same data type
        // as the first component
        vtkDataArray* compArray = vtkDataArray::CreateDataArray(col->GetDataType());
        compArray->SetName(it->first.c_str());
        compArray->SetNumberOfComponents(comps.size());
        compArray->SetNumberOfTuples(col->GetNumberOfTuples());

        // copy the existing single columns into the multi-component column,
        // then remove the single columns
        vtkIdType compId = 0;
        for(auto cit = comps.begin(); cit != comps.end(); ++cit)
        {
            // name the component
            compArray->SetComponentName(compId, cit->c_str());

            // get the source column
            name = it->first + ":" + *cit;
            vtkDataArray* col = vtkDataArray::SafeDownCast(outTable->GetColumnByName(name.c_str()));

            // copy to target, and remove source
            compArray->CopyComponent(compId++, col, 0);
            outTable->RemoveColumnByName(name.c_str());
        }

        // add the multi-component column to the output
        outTable->AddColumn(compArray);
        compArray->Delete();
    }

    // if there is a reference data set, apply it to the correctives table
    if(nullptr != inRef)
    {
        vtkCellData* cellRef = inRef->GetCellData();
        for(vtkIdType col = 0; col < cellRef->GetNumberOfArrays(); ++col)
        {
            std::string name = cellRef->GetArrayName(col);
            utilNormNames::Type ty = (utilNormNames::Type)utilNormNames::getType(name);

            bool add = false;
            double value = 0.;

            switch(ty)
            {
                case utilNormNames::TY_DIRECT:
                case utilNormNames::TY_RATIO:
                {
                    add = true;
                    break;
                }

                case utilNormNames::TY_TONNAGE:
                {
                    utilNormNames::Index inx = (utilNormNames::Index)utilNormNames::getIndex(name).first;
                    if((utilNormNames::KTONS == inx) ||
                       (utilNormNames::TONS == inx))
                    {
                        add = true;
                        value = 1000000000.;
                    }
                    break;
                }

                default:
                {
                    // ignore
                }
            }

            if(add)
            {
                if(0 == outTable->GetColumnByName(name.c_str()))
                {
                    vtkSmartPointer<vtkDoubleArray> arr = vtkSmartPointer<vtkDoubleArray>::New();
                    arr->SetName(name.c_str());
                    arr->SetNumberOfTuples(outTable->GetNumberOfRows());
                    arr->Fill(value);
                    outTable->AddColumn(arr);
                }
            }
        }
    }

    return 1;
}

int vtkAtgNormalizeNames::RequestInformation(vtkInformation* request,
                                           vtkInformationVector** inputVector,
                                           vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgNormalizeNames::AppendError(std::string const& msg, std::string const& column)
{
    std::string err;

    if(nullptr != GetError())
        err = GetError();

    err += msg;

    auto pos = err.find("%s");
    if(std::string::npos != pos)
        err.replace(pos, 2, column);

    SetError(err.c_str());
}

void vtkAtgNormalizeNames::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
