/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCsvReader.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/regex.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkTextCodec.h>
#include <vtkTextCodecFactory.h>
#include <vtkStringArray.h>
#include <vtkDoubleArray.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <utilAddGridCells.h>
#include <vtkAtgCsvReader.h>

template<typename Numeric>
bool is_number(const std::string& s)
{
    Numeric n;
    return((std::istringstream(s) >> n >> std::ws).eof());
}

typedef std::vector<std::string> CellRow;
static std::vector<CellRow> parseCsv(std::string const& csvStr, std::string const& fieldSep, bool tabs)
{
    // used to split the file in lines
    static const boost::regex linesregx("\\r\\n|\\n\\r|\\n|\\r");

    // used to split each line to tokens, assuming ',' as column separator
    std::string fieldregex_str = fieldSep;
    if(tabs)
        fieldregex_str += "\\t";
    fieldregex_str = "[" + fieldregex_str + "](?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
    boost::regex fieldsregx(fieldregex_str);

    std::vector<CellRow> result;

    // iterator splits data to lines
    boost::sregex_token_iterator li(csvStr.begin(), csvStr.end(), linesregx, -1);
    boost::sregex_token_iterator end;

    while(li != end)
    {
        std::string line = li->str();
        ++li;

        // Split line to tokens
        boost::sregex_token_iterator ti(line.begin(), line.end(), fieldsregx, -1);
        boost::sregex_token_iterator end2;

        std::vector<std::string> row;

        while(ti != end2)
        {
            std::string token = ti->str();
            ++ti;
            row.push_back(token);
        }

        if(line.back() == ',')
        {
            // last character was a separator
            row.push_back("");
        }

        result.push_back(row);
    }

    return result;
}

vtkStandardNewMacro(vtkAtgCsvReader)

vtkAtgCsvReader::vtkAtgCsvReader()
:   FileName(nullptr),
    FieldDelimiterCharacters(nullptr),
    EmptyStringReplacement(nullptr),
    AddTabFieldDelimiter(false),
    EmptyNumberReplacement(-1.),
    RemoveLastLineIfLessCellsThanHeader(false),
    WarningMessages(nullptr),
    ErrorMessage(nullptr),
    TreatExcelErrorsAsEmpty(false)
{
    SetFieldDelimiterCharacters(",");
    SetEmptyStringReplacement("--");

    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(1);
}

vtkAtgCsvReader::~vtkAtgCsvReader()
{
    SetEmptyStringReplacement(nullptr);
    SetFieldDelimiterCharacters(nullptr);
    SetFileName(nullptr);
    SetWarningMessages(nullptr);
    SetErrorMessage(nullptr);
}

int vtkAtgCsvReader::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: table with attributes
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCsvReader::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCsvReader::RequestData(vtkInformation* request,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
    (void)request; (void)inputVector;

    // start with no error message
    SetWarningMessages(nullptr);
    SetErrorMessage(nullptr);

    // get the output table
    vtkTable* output = vtkTable::GetData(outputVector->GetInformationObject(0));
    output->Initialize();

    // initial check
    if(nullptr == FileName)
        return 1;

    // this is where we are first storing all the data
    std::vector<CellRow> input;

    try
    {
        // read file into string
        std::ifstream inpStream(FileName, std::ios_base::in);
        std::string csvStr((std::istreambuf_iterator<char>(inpStream)), std::istreambuf_iterator<char>());
        inpStream.close();

        // split the file into rows and cells
        if((nullptr == FieldDelimiterCharacters) || 0 == (::strlen(FieldDelimiterCharacters)))
            SetFieldDelimiterCharacters(",");
        input = parseCsv(csvStr, FieldDelimiterCharacters, AddTabFieldDelimiter);
        csvStr.clear();
        if(input.empty())
            return 1;

        // trim all cells and remove quotes
        for(auto rit = input.begin(); rit != input.end(); ++rit)
        {
            for(auto cit = (*rit).begin(); cit != (*rit).end(); ++cit)
            {
                std::string cell = *cit;
                if(!cell.empty())
                {
                    boost::algorithm::trim(cell);
                    if(('\"' == cell.front()) && ('\"' == cell.back()))
                    {
                        cell.erase(0, 1);
                        cell.erase(cell.size() - 1);
                        boost::algorithm::trim(cell);
                    }

                    // recognize "special characters" beyond ASCII 0x21 to 0x7F
                    // and convert everything that goes beyond this range to '_'
                    std::istringstream isstr(cell);
                    std::istream* istr = dynamic_cast<std::istream*>(&isstr);
                    vtkTextCodec* textCodec = vtkTextCodecFactory::CodecToHandle(*istr);
                    if(nullptr != textCodec)
                    {
                        std::string cc;
                        try
                        {
                            while(!istr->eof())
                            {
                                const vtkTypeUInt32 cp = textCodec->NextUTF32CodePoint(*istr);
                                if((0x20 < cp) && (0x80 > cp))
                                    cc.push_back(static_cast<char>(cp));
                                else
                                    cc.push_back('_');
                            }
                        }
                        catch(...)
                        {
                            // catch all - do nothing but simply stop the decoding
                        }
                        cell = cc;
                        textCodec->Delete();
                    }
                }

                *cit = cell;
            }
        }

        // this is for cases where the last row of a table contains only some explanatory text
        // that should be dropped for the further processing
        if(RemoveLastLineIfLessCellsThanHeader)
        {
            unsigned long lenHeaderRow = input.front().size(),
                    lenLastRow = input.back().size();
            if(lenLastRow < lenHeaderRow)
                input.resize(input.size() - 1);
        }
    }
    catch (std::exception& e)
    {
        SetErrorMessage(e.what());
        return 1;
    }
    catch (...)
    {
        SetErrorMessage("Unknown exception during file reading");
        return 1;
    }

    // convert into columns
    // note: the CellRow type is used for columns here
    std::vector<CellRow> cols(input[0].size());
    CellsOutsideRangeExist = false;
    for(auto rit = input.begin(); rit != input.end(); ++rit)
    {
        // read a row and append to the columns
        for(unsigned long col = 0; col < cols.size(); ++col)
        {
            std::string cell = (col < (*rit).size()) ? (*rit).at(col) : "";
            cols[col].push_back(cell);
        }

        // if this is the first (header) row, we want to check if no headers are empty
        // note: if they are at the end, we clip them off, otherwise we put dummies in
        if(rit == input.begin())
        {
            while(!cols.empty() && cols[cols.size() - 1][0].empty())
                cols.resize(cols.size() - 1);

            for(unsigned long col = 0; col < cols.size(); ++col)
            {
                if(cols[col][0].empty())
                {
                    cols[col][0] = "Col_" + std::to_string(col);

                    std::string err = "Replacing an empty column header by \"" + cols[col][0] + "\"";
                    if(nullptr == GetWarningMessages())
                        SetWarningMessages(err.c_str());
                    else
                        SetWarningMessages((std::string(GetWarningMessages()) + "\n" + err).c_str());
                }
            }
        }

        // we check if there are cells with real content beyond the last one
        // of the header row
        // note: if we already found such a cell, we do not check for any further
        else if(!CellsOutsideRangeExist)
        {
            for(unsigned long col = cols.size(); col < (*rit).size(); ++col)
            {
                if(!(*rit).at(col).empty())
                {
                    CellsOutsideRangeExist = true;
                    break;
                }
            }
        }
    }
    input.clear();

    // This part is currently not required: also the vtkDelimitedTextReader doesn't merge
    // columns with : or _ into multi-component columns:
    // - the block model and sampling readers have to take care of that
    // - and it also makes more sense because there might be columns with _ that are not
    //   supposed to be merged at all!
    /*
    // see if some columns would belong together, i.e. components of one and the same
    // note: we are checking the pattern for ':' or '_' plus number, where the ':' comes
    //   in if the user saves with "Save Data" to CSV, and the '_' will be there if he
    //   saves with "Save Table" from the Table or SpreadSheet view
    std::vector<std::vector<int>> colInx;
    std::map<std::string,std::vector<std::string>> compNames;
    boost::regex header_comp_filt("(.*)([:_])(.+)");
    for(int col = 0; col < cols.size(); ++col)
    {
        // check if we have this column already as a component in the list
        bool found = false;
        for(auto cit = colInx.begin(); !found && (cit != colInx.end()); ++cit)
        {
            if((*cit).end() != std::find((*cit).begin(), (*cit).end(), col))
            {
                found = true;
                break;
            }
        }
        if(found)
            continue;

        // new index for new column
        std::vector<int> newInx;
        newInx.push_back(col);

        // see if this column is a first component
        std::string header = cols[col][0];
        boost::smatch match;
        if(boost::regex_match(header, match, header_comp_filt))
        {
            // separate main and component part
            std::string basename = match.str(1),
                        sep = match.str(2),
                        compname = match.str(3);
            std::vector<std::string> newComps;
            newComps.push_back(compname);

            // find the other components
            std::string next_cand_filt_str = basename + sep + "(.+)";
            boost::regex next_cand_filt(next_cand_filt_str);
            for(int ccol = col + 1; ccol < cols.size(); ++ccol)
            {
                if(boost::regex_match(cols[ccol][0], match, next_cand_filt))
                {
                    std::string compname = match.str(1);
                    newInx.push_back(ccol);
                    newComps.push_back(compname);
                }
            }

            // remember the component names
            compNames[basename] = newComps;
        }

        // add now to the index
        colInx.push_back(newInx);
    }

    // note: here the multi-column merging functionality is NOT finished: if it should
    //   be activated it still has to be done. So far only the headers are properly
    //   sorted out and the column and component names stored
    */

    // add columns
    for(auto cit = cols.begin(); cit != cols.end(); ++cit)
    {
        // find out if we have a string or numeric column
        // - first we want to know what data type would be expected from the column name
        utilNormNames::DataType dtype =
                (utilNormNames::DataType)utilNormNames::getDataType((*cit)[0].c_str());

        // - next we have a look what would follow from looking at the actual data
        bool non_num_vals = false;
        for(unsigned long row = 1; row < (*cit).size(); ++row)
        {
            // reference to the cell - for convenience
            std::string& cell = (*cit)[row];

            // first make empty if this is an Excel error and we have this option
            if(TreatExcelErrorsAsEmpty && !cell.empty() &&
               (cell.front() == '#') && (cell.back() == '!'))
                cell.clear();

            // get rid of "inner apostrophe" if this makes the result a number
            // - and at the same time find out if it really is a number
            std::string cellNA = boost::replace_all_copy<std::string>(cell, "\'", "");
            non_num_vals = !cell.empty() && !is_number<double>(cellNA);
            if(!non_num_vals)
                cell = cellNA;

            // if this is a non-numeric cell, we need no further checking
            if(non_num_vals)
                break;
        }

        // now we do some inconsistency checks regarding data type
        bool non_num_type = (utilNormNames::DTY_FLOAT != dtype) &&
                            (utilNormNames::DTY_INTEGER != dtype);
        if(non_num_vals && !non_num_type)
        {
            std::string err = "Column <" + (*cit)[0]
                            + ">: should be numerical, but string values found";
            appendWarningMessage(err.c_str());
        }
        if(!non_num_vals && (utilNormNames::DTY_STRING == dtype))
            non_num_vals = true;

        // add a string column
        if(non_num_vals)
        {
            vtkSmartPointer<vtkStringArray> arr = vtkSmartPointer<vtkStringArray>::New();
            arr->SetName((*cit)[0].c_str());
            arr->SetNumberOfTuples(static_cast<vtkIdType>((*cit).size()) - 1);
            for(unsigned long row = 1; row < (*cit).size(); ++row)
            {
                std::string cell = (*cit)[row];
                if(cell.empty())
                    cell = EmptyStringReplacement;
                arr->SetValue(static_cast<vtkIdType>(row) - 1, cell.c_str());
            }
            output->AddColumn(arr);
        }

        // add a numeric column
        else
        {
            vtkSmartPointer<vtkDoubleArray> arr = vtkSmartPointer<vtkDoubleArray>::New();
            arr->SetName((*cit)[0].c_str());
            arr->SetNumberOfTuples(static_cast<vtkIdType>((*cit).size()) - 1);
            for(unsigned long row = 1; row < (*cit).size(); ++row)
            {
                double value;
                std::string err;
                try
                {
                    value = (*cit)[row].empty() ? EmptyNumberReplacement : std::stod((*cit)[row]);
                }
                catch(std::invalid_argument const&)
                {
                    value = EmptyNumberReplacement;
                    err = "Not a number: <" + (*cit)[row] + "\"";
                }
                catch(std::out_of_range const&)
                {
                    value = EmptyNumberReplacement;
                    err = "Value out of range: <" + (*cit)[row] + "\"";
                }
                arr->SetValue(static_cast<vtkIdType>(row) - 1, value);
                if(!err.empty())
                {
                    if(nullptr == GetWarningMessages())
                        SetWarningMessages(err.c_str());
                    else
                        SetWarningMessages((std::string(GetWarningMessages()) + "\n" + err).c_str());
                }
            }
            output->AddColumn(arr);
        }
    }
    cols.clear();

    return 1;
}

void vtkAtgCsvReader::appendWarningMessage(std::string const& msg)
{
    std::string err;
    if(nullptr != GetWarningMessages())
       err = std::string(GetWarningMessages()) + "\n";
    err += msg;

    SetWarningMessages(err.c_str());
}

void vtkAtgCsvReader::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
