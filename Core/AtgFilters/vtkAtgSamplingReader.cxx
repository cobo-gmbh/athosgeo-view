/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSamplingReader.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkStringArray.h>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <vtkAtgCsvReader.h>
#include <vtkCellData.h>
#include <vtkAtgNormalizeNames.h>
#include <vtkAtgMergeCollarWithKey.h>
#include <vtkAtgMergeSurveyWithKey.h>
#include <vtkAtgTableToItemAndLabel.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgSamplingReader.h>

vtkStandardNewMacro(vtkAtgSamplingReader)

vtkAtgSamplingReader::vtkAtgSamplingReader()
:   AssayFile(nullptr),
    CollarFile(nullptr),
    SurveyFile(nullptr),
    NegativeDipDown(false),
    FieldDelimiterCharacters(nullptr),
    AddTabFieldDelimiter(false),
    SamplingIdPrefix(nullptr),
    EmptyCellString(nullptr),
    EmptyCellValue(-1.),
    Decorations(nullptr)
{
    SetNumberOfInputPorts(0);
    SetNumberOfOutputPorts(2);
}

vtkAtgSamplingReader::~vtkAtgSamplingReader()
{
    // this will free the memory for the strings
    SetDecorations(nullptr);
    SetEmptyCellString(nullptr);
    SetSamplingIdPrefix(nullptr);
    SetFieldDelimiterCharacters(nullptr);
    SetSurveyFile(nullptr);
    SetCollarFile(nullptr);
    SetAssayFile(nullptr);
}

void vtkAtgSamplingReader::PrintSelf(ostream& os, vtkIndent indent)
{
    Superclass::PrintSelf(os, indent);

    os << indent <<  "AssayFile: "
       << (this->AssayFile ? this->AssayFile : "(none)") << "\n";
    os << indent <<  "CollarFile: "
       << (this->CollarFile ? this->CollarFile : "(none)") << "\n";
    os << indent <<  "SurveyFile: "
       << (this->SurveyFile ? this->SurveyFile : "(none)") << "\n";
}

int vtkAtgSamplingReader::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0: // output geometry
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        case 1: // output labels
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgSamplingReader::RequestData(vtkInformation* request,
                                      vtkInformationVector** inputVector,
                                      vtkInformationVector* outputVector)
{
    (void)request;
    (void)inputVector;

    TRACK_FUNCTION_TIME

    // make sure we have a file to read
    if(!AssayFile || (0 == ::strlen(AssayFile)))
    {
        vtkOutputWindowDisplayWarningText("No (Assay) file name specified\n");
        return 0;
    }

    // make sure we have 3 valid strings for filenames - even if empty
    std::vector<std::string> infNames(std::initializer_list<std::string>(
    {
        AssayFile,
        ((nullptr == CollarFile) ? "" : CollarFile),
        ((nullptr == SurveyFile) ? "" : SurveyFile)
    }));

    // only if we have a depth range, we have holes, and only if we have holes, we
    // will take care of additional tables
    bool isHoles = true; // just to let the following loop start

    // read input files as far as present
    std::vector<vtkSmartPointer<vtkTable>> infTables(3, vtkSmartPointer<vtkTable>());
    for(unsigned long ii = 0; isHoles && (ii < 3); ++ii)
    {
        // collar and survey may be missing
        if(infNames[ii].empty())
            continue;

        // start by checking if the file exists at all
        std::ifstream file;
        file.open(infNames[ii], ios::in);
        bool ok = file.is_open();
        file.close();
        if(!ok)
        {
            if(0 == ii)
            {
                vtkOutputWindowDisplayWarningText("The (Assay) input file does not exist\n");
                return 1;
            }
            else
            {
                // the others are not mandatory, but if names are given we still tell
                // the user that we did not find them
                std::string msg = "Input file not found:\n" + infNames[ii] + "\nTrying without\n";
                vtkOutputWindowDisplayWarningText(msg.c_str());
                continue;
            }
        }

        TRACK_ELAPSED("start reading delimited text")

        // read the file into a vtkTable, using the user specified properties
        auto reader = vtkSmartPointer<vtkAtgCsvReader>::New();
        reader->SetFileName(infNames[ii].c_str());
        reader->SetFieldDelimiterCharacters(FieldDelimiterCharacters);
        reader->SetEmptyStringReplacement(EmptyCellString);
        reader->SetAddTabFieldDelimiter(false);
        reader->SetEmptyNumberReplacement(EmptyCellValue);
        reader->SetTreatExcelErrorsAsEmpty(true);
        reader->SetRemoveLastLineIfLessCellsThanHeader(true);
        reader->Update();

        // reading errors are fatal
        if(nullptr != reader->GetErrorMessage())
        {
            std::string msg = std::string("Error reading file:\n") + reader->GetErrorMessage();
            vtkOutputWindowDisplayWarningText(msg.c_str());
            return 1;
        }

        // we treat reading errors of single values like "empty cells" and report them as warning
        if(nullptr != reader->GetWarningMessages())
        {
            std::string msg = std::string("Warning:\n") + reader->GetWarningMessages();
            vtkOutputWindowDisplayWarningText(msg.c_str());
        }

        // if we have no rows or columns, this is fatal
        if(0 == reader->GetOutput()->GetNumberOfColumns())
        {
            std::string msg = "Input table has no columns:\n" + infNames[ii] + "\n";
            vtkOutputWindowDisplayWarningText(msg.c_str());
            return 1;
        }
        if(0 == reader->GetOutput()->GetNumberOfRows())
        {
            std::string msg = "Input table has no rows:\n" + infNames[ii] + "\n";
            vtkOutputWindowDisplayWarningText(msg.c_str());
            return 1;
        }

        TRACK_ELAPSED("start normalizing columns")

        // normalize the column names, including upper/lower case
        vtkSmartPointer<vtkAtgNormalizeNames> colNorm =
                vtkSmartPointer<vtkAtgNormalizeNames>::New();
        colNorm->SetIncludeComponent(true);
        colNorm->SetInputData(reader->GetOutput());
        colNorm->Update();

        // report error during column name normalization
        if(nullptr != colNorm->GetError())
        {
            std::string err = std::string("Error during column name normalization:\n") + colNorm->GetError();
            vtkOutputWindowDisplayWarningText(err.c_str());
            return 1;
        }

        // get current date and time, to be used as default
        time_t rawTime;
        ::time(&rawTime);
#ifdef MSVC
        struct tm dtInfoMem;
        struct tm* dtInfo = &dtInfoMem;
        ::localtime_s(dtInfo, &rawTime);
#else
        struct tm* dtInfo = ::localtime(&rawTime);
#endif

        // if we have a date column, normalize the dates and make them ISO norm format
        vtkTable* normTable = colNorm->GetOutput();
        std::string dateName = utilNormNames::getName(utilNormNames::DATE);
        for(vtkIdType col = 0; col < normTable->GetNumberOfColumns(); ++col)
        {
            // see if this is a date array
            if(normTable->GetColumnName(col) != dateName)
                continue;
            vtkStringArray* dateArr = vtkStringArray::SafeDownCast(normTable->GetColumn(col));
            if(nullptr == dateArr)
                continue;

            // go through the rows and normalize the date string, or replace by default
            for(vtkIdType row = 0; row < dateArr->GetNumberOfValues(); ++row)
            {
                // we generate one or two vectors of ulong, representing day/month/year and optionally
                // also time time (hours/min/sec)
                std::vector<std::string> sepVec(std::initializer_list<std::string>({"-/.", ":"}));
                std::vector<std::vector<unsigned long>> ulVec;
                std::vector<std::string> dateTime;
                boost::split(dateTime, dateArr->GetValue(row), boost::is_any_of(" "));
                for(int p = 0; p < std::min<int>(2, dateTime.size()); ++p)
                {
                    std::vector<unsigned long> ulv;
                    std::vector<std::string> svec;
                    boost::split(svec, dateTime[p], boost::is_any_of(sepVec[p]));
                    for(auto it = svec.begin(); it != svec.end(); ++it)
                    {
                        // note: we want to allow a dot (like for seconds with decimals), but then
                        // ignore the digits (so only accept the integer part of seconds)
                        bool isNum = true;
                        for(std::string::size_type i = 0; isNum && ('.' != (*it)[i]) && (i < it->size()); ++i)
                            isNum = ((*it)[i] >= '0') && ((*it)[i] <= '9');
                        if(isNum)
                            ulv.push_back(std::atoi(it->c_str()));
                    }
                    ulVec.push_back(ulv);
                }

                // if we have no valid date, we work with the current date and time as default
                if(ulVec.empty() || (3 > ulVec[0].size()))
                {
                    ulVec.clear();
                    std::vector<unsigned long> dt(std::initializer_list<unsigned long>(
                                                      {static_cast<unsigned long>(dtInfo->tm_year) + 1900,
                                                       static_cast<unsigned long>(dtInfo->tm_mon) + 1,
                                                       static_cast<unsigned long>(dtInfo->tm_mday)}));
                    ulVec.push_back(dt);
                    std::vector<unsigned long> tim(std::initializer_list<unsigned long>(
                                                      {static_cast<unsigned long>(dtInfo->tm_hour),
                                                       static_cast<unsigned long>(dtInfo->tm_min),
                                                       static_cast<unsigned long>(dtInfo->tm_sec)}));
                    ulVec.push_back(tim);
                }

                // if the last number is >100, we assume it is the year and switch first and last
                if(100 <= ulVec[0][2])
                {
                    unsigned long temp = ulVec[0][2];
                    ulVec[0][2] = ulVec[0][0];
                    ulVec[0][0] = temp;
                }

                // rebuild date in standard format
                std::stringstream sstr;
                sstr << std::fixed << std::setfill('0')
                     << std::setw(4) << ulVec[0][0]
                     << '-' << std::setw(2) << ulVec[0][1]
                     << '-' << ulVec[0][2];
                if(1 < ulVec.size())
                {
                    for(int i = 0; i < std::max<int>(3, ulVec[1].size()); ++i)
                        sstr << ((0 == i) ? ' ' : ':') << ulVec[1][i];
                }

                // write the normalizeddate back to the array
                dateArr->SetValue(row, sstr.str());
            }
        }

        // remember table for further processing
        infTables[ii] = vtkSmartPointer<vtkTable>::New();
        infTables[ii]->DeepCopy(normTable);

        // special cases:
        // - if we have a Center attribute, but no Collar, we rename the Center to Collar
        // note: this is for the case that the table had X,Y,Z initially - which looks like quite a
        //   reasonable input not only for a block model, but also for a sampling table
        // - if we have a Size:Y attribute, but no other Size attributes, it should become MaxDepth
        // note: this happens if the user had written Length for the MaxDepth attribute
        std::string collarName = utilNormNames::getName(utilNormNames::COLLAR),
                    centerName = utilNormNames::getName(utilNormNames::BLOCKCENTER),
                    sizeName = utilNormNames::getName(utilNormNames::BLOCKSIZE),
                    maxdepthName = utilNormNames::getName(utilNormNames::MAXDEPTH);
        std::pair<int, int> fullInx = utilNormNames::getIndex(sizeName, 1);
        std::string sizeyName = utilNormNames::getFullName(fullInx.first, fullInx.second);
        if(nullptr == infTables[ii]->GetColumnByName(collarName.c_str()))
        {
            vtkDataArray* centArr = vtkDataArray::SafeDownCast(infTables[ii]->GetColumnByName(centerName.c_str()));
            if(nullptr != centArr)
                centArr->SetName(collarName.c_str());
        }
        if((nullptr == infTables[ii]->GetColumnByName(maxdepthName.c_str())) &&
           (nullptr == infTables[ii]->GetColumnByName(sizeName.c_str())))
        {
            vtkDataArray* sizeyArr = vtkDataArray::SafeDownCast(infTables[ii]->GetColumnByName(sizeyName.c_str()));
            if(nullptr != sizeyArr)
                sizeyArr->SetName(maxdepthName.c_str());
        }

        // only if we have a depth range we will take care of any additional tables!
        if(0 == ii)
        {
            std::string depthFromName = utilNormNames::getName(utilNormNames::DEPTH);
            isHoles = nullptr != infTables[0]->GetColumnByName(depthFromName.c_str());
        }
    }

    // we may need this if there are tables to be merged
    // note: the tables merger will check if hole id exists and otherwise generate
    //   an error message that will be handled here in the loop
    std::string holeIdName = utilNormNames::getName(utilNormNames::HOLEID);

    // if we have a collar table, we are going to merge it into the assay table
    // note: can only happen if we are dealing with holes and having a depth attribute
    if(infTables[1])
    {
        TRACK_ELAPSED("start merging collar table with assay table")

        // merge data from collar table into assay table
        vtkSmartPointer<vtkAtgMergeCollarWithKey> tabsMerge =
                vtkSmartPointer<vtkAtgMergeCollarWithKey>::New();
        tabsMerge->SetInputData(0, infTables[0]);
        tabsMerge->SetInputData(1, infTables[1]);
        tabsMerge->SetKeyColumn(holeIdName.c_str());
        tabsMerge->SetFirstTableName("assay table");
        tabsMerge->SetSecondTableName("collar table");
        tabsMerge->Update();

        TRACK_ELAPSED("done merging collar table with assay table");

        // if there was an error, report it and continue
        if(!tabsMerge->ErrorMessage().empty())
            vtkOutputWindowDisplayWarningText((tabsMerge->ErrorMessage() + "\n").c_str());
        if(tabsMerge->FatalError())
            return 1;

        // copy the result back into assay table
        infTables[0]->DeepCopy(tabsMerge->GetOutput());

        TRACK_ELAPSED("merging result copied into assay table table");
    }

    // if we have a survey table, we are going to merge it into the assay table
    // note: can only happen if we are dealing with holes and having a depth attribute
    if(infTables[2])
    {
        TRACK_ELAPSED("start merging survey table with assay table")

        // merge data from survey table into assay table
        // note: the dip values that will afterwards be in the unified table will
        //       always count positive dip downwards
        vtkSmartPointer<vtkAtgMergeSurveyWithKey> tabsMerge =
                vtkSmartPointer<vtkAtgMergeSurveyWithKey>::New();
        tabsMerge->SetInputData(0, infTables[0]);
        tabsMerge->SetInputData(1, infTables[2]);
        tabsMerge->SetKeyColumn(holeIdName.c_str());
        tabsMerge->SetFirstTableName("assay table");
        tabsMerge->SetSecondTableName("survey table");
        tabsMerge->SetNegativeDipDown(NegativeDipDown);
        tabsMerge->Update();

        TRACK_ELAPSED("done merging survey table with assay table");

        // if there was an error, report it and continue
        if(!tabsMerge->ErrorMessage().empty())
            vtkOutputWindowDisplayWarningText((tabsMerge->ErrorMessage() + "\n").c_str());
        if(tabsMerge->FatalError())
            return 1;

        // copy the result back into assay table
        infTables[0]->DeepCopy(tabsMerge->GetOutput());

        TRACK_ELAPSED("merging result copied into assay table table");
    }

    // if we have no survey table, but still holes, we need to adapt the collar
    // heights anyway
    else if(isHoles)
    {
        TRACK_ELAPSED("start merging artificial survey table with assay table")

        // get azimuth and dip arrays - if they exist in the initial assay table
        std::string holeAzName = utilNormNames::getName(utilNormNames::HOLEAZIMUTH),
                    holeDipName = utilNormNames::getName(utilNormNames::HOLEDIP);
        vtkDataArray* surveyArrs[2] =
        {
            vtkDataArray::SafeDownCast(infTables[0]->GetColumnByName(holeAzName.c_str())),
            vtkDataArray::SafeDownCast(infTables[0]->GetColumnByName(holeDipName.c_str()))
        };
        vtkStringArray* holeIdArr = vtkStringArray::SafeDownCast(infTables[0]->GetColumnByName(holeIdName.c_str()));

        // we generate a kind of survey table that we can "merge" to the assay,
        // which at the same time takes care of the collar attribute
        vtkSmartPointer<vtkTable> tempSurveyTable = vtkSmartPointer<vtkTable>::New();
        vtkSmartPointer<vtkStringArray> tsbHoleIdArr = vtkSmartPointer<vtkStringArray>::New();
        tsbHoleIdArr->SetName(holeIdArr->GetName());
        tsbHoleIdArr->DeepCopy(holeIdArr);
        tempSurveyTable->AddColumn(tsbHoleIdArr);
        for(int i = 0; i < 2; ++i)
        {
            if(nullptr != surveyArrs[i])
            {
                vtkDataArray* tsbSurveyArr = surveyArrs[i]->NewInstance();
                tsbSurveyArr->SetName(surveyArrs[i]->GetName());
                tsbSurveyArr->DeepCopy(surveyArrs[i]);
                tempSurveyTable->AddColumn(tsbSurveyArr);
                tsbSurveyArr->Delete();
                infTables[0]->RemoveColumnByName(surveyArrs[i]->GetName());
            }
        }

        // merge data from artificial survey table into assay table
        vtkSmartPointer<vtkAtgMergeSurveyWithKey> tabsMerge =
                vtkSmartPointer<vtkAtgMergeSurveyWithKey>::New();
        tabsMerge->SetInputData(0, infTables[0]);
        tabsMerge->SetInputData(1, tempSurveyTable);
        tabsMerge->SetKeyColumn(holeIdName.c_str());
        tabsMerge->SetFirstTableName("assay table");
        tabsMerge->SetSecondTableName("artificial survey table");
        tabsMerge->Update();

        TRACK_ELAPSED("done merging artificial survey table with assay table");

        // if there was an error, report it and continue
        if(!tabsMerge->ErrorMessage().empty())
            vtkOutputWindowDisplayWarningText((tabsMerge->ErrorMessage() + "\n").c_str());
        if(tabsMerge->FatalError())
            return 1;

        // copy the result back into assay table
        infTables[0]->DeepCopy(tabsMerge->GetOutput());

        TRACK_ELAPSED("merging result copied into assay table table");
    }

    // from now on we have only one input table, and we generate an alias
    vtkTable* inTable = infTables[0].Get();

    // special case: if we have a HoleId but not holes and also no SampId, we change the name
    // from HoleId to SampId
    if(!isHoles)
    {
        std::string holeIdName = utilNormNames::getName(utilNormNames::HOLEID),
                    sampIdName = utilNormNames::getName(utilNormNames::SAMPID);
        if(nullptr == inTable->GetColumnByName(sampIdName.c_str()))
        {
            vtkAbstractArray* holeIdArr =
                    vtkAbstractArray::SafeDownCast(inTable->GetColumnByName(holeIdName.c_str()));
            if(nullptr != holeIdArr)
            {
                holeIdArr->SetName(sampIdName.c_str());
            }
        }
    }

    TRACK_ELAPSED("start checking");

    // check for mandatory columns - which are Collar:X/Y/Z
    std::vector<std::string> mand;
    mand.push_back(utilNormNames::getName(utilNormNames::COLLAR));
    std::vector<std::string> notFound;
    for(auto col = mand.begin(); col != mand.end(); ++col)
    {
        if(nullptr == inTable->GetColumnByName(col->c_str()))
            notFound.push_back(*col);
    }
    if(!notFound.empty())
    {
        vtkOutputWindowDisplayWarningText(std::string("Mandatory input column(s) not found:\n" +
                                                      boost::algorithm::join(notFound, ", ") +
                                                      "\n(or some accepted equivalent(s))\n").c_str());
        return 1;
    }

    // generate an unstructured grid with data items and labels as polydata
    // note: with size factor 0 we are telling the tool that 3d symbols need to be generated
    //   by sample/hole, not globally
    vtkSmartPointer<vtkAtgTableToItemAndLabel> tabToItemLab =
            vtkSmartPointer<vtkAtgTableToItemAndLabel>::New();
    tabToItemLab->SetInputData(inTable);
    tabToItemLab->SetSizeFactor(0.);
    tabToItemLab->SetLabelAttOrPrefix(SamplingIdPrefix);
    tabToItemLab->SetIsHoles(isHoles);
    tabToItemLab->SetDecorations(Decorations);
    tabToItemLab->Update();

    // retrieve the results
    vtkUnstructuredGrid* outputSampling = vtkUnstructuredGrid::GetData(outputVector->GetInformationObject(0));
    outputSampling->Allocate();
    outputSampling->InitializeFacesRepresentation(tabToItemLab->GetOutput()->GetNumberOfCells());
    outputSampling->DeepCopy(tabToItemLab->GetOutput());

    vtkPolyData* outputLabels = vtkPolyData::GetData(outputVector->GetInformationObject(1));
    outputLabels->DeepCopy(vtkPolyData::SafeDownCast(tabToItemLab->GetOutputDataObject(1)));

    // handle string arrays and ensure that they are "categories"
    prepareNameAttributes(outputSampling);

    return 1;
}

void vtkAtgSamplingReader::prepareNameAttributes(vtkUnstructuredGrid* grid)
{
    for(vtkIdType col = 0; col < grid->GetCellData()->GetNumberOfArrays(); ++col)
    {
        vtkAbstractArray* arr = grid->GetCellData()->GetAbstractArray(col);
        if(VTK_STRING != arr->GetDataType())
           continue;

        // if a string array is not in the type "category" we prepend a N_ (to bring it there)
        switch(utilNormNames::getType(arr->GetName()))
        {
            case utilNormNames::TY_NAME:
            case utilNormNames::TY_DATE:
            case utilNormNames::TY_CATEGORY:
            {
                // these are already ok
                break;
            }
            default:
            {
                arr->SetName((std::string("N_") + arr->GetName()).c_str());
            }
        }
    }
}
