/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgMergeSurveyWithKey.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <boost/algorithm/string/join.hpp>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkTable.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgMergeSurveyWithKey.h>

static const double epsMeters = 0.01;

vtkStandardNewMacro(vtkAtgMergeSurveyWithKey)

vtkAtgMergeSurveyWithKey::vtkAtgMergeSurveyWithKey()
:   KeyColumn(nullptr),
    NegativeDipDown(false),
    FirstTableName(nullptr),
    SecondTableName(nullptr),
    FatalErr(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgMergeSurveyWithKey::~vtkAtgMergeSurveyWithKey()
{
    SetSecondTableName(nullptr);
    SetFirstTableName(nullptr);
    SetKeyColumn(nullptr);
}

int vtkAtgMergeSurveyWithKey::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
        case 1:
            // first and second table
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeSurveyWithKey::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // merged table
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeSurveyWithKey::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    // reset error conditions
    Err.clear();
    FatalErr = false;

    // check if we have a key
    if((nullptr == KeyColumn) || std::string(KeyColumn).empty())
    {
        Err = "vtkAtgMergeSurveyWithKey: no key defined";
        FatalErr = true;
        return 1;
    }

    // set defaults in the case that the names are not there
    if((nullptr == FirstTableName) || std::string(FirstTableName).empty())
        SetFirstTableName("first table");
    if((nullptr == SecondTableName) || std::string(SecondTableName).empty())
        SetSecondTableName("second table");
    std::vector<std::string>
            tabNames(std::initializer_list<std::string>({FirstTableName, SecondTableName}));

    // input and output tables
    std::vector<vtkTable*> inTabs;
    inTabs.push_back(vtkTable::GetData(inputVector[0]->GetInformationObject(0)));
    inTabs.push_back(vtkTable::GetData(inputVector[1]->GetInformationObject(0)));
    vtkTable* outTab = vtkTable::GetData(outputVector->GetInformationObject(0));

    // check if we have the two input tables and if they have the key column
    std::vector<vtkStringArray*> keyCols;
    for(int i = 0; i < 2; ++i)
    {
        if(nullptr == inTabs[i])
        {
            Err = tabNames[i] + " does not exist";
            FatalErr = true;
            return 1;
        }

        keyCols.push_back(vtkStringArray::SafeDownCast(inTabs[i]->GetColumnByName(KeyColumn)));
        if(nullptr == keyCols[i])
        {
            Err = tabNames[i] + " does not have a column named <" + KeyColumn + ">";
            FatalErr = true;
            return 1;
        }
    }

    // copy first input to output
    // note: we are doing a deep copy because we may later on remove rows
    //   and should not damage the input table
    outTab->DeepCopy(inTabs[0]);

    // get some existing arrays for further processing
    std::string collarName = utilNormNames::getName(utilNormNames::COLLAR),
                depthName = utilNormNames::getName(utilNormNames::DEPTH),
                surveyAtName = utilNormNames::getName(utilNormNames::SURVEYAT),
                holeAzName = utilNormNames::getName(utilNormNames::HOLEAZIMUTH),
                holeDipName = utilNormNames::getName(utilNormNames::HOLEDIP);
    vtkDataArray *collarArr = vtkDataArray::SafeDownCast(outTab->GetColumnByName(collarName.c_str())),
                 *depthArr = vtkDataArray::SafeDownCast(outTab->GetColumnByName(depthName.c_str())),
                 *surveyAtArr = vtkDataArray::SafeDownCast(inTabs[1]->GetColumnByName(surveyAtName.c_str())),
                 *holeAzArr = vtkDataArray::SafeDownCast(inTabs[1]->GetColumnByName(holeAzName.c_str())),
                 *holeDipArr = vtkDataArray::SafeDownCast(inTabs[1]->GetColumnByName(holeDipName.c_str()));
    std::vector<std::string> missingArrs;
    if(nullptr == collarArr)
        missingArrs.push_back((collarName + "[assay table]").c_str());
    if(nullptr == depthArr)
        missingArrs.push_back((depthName + "[assay table]").c_str());
    if(!missingArrs.empty())
    {
        Err = "Mandatory table column(s) (or accepted equivalents) not found:\n<" +
              boost::algorithm::join(missingArrs, ">, <") + ">";
        FatalErr = true;
        return 1;
    }
    if(3 != collarArr->GetNumberOfComponents())
    {
        Err = "Collar column must have 3 components (x, y, z or equivalent)";
        FatalErr = true;
        return 1;
    }
    if(2 != depthArr->GetNumberOfComponents())
    {
        Err = "Depth column must have 2 components (from, to or equivalent)";
        FatalErr = true;
        return 1;
    }

    // build a survey data structure
    typedef std::pair<double, double> SurveyRec;
    typedef std::map<double, SurveyRec> Survey;
    std::map<std::string, Survey> surveyInx;
    for(vtkIdType r = 0; r < inTabs[1]->GetNumberOfRows(); ++r)
    {
        std::string holeId = keyCols[1]->GetValue(r);
        double at = 0.,
               azimuth = 0.,
               dip = 90.;
        if(nullptr != surveyAtArr)
            at = surveyAtArr->GetVariantValue(r).ToDouble();
        if(nullptr != holeAzArr)
            azimuth = holeAzArr->GetVariantValue(r).ToDouble();
        if(nullptr != holeDipArr)
            dip = holeDipArr->GetVariantValue(r).ToDouble();

        if(surveyInx.end() == surveyInx.find(holeId))
            surveyInx[holeId] = Survey();
        surveyInx[holeId][at] = SurveyRec(azimuth, dip);
    }

    // add azimuth and dip columns to the assay table
    vtkSmartPointer<vtkDoubleArray> azArr = vtkSmartPointer<vtkDoubleArray>::New();
    azArr->SetName(holeAzName.c_str());
    azArr->SetNumberOfTuples(outTab->GetNumberOfRows());
    azArr->Fill(0.);
    outTab->AddColumn(azArr);
    vtkSmartPointer<vtkDoubleArray> dipArr = vtkSmartPointer<vtkDoubleArray>::New();
    dipArr->SetName(holeDipName.c_str());
    dipArr->SetNumberOfTuples(outTab->GetNumberOfRows());
    dipArr->Fill(90.);
    outTab->AddColumn(dipArr);

    // handle the assay table row by row
    // note: here we prepare a dummy dip that is negative if NegativeDipDown is true
    double collar[3];
    double depth[2];
    double dipFactor = NegativeDipDown ? -1. : 1.;
    Survey dummySurvey;
    dummySurvey[0.] = SurveyRec(0., dipFactor * 90.);
    for(vtkIdType r = 0; r < inTabs[0]->GetNumberOfRows(); ++r)
    {
        // get the relevant data for handling the assay table rows
        // note: we are going to ensure positive values in the depth range because
        //       all further calculations are assuming that these values are positive,
        //       which they may not always be in all input data sets
        std::string holeId = keyCols[0]->GetValue(r);
        collarArr->GetTuple(r, collar);
        depthArr->GetTuple(r, depth);
        depth[0] = ::fabs(depth[0]);
        depth[1] = ::fabs(depth[1]);
        Survey& survey = (surveyInx.end() != surveyInx.find(holeId)) ?
                         surveyInx[holeId] :
                         dummySurvey;

        // here we calculate from the initial collar along the survey records down to the
        // top of the drill hole segment that is handled by the current assay row, then
        // we are using the then "current" survey record for the entire segment
        // note that the effect is that we may apply a new direction always a bit "too late",
        // like in the following case:
        // survey records:
        //   [0] from 0 to 250
        //   [1] from 250 to 716
        //   [2] from 716 to end
        // assay records:
        //   from 0 to 36 -> [0]
        //     ...
        //   from 243 to 253 -> [0]
        //   from 253 to 263 -> [1]
        //     ...
        //   from 713 to 723 -> [1]
        //   from 723 to 733 -> [2]
        //     ...
        // note: here we make sure that dip is always downwards positive
        double currentDepth = 0,
               currentAzimuth = survey.begin()->second.first,
               currentDip = dipFactor * survey.begin()->second.second;
        for(Survey::iterator sit = survey.begin();
            (sit != survey.end()) && ((depth[0] - currentDepth) > epsMeters);
            ++sit)
        {
            // get the maximum depth validity for the current survey record
            // note: we assume that no hole will reach 100000m depth...
            Survey::iterator sitNext = sit;
            ++sitNext;
            double maxRange = (survey.end() != sitNext) ? sitNext->first : 100000.;

            // now we add either the full depth range, or the range maximum
            double nextStep = std::min<double>(maxRange, depth[0]) - currentDepth;

            // get the directional angles and convert to radians
            static double const pi = 3.14159264;
            currentAzimuth = sit->second.first;
            currentDip = dipFactor * sit->second.second;
            double az = pi * currentAzimuth / 180.,
                   dp = pi * currentDip / 180.;

            // directional components
            double diff[3];
            diff[0] = ::cos(dp) * ::sin(az);
            diff[1] = ::cos(dp) * ::cos(az);
            diff[2] = -::sin(dp);

            // apply directional components to collar coordinates
            for(int i = 0; i < 3; ++i)
                collar[i] += nextStep * diff[i];

            // prepare for next step - if any
            currentDepth += nextStep;
        }

        // update the collar to be at the top of the assay (sample) segment
        collarArr->SetTuple(r, collar);

        // remember the azimuth and dip that are valid at the collar point
        azArr->SetValue(r, currentAzimuth);
        dipArr->SetValue(r, currentDip);
    }

    return 1;
}

int vtkAtgMergeSurveyWithKey::RequestInformation(vtkInformation* request,
                                                 vtkInformationVector** inputVector,
                                                 vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgMergeSurveyWithKey::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
