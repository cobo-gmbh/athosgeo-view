/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgMergeCollarWithKey.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <boost/algorithm/string/join.hpp>
#include <utilNormNames.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkAbstractArray.h>
#include <vtkTable.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgMergeCollarWithKey.h>

vtkStandardNewMacro(vtkAtgMergeCollarWithKey)

vtkAtgMergeCollarWithKey::vtkAtgMergeCollarWithKey()
:   KeyColumn(nullptr),
    FirstTableName(nullptr),
    SecondTableName(nullptr),
    FatalErr(false)
{
    SetNumberOfInputPorts(2);
    SetNumberOfOutputPorts(1);
}

vtkAtgMergeCollarWithKey::~vtkAtgMergeCollarWithKey()
{
    SetSecondTableName(nullptr);
    SetFirstTableName(nullptr);
    SetKeyColumn(nullptr);
}

int vtkAtgMergeCollarWithKey::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
        case 1:
            // first and second table
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeCollarWithKey::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // merged table
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgMergeCollarWithKey::RequestData(vtkInformation* request,
                                          vtkInformationVector** inputVector,
                                          vtkInformationVector* outputVector)
{
    // reset error conditions
    Err.clear();
    FatalErr = false;

    // check if we have a key
    if((nullptr == KeyColumn) || std::string(KeyColumn).empty())
    {
        Err = "vtkAtgMergeCollarWithKey: no key defined";
        FatalErr = true;
        return 1;
    }

    // set defaults in the case that the names are not there
    if((nullptr == FirstTableName) || std::string(FirstTableName).empty())
        SetFirstTableName("first table");
    if((nullptr == SecondTableName) || std::string(SecondTableName).empty())
        SetSecondTableName("second table");
    std::vector<std::string>
            tabNames(std::initializer_list<std::string>({FirstTableName, SecondTableName}));

    // input and output tables
    std::vector<vtkTable*> inTabs;
    inTabs.push_back(vtkTable::GetData(inputVector[0]->GetInformationObject(0)));
    inTabs.push_back(vtkTable::GetData(inputVector[1]->GetInformationObject(0)));
    vtkTable* outTab = vtkTable::GetData(outputVector->GetInformationObject(0));

    // check if we have the two input tables and if they have the key column
    std::vector<vtkStringArray*> keyCols;
    for(int i = 0; i < 2; ++i)
    {
        if(nullptr == inTabs[i])
        {
            Err = tabNames[i] + " does not exist";
            FatalErr = true;
            return 1;
        }

        keyCols.push_back(vtkStringArray::SafeDownCast(inTabs[i]->GetColumnByName(KeyColumn)));
        if(nullptr == keyCols[i])
        {
            Err = tabNames[i] + " does not have a column named <" + KeyColumn + ">";
            FatalErr = true;
            return 1;
        }
    }

    // copy first input to output
    // note: we are doing a deep copy because we may later on remove rows
    //   and should not damage the input table
    outTab->DeepCopy(inTabs[0]);

    // set up an index to quickly find the row in the second table from the key
    std::map<std::string, vtkIdType> keyInx;
    for(vtkIdType r = 0; r < inTabs[1]->GetNumberOfRows(); ++r)
        keyInx[keyCols[1]->GetValue(r)] = r;

    // here we want to collect the pointers to the table columns for copying the values
    // from the collar table into the assay table
    std::vector<vtkAbstractArray*> firstCols,
                                   secondCols;

    // handle the special case that we have a generated MaxDepth from the assay and
    // an imported one in the collar table
    std::string maxDepthName = utilNormNames::getName(utilNormNames::MAXDEPTH);

    // add columns from second table to first table - if not existing yet
    std::vector<std::string> dupNames;
    for(vtkIdType c = 0; c < inTabs[1]->GetNumberOfColumns(); ++c)
    {
        // skip if this is the key column
        std::string cname = inTabs[1]->GetColumnName(c);
        if(cname == KeyColumn)
            continue;

        // avoid duplicate column names
        if(nullptr != inTabs[0]->GetColumnByName(cname.c_str()))
        {
            if(maxDepthName == cname)
            {
                // any max depth array from the assay table should be dropped if
                // we have a "real" one in the collar table
                outTab->RemoveColumnByName(maxDepthName.c_str());
            }
            else
            {
                // any other duplicates should be reported and ignored
                dupNames.push_back(cname);
                continue;
            }
        }

        // generate new column in first table
        vtkAbstractArray* cArr = inTabs[1]->GetColumn(c);
        vtkAbstractArray* aArr = cArr->NewInstance();
        aArr->SetName(cname.c_str());
        aArr->SetNumberOfComponents(cArr->GetNumberOfComponents());
        for(vtkIdType comp = 0; comp < cArr->GetNumberOfComponents(); ++comp)
            aArr->SetComponentName(comp, cArr->GetComponentName(comp));
        aArr->SetNumberOfTuples(inTabs[0]->GetNumberOfRows());
        outTab->AddColumn(aArr);
        aArr->Delete();

        // remember the pointers
        firstCols.push_back(aArr);
        secondCols.push_back(cArr);
    }

    // report any duplicate names
    if(!dupNames.empty())
    {
        Err += std::string("Colum(s) in ") + SecondTableName +
               " already exist:\n<" +
               boost::algorithm::join(dupNames, ">, <") +
               ">\nThey will be ignored";
    }

    // do the copy row by row
    std::set<std::string> unknownHoleIds;
    keyCols[0] = vtkStringArray::SafeDownCast(outTab->GetColumnByName(KeyColumn));
    for(vtkIdType row = 0; row < outTab->GetNumberOfRows(); ++row)
    {
        // find the key in the second table
        std::string key = keyCols[0]->GetValue(row);
        auto inxIt = keyInx.find(key);

        // handle case of not finding the key in the second table:
        // we have to remove the row also from the first table!
        if(keyInx.end() == inxIt)
        {
            unknownHoleIds.insert(key);
            outTab->RemoveRow(row);
            --row;
            continue;
        }

        // get the collar row id and the output row id
        vtkIdType row2 = keyInx[key];

        // ...and copy the values
        for(vtkIdType cc = 0; cc < secondCols.size(); ++cc)
        {
            vtkIdType numComp = secondCols[cc]->GetNumberOfComponents();
            for(vtkIdType comp = 0; comp < numComp; ++comp)
            {
                vtkVariant val = secondCols[cc]->GetVariantValue(numComp * row2 + comp);
                firstCols[cc]->SetVariantValue(numComp * row + comp, val);
            }
        }
    }

    // report any unknown hole IDs
    if(!unknownHoleIds.empty())
    {
        if(!Err.empty())
            Err += "\n";
        Err += std::string(KeyColumn) + " value(s) that was/were not found in " +
               SecondTableName + ":\n<" +
               boost::algorithm::join(unknownHoleIds, ">, <") +
               ">\nThe row(s) will be ignored";
    }

    return 1;
}

int vtkAtgMergeCollarWithKey::RequestInformation(vtkInformation* request,
                                                 vtkInformationVector** inputVector,
                                                 vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgMergeCollarWithKey::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
