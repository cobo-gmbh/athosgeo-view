/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToSamplings.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cmath>
#include <vtkTable.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkVertex.h>
#include <vtkLine.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilNormNames.h>
#include <vtkAtgTableToSamplings.h>

vtkStandardNewMacro(vtkAtgTableToSamplings)

vtkAtgTableToSamplings::vtkAtgTableToSamplings()
{
}

vtkAtgTableToSamplings::~vtkAtgTableToSamplings()
{
}

int vtkAtgTableToSamplings::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: table with attributes
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToSamplings::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToSamplings::RequestData(vtkInformation* request,
                                        vtkInformationVector** inputVector,
                                        vtkInformationVector* outputVector)
{
    (void)request;

    // get the input table
    vtkTable* input = vtkTable::GetData(inputVector[0]->GetInformationObject(0));

    // get the attribute columns
    // note about the different attributes:
    // - collar is the only mandatory attribute (with 3 components)
    // - if depth is not present, the collar coordinates are treated as coordinates of
    //   single (point) samples and azimuth and dip are not even checked
    // - if azimuth (N via E etc) and dip (-90 = vertically down) are not present,
    //   the hole is assumed to be vertical
    std::string nameCollar = utilNormNames::getName(utilNormNames::COLLAR),
                nameDepth = utilNormNames::getName(utilNormNames::DEPTH),
                nameAzimuth = utilNormNames::getName(utilNormNames::HOLEAZIMUTH),
                nameDip = utilNormNames::getName(utilNormNames::HOLEDIP);
    vtkDataArray *collar = vtkDataArray::SafeDownCast(input->GetColumnByName(nameCollar.c_str())),
                 *depth = vtkDataArray::SafeDownCast(input->GetColumnByName(nameDepth.c_str())),
                 *azimuth = vtkDataArray::SafeDownCast(input->GetColumnByName(nameAzimuth.c_str())),
                 *dip = vtkDataArray::SafeDownCast(input->GetColumnByName(nameDip.c_str()));
    bool isHoles = nullptr != depth;

    // check if collar has 3 components and depth, if existing, has 2
    if(!collar)
        return 1;
    if(collar->GetNumberOfComponents() < 3)
        return 1;
    if(depth && (depth->GetNumberOfComponents() < 2))
        return 1;

    // get the output data object
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> vertices = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();

    // going through the rows of the input table and generating either line segments or points
    // note: for a hole, the assumption is that collar coordinates are for the top
    //       point of the hole segment already and any azimuth and dip attributes are
    //       for the segment that is handled here
    for(vtkIdType r = 0; r < input->GetNumberOfRows(); ++r)
    {
        // extract collar coordinates
        std::vector<std::vector<double>> pts(2);
        for(int c = 0; c < 3; ++c)
            pts[0].push_back(collar->GetComponent(r, c));
        pts[1] = pts[0];

        // line segment
        // continue here only if we have a drill hole (i.e., depth exists)
        if(isHoles)
        {
            // get the range
            std::vector<double> range;
            range.push_back(depth->GetComponent(r, 0));
            range.push_back(depth->GetComponent(r, 1));

            // change the range to 0..end
            // note: by taking the absolute value, we are automatically taking care of
            //       depth range values (from..to) that are given with negative values down
            // note: this implicitly means that we cannot handle drillholes going upwards!
            range[1] = ::fabs(range[0] - range[1]);
            range[0] = 0.;

            // directional components for vertical drillhole...
            // note: z negative because it is downwards
            std::vector<double> diff(std::initializer_list<double>({0., 0., -1.}));

            // ...or else for non-vertical
            if(azimuth && dip)
            {
                // get the directional angles and convert to radians
                // note: here we assume that negative dip angles are always upwards
                static double const pi = 3.14159264;
                double az = pi * azimuth->GetComponent(r, 0) / 180.,
                       dp = pi * dip->GetComponent(r, 0) / 180.;

                // directional components for oblique drillhole
                // note: z negative because it is downwards
                diff[0] = ::cos(dp) * ::sin(az);
                diff[1] = ::cos(dp) * ::cos(az);
                diff[2] = -::sin(dp);
            }

            // apply directional components to second coordinates
            for(unsigned long i = 0; i < 3; ++i)
                pts[1][i] += range[1] * diff[i];

            vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
            for(unsigned long i = 0; i < 2; ++i)
                line->GetPointIds()->SetId(static_cast<vtkIdType>(i), points->InsertNextPoint(pts[i][0], pts[i][1], pts[i][2]));
            lines->InsertNextCell(line);
        }

        // single sample
        else
        {
            vtkSmartPointer<vtkVertex> vert = vtkSmartPointer<vtkVertex>::New();
            vert->GetPointIds()->SetId(0, points->InsertNextPoint(pts[0][0], pts[0][1], pts[0][2]));
            vertices->InsertNextCell(vert);
        }
    }

    // prepare the output
    vtkPolyData* output = vtkPolyData::GetData(outputVector->GetInformationObject(0));
    output->SetPoints(points);
    output->SetVerts(vertices);
    output->SetLines(lines);

    // copy the table data to the cell data - either line segment or vertex cells
    vtkCellData* cellData = output->GetCellData();
    cellData->PassData(input->GetRowData());

    // remove any temporary arrays with names starting with "__"
    for(int a = 0; a < cellData->GetNumberOfArrays(); ++a)
    {
        std::string name = cellData->GetArrayName(a);
        if(name.substr(0, 2) == "__")
        {
            cellData->RemoveArray(a);
            --a;
        }
    }

    return 1;
}

void vtkAtgTableToSamplings::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
