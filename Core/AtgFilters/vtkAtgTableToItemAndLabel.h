/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToItemAndLabel.h

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTableToItemAndLabel_h
#define vtkAtgTableToItemAndLabel_h

#include <vector>
#include <string>

#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgFiltersModule.h>

class vtkTable;

class ATGFILTERS_EXPORT vtkAtgTableToItemAndLabel: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgTableToItemAndLabel* New();
    vtkTypeMacro(vtkAtgTableToItemAndLabel, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetMacro(SizeFactor, double)
    vtkGetMacro(SizeFactor, double)

    vtkSetMacro(Symbol3D, int)
    vtkGetMacro(Symbol3D, int)

    vtkSetStringMacro(LabelAttOrPrefix)
    vtkGetStringMacro(LabelAttOrPrefix)

    vtkSetMacro(IsHoles, bool)
    vtkGetMacro(IsHoles, bool)

    vtkSetStringMacro(Decorations)
    vtkGetStringMacro(Decorations)

protected:

    vtkAtgTableToItemAndLabel();
    ~vtkAtgTableToItemAndLabel();

    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);

private:

    // copy constructor and assignment not implemented
    vtkAtgTableToItemAndLabel(vtkAtgTableToItemAndLabel const&);
    void operator=(vtkAtgTableToItemAndLabel const&);

    // generate a sampling id if such a thing is not present on input,
    // and generate a table with collars and sampling ids
    void handleSamplingIds(vtkTable* samplingTable,
                           std::string const& prefix, bool isHoles);

    // parse decorations string
    std::vector<std::vector<std::string>> parseDecorations(std::string const& decorations);

    // this is about a global size factor for all 3D symbols to be generated
    // note: if the value is 0 (or less), it is assumed that the user provides information
    //   about symbols and size factor row by row in the input
    double SizeFactor;

    // 3D symbol to be used globally
    // note: if the SizeFactor (above) is 0 or less, this will be ignored
    int Symbol3D;

    // main label column to be used in case of a global label 3d symbol and label generation
    char* LabelAttOrPrefix;

    // treat samplings as holes with a depth attribute
    // note: this is not possible for global treatment
    bool IsHoles;

    // decorations as a string: "<type>",<id>,<size>;"<type>",<id>,<size> etc., no spaces
    // note: this is for the individual treatment of samples or holes
    char* Decorations;

};

#endif
