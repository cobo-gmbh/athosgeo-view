/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgSamplingReader.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __vtkAtgSamplingReader_h
#define __vtkAtgSamplingReader_h

#include <string>
#include <vector>
#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgFiltersModule.h>

class vtkTable;

class ATGFILTERS_EXPORT vtkAtgSamplingReader: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgSamplingReader* New();
    vtkTypeMacro(vtkAtgSamplingReader, vtkUnstructuredGridAlgorithm)
    void PrintSelf(ostream& os, vtkIndent indent);

    vtkSetStringMacro(AssayFile)
    vtkGetStringMacro(AssayFile)

    vtkSetStringMacro(CollarFile)
    vtkGetStringMacro(CollarFile)

    vtkSetStringMacro(SurveyFile)
    vtkGetStringMacro(SurveyFile)

    vtkSetMacro(NegativeDipDown, bool)
    vtkGetMacro(NegativeDipDown, bool)

    vtkSetStringMacro(FieldDelimiterCharacters)
    vtkGetStringMacro(FieldDelimiterCharacters)

    vtkSetMacro(AddTabFieldDelimiter, bool)
    vtkGetMacro(AddTabFieldDelimiter, bool)

    vtkSetStringMacro(SamplingIdPrefix)
    vtkGetStringMacro(SamplingIdPrefix)

    vtkSetStringMacro(EmptyCellString)
    vtkGetStringMacro(EmptyCellString)

    vtkSetMacro(EmptyCellValue, double)
    vtkGetMacro(EmptyCellValue, double)

    vtkSetStringMacro(Decorations)
    vtkGetStringMacro(Decorations)

protected:

    vtkAtgSamplingReader();
    ~vtkAtgSamplingReader();

    int RequestData(vtkInformation* request,
                    vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector);

    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // name of CSV files (full path)
    char* AssayFile; // assay
    char* CollarFile;
    char* SurveyFile;

    // if this is true, negative dip values will count down, otherwise upwards
    bool NegativeDipDown;

    // field delimiter characters and related options
    char* FieldDelimiterCharacters;
    bool AddTabFieldDelimiter;

    // prefix for sampling ids, if none found in Assay or Collar
    char* SamplingIdPrefix;

    // string to be entered for empty string cells
    char* EmptyCellString;

    // value to be entered for empty numeric cells
    double EmptyCellValue;

    // decorations as a string: "<type>",<id>,<size>;"<type>",<id>,<size> etc., no spaces
    char* Decorations;

    // make sure string table columns are of type category, prepending a "N_" if necessary
    void prepareNameAttributes(vtkUnstructuredGrid* grid);

};

#endif
