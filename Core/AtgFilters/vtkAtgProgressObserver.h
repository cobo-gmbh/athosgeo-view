/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgProgressObserver.h

   Copyright (c) 2019 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgProgressObserverFilter_h
#define vtkAtgProgressObserverFilter_h

#include <vtkSetGet.h>
#include <vtkProgressObserver.h>
#include <AtgFiltersModule.h>

class vtkAlgorithm;

class ATGFILTERS_EXPORT vtkAtgProgressObserver: public vtkProgressObserver
{
public:

    static vtkAtgProgressObserver* New();

    vtkTypeMacro(vtkAtgProgressObserver, vtkProgressObserver)

    // note that this observer does not take ownership of the algorithm
    // (but probably it should handle the reference counter...)
    void SetAlgorithm(vtkAlgorithm* algorithm);
    vtkAlgorithm* GetAlgorithm() const;

    // value range between 0 and 1 to which the progress will be scaled
    // default: from = 0, to = 1
    void SetRange(double from, double to);
    void GetRange(double& from, double& to) const;

    virtual void UpdateProgress(double amount);

    virtual double GetProgress();

protected:

    vtkAtgProgressObserver();
    ~vtkAtgProgressObserver() override;

private:

    vtkAtgProgressObserver(const vtkAtgProgressObserver&) = delete;
    void operator=(const vtkAtgProgressObserver&) = delete;

    vtkAlgorithm* Filter;
    double From,
           To,
           Amount;

};

#endif
