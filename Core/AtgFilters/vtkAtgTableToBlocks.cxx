/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToBlocks.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <vtkSmartPointer.h>
#include <vtkTable.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCleanUnstructuredGrid.h>
#include <vtkMergePoints.h>
#include <vtkPoints.h>
#include <vtkDataArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <utilAddGridCells.h>
#include <vtkAtgProgressObserver.h>
#include <vtkAtgTableToBlocks.h>

vtkStandardNewMacro(vtkAtgTableToBlocks)

vtkAtgTableToBlocks::vtkAtgTableToBlocks()
:   RotAngle(0.),
    BlockSize{10., 10., 10.}
{
}

vtkAtgTableToBlocks::~vtkAtgTableToBlocks()
{
}

int vtkAtgTableToBlocks::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - mandatory: table with attributes
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToBlocks::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main output: block model
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToBlocks::RequestData(vtkInformation* request,
                                     vtkInformationVector** inputVector,
                                     vtkInformationVector* outputVector)
{
    TRACK_FUNCTION_TIME

    // get the input table
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
    vtkTable* input = vtkTable::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));

    // create a temporary copy of the input table - for adding size/angle columns if required
    vtkSmartPointer<vtkTable> input2 = vtkSmartPointer<vtkTable>::New();
    input2->ShallowCopy(input);

    // get the output grid
    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkUnstructuredGrid* output =
            vtkUnstructuredGrid::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    // get the attribute columns
    // note: it is assumed that all these columns do exist or have been generated
    //   before applying this filter
    std::string nameCent = utilNormNames::getName(utilNormNames::BLOCKCENTER),
                nameSize = utilNormNames::getName(utilNormNames::BLOCKSIZE),
                nameAng = utilNormNames::getName(utilNormNames::ANGLE);
    vtkDataArray *cent = vtkDataArray::SafeDownCast(input2->GetColumnByName(nameCent.c_str())),
                 *size = vtkDataArray::SafeDownCast(input2->GetColumnByName(nameSize.c_str())),
                 *ang = vtkDataArray::SafeDownCast(input2->GetColumnByName(nameAng.c_str()));

    // temporary structures to build the block model block by block
    vtkSmartPointer<vtkUnstructuredGrid> tempGrid =
            vtkSmartPointer<vtkUnstructuredGrid>::New();
    vtkSmartPointer<vtkPoints> tempPts = vtkSmartPointer<vtkPoints>::New();

    // loop over input rows and convert them into blocks
    utilAddGridCells* addCells = new utilAddGridCells(tempGrid, tempPts);
    for(vtkIdType i = 0; i < input2->GetNumberOfRows(); ++i)
    {
        double angle = (ang == nullptr) ? 0. : ang->GetVariantValue(i).ToDouble();
        addCells->generateCell(cent->GetTuple(i), size->GetTuple(i), angle);
    }
    tempGrid->SetPoints(tempPts);
    delete addCells;

    TRACK_ELAPSED("Cells prepared")

    // transforming into a "clean unstructured grid" will detect common points
    // between neighboring cells: so far we did not care about them
    // note: we assume that mm precision should be good enough for block models
    // note: if the coordinates are in the order of magnitude of "millions", then
    //   3 additional digits means that we need "double" precision for the point
    //   coordinates (which the "clean" filter provides if the input grid uses
    //   the same coordinate data type)
    vtkSmartPointer<vtkCleanUnstructuredGrid> cleanGrid =
            vtkSmartPointer<vtkCleanUnstructuredGrid>::New();
    vtkSmartPointer<vtkAtgProgressObserver> progressObs =
            vtkSmartPointer<vtkAtgProgressObserver>::New();
    progressObs->SetAlgorithm(this);
    cleanGrid->SetToleranceIsAbsolute(true);
    cleanGrid->SetAbsoluteTolerance(0.001);
    cleanGrid->SetInputData(tempGrid);
    cleanGrid->SetProgressObserver(progressObs);
    cleanGrid->Update();

    TRACK_ELAPSED("Grid cleaned")

    // copy the temporary "clean grid" to the output
    output->ShallowCopy(cleanGrid->GetOutput());

    // pass the input table data to the output as cell data
    output->GetCellData()->PassData(input2->GetRowData());

    return 1;
}

int vtkAtgTableToBlocks::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgTableToBlocks::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
