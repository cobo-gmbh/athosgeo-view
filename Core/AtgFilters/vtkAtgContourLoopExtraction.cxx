/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgContourLoopExtraction.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <set>
#include <numeric>
#include <boost/dynamic_bitset.hpp>
#include <utilVector2D.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkCleanPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgContourLoopExtraction.h>

vtkStandardNewMacro(vtkAtgContourLoopExtraction)

static double const distEps = 0.01;

vtkAtgContourLoopExtraction::vtkAtgContourLoopExtraction()
{
}

vtkAtgContourLoopExtraction::~vtkAtgContourLoopExtraction()
{
}

int vtkAtgContourLoopExtraction::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // linear cells assumed
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgContourLoopExtraction::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // closed loops as polygons
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgContourLoopExtraction::RequestData(vtkInformation* request,
                                             vtkInformationVector** inputVector,
                                             vtkInformationVector* outputVector)
{
    // input port 0: the input raw boundary
    vtkPolyData* inputRaw = vtkPolyData::GetData(inputVector[0]);
    if(nullptr == inputRaw)
    {
        vtkOutputWindowDisplayWarningText("vtkAtgContourLoopExtraction: no input found");
        return 1;
    }

    // get the output polygon
    vtkPolyData* outPolygon = vtkPolyData::GetData(outputVector, 0);
    if(nullptr == outPolygon)
    {
        vtkOutputWindowDisplayWarningText("vtkAtgContourLoopExtraction: no output polydata found");
        return 1;
    }

    // create a clean polydata containing only line segments and without other
    // topological types. This simplifies the filter.
    vtkCellArray* lines = inputRaw->GetLines();
    vtkPoints* points = inputRaw->GetPoints();
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    polyData->Allocate();
    vtkIdType npts;
    vtkIdType const* pts;
    for(lines->InitTraversal(); lines->GetNextCell(npts, pts); )
    {
        for(int i = 0; i < (npts - 1); ++i)
            polyData->InsertNextCell(VTK_LINE, 2, pts + i);
    }
    polyData->SetPoints(points);

    // we want to make sure we have no duplicate points at this point
    vtkSmartPointer<vtkCleanPolyData> cleanpd = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanpd->SetInputData(polyData);
    cleanpd->Update();

    // cell id and coordinates of point
    typedef std::pair<vtkIdType, utilVector2D> CellDef;

    // split points in such a way that points have never more than two cells while avoiding
    // polylines self-intersectins
    // note: ideally we do not encounter points with odd numbers of cells, but if we do,
    //   the result is not really predictable
    vtkPolyData* pd2 = cleanpd->GetOutput();
    pd2->BuildLinks();
    vtkPoints* pt2 = pd2->GetPoints();
    vtkIdType initialNumberOfPoints = pt2->GetNumberOfPoints();
    std::set<vtkIdType> endPoints;
    for(vtkIdType p = 0; p < initialNumberOfPoints; ++p)
    {
        // find all the cells that are connected to the point
        // note: if the number of cells is odd, the current point will be an open end
        //   point after removing all pairs
        vtkIdType ncells,
                  *cells;
        pd2->GetPointCells(p, ncells, cells);
        if(1 == ncells)
            endPoints.insert(p);

        // if we have more than 2 cells connected, remove cells pairwise in such a way that
        // they are always "neighbors" - thus avoiding self intersections
        if(2 < ncells)
        {
            // generate a vector of points around the current point that should be split
            std::vector<CellDef> cvec;
            double pt[3];
            for(int i = 0; i < ncells; ++i)
            {
                vtkIdType cellId = cells[i];
                vtkCell* cell = pd2->GetCell(cellId);

                vtkIdType otherPt = (cell->GetPointId(0) == p) ? 1 : 0;
                pd2->GetPoint(cell->GetPointId(otherPt), pt);
                cvec.push_back(CellDef(cellId, utilVector2D(pt[0], pt[1])));
            }

            // with this we sort the adjoining cells in such a way that their angles
            // are in a sequence
            pd2->GetPoint(p, pt);
            utilVector2D rootPt(pt[0], pt[1]);
            std::sort(cvec.begin(), cvec.end(),
                      [rootPt](CellDef const& pt1, CellDef const& pt2)
            {
                return rootPt.isLessAngle(pt1.second, pt2.second);
            });

            // the first two cells we keep like they are
            auto cvit = cvec.begin();
            ++cvit; ++cvit;

            // for all others we generate duplicates of the initial point
            while(cvit != cvec.end())
            {
                // duplicate the point
                vtkIdType newPtId = pt2->InsertNextPoint(pt);

                // we have to do this either once or twice
                std::vector<double> azVec;
                for(int i = 0; (i < 2) && (cvit != cvec.end()); ++i)
                {
                    pd2->ReplaceCellPoint(cvit->first, p, newPtId);

                    // if we reach the end without a second cell, this is another end point
                    ++cvit;
                    if((cvit == cvec.end()) && (0 == i))
                        endPoints.insert(newPtId);
                }
            }
        }
    }
    pd2->DeleteLinks();
    pd2->BuildLinks();

    // generate one or several polygons for output
    // note: our input consists now of only line segments (two end points), points that
    //   never have m
    points = pd2->GetPoints();
    boost::dynamic_bitset<> pointNotVisited(points->GetNumberOfPoints());
    pointNotVisited.set();
    outPolygon->Allocate();

    // if we have end points, follow them to the other end, generating thus a polygon
    while(!endPoints.empty())
    {
        vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
        vtkIdType ptId = *endPoints.begin(),
                  cellId = -1;
        endPoints.erase(endPoints.begin());
        double pt[3];
        points->GetPoint(ptId, pt);
        ptIds->InsertNextId(ptId);
        pointNotVisited.reset(ptId);
        vtkIdType ncells,
                  *cells;
        pd2->GetPointCells(ptId, ncells, cells);
        do
        {
            vtkIdType cellId2 = cells[0];
            if(cellId == cellId2)
                cellId2 = cells[1];
            vtkCell* cell = pd2->GetCell(cellId2);
            vtkIdType ptId2 = cell->GetPointId(0);
            if(ptId == ptId2)
                ptId2 = cell->GetPointId(1);
            points->GetPoint(ptId2, pt);
            ptIds->InsertNextId(ptId2);
            pointNotVisited.reset(ptId2);
            pd2->GetPointCells(ptId2, ncells, cells);
            ptId = ptId2;
            cellId = cellId2;
        }
        while(2 == ncells);
        endPoints.erase(endPoints.find(ptId));

        // close the polygon
        ptIds->InsertNextId(ptIds->GetId(0));
        outPolygon->InsertNextCell(VTK_POLYGON, ptIds);
    }

    // from now, only closed polygons can exist, so we have to always add points
    // until we come back to the start point
    while(!pointNotVisited.none())
    {
        vtkSmartPointer<vtkIdList> ptIds = vtkSmartPointer<vtkIdList>::New();
        vtkIdType firstPtId = pointNotVisited.find_first(),
                  ptId = firstPtId,
                  cellId = -1;
        double pt[3];
        points->GetPoint(ptId, pt);
        ptIds->InsertNextId(ptId);
        pointNotVisited.reset(ptId);
        vtkIdType ncells,
                  *cells;
        pd2->GetPointCells(ptId, ncells, cells);
        do
        {
            vtkIdType cellId2 = cells[0];
            if(cellId == cellId2)
                cellId2 = cells[1];
            vtkCell* cell = pd2->GetCell(cellId2);
            vtkIdType ptId2 = cell->GetPointId(0);
            if(ptId == ptId2)
                ptId2 = cell->GetPointId(1);
            points->GetPoint(ptId2, pt);
            ptIds->InsertNextId(ptId2);
            pointNotVisited.reset(ptId2);
            pd2->GetPointCells(ptId2, ncells, cells);
            ptId = ptId2;
            cellId = cellId2;
        }
        while(ptId != firstPtId);
        outPolygon->InsertNextCell(VTK_POLYGON, ptIds);
    }

    // finish the output polydata
    vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();
    newPoints->DeepCopy(points);
    outPolygon->SetPoints(newPoints);

    return 1;
}

int vtkAtgContourLoopExtraction::RequestInformation(vtkInformation* request,
                                                    vtkInformationVector** inputVector,
                                                    vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgContourLoopExtraction::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
