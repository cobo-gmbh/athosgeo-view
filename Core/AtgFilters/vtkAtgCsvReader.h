/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCsvReader.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgCsvReaderFilter_h
#define vtkAtgCsvReaderFilter_h

#include <string>
#include <vtkTableAlgorithm.h>
#include <AtgFiltersModule.h>

class ATGFILTERS_EXPORT vtkAtgCsvReader: public vtkTableAlgorithm
{
public:

    static vtkAtgCsvReader* New();
    vtkTypeMacro(vtkAtgCsvReader, vtkTableAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkGetStringMacro(FileName)
    vtkSetStringMacro(FileName)

    vtkGetStringMacro(FieldDelimiterCharacters)
    vtkSetStringMacro(FieldDelimiterCharacters)

    vtkGetStringMacro(EmptyStringReplacement)
    vtkSetStringMacro(EmptyStringReplacement)

    vtkGetMacro(AddTabFieldDelimiter, bool)
    vtkSetMacro(AddTabFieldDelimiter, bool)

    vtkGetMacro(EmptyNumberReplacement, double)
    vtkSetMacro(EmptyNumberReplacement, double)

    vtkGetMacro(RemoveLastLineIfLessCellsThanHeader, bool)
    vtkSetMacro(RemoveLastLineIfLessCellsThanHeader, bool)

    vtkGetMacro(TreatExcelErrorsAsEmpty, bool)
    vtkSetMacro(TreatExcelErrorsAsEmpty, bool)

    vtkGetStringMacro(WarningMessages);
    vtkSetStringMacro(WarningMessages);

    vtkGetStringMacro(ErrorMessage);
    vtkSetStringMacro(ErrorMessage);

protected:

    vtkAtgCsvReader();
    ~vtkAtgCsvReader();

    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgCsvReader(vtkAtgCsvReader const&);
    void operator=(vtkAtgCsvReader const&);

    // append a warning message to the existing one
    void appendWarningMessage(std::string const& msg);

    char* FileName;
    char* FieldDelimiterCharacters;
    char* EmptyStringReplacement;
    bool AddTabFieldDelimiter;
    double EmptyNumberReplacement;
    bool RemoveLastLineIfLessCellsThanHeader;
    char *WarningMessages,
         *ErrorMessage;

    // with this set to true, all strings starting with # and ending with ! are treated
    // like an empty value
    bool TreatExcelErrorsAsEmpty;

    // output
    bool CellsOutsideRangeExist;

};

#endif
