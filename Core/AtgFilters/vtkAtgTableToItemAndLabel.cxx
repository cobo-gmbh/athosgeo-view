/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToItemAndLabel.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <memory>
#include <map>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <utilNormNames.h>
#include <utilSampleToGridCell.h>
#include <vtkAtgTableToSamplings.h>
#include <vtkTableToPolyData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTable.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgTableToItemAndLabel.h>

vtkStandardNewMacro(vtkAtgTableToItemAndLabel)

static const double epsMeters = 0.01;

vtkAtgTableToItemAndLabel::vtkAtgTableToItemAndLabel()
:   SizeFactor(0.),
    Symbol3D(-1),
    LabelAttOrPrefix(nullptr),
    IsHoles(false),
    Decorations(nullptr)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(2);
}

vtkAtgTableToItemAndLabel::~vtkAtgTableToItemAndLabel()
{
    SetLabelAttOrPrefix(nullptr);
    SetDecorations(nullptr);
}

int vtkAtgTableToItemAndLabel::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // main input - table with data to be displayed
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkTable");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToItemAndLabel::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0: // output geometry
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
            return 1;

        case 1: // output labels
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgTableToItemAndLabel::RequestData(vtkInformation* request,
                                           vtkInformationVector** inputVector,
                                           vtkInformationVector* outputVector)
{
    (void)request;

    // get the input table
    vtkTable* inputTable = vtkTable::GetData(inputVector[0]);
    if(nullptr == inputTable)
        return 1;
    vtkSmartPointer<vtkTable> inTable = vtkSmartPointer<vtkTable>::New();
    inTable->ShallowCopy(inputTable);

    // prepare the table to collect info for decoration and labels
    vtkSmartPointer<vtkStringArray> labelsArr = vtkSmartPointer<vtkStringArray>::New();
    labelsArr->SetName("__Labels__");
    labelsArr->SetNumberOfTuples(inTable->GetNumberOfRows());
    inTable->AddColumn(labelsArr);
    vtkSmartPointer<vtkIntArray> decTypeArr = vtkSmartPointer<vtkIntArray>::New();
    decTypeArr->SetName("__DecType__");
    decTypeArr->SetNumberOfTuples(inTable->GetNumberOfRows());
    decTypeArr->Fill(-1.);
    inTable->AddColumn(decTypeArr);
    vtkSmartPointer<vtkFloatArray> decSizeArr = vtkSmartPointer<vtkFloatArray>::New();
    decSizeArr->SetName("__DecSize__");
    decSizeArr->SetNumberOfTuples(inTable->GetNumberOfRows());
    decSizeArr->Fill(0.);
    inTable->AddColumn(decSizeArr);
    vtkSmartPointer<vtkFloatArray> shiftUpArr = vtkSmartPointer<vtkFloatArray>::New();
    shiftUpArr->SetName("__ShiftUp__");
    shiftUpArr->SetNumberOfTuples(inTable->GetNumberOfRows());
    shiftUpArr->Fill(0.);
    inTable->AddColumn(shiftUpArr);

    // we use the SizeFactor here to specify whether we need to handle the samples
    // one by one or globally - with one single SizeFactor for example
    if(0. < SizeFactor)
    {
        labelsArr->DeepCopy(inTable->GetColumnByName(LabelAttOrPrefix));
        decTypeArr->Fill(Symbol3D);
        decSizeArr->Fill(SizeFactor);
        double shift = 1.5 * SizeFactor;
        if(Symbol3D == utilSampleToGridCell::BLAST_PILE)
            shift = 3. * SizeFactor;
        shiftUpArr->Fill(shift);
    }

    // otherwise we need to handle the sampling input table in more detail
    else
    {
        // generate sampling ID if not present yet, and fill the __Label__ column for those
        // rows that will need a label
        handleSamplingIds(inTable, LabelAttOrPrefix, IsHoles);

        // hole type
        std::string holeTypeName = utilNormNames::getName(utilNormNames::HOLETYPE);
        vtkAbstractArray* holeTypeArr = inTable->GetColumnByName(holeTypeName.c_str());

        // loop over sampling types and fill the __DecType__, __DecSize__ and __ShiftUp__ columns
        // note: the decorations are vectors of 3 strings:
        //   1. type string (string)
        //   2. decorator id ()
        //   3. decorator radius (double)
        // decorator ids are as follows: see definitions in utilSampleToGridCell class
        // note: only VTK cell types can be generated as cells of an unstructured grid,
        //       so e.g. an octahedron cannot be done! And why do we want this? To make
        //       it consistent / more understandable for the user: samples are also like
        //       the blocks of a block model, also regarding selection (which is not
        //       possible for points with the Athos Render View anyway)
        // note 2: octahedron is not directly available, but polyhedron is possible and
        //         allows the generation of an octahedron like many others!
        std::vector<std::vector<std::string>> decs = parseDecorations(Decorations);
        for(auto it1 = decs.begin(); it1 != decs.end(); ++it1)
        {
            std::string decorationType = (*it1)[0];

            for(vtkIdType r = 0; r < inTable->GetNumberOfRows(); ++r)
            {
                std::string ht("(all)");
                if(nullptr != holeTypeArr)
                    ht = holeTypeArr->GetVariantValue(r).ToString();
                if(ht == decorationType)
                {
                    int type = std::stoi((*it1)[1]);
                    if(!IsHoles)
                        type += utilSampleToGridCell::SPHERE;
                    float size = std::stof((*it1)[2]),
                          shift = .5f * size;
                    if(!IsHoles)
                        shift = 1.5f * size;
                    if(type == utilSampleToGridCell::BLAST_PILE)
                        shift = 3.f * size;
                    decTypeArr->SetValue(r, type);
                    decSizeArr->SetValue(r, size);
                    shiftUpArr->SetValue(r, shift);
                }
            }

            // done if we do not have a holeType attribute
            if(nullptr == holeTypeArr)
                break;
        }
    }

    // convert table into sampling
    vtkSmartPointer<vtkAtgTableToSamplings> generateSampling =
            vtkSmartPointer<vtkAtgTableToSamplings>::New();
    generateSampling->SetInputData(inTable);
    generateSampling->Update();
    vtkPolyData* dataItems = generateSampling->GetOutput();

    // get and prepare the output grid
    vtkUnstructuredGrid* outputItemsUGrid =
            vtkUnstructuredGrid::GetData(outputVector->GetInformationObject(0));
    outputItemsUGrid->Allocate();
    vtkSmartPointer<vtkPoints> outPoints = vtkSmartPointer<vtkPoints>::New();
    outputItemsUGrid->SetPoints(outPoints);

    // we do not use normals: this would be fine for round display (sphere, round bar)
    // with Gouraud shading, but it cannot be turned off for the non-round cells
    //vtkSmartPointer<vtkFloatArray> narr = vtkSmartPointer<vtkFloatArray>::New();
    //narr->SetNumberOfComponents(3);
    //narr->SetName("Normals");
    //vtkPointData* pData = outputSampling->GetPointData();
    //pData->AddArray(narr);
    //pData->SetNormals(narr);

    // prepare a table for generating the labels
    vtkSmartPointer<vtkTable> tabLabels = vtkSmartPointer<vtkTable>::New();
    vtkSmartPointer<vtkDoubleArray> tabCollarArr = vtkSmartPointer<vtkDoubleArray>::New();
    std::string collarName = utilNormNames::getName(utilNormNames::COLLAR);
    tabCollarArr->SetName(collarName.c_str());
    tabCollarArr->SetNumberOfComponents(3);
    tabLabels->AddColumn(tabCollarArr);
    vtkSmartPointer<vtkStringArray> tabLabelsArr = vtkSmartPointer<vtkStringArray>::New();
    tabLabelsArr->SetName("Labels");
    tabLabels->AddColumn(tabLabelsArr);

    // with this utility we add the cells, one by one
    std::unique_ptr<utilSampleToGridCell>
            sampleToCell(new utilSampleToGridCell(outputItemsUGrid));

    // generate sampling cells and label table entries
    for(vtkIdType c = 0; c < dataItems->GetNumberOfCells(); ++c)
    {
        // get cell type and geometry
        vtkCell* cell = dataItems->GetCell(c);
        int inType = cell->GetCellType();
        vtkPoints* pts = cell->GetPoints();

        // get label and decorator parameters from the initial table
        // note: we assume that vtkAtgTableToSampling does not change the order or size
        //       of items in either the table or the geometry
        std::string label = labelsArr->GetValue(c);
        utilSampleToGridCell::CellType outType =
                static_cast<utilSampleToGridCell::CellType>(decTypeArr->GetValue(c));
        float decSize = decSizeArr->GetValue(c),
              shiftUp = shiftUpArr->GetValue(c);

        // add a cell to the output grid
        sampleToCell->addCell(inType, outType, pts, decSize);

        // if we have a label, add it to the labels table
        if(!label.empty())
        {
            vtkIdType row = tabLabels->InsertNextBlankRow();
            tabLabelsArr->SetValue(row, label.c_str());
            float refPt[3];
            refPt[0] = static_cast<float>(pts->GetPoint(0)[0]);
            refPt[1] = static_cast<float>(pts->GetPoint(0)[1]);
            refPt[2] = static_cast<float>(pts->GetPoint(0)[2]) + shiftUp;
            tabCollarArr->SetTuple(row, refPt);
        }
    }

    // passing the data from the temporary poly data (sampling) to the output
    // note: vtkAtgTableToSampling already removed the temporary data arrays
    outputItemsUGrid->GetCellData()->PassData(dataItems->GetCellData());

    // generate polydata from labels table
    vtkSmartPointer<vtkTableToPolyData> generatePoints = vtkSmartPointer<vtkTableToPolyData>::New();
    generatePoints->SetInputData(tabLabels);
    generatePoints->SetXColumn(collarName.c_str());
    generatePoints->SetXComponent(0);
    generatePoints->SetYColumn(collarName.c_str());
    generatePoints->SetYComponent(1);
    generatePoints->SetZColumn(collarName.c_str());
    generatePoints->SetZComponent(2);
    generatePoints->Update();

    // generate the label points output from the labels table
    vtkPolyData* outputLabels = vtkPolyData::GetData(outputVector->GetInformationObject(1));
    outputLabels->DeepCopy(generatePoints->GetOutput());

    return 1;
}

void vtkAtgTableToItemAndLabel::handleSamplingIds(vtkTable* samplingTable,
                                                  std::string const& prefix, bool isHoles)
{
    // name depending on sampling type, plus collar and depth arrays
    std::string idName = isHoles ?
                         utilNormNames::getName(utilNormNames::HOLEID) :
                         utilNormNames::getName(utilNormNames::SAMPID),
                collName = utilNormNames::getName(utilNormNames::COLLAR),
                depName = utilNormNames::getName(utilNormNames::DEPTH);
    vtkAbstractArray* samplingIdArr = samplingTable->GetColumnByName(idName.c_str());
    vtkDataArray *collArr = vtkDataArray::SafeDownCast(samplingTable->GetColumnByName(collName.c_str())),
                 *depArr = vtkDataArray::SafeDownCast(samplingTable->GetColumnByName(depName.c_str()));
    assert(nullptr != collArr);
    bool haveIds = nullptr != samplingIdArr,
         haveDepth = nullptr != depArr;

    // generate the new names attribute if not present
    vtkSmartPointer<vtkStringArray> idArr;
    if(!haveIds)
    {
        idArr = vtkSmartPointer<vtkStringArray>::New();
        idArr->SetNumberOfTuples(samplingTable->GetNumberOfRows());
        idArr->SetName(idName.c_str());
        samplingTable->AddColumn(idArr);
    }

    // get the labels array
    vtkStringArray* labelsArr =
            vtkStringArray::SafeDownCast(samplingTable->GetColumnByName("__Labels__"));
    assert(nullptr != labelsArr);

    // if we have both ids and depth, we need for each drill hole the smallest depth
    std::map<std::string, double> minDepths;
    if(haveIds && haveDepth)
    {
        for(vtkIdType row = 0; row < samplingTable->GetNumberOfRows(); ++row)
        {
            std::string idName = samplingIdArr->GetVariantValue(row).ToString();
            double depth = depArr->GetComponent(row, 0);
            auto it = minDepths.find(idName);
            if(minDepths.end() != it)
            {
                if(it->second > depth)
                    minDepths[idName] = depth;
            }
            else
            {
                minDepths[idName] = depth;
            }
        }
    }

    // prepare for the possible sampling id generation
    int digs = int(::floor(::log10(samplingTable->GetNumberOfRows()))) + 1,
        num = 1;

    // go through input table and collect labels and
    // generate the hole or sample IDs if required
    for(vtkIdType row = 0; row < samplingTable->GetNumberOfRows(); ++row)
    {
        // sample location
        std::vector<double> loc(std::initializer_list<double>(
        {
            collArr->GetComponent(row, 0),
            collArr->GetComponent(row, 1),
            collArr->GetComponent(row, 2)
        }));

        // try to find the collar and either generate or get the id name
        std::string idName;

        if(haveIds)
        {
            idName = samplingIdArr->GetVariantValue(row).ToString();
        }
        else
        {
            idName = std::to_string(num++);
            while(digs > static_cast<int>(idName.size()))
                idName = "0" + idName;
            idName = prefix + idName;
        }

        bool needsLabel = true;
        if(haveDepth && haveIds)
            needsLabel = epsMeters > ::fabs(minDepths[idName] - depArr->GetComponent(row, 0));
        labelsArr->SetValue(row, (needsLabel ? idName.c_str() : ""));

        // apply the id name if none did exist
        if(!haveIds)
            idArr->SetValue(row, idName.c_str());
    }
}

std::vector<std::vector<std::string>> vtkAtgTableToItemAndLabel::parseDecorations(std::string const& decorations)
{
    // used to split the file in lines
    static const boost::regex linesregx(";");

    // used to split each line to tokens, assuming ',' as column separator
    static const boost::regex fieldsregx(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

    std::vector<std::vector<std::string>> result;

    // iterator splits data to lines
    boost::sregex_token_iterator li(decorations.begin(), decorations.end(), linesregx, -1);
    boost::sregex_token_iterator end;

    while(li != end)
    {
        std::string line = li->str();
        ++li;

        // split line to tokens
        boost::sregex_token_iterator ti(line.begin(), line.end(), fieldsregx, -1);
        boost::sregex_token_iterator end2;

        std::vector<std::string> row;

        while(ti != end2)
        {
            std::string token = ti->str();
            boost::trim_if(token, boost::is_any_of("\""));
            ++ti;
            row.push_back(token);
        }

        if(line.back() == ',')
        {
            // last character was a separator
            row.push_back("");
        }

        result.push_back(row);
    }

    return result;
}

void vtkAtgTableToItemAndLabel::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
