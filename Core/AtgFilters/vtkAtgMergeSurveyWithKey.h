/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgMergeSurveyWithKey.h

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgMergeSurveyWithKeyFilter_h
#define vtkAtgMergeSurveyWithKeyFilter_h

#include <string>
#include <vtkTableAlgorithm.h>
#include <AtgFiltersModule.h>

class ATGFILTERS_EXPORT vtkAtgMergeSurveyWithKey: public vtkTableAlgorithm
{
public:

    static vtkAtgMergeSurveyWithKey* New();
    vtkTypeMacro(vtkAtgMergeSurveyWithKey, vtkTableAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkGetStringMacro(KeyColumn)
    vtkSetStringMacro(KeyColumn)

    vtkSetMacro(NegativeDipDown, bool)
    vtkGetMacro(NegativeDipDown, bool)

    vtkGetStringMacro(FirstTableName)
    vtkSetStringMacro(FirstTableName)

    vtkGetStringMacro(SecondTableName)
    vtkSetStringMacro(SecondTableName)

    inline std::string ErrorMessage() const
    {
        return Err;
    }

    inline bool FatalError() const
    {
        return FatalErr;
    }

protected:

    vtkAtgMergeSurveyWithKey();
    ~vtkAtgMergeSurveyWithKey();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgMergeSurveyWithKey(vtkAtgMergeSurveyWithKey const&);
    void operator=(vtkAtgMergeSurveyWithKey const&);

    // column name for the merge key
    char* KeyColumn;

    // flag: if true, negative dip angles are treated as downwards, otherwise they
    //       would be for upwards drillings
    bool NegativeDipDown;

    // names of the two tables, for meaningful error messages
    char* FirstTableName;
    char* SecondTableName;

    // if we find error conditions or warnings, put a message here
    std::string Err;

    // set to true if there was a fatal error that requires termination of the process
    bool FatalErr;

};

#endif
