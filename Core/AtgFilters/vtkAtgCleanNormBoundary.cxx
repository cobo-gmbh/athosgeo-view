/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCleanNormBoundary.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <set>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkCell.h>
#include <vtkCellArrayIterator.h>
#include <vtkPolygon.h>
#include <vtkPoints.h>
#include <vtkIdList.h>

#include <vtkDataArray.h>
#include <vtkCellType.h>
#include <vtkType.h>

#include <vtkTriangleFilter.h>
#include <vtkPolyDataNormals.h>
#include <vtkFeatureEdges.h>
#include <vtkStripper.h>

#include <vtkCleanPolyData.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkAtgCleanNormBoundary.h>

vtkStandardNewMacro(vtkAtgCleanNormBoundary)

static double const distEps = 0.01;

namespace
{

// check if two points coincide within the range of distEps
bool coincide(vtkIdType inx1, vtkIdType inx2, vtkPoints* pts)
{
    double pt1[3];
    pts->GetPoint(inx1, pt1);
    double pt2[3];
    pts->GetPoint(inx2, pt2);

    double diff[3] = {pt2[0] - pt1[0], pt2[1] - pt1[1], pt2[2] - pt1[2]},
           dist = ::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);

    return dist < distEps;
}

// revert the order of IDs in a id list
void revert(vtkIdList* ids)
{
    vtkIdType p1, p2;
    for(p1 = 0, p2 = ids->GetNumberOfIds() - 1; p2 > p1; ++p1, --p2)
    {
        vtkIdType tmp = ids->GetId(p2);
        ids->SetId(p2, ids->GetId(p1));
        ids->SetId(p1, tmp);
    }
}

}

vtkAtgCleanNormBoundary::vtkAtgCleanNormBoundary()
:   ClosePolygons(true),
    ErrorMsg(nullptr),
    EmitErrors(false)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);
}

vtkAtgCleanNormBoundary::~vtkAtgCleanNormBoundary()
{
    SetErrorMsg(nullptr);
}

int vtkAtgCleanNormBoundary::FillInputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // "raw" boundary
            info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCleanNormBoundary::FillOutputPortInformation(int port, vtkInformation* info)
{
    switch(port)
    {
        case 0:
            // cleaned boundary
            info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
            return 1;

        default:
            return 0;
    }
}

int vtkAtgCleanNormBoundary::RequestData(vtkInformation* request,
                                         vtkInformationVector** inputVector,
                                         vtkInformationVector* outputVector)
{
    // input port 0: the input raw boundary
    vtkPolyData* inputRaw = vtkPolyData::GetData(inputVector[0]);
    if(nullptr == inputRaw)
    {
        static const char err[] = "No input";
        ErrorMsg = const_cast<char*>(err);
        return 1;
    }

    // get the output poly
    vtkPolyData *outBoundary = vtkPolyData::GetData(outputVector, 0);
    if(nullptr == outBoundary)
    {
        static const char err[] = "No output";
        ErrorMsg = const_cast<char*>(err);
        return 1;
    }

    // first merge any polyline segments with common end points
    vtkSmartPointer<vtkPolyData> mergedSegs = vtkSmartPointer<vtkPolyData>::New();
    mergeSegments(inputRaw, mergedSegs);

    vtkSmartPointer<vtkPolyData> closedSegs = vtkSmartPointer<vtkPolyData>::New();
    if(ClosePolygons)
    {
        // copy the segments and close those that are not closed yet
        copyCloseSegments(mergedSegs, closedSegs);
    }
    else
    {
        // if closing is not required, just copy
        closedSegs->ShallowCopy(mergedSegs);
    }

    // do some cleanup
    vtkSmartPointer<vtkCleanPolyData> cleanPoly = vtkSmartPointer<vtkCleanPolyData>::New();
    cleanPoly->SetInputData(closedSegs);
    cleanPoly->Update();
    outBoundary->ShallowCopy(cleanPoly->GetOutput());

    return 1;
}

void vtkAtgCleanNormBoundary::errorMessage(std::string const& msg)
{
    if(EmitErrors)
        vtkOutputWindowDisplayWarningText(("AtgCleanNormBoundary: " + msg + "\n").c_str());

    SetErrorMsg(msg.c_str());
}

void vtkAtgCleanNormBoundary::mergeSegments(vtkPolyData* inRaw, vtkPolyData* merged)
{
    // get the input points and lines
    vtkPoints* inPoints = inRaw->GetPoints();
    vtkCellArray* inLines = inRaw->GetLines();

    // generate output points and lines
    vtkSmartPointer<vtkPoints> outPoints = vtkSmartPointer<vtkPoints>::New();
    outPoints->SetDataType(VTK_DOUBLE);
    outPoints->ShallowCopy(inPoints);
    vtkSmartPointer<vtkCellArray> outLines = vtkSmartPointer<vtkCellArray>::New();

    // here we will collect "locations" of cells that we already handled,
    // i.e. copied into the output - either directly or appended to another segment
    std::set<vtkIdType> cellDone;

    // iterate over all input lines
    vtkCellArrayIterator* cit = inLines->NewIterator();
    while(!cit->IsDoneWithTraversal())
    {
        // get the points of the current cell and go to next one
        vtkSmartPointer<vtkIdList> cPl = vtkSmartPointer<vtkIdList>::New();
        cit->GetCurrentCell(cPl);
        cit->GoToNextCell();

        // remember the location, in order to return before continuing the loop
        // note: it is possible that a line segment is already "done": if it was
        //   appended to some other line segment already
        vtkIdType cLoc = cit->GetCurrentCellId();
        if(cellDone.end() != cellDone.find(cLoc))
            continue;
        cellDone.insert(cLoc);

        // create a new id list and start by copying the current input line segment
        vtkSmartPointer<vtkIdList> outPl = vtkSmartPointer<vtkIdList>::New();
        for(vtkIdType p = 0; p < cPl->GetNumberOfIds(); ++p)
            outPl->InsertNextId(cPl->GetId(p));

        // we want to repeat this iteration until we did it once without adding anything
        bool somethingAdded = false;
        do
        {
            // reset the flag and the traversal location
            somethingAdded = false;
            cit->GoToCell(cLoc);

            // go through all further line segments
            while(!cit->IsDoneWithTraversal())
            {
                vtkSmartPointer<vtkIdList> cPl2 = vtkSmartPointer<vtkIdList>::New();
                cit->GetCurrentCell(cPl2);
                cit->GoToNextCell();

                // like in the outer loop: do not care about line segments that were already handled
                vtkIdType cLoc2 = cit->GetCurrentCellId();
                if(cellDone.end() != cellDone.find(cLoc2))
                    continue;

                // check four possibilities how the output line and the current line segment can fit:
                // 1. end -> beginning
                // 2. end -> end
                // 3. beginning -> beginning
                // 4. beginning -> end
                bool join = false,
                     rev1 = false,
                     rev2 = false;
                if(coincide(outPl->GetId(outPl->GetNumberOfIds() - 1), cPl2->GetId(0), inPoints))
                {
                    join = true;
                }
                else if(coincide(outPl->GetId(outPl->GetNumberOfIds() - 1), cPl2->GetId(cPl2->GetNumberOfIds() - 1), inPoints))
                {
                    join = true;
                    rev2 = true;
                }
                else if(coincide(outPl->GetId(0), cPl2->GetId(0), inPoints))
                {
                    join = true;
                    rev1 = true;
                }
                else if(coincide(outPl->GetId(0), cPl2->GetId(cPl2->GetNumberOfIds() - 1), inPoints))
                {
                    join = true;
                    rev1 = rev2 = true;
                }

                // no fit - nothing else to do
                if(!join)
                    continue;
                else
                    cellDone.insert(cLoc2);

                // depending on the type of fit, revert the output line or the current input segment or both
                if(rev1)
                    revert(outPl);
                if(rev2)
                    revert(cPl2);

                // append input segment to output line
                for(vtkIdType p = 1; p < cPl2->GetNumberOfIds(); ++p)
                    outPl->InsertNextId(cPl2->GetId(p));

                // flag
                somethingAdded = true;
            }
        }
        while(somethingAdded);

        // add output line as a new "cell"
        outLines->InsertNextCell(outPl);
        cit->GoToCell(cLoc);
    }

    // generate the output polydata
    merged->SetPoints(outPoints);
    merged->SetLines(outLines);
}

void vtkAtgCleanNormBoundary::copyCloseSegments(vtkPolyData* merged, vtkPolyData* closed)
{
    // input points and lines
    vtkPoints* inPoints = merged->GetPoints();
    vtkCellArray* inLines = merged->GetLines();

    // generate output points and lines
    vtkSmartPointer<vtkPoints> outPoints = vtkSmartPointer<vtkPoints>::New();
    outPoints->SetDataType(VTK_DOUBLE);
    outPoints->ShallowCopy(inPoints);
    vtkSmartPointer<vtkCellArray> outLines = vtkSmartPointer<vtkCellArray>::New();

    // iterate through all lines
    inLines->InitTraversal();
    vtkSmartPointer<vtkIdList> cPl = vtkSmartPointer<vtkIdList>::New();
    while(inLines->GetNextCell(cPl))
    {
        // generate an output line, and start by copying the input line segment
        vtkSmartPointer<vtkIdList> outPl = vtkSmartPointer<vtkIdList>::New();
        outPl->DeepCopy(cPl);

        // repeat the first point if not closed
        if(!coincide(outPl->GetId(0), outPl->GetId(outPl->GetNumberOfIds() - 1), inPoints))
            outPl-> InsertNextId(outPl->GetId(0));

        // add line to output as new "cell"
        vtkSmartPointer<vtkPolygon> pg = vtkSmartPointer<vtkPolygon>::New();
        pg->PointIds->DeepCopy(outPl);
        outLines->InsertNextCell(pg);
    }

    // generate the output polydata
    closed->SetPoints(outPoints);
    closed->SetPolys(outLines);
}

int vtkAtgCleanNormBoundary::RequestInformation(vtkInformation* request,
                                            vtkInformationVector** inputVector,
                                            vtkInformationVector* outputVector)
{
    return 1;
}

void vtkAtgCleanNormBoundary::PrintSelf(std::ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
