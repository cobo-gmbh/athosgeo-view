/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgTableToBlocks.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgTableToBlocksFilter_h
#define vtkAtgTableToBlocksFilter_h

#include <vtkUnstructuredGridAlgorithm.h>
#include <AtgFiltersModule.h>

class ATGFILTERS_EXPORT vtkAtgTableToBlocks: public vtkUnstructuredGridAlgorithm
{
public:

    static vtkAtgTableToBlocks* New();
    vtkTypeMacro(vtkAtgTableToBlocks, vtkUnstructuredGridAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkSetVector3Macro(BlockSize, double)
    vtkGetVector3Macro(BlockSize, double)

    vtkSetMacro(RotAngle, double)
    vtkGetMacro(RotAngle, double)

protected:

    vtkAtgTableToBlocks();
    ~vtkAtgTableToBlocks();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgTableToBlocks(vtkAtgTableToBlocks const&);
    void operator=(vtkAtgTableToBlocks const&);

    // block size - if not already in table
    double BlockSize[3];

    // rotation angle - if not already in table
    double RotAngle;

};

#endif
