/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgCleanNormBoundary.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef vtkAtgCleanNormBoundary_h
#define vtkAtgCleanNormBoundary_h

#include <string>
#include <vtkPolyDataAlgorithm.h>
#include <AtgFiltersModule.h>

class vtkPolyData;

class ATGFILTERS_EXPORT vtkAtgCleanNormBoundary: public vtkPolyDataAlgorithm
{
public:

    static vtkAtgCleanNormBoundary* New();
    vtkTypeMacro(vtkAtgCleanNormBoundary, vtkPolyDataAlgorithm)
    void PrintSelf(std::ostream& os, vtkIndent indent);

    vtkGetMacro(ClosePolygons, bool)
    vtkSetMacro(ClosePolygons, bool)

    vtkSetStringMacro(ErrorMsg)
    vtkGetStringMacro(ErrorMsg)

    vtkSetMacro(EmitErrors, bool)
    vtkGetMacro(EmitErrors, bool)

protected:

    vtkAtgCleanNormBoundary();
    ~vtkAtgCleanNormBoundary();

    int RequestInformation(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    int FillInputPortInformation(int port, vtkInformation* info);
    int FillOutputPortInformation(int port, vtkInformation* info);

private:

    // copy constructor and assignment not implemented
    vtkAtgCleanNormBoundary(vtkAtgCleanNormBoundary const&);
    void operator=(vtkAtgCleanNormBoundary const&);

    // handle error conditions
    void errorMessage(std::string const& msg);

    // checks if segments have common ends, and if they have put them together
    // note: the polydata will have to be deleted with "Delete" afterwards
    void mergeSegments(vtkPolyData* inRaw, vtkPolyData* merged);

    // for every segment, check if it is closed (first and last point coinciding), and
    // if not, add the first point once more at the end
    // note: the polydata will have to be deleted with "Delete" afterwards
    void copyCloseSegments(vtkPolyData* merged, vtkPolyData* closed);

    // if true, polylines that are not closed will be closed automatically
    bool ClosePolygons;

    // error handling
    char* ErrorMsg;
    bool EmitErrors;

};

#endif
