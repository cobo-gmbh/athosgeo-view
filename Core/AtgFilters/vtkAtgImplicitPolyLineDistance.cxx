/*=========================================================================

   Program: AthosGEO
   Module:  vtkAtgImplicitPolyLineDistance.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/multi_polygon.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/register/ring.hpp>
#include <boost/geometry/geometries/adapted/boost_tuple.hpp>
#include <vtkSmartPointer.h>
#include <vtkAtgCleanNormBoundary.h>
#include <vtkObjectFactory.h>
#include <vtkAtgImplicitPolyLineDistance.h>

static const double deltaFactor = 0.0001;

namespace bg = boost::geometry;

BOOST_GEOMETRY_REGISTER_BOOST_TUPLE_CS(cs::cartesian)

typedef boost::tuple<double, double> zfbPair;
typedef bg::model::point<double, 2, bg::cs::cartesian> zfbPoint;
typedef bg::model::ring<zfbPair> zfbRing;
typedef bg::model::polygon<zfbPair> zfbPolygon;
typedef bg::model::multi_polygon<zfbPolygon> zfbMultiPolygon;

struct zfbRingLess
{
    bool pairLess(zfbPair const& p1, zfbPair const& p2) const
    {
        if(bg::get<0>(p1) < bg::get<0>(p2))
            return true;
        if(bg::get<0>(p1) > bg::get<0>(p2))
            return false;
        return bg::get<1>(p1) < bg::get<1>(p2);
    }

    bool operator()(zfbRing const& r1, zfbRing const& r2) const
    {
        unsigned long npts = std::min(r1.size(), r2.size());
        for(unsigned long n = 0; n < npts; ++n)
        {
            zfbPair const& pt1 = r1.at(n);
            zfbPair const& pt2 = r2.at(n);
            if(pairLess(pt1, pt2))
                return true;
            if(pairLess(pt2, pt1))
                return false;
        }
        return false;
    }
};

class vtkAtgImplicitPolyLineDistance::Internal
{
public:

    zfbMultiPolygon mPolyForDist,
                    mPolyForTopo;

};

vtkStandardNewMacro(vtkAtgImplicitPolyLineDistance)

vtkAtgImplicitPolyLineDistance::vtkAtgImplicitPolyLineDistance()
:   vtkImplicitFunction(),
    InvertDistances(false),
    d(nullptr)
{
}

vtkAtgImplicitPolyLineDistance::~vtkAtgImplicitPolyLineDistance()
{
    if(nullptr != d)
    {
        delete d;
        d = nullptr;
    }
}

void vtkAtgImplicitPolyLineDistance::SetInput(vtkPolyData* input)
{
    // clean and normalize the "raw boundary" poly data
    // - merge line segments with common end points
    // - close open polylines
    // - remove polygons with less than 3 points
    vtkSmartPointer<vtkAtgCleanNormBoundary> cleanBound = vtkSmartPointer<vtkAtgCleanNormBoundary>::New();
    cleanBound->SetClosePolygons(true);
    cleanBound->SetInputData(input);
    cleanBound->Update();
    if(nullptr != cleanBound->GetErrorMsg())
    {
        std::string msg(cleanBound->GetErrorMsg());
        vtkOutputWindowDisplayWarningText(("AtgClipGeometry: " + msg + "\n").c_str());
        return;
    }

    // boundary polyline
    vtkSmartPointer<vtkPolyData> boundPoly = vtkSmartPointer<vtkPolyData>::New();
    boundPoly->ShallowCopy(cleanBound->GetOutput());

    // we take the bounds for a step unit for the gradient calculation
    boundPoly->ComputeBounds();
    double bb[6];
    boundPoly->GetBounds(bb);
    delta = deltaFactor * std::max(::fabs(bb[1] - bb[0]), ::fabs(bb[3] - bb[2]));

    // generate a new polyline
    if(nullptr != d)
        delete d;
    d = new vtkAtgImplicitPolyLineDistance::Internal();

    // generate multi-polygon structure from boundary polygons
    vtkPoints* pts = boundPoly->GetPoints();
    for(vtkIdType p = 0; p < boundPoly->GetNumberOfCells(); ++p)
    {
        vtkCell* cell = boundPoly->GetCell(p);
        if((nullptr == cell) && (2 < cell->GetPointIds()->GetNumberOfIds()))
            continue;

        // collect the points into a vector
        std::vector<zfbPair> plist;
        for(vtkIdType n = 0; n < cell->GetPointIds()->GetNumberOfIds(); ++n)
        {
            double pt[3];
            pts->GetPoint(cell->GetPointId(static_cast<int>(n)), pt);
            plist.push_back(zfbPair({pt[0], pt[1]}));
        }

        // close if not the case yet
        vtkIdType n0 = cell->GetPointId(0);
        if(n0 != cell->GetPointId(static_cast<int>(cell->GetPointIds()->GetNumberOfIds() - 1)))
            plist.push_back(plist[0]);

        // generate a "ring" from the point vector
        zfbRing ring(plist.begin(), plist.end());

        // check orientation and reverse if necessary
        double ar = bg::area(ring);
        if(ar < 0.)
            bg::reverse(ring);

        // use the "ring" as the "exterior ring" of a "polygon"
        // note: other rings would be holes and have to turn the other way round
        zfbPolygon poly;
        bg::exterior_ring(poly) = ring;

        // keep a copy without the duplicated ring for the topology
        zfbPolygon poly2(poly);
        d->mPolyForTopo.push_back(poly2);

        // add a second inner ring, so the polygon has a "hole" of it's own size
        // and add that to the multi-polygon for distance calculation
        poly.inners().resize(1);
        poly.inners()[0] = ring;
        d->mPolyForDist.push_back(poly);
    }
}

double vtkAtgImplicitPolyLineDistance::EvaluateFunction(double x[3])
{
    // with this we get a distance that is always positive
    // note: without the inner ring, the distance would be always 0
    //   inside the polygon - not working for the clipping
    zfbPoint point(x[0], x[1]);
    double func = bg::distance(point, d->mPolyForDist);

    // the within function only works if we do _not_ have an inner ring,
    // because then all points are considered "outside"
    if(bg::within(point, d->mPolyForTopo))
        func = -func;

    // invert if option
    if(InvertDistances)
        func = -func;

    return func;
}

void vtkAtgImplicitPolyLineDistance::EvaluateGradient(double x[3], double g[3])
{
    double d0 = EvaluateFunction(x);
    double px[3] = {x[0] + delta, x[1], 0.},
           py[3] = {x[0], x[1] + delta, 0.};
    g[0] = (EvaluateFunction(px) - d0) / delta;
    g[1] = (EvaluateFunction(py) - d0) / delta;
    g[2] = 0.;
}

void vtkAtgImplicitPolyLineDistance::PrintSelf(std::ostream& os, vtkIndent indent)
{
    // todo - more details
    vtkImplicitFunction::PrintSelf(os, indent);
}
