/*=========================================================================

   Program: AthosGEO
   Module:  utilDebugLog.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <string>
#include <iostream>
#include <utilDebugLog.h>

static std::string get_homedir(void)
{
#ifdef _WIN32
    std::string homedir = std::string(::getenv("HOMEDRIVE")) + std::string(::getenv("HOMEPATH")) + "\\";
#else
    std::string homedir = std::string(::getenv("HOME")) + "/";
#endif
    return homedir;
}

utilDebugLog::utilDebugLog()
{
    // this generates a log file name in the home directory that does not exist yet
    // note: if this constructor is called later on somewhere in the program, the effect
    //   is that it will again generate a new log file name, but that will never be used
    //   because in the log() function only the first call will open a file
    std::string homedir = get_homedir(),
                logfile;
    for(int n = 1; n < 1000; ++n)
    {
        std::string num = std::to_string(n);
        logfile = homedir + "athosgeo_" + std::string(3 - num.size(), '0') + num + ".log";
        std::ifstream inf(logfile);
        if(inf.fail())
            break;
        inf.close();
    }

    // initialize the log() function
    // The first call in a process will generate an output file (stream) while
    // for consecutive calls always the same logfile will be returned
    log(logfile);
}

utilDebugLog::~utilDebugLog()
{
}

std::ofstream& utilDebugLog::log(std::string const& logf)
{
    // open during the first call, from then on just return the same open file stream
    static std::ofstream log(logf);

    // assign the
    return log;
}
