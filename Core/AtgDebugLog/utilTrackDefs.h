/*=========================================================================

   Program: AthosGEO
   Module:  utilTrackDefs.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef UTILTRACKDEFS_H
#define UTILTRACKDEFS_H

// for the filter timer
#include <iostream>
#include <iomanip>
#include <string>
#include <boost/timer/timer.hpp>
#include <boost/current_function.hpp>

// definitions to turn tracking of certain activities on or off
// ------------------------------------------------------------

// timer tracking for functions (like filters)
// note: use _t.report() to emit intermediate times
/* <- add/remove first / to enable/disable definition
class __TrackFunctionTime
{
public:
    inline __TrackFunctionTime(std::string const& func)
    :   _func(func),
        _t(std::string("%ws (" + func + ")\n")),
        _et(0)
    {
        std::cout << "BEGIN " << func << std::endl;
    }
    inline static void emit_timer_time(boost::timer::nanosecond_type t)
    {
        std::cout << std::fixed << std::setprecision(6)
                  << (double(t) * .000000001) << "s";
    }
    inline static void emit_collected(boost::timer::nanosecond_type t, std::string const& what)
    {
        emit_timer_time(t);
        std::cout << " " << what << std::endl;
    }
    inline void elapsed(std::string const& what)
    {
        boost::timer::nanosecond_type _et_new = _t.elapsed().wall;
        emit_timer_time(_et_new);
        std::cout << " (";
        emit_timer_time(_et_new - _et);
        std::cout << ") " << what << std::endl;
        _et = _et_new;
    }
    inline ~__TrackFunctionTime()
    {
        std::cout << "END " << _func << std::endl;
    }
    std::string _func;
    boost::timer::auto_cpu_timer _t;
    boost::timer::nanosecond_type _et;
};

#define TRACK_FUNCTION_TIME \
    __TrackFunctionTime _ftm(BOOST_CURRENT_FUNCTION);
#define TRACK_ELAPSED(what) \
    _ftm.elapsed(what);
#define TRACK_COLLECT_INIT(name) \
    boost::timer::nanosecond_type _##name##_collect = 0., \
                                  _##name##_temp;
#define TRACK_COLLECT_START(name) \
    _##name##_temp = _ftm._t.elapsed().wall;
#define TRACK_COLLECT_STOP(name) \
    _##name##_collect += _ftm._t.elapsed().wall - _##name##_temp;
#define TRACK_COLLECT_REPORT(name, what) \
    __TrackFunctionTime::emit_collected(_##name##_collect, what);
/*/
#define TRACK_FUNCTION_TIME
#define TRACK_ELAPSED(what)
#define TRACK_COLLECT_INIT(name)
#define TRACK_COLLECT_START(name)
#define TRACK_COLLECT_STOP(name)
#define TRACK_COLLECT_REPORT(name, what)
//*/
// this would be the full default format string:
//boost::timer::auto_cpu_timer _t(std::string("%ws wall, %us user + %ss system = %ts CPU (%p%) (" + _fn + ")\n"));

// table view: column sorting
//*
#undef TRACK_COLUMN_SORT
/*/
#define TRACK_COLUMN_SORT
//*/

// main window: turn on tracking into debug file, using the utilDebugLog class
//*
#undef TRACK_INTO_DEBUG_FILE
/*/
#define TRACK_INTO_DEBUG_FILE
//*/

// main window: loading plugins automatically
//*
#undef TRACK_PLUGIN_LOADING
/*/
#define TRACK_PLUGIN_LOADING
//*/

// main window: list of available resources in the program
//*
#undef TRACK_AVAILABLE_RESOURCES
/*/
#define TRACK_AVAILABLE_RESOURCES
//*/

// selection ids widget: track selection changes
//*
#undef TRACK_ACTIVE_SELECTION_CHANGED
/*/
#define TRACK_ACTIVE_SELECTION_CHANGED
//*/

#endif
