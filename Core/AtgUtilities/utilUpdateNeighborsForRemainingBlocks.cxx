/*=========================================================================

   Program: AthosGEO
   Module:  utilUpdateNeighborsForRemainingBlocks.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <utilNormNames.h>
#include <utilGetNeighbor.h>
#include <vtkLongLongArray.h>
#include <vtkDataArray.h>
#include <utilUpdateNeighborsForRemainingBlocks.h>

utilUpdateNeighborsForRemainingBlocks::utilUpdateNeighborsForRemainingBlocks(vtkUnstructuredGrid* grid)
:   grid_(grid)
{
}

utilUpdateNeighborsForRemainingBlocks::~utilUpdateNeighborsForRemainingBlocks()
{
}

void utilUpdateNeighborsForRemainingBlocks::updateForRemaining(const boost::dynamic_bitset<>& remaining)
{
    // try to find block and neighbor id arrays in the grid, and if one of them does not exist,
    // we have nothing to do
    std::string blockIdName = utilNormNames::getName(utilNormNames::BLOCKID),
                nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
    vtkDataArray *blockIdArr = grid_->GetCellData()->GetArray(blockIdName.c_str()),
                 *nbArr = grid_->GetCellData()->GetArray(nbName.c_str());
    if((nullptr == blockIdArr) || (nullptr == nbArr))
        return;

    // this utility class allows to find neighbors from block ids that are not identical to
    // the cell index in the data arrays by providing some remapping
    std::unique_ptr<utilGetNeighbor> getNb =
            std::unique_ptr<utilGetNeighbor>(new utilGetNeighbor(blockIdArr, nbArr));

    // check if blocks still have their horizontal neighbors, and if not replace
    // the neighbor id with one that is higher up in the block column
    for(vtkIdType bl = 0; bl < nbArr->GetNumberOfTuples(); ++bl)
    {
        for(vtkIdType dir = utilNormNames::DIR_XNEG; dir < utilNormNames::DIR_ZNEG; ++dir)
        {
            vtkIdType nbInx = getNb->neighborIndex(bl, dir);
            while((0 <= nbInx) && !remaining.test(nbInx))
                nbInx = getNb->neighborIndex(nbInx, utilNormNames::DIR_ZPOS);
            nbArr->SetComponent(bl, dir, nbInx);
        }
    }
}
