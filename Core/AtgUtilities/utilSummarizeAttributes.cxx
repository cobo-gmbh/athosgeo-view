/*=========================================================================

   Program: AthosGEO
   Module:  utilSummarizeAttributes.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cmath>
#include <vtkSmartPointer.h>
#include <vtkDataSetAttributes.h>
#include <vtkTable.h>
#include <vtkMergeTables.h>
#include <vtkAbstractArray.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>
#include <vtkLongLongArray.h>
#include <vtkStringArray.h>
#include <utilTrackDefs.h>
#include <utilDerivedValues.h>
#include <utilNormNames.h>
#include <utilSummarizeAttributes.h>

static const double eps = 0.001;

utilSummarizeAttributes::utilSummarizeAttributes(vtkDataSetAttributes* input)
:   input_(input),
    allowNonWeighted_(false),
    category_(nullptr),
    percNormArr_(nullptr),
    selectedOnly_(false),
    includeCementModuli_(true),
    includeCollars_(false),
    valuesOnly_(false),
    excludeTonnAtts_(false),
    alwaysProductAtt_(false),
    handleNullValues_(false),
    nullValue_(-1.)
{
}

utilSummarizeAttributes::~utilSummarizeAttributes()
{
    if(nullptr != percNormArr_)
        percNormArr_->Delete();
}

void utilSummarizeAttributes::setNormalization(std::vector<std::string> const& weights,
                                               int comp)
{
    tonnWeights_.clear();
    tonnComps_.clear();

    // try to find the weight attribute columns and remember them
    for(unsigned long w = 0; w < weights.size(); ++w)
    {
        std::string wt = weights[w];
        vtkDataArray* arr = input_->GetArray(wt.c_str());

        if(nullptr == arr)
        {
            errorMsg_ = std::string("Weight \'")
                    + std::to_string(w)
                    + "\' is not available in the data set";
            return;
        }

        tonnWeights_.push_back(arr);

        if(0 == w)
            tonnComps_.push_back(comp);
        else
            tonnComps_.push_back(0);
    }
}

void utilSummarizeAttributes::setAllowNonWeighted(bool allow)
{
    allowNonWeighted_ = allow;
}

void utilSummarizeAttributes::setNormSumTarget(std::string const& normSumTarget)
{
    normSumTarget_ = normSumTarget;
}

void utilSummarizeAttributes::setCategory(std::string const& category)
{
    category_ = input_->GetAbstractArray(category.c_str());
    if(nullptr == category_)
    {
        errorMsg_ = std::string("Category \'")
                + category
                + "\' is not available in the data set";
        return;
    }
}

void utilSummarizeAttributes::setSelectedOnly(bool selectedOnly)
{
    selectedOnly_ = selectedOnly;
}

void utilSummarizeAttributes::setSelectedIds(std::set<int> const& ids)
{
    selectedIds_.clear();
    for(auto it = ids.begin(); it != ids.end(); ++it)
        selectedIds_.insert(static_cast<vtkIdType>(*it));
}

void utilSummarizeAttributes::setIncludeCementModuli(bool incCM)
{
    includeCementModuli_ = incCM;
}

void utilSummarizeAttributes::setIncludeCollars(bool incColl)
{
    includeCollars_ = incColl;
}

void utilSummarizeAttributes::setValuesOnly(bool valuesOnly)
{
    valuesOnly_ = valuesOnly;
}

void utilSummarizeAttributes::setExcludeTonnAtts(bool excludeAtts)
{
    excludeTonnAtts_ = excludeAtts;
}

void utilSummarizeAttributes::setNullHandling(bool on, double nullVal)
{
    handleNullValues_ = on;
    nullValue_ = nullVal;
}

void utilSummarizeAttributes::setAlwaysShowProduct(bool show)
{
    alwaysProductAtt_ = show;
}

void utilSummarizeAttributes::summarize(vtkTable* output)
{
    // no tonnage weights - use KTons
    if(tonnWeights_.empty())
    {
        std::vector<std::string> tonnAttNames(std::initializer_list<std::string>({
            utilNormNames::getName(utilNormNames::KTONS),
            utilNormNames::getName(utilNormNames::TONS)
        }));
        std::string not_found;
        for(auto tnit = tonnAttNames.begin(); tnit != tonnAttNames.end(); ++tnit)
        {
            vtkDataArray* tonn = vtkDataArray::SafeDownCast(input_->GetArray(tnit->c_str()));
            if(nullptr == tonn)
            {
                not_found += (!not_found.empty() ? ", " : "") + *tnit;
            }
            else
            {
                tonnWeights_.push_back(tonn);
                break;
            }
        }
        if(tonnWeights_.empty() && !allowNonWeighted_)
        {
            errorMsg_ = std::string("None of \'")
                    + not_found
                    + "\' attributes found for weighed means calculation";
            return;
        }
        tonnComps_.clear();
        tonnComps_.push_back(0);
    }

    // collect the attributes (pointers to the columns)
    AttSet atts;
    for(vtkIdType a = 0; a < input_->GetNumberOfArrays(); ++a)
    {
        vtkDataArray* att = input_->GetArray(static_cast<int>(a));
        if(nullptr != att)
            atts.insert(att);
    }

    // sort the attributes by types
    tonnSet_.clear();
    dim3Set_.clear();
    ktonsDensSet_.clear();
    densSet_.clear();
    directSet_.clear();
    derivedSet_.clear();
    weightSet_.clear();
    dimSet_.clear();
    for(auto it = atts.begin(); it != atts.end(); ++it)
    {
        int ty = utilNormNames::getType((*it)->GetName());

        switch(ty)
        {
            case utilNormNames::TY_TONNAGE:
                tonnSet_.insert(*it);
                if(utilNormNames::KTONS == utilNormNames::getIndex((*it)->GetName()).first)
                    ktonsDensSet_.insert(*it);
                else if(utilNormNames::TONS == utilNormNames::getIndex((*it)->GetName()).first)
                    ktonsDensSet_.insert(*it);
                break;
            case utilNormNames::TY_DIMENSION3:
                dim3Set_.insert(*it); break;
            case utilNormNames::TY_SPECTONNAGE:
                densSet_.insert(*it);
                ktonsDensSet_.insert(*it); break;
            case utilNormNames::TY_DIRECT:
            case utilNormNames::TY_RATIO:
            case utilNormNames::TY_NONE:
                directSet_.insert(*it); break;
            case utilNormNames::TY_COORDINATE:
                if(includeCollars_)
                    directSet_.insert(*it);
                break;
            case utilNormNames::TY_DERIVED:
                derivedSet_.insert(*it); break;
            case utilNormNames::TY_WEIGHT:
                weightSet_.insert(*it); break;
            case utilNormNames::TY_DIMENSION:
                dimSet_.insert(*it); break;
            default:
                ; // do nothing - ignore
        }
    }

    // initialize the volume weights - 3 different strategies:
    // 1. if there is a volume column (an entry in the dim2Set), use this
    // 2. else if there is a ktons and a density column - create a column that is
    //    (ktons or tons) / density and use this
    // 3. else if we have 3 dimensional columns, i.e. block size in 3 dimensions,
    //    use all 3, to be multiplied
    // note: the component array is just a dummy to make the volume weighted calculations
    //   similar to the calculation with tonnage weights
    volArray_ = vtkSmartPointer<vtkDoubleArray>::New();
    volArray_->SetName("__VolumeWeights__");
    volArray_->SetNumberOfTuples(input_->GetNumberOfTuples());
    volArray_->Fill(1.);
    if(1 == dim3Set_.size())
    {
        volArray_->DeepCopy(*dim3Set_.begin());
    }
    else if((2 == ktonsDensSet_.size()) && !densSet_.empty())
    {
        vtkDataArray *ktonsArray = nullptr,
                     *densArray = *densSet_.begin();
        for(AttSet::iterator kit = ktonsDensSet_.begin(); kit != ktonsDensSet_.end(); ++kit)
            if((*kit) != densArray)
                ktonsArray = *kit;

        for(int n = 0; n < volArray_->GetNumberOfTuples(); ++n)
        {
            double val = 0.;
            if(0.0001 < densArray->GetVariantValue(n).ToDouble())
                val = ktonsArray->GetVariantValue(n).ToDouble() /
                      densArray->GetVariantValue(n).ToDouble();
            volArray_->SetValue(n, val);
        }
    }
    else if(3 == dimSet_.size())
    {
        for(int n = 0; n < volArray_->GetNumberOfTuples(); ++n)
        {
            double vol = 1.;
            for(auto ait = dimSet_.begin(); ait != dimSet_.end(); ++ait)
                vol *= (*ait)->GetVariantValue(n).ToDouble();
            volArray_->SetValue(n, vol);
        }
    }
    volWeights_.push_back(volArray_);

    // pit (or other additional tonnage weight attributes are in percent,
    // so we add another "attribute" to compensate for this - once for every
    // additional weight attribute
    if(1 < tonnWeights_.size())
    {
        double xw = 1.;
        for(unsigned long w = 1; w < tonnWeights_.size(); ++w)
            xw /= 100.;

        if(nullptr == percNormArr_)
            percNormArr_ = vtkDoubleArray::New();
        percNormArr_->SetNumberOfTuples(tonnWeights_[0]->GetNumberOfTuples());
        percNormArr_->SetName("__PercNorm__");
        percNormArr_->Fill(xw);

        tonnWeights_.push_back(percNormArr_);
        tonnComps_.push_back(0);
    }

    // get existing category values with at least one row
    collectCategoryValues();

    // from here we need to differentiate between operation by category value or global
    // this is for global summaries...
    if(nullptr == category_)
    {
        doSummarize(output);
    }

    // ...and this for specific values
    else
    {
        for(auto it = catValues_.begin(); it != catValues_.end(); ++it)
        {
            doSummarize(output, it->first);
        }
    }

    // clean up
    volWeights_.clear();
}

bool utilSummarizeAttributes::summarizeProducts(vtkTable* table,
                                                std::vector<std::string>& prodNames,
                                                std::string const& normAtt)
{
    for(unsigned long prod = 0; prod < prodNames.size(); ++prod)
    {
        // summarize blocks for one product
        std::string tkWt = normAtt + "_" + prodNames[prod];
        std::vector<std::string> na(1, tkWt);
        setNormalization(na);
        setNormSumTarget(normAtt);
        vtkSmartPointer<vtkTable> summTable = vtkSmartPointer<vtkTable>::New();
        summarize(summTable);
        if(!errorMsg_.empty())
            return false;

        // remember the product name
        if(alwaysProductAtt_ || (1 < prodNames.size()))
        {
            vtkSmartPointer<vtkStringArray> prodArr = vtkSmartPointer<vtkStringArray>::New();
            std::string prodName = utilNormNames::getName(utilNormNames::PRODUCT);
            prodArr->SetName(prodName.c_str());
            int numRowsPerCat = valuesOnly_ ? 1 : 3;
            prodArr->SetNumberOfTuples(numRowsPerCat);
            for(int n = 0; n < numRowsPerCat; ++n)
                prodArr->SetValue(n, prodNames[prod]);
            summTable->AddColumn(prodArr);
        }

        // either copy the product summary table to the output...
        if(0 == prod)
        {
            table->ShallowCopy(summTable);
        }

        // ...or append it to it
        else
        {
            vtkSmartPointer<vtkMergeTables> merge = vtkSmartPointer<vtkMergeTables>::New();
            merge->SetMergeColumnsByName(true);
            merge->SetInputData(table);
            merge->SetInputData(1, summTable);
            merge->Update();
            table->DeepCopy(merge->GetOutput());
        }
    }

    // remove the tonages - which are just confusing for a summarized table
    table->RemoveColumnByName(utilNormNames::getName(utilNormNames::KTONS).c_str());
    table->RemoveColumnByName(utilNormNames::getName(utilNormNames::TONS).c_str());

    return true;
}

void utilSummarizeAttributes::collectCategoryValues()
{
    // initialize
    catValues_.clear();
    if(nullptr == category_)
        return;

    for(vtkIdType row = 0; row < category_->GetNumberOfTuples(); ++row)
    {
        // do not include non-selected rows if "selected only" is chosen
        if(selectedOnly_ && (selectedIds_.end() == selectedIds_.find(row)))
            continue;

        // index value, string or rounded numeric value (vtkIdType)
        vtkVariant value = getCategoryValue(row);

        // this takes care of removing duplicates, so we end up with an ordered
        // list of the existing values
        // at the same time we are counting the number of occurrences of any category value
        // note: at this moment we do nothing with this count information...
        auto it = catValues_.find(value);
        if(catValues_.end() == it)
            catValues_[value] = 1;
        else
            catValues_[value] = catValues_[value] + 1;
    }
}

vtkVariant utilSummarizeAttributes::getCategoryValue(vtkIdType row)
{
    vtkVariant value = category_->GetVariantValue(row);
    if(!value.IsNumeric())
        return value;

    double numVal = value.ToDouble();
    return vtkVariant(static_cast<vtkIdType>(::floor(numVal + .5)));
}

void utilSummarizeAttributes::doSummarize(vtkTable* output, vtkVariant cat)
{
    // total values for the tonnage and volume normalization - if possible!
    // note: as an alternative for the volume weighted average we will go
    // for a plain average, but before that we will try either an explicit
    // volume attribute or the product of 3 size (dimension) attributes
    double tonnWeightTotal = 0.,
           volWeightTotal = 0.;

    // initialize the input filter bitset - for avoiding to iterate through
    // all the rows if only a few are really of interest
    usedRowsFilt_.resize(static_cast<unsigned long>(input_->GetNumberOfTuples()), false);
    usedRowsFilt_.reset();

    // summarize weight products, and at the same time count the
    // number of items where the tonnage weights are not 0
    int numberOfRows = 0;
    for(vtkIdType row = 0; row < input_->GetNumberOfTuples(); ++row)
    {
        // if we respect a selection, ignore all others
        if(selectedOnly_ && (selectedIds_.end() == selectedIds_.find(static_cast<vtkIdType>(row))))
            continue;

        // if we summarize one category only, ignore all others
        if(nullptr != category_)
        {
            vtkVariant value = getCategoryValue(row);
            if(value != cat)
                continue;
        }

        // product of all the weight attributes
        // note that the first refers to a product already if this option was chosen
        double wt = 1.;
        for(unsigned long w = 0; w < tonnWeights_.size(); ++w)
            wt *= tonnWeights_[w]->GetComponent(row, static_cast<int>(tonnComps_[w]));
        tonnWeightTotal += wt;

        // note that with this we do not consider blocks with no tonnage
        // weight for the calculation of total volume - and we need to do the
        // same also when summarizing volume weighted
        // note: by introducing and using the tonnRowFilt bitset the latter will
        //   happen automatically if we set the bits here!
        if(eps < wt)
        {
            ++numberOfRows;
            usedRowsFilt_.set(static_cast<unsigned long>(row), true);
            volWeightTotal += volArray_->GetValue(row);
        }
    }

    // number of rows to add per category
    int numRowsPerCat = valuesOnly_ ? 1 : 3;
    vtkIdType newNumRows = output->GetNumberOfRows() + numRowsPerCat;

    // if we have already columns, extend the table by numRowsPerCat new rows
    if(0 < output->GetNumberOfColumns())
    {
        for(int n = 0; n < numRowsPerCat; ++n)
            output->InsertNextBlankRow();
    }

    // make sure that we have a target attribute for the total tonnage weight
    if(normSumTarget_.empty() && !allowNonWeighted_)
        normSumTarget_ = tonnWeights_[0]->GetName();

    if(!normSumTarget_.empty())
    {
        vtkDataArray* tonnWtArr =
                vtkDataArray::SafeDownCast(output->GetColumnByName(normSumTarget_.c_str()));
        if(nullptr == tonnWtArr)
        {
            vtkSmartPointer<vtkDoubleArray> tonnWtArr2 = vtkSmartPointer<vtkDoubleArray>::New();
            tonnWtArr2->SetName(normSumTarget_.c_str());
            tonnWtArr2->SetNumberOfTuples(newNumRows);
            output->AddColumn(tonnWtArr2);
            tonnWtArr = vtkDataArray::SafeDownCast(output->GetColumnByName(normSumTarget_.c_str()));
        }
        vtkIdType rr = output->GetNumberOfRows() - numRowsPerCat;
        for(int n = 0; n < numRowsPerCat; ++n)
            tonnWtArr->SetVariantValue(rr++, tonnWeightTotal);
    }

    // if summarizing by category, first add the category column
    if(nullptr != category_)
    {
        // note: with this the correct data type will be generated, and the additional instances
        //   deleted automatically through the smart pointer
        vtkAbstractArray* catArr = output->GetColumnByName(category_->GetName());

        // if we do not have such a column yet, we generate it
        if(nullptr == catArr)
        {
            // if numeric we can go for integer numbers
            if(cat.IsNumeric())
            {
                vtkSmartPointer<vtkLongLongArray> numArr = vtkSmartPointer<vtkLongLongArray>::New();
                numArr->SetName(category_->GetName());
                numArr->SetNumberOfTuples(newNumRows);
                output->AddColumn(numArr);
                catArr = output->GetColumnByName(category_->GetName());
            }

            // otherwise we go for a string array
            else
            {
                vtkSmartPointer<vtkStringArray> strArr = vtkSmartPointer<vtkStringArray>::New();
                strArr->SetName(category_->GetName());
                strArr->SetNumberOfTuples(newNumRows);
                output->AddColumn(strArr);
                catArr = output->GetColumnByName(category_->GetName());
            }
        }

        // fill in the category values
        vtkIdType rr = output->GetNumberOfRows() - numRowsPerCat;
        for(int n = 0; n < numRowsPerCat; ++n)
            catArr->SetVariantValue(rr++, cat);
    }

    // insert the row type indicator string
    if(!valuesOnly_)
    {
        std::string typeName = utilNormNames::getName(utilNormNames::ROWTYPE);
        vtkStringArray* typeArr = vtkStringArray::SafeDownCast(output->GetColumnByName(typeName.c_str()));
        if(nullptr == typeArr)
        {
            vtkSmartPointer<vtkStringArray> typeArr2 = vtkSmartPointer<vtkStringArray>::New();
            typeArr2->SetName(typeName.c_str());
            typeArr2->SetNumberOfTuples(newNumRows);
            output->AddColumn(typeArr2);
            typeArr = vtkStringArray::SafeDownCast(output->GetColumnByName(typeName.c_str()));
        }
        vtkIdType rr = output->GetNumberOfRows() - numRowsPerCat;
        static char const* valTypeNames[] = { "Value", "Min", "Max" };
        for(int n = 0; n < numRowsPerCat; ++n)
            typeArr->SetValue(rr++, valTypeNames[n]);
    }

    // insert the item count column
    std::string numName = utilNormNames::getName(utilNormNames::NUMITEMS);
    vtkDataArray* numItemArr = vtkDataArray::SafeDownCast(output->GetColumnByName(numName.c_str()));
    if(nullptr == numItemArr)
    {
        vtkSmartPointer<vtkLongLongArray> numItemArr2 = vtkSmartPointer<vtkLongLongArray>::New();
        numItemArr2->SetName(numName.c_str());
        numItemArr2->SetNumberOfTuples(newNumRows);
        output->AddColumn(numItemArr2);
        numItemArr = vtkDataArray::SafeDownCast(output->GetColumnByName(numName.c_str()));
    }
    vtkIdType rr = output->GetNumberOfRows() - numRowsPerCat;
    for(int n = 0; n < numRowsPerCat; ++n)
        output->SetValueByName(rr++, numName.c_str(), numberOfRows);

    // we need an additional tonnage weight array that does not include the main
    // normalizing tonnage, but only additional weights (Pit, Zone...) which we want
    // to apply to the tonnages
    std::vector<vtkDataArray*> weightsForTonnages = tonnWeights_;
    std::vector<vtkIdType> weightCompsForTonnages = tonnComps_;
    for(unsigned long long w = 0; w < weightsForTonnages.size(); ++w)
    {
        vtkDataArray* arr = weightsForTonnages[w];
        if(utilNormNames::TY_TONNAGE == utilNormNames::getType(arr->GetName()))
        {
            auto wit = weightsForTonnages.begin();
            std::advance(wit, w);
            weightsForTonnages.erase(wit);
            auto cit = weightCompsForTonnages.begin();
            std::advance(cit, w);
            weightCompsForTonnages.erase(cit);
        }
    }

    // volume weight components are all 0 (because all volume attributes have 1 comp only)
    std::vector<vtkIdType> volComps(volWeights_.size(), 0);

    // if we have attributes in the tonnWeights_ that are in the weightSet_, we do
    // not want to summarize the weightSet_ attributes because we cannot calculate
    // valid summaries
    // explanation:
    // - assume that weight set attribute Pit_A is in the tonnage
    // - the summary value for Pit_A should always be 100% because we are extracting only
    //   material that is from Pit_A
    // - the Pit_A weighted summary for Pit_B is hard to calculate for a block where both
    //   Pit_A and Pit_B are less than 100%: Pit_A actually cuts a part from that block,
    //   like also Pit_B cuts a part off. However, the thing that we do NOT know is: are
    //   the parts that Pit_A and Pit_B are cutting off are the same, or partly the same,
    //   or not at all connected
    // conclusion: for these reasons, we simply ignore weightSet_ attributes in that case!
    bool usesWeightSetAttribute = false;
    for(auto it = tonnWeights_.begin(); !usesWeightSetAttribute && (it != tonnWeights_.end()); ++it)
        usesWeightSetAttribute = weightSet_.end() != weightSet_.find(*it);

    // additional weights: volume weighted mean, including also blocks/rows that are not
    // in the numItems set
    if(!usesWeightSetAttribute)
    {
        for(AttSet::iterator it = weightSet_.begin(); it != weightSet_.end(); ++it)
            if(!addWeightedMean(output, *it, tonnWeights_, tonnComps_, tonnWeightTotal, cat))
                return;
    }

    // for densities, we use also volume weights, but do not include blocks/rows that are
    // not part of the numItems set
    for(AttSet::iterator it = densSet_.begin(); it != densSet_.end(); ++it)
        if(!addWeightedMean(output, *it, volWeights_, volComps, volWeightTotal, cat))
            return;

    // for tonnages we want to calculate the sum, not a weighted average, so we use 1. for
    // the total
    // note: if additional weights (pit, zone...) are applied, we want to apply them also to
    //   all the tonnages - and we do it for all rows/blocks, no matter if they are in the
    //   numItems set or not
    if(!excludeTonnAtts_)
    {
        for(AttSet::iterator it = tonnSet_.begin(); it != tonnSet_.end(); ++it)
            if(!addWeightedMean(output, *it, weightsForTonnages, weightCompsForTonnages, 1., cat))
                return;
    }

    // these are the regular "direct" attributes: chemical, others...
    for(AttSet::iterator it = directSet_.begin(); it != directSet_.end(); ++it)
        if(!addWeightedMean(output, *it, tonnWeights_, tonnComps_, tonnWeightTotal, cat))
            return;

    // calculated columns
    if(includeCementModuli_)
    {
        utilDerivedValues cdv(output->GetRowData());
        vtkIdType rr = output->GetNumberOfRows() - numRowsPerCat;
        for(int n = utilNormNames::getDerivedIndex(true); n < utilNormNames::getDerivedIndex(false); ++n)
        {
            std::string dn = utilNormNames::getName(n);
            if(!cdv.derivedExists(dn))
                continue;

            // see if we already have a column for the derived value in the input
            // - and otherwise generate one
            vtkDataArray* darr = input_->GetArray(dn.c_str());
            vtkSmartPointer<vtkDataArray> darrPtr;
            if(nullptr == darr)
            {
                utilDerivedValues cdvIn(input_);
                if(cdvIn.derivedExists(dn))
                {
                    // this smart pointer is used to avoid a memory leak because we actually
                    // do not attach the array to anything here
                    darrPtr.TakeReference(vtkDoubleArray::New());
                    darr = darrPtr.Get();
                    darr->SetName(dn.c_str());
                    darr->SetNumberOfTuples(input_->GetNumberOfTuples());
                    for(vtkIdType r = 0; r < input_->GetNumberOfTuples(); ++r)
                    {
                        double dval = cdvIn.calculate(dn, r);
                        darr->SetVariantValue(r, dval);
                    }
                }
            }

            // if we have a derived attribute already, get min and max,
            // or otherwise set them both to 0
            if(nullptr != darr)
            {
                // note: this is only for the min/max rows - if they exist or could be generated
                // note: the "weight" has the effect of excluding values where the weight is 0
                if(!addWeightedMean(output, darr, tonnWeights_, tonnComps_, 1., cat))
                    return;
            }
            else
            {
                std::vector<double> zero(std::initializer_list<double>({0., 0., 0.}));
                addColumn(output, dn, zero);
            }

            // re-calculate value row
            double val = cdv.calculate(dn, rr);
            output->SetValueByName(rr, dn.c_str(), val);
        }
    }
}

bool utilSummarizeAttributes::addWeightedMean(vtkTable* output, vtkDataArray* arr,
                                              std::vector<vtkDataArray*>& weights,
                                              std::vector<vtkIdType>& weightComps,
                                              double weightTotal, vtkVariant cat)
{
    // collect values, depending on valuesOnly_ flag and components
    std::vector<double> val;

    // initialize with first value for all components
    unsigned long numComps = static_cast<unsigned long>(arr->GetNumberOfComponents());
    std::vector<double> sum(numComps, 0.),
                        minVal(numComps, 0.),
                        maxVal(numComps, 0.);

    // go through all input table rows - with the bitset as filter for performance
    bool first = true,
         nullFound = false;
    for(boost::dynamic_bitset<>::size_type row = usedRowsFilt_.find_first();
        row != usedRowsFilt_.npos;
        row = usedRowsFilt_.find_next(row))
    {
        // check if we are in the correct category
        if(nullptr != category_)
        {
            vtkVariant category = getCategoryValue(static_cast<vtkIdType>(row));
            if(category != cat)
                continue;
        }

        // calculate the weight and skip the further calculation if it is too little
        // note: this "saves" even a NULL value - and it makes sense because with a weight
        //   of 0, any possible other value will be 0 as well!
        double wt = 1.;
        for(unsigned long w = 0; w < weights.size(); ++w)
            wt *= weights[w]->GetComponent(static_cast<vtkIdType>(row), static_cast<int>(weightComps[w]));
        if(eps > wt)
            continue;

        // handle the components - if any
        for(unsigned long comp = 0; comp < numComps; ++comp)
        {
            double value = arr->GetComponent(static_cast<vtkIdType>(row), static_cast<int>(comp));

            // NULL handling
            // note: if this is a multi-component array, we will set ALL components to NULL
            //   if we find even ANY NULL value in the input! Normally this will not have an
            //   effect because normally we only expect coordinates to be multi-component, and
            //   these should not be NULL anyway
            // note: now we allowed multi-component "direct" attributes, but we keep that rule,
            //   assuming that it makes no sense to keep only single components if others are
            //   not fully valid
            if(handleNullValues_ && (eps > ::fabs(value - nullValue_)))
            {
                nullFound = true;
                break;
            }

            sum[comp] += wt * value;

            if(first)
            {
                minVal[comp] = maxVal[comp] = value;
                first = false;
            }
            else
            {
                minVal[comp] = std::min<double>(minVal[comp], value);
                maxVal[comp] = std::max<double>(maxVal[comp], value);
            }
        }
    }

    if(nullFound)
    {
        val = std::vector<double>(numComps * 3, nullValue_);
    }

    else
    {
        for(unsigned long comp = 0; comp < numComps; ++comp)
        {
            val.push_back(sum[comp] / weightTotal);
            if(!valuesOnly_)
            {
                val.push_back(minVal[comp]);
                val.push_back(maxVal[comp]);
            }
        }
    }

    // note: only in the case of coordinates it is possible that we have more
    //   than one component so far
    addColumn(output, arr->GetName(), val, arr->GetNumberOfComponents());

    return true;
}

void utilSummarizeAttributes::addColumn(vtkTable* output, std::string const& name,
                                        std::vector<double> const& value, int numComponents)
{
    vtkIdType rowBase = output->GetNumberOfRows();
    vtkDataArray* arr = nullptr;

    int numRowsPerCat = valuesOnly_ ? 1 : 3;

    // if the table has so far only numRowsPerCat rows, we need to generate a new column
    // with that name, replacing any other that might already be there
    if(numRowsPerCat == rowBase)
    {
        vtkSmartPointer<vtkDoubleArray> newArr = vtkSmartPointer<vtkDoubleArray>::New();
        newArr->SetName(name.c_str());
        newArr->SetNumberOfComponents(numComponents);
        newArr->SetNumberOfTuples(rowBase);
        output->AddColumn(newArr);
    }

    // now we can expect that there is already an array of the
    // required type (which is double)
    arr = vtkDataArray::SafeDownCast(output->GetColumnByName(name.c_str()));

    // fill the last numRowsPerCat rows with the numRowsPerCat entry values (value, min, max)
    // note: in case of valuesOnly_ the min and max values are simply ignored
    rowBase -= numRowsPerCat;
    unsigned long valPtr = 0;
    for(int comp = 0; comp < numComponents; ++comp)
    {
        for(int row = 0; row < numRowsPerCat; ++row)
        {
            double val = std::isnan(value[valPtr]) ? 0. : value[valPtr];
            ++valPtr;

            arr->SetComponent(rowBase + row, comp, val);
        }
    }
}
