/*=========================================================================

   Program: AthosGEO
   Module:  utilNormNames.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef utilNormNames_h
#define utilNormNames_h

#include <vector>
#include <string>
#include <vtkSystemIncludes.h>
#include <AtgUtilitiesModule.h>

class ATGUTILITIES_EXPORT utilNormNames
{
public:

    // types of attributes
    typedef enum
    {
        // block indices and categories
        BLOCKID,        // unique ID for blocks in block model, negative for correctives
        MATERIAL,       // names of corrective materials
        ROWTYPE,        // row type in summary table (min, max, mean...)
        CODE,           // some code for indicating whatever
        GEOLOGY,        // some indication of geological unit (up to user)
        CLASS,          // some material classification
        PRODUCT,        // some indication of product (up to user)
        NUMITEMS,       // "number of items" attribute in summary tables
        DATE,           // a date in the form YYYY-MM-DD
        CELLCOLOR,      // 3 component array with rgb values from 0 to 1 (can also be point color)
        CELLALPHA,      // array with alpha values from 0 to 1 (for info only, not displayed)
        LEVEL,          // some indication of level - can be used for block merging
        COLUMN,         // additional categories
        ROW,            //   "
        LINE,           //   "
        I,              //   "
        J,              //   "
        K,              //   "
        CAT,            // category of the format Cat_<something> - user defined
        NAME,           // category of string "names"
        NAMEX,          // category of strings with the form N_<something> - user defined
        START,          // start period - 0 meaning "not a start block"
        FORCE,          // forced block with period - 0 meaning "block not forced"
        // block geometry
        BLOCKCENTER,    // with 3 SubInxCoord components
        BLOCKSIZE,      // with 3 SubInxCoord components
        ANGLE,
        VOLUME,
        BLOCKNEIGHBOR,  // with 6 SubInxDir components
        // drillhole related
        COLLAR,         // collar coordinates
        DEPTH,          // sample depth along drillhole, from..to
        MAXDEPTH,       // maximum depth of a drillhole
        SURVEYAT,       // starting depth of a survey data set
        HOLEAZIMUTH,    // azimuth of drilling direction
        HOLEDIP,        // dip of drilling direction
        HOLEID,         // ID of the drillhole (string)
        SAMPID,         // ID of single samples
        HOLETYPE,       // drillhole, excavator, ...
        HOLEPATH,       // linear, ...
        LABELS,         // standard attribute for labelled point clouds
        // planning related
        BLASTID,        // ID of a blast
        PLANID,         // ID of a product (generated during planning)
        CONMIN,         // minimum tonnage (from blast)
        CONMAX,         // maximum tonnage (from blast)
        TONSINITIAL,    // initial tonnage of a blast, before removing tonnage for plannings
        // mining related
        MININGSLOPE,    // with 4 SubInxDir components
        PREFERENCE,     // with 4 SubInxDir components
        KTONS,
        TONS,
        TONSPROD,       // this makes sense if some tonnages of products are in the blocks
        OFFEREDTOTAL,   // total offered
        OFFEREDTOTPERC, // dto, percentage of block
        OFFEREDPROD,    // offered for a product (Offered_<name>)
        OFFEREDPRDPERC, // dto, percentage of block
        TAKENTOTAL,     // taken for all products (Taken_Total)
        TAKENTOTPERC,   // dto, percentage of block
        TAKENPROD,      // taken for specific product (Taken_<name>)
        TAKENPRDPERC,   // dto, percentage of block
        WASTED4BLOCK,   // wasted tonnage to unbury a block
        WASTED,         // wasted tonnage
        WASTEDPERC,     // dto, percentage of block
        MININGPERIOD,   // mining period in the output tables
        TAKENPERIOD,    // period in which the first ton was taken
        WASTEDPERIOD,   // period in which the remaining tonnage was wasted
        WASTERATIO,     // percent waste per taken material - or 0 if nothing taken
        PIT,            // a pit attribute - more than one possible (Pit_<Name> or Pit)
        ZONE,           // some indication of "zone" (up to user)
        DENSITY,
        VOLFACTOR,      // volume factor or filling degree of block
        // calculated attributes (cement related)
        CALCULATED,     // mark the beginning of calculated attributes
        LS,
        SR,
        AR,
        ASR,
        NAEQ,
        // measured attributes
        MEASURED,      // mark the beginning of variable attributes
        LOI,
        SIO2,
        AL2O3,
        FE2O3,
        CAO,
        MGO,
        SO3,
        K2O,
        NA2O,
        TIO2,
        MN2O3,
        P2O5,
        CR2O3,
        CL,
        F,
        HG,
        TOC,
        // temporary attributes, for internal purposes
        TEMP,
        // last - end of known attributes
        ENDATTS
    }
    Index;

    // sub indices for coordinates
    typedef enum
    {
        COORD_X,
        COORD_Y,
        COORD_Z,
        COORD_MAG
    }
    SubInxCoord;

    typedef enum
    {
        HOLE_FROM,
        HOLE_TO,
        HOLE_MAG
    }
    HoleRange;

    // sub indices for directions
    typedef enum
    {
        DIR_XNEG,
        DIR_XPOS,
        DIR_YNEG,
        DIR_YPOS,
        DIR_ZNEG,
        DIR_ZPOS,
        DIR_MAG
    }
    SubInxDir;

    // note: if updating, also update typeDesc in utilNormNames.cxx!
    typedef enum
    {
        TY_NONE,
        TY_NAME,
        TY_DATE,
        TY_INDEX,
        TY_CATEGORY,
        TY_COORDINATE,
        TY_ANGLE,
        TY_DIMENSION,
        TY_DIMENSION3,
        TY_RANGE,
        TY_TONNAGE,
        TY_RATIO,
        TY_PERIOD,
        TY_WEIGHT, // additional weight factor, like a pit variable
        TY_SPECTONNAGE,
        TY_DERIVED,
        TY_DIRECT,
        TY_TEMPORARY,
        TY_IGNORE
    }
    Type;

    // expected data type
    typedef enum
    {
        DTY_FLOAT,      // double precision floating point
        DTY_INTEGER,    // long long integer
        DTY_STRING,     // character string
        DTY_ANY
    }
    DataType;

    enum
    {
        INDEX_NO_BLOCK = -10000
    };

    // find the main name from the index value
    static std::string getName(int index);

    // find the full name, including component name if available
    static std::string getFullName(int index, int component);

    // find the index from the base name (or [IGNORE,0] if not available)
    static std::pair<int, int> getIndex(std::string const& baseName, int compIndex = 0);

    // get the type of a variable (or NONE if not available)
    static int getType(std::string const& baseName);

    // get the type description of a variable
    static std::string getTypeDesc(std::string const& baseName);

    // get the expected data type from a (non-normalized) column name
    static int getDataType(std::string const& baseName);

    // get the index of the first / last calculated value
    // note: if first is true, get the index of the first derived attribute,
    // and if it is false, get the first index beyond the last derived attribute
    static int getDerivedIndex(bool first);

    // get the index of the first / last measured value
    // note: if first is true, get the index of the first measured attribute,
    // and if it is false, get the first index beyond the last measured attribute
    static int getMeasuredIndex(bool first);

    // get the list of component names for a column
    static std::vector<std::string> getComponents(std::string const& baseName);

    // normalize a name, by using the regular expressions
    // note: if includeComponent is true, a full name including the component part
    // will be generated if available
    static std::string normalize(std::string const& name, bool includeComponent);

private:

    // these functions are not defined and should not be used
    utilNormNames();
    utilNormNames(utilNormNames const&);
    void operator=(utilNormNames const&);

    // helper function
    static int inxInCols(int index);

};

#endif
