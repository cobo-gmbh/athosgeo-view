/*=========================================================================

   Program: AthosGEO
   Module:  utilVector2D.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <utility>
#include <boost/functional/hash.hpp>
#include <utilVector2D.h>

static const double eps = .001,
                    infinity = 1.e9,
                    maxDist = ::sqrt(.75);

utilVector2D::utilVector2D(double x, double y)
:   x_(x),
    y_(y)
{
}

void utilVector2D::getBoundaryBox(utilVector2D const& pt2, double* bbox)
{
    bbox[0] = bbox[1] = x_;
    bbox[2] = bbox[3] = y_;
    if(pt2.x_ < x_)
        bbox[0] = pt2.x_;
    if(pt2.x_ > x_)
        bbox[1] = pt2.x_;
    if(pt2.y_ < y_)
        bbox[2] = pt2.y_;
    if(pt2.y_ > y_)
        bbox[3] = pt2.y_;
}

bool utilVector2D::linesIntersect(utilVector2D const& p11, utilVector2D const& p12,
                                  utilVector2D const& p21, utilVector2D const& p22,
                                  double& outDistLine1, double& outDistLine2)
{
    outDistLine1 = infinity;
    outDistLine2 = infinity;

    double denominator = ((p22.y_ - p21.y_) * (p12.x_ - p11.x_)) -
                         ((p22.x_ - p21.x_) * (p12.y_ - p11.y_));

    // cancel if parallel or collinear
    if(isZero(denominator))
        return false;

    double numerator1 = ((p22.x_ - p21.x_) * (p11.y_ - p21.y_)) - ((p22.y_ - p21.y_) * (p11.x_ - p21.x_)),
           numerator2 = ((p12.x_ - p11.x_) * (p11.y_ - p21.y_)) - ((p12.y_ - p11.y_) * (p11.x_ - p21.x_));

    outDistLine1 = numerator1 / denominator;
    outDistLine2 = numerator2 / denominator;

    return true;
}

bool utilVector2D::lineSegmentsIntersect(utilVector2D const& p11, utilVector2D const& p12,
                                         utilVector2D const& p21, utilVector2D const& p22)
{
    double dist1, dist2;

    if(linesIntersect(p11, p12, p21, p22, dist1, dist2))
    {
        return isLess(dist1, 1.) && isLess(dist2, 1.) && isGreater(dist1, 0.) && isGreater(dist2, 0.);
    }

    return false;
}

utilVector2D& utilVector2D::normalize()
{
    double len = ::sqrt(x_ * x_ + y_ * y_);
    if(::fabs(len) > eps)
    {
        x_ /= len;
        y_ /= len;
    }

    return *this;
}

double utilVector2D::azimuth(utilVector2D const& pt) const
{
    utilVector2D vec(pt.x_ - x_, pt.y_ - y_);
    vec.normalize();
    return ::atan2(vec.y_, vec.x_);
}

bool utilVector2D::operator<(utilVector2D const& pt2) const
{
    boost::hash<std::pair<double, double>> coord_hash;
    return coord_hash(std::pair<double, double>(x_, y_)) < coord_hash(std::pair<double, double>(pt2.x_, pt2.y_));
}

bool utilVector2D::isLessAngle(utilVector2D const& pt1, utilVector2D const& pt2) const
{
    return azimuth(pt1) < azimuth(pt2);

    /*
    utilVector2D vec1(pt1.x_ - x_, pt1.y_ - y_),
                 vec2(pt2.x_ - x_, pt2.y_ - y_);
    vec1.normalize();
    vec2.normalize();

    return vec1.octantDist() < vec2.octantDist();
    */
}

bool utilVector2D::are3PointsColinear(utilVector2D const& pt2, utilVector2D const& pt3) const
{
    return ::fabs((pt2.x_ - x_) * (pt3.y_ - y_) - (pt3.x_ - x_) * (pt2.y_ - y_)) < eps;
}

bool utilVector2D::isZero(double f)
{
    return ::fabs(f) <= eps;
}

double utilVector2D::octantDist()
{
    if(y_ >= 0.)
    {
        if(x_ >= 0.)
        {
            if(x_ >= y_)
                return y_;
            else
                return 1. + maxDist - x_;
        }
        else
        {
            if(y_ >= -x_)
                return 2. - x_;
            else
                return 3. + maxDist - y_;
        }
    }
    else
    {
        if(x_ < 0.)
        {
            if(-x_ >= -y_)
                return 4. - y_;
            else
                return 5. + maxDist + x_;
        }
        else
        {
            if(-y_ >= x_)
                return 6. + x_;
            else
                return 7. + maxDist + y_;
        }
    }
}
