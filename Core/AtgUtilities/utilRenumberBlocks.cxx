/*=========================================================================

   Program: AthosGEO
   Module:  utilRenumberBlocks.cxx

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vector>
#include <utilNormNames.h>
#include <utilTrackDefs.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkLongLongArray.h>
#include <vtkDataArray.h>
#include <utilRenumberBlocks.h>

utilRenumberBlocks::utilRenumberBlocks(vtkUnstructuredGrid* grid)
:   grid_(grid)
{
}

utilRenumberBlocks::~utilRenumberBlocks()
{
}

void utilRenumberBlocks::renumberBlocksAndNeighbors()
{
    TRACK_FUNCTION_TIME

    // get the block id and neighbor arrays
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID),
                nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
    vtkDataArray *oldBidArr = grid_->GetCellData()->GetArray(bidName.c_str()),
                 *nbArr = grid_->GetCellData()->GetArray(nbName.c_str());

    // if we do not have an old block id array, we simply generate a new one
    if(nullptr == oldBidArr)
    {
        // note: would be very strange if we had neighbor ids, but no blick ids...
        if(nullptr != nbArr)
            grid_->GetCellData()->RemoveArray(nbName.c_str());

        generateNewBlockIds();

        TRACK_ELAPSED("new block ids generated");

        return;
    }

    // if we have no neighbor array, we simply renumber the block ids
    if(nullptr == nbArr)
    {
        renumberBlocks();

        TRACK_ELAPSED("block ids renumbered, no nbs available");

        return;
    }

    // no matter if we already had a block id array, we generate a new one
    // note: if we had an old one, we still also need it for the neighbor array update
    vtkSmartPointer<vtkLongLongArray> bidArr = vtkSmartPointer<vtkLongLongArray>::New();
    bidArr->SetName(bidName.c_str());
    bidArr->SetNumberOfTuples(grid_->GetNumberOfCells());
    for(long long bid = 0; bid < bidArr->GetNumberOfTuples(); ++bid)
        bidArr->SetValue(bid, bid);

    // find maximum block id in the nb array
    // note: we have to go through all the components explicitly because
    //   if we make it -1, we will get min/max of the "magnitude" only
    double range[2],
           maxId = 0;
    for(int c = 0; c < nbArr->GetNumberOfComponents(); ++c)
    {
        nbArr->GetRange(range, c);
        maxId = static_cast<unsigned long long>(std::max<double>(maxId, range[1]));
    }

    // if we have a neighbor array we update it
    // note: this normally implies that we also have an old block id array
    if((nullptr != nbArr) && (nullptr != oldBidArr))
    {
        // block id translation table from old to new
        std::vector<vtkIdType> newIds(static_cast<unsigned long long>(maxId) + 1, utilNormNames::INDEX_NO_BLOCK);
        for(vtkIdType bid = 0; bid < grid_->GetNumberOfCells(); ++bid)
        {
            vtkIdType oldBid = oldBidArr->GetVariantValue(bid).ToLongLong();
            if(0 <= oldBid)
                newIds[static_cast<unsigned long>(oldBid)] = bid;
        }

        // translate the block ids in the neighbor attribute
        for(vtkIdType b = 0; b < grid_->GetNumberOfCells(); ++b)
        {
            for(vtkIdType c = 0; c < 6; ++c)
            {
                vtkIdType bid = static_cast<long long>(nbArr->GetComponent(b, static_cast<int>(c)));
                if(0 <= bid)
                    nbArr->SetComponent(b, static_cast<int>(c), newIds[static_cast<unsigned long>(bid)]);
            }
        }
    }

    // add or replace block id array
    grid_->GetCellData()->AddArray(bidArr);

    TRACK_ELAPSED("block ids and nbs renumbered");

    // with this, ranges will be recalculated if required
    bidArr->Modified();
    nbArr->Modified();
}

void utilRenumberBlocks::generateNewBlockIds()
{
    // generate a block id array
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID);
    auto bidArr = vtkSmartPointer<vtkIdTypeArray>::New();
    bidArr->SetName(bidName.c_str());
    bidArr->SetNumberOfTuples(grid_->GetCellData()->GetNumberOfTuples());
    grid_->GetCellData()->AddArray(bidArr);

    for(vtkIdType bid = 0; bid < bidArr->GetNumberOfTuples(); ++bid)
        bidArr->SetValue(bid, bid);
}

void utilRenumberBlocks::renumberBlocks()
{
    // get existing block id array
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID);
    vtkDataArray* bidArr = grid_->GetCellData()->GetArray(bidName.c_str());
    if(nullptr == bidArr)
        return;

    for(vtkIdType bid = 0; bid < bidArr->GetNumberOfTuples(); ++bid)
        bidArr->SetVariantValue(bid, vtkVariant(bid));
}
