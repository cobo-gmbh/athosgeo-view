/*=========================================================================

   Program: AthosGEO
   Module:  utilCheckNeighborAttribute.cxx

   Copyright (c) 2022 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vector>
#include <set>
#include <utilNormNames.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>
#include <utilCheckNeighborAttribute.h>

utilCheckNeighborAttribute::utilCheckNeighborAttribute(vtkUnstructuredGrid* grid)
:   grid_(grid)
{
}

utilCheckNeighborAttribute::~utilCheckNeighborAttribute()
{
}

bool utilCheckNeighborAttribute::neighborBlockIdsValid()
{
    error_ = "";

    // get the block id and neighbor arrays
    std::string bidName = utilNormNames::getName(utilNormNames::BLOCKID),
                nbName = utilNormNames::getName(utilNormNames::BLOCKNEIGHBOR);
    vtkDataArray *bidArr = grid_->GetCellData()->GetArray(bidName.c_str()),
                 *nbArr = grid_->GetCellData()->GetArray(nbName.c_str());

    // we need to have the two attributes BlockID and Nb
    if((nullptr == bidArr) || (nullptr == nbArr))
    {
        std::string msg = "Not all required attributes available:";
        if(nullptr == bidArr)
            msg += " " + bidName;
        if(nullptr == nbArr)
            msg += " " + nbName;
        error_ = msg + " missing";
        return false;
    }

    // collect existing block id values
    std::set<long long> bids;
    for(vtkIdType id = 0; id < bidArr->GetNumberOfValues(); ++id)
        bids.insert(bidArr->GetVariantValue(id).ToLongLong());

    // check Nb attributes for invalid block ids
    for(vtkIdType id = 0; id < nbArr->GetNumberOfValues(); ++id)
    {
        long long bid = nbArr->GetVariantValue(id).ToLongLong();
        if(bid < 0)
            continue;
        if(bids.end() == bids.find(bid))
        {
            error_ = "Invalid neighbor id(s) found: Recalculation of Nb attribute required";
            return false;
        }
    }

    return true;
}

std::string utilCheckNeighborAttribute::lastError() const
{
    return error_;
}
