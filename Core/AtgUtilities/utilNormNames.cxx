/*=========================================================================

   Program: AthosGEO
   Module:  utilNormNames.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <string>
#include <boost/regex.hpp>
#include <utilNormNames.h>

// structure that brings together column indices, names, types etc.
struct ColNames
{
    inline ColNames(std::pair<int, int>& index, int type, int dataType,
                    std::string const& normBase, std::string const& normComponent,
                    boost::regex const& recognized)
    :   index_(index),
        type_(type),
        dataType_(dataType),
        normBase_(normBase),
        normComponent_(normComponent),
        recognized_(recognized)
    {}

    inline ColNames(int mainInx, int subInx, int type, int dataType,
                    std::string const& normBase, std::string const& normComponent,
                    boost::regex const& recognized)
    :   index_(mainInx, subInx),
        type_(type),
        dataType_(dataType),
        normBase_(normBase),
        normComponent_(normComponent),
        recognized_(recognized)
    {}

    std::pair<int, int> index_;
    int type_,
        dataType_;
    std::string normBase_,
                normComponent_;
    boost::regex recognized_;
};

static const std::string typeDesc[] =
{
    "None",
    "Name",
    "Date",
    "Index",
    "Category",
    "Coordinate",
    "Angle",
    "Dimension",
    "Dimension3",
    "Range",
    "Tonnage",
    "Ratio",
    "Period",
    "Weight",
    "SpecTonnage",
    "Derived",
    "Direct",
    "Temporary",
    "Ignore"
};

static const std::string dataTypeDesc[] =
{
    "double",
    "long_long",
    "string"
};

// - the regex is the key for names input, to be converted into the normalized form (standard name)
// - a component name can be optionally appended with a : if available
// - the standard name is also the key for the code and the category of an attribute
// - there may be an extension part in the regex, like "pit(.*)" which will be appended to the normalized name
//   (note that this does not change the code and the category of the attribute, so if first a code is found
//   from a name with extension, and then the code should be converted back to a name, the extension is lost)
static const int fl = boost::regex::icase | boost::regex::perl;
static const std::vector<ColNames> cols
{
    //       code                           subc                      category                       data type                   standard name                  std components            possible names on CSV import
    //       (in numeric order)                                                                                                  (case sensitive)               (case sensitive)          (case insensitive)
    ColNames(utilNormNames::BLOCKID,        0,                        utilNormNames::TY_INDEX,       utilNormNames::DTY_FLOAT,   std::string("BlockId"),        std::string(""),          boost::regex("blockid", fl)),
    ColNames(utilNormNames::MATERIAL,       0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("Material"),       std::string(""),          boost::regex("material|corr.*", fl)),
    ColNames(utilNormNames::ROWTYPE,        0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("RowType"),        std::string(""),          boost::regex("rowtype", fl)),
    ColNames(utilNormNames::CODE,           0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_ANY,     std::string("Code"),           std::string(""),          boost::regex("code|code_(.+)", fl)),
    ColNames(utilNormNames::GEOLOGY,        0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_ANY,     std::string("Geology"),        std::string(""),          boost::regex("geol.*", fl)),
    ColNames(utilNormNames::CLASS,          0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_ANY,     std::string("Class"),          std::string(""),          boost::regex("class|class_(.+)", fl)),
    ColNames(utilNormNames::PRODUCT,        0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_ANY,     std::string("Product"),        std::string(""),          boost::regex("product", fl)),
    ColNames(utilNormNames::NUMITEMS,       0,                        utilNormNames::TY_DIMENSION,   utilNormNames::DTY_INTEGER, std::string("NumItems"),       std::string(""),          boost::regex("numitems", fl)),
    ColNames(utilNormNames::DATE,           0,                        utilNormNames::TY_DATE,        utilNormNames::DTY_STRING,  std::string("Date"),           std::string(""),          boost::regex("date", fl)),
    ColNames(utilNormNames::CELLCOLOR,      0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Colors"),         std::string(""),          boost::regex("colors", fl)),
    ColNames(utilNormNames::CELLALPHA,      0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("ColorsAlpha"),    std::string(""),          boost::regex("coloralpha", fl)),
    ColNames(utilNormNames::LEVEL,          0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Level"),          std::string(""),          boost::regex("level|level_(.+)", fl)),
    ColNames(utilNormNames::COLUMN,         0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Column"),         std::string(""),          boost::regex("column|column_(.+)", fl)),
    ColNames(utilNormNames::ROW,            0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Row"),            std::string(""),          boost::regex("row|row_(.+)", fl)),
    ColNames(utilNormNames::LINE,           0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Line"),           std::string(""),          boost::regex("line", fl)),
    ColNames(utilNormNames::I,              0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("I"),              std::string(""),          boost::regex("i", fl)),
    ColNames(utilNormNames::J,              0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("J"),              std::string(""),          boost::regex("j", fl)),
    ColNames(utilNormNames::K,              0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("K"),              std::string(""),          boost::regex("k", fl)),
    ColNames(utilNormNames::CAT,            0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_ANY,     std::string("Cat"),            std::string(""),          boost::regex("cat_(.+)", fl)),
    ColNames(utilNormNames::NAME,           0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_STRING,  std::string("Name"),           std::string(""),          boost::regex("name", fl)),
    ColNames(utilNormNames::NAMEX,          0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_STRING,  std::string("N"),              std::string(""),          boost::regex("n_(.+)", fl)),
    ColNames(utilNormNames::START,          0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Start"),          std::string(""),          boost::regex("start|begin|open|first", fl)),
    ColNames(utilNormNames::FORCE,          0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_INTEGER, std::string("Force"),          std::string(""),          boost::regex("force", fl)),
    ColNames(utilNormNames::BLOCKCENTER,    utilNormNames::COORD_X,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Center"),         std::string("X"),         boost::regex("center:0|center_0|cent.*x|world.*x|x.*world|x|east.*", fl)),
    ColNames(utilNormNames::BLOCKCENTER,    utilNormNames::COORD_Y,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Center"),         std::string("Y"),         boost::regex("center:1|center_1|cent.*y|world.*y|y.*world|y|north.*", fl)),
    ColNames(utilNormNames::BLOCKCENTER,    utilNormNames::COORD_Z,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Center"),         std::string("Z"),         boost::regex("center:2|center_2|cent.*z|world.*z|z.*world|z|alt.*", fl)),
    ColNames(utilNormNames::BLOCKCENTER,    utilNormNames::COORD_MAG, utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Center"),         std::string("Magnitude"), boost::regex("center_magnitude", fl)),
    ColNames(utilNormNames::BLOCKSIZE,      utilNormNames::COORD_X,   utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Size"),           std::string("X"),         boost::regex("size:0|size_0|size.*x.*|dx|width", fl)),
    ColNames(utilNormNames::BLOCKSIZE,      utilNormNames::COORD_Y,   utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Size"),           std::string("Y"),         boost::regex("size:1|size_1|size.*y.*|dy|length", fl)),
    ColNames(utilNormNames::BLOCKSIZE,      utilNormNames::COORD_Z,   utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Size"),           std::string("Z"),         boost::regex("size:2|size_2|size.*z.*|dz|height", fl)),
    ColNames(utilNormNames::BLOCKSIZE,      utilNormNames::COORD_MAG, utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Size"),           std::string("Magnitude"), boost::regex("size_magnitude", fl)),
    ColNames(utilNormNames::ANGLE,          0,                        utilNormNames::TY_ANGLE,       utilNormNames::DTY_FLOAT,   std::string("Angle"),          std::string(""),          boost::regex("ang.*|rot.*", fl)),
    ColNames(utilNormNames::VOLUME,         0,                        utilNormNames::TY_DIMENSION3,  utilNormNames::DTY_FLOAT,   std::string("Volume"),         std::string(""),          boost::regex("vol|volume", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_XNEG,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("XNeg"),      boost::regex("nb:0|nb_0|nb:xneg|nbxneg", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_XPOS,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("XPos"),      boost::regex("nb:1|nb_1|nb:xpos|nbxpos", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_YNEG,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("YNeg"),      boost::regex("nb:2|nb_2|nb:yneg|nbyneg", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_YPOS,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("YPos"),      boost::regex("nb:3|nb_3|nb:ypos|nbypos", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_ZNEG,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("ZNeg"),      boost::regex("nb:4|nb_4|nb:zneg|nbzneg", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_ZPOS,  utilNormNames::TY_INDEX,       utilNormNames::DTY_INTEGER, std::string("Nb"),             std::string("ZPos"),      boost::regex("nb:5|nb_5|nb:zpos|nbzpos", fl)),
    ColNames(utilNormNames::BLOCKNEIGHBOR,  utilNormNames::DIR_MAG,   utilNormNames::TY_INDEX,       utilNormNames::DTY_FLOAT,   std::string("Nb"),             std::string("Magnitude"), boost::regex("nb_magnitude", fl)),
    ColNames(utilNormNames::COLLAR,         utilNormNames::COORD_X,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Collar"),         std::string("X"),         boost::regex("xcollar|collar:0|collar_0|collar.*x|sondage.*x", fl)),
    ColNames(utilNormNames::COLLAR,         utilNormNames::COORD_Y,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Collar"),         std::string("Y"),         boost::regex("ycollar|collar:1|collar_1|collar.*y|sondage.*y", fl)),
    ColNames(utilNormNames::COLLAR,         utilNormNames::COORD_Z,   utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Collar"),         std::string("Z"),         boost::regex("zcollar|collar:2|collar_2|collar.*z|sondage.*z", fl)),
    ColNames(utilNormNames::COLLAR,         utilNormNames::COORD_MAG, utilNormNames::TY_COORDINATE,  utilNormNames::DTY_FLOAT,   std::string("Collar"),         std::string("Magnitude"), boost::regex("collar_magnitude", fl)),
    ColNames(utilNormNames::DEPTH,          utilNormNames::HOLE_FROM, utilNormNames::TY_RANGE,       utilNormNames::DTY_FLOAT,   std::string("Depth"),          std::string("From"),      boost::regex("from|depth:0|depth_0|depth.*from|schicht.*von", fl)),
    ColNames(utilNormNames::DEPTH,          utilNormNames::HOLE_TO,   utilNormNames::TY_RANGE,       utilNormNames::DTY_FLOAT,   std::string("Depth"),          std::string("To"),        boost::regex("to|depth:1|depth_1|depth.*to|schicht.*bis", fl)),
    ColNames(utilNormNames::DEPTH,          utilNormNames::HOLE_MAG,  utilNormNames::TY_RANGE,       utilNormNames::DTY_FLOAT,   std::string("Depth"),          std::string("Magnitude"), boost::regex("depth_magnitude", fl)),
    ColNames(utilNormNames::MAXDEPTH,       0,                        utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("MaxDepth"),       std::string(""),          boost::regex("depth|max_depth", fl)),
    ColNames(utilNormNames::SURVEYAT,       0,                        utilNormNames::TY_RANGE,       utilNormNames::DTY_FLOAT,   std::string("SurveyAt"),       std::string(""),          boost::regex("surveyat|at", fl)),
    ColNames(utilNormNames::HOLEAZIMUTH,    0,                        utilNormNames::TY_ANGLE,       utilNormNames::DTY_FLOAT,   std::string("Azimuth"),        std::string(""),          boost::regex("az|azimuth", fl)),
    ColNames(utilNormNames::HOLEDIP,        0,                        utilNormNames::TY_ANGLE,       utilNormNames::DTY_FLOAT,   std::string("Dip"),            std::string(""),          boost::regex("dip", fl)),
    ColNames(utilNormNames::HOLEID,         0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("HoleId"),         std::string(""),          boost::regex("holeid|hole_id|bhid|dhid|dhnr|name.*hole|sond.*name|name.*sond.*", fl)),
    ColNames(utilNormNames::SAMPID,         0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("SampleId"),       std::string(""),          boost::regex("sampleid|samp.*id|prob.*name|name.*prob.*", fl)),
    ColNames(utilNormNames::HOLETYPE,       0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_FLOAT,   std::string("HoleType"),       std::string(""),          boost::regex("holetype|hole_type|sond.*meth.*|meth.*sond.*", fl)),
    ColNames(utilNormNames::HOLEPATH,       0,                        utilNormNames::TY_CATEGORY,    utilNormNames::DTY_FLOAT,   std::string("HolePath"),       std::string(""),          boost::regex("holepath|hole_path", fl)),
    ColNames(utilNormNames::LABELS,         0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("Labels"),         std::string(""),          boost::regex("labels", fl)),
    ColNames(utilNormNames::BLASTID,        0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("BlastId"),        std::string(""),          boost::regex("blastid", fl)),
    ColNames(utilNormNames::PLANID,         0,                        utilNormNames::TY_NAME,        utilNormNames::DTY_STRING,  std::string("PlanId"),         std::string(""),          boost::regex("planid", fl)),
    ColNames(utilNormNames::CONMIN,         0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("MinTons"),        std::string(""),          boost::regex("mintons", fl)),
    ColNames(utilNormNames::CONMAX,         0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("MaxTons"),        std::string(""),          boost::regex("maxtons", fl)),
    ColNames(utilNormNames::TONSINITIAL,    0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("TonsInitial"),    std::string(""),          boost::regex("tonsinitial", fl)),
    ColNames(utilNormNames::MININGSLOPE,    utilNormNames::DIR_XNEG,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Slope"),          std::string("XNeg"),      boost::regex("slope:0|slope_0|slope:xneg|slopexneg", fl)),
    ColNames(utilNormNames::MININGSLOPE,    utilNormNames::DIR_XPOS,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Slope"),          std::string("XPos"),      boost::regex("slope:1|slope_1|slope:xpos|slopexpos", fl)),
    ColNames(utilNormNames::MININGSLOPE,    utilNormNames::DIR_YNEG,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Slope"),          std::string("YNeg"),      boost::regex("slope:2|slope_2|slope:yneg|slopeyneg", fl)),
    ColNames(utilNormNames::MININGSLOPE,    utilNormNames::DIR_YPOS,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Slope"),          std::string("YPos"),      boost::regex("slope:3|slope_3|slope:ypos|slopeypos", fl)),
    ColNames(utilNormNames::MININGSLOPE,    utilNormNames::DIR_MAG,   utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Slope"),          std::string("Magnitude"), boost::regex("slope_magnitude", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_XNEG,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("XNeg"),      boost::regex("pref:0|pref_0|pref:xneg|prefxneg", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_XPOS,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("XPos"),      boost::regex("pref:1|pref_1|pref:xpos|prefxpos", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_YNEG,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("YNeg"),      boost::regex("pref:2|pref_2|pref:yneg|prefyneg", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_YPOS,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("YPos"),      boost::regex("pref:3|pref_3|pref:ypos|prefypos", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_ZNEG,  utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("ZNeg"),      boost::regex("pref:4|pref_4|pref:zneg|prefzneg", fl)),
    ColNames(utilNormNames::PREFERENCE,     utilNormNames::DIR_MAG,   utilNormNames::TY_DIMENSION,   utilNormNames::DTY_FLOAT,   std::string("Pref"),           std::string("Magnitude"), boost::regex("pref_magnitude", fl)),
    ColNames(utilNormNames::KTONS,          0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("KTons"),          std::string(""),          boost::regex("ktons|tonnage", fl)),
    ColNames(utilNormNames::TONS,           0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Tons"),           std::string(""),          boost::regex("tons", fl)),
    ColNames(utilNormNames::TONSPROD,       0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Tons"),           std::string(""),          boost::regex("tons_(.+)", fl)),
    ColNames(utilNormNames::OFFEREDTOTAL,   0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Offered"),        std::string(""),          boost::regex("offered", fl)),
    ColNames(utilNormNames::OFFEREDTOTPERC, 0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("OfferedPerc"),    std::string(""),          boost::regex("offeredperc", fl)),
    ColNames(utilNormNames::OFFEREDPROD,    0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Offered"),        std::string(""),          boost::regex("offered_(.+)", fl)),
    ColNames(utilNormNames::OFFEREDPRDPERC, 0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("OfferedPerc"),    std::string(""),          boost::regex("offeredperc_(.+)", fl)),
    ColNames(utilNormNames::TAKENTOTAL,     0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Taken"),          std::string(""),          boost::regex("taken", fl)),
    ColNames(utilNormNames::TAKENTOTPERC,   0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("TakenPerc"),      std::string(""),          boost::regex("takenperc", fl)),
    ColNames(utilNormNames::TAKENPROD,      0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Taken"),          std::string(""),          boost::regex("taken_(.+)", fl)),
    ColNames(utilNormNames::TAKENPRDPERC,   0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("TakenPerc"),      std::string(""),          boost::regex("takenperc_(.+)", fl)),
    ColNames(utilNormNames::WASTED4BLOCK,   0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("WastedForBlock"), std::string(""),          boost::regex("wastedforblock", fl)),
    ColNames(utilNormNames::WASTED,         0,                        utilNormNames::TY_TONNAGE,     utilNormNames::DTY_FLOAT,   std::string("Wasted"),         std::string(""),          boost::regex("wasted|waste|stripping", fl)),
    ColNames(utilNormNames::WASTEDPERC,     0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("WastedPerc"),     std::string(""),          boost::regex("wastedperc", fl)),
    ColNames(utilNormNames::MININGPERIOD,   0,                        utilNormNames::TY_PERIOD,      utilNormNames::DTY_INTEGER, std::string("Period"),         std::string(""),          boost::regex("miningperiod|period", fl)),
    ColNames(utilNormNames::TAKENPERIOD,    0,                        utilNormNames::TY_PERIOD,      utilNormNames::DTY_INTEGER, std::string("FirstPeriod"),    std::string(""),          boost::regex("firstperiod", fl)),
    ColNames(utilNormNames::WASTEDPERIOD,   0,                        utilNormNames::TY_PERIOD,      utilNormNames::DTY_INTEGER, std::string("WastePeriod"),    std::string(""),          boost::regex("wasteperiod", fl)),
    ColNames(utilNormNames::WASTERATIO,     0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("WasteRatio"),     std::string(""),          boost::regex("wasteratio", fl)),
    ColNames(utilNormNames::PIT,            0,                        utilNormNames::TY_WEIGHT,      utilNormNames::DTY_FLOAT,   std::string("Pit"),            std::string(""),          boost::regex("pit|pit_(.+)", fl)),
    ColNames(utilNormNames::ZONE,           0,                        utilNormNames::TY_WEIGHT,      utilNormNames::DTY_FLOAT,   std::string("Zone"),           std::string(""),          boost::regex("zone|zone_(.+)", fl)),
    ColNames(utilNormNames::DENSITY,        0,                        utilNormNames::TY_SPECTONNAGE, utilNormNames::DTY_FLOAT,   std::string("Density"),        std::string(""),          boost::regex("dens.*|sg|spec.*", fl)),
    ColNames(utilNormNames::VOLFACTOR,      0,                        utilNormNames::TY_RATIO,       utilNormNames::DTY_FLOAT,   std::string("VolFactor"),      std::string(""),          boost::regex("volfactor|volumefactor|fillingdeg.*", fl)),
    ColNames(utilNormNames::LS,             0,                        utilNormNames::TY_DERIVED,     utilNormNames::DTY_FLOAT,   std::string("LS"),             std::string(""),          boost::regex("ls", fl)),
    ColNames(utilNormNames::SR,             0,                        utilNormNames::TY_DERIVED,     utilNormNames::DTY_FLOAT,   std::string("SR"),             std::string(""),          boost::regex("sr", fl)),
    ColNames(utilNormNames::AR,             0,                        utilNormNames::TY_DERIVED,     utilNormNames::DTY_FLOAT,   std::string("AR"),             std::string(""),          boost::regex("ar", fl)),
    ColNames(utilNormNames::ASR,            0,                        utilNormNames::TY_DERIVED,     utilNormNames::DTY_FLOAT,   std::string("ASR"),            std::string(""),          boost::regex("asr", fl)),
    ColNames(utilNormNames::NAEQ,           0,                        utilNormNames::TY_DERIVED,     utilNormNames::DTY_FLOAT,   std::string("NaEq"),           std::string(""),          boost::regex("naeq", fl)),
    ColNames(utilNormNames::LOI,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("LOI"),            std::string(""),          boost::regex("loi", fl)),
    ColNames(utilNormNames::SIO2,           0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("SiO2"),           std::string(""),          boost::regex("sio2", fl)),
    ColNames(utilNormNames::AL2O3,          0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Al2O3"),          std::string(""),          boost::regex("al2o3", fl)),
    ColNames(utilNormNames::FE2O3,          0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Fe2O3"),          std::string(""),          boost::regex("fe2o3", fl)),
    ColNames(utilNormNames::CAO,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("CaO"),            std::string(""),          boost::regex("cao", fl)),
    ColNames(utilNormNames::MGO,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("MgO"),            std::string(""),          boost::regex("mgo", fl)),
    ColNames(utilNormNames::SO3,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("SO3"),            std::string(""),          boost::regex("so3", fl)),
    ColNames(utilNormNames::K2O,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("K2O"),            std::string(""),          boost::regex("k2o", fl)),
    ColNames(utilNormNames::NA2O,           0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Na2O"),           std::string(""),          boost::regex("na2o", fl)),
    ColNames(utilNormNames::TIO2,           0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("TiO2"),           std::string(""),          boost::regex("tio2", fl)),
    ColNames(utilNormNames::MN2O3,          0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Mn2O3"),          std::string(""),          boost::regex("mn2o3", fl)),
    ColNames(utilNormNames::P2O5,           0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("P2O5"),           std::string(""),          boost::regex("p2o5", fl)),
    ColNames(utilNormNames::CR2O3,          0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Cr2O3"),          std::string(""),          boost::regex("cr2o3", fl)),
    ColNames(utilNormNames::CL,             0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Cl"),             std::string(""),          boost::regex("cl", fl)),
    ColNames(utilNormNames::F,              0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("F"),              std::string(""),          boost::regex("f", fl)),
    ColNames(utilNormNames::HG,             0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("Hg"),             std::string(""),          boost::regex("hg", fl)),
    ColNames(utilNormNames::TOC,            0,                        utilNormNames::TY_DIRECT,      utilNormNames::DTY_FLOAT,   std::string("TOC"),            std::string(""),          boost::regex("toc", fl)),
    ColNames(utilNormNames::TEMP,           0,                        utilNormNames::TY_TEMPORARY,   utilNormNames::DTY_FLOAT,   std::string("_TEMP_"),         std::string(""),          boost::regex("_TEMP_.*|vtk.*", fl)),
    ColNames(utilNormNames::ENDATTS,        0,                        utilNormNames::TY_IGNORE,      utilNormNames::DTY_FLOAT,   std::string(""),               std::string(""),          boost::regex("^(?![\\s\\S])", fl))
                                                                                                                                                                                          // includes the empty string (last alternative)
};

std::string utilNormNames::getName(int index)
{
    int n = inxInCols(index);
    if(0 > n)
        return std::string();

    return cols[n].normBase_;
}

std::string utilNormNames::getFullName(int index, int component)
{
    int n = inxInCols(index);
    if(0 > n)
        return std::string();

    // check for vector overflow
    if((n + component) >= cols.size())
        return std::string();

    // put together the full name with :
    std::string fn(cols[n].normBase_ + ":" + cols[n + component].normComponent_);

    return fn;
}

std::pair<int, int> utilNormNames::getIndex(const std::string& baseName, int compIndex)
{
    // complete the name with component - in case we really have one
    std::string fullName = baseName + ":" + std::to_string(compIndex);

    // we use the regex here to find the proper index
    for(auto it = cols.begin(); it != cols.end(); ++it)
    {
        if(boost::regex_match(baseName, it->recognized_) ||
           boost::regex_match(fullName, it->recognized_))
        {
            return std::pair<int, int>(it->index_.first, compIndex);
        }
    }

    // nothing found
    return std::pair<int, int>(ENDATTS + 1, 0);
}

int utilNormNames::getType(const std::string& baseName)
{
    std::pair<int, int> inx = getIndex(baseName);

    if(ENDATTS < inx.first)
    {
        return TY_DIRECT;
    }

    int n = inxInCols(inx.first);
    if(0 > n)
    {
        return TY_DIRECT;
    }

    int ty = cols[n].type_;

    return ty;
}

std::string utilNormNames::getTypeDesc(std::string const& baseName)
{
    return typeDesc[getType(baseName)];
}

int utilNormNames::getDataType(std::string const& baseName)
{
    std::pair<int, int> inx = getIndex(baseName);

    if(ENDATTS < inx.first)
    {
        return DTY_ANY;
    }

    int n = inxInCols(inx.first);
    if(0 > n)
    {
        return DTY_ANY;
    }

    int ty = cols[n].dataType_;

    return ty;
}

int utilNormNames::getDerivedIndex(bool first)
{
    if(first)
        return CALCULATED + 1;
    else
        return MEASURED;
}

int utilNormNames::getMeasuredIndex(bool first)
{
    if(first)
        return MEASURED + 1;
    else
        return ENDATTS;
}

std::vector<std::string> utilNormNames::getComponents(std::string const& baseName)
{
    // return vector
    std::vector<std::string> sl;

    // get the index and see if it is valid
    std::pair<int, int> inx = getIndex(baseName, 0);
    if(ENDATTS < inx.first)
    {
        return sl;
    }

    int n = inxInCols(inx.first);
    if(0 > n)
        return sl;

    // collect all component names
    for(int cinx = n; cols[cinx].normBase_ == baseName; ++cinx)
    {
        std::string cmp = cols[cinx].normComponent_;
        if(!cmp.empty() && (cmp != "Magnitude"))
            sl.push_back(cmp);
        else
            break;
    }

    return sl;
}

std::string utilNormNames::normalize(std::string const& name, bool includeComponent)
{
    // this is supposed to get rid of any UTF-8 BOM codes that sometimes
    // happen to end up in the column names of the first column of a CSV file
    std::string normName = name;
    if((3 < normName.size()) &&
       (239 == static_cast<unsigned long>((unsigned char)normName.at(0))) &&
       (187 == static_cast<unsigned long>((unsigned char)normName.at(1))) &&
       (191 == static_cast<unsigned long>((unsigned char)normName.at(2))))
        normName = normName.substr(3);

    for(auto it = cols.begin(); it != cols.end(); ++it)
    {
        boost::smatch mres;
        std::string newName;
        if(boost::regex_match(normName, mres, it->recognized_))
        {
            // this is the main name of any data type
            newName = it->normBase_;

            // if the name has an extension for the base name, append it here
            // note: we do not really support more than one extension
            if(2 <= mres.size())
            {
                std::string xname = mres[1];
                if(!xname.empty())
                    newName += std::string("_") + xname;
            }

            // add component if required
            if(includeComponent)
            {
                std::string compName = it->normComponent_;
                if(!compName.empty())
                    newName += ":" + compName;
            }

            // until here we kept the normName intact because by changing it
            // we would also invalidate the mres data structure
            normName = newName;
            break;
        }
    }

    return normName;
}

int utilNormNames::inxInCols(int index)
{
    if(index >= cols.size())
        return -1;

    // this assumes that index is numerically sorted so we know that n is always >= index
    for(int n = index; n < cols.size(); ++n)
        if(cols[n].index_.first == index)
            return n;

    return -1;
}
