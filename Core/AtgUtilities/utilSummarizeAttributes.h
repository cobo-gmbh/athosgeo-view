/*=========================================================================

   Program: AthosGEO
   Module:  utilSummarizeAttributes.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef utilSummarizeAttributes_h
#define utilSummarizeAttributes_h

#include <set>
#include <map>
#include <vector>
#include <string>
#include <boost/dynamic_bitset.hpp>
#include <vtkSystemIncludes.h>
#include <vtkSmartPointer.h>
#include <vtkVariant.h>
#include <vtkType.h>
#include <AtgUtilitiesModule.h>

class vtkTable;
class vtkAbstractArray;
class vtkDataArray;
class vtkDataSetAttributes;
class vtkDoubleArray;

class ATGUTILITIES_EXPORT utilSummarizeAttributes
{
public:

    // constructor, defining input data set
    utilSummarizeAttributes(vtkDataSetAttributes* input);
    ~utilSummarizeAttributes();

    // select one or several attributes for normalization
    // comp is a component that will be taken from the first of the weight attributes
    // note: for some temporary taken tonnage attribute, components are used to store
    //   the values for different products
    void setNormalization(std::vector<std::string> const& weights, int comp = 0);

    // if this is true explicitly, we take 1 as a weight instead of any tonnage
    void setAllowNonWeighted(bool allow);

    // if the sum of the norm values goes to a specific target column, put
    // the name here - or else make it an empty string
    void setNormSumTarget(std::string const& normSumTarget);

    // define a category attribute for summarization
    void setCategory(std::string const& category);

    // specify whether all or only selected items are to be summarized
    // note: this option is only valid if there is only one input data
    // set provided (because otherwise we would need to generate
    // global ids for the selected ids: see below)
    void setSelectedOnly(bool selectedOnly);

    // specify the selected IDs for the "selected only" option
    void setSelectedIds(std::set<int> const& ids);

    // option to add cement moduli after summarizing
    void setIncludeCementModuli(bool incCM);

    // option to include averages of collars if such an attribute exists
    void setIncludeCollars(bool incColl);

    // if this is turned on, min and max value rows are not generated, and also
    // not a rowtype attribute
    void setValuesOnly(bool valuesOnly);

    // exclude tonnage attributes (default: false)
    // note: this makes sense for output tables from optimizer or scheduler because things
    //   like "offered_2ndProd" are confusing in a row that indicates taken tons for 1stProd...
    void setExcludeTonnAtts(bool excludeAtts);

    // NULL handling: if it is ON, the given value is treated as being NULL, and if one
    // occurs somewhere, any sum or mean will be NULL as well, no matter what else is
    // in the input
    void setNullHandling(bool on, double nullVal = -1.);

    // generate a Product column also with only one product (default: false)
    void setAlwaysShowProduct(bool show);

    // summarize according to defined parameters
    // note: before calling this, normalization and optionally norm component must
    //   be properly set
    void summarize(vtkTable* output);

    // do the above for all products, joining the output of summarize calls for
    // every product available
    // note that in this case setting normalization and norm component will be handled
    //   automatically by the function
    // note: after the summarization, any KTons column is removed from the output
    //   because it would be confusing in this context
    bool summarizeProducts(vtkTable* outputTaken,
                           std::vector<std::string>& prodNames,
                           std::string const& normAtt);

    // if some error occurred this will not be empty
    inline std::string const& error() const;

private:

    // input data, either cells of an unstructured grid or table
    vtkDataSetAttributes* input_;

    // weights to be considered for either tonnage or volume weighted summary
    std::vector<vtkDataArray*> tonnWeights_,
                               volWeights_;
    std::vector<vtkIdType> tonnComps_;
    vtkSmartPointer<vtkDoubleArray> volArray_;

    // allow explicitly for non-weighted summary
    bool allowNonWeighted_;

    // total tonnage weight attribute
    // note: this is the target of the tonnage, and if it is not specified
    //   before calling the summarizing functions, the first of the tonnage
    //   weight attributes will be used
    std::string normSumTarget_;

    // if filtering by category values
    // note: these can be numeric - and in this case they are rounded to integer
    //   values if they are of a different type -, or else they can be strings (names)
    vtkAbstractArray* category_;

    // if additional weight attributes are being used (like Pit) which are typically
    // in percent, this is used to compensate for the factor 100
    vtkDataArray* percNormArr_;

    // if working with selected blocks only
    bool selectedOnly_;
    std::set<vtkIdType> selectedIds_;

    // add the cement moduli after calculating the summaries
    bool includeCementModuli_;

    // include calculating average of collar values if they exist
    bool includeCollars_;

    // calculate summary values only, no min and max
    bool valuesOnly_;

    // exclude tonnage attributes (optimizer or scheduler results)
    bool excludeTonnAtts_;

    // always show a Product attribute, even if only one
    bool alwaysProductAtt_;

    // NULL value handling
    bool handleNullValues_;
    double nullValue_;

    // whatever error may happen
    std::string errorMsg_;

    // convenience typedef
    typedef std::set<vtkDataArray*> AttSet;

    // sort attributes by type
    // sort the attributes by types
    AttSet tonnSet_,
           dim3Set_,
           ktonsDensSet_, // for ktons and density: if both are there -> volume
           densSet_,
           directSet_,
           derivedSet_,
           weightSet_, // pit or zone column - to be weighted by tonnage or else volume
           dimSet_; // 3 dimensions - as an alternative for volume for weighted mean

    // helper for summarizing for category values that can be numeric or string values
    // note: the second value is a counter of the number of occurrences
    std::map<vtkVariant, vtkIdType> catValues_;

    // convenience data structures: bits will be set only for those input table
    // rows where all weights are non-zero, and if we are summarizing for a selection only,
    // also this will be reflected
    boost::dynamic_bitset<> usedRowsFilt_;

    // collect all available values for a category
    void collectCategoryValues();

    // get a category value, either string or rounded numeric
    vtkVariant getCategoryValue(vtkIdType row);

    // summarize for one category value, or globally if category_ is empty
    void doSummarize(vtkTable* output, vtkVariant cat = vtkVariant());

    // helper function for weighted mean
    // note: a plain sum can be obtained by providing no weights array(s) and
    //   a weightsTotal value of 1.
    // note: weightComps gives the components for the respective weights arrays
    bool addWeightedMean(vtkTable* output, vtkDataArray* arr,
                         std::vector<vtkDataArray*>& weights, std::vector<vtkIdType>& weightComps,
                         double weightTotal, vtkVariant cat);

    // this function will append values to the column with 'name' in the 'output' table,
    // and if it does not exist, it will generate it (as a vtkDoubleArray), with the
    // indicated number of components.
    // note: the value array is assumed to hold all the required values, depending on
    //   valuesOnly_ flag and number of components
    // note: it is assumed that the table has already the necessary number of rows to
    //   enter all the values
    void addColumn(vtkTable* output, std::string const& name,
                   std::vector<double> const& value, int numComponents = 1);

};

inline std::string const& utilSummarizeAttributes::error() const
{
    return errorMsg_;
}

#endif
