/*=========================================================================

   Program: AthosGEO
   Module:  utilPolynomial.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <vtkDataSetAttributes.h>
#include <vtkDataArray.h>
#include <utilPolynomial.h>

utilPolynomial::utilPolynomial()
:   constant_(0.)
{
}

utilPolynomial::utilPolynomial(std::string const& params)
:   constant_(0.)
{
    std::vector<std::string> toks;
    boost::split(toks, params, boost::is_any_of("|"));

    for(int m = 0; m < toks.size(); ++m)
    {
        // extract name and factor, skipping empty elements
        std::vector<std::string> elems;
        boost::split(elems, toks.at(m), boost::is_any_of(" "), boost::token_compress_on);

        if(2 != elems.size())
            continue; // emergency exit - should not happen

        double coeff = 0.;
        try
        {
            coeff = std::stod(elems.at(0));
        }
        catch(...)
        {
            // do just nothing - leave the value: it's an emergency brake anyway
        }
        std::string pname = elems.at(1);

        // handle result
        if('$' == pname.at(0))
            constant_ = coeff;
        else
            param[pname] = coeff;
    }
}

bool utilPolynomial::initArrays(vtkDataSetAttributes* data)
{
    arrays.clear();

    // go through the parameters and try to find double arrays for them
    // note: if one of them cannot be found, the function will fail (return false)
    for(auto it = param.begin(); it != param.end(); ++it)
    {
        vtkDataArray* arr = vtkDataArray::SafeDownCast(data->GetArray(it->first.c_str()));

        if(nullptr == arr)
            return false;

        arrays[it->first] = arr;
    }

    // everything found, so ok
    return true;
}

double utilPolynomial::operator()(int row)
{
    double val = constant_;

    for(auto it = param.begin(); it != param.end(); ++it)
    {
        vtkDataArray* arr = arrays[it->first];
        val += it->second * arr->GetVariantValue(row).ToDouble();
    }

    return val;
}

void utilPolynomial::print(std::ostream& os) const
{
    os << "(";
    for(auto it = param.begin(); it != param.end(); ++it)
        os << "( " << it->first << " " << it->second << " )";
    os << ")";
}
