/*=========================================================================

   Program: AthosGEO
   Module:  utilDerivedValues.h

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef utilDerivedValues_h
#define utilDerivedValues_h

#include <map>
#include <vector>
#include <string>
#include <vtkSystemIncludes.h>
#include <vtkType.h>
#include <AtgUtilitiesModule.h>

class utilCementModule;
class vtkDataSetAttributes;

class ATGUTILITIES_EXPORT utilDerivedValues
{
public:

    // constructor with specific data set attributes
    utilDerivedValues(vtkDataSetAttributes* data);
    ~utilDerivedValues();

    /// names of derived values
    /// \param all if true, all existing derived values are listed, otherwise only
    ///        the ones that are valid for the current data set
    std::vector<std::string> derivedValues(bool all = false) const;

    // find out if for the currently prepared table a derived value can be calculated
    bool derivedExists(std::string const& mod) const;

    // for calculating numerator and denumerator separately, for many rows
    // without having to search the map
    utilCementModule* module(std::string const& mod);

    // calculate a derived value for a table row
    double calculate(std::string const& mod, vtkIdType row);

private:

    // do not copy this class
    utilDerivedValues(utilDerivedValues const&);
    void operator=(utilDerivedValues const&);

    // names of derived values (all existing) - valid for all
    static std::vector<std::string> derivedValues_;

    // implementation with data specific modules
    std::map<std::string, utilCementModule*> cemMods_;

};

#endif
