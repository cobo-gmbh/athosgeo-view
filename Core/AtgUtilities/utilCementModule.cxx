/*=========================================================================

   Program: AthosGEO
   Module:  utilCementModule.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <vtkDataSetAttributes.h>
#include <utilCementModule.h>

utilCementModule::utilCementModule(int index, const std::string& numer, std::string const& denom)
:   index_(index),
    numer_(numer),
    denom_(denom),
    prepared_(false)
{}

bool utilCementModule::initArrays(vtkDataSetAttributes* data)
{
    prepared_ = numer_.initArrays(data) &&
                denom_.initArrays(data);
    return prepared_;
}

bool utilCementModule::isPrepared() const
{
    return prepared_;
}

double utilCementModule::numer(vtkIdType row)
{
    return numer_(row);
}

double utilCementModule::denom(vtkIdType row)
{
    return denom_(row);
}

double utilCementModule::operator()(vtkIdType row)
{
    double n = numer(row),
           d = denom(row);

    if(0. == d)
        return 0.;
    else
        return n / d;
}
