/*=========================================================================

   Program: AthosGEO
   Module:  utilSampleToGridCell.h

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef __utilSampleToGridCell_h
#define __utilSampleToGridCell_h

#include <vtkType.h>
#include <AtgUtilitiesModule.h>

class vtkUnstructuredGrid;
class vtkPoints;
class vtkFloatArray;

class ATGUTILITIES_EXPORT utilSampleToGridCell
{
public:

    typedef enum
    {
        // hole types
        ROUND_CYLINDER = 0,
        QUADRATIC_CYLINDER,
        TRIANGULAR_CYLINDER,
        HEXAGONAL_CYLINDER,

        // spot sample types
        SPHERE = 10,
        CUBE,
        TETRAHEDRON,
        OCTAHEDRON,

        // blast piles
        BLAST_PILE = 20,

        // possible further extensions
        EXTENSIONS = 30
    }
    CellType;

    utilSampleToGridCell(vtkUnstructuredGrid* grid);
    ~utilSampleToGridCell();

    void addCell(int inType, CellType outType, vtkPoints* pts, float decSize);

protected:

    vtkUnstructuredGrid* Grid;
    vtkPoints* Points;
    vtkFloatArray* Normals;

    // helper functions for inclined drillholes
    void calculateTransform(double pt1[3], double pt2[3], double mat[3][3]);
    void applyInclination(double pt[3], double mat[3][3]);

private:

    // hole types
    vtkIdType appendRoundCylinder(vtkPoints* pts, float size);
    vtkIdType appendQuadraticCylinder(vtkPoints* pts, float size);
    vtkIdType appendTriangularCylinder(vtkPoints* pts, float size);
    vtkIdType appendHexagonalCylinder(vtkPoints* pts, float size);

    // spot sample types
    vtkIdType appendSphere(vtkPoints* pts, float size);
    vtkIdType appendCube(vtkPoints* pts, float size);
    vtkIdType appendTetrahedron(vtkPoints* pts, float size);
    vtkIdType appendOctahedron(vtkPoints* pts, float size);

    // spot sample type
    vtkIdType appendBlastPile(vtkPoints* pts, float size);

};

#endif
