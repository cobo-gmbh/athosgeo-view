/*=========================================================================

   Program: AthosGEO
   Module:  utilDerivedValues.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <algorithm>
#include <vtkStringArray.h>
#include <vtkDataSetAttributes.h>
#include <utilNormNames.h>
#include <utilPolynomial.h>
#include <utilCementModule.h>
#include <utilDerivedValues.h>

std::vector<std::string> utilDerivedValues::derivedValues_;

utilDerivedValues::utilDerivedValues(vtkDataSetAttributes* data)
{
    static const utilCementModule cemMod[] =
    {
        utilCementModule(utilNormNames::LS,
                         std::string("100. CaO"),
                         std::string("2.8 SiO2|1.18 Al2O3|.65 Fe2O3")),
        utilCementModule(utilNormNames::SR,
                         std::string("1. SiO2"),
                         std::string("1. Al2O3|1. Fe2O3")),
        utilCementModule(utilNormNames::AR,
                         std::string("1. Al2O3"),
                         std::string("1. Fe2O3")),
        utilCementModule(utilNormNames::ASR,
                         std::string(".010638 K2O|.016129 Na2O|-.014085 Cl"),
                         std::string(".0125 SO3")),
        utilCementModule(utilNormNames::NAEQ,
                         std::string("1. Na2O|.658 K2O"),
                         std::string("1. $$"))  // means: constant value
    };

    // initialize names - once for all (static)
    if(derivedValues_.empty())
    {
        for(int n = 0; n < (sizeof(cemMod) / sizeof(utilCementModule)); ++n)
        {
            std::string mod = utilNormNames::getName(cemMod[n].index_);
            derivedValues_.push_back(mod);
        }
    }

    // initialize data specific modules
    for(int n = 0; n < (sizeof(cemMod) / sizeof(utilCementModule)); ++n)
    {
        utilCementModule* cm = new utilCementModule(cemMod[n]);
        cm->initArrays(data);
        std::string mod = utilNormNames::getName(cm->index_);
        cemMods_[mod] = cm;
    }
}

utilDerivedValues::~utilDerivedValues()
{
    for(auto it = cemMods_.begin(); it != cemMods_.end(); ++it)
        delete it->second;
    cemMods_.clear();
}

std::vector<std::string> utilDerivedValues::derivedValues(bool all) const
{
    if(all)
        return derivedValues_;

    std::vector<std::string> dv(derivedValues_.size());
    auto it = std::copy_if(derivedValues_.begin(), derivedValues_.end(), dv.begin(),
                           [this](std::string const& mod)
                           {
                               return derivedExists(mod);
                           });
    dv.resize(std::distance(dv.begin(), it));

    return dv;
}

bool utilDerivedValues::derivedExists(std::string const& mod) const
{
    std::map<std::string, utilCementModule*>::const_iterator it = cemMods_.find(mod);
    if(it == cemMods_.end())
        return false;

    return it->second->isPrepared();
}

utilCementModule* utilDerivedValues::module(std::string const& mod)
{
    std::map<std::string, utilCementModule*>::const_iterator it = cemMods_.find(mod);
    if(it != cemMods_.end())
        return it->second;
    else
        return 0;
}

double utilDerivedValues::calculate(std::string const& mod, vtkIdType row)
{
    std::map<std::string, utilCementModule*>::const_iterator it = cemMods_.find(mod);
    if(it == cemMods_.end())
        return 0.;

    return (*it->second)(row);
}
