/*=========================================================================

   Program: AthosGEO
   Module:  utilAddGridCells.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstring>
#include <vtkCellType.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPoints.h>
#include <utilNormNames.h>
#include <utilAddGridCells.h>

utilAddGridCells::utilAddGridCells(vtkUnstructuredGrid* grid, vtkPoints* points)
:   grid_(grid),
    points_(points)
{
}

vtkIdType utilAddGridCells::generateCell(double center[3], double size[3],
                                         double angle, double shift[3])
{
    // hexahedral cells are defined in VTK by 8 points that have to be aligned
    // in the id list in the following order:
    // order of points in a cell
    //
    // Z
    // ^   7 -------- 6
    // | / |        / |
    // 4 -------- 5   |
    // |   |   Y  |   |
    // |   | /    |   |
    // |   3 ---- | - 2
    // | /        | /
    // 0 -------- 1  -> X
    //
    // The patterns of signs for adding (half block size) to (centroid)
    // this means:
    //
    const static int sig[8][3] =
    {{-1, -1, -1},
     {+1, -1, -1},
     {+1, +1, -1},
     {-1, +1, -1},
     {-1, -1, +1},
     {+1, -1, +1},
     {+1, +1, +1},
     {-1, +1, +1}};

     // ---
    // +--
    // ++-
    // -+-
    // --+
    // +-+
    // +++
    // -++
    //

    // preparing the rotation
    static double const pi = 3.14159264;
    double a = pi * angle / 180.,
           sa = ::sin(a),
           ca = ::cos(a);

    // applying the shift to the center (if required)
    // note: this generates the side effect of returning also the new center
    double sCenter[3];
    ::memcpy(sCenter, center, sizeof(sCenter));
    if(0 != shift)
    {
        sCenter[0] += shift[0] * ca - shift[1] * sa;
        sCenter[1] += shift[0] * sa + shift[1] * ca;
        sCenter[2] += shift[2];
    }

    // pre-calculation for generating block corners
    // note: we use the values from the table if they exist, or else the ones
    // that are parameters of this filter
    double dx[2],
           dy[2];
    dx[0] = .5 * (  size[0] * ca + size[1] * sa);
    dx[1] = .5 * (  size[0] * ca - size[1] * sa);
    dy[0] = .5 * (- size[0] * sa + size[1] * ca);
    dy[1] = .5 * (  size[0] * sa + size[1] * ca);

    // generate block corners
    // note: the order is important here to add the points
    vtkIdType ptIndex[8];
    for(int i = 0; i < 8; ++i)
    {
        int di = (i + 1) % 2;
        ptIndex[i] = points_->InsertNextPoint(
                sCenter[0] + sig[i][0] * dx[di],
                sCenter[1] + sig[i][1] * dy[di],
                sCenter[2] + sig[i][2] * .5 * size[2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = grid_->InsertNextCell(VTK_HEXAHEDRON, 8, ptIndex);

    return cellId;
}
