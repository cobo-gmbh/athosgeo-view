/*=========================================================================

   Program: AthosGEO
   Module:  utilSampleToGridCell.cxx

   Copyright (c) 2020 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cassert>
#include <vtkUnstructuredGrid.h>
#include <vtkFloatArray.h>
#include <vtkCell.h>
#include <vtkPoints.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkDataObject.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <utilSampleToGridCell.h>

static const double epsPos = 0.001; // 1mm eps should be good enough for positions

utilSampleToGridCell::utilSampleToGridCell(vtkUnstructuredGrid* grid)
:   Grid(grid),
    Points(grid->GetPoints()),
    Normals(nullptr)
{
    // it is possible that Normals is nullptr, which will generate
    // cells with no Gouraud shading - which is better for some of them
    vtkPointData* pData = grid->GetPointData();
    Normals = vtkFloatArray::SafeDownCast(pData->GetArray("Normals"));
}

utilSampleToGridCell::~utilSampleToGridCell()
{
}

void utilSampleToGridCell::addCell(int inType, CellType outType, vtkPoints* pts, float decSize)
{
    (void)inType;

    switch(outType)
    {
        // holes
        case ROUND_CYLINDER:
            appendRoundCylinder(pts, decSize);
            break;
        case QUADRATIC_CYLINDER:
            appendQuadraticCylinder(pts, decSize);
            break;
        case TRIANGULAR_CYLINDER:
            appendTriangularCylinder(pts, decSize);
            break;
        case HEXAGONAL_CYLINDER:
            appendHexagonalCylinder(pts, decSize);
            break;

        // samples
        case SPHERE:
            appendSphere(pts, decSize);
            break;
        case CUBE:
            appendCube(pts, decSize);
            break;
        case TETRAHEDRON:
            appendTetrahedron(pts, decSize);
            break;
        case OCTAHEDRON:
            appendOctahedron(pts, decSize);
            break;

        // blast pile
        case BLAST_PILE:
            appendBlastPile(pts, decSize);
            break;

        default:
            appendSphere(pts, decSize);
    }
}

void utilSampleToGridCell::calculateTransform(double pt1[3], double pt2[3], double mat[3][3])
{
    /*
       These are the instructions from some internet discussion that is followed below:

       Suppose you want to find a rotation matrix R that rotates unit vector a onto unit vector b

       Proceed as follows:

       Let v = a × b
       Let s = ∥v∥ (sine of angle)
       Let c = a ⋅ b (cosine of angle)

       Then the rotation matrix R is given by:
       R = I + [v] + [v]^2 * (1 − c) / s^2

       where [v] is the skew-symmetric cross-product matrix of v,
                ⎡ 0   −v3 v2  ⎤
       [v] =def ⎢ v3  0   −v1 ⎥
                ⎣ −v2 v1  0   ⎦
       and I is a unit matrix

       The last part of the formula can be simplified to
       (1 − c) / s^2 == (1 − c) / (1 − c^2) == 1 / (1 + c)
       revealing that it is only for cos(∠(a,b)) = −1 not applicable,
       i.e., if a and b point into exactly opposite directions.
    */

    // handle special case: if p1 and p2 are identical, we will move p2 down by 1m
    if((epsPos > ::fabs(pt1[0] - pt2[0])) &&
       (epsPos > ::fabs(pt1[1] - pt2[1])) &&
       (epsPos > ::fabs(pt1[2] - pt2[2])))
        pt2[2] -= 1.;

    // we will start with two unit vectors: one pointing vertically down, the other
    // goes from pt1 to pt2 and is normalized to length 1
    double p1[3] = {0., 0., -1.},
           p2[3] = {pt2[0] - pt1[0], pt2[1] - pt1[1], pt2[2] - pt1[2]};
    double p2len = ::sqrt(p2[0] * p2[0] + p2[1] * p2[1] + p2[2] * p2[2]);
    for(int i = 0; i < 3; ++i)
        p2[i] /= p2len;

    // test data
    /*
    p1[0] = 0.043477;
    p1[1] = 0.036412;
    p1[2] = 0.998391;
    p2[0] = 0.60958;
    p2[1] = 0.73540;
    p2[2] = 0.29597;
    */

    // calculate the negative cross procuct v and the scalar product c
    // note: this gives us the rotation axis and the cosine of the rotation angle
    // note: negative cross product will give us the rotation matrix p1 -> p2;
    //       if we take it positive (like in the instructions followed here),
    //       the sense of rotation will be p2 -> p1
    double v[3] =
    {
        - p1[1] * p2[2] + p1[2] * p2[1],
        - p1[2] * p2[0] + p1[0] * p2[2],
        - p1[0] * p2[1] + p1[1] * p2[0]
    };
    double c = p1[0] * p2[0] + p1[1] * p2[1] + p1[2] * p2[2];

    // if the scalar product is -1, the two vectors are pointing in exactly the opposite
    // direction, meaning that our transformation matrix is simply a negative unit matrix
    if(epsPos > ::fabs(1. + c))
    {
        for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)
                mat[i][j] = (i == j) ? -1. : 0.;
        return;
    }

    // we need the "skew symmetric cross product of v" (v1) and it's square (v2)
    double v1[3][3] =
    {{    0., -v[2],  v[1]},
     {  v[2],    0., -v[0]},
     { -v[1],  v[0],    0.}};
    double v2[3][3];
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            v2[i][j] = 0.;
            for(int k = 0; k < 3; ++k)
                v2[i][j] += v1[i][k] * v1[k][j];
        }
    }

    // finally we calculate the rotation matrix as I + v1 + v2 * (1. / (1. + c))
    double f = 1. / (1. + c);
    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
            mat[i][j] = ((i == j) ? 1. : 0.) + v1[i][j] + f * v2[i][j];

    // expected result with test data -> ok
//    0.73680  -0.32931   0.59049
//   -0.30976   0.61190   0.72776
//   -0.60098  -0.71912   0.34884

    // testing with input data
    /*
    double test[3] = {p1[0], p1[1], p1[2]};
    std::cout << "- testing matrix with input array" << std::endl;
    std::cout << "- before " << test[0] << " " << test[1] << " " << test[2] << std::endl;
    applyInclination(test, mat);
    std::cout << "- after  " << test[0] << " " << test[1] << " " << test[2] << std::endl;
    */
}

void utilSampleToGridCell::applyInclination(double pt[3], double mat[3][3])
{
    double temp[3];
    for(int i = 0; i < 3; ++i)
    {
        temp[i] = 0.;
        for(int j = 0; j < 3; ++j)
            temp[i] += pt[j] * mat[j][i];
    }
    for(int i = 0; i < 3; ++i)
        pt[i] = temp[i];

    /*
    // get the directional angles and convert to radians
    static double const pi = 3.14159264;
    double az = pi * azimuth->GetComponent(r, 0) / 180.,
           dp = pi * dip->GetComponent(r, 0) / 180.;

    // directional components for oblique drillhole
    diff[0] = ::cos(dp) * ::sin(az);
    diff[1] = ::cos(dp) * ::cos(az);
    diff[2] *= ::sin(dp); // take care of the InvertDepth flag -> see above
    */
}

vtkIdType utilSampleToGridCell::appendRoundCylinder(vtkPoints* pts, float size)
{
    // The round cylinders are approximated with
    // "biquadratic quadratic hexahedron" cells
    // the point indices 0 to 7 are ordered like for the sphere (see appendSphere()),
    // except for the 3 last points that do not exist
    const static double sq2 = 1. / ::sqrt(2.);
    const static double shift[24][3] =
     // corners
    {{-sq2, -sq2, -1.},
     { sq2, -sq2, -1.},
     { sq2,  sq2, -1.},
     {-sq2,  sq2, -1.},
     {-sq2, -sq2,  0.},
     { sq2, -sq2,  0.},
     { sq2,  sq2,  0.},
     {-sq2,  sq2,  0.},
     // mid-edges
     { 0., -1., -1.},
     { 1.,  0., -1.},
     { 0.,  1., -1.},
     {-1.,  0., -1.},
     { 0., -1.,  0.},
     { 1.,  0.,  0.},
     { 0.,  1.,  0.},
     {-1.,  0.,  0.},
     {-sq2, -sq2, -.5},
     { sq2, -sq2, -.5},
     { sq2,  sq2, -.5},
     {-sq2,  sq2, -.5},
     // mid-faces
     {-1.,  0., -.5},
     { 1.,  0., -.5},
     { 0., -1., -.5},
     { 0.,  1., -.5}};

    // center coordinates
    double center[2][3];
    assert(2 <= pts->GetNumberOfPoints());
    pts->GetPoint(0, center[0]);
    pts->GetPoint(1, center[1]);

    // length of the segment
    double diff[3] =
    {center[1][0] - center[0][0],
     center[1][1] - center[0][1],
     center[1][2] - center[0][2]};
    double depth = ::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);

    // generate the rotation matrix
    double mat[3][3];
    calculateTransform(center[0], center[1], mat);

    // generate points and collect point indices
    vtkIdType ptIndex[24];
    for(int i = 0; i < 24; ++i)
    {
        double pt[3] =
        {
            shift[i][0] * static_cast<double>(size),
            shift[i][1] * static_cast<double>(size),
            shift[i][2] * depth
        };
        applyInclination(pt, mat);
        for(int i = 0; i < 3; ++i)
            pt[i] += center[0][i];
        ptIndex[i] = Points->InsertNextPoint(pt);
        if(nullptr != Normals)
            Normals->InsertNextTuple3(shift[i][0], shift[i][1], shift[i][2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_BIQUADRATIC_QUADRATIC_HEXAHEDRON, 24, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendQuadraticCylinder(vtkPoints* pts, float size)
{
    // Quadratic cylinders are simply hexahedron cells, like the cubes,
    // but with an elongation along z and a rotation according to azimuth and dip
    const static double shift[8][3] =
    {{-1., -1., -1.},
     { 1., -1., -1.},
     { 1.,  1., -1.},
     {-1.,  1., -1.},
     {-1., -1.,  0.},
     { 1., -1.,  0.},
     { 1.,  1.,  0.},
     {-1.,  1.,  0.}};

    // center coordinates
    double center[2][3];
    assert(2 <= pts->GetNumberOfPoints());
    pts->GetPoint(0, center[0]);
    pts->GetPoint(1, center[1]);

    // length of the segment
    double diff[3] =
    {center[1][0] - center[0][0],
     center[1][1] - center[0][1],
     center[1][2] - center[0][2]};
    double depth = ::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);

    // generate the rotation matrix
    double mat[3][3];
    calculateTransform(center[0], center[1], mat);

    // generate points and collect point indices
    vtkIdType ptIndex[8];
    for(int i = 0; i < 8; ++i)
    {
        double pt[3] =
        {
            shift[i][0] * static_cast<double>(size),
            shift[i][1] * static_cast<double>(size),
            shift[i][2] * depth
        };
        applyInclination(pt, mat);
        for(int i = 0; i < 3; ++i)
            pt[i] += center[0][i];
        ptIndex[i] = Points->InsertNextPoint(pt);
        if(nullptr != Normals)
            Normals->InsertNextTuple3(shift[i][0], shift[i][1], shift[i][2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_HEXAHEDRON, 8, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendTriangularCylinder(vtkPoints* pts, float size)
{
    // Triangular cylinders are "wedge" cells, but with an elongation along z
    // and a rotation according to azimuth and dip
    const static double x1 = 1.5, // adaptable to give a visibly good size
                        x2 = x1 / 2.,
                        y2 = x1 * sqrt(3.) / 2.;
    const static double shift[6][3] =
     // bottom face
    {{ x1,  0., -1.},
     {-x2, -y2, -1.},
     {-x2,  y2, -1.},
     // top face
     { x1,  0., 0.},
     {-x2, -y2, 0.},
     {-x2,  y2, 0.}};

    // center coordinates
    double center[2][3];
    assert(2 <= pts->GetNumberOfPoints());
    pts->GetPoint(0, center[0]);
    pts->GetPoint(1, center[1]);

    // length of the segment
    double diff[3] =
    {center[1][0] - center[0][0],
     center[1][1] - center[0][1],
     center[1][2] - center[0][2]};
    double depth = ::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);

    // generate the rotation matrix
    double mat[3][3];
    calculateTransform(center[0], center[1], mat);

    // generate points and collect point indices
    vtkIdType ptIndex[6];
    for(int i = 0; i < 6; ++i)
    {
        double pt[3] =
        {
            shift[i][0] * static_cast<double>(size),
            shift[i][1] * static_cast<double>(size),
            shift[i][2] * depth
        };
        applyInclination(pt, mat);
        for(int i = 0; i < 3; ++i)
            pt[i] += center[0][i];
        ptIndex[i] = Points->InsertNextPoint(pt);
        if(nullptr != Normals)
            Normals->InsertNextTuple3(shift[i][0], shift[i][1], shift[i][2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_WEDGE, 6, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendHexagonalCylinder(vtkPoints* pts, float size)
{
    // Hexagonal cylinders are "hexagonal prism" cells, but with an elongation along z
    // and a rotation according to azimuth and dip
    const static double x1 = 1.2, // adaptable to give a visibly good size
                        x2 = x1 / 2.,
                        y2 = x1 * sqrt(3.) / 2.;
    const static double shift[12][3] =
     // bottom face
    {{-x1,  0., -1.},
     {-x2, -y2, -1.},
     { x2, -y2, -1.},
     { x1,  0., -1.},
     { x2,  y2, -1.},
     {-x2,  y2, -1.},
     // top face
     {-x1,  0., 0.},
     {-x2, -y2, 0.},
     { x2, -y2, 0.},
     { x1,  0., 0.},
     { x2,  y2, 0.},
     {-x2,  y2, 0.}};

    // center coordinates
    double center[2][3];
    assert(2 <= pts->GetNumberOfPoints());
    pts->GetPoint(0, center[0]);
    pts->GetPoint(1, center[1]);

    // length of the segment
    double diff[3] =
    {center[1][0] - center[0][0],
     center[1][1] - center[0][1],
     center[1][2] - center[0][2]};
    double depth = ::sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);

    // generate the rotation matrix
    double mat[3][3];
    calculateTransform(center[0], center[1], mat);

    // generate points and collect point indices
    vtkIdType ptIndex[12];
    for(int i = 0; i < 12; ++i)
    {
        double pt[3] =
        {
            shift[i][0] * static_cast<double>(size),
            shift[i][1] * static_cast<double>(size),
            shift[i][2] * depth
        };
        applyInclination(pt, mat);
        for(int i = 0; i < 3; ++i)
            pt[i] += center[0][i];
        ptIndex[i] = Points->InsertNextPoint(pt);
        if(nullptr != Normals)
            Normals->InsertNextTuple3(shift[i][0], shift[i][1], shift[i][2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_HEXAGONAL_PRISM, 12, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendSphere(vtkPoints* pts, float size)
{
    // The spheres are approximated with "triquadratic hexahedron" cells
    // the point indices 0 to 7 are ordered like for a common hexahedron cell:
    //
    // Z
    // ^   7 -------- 6
    // | / |        / |
    // 4 -------- 5   |
    // |   |   Y  |   |
    // |   | /    |   |
    // |   3 ---- | - 2
    // | /        | /
    // 0 -------- 1  -> X
    //
    // and the following point indices are:
    //
    // 8 to 11 - mid-edge nodes on bottom plane
    // 12 to 15 - mid-edge nodes on top plane
    // 16 to 19 - mid-edge nodes on vertical edges
    // 20 to 25 - mid-face nodes along X, Y and Z direction
    // 26 - mid-volume node
    //
    // With the following array, the point coordinates will be generated from
    // center coordinates and radius:
    //
    const static double sq3 = 1. / ::sqrt(3.),
                        sq2 = 1. / ::sqrt(2.);
    const static double shift[27][3] =
     // corners
    {{-sq3, -sq3, -sq3},
     { sq3, -sq3, -sq3},
     { sq3,  sq3, -sq3},
     {-sq3,  sq3, -sq3},
     {-sq3, -sq3,  sq3},
     { sq3, -sq3,  sq3},
     { sq3,  sq3,  sq3},
     {-sq3,  sq3,  sq3},
     // mid-edges
     { 0., -sq2, -sq2},
     { sq2,  0., -sq2},
     { 0.,  sq2, -sq2},
     {-sq2,  0., -sq2},
     { 0., -sq2,  sq2},
     { sq2,  0.,  sq2},
     { 0.,  sq2,  sq2},
     {-sq2,  0.,  sq2},
     {-sq2, -sq2,  0.},
     { sq2, -sq2,  0.},
     { sq2,  sq2,  0.},
     {-sq2,  sq2,  0.},
     // mid-faces
     {-1.,  0.,  0.},
     { 1.,  0.,  0.},
     { 0., -1.,  0.},
     { 0.,  1.,  0.},
     { 0.,  0., -1.},
     { 0.,  0.,  1.},
     // mid-volume
     { 0.,  0.,  0.}};

    // center coordinates
    double center[3];
    pts->GetPoint(0, center);

    // generate points and collect point indices
    vtkIdType ptIndex[27];
    for(int i = 0; i < 27; ++i)
    {
        ptIndex[i] = Points->InsertNextPoint(
                         center[0] + shift[i][0] * static_cast<double>(size),
                         center[1] + shift[i][1] * static_cast<double>(size),
                         center[2] + shift[i][2] * static_cast<double>(size));
        if(nullptr != Normals)
            Normals->InsertNextTuple3(shift[i][0], shift[i][1], shift[i][2]);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_TRIQUADRATIC_HEXAHEDRON, 27, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendCube(vtkPoints* pts, float size)
{
    const static double f = .8;
    const static double shift[8][3] =
     // bottom face
    {{-f, -f, -f},
     { f, -f, -f},
     { f,  f, -f},
     {-f,  f, -f},
     // top face
     {-f, -f,  f},
     { f, -f,  f},
     { f,  f,  f},
     {-f,  f,  f}};

    // center coordinates
    double center[3];
    pts->GetPoint(0, center);

    // generate points and collect point indices
    vtkIdType ptIndex[8];
    for(int i = 0; i < 8; ++i)
    {
        ptIndex[i] = Points->InsertNextPoint(
                         center[0] + shift[i][0] * static_cast<double>(size),
                         center[1] + shift[i][1] * static_cast<double>(size),
                         center[2] + shift[i][2] * static_cast<double>(size));

        // note that this is just for filling the array: it will generate black
        // ("fully shadowed") objects with Gouraud shading!
        if(nullptr != Normals)
            Normals->InsertNextTuple3(0., 0., 0.);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_HEXAHEDRON, 8, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendTetrahedron(vtkPoints* pts, float size)
{
    const static double f = 1.5, // radius of outer sphere
                        x1 = f * 2. * ::sqrt(2.) / 3.,
                        x2 = f * ::sqrt(2.) / 3.,
                        y2 = f * 2. / ::sqrt(6.),
                        zb = f / 3.;
    const static double shift[4][3] =
     // corners
    {{ x1,  0., -zb},
     {-x2,  y2, -zb},
     {-x2, -y2, -zb},
     // top point
     { 0.,  0.,   f}};

    // center coordinates
    double center[3];
    pts->GetPoint(0, center);

    // generate points and collect point indices
    vtkIdType ptIndex[4];
    for(int i = 0; i < 4; ++i)
    {
        ptIndex[i] = Points->InsertNextPoint(
                         center[0] + shift[i][0] * static_cast<double>(size),
                         center[1] + shift[i][1] * static_cast<double>(size),
                         center[2] + shift[i][2] * static_cast<double>(size));

        // note that this is just for filling the array: it will generate black
        // ("fully shadowed") objects with Gouraud shading!
        if(nullptr != Normals)
            Normals->InsertNextTuple3(0., 0., 0.);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_TETRA, 4, ptIndex);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendOctahedron(vtkPoints* pts, float size)
{
    // basic size numbers for polyhedron
    const static int numPoints = 6,
                     numFaces = 8,
                     numPointsPerFace = 3;

    // an octahedron does not exist as a primitive cell type, but we can
    // generate it as a "polyhedron" cell, by defining points and also faces
    const static double shift[numPoints][3] =
     // bottom point
    {{ 0.,  0., -1.},
     // center ring
     {-1.,  0.,  0.},
     { 0., -1.,  0.},
     { 1.,  0.,  0.},
     { 0.,  1.,  0.},
     // top point
     { 0.,  0.,  1.}};

    // center coordinates
    double center[3];
    pts->GetPoint(0, center);

    // remember the current starting index of the points
    vtkIdType pointsBase = Points->GetNumberOfPoints();

    // generate points and collect point indices
    vtkIdType ptIndex[numPoints];
    for(int i = 0; i < numPoints; ++i)
    {
        ptIndex[i] = Points->InsertNextPoint(
                         center[0] + shift[i][0] * static_cast<double>(size),
                         center[1] + shift[i][1] * static_cast<double>(size),
                         center[2] + shift[i][2] * static_cast<double>(size));

        // note that this is just for filling the array: it will generate black
        // ("fully shadowed") objects with Gouraud shading!
        if(nullptr != Normals)
            Normals->InsertNextTuple3(0., 0., 0.);
    }

    // relative point indices for the polyhedron faces
    const static vtkIdType fptIds[numFaces][numPointsPerFace] =
    {{5, 2, 1},
     {5, 3, 2},
     {5, 4, 3},
     {5, 1, 4},
     {0, 2, 1},
     {0, 3, 2},
     {0, 4, 3},
     {0, 1, 4}};

    // generate an id list that contains the face point indices in
    // the following format:
    //  <num pts for face> <face indices...> <next face...>
    vtkIdType facePtIds[numFaces * (numPointsPerFace + 1)];
    int i = 0;
    for(vtkIdType f = 0; f < numFaces; ++f)
    {
        facePtIds[i++] = numPointsPerFace;
        for(vtkIdType p = 0; p < numPointsPerFace; ++p)
            facePtIds[i++] = pointsBase + fptIds[f][p];
    }

    // insert the index list into the grid
    //vtkIdType cellId = Grid->InsertNextCell(VTK_POLYHEDRON, numFaces, facePtIds); // other possibility
    vtkIdType cellId = Grid->InsertNextCell(VTK_POLYHEDRON, numPoints, ptIndex, numFaces, facePtIds);

    return cellId;
}

vtkIdType utilSampleToGridCell::appendBlastPile(vtkPoints* pts, float size)
{
    // Triangular cylinders are "wedge" cells, but with an elongation along z
    // and a rotation according to azimuth and dip
    const static double z1 = 2., // adaptable to give a visibly good "thickness"
                        y2 = z1 * sqrt(3.) / 2.;
    const static double shift[6][3] =
     // "left" face
    {{-1.,  0., z1},
     {-2.,  y2, 0.},
     {-2., -y2, 0.},
     // "right" face
     { 1.,  0., z1},
     { 2.,  y2, 0.},
     { 2., -y2, 0.}};

    // center coordinates
    double center[3];
    pts->GetPoint(0, center);

    // generate points and collect point indices
    vtkIdType ptIndex[6];
    for(int i = 0; i < 6; ++i)
    {
        ptIndex[i] = Points->InsertNextPoint(
                         center[0] + shift[i][0] * static_cast<double>(size),
                         center[1] + shift[i][1] * static_cast<double>(size),
                         center[2] + shift[i][2] * static_cast<double>(size));

        // note that this is just for filling the array: it will generate black
        // ("fully shadowed") objects with Gouraud shading!
        if(nullptr != Normals)
            Normals->InsertNextTuple3(0., 0., 0.);
    }

    // insert the index list into the grid
    vtkIdType cellId = Grid->InsertNextCell(VTK_WEDGE, 6, ptIndex);

    return cellId;
}
