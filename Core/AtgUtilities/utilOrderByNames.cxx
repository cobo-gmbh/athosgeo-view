/*=========================================================================

   Program: AthosGEO
   Module:  utilOrderByNames.cxx

   Copyright (c) 2023 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <iostream>
#include <cassert>
#include <string>
#include <utilTrackDefs.h>
#include <utilNormNames.h>
#include <utilOrderByNames.h>

bool OrderByNames::operator()(vtkAbstractArray* a1, vtkAbstractArray* a2)
{
    static const char* order[] =
    {
        "vtkOriginalProcessIds",
        "vtkCompositeIndexArray",
        "vtkOriginalIndices",
        "vtkOriginalCellIds",
        "vtkOriginalPointIds",
        "vtkOriginalRowIds",
        "Structured Coordinates",
        nullptr
    };

    std::string a1Name = a1->GetName() ? a1->GetName() : "";
    std::string a2Name = a2->GetName() ? a2->GetName() : "";
    int a1Index = VTK_INT_MAX, a2Index = VTK_INT_MAX;

#ifdef TRACK_COLUMN_SORT
        std::cout << "compare <" << a1Name << ">-<" << a2Name << ">";
#endif

    for(int cc = 0; order[cc] != nullptr; cc++)
    {
        if(a1Index == VTK_INT_MAX && a1Name == order[cc])
            a1Index = cc;
        if(a2Index == VTK_INT_MAX && a2Name == order[cc])
            a2Index = cc;
    }

    // this is the original PV logic, and we follow it if at least one of the two
    // names falls in the above "order" group
    if((a1Index < VTK_INT_MAX) || (a2Index < VTK_INT_MAX))
    {
#ifdef TRACK_COLUMN_SORT
            std::cout << " returning " << (a1Index < a2Index) << std::endl;
#endif

        if(a1Index < a2Index)
            return true;
        if(a2Index < a1Index)
            return false;

        // we can reach here only when both array names are not in the "priority"
        // set or they are the same (which does happen, see BUG #9808).
        // cobo: what's this??
        // - it cannot happen that both indices are VTK_INT_MAX here because of the if
        //   above that ensures that ONE is at least less than that! so the first bracket
        //   is for the garbage because always false
        // - what remains is a check that both names should be the same - what for?
        //   shouldn't it be at least the opposite, i.e. a1Name != a2Name ??
        //assert((a1Index == VTK_INT_MAX && a2Index == VTK_INT_MAX) || (a1Name == a2Name));
        //return (a1Name < a2Name);
    }

    // 1) order by AthosGEO categories (according to list)
    static const std::vector<int> cat(std::initializer_list<int>(
    {
        utilNormNames::TY_NAME,
        utilNormNames::TY_DATE,
        utilNormNames::TY_CATEGORY,
        utilNormNames::TY_PERIOD,
        utilNormNames::TY_TONNAGE,
        utilNormNames::TY_WEIGHT,
        utilNormNames::TY_SPECTONNAGE,
        utilNormNames::TY_DERIVED,
        utilNormNames::TY_DIRECT,
        utilNormNames::TY_RATIO,
        utilNormNames::TY_COORDINATE,
        utilNormNames::TY_DIMENSION,
        utilNormNames::TY_DIMENSION3,
        utilNormNames::TY_INDEX,
        utilNormNames::TY_TEMPORARY,
        utilNormNames::TY_IGNORE
    }));

    // note: the getType() function will always find a value that is within the above cat vector
    int cat1 = std::find(cat.begin(), cat.end(), utilNormNames::getType(a1Name)) - cat.begin(),
        cat2 = std::find(cat.begin(), cat.end(), utilNormNames::getType(a2Name)) - cat.begin();

#ifdef TRACK_COLUMN_SORT
        std::cout << " cat " << cat1 << "/" << cat2;
        if(cat1 != cat2)
            std::cout << " returning " << (cat1 < cat2) << std::endl;
#endif

    if(cat1 < cat2)
        return true;
    if(cat1 > cat2)
        return false;

    // 2) order by name indices
    int inx1 = utilNormNames::getIndex(a1Name).first,
        inx2 = utilNormNames::getIndex(a2Name).first;

#ifdef TRACK_COLUMN_SORT
        std::cout << " inx " << inx1 << "/" << inx2;
        if(inx1 != inx2)
            std::cout << " returning " << (inx1 < inx2) << std::endl;
#endif

    if(inx1 < inx2)
        return true;
    if(inx1 > inx2)
        return false;

    // 3) order alphabetically
    // note: as above, we will reach here if both names are the same - which _should_ not happen
    //   but looks like it does...
    assert(a1Name != a2Name);

#ifdef TRACK_COLUMN_SORT
        std::cout << " returning " << (a1Name < a2Name) << std::endl;
#endif

    return a1Name < a2Name;
}
