/*=========================================================================

   Program: AthosGEO
   Module:  utilVector2D.h

   Copyright (c) 2021 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#ifndef utilVector2D_h
#define utilVector2D_h

#include <utility>
#include <cmath>
#include <AtgUtilitiesModule.h>

class ATGUTILITIES_EXPORT utilVector2D
{
public:

    // default constructor
    inline utilVector2D()
    : x_(0.), y_(0.) {}

    // constructor with 2D coordinates
    utilVector2D(double x, double y);

    void getBoundaryBox(utilVector2D const& pt2, double* bbox);

    static bool linesIntersect(utilVector2D const& p11, utilVector2D const& p12,
                               utilVector2D const& p21, utilVector2D const& p22,
                               double& outDistLine1, double& outDistLine2);

    static bool lineSegmentsIntersect(utilVector2D const& p11, utilVector2D const& p12,
                                      utilVector2D const& p21, utilVector2D const& p22);

    inline double getX() const
    {
        return x_;
    }

    inline double getY() const
    {
        return y_;
    }

    utilVector2D& normalize();

    double azimuth(utilVector2D const& pt) const;

    bool operator<(utilVector2D const& pt2) const;

    // the angle that of a vector from "this" point to anogher one, counted from the x positive
    // axix in a positive sense of rotation.
    // the function tells whether the first point's angle is less than the second point's
    // note: this function does not actually calculate any angle
    bool isLessAngle(utilVector2D const& pt1, utilVector2D const& pt2) const;

    bool are3PointsColinear(utilVector2D const& pt2, utilVector2D const& pt3) const;

private:

    static bool isZero(double f);

    inline static bool isLess(double f1, double f2)
    {
        return (f1 < f2) && !isZero(f1 - f2);
    }

    bool static isGreater(double f1, double f2)
    {
        return (f1 > f2) && !isZero(f1 - f2);
    }

    // assuming that we have a normalized vector, get the octant, counting from the positive
    // x axis in a positive sense of rotation, from 0 to 7. Then a normal distance is added
    // that grows towards the next octant
    // note: the purpose of this is to have a value that can be compared in order to give
    //   the same order as calculating an angle, without actually doing it (performance)
    double octantDist();

    double x_,
           y_;

};

#endif
