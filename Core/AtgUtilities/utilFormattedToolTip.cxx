/*=========================================================================

   Program: AthosGEO
   Module:  utilFormattedToolTip.cxx

   Copyright (c) 2018 cobo GmbH
   All rights reserved.

   AthosGEO View is a free software based on ParaView; you can redistribute
   and/or modify it under the same terms as ParaView

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/

#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/regex.hpp>
#include <utilFormattedToolTip.h>

utilFormattedToolTip::utilFormattedToolTip(char const* rawText)
{
    // a null raw text would crash, but now we simply get an empty form text
    if(nullptr == rawText)
        return;

    // split raw text into rows and words
    std::vector<std::vector<std::string>> pt = parseRaw(rawText);

    // handle _bold_ and :italic: words
    accentuation(pt);

    // handle bullet lists
    bulletList(pt);

    // put words together into a line
    formText = "<html>";
    for(auto it = pt.begin(); it != pt.end(); ++it)
    {
        formText.append("\n");
        formText.append(boost::algorithm::join(*it, " "));
    }
    formText.append("<\n</html>");
}

std::string const& utilFormattedToolTip::formattedText() const
{
    return formText;
}

std::vector<std::vector<std::string>>
    utilFormattedToolTip::parseRaw(std::string const& rawStr)
{
    // split raw text into lines
    std::vector<std::string> rows;
    static const boost::regex linesregx("\\r\\n|\\n\\r|\\n|\\r");
    boost::sregex_token_iterator li(rawStr.begin(), rawStr.end(), linesregx, -1);
    boost::sregex_token_iterator end;
    for(; li != end; ++li)
        rows.push_back(li->str());

    // remove leading and trailing space and collapse consecutive spaces in between
    for(auto it = rows.begin(); it != rows.end(); ++it)
        boost::algorithm::trim_all(*it);

    // merge rows if no empty row is in between
    for(int r = 0; r < rows.size(); ++r)
    {
        if(rows[r].empty())
        {
            rows.erase(rows.begin() + r);
            --r;
        }

        else if(((r + 1) < rows.size()) && (!rows[r + 1].empty()))
        {
            rows[r].append(" ");
            rows[r].append(rows[r + 1]);
            rows.erase(rows.begin() + r + 1);
            --r;
        }
    }

    // split rows into words
    std::vector<std::vector<std::string>> words;
    static const boost::regex wordsregx("\\ ");
    for(auto it = rows.begin(); it != rows.end(); ++it)
    {
        boost::sregex_token_iterator wi(it->begin(), it->end(), wordsregx, -1);
        std::vector<std::string> rwords;

        for(; wi != end; ++wi)
            rwords.push_back(wi->str());

        words.push_back(rwords);
    }

    return words;
}

void utilFormattedToolTip::accentuation(std::vector<std::vector<std::string>>& pt)
{
    for(auto rit = pt.begin(); rit != pt.end(); ++rit)
    {
        for(auto wit = rit->begin(); wit != rit->end(); ++wit)
        {
            // should not happen...
            if(wit->empty())
                continue;

            // codes in the string
            std::string accCodes("_:");
            std::vector<std::string> accHtml{"b", "i"};

            // replace codes by HTML
            for(int n = 0; n < accCodes.size(); ++n)
            {
                if((accCodes.at(n) == wit->at(0)) && (accCodes.at(n) == wit->at(wit->size() - 1)))
                {
                    *wit = std::string("<") + accHtml.at(n) + ">" +
                           wit->substr(1, wit->size() - 2) +
                           "</" + accHtml.at(n) + ">";
                }
            }
        }
    }
}

void utilFormattedToolTip::bulletList(std::vector<std::vector<std::string>>& pt)
{
    bool lastWasListItem = false;

    for(auto rit = pt.begin(); rit != pt.end(); ++rit)
    {
        // we need at least two "words" on a row
        if(2 > rit->size())
            continue;

        // if the first "word" is only a '-', we assume this is a bullet list item
        // and connect it with the next word with a TAB
        std::string& fs = rit->at(0);
        bool listItem = (1 == fs.size()) && ('-' == fs[0]);

        // if this is now a list, but was not a list before, we need to "open" it
        if(listItem)
        {
            // remove the '-' marker
            rit->erase(rit->begin());
            std::string& first = rit->at(0);

            // prepend list and list item start
            std::string prepend;
            if(!lastWasListItem)
                prepend = "<ul>";
            prepend.append("<li>");
            first = prepend + first;

            // append list item end tag
            rit->at(rit->size() - 1).append("</li>");
        }

        else
        {
            std::string prepend;
            if(lastWasListItem)
                prepend = "</ul>";
            prepend.append("<p>");
            fs = prepend + fs;

            rit->at(rit->size() - 1).append("</p>");
        }

        lastWasListItem = listItem;
    }
}
